import {Headers} from '@angular/http';
import {UserService} from '../services/user.service';
import {DateFormatting} from './util';
import {environment} from '../environments/environment';
import {Cache} from './cache';


/**
 *  Environment type configuration
 */


export class ApiConfig extends DateFormatting {

  static API_DEFAULT_URL = ApiConfig.CacheHostname();
  static API_KEY = environment.API_KEY;
  protected headers = {headers: this.setHeaders()};
  protected authToken: any;


  static CacheHostname() {
      const API_URL = environment.API_URL;
      return `${API_URL.prefix}${API_URL.url_route}`;
  }


  constructor(private myUserService: UserService) {
    super();
  }

  /**
   * This is used to Set Headers on before request
   * @returns {Headers}
   */
  protected setHeaders(): Headers {
    this.authToken = this.myUserService.getAuthUser();


    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    };
    if (this.myUserService.isLoggedIn()) {
      headersConfig['Authorization'] = `${this.myUserService.getAuthUserToken()}`;
    }
    if (Cache.get('educhoice_language')) {
      headersConfig['locale'] = Cache.get('educhoice_language');
    }
    if (ApiConfig.API_KEY) {
      headersConfig['API-KEY'] = ApiConfig.API_KEY;
    }
    // if (ApiConfig.API_KEY) {
    //   headersConfig['API-KEY'] = ApiConfig.API_KEY;
    // }
    return new Headers(headersConfig);
  }

  public setHeader2(headersConfig: any): Headers {
    this.authToken = this.myUserService.getAuthUser();

    // const headersConfig = headersConfig;
    if (this.myUserService.isLoggedIn()) {
      headersConfig['Authorization'] = `${this.myUserService.getAuthUserToken()}`;
    }
    if (ApiConfig.API_KEY) {
      headersConfig['API-KEY'] = ApiConfig.API_KEY;
    }
    // if (ApiConfig.API_KEY) {
    //   headersConfig['API-KEY'] = ApiConfig.API_KEY;
    // }
    return new Headers(headersConfig);
  }


}

