/**
 * Created by Serere on 4/11/2018.
 */
declare const $: any;

export class TableExport {

  static download_csv(csv, filename) {
    let csvFile;
    let downloadLink;

    // CSV FILE
    csvFile = new Blob([csv], {type: "text/csv"});

    // Download link
    downloadLink = document.createElement("a");

    // File name
    downloadLink.download = filename;

    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Make sure that the link is not displayed
    downloadLink.style.display = "none";

    // Add the link to your DOM
    document.body.appendChild(downloadLink);

    // Lanzamos
    downloadLink.click();
  }

  static export_table_to_csv(html, filename) {
    let csv = [];
    let rows = document.querySelectorAll("table tr");
    console.log(rows);
    for (let i = 0; i < rows.length; i++) {
      let row = [], cols = rows[i].querySelectorAll("td, th");

      for (let j = 0; j < cols.length; j++)
        { //noinspection TypeScriptUnresolvedVariable
          row.push(cols[j].innerHTML);
        }

      csv.push(row.join(","));
    }

    // Download CSV
    TableExport.download_csv(csv.join("\n"), filename);
  }

  static downloadCSV(buttonId, tableId, filename){
    document.querySelector(buttonId).addEventListener("click", function () {
      let html = document.querySelector(tableId).outerHTML;
      TableExport.export_table_to_csv(html, filename);
    });
  }
}

