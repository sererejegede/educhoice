export const CONSTANTS = {
    DEFAULT_ERROR_MESSAGE: 'Error was encountered while processing this request. please try again',
    DEFAULT_SUCCESS_MESSAGE: 'Success',
    TIMEOUT_5: 5000,
    TIMEOUT_9: 9000,
    TIMEOUT_MIL_2: 200,
    TIMEOUT_MIL_5: 500,
    TIMEOUT_15: 15000,
    TIMEOUT_20: 20000,
    PAYMENT_CONFIRMATION_COMPLETED: 'payment-confirmation-completed',
    PAYMENT_CONFIRMED: 'payment-confirmed',
    SHOW_CONFIRM: 'show-confirm',
    SHOW_RECEIPT: 'show-receipt',
    SHOW_TABS: 'show-tabs',
    SWITCH_DISPLAY_MODE: 'switch-display-mode',
    SEARCH_COMPLETED: 'search-completed',
    FILL_GLOBAL_FORM_FIELD: 'fill-global-form-field',
    ACCEPT_LOGIN: 'accept-login',
};

export const abbreviations = [
  {short: 'TC', long: 'TOTAL CREDIT'},
  {short: 'TWGP', long: 'TOTAL WEIGHTED GRADE POINTS'},
  {short: 'GPA', long: 'GRADE POINT AVERAGE'},
  {short: 'CTC', long: 'CUMMULATIVE TOTAL CREDIT'},
  {short: 'CTWGP', long: 'CUMMULATIVE TOTALWEIGHTED GRADE POINT POPOINTPOINTS'},
  {short: 'CGPA', long: 'CUMMULATIVE GRADE POINT AVERAGE'},
  {short: 'REMM', long: 'REMARKS'},
  {short: 'NA', long: 'NOT APPLICABLE'},
  {short: 'ABS', long: 'ABSENT WITH PERMISSION'},
  {short: 'RNA', long: 'RESULT NOT AVAILABLE'},
  {short: 'W', long: 'WITHDRAWAL'},
  {short: 'VW', long: 'VOLUNTARY WITHDRAWAL'},
  {short: 'PRI', long: 'PROBATION 1'},
  {short: 'PR2', long: 'PROBATION 2'},
  {short: 'NR', long: 'NOT REGISTERED FOR'},
  {short: 'GS', long: 'IN GOOD STANDING'},
  {short: 'NGS', long: 'NOT IN GOOD STANDING'},
  {short: 'SDC', long: 'STUDENT DISCIPLINARY COMMITTEE'},
  {short: 'DHR', long: 'DEANS HONOURS ROLL'},
  {short: 'VCL', long: 'VICE CHANCELLOR’S LIST'},
  {short: 'NP', long: 'NO PAYMENT'},
  {short: 'SOS', long: 'SUSPENSION OF STUDIES'},
];

export const denominations = [
  {
    name: 'Anglican',
    value: 'Anglican'
  },
  {
    name: 'Baptist',
    value: 'Baptist'
  },
  {
    name: 'Catholic',
    value: 'Catholic'
  },
  {
    name: 'Methodist',
    value: 'Methodist'
  },
  {
    name: 'Presbyterian',
    value: 'Presbyterian'
  }
];
