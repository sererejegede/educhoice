// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  API_KEY: '',
  API_URL: {
    // prefix: 'http://',
    // url_route: 'ws.upltest.com/api/v1/',
    //   prefix: 'https://',
    //   url_route: 'ws.educhoice.ng/api/v1/',
    prefix: 'https://',
    url_route: 'ws.educhoicetest.com.ng/api/v1/',
    login: 'auth/login',
    ping: 'ping',
    programme: 'programme',
    schoolType: 'school-type',
    programmeType: 'programme-type',
    faculty: 'faculty',
    department: 'department',
    level: 'level',
    degree: 'degree',
    course_of_study: 'course-of-study',
    session: 'session',
    course: 'course-manager/course',
    curriculum: 'course-manager/curriculum',
    curriculum_course: 'course-manager/curriculum-course',
    permission: 'administration/permissions',
    active_hour: 'active-hour',
    profile: 'profile',
    filteredCourseOfStudy: 'course-of-study',
    hostel: 'hostel_management',
    student: 'student',
    room: 'room_management',
    hostel_session: 'hostel_sesion_management',
    bedspace: 'bed_space_management',
    applicant_report: 'applicant_report',
    course_report: 'course_report',
    report: 'report',
    applicant_interview: 'applicant_interview',
    hostel_allocation: 'hostel_allocation_management',
    settings: 'settings',
    zone: 'zone',
    matric_no: 'matric_no',
    screening_center: 'screening_center',
    course_manager: 'course-manager',
    reQueryURL: {
      general: 'transactions/re_query',
      pay_choice: 'transactions/validate_paychoice',
      etranzact: 'transactions/validate_etranzact',
      ebillspay: 'transactions/validate_ebillspay'
    }
  },
  CAPTCHA_KEY: '6LcYtXoUAAAAABwUy5STfG2irp8bewj8vYAz1BV5',
  DEFAULT_DOMAIN: 'school.app.com',
  UPPERLINK_URL: 'school.app.com',
  ETRANSACT_URL: 'http://demo.etranzact.com/bankIT/',
  // XPRESSPAY_URL: 'https://staging.payxpress.com/xp-gateway/v2',
  XPRESSPAY_URL: 'https://xpresspayonline.com/xp-gateway/v2'
};
