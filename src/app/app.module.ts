///<reference path='backend/backend.module.ts'/>
///<reference path='portal/studentArea/student-routing-module.module.ts'/>
import {BrowserModule, Title} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {NotificationService} from '../services/notification.service';
import {UserService} from '../services/user.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ValidationErrorService} from '../services/validation-error.service';
import {EventsService} from '../services/event.service';
import {AuthenticationService} from '../services/authentication.service';
import {ApiHandlerService} from '../services/api-handler.service';
import {ScriptLoaderService} from '../services/script-loader.service';
import {SchoolService} from '../services/school.service';
import {StudentLoginComponent} from './portal/studentArea/student-authentication/student-login.component';
import {ReportServiceService} from '../services/api-service/report-service.service';
import {StaffConfigService} from '../services/api-service/staff-config.service';
import {CourseManagementServiceService} from '../services/api-service/course-management-service.service';
import {AdministationServiceService} from '../services/api-service/administation-service.service';
import {CopingsService} from '../services/copings.service';
import {AdmissionsService} from '../services/api-service/admissions.service';
import {LogoutServiceService} from '../services/api-service/login-service.service';
import {StudentServiceService} from '../services/api-service/student-service.service';
import {ResultsService} from '../services/api-service/results.service';
import {StudentDashboardModule} from './portal/studentArea/student-dashboard/dashboard.module';
import {DataTablesModule} from 'angular-datatables';
import {NotificationModule} from '../shared/modules/notification.module';
import {LayoutModule} from './layouts/layout.module';
import {PipeModule} from '../shared/modules/pipe.module';
import {CKEditorModule} from 'ng2-ckeditor';
import {MatAutocompleteModule, MatInputModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ApplicationTypeComponent} from './portal/studentArea/applicant/application-type/application-type.component';
import {ErrorPageComponent} from './error-page/error-page.component';
import {RoleGuardService} from '../services/role-guard.service';
import {EmitPaymentService} from '../services/emit-payment-service';
import {OpenProgrammesComponent} from './portal/studentArea/applicant/open-programmes/open-programmes.component';
import {Select2Module} from 'ng2-select2';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {PaymentModule} from './portal/staffArea/staff-dashboard/payment/payment.module';
import {AgentService} from '../services/agent.service';
import {EmitSettingsService} from '../services/emitSettings.service';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {EmailAuthComponent} from './email-auth/email-auth.component';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {Paginator} from '../utils/paginator';
import {environment} from "../environments/environment";
import {RecaptchaModule} from "angular5-google-recaptcha";
import { ParentResultViewComponent } from './parent-result-view/parent-result-view.component';
// import {NgxCaptchaModule} from "ngx-captcha";


const APP_ROUTES: Routes = [
  {path: '', component: StudentLoginComponent},
  // {path: 'apply', component: ApplicationTypeComponent},
  {path: 'apply', component: OpenProgrammesComponent},
  {path: 'apply/:id', component: ApplicationTypeComponent},
  {path: 'applicant', loadChildren: './portal/studentArea/applicant/applicant.module#ApplicantModule'},
  {path: 'home', loadChildren: './backend/backend.module#BackendModule'},
  {path: 'staff', loadChildren: './portal/staffArea/staff-routing-module.module#StaffRoutingModule'},
  {path: 'student', loadChildren: './portal/studentArea/student-routing-module.module#StudentRoutingModule'},
  {path: 'forgot-password', component: ForgotPasswordComponent},
  {path: 'home/forgot-password', component: ForgotPasswordComponent},
  {path: 'reset-password', component: EmailAuthComponent},
  {path: 'home/reset-password', component: EmailAuthComponent},
  {path: 'result-view', component: ParentResultViewComponent},
  {
    canActivate: [RoleGuardService],
    path: 'change-password', component: ChangePasswordComponent
  },
  {path: '**', component: ErrorPageComponent},


];

@NgModule({
  declarations: [
    AppComponent,
    StudentLoginComponent,
    ApplicationTypeComponent,
    OpenProgrammesComponent,
    ErrorPageComponent,
    ForgotPasswordComponent,
    EmailAuthComponent,
    ChangePasswordComponent,
    ParentResultViewComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    PipeModule,
    MatAutocompleteModule,
    MatInputModule,
    BrowserAnimationsModule,
    Select2Module,
    LayoutModule,
    CKEditorModule,
    NotificationModule,
    MultiselectDropdownModule,
    PaymentModule,
    // ReCaptchaModule,
    // NgxCaptchaModule,
    RecaptchaModule.forRoot({
      siteKey: environment.CAPTCHA_KEY,
    }),
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [
    Title,
    ScriptLoaderService,
    ApiHandlerService,
    NotificationService,
    UserService,
    ValidationErrorService,
    EventsService,
    AuthenticationService,
    SchoolService,
    StaffConfigService,
    CopingsService,
    LogoutServiceService,
    StudentServiceService,
    CourseManagementServiceService,
    AdministationServiceService,
    AdmissionsService,
    ResultsService,
    EmitPaymentService,
    EmitSettingsService,
    RoleGuardService,
    AgentService,
    Paginator
  ],
  bootstrap: [AppComponent]
})


export class AppModule {
}
