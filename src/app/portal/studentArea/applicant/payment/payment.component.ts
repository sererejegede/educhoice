import {Component, ElementRef, EventEmitter, OnInit, Output} from '@angular/core';
import {ResultsService} from '../../../../../services/api-service/results.service';
import {UserService} from '../../../../../services/user.service';
import {AdmissionsService} from '../../../../../services/api-service/admissions.service';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import {NotificationService} from '../../../../../services/notification.service';
import {EmitPaymentService} from '../../../../../services/emit-payment-service';
import {Cache} from '../../../../../utils/cache';
import {Router} from '@angular/router';
import * as CryptoJS from 'crypto-js';
import {SECRET_PAYMENT_KEY} from '../../../../../utils/magic-methods';
import {environment} from '../../../../../environments/environment';


declare const $: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  public activeApplicant;
  public eBillsPay = {
    id: null,
    amount: 0
  };
  public applicationFee = false;
  public acceptanceFee = false;
  public payChoiceDiv = false;
  public loadIframe = false;
  public activeApplicantProgramme;
  public appSettings;
  public school;
  public allPaymentMethods: any[] = [];
  public allActiveApplications;
  public applicationAmount: any = '';
  public paystackRef = '';
  public feeDescription = '';
  public stackButton = false;
  public acceptanceFeeAmount: any;
  public applicationName: any = '';
  public applicantId = 0;
  public customPayForm: FormGroup;
  public acceptanceFeeForm: FormGroup;
  public ETRANSACT_URL = environment.ETRANSACT_URL;
  public etransactObject = {
    AMOUNT: 0,
    DESCRIPTION: '',
    LOGO_URL: '',
    NOTIFICATION_URL: '',
    RESPONSE_URL: '',
    TERMINAL_ID: '',
    TRANSACTION_ID: '',
    CHECKSUM: 'CHECKSUM',
    COL1: 'COL1'
  }
  public load = {
    message: {
      create: 'Make '
    },
    requesting: {
      create: false
    }
  };
  static customPayForm = function () {
    return {
      payment_method: ['', Validators.compose([Validators.required])],
      amount: ['']

    };
    // email,first_name,last_name,course_of_study_id
  };

  static acceptanceFeeForm = function () {
    return {
      payment_method: ['', Validators.compose([Validators.required])],
      amount: ''

    };
    // email,first_name,last_name,course_of_study_id
  };


  constructor(private admissionService: AdmissionsService,
              private userService: UserService,
              private Alert: NotificationService,
              private fb: FormBuilder,
              private emitPaymentService: EmitPaymentService,
              private el: ElementRef,
              private router: Router) {

    this.applicantId = this.userService.getAuthUser()['login']['user']['id'];
    this.getApplicantProfile(this.applicantId);
    if (Cache.get('app_settings')) {
      this.school = Cache.get('app_settings');
      this.appSettings = this.school['settings'];
      this.allPaymentMethods = this.appSettings['payment_method'];
    }

    this.customPayForm = this.fb.group(PaymentComponent.customPayForm());
    this.acceptanceFeeForm = this.fb.group(PaymentComponent.acceptanceFeeForm());
    Cache.set('user_is_applicant', true);
  }

  ngOnInit() {
    this.getActiveApplications();
    // this.getAcceptanceFee();
  }


  public prepareApplicantForPayment() {
    const applicantEmail = this.activeApplicant['email'];
    this.admissionService.prepareApplicantForPayment(applicantEmail).subscribe(
      (applicantPaymentResponse) => {
        console.log('Applicant payment response ', applicantPaymentResponse);
      });
  }

  public forceApplicantPayment() {
    const applicantid = this.activeApplicant['id'];
    this.admissionService.forceApplicantPayment(applicantid).subscribe(
      (applicantForcePayResponse) => {
        console.log('Applicant force pay response ', applicantForcePayResponse);
        // this.getApplicantProfile();
      },
      (error) => {
        console.log('Could not load applicant force pay response', error);
      }
    );
  }

  public getApplicantProfile(id) {
    this.admissionService.getApplicantProfile(id).subscribe(
      (applicantProfileResponse) => {
        this.activeApplicant = applicantProfileResponse['applicant'];
        this.applicationAmount = this.activeApplicant['application']['amount'];
        this.activeApplicant['programme_object'] = this.activeApplicant['application']['programme'];
        this.acceptanceFeeAmount = this.activeApplicant['programme_object']['acceptance_fee']['amount'];
        this.acceptanceFeeForm.patchValue({
          amount: this.acceptanceFeeAmount
        });
        this.applicationName = (this.activeApplicant['application']) ? this.activeApplicant['application']['name']
          : '';
        if (this.activeApplicant['payment_status'] === '1') {
          this.emitPaymentService.emitChange('true');
          this.applicationFee = true;
        }
        const applicantIsOnAdmissionList = this.activeApplicant['admission_status'] === '1';
        const applicantHasPaidAcceptanceFee = this.activeApplicant['acceptance_fee_status'] === '0';

        if (applicantIsOnAdmissionList && applicantHasPaidAcceptanceFee) { //reverse the logic; the ! should be on acceptancefee
          this.Alert.info('You have been shortlisted for admission!. Please pay your acceptance fee');
          this.triggerModalOrOverlay('open', 'acceptanceFeePayment');
          this.acceptanceFee = true;
        }
        this.applicationAmount = this.activeApplicant['application']['amount'];
        console.log('Applicant profile response form payment now', applicantProfileResponse);
      },
      (error) => {
        console.log('Could not load applicant profile response', error);
      }
    );
  }

  // public processApplicantApplicantionType(applicsntProgramme) {
  //     if (this.allActiveApplications.length > 0) {
  //         if(this.allActiveApplications.in)
  //     }
  // }

  private generateTransactionId() {
    const applicantDateils = 'anything';
    this.admissionService.generateTransactionId(applicantDateils).subscribe(
      (transactionResponse) => {
        console.log('Applicant transaction response ', transactionResponse);
      });
  }

  public async getActiveApplications() {
    await this.admissionService.getAllActiveApplicantApplications().subscribe(
      (activeAppliationsResponse) => {
        this.allActiveApplications = activeAppliationsResponse;
        console.log('All active applications ', activeAppliationsResponse);
      },
      (error) => {
        console.log('An error occurred ', error);
      },
      () => {
        // this.getAllProgrammes();
      }
    );
  }

  public printSlip(id, title) {
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['name'] = 'frame1';
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    frameDoc.document.write('</head><body>');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    setTimeout(function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      frame1.remove();
    }, 500);
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    if (ind) {
      $('#iframe').remove();
    }
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

  public payApplicationFee() {
    this.load.message.create = 'Making ';
    this.feeDescription = 'Application Fee';
    this.load.requesting.create = true;
    this.customPayForm.value['amount'] = parseInt(this.applicationAmount, 10);

    this.admissionService.transactApplicantPayment(this.customPayForm.value, this.activeApplicant['id']).subscribe(
      (paymentResponse) => {
        this.load.message.create = 'Make ';
        this.load.requesting.create = false;
        // this.Alert.success('Payment Successful');
        this.processPaymentByMethod(this.customPayForm.value['payment_method'], paymentResponse);


        console.log('Your Payment Response', paymentResponse);
      },
      (error) => {
        this.load.message.create = 'Make ';
        // this.emitPaymentService.emitChange('true');
        this.load.requesting.create = false;
        this.Alert.error('An error occurred. Your payment failed', error);
      }
    );
  }

  public payAcceptanceFee() {
    this.load.message.create = 'Making ';
    this.feeDescription = 'Acceptance Fee';
    this.load.requesting.create = true;
    const data = this.acceptanceFeeForm.value;
    data['amount'] = this.acceptanceFeeAmount;
    this.admissionService.createAcceptanceFee(this.activeApplicant['id'], data).subscribe(
      (acceptanceFeeResponse) => {
        this.load.message.create = 'Make ';
        this.load.requesting.create = false;
        this.processPaymentByMethod(this.acceptanceFeeForm.value['payment_method'], acceptanceFeeResponse);
        // this.Alert.success('Acceptance fee has been paid succesfully');
        this.triggerModalOrOverlay('close', 'acceptanceFeePayment');
      },
      (error) => {
        this.Alert.error('An error occurred. Could not pay fees', error);
      }
    );
  }


  public paystackPaymentCancel() {
    this.triggerModalOrOverlay('close', 'createPayment');
  }

  public paystackPaymentDone(event) {
    console.log('Payment event ', event);
    this.triggerModalOrOverlay('close', 'createPayment');
    this.router.navigate(['/pay-transact', event]);
  }

  public processPaymentByMethod(payment_method, data) {

    Cache.set('RETURN_URL', window.location.href);
    const relevantDetails = {
      total_amount: this.activeApplicant['application']['amount'],
      form_no: this.activeApplicant['applicant_profile']['form_no'],
      name: this.activeApplicant['name']
    }
    Cache.set('paymentDetails', relevantDetails);
    switch (payment_method.toLowerCase()) {
      case 'paystack':
        this.paystackRef = data['response'].transaction_id;
        console.log('paystack data response', data['response']);
        // payButton.attr('href', data['response'].authorization_url);
        const payButton = $('#paystack');
        setTimeout(() => {
          payButton[0].click();
        }, 10);
        break;
      case 'custom':
        this.emitPaymentService.emitChange('true');
        this.activeApplicant.acceptance_fee_status = '1';
        this.activeApplicant['payment_status'] = '1';
        this.triggerModalOrOverlay('close', 'createPayment');
        setTimeout(() => {
          this.router.navigateByUrl('/applicant/form');
        }, 2000);
        break;
      case 'paychoice':
        this.triggerModalOrOverlay('close', 'createPayment');
        const transactionObject = {
          tid: data['transaction_id'],
          tcost: data['transaction_cost'],
          url: data['url']
        };
        // console.log('Paychoicd transactionobject ',transactionObject)
        const encryptedTransaction = CryptoJS['AES'].encrypt(JSON.stringify(transactionObject), SECRET_PAYMENT_KEY);
        this.router.navigate(['/pay-transact', {paychoice: encryptedTransaction}]);
        break;
      case 'ebillspay':
        this.triggerModalOrOverlay('close', 'createPayment');
        this.eBillsPay.id = data.response;
        this.eBillsPay.amount = data.items.transaction_fee;
        break;
      case 'etransact':
        this.etransactObject = data;
        setTimeout(() => {
          this.el.nativeElement.querySelector('form#etransact_submit').submit();
        }, 10)
        break;
      default:
        this.Alert.warning('No Payment Transacted');
    }

  }

  public finishPaymentFeedBack() {
    this.Alert.success('Your payment has been made successfully, procced to fill your application form');
    this.activeApplicant['payment_status'] = '1';
    this.triggerModalOrOverlay('close', 'createPayment');
    setTimeout(() => {
      this.router.navigateByUrl('/applicant/form');
    }, 2000);
  }

  /**
   * Serere Jegede reporting
   * */

  public makeReQuery(transaction_id, index, etranzact?) {
    // const url = this.reQuery.url + transaction_id;
    this.activeApplicant['applicant_transactions'][index]['loading'] = true;
    this.admissionService.makeReQuery(transaction_id, etranzact).subscribe(res => {
      this.activeApplicant['applicant_transactions'][index]['loading'] = false;
      if (res.message) {
        this.Alert.info(res.message);
        this.getApplicantProfile(this.applicantId);
      } else {
        this.Alert.warning('Please retry');
      }
    }, err => {
      this.Alert.error('Could not make Requery', err);
      this.activeApplicant['applicant_transactions'][index]['loading'] = false;
    });
  }
}
