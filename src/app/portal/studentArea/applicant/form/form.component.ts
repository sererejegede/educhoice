import {Component, ElementRef, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, AbstractControl, FormArray, FormControl} from '@angular/forms';
import {UserService} from '../../../../../services/user.service';
import {AdmissionsService} from '../../../../../services/api-service/admissions.service';
import {NotificationService} from '../../../../../services/notification.service';
import {EmitPaymentService} from '../../../../../services/emit-payment-service';
import {Cache} from '../../../../../utils/cache';
import {parents, courseChoices} from '../../../../../interfaces/parents.interface';
import {SchoolService} from "../../../../../services/school.service";
import {forEach} from "@angular/router/src/utils/collection";


declare const $: any;

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public formFile;
  public filename = '';
  public tabId = '';
  public authenticatedUser: any = {};
  public activeApplicant: any;
  public acceptanceFeeAmount: any;
  public updateMessage = 'Submit';
  public viewMessage = 'Preview';
  public printing = false;
  public frame1: any = null;
  public ack_card = false;
  public printMedia = false;
  public returningApplicant = false;
  public totalUTMEScore = 0;

  public loading = false;
  public submitLoading = false;
  public appSettings;
  public loadPhoto = false;
  public showSubjectDiv = false;
  public applicantId = 0;
  public countResults = 0;
  public addResultButton = false;
  public finishResultButton = false;
  public applicationFormPreview = false;
  public applicationMode = false;
  public previewloading = false;
  public jambTotalIsValid = true;
  public applicationFormView: any;
  public compoundData: any;
  public facultyName;
  public facultyNames;
  public formUpdateForm: FormGroup;
  public updateSubjectForm: FormGroup;
  public olevelSubjectsArray: FormGroup;
  public olevelResultsArray: FormGroup;
  public applicantFormTabTwo: any = FormGroup;
  public applicantFormTabTwoSittings: any = FormGroup;

  public olevelResultsVersionTwo: any = FormGroup;
  public olevelSubjectsVersionTwo: any = FormGroup;
  public jambResultVersionTwo: any = FormGroup;
  public otherQualificationsForm: any = FormGroup;

  public applicantResultsVersionTwo = {
    result: null,
    subjects: []
  };
  public holdAllVersionTwoResults = [];

  public applicantFormTabOne: any = FormGroup;
  public allSubjects: any[] = [];
  public allFirstFaculties: any[] = [];
  public allSecondFaculties: any[] = [];
  public allDepartments: any[] = [];
  public allCourses: any[] = [];


  public old_version_active = null;
  public showResultForm: boolean = false;
  public showAddSubject: boolean = false;
  public subjectsHaveBeenEdited: boolean = false;
  public disableJambResultInput: boolean = false;
  public show_other_subjects: boolean = false;
  public showOthers: boolean = false;

  public firstChoice = {
    deparment_id: '',

  };
  public secondChoice = {
    deparment_id: '',
  };
  public allFirstDepartments: any[] = [];
  public allSecondDepartments: any[] = [];
  public allFirstCourses: any[] = [];
  public allScreeningCentres: any[] = [];
  public allSecondCourses: any[] = [];
  public yearsArray: any[] = [];
  public holdAllOlevelResults: any[] = [];
  public holdAllOtherQualifications: any[] = [];
  public allCountries: any[] = [];
  public allStates: any[] = [];
  public lgas: any[] = [];
  public allLevelResults: any[] = [];
  public selectedSubjects: any[] = [];
  public jambResult: any[] = [];
  public scores: any[] = [];

  public activeResultIndex = -1;
  public activeSitting = 0;
  public jambSubjectCount = 1;
  public englishScore: number = 0;


  public formInputIds = {
    firstfacultyId: 0,
    secondfacultyId: 0,

    firstdepartmentId: 0,
    seconddepartmentId: 0,

    firstcourseId: 0,
    secondcourseId: 0

  };
  public addButton = {
    oneSitting: false,
    twoSittings: false

  };
  public load = {
    other_applicants: {
      loading: false,
      creating: false
    },
    departments: {
      loading: false,
      list: false
    },
    courses: {
      loading: false,
      list: false
    },
    states: {
      loading: false,
      list: false
    },
    lgas: {
      loading: false,
      list: false
    }
  };


  constructor(private fb: FormBuilder,
              private userService: UserService,
              private el: ElementRef,
              private admissionService: AdmissionsService,
              private Alert: NotificationService,
              private emitPaymentService: EmitPaymentService,
              private schoolService: SchoolService) {
    this.applicantId = this.userService.getAuthUser()['login']['user']['id'];
    this.appSettings = Cache.get('app_settings');
    this.getAllApplicantFaculties();
    this.formUpdateForm = this.fb.group(FormComponent.formUpdateForm());
    this.updateSubjectForm = this.fb.group(FormComponent.updateSubjectForm());
    this.applicantFormTabTwoSittings = this.fb.group(FormComponent.applicantFormTabTwoSittings());
    this.applicantFormTabOne = this.fb.group(FormComponent.applicantFormTabOne());

    this.olevelResultsVersionTwo = this.fb.group(FormComponent.olevelResultsVersionTwo());
    this.olevelSubjectsVersionTwo = this.fb.group(FormComponent.olevelSubjectsVersionTwo());
    this.jambResultVersionTwo = this.fb.group(FormComponent.jambResultVersionTwo());
    this.otherQualificationsForm = this.fb.group(FormComponent.otherQualificationsForm());

    this.olevelResultsArray = this.fb.group(this.olevelResultsArrayForm());
    this.olevelSubjectsArray = this.fb.group(this.olevelSubjectsArrayForm());
    this.applicantFormTabTwo = this.fb.group(FormComponent.applicantFormTabTwo());
  }


  static formUpdateForm = function () {
    return {
      kin_phone: ['', Validators.compose([Validators.required])],
      phone: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      // name: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      kin_address: ['', Validators.compose([Validators.required])],
      next_of_kin: ['', Validators.compose([Validators.required])],
    };
  };
  static updateSubjectForm = function () {
    return {
      subj_name: ['', Validators.compose([Validators.required])],
      subj_grade: ['', Validators.compose([Validators.required])]
    };
  };
  static jambResultVersionTwo = function () {
    return {
      subj_score: ['', Validators.compose([Validators.required])],
      subj: ['', Validators.compose([Validators.required])]
    };
  };
  static otherQualificationsForm = function () {
    return {
      name: ['', Validators.compose([Validators.required])],
      address_of_institution: ['', Validators.compose([Validators.required])],
      from: ['', Validators.compose([Validators.required])],
      to: ['', Validators.compose([Validators.required])],
      qualification_date: ['', Validators.compose([Validators.required])],
      qualification: ['', Validators.compose([Validators.required])],
      qualification_grade: ['', Validators.compose([Validators.required])],
    };
  };
  static applicantFormTabOne = function () {
    return {
      parents_title: ['', Validators.compose([Validators.required])],
      parent_other_title: [''],
      parent_name: ['', Validators.compose([Validators.required])],
      parent_relationship: ['', Validators.compose([Validators.required])],
      parent_tel: ['', Validators.compose([Validators.required])],
      parent_email: ['', Validators.compose([Validators.required])],
      parent_address: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])],
      applicant_landtel: '',
      applicant_homeaddress: ['', Validators.compose([Validators.required])],
      marital_status: ['', Validators.compose([Validators.required])],
      birthdate: ['', Validators.compose([Validators.required])],
      place_of_birth: ['', Validators.compose([Validators.required])],
      home_town: ['', Validators.compose([Validators.required])],
      religion: [''],
      denomination: [''],
      other_denomination: [''],
      preferred_screening_centre: ['', Validators.compose([Validators.required])],
      first_faculty_id: ['', Validators.compose([Validators.required])],
      first_department_id: ['', Validators.compose([Validators.required])],
      second_faculty_id: ['', Validators.compose([Validators.required])],
      second_department_id: ['', Validators.compose([Validators.required])],
      first_course_id: ['', Validators.compose([Validators.required])],
      second_course_id: ['', Validators.compose([Validators.required])],
      state_origin: ['', Validators.required],
      country_id: [''],
      lga: ['', Validators.required],
      age: ['', Validators.required],
      phone: ['', Validators.required],
      address: ['', Validators.required],
      next_of_kin: ['', Validators.required],
      kin_phone: ['', Validators.required],
      kin_address: ['', Validators.required],
      faculty_id: ['', Validators.compose([Validators.required])],
      department_id: ['', Validators.compose([Validators.required])],
      course_of_study_id: ['', Validators.compose([Validators.required])],
      referrer_channel: ['', Validators.compose([Validators.required])],
      referrer_name: ['']
    };
  };
  static applicantFormTabTwo = function () {
    return {
      subj1: ['', Validators.compose([Validators.required])],
      subjgrade1: ['', Validators.compose([Validators.required])],
      subj2: ['', Validators.compose([Validators.required])],
      subjgrade2: ['', Validators.compose([Validators.required])],
      subj3: ['', Validators.compose([Validators.required])],
      subjgrade3: ['', Validators.compose([Validators.required])],
      subj4: ['', Validators.compose([Validators.required])],
      subjgrade4: ['', Validators.compose([Validators.required])],
      subj5: ['', Validators.compose([Validators.required])],
      subjgrade5: ['', Validators.compose([Validators.required])],
      subj6: ['', Validators.compose([Validators.required])],
      subjgrade6: ['', Validators.compose([Validators.required])],
      subj7: ['', Validators.compose([Validators.required])],
      subjgrade7: ['', Validators.compose([Validators.required])],
      subj8: ['', Validators.compose([Validators.required])],
      subjgrade8: ['', Validators.compose([Validators.required])],
      subj9: ['', Validators.compose([Validators.required])],
      subjgrade9: ['', Validators.compose([Validators.required])],

    };
  };

  private olevelResultsArrayForm() {
    return {
      results_array: this.fb.array([this.olevelResults()])
    };
  }

  private olevelSubjectsArrayForm() {
    return {
      subjects_array: this.fb.array([this.olevelSubjects()])
    };
  }


  public olevelResults() {
    return this.fb.group({
      exam_name: ['', Validators.compose([Validators.required])],
      exam_year: ['', Validators.compose([Validators.required])],
      exam_number: ['', Validators.compose([Validators.required])]
    });
  }

  static olevelResultsVersionTwo() {
    return {
      exam_name: ['', Validators.compose([Validators.required])],
      exam_year: ['', Validators.compose([Validators.required])],
      exam_number: ['', Validators.compose([Validators.required])]
    };
  }

  static choiceOfStudyForm() {
    return {
      first_choice: ['', Validators.compose([Validators.required])],
      first_choice_course_of_study: ['', Validators.compose([Validators.required])],
      second_choice: ['', Validators.compose([Validators.required])],
      second_choice_course_of_study: ['', Validators.compose([Validators.required])]

    };
  }


  public olevelSubjects() {
    return this.fb.group({
      subj: ['', Validators.compose([Validators.required])],
      subj_grade: ['', Validators.compose([Validators.required])]
    });
  }


  static olevelSubjectsVersionTwo() {
    return {
      subj: ['', Validators.compose([Validators.required])],
      subj_grade: ['', Validators.compose([Validators.required])]
    };
  }


  static applicantFormTabTwoSittings = function () {
    return {
      first_subj1: ['', Validators.compose([Validators.required])],
      first_subjgrade1: ['', Validators.compose([Validators.required])],
      first_subj2: ['', Validators.compose([Validators.required])],
      first_subjgrade2: ['', Validators.compose([Validators.required])],
      first_subj3: ['', Validators.compose([Validators.required])],
      first_subjgrade3: ['', Validators.compose([Validators.required])],
      first_subj4: ['', Validators.compose([Validators.required])],
      first_subjgrade4: ['', Validators.compose([Validators.required])],
      first_subj5: ['', Validators.compose([Validators.required])],
      first_subjgrade5: ['', Validators.compose([Validators.required])],
      first_subj6: ['', Validators.compose([Validators.required])],
      first_subjgrade6: ['', Validators.compose([Validators.required])],
      first_subj7: ['', Validators.compose([Validators.required])],
      first_subjgrade7: ['', Validators.compose([Validators.required])],
      first_subj8: ['', Validators.compose([Validators.required])],
      first_subjgrade8: ['', Validators.compose([Validators.required])],
      first_subj9: ['', Validators.compose([Validators.required])],
      first_subjgrade9: ['', Validators.compose([Validators.required])],

      second_subj1: ['', Validators.compose([Validators.required])],
      second_subjgrade1: ['', Validators.compose([Validators.required])],
      second_subj2: ['', Validators.compose([Validators.required])],
      second_subjgrade2: ['', Validators.compose([Validators.required])],
      second_subj3: ['', Validators.compose([Validators.required])],
      second_subjgrade3: ['', Validators.compose([Validators.required])],
      second_subj4: ['', Validators.compose([Validators.required])],
      second_subjgrade4: ['', Validators.compose([Validators.required])],
      second_subj5: ['', Validators.compose([Validators.required])],
      second_subjgrade5: ['', Validators.compose([Validators.required])],
      second_subj6: ['', Validators.compose([Validators.required])],
      second_subjgrade6: ['', Validators.compose([Validators.required])],
      second_subj7: ['', Validators.compose([Validators.required])],
      second_subjgrade7: ['', Validators.compose([Validators.required])],
      second_subj8: ['', Validators.compose([Validators.required])],
      second_subjgrade8: ['', Validators.compose([Validators.required])],
      second_subj9: ['', Validators.compose([Validators.required])],
      second_subjgrade9: ['', Validators.compose([Validators.required])],

    };
  };

  // static choicesForm = function() {
  //
  //     return {
  //         first_choice_dept: ['', Validators.compose([Validators.required])],
  //         second_subjgrade9: ['', Validators.compose([Validators.required])],
  //         second_subjgrade9: ['', Validators.compose([Validators.required])],
  //         second_subjgrade9: ['', Validators.compose([Validators.required])],
  //         second_subjgrade9: ['', Validators.compose([Validators.required])],
  //         second_subjgrade9: ['', Validators.compose([Validators.required])],
  //         second_subjgrade9: ['', Validators.compose([Validators.required])],
  //
  //     }
  // }

  ngOnInit() {

    console.log('Active applicant on application form', Cache.get('applicant_profile'));
// alert("Total UME Score "+this.activeApplicant['total_ume_score']);
    this.getYears();
    this.getAllOlevelSubjects();
    this.getAllApplicantScreeningCentres();
    this.getAllCountries();
    this.getAllApplicantFaculties();
    this.facultyName = Cache.get('app_settings')['settings']['config_faculty'];
    this.facultyNames = Cache.get('app_settings')['settings']['config_faculties'];
    // this.formUpdateForm.patchValue(this.activeApplicant);
    // this.getApplicantProfile();

  }


  public updateApplicant() {
    this.updateMessage = 'Submitting';
    this.loading = true;
    this.admissionService.updateApplicantInfo(this.activeApplicant['reg_no'], this.formUpdateForm.value).subscribe((updateResponse) => {
        this.updateMessage = 'Submit';
        this.loading = false;
        this.Alert.success('Your profile changes have been updated');
      },
      (error) => {
        this.updateMessage = 'Submit';
        this.loading = false;
        this.Alert.error('An error occurred, could not update your profile', error);
      }
    );
  }


  public getPhoto(e, preview) {
    this.formFile = e.target;
    this.filename = this.formFile.files[0].name;
    // return console.log('filesize', this.formFile.files[0].size);
    if (this.formFile.files[0].size > 500 * 1024) {
      return this.Alert.error('Maximum allowed image size is 500kb');
    }
    if (this.validateImage('studentPhotoOther', preview)) {
      this.previewImage(preview);
    }
  }

  public toggleSelection() {
    this.loadPhoto = !this.loadPhoto;
  }

  private validateImage(img_input, img_tag): boolean {
    const profilePhotoId = $(`#${img_tag}`);
    const $linkId = $(`#${img_input}`);
    const $newLinkId = $linkId.val();
    const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
    $linkId.removeClass('alert alert-danger animated rubberBand');
    if (file_type && (file_type !== '.jpg') && (file_type !== '.png') && (file_type !== '.jpeg')) {
      $linkId.addClass('alert alert-danger animated rubberBand');
      this.Alert.error('You can only upload jpg, jpeg or png files');
      return false;
    }
    if (!this.formFile) {
      this.Alert.error('You must provide a photo');
      this.scrollIntoView('#profile_photo');
      profilePhotoId.addClass('alert alert-danger animated rubberBand');
      setTimeout(() => {
        profilePhotoId.removeClass('alert alert-danger animated rubberBand');
      }, 2000);
      return false;
    }
    return true;
  }

  private previewImage(preview) {
    if (this.formFile.files && this.formFile.files[0]) {
      const reader = new FileReader();
      reader.onload = (e) => {
        $(`#${preview}`).attr('src', e.target['result']);
      };
      reader.readAsDataURL(this.formFile.files[0]);
    }
    this.toggleSelection();
  }

  public previewApplicationForm(data, action?) {
    console.log('Returning form data ', data);
    this.applicationFormPreview = (this.activeApplicant['applicant_profile']['submit'].toString() === '1' && action == 'returning');
    if (action == 'preview') {
      data['preferred_screening_centre'] = (data['preferred_screening_centre']['id']) ?
        this.selectWhereId(this.allScreeningCentres, 'id', data['preferred_screening_centre'])['name'] : data['preferred_screening_centre'];

      data['olevel'].forEach((datum) => {
        const olevelSubjects: any[] = datum['subjects'];
        olevelSubjects.forEach((olevelsubject) => {
          olevelsubject['subj'] = this.selectWhereId(this.allSubjects, 'id', olevelsubject['subj']);
        });
      });
      this.applicationFormPreview = true;
    }
    this.compoundData = data;

    this.applicationFormView = data;
    // alert("Form Subj 2 score " + this.applicationFormView['subj2_score']);
    // this.setApplicationFormPreviewObject(this.compoundData);


  }

  public saveApplicationFormPreview() {
    this.previewloading = true;
    this.admissionService.saveApplicationForm(this.compoundData, this.activeApplicant['id'], true).subscribe(
      (applicationResponse) => {
        this.previewloading = false;
        this.loading = false;
        this.Alert.success('Your application has been successfully saved');
        this.emitPaymentService.emitChange('form_submitted');
        console.log('Response after application ', applicationResponse);
      },
      (error) => {
        this.previewloading = false;
        this.loading = false;
        this.Alert.error('An error occurred, could not save your application ', error);
      }
    );
  }

  private prepareApplication(applicationMode: string) {


    this.applicationMode = (applicationMode === 'submit');
    const choices = {};


    const applicantParent = [];
    let compoundData = {
      personal_details: null,
      academic_records: null
    };
    // const olevelResults = this.holdAllOlevelResults;
    const olevelResults = this.holdAllVersionTwoResults;
    const personalDetails = this.applicantFormTabOne.value;
    const jambResult = this.expandJambResult(this.jambResult);
    const parentDetails = {
      address: personalDetails['parent_address'],
      email: personalDetails['parent_email'],
      surname: personalDetails['parent_name'].split(' ')[0] || 'None',
      name: personalDetails['parent_name'].split(' ')[0] || 'None',
      title: (personalDetails['parents_title']) ? personalDetails['parents_title'] : 'None',
      relationship: personalDetails['parent_relationship'] || 'None',
      alt_title: personalDetails['parent_other_title'] || 'None',
      tel: personalDetails['parent_tel'] || 'None'
    };
    applicantParent.push(parentDetails);
    personalDetails['nationality'] = 'Nigerian';
    personalDetails['religion'] = 'Christianity';
    personalDetails['denomination'] = (this.showOthers) ? personalDetails['other_denomination'] : personalDetails['denomination'];
    choices['first_choice'] = compoundData['academic_records'] = {olevel: olevelResults};
    compoundData['parents'] = {parents: applicantParent};
    compoundData['other_qualifications'] = {other_qualifications: this.holdAllOtherQualifications};
    compoundData['personal_details'] = personalDetails;
    const choiceIds = [this.applicantFormTabOne.value.first_course_id, this.applicantFormTabOne.value.second_course_id];
    compoundData['course_of_choice'] = {course_of_study_choice: this.setCourseOfStudy(choiceIds)};
    compoundData = Object.assign(compoundData['academic_records'], compoundData['personal_details'], compoundData['course_of_choice'], compoundData['parents'], compoundData['other_qualifications'], jambResult);
    return compoundData;
  }

  public saveApplicationForm(applicationMode: string, action?: string) {
    const compoundData = this.prepareApplication(applicationMode);
    if (action && action === 'preview') {
      this.previewApplicationForm(compoundData, 'preview');
      return this.triggerModalOrOverlay('close', 'verifySubmission');
    }
    if (!this.jambTotalIsValid && this.activeApplicant['total_ume_score']) {
      return this.Alert.error(`Your total in Jamb must be equal to: ${this.activeApplicant['total_ume_score']}`);
    }
    this.submitLoading = true;
    this.triggerModalOrOverlay('close', 'verifySubmission');
    // console.log('Compound data all jampacked details ', compoundData);
    this.admissionService.saveApplicationForm(compoundData, this.activeApplicant['id'], this.applicationMode).subscribe(
      (applicationResponse) => {
        this.admissionService.saveApplicationForm2(this.activeApplicant['id'], this.formFile).subscribe(res => {
          console.log('image upload', res);
          this.activeApplicant.applicant_profile.submit = 1
          // this.previewApplicationForm(compoundData, 'preview');
          window.location.reload(true);
        }, err => {
          this.Alert.error('It seems network connection is down', err);
          this.submitLoading = false;
          return console.log('Image upload after data submission', err);
        });
        this.submitLoading = false;
        this.applicationFormPreview = this.applicationMode;
        this.Alert.success('Your application has been successfully saved');
        this.emitPaymentService.emitChange('form_submitted');
        console.log('Response after application ', applicationResponse);
      },
      (error) => {
        this.submitLoading = false;

        this.Alert.error('An error occurred, could not save your application ', error);
      }
    );
  }


  public editApplication() {
    this.applicationFormPreview = false;
  }

  public addNewOlevelResult() {
    this.addResultButton = false;
    const control = <FormArray>this.olevelResultsArray.controls['results_array'];
    control.push(this.olevelResults());

  }

  public addExamVersionTwo() {
    console.log("Exam Called!")
    const resultObject = {result: null, subjects: []};
    resultObject.result = JSON.parse(JSON.stringify(this.olevelResultsVersionTwo.value));
    this.activeResultIndex += 1;
    if (this.subjectsHaveBeenEdited) {
      this.activeResultIndex = this.holdAllVersionTwoResults.length;
      this.subjectsHaveBeenEdited = false;
    }
    this.holdAllVersionTwoResults.push(resultObject);
    this.showAddSubject = true;
    this.olevelResultsVersionTwo.reset();

  }

  public addSubjectsVersionTwo() {
    if (this.holdAllVersionTwoResults[this.activeResultIndex]['subjects'].length < 9) {
      const subjects = JSON.parse(JSON.stringify(this.olevelSubjectsVersionTwo.value));
      this.holdAllVersionTwoResults[this.activeResultIndex]['subjects'].push(subjects);
      // } else {
      //     this.holdAllVersionTwoResults[this.activeResultIndex]['subjects'] = subjects;
      // }
      // this.showAddSubject = false;
      this.olevelSubjectsVersionTwo.reset();
    } else {
      this.Alert.warning('Maximum in a sitting is 9');
    }
  }

  public removeSubjectVersionTwo(examIndex, subjectIndex) {
    this.subjectsHaveBeenEdited = true;
    this.activeResultIndex = examIndex;
    this.activeSitting = examIndex + 1;
    this.holdAllVersionTwoResults[examIndex]['subjects'].splice(subjectIndex, 1);
  }

  public setVisibleForms() {
    this.showAddSubject = false;
    this.showResultForm = true;
    this.activeSitting += 1;
  }


  public checkOlevelChoice(subjValue, olevel = true) {
    if (olevel) {
      const subjectHasBeenChosen = this.holdAllVersionTwoResults[this.activeResultIndex]['subjects'].filter((subject) => {
        return subject['subj'].toString() === subjValue.toString()
      })[0];

      if (subjectHasBeenChosen) {
        this.olevelSubjectsVersionTwo.reset();
        return this.Alert.error(`${this.getSubjectNameById(subjValue)['name']} has been chosen for Exam ${this.activeSitting}. Please select another subject.`);
      }
      return;
    }
    const subjectHasBeenChosen = this.jambResult.filter((subject) => {
      return subject['subj_id'].toString() === subjValue.toString()
    })[0];

    if (subjectHasBeenChosen) {
      this.jambResultVersionTwo.reset();
      return this.Alert.error(`${this.getSubjectNameById(subjValue)['name']} has been chosen. Please select another subject.`);
    }


  }

  public addJambResultVersionTwo(newEntry = true, nameValueObj?) {
    let jambResultObj = {};
    if (nameValueObj) {
      const key_index = nameValueObj['i'];
      const val_index = `subj${key_index}`;
      const score_index = `subj${key_index}_score`;
      jambResultObj['subj'] = nameValueObj[val_index];
      jambResultObj['score'] = nameValueObj[score_index];
    }
    const subj = (newEntry) ? this.jambResultVersionTwo.value['subj'] : jambResultObj['subj'];
    const score = (newEntry) ? this.jambResultVersionTwo.value['subj_score'] : jambResultObj['score'];
    if (score > 100) {
      return this.Alert.error('You can not score greater than 100 in a subject')
    }
    let  suprefix = this.jambSubjectCount + 1;
    // const suprefix = (this.returningApplicant) ? this.jambSubjectCount + 1 : this.jambSubjectCount;
    const subj_key = `subj${suprefix}`;

    const subj_score = `subj${suprefix}_score`;
    const resultObject = {};
    resultObject[subj_key] = subj;
    resultObject[subj_score] = score;
    resultObject['subj_id'] = subj;
    if (this.jambSubjectCount < 4) {
      console.info(score);
      this.totalUTMEScore += +score;
      this.jambSubjectCount += 1;
      this.jambResult.push(resultObject);
      this.scores.push({name: subj, res_score: score});
    }
    if (this.jambSubjectCount == 4) {
      this.jambResultVersionTwo.disable();
      this.disableJambResultInput = true;
    }
    this.jambResultVersionTwo.reset();
  }

  public addQualification() {
    const qualificationObject = this.otherQualificationsForm.value;
    qualificationObject['period_of_attendance'] = `${this.otherQualificationsForm.value['from']} - ${this.otherQualificationsForm.value['to']}`
    this.holdAllOtherQualifications.push(qualificationObject);
    this.otherQualificationsForm.reset();
  }

  public removeJambSubject(i, score?) {
    this.jambResult.splice(i, 1);
    this.scores.splice(i, 1);
    this.jambSubjectCount -= 1;
    this.jambResultVersionTwo.enable();
    this.disableJambResultInput = false;
    if (score) {
      this.totalUTMEScore -= score;
    }
  }

  public removeQualification(j) {
    this.holdAllOtherQualifications.splice(j, 1);
  }


  public removeOlevelExam(i) {
    this.holdAllVersionTwoResults.splice(i, 1);
    this.showAddSubject = false;
  }

  public addSubjectToOlevelExam(i) {
    // this.holdAllVersionTwoResults[i]['subjects']
    this.activeResultIndex = i;
    this.showAddSubject = true;

  }

  public setCardClass(i) {
    let className = 'container card';
    if (this.activeResultIndex == i) {
      className += ' shadow_div';
    }

    return className;
  }

  public saveEnglishScore(value) {
    if (+value > 100) {
      return this.Alert.error('You can not get more than 100 in a subject!');
    }
    this.englishScore = value;
    this.totalUTMEScore += +value;
    this.show_other_subjects = true;
  }

  public expandJambResult(resultArray: any[]): any {
    const resultObject = {};
    resultArray.forEach((result) => {
      for (let key in result) {
        resultObject[key] = result[key]
      }
    });
    resultObject['eng_score'] = this.englishScore;
    resultObject['total_ume_score'] = this.activeApplicant['total_ume_score'] || null;
    this.checkJambTotal(resultObject);
    return resultObject;
  }

  public checkDenomination(denomination: string): void {
    this.showOthers = (denomination.toLowerCase() == 'others');
  }

  public checkJambTotal(resultObject): void {
    let jambTotal = 0;
    for (let key in resultObject) {
      if (key.endsWith('score') && key !== 'total_ume_score') {
        jambTotal += +resultObject[key];
      }
    }
    this.jambTotalIsValid = (jambTotal == +this.activeApplicant['total_ume_score']);
  }

  // public getResultScore(i){
  //     const subj_score = `subj${i+1}_score`
  //     this.scores.push(this.jambResult[i][subj_score]);
  // }

  public addNewOlevelSubject(ind) {
    this.finishResultButton = true;
    if (this.countResults == 0) {
      this.countResults += 1;
      this.showSubjectDiv = true;
      return;
    }
    // const key = `newindex${ind}`;
    // this.resultCaptions[key] = this.olevelResultsArray.value['results_array'][ind]['exam_name'];
    const control = <FormArray>this.olevelSubjectsArray.controls['subjects_array'];
    control.push(this.olevelSubjects());

  }


  public populatePreviouslyFilledOlevelSubjects(data) {
    this.deleteResult(0);
    this.deleteSubject(0);
    this.showSubjectDiv = true;
    const resultsControl = <FormArray>this.olevelResultsArray.controls['results_array'];
    const subjectsControl = <FormArray>this.olevelSubjectsArray.controls['subjects_array'];

    if (data) {
      data.forEach((datum) => {

        resultsControl.push(this.fb.group(datum['result']));
        datum['subjects'].forEach((datumSubject) => {
          subjectsControl.push(this.fb.group(datumSubject));

        });
      });
    }


  }

  public deleteSubject(ind: number) {
    const control = <FormArray>this.olevelSubjectsArray.controls['subjects_array'];
    control.removeAt(ind);
  }

  public deleteResult(ind) {
    const control = <FormArray>this.olevelResultsArray.controls['results_array'];
    control.removeAt(ind);
    this.addResultButton = true;
  }

  public activateAddButton(value) {
    const newValue = parseInt(value);
    if (newValue == 1) {
      this.addButton.twoSittings = false;
      this.addButton.oneSitting = true;
    } else if (newValue == 2) {
      this.addButton.twoSittings = true;
      this.addButton.oneSitting = false;
    }
  }

  public getApplicantProfile(id) {
    this.admissionService.getApplicantProfile(id).subscribe(
      (applicantProfileResponse) => {
        this.activeApplicant = applicantProfileResponse['applicant'];
        if (this.activeApplicant.country_id) {
          this.getStateByCountryId(this.activeApplicant.country_id);
        }
        this.authenticatedUser = this.activeApplicant;

        if (this.authenticatedUser['application_status']) {
          if (this.authenticatedUser['application_status']['programme'] &&
            this.authenticatedUser['application_status']['programme']['acceptance_fee'] &&
            this.authenticatedUser['application_status']['programme']['acceptance_fee']['amount']) {
            this.acceptanceFeeAmount = this.authenticatedUser['application_status']['programme']['acceptance_fee']['amount'];
          }
        }
        const applicationFormPreviewObject = this.setApplicationFormPreviewObject(this.activeApplicant['applicant_profile']);
        this.holdAllOtherQualifications = this.activeApplicant['applicant_profile']['other_qualifications'] || [];
        this.holdAllVersionTwoResults = this.activeApplicant['applicant_profile']['olevel'] || [];

        this.patchJambSubjects();

        // console.log('Active applicant response from form page ', applicantProfileResponse);
        this.activeApplicant['programme_object'] = applicantProfileResponse['programme'];
        if (this.activeApplicant['payment_status'].toString() === '1') {
          this.emitPaymentService.emitChange('true');
        }
        if (this.activeApplicant['applicant_profile'] && (this.activeApplicant['applicant_profile']['submit'].toString() === '0')) {
          this.applicantFormTabOne.patchValue(applicationFormPreviewObject);
        } else if (this.activeApplicant['applicant_profile'] && (this.activeApplicant['applicant_profile']['application_form_status'].toString() === '1')) {
          let formPreviewData = this.activeApplicant['applicant_profile'];
          formPreviewData = Object.assign(formPreviewData, this.setParentDetails(formPreviewData['parents'][0]));

          this.previewApplicationForm(formPreviewData, 'returning');
        }

        // this.applicantFormTabTwoSittings.patchValue(applicationFormPreviewObject);


        console.log('Applicant profile response ', applicantProfileResponse);
      },
      (error) => {
        console.log('Could not load applicant profile response', error);
      }
    );
  }

  private patchJambSubjects() {

    if (this.activeApplicant['eng_score']) {
      this.returningApplicant = true;
      this.setVisibleForms();
      this.saveEnglishScore(this.activeApplicant['eng_score']);
      const resultObject = {
        'subj2': this.activeApplicant['subj2'],
        'subj3': this.activeApplicant['subj3'],
        'subj4': this.activeApplicant['subj4'],
        'subj2_score': this.activeApplicant['subj2_score'],
        'subj3_score': this.activeApplicant['subj3_score'],
        'subj4_score': this.activeApplicant['subj4_score']
      };
      let j = (this.returningApplicant) ? 1 : 0;
      for (let i = 0; i<3; i++) {
        resultObject['i'] = j + 1;
        this.addJambResultVersionTwo(false, resultObject);
        j++;
      }
    }

  }

  public removeEnglishScore() {
    this.totalUTMEScore -= this.englishScore;
    this.englishScore = null;
    this.show_other_subjects = !this.show_other_subjects;
  }


  private patchOlevelResults(olevelResults) {
    // olevelResults.forEach(result => {
    //     // this.addExamVersionTwo(result['result']);
    //     // this.addSubjectsVersionTwo(result['subjects']);
    // })
  }

  public getAllApplicantScreeningCentres() {
    this.admissionService.getAllApplicantScreeningCenters().subscribe(
      (screeningCentreResponse) => {
        this.allScreeningCentres = screeningCentreResponse;
        console.log('All applicant screening centers ', screeningCentreResponse);
      },
      (error) => {
        console.log('An error occurred loading screening centers ', error);
      }
    );

  }

  public getAllOlevelSubjects() {
    this.admissionService.getAllOlevelSubjects().subscribe(
      (subjectsResponse) => {
        this.allSubjects = subjectsResponse;
        console.log('All applicant subjects centers ', subjectsResponse);
      },
      (error) => {
        console.log('An error occurred loading subjects centers ', error);
      }
    );
  }

  public getAllApplicantFaculties() {
    const coursesOfStudy: any[] = [];
    const allDepartments: any[] = [];
    this.admissionService.getAllApllicantFaculties().subscribe(
      (facultiesResponse) => {
        const facultiesArrayObject = [];
        for (let key in facultiesResponse) {
          facultiesArrayObject.push(facultiesResponse[key]);
        }
        this.allFirstFaculties = this.allSecondFaculties = facultiesArrayObject;
        facultiesArrayObject.forEach((faculty) => {
          faculty['departments'].forEach((department) => {
            allDepartments.push(department);
            department['course_of_studies'].forEach((course_of_study) => {
              coursesOfStudy.push(course_of_study);
            });
          });
        });

        console.log('All Applicant Faculties ', facultiesResponse);
        this.allFirstDepartments = this.allSecondDepartments = allDepartments;
        this.allCourses = this.allFirstCourses = this.allSecondCourses = coursesOfStudy;
        this.getApplicantProfile(this.applicantId);
        console.log('All Applicant Courses ', coursesOfStudy);
        console.log('All Applicant departments ', this.allFirstDepartments);
      },
      (error) => {
        this.Alert.error('Could not load faculties ', error);
      }
    );
  }

  public getDepartmentByFacultyIndex(id, type: string) {
    if (!id) {
      return;
    }
    if (type == 'first') {
      const selectedFaculty = this.selectWhereId(this.allFirstFaculties, 'id', id);
      this.formInputIds.firstfacultyId = selectedFaculty.id;
      this.allFirstDepartments = selectedFaculty['departments'];
    } else {
      const selectedFaculty = this.selectWhereId(this.allSecondFaculties, 'id', id);
      this.formInputIds.secondfacultyId = selectedFaculty.id;
      this.allSecondDepartments = selectedFaculty['departments'];
    }

  }

  public getCourseByDepartmentIndex(id, type: string) {
    if (!id) {
      return;
    }
    if (type == 'first') {
      const selectedDepartment = this.selectWhereId(this.allFirstDepartments, 'id', id);
      this.formInputIds.firstcourseId = selectedDepartment.id;
      this.allFirstCourses = selectedDepartment['course_of_studies'];
    } else {
      const selectedDepartment = this.selectWhereId(this.allSecondDepartments, 'id', id);
      this.formInputIds.secondcourseId = selectedDepartment.id;
      this.allSecondCourses = selectedDepartment['course_of_studies'];


    }
  }

  public setParentDetails(applicantParents) {
    return {
      parents_title: applicantParents['title'],
      parent_other_title: applicantParents['alt_title'],
      parent_name: `${applicantParents['name']} ${applicantParents['surname']}`,
      parent_relationship: applicantParents['relationship'],
      parent_tel: applicantParents['tel'],
      parent_email: applicantParents['email'],
      parent_address: applicantParents['address']
    };
  }

  public setApplicationFormPreviewObject(data) {
    this.viewMessage = 'View';
    const applicantParents = (data['parents']) ? data['parents'][0] : parents;
    const applicantChoices = (data['course_of_study_choice']) ?
      data['course_of_study_choice'][0] : courseChoices;
    const firstChoice = (applicantChoices['first_choice']) ? applicantChoices['first_choice'] : this.firstChoice;
    const first_department_object = this.selectWhereId(this.allFirstDepartments, 'id', firstChoice['department_id']);
    let first_faculty_id = (first_department_object) ? first_department_object['faculty_id'] : null;
    first_faculty_id = (first_faculty_id) ? this.selectWhereId(this.allFirstFaculties, 'id', first_faculty_id)['id'] : null;

    const secondChoice = (applicantChoices['second_choice']) ? applicantChoices['second_choice'] : this.secondChoice;
    const second_department_object = this.selectWhereId(this.allSecondDepartments, 'id', secondChoice['department_id']);
    let second_faculty_id = (second_department_object) ? second_department_object['faculty_id'] : null;
    second_faculty_id = (second_faculty_id) ? this.selectWhereId(this.allFirstFaculties, 'id', second_faculty_id)['id'] : null;

    const applicationFormPreviewObject = {
      parents_title: applicantParents['title'],
      parent_other_title: applicantParents['alt_title'],
      parent_name: `${applicantParents['name']} ${applicantParents['surname']}`,
      parent_relationship: applicantParents['relationship'],
      parent_tel: applicantParents['tel'],
      parent_email: applicantParents['email'],
      parent_address: applicantParents['address'],
      applicant_landtel: data['applicant_landtel'],
      applicant_homeaddress: data['applicant_homeaddress'],
      marital_status: data['marital_status'],
      birthdate: data['birthdate'],
      place_of_birth: data['place_of_birth'],
      home_town: data['home_town'],
      religion: data['religion'],
      denomination: data['denomination'],
      preferred_screening_centre: data['preferred_screening_centre'],
      first_faculty_id: first_faculty_id,
      second_faculty_id: second_faculty_id,
      first_department_id: firstChoice['department_id'],
      second_department_id: secondChoice['department_id'],
      first_course_id: firstChoice['id'],
      second_course_id: secondChoice['id'],
      ...this.activeApplicant
    };
    this.populatePreviouslyFilledOlevelSubjects(data['olevel']);
    return applicationFormPreviewObject;
  }

  public saveApplicantTabTwo() {
    this.admissionService.createApplicantSSCEResult(this.applicantFormTabTwo.value, 1).subscribe(
      (resultSaveResponse) => {
        this.Alert.success('Successfully saved');
      },
      (error) => {
        this.Alert.error('An error occurred');
      }
    );
  }

  public saveApplicantTabOne() {

  }

  public moveToTabTwo(id) {

    // const tabButton = $(`#${id}`);
    // tabButton[0].click()
    const tabId = `#${id}`;
    $(tabId).addClass('active');
    const paneClass = `.tab-content #tab-`;
    const tabClass = `#tab`;
    const btnId = `#btn_`;
    const prevBtn = `${btnId}${this.tabId}`;
    const prevTab = `${tabClass}${this.tabId}`;
    const previousPane = `${paneClass}${this.tabId}`;
    this.tabId = id;
    const activePane = `${paneClass}${id}`;
    const activeBtn = `${btnId}${id}`;
    const activeTab = `${tabClass}${id}`;
    $(previousPane).removeClass('active');
    $(prevTab).removeClass('active');
    $(prevBtn).removeClass('btn-success');
    $(btnId).removeClass('btn-default');
    $(activeBtn).addClass('btn-success');
    $(activePane).addClass('active');
    $(activeTab).addClass('active');
    this.openOLevelForm();
  }

  public checkPhoneLength(id) {
    const landLine = `#${id}`;
    const landLength = $(landLine).val().length;
    if (landLength !== 11) {
      {
        $(landLine).addClass('alert alert-danger animated rubberBand');
        this.Alert.error('Phone Number should be 11 digits long');
        setTimeout(() => {
          $(landLine).removeClass('alert alert-danger animated rubberBand');
        }, 2000);
      }
    }
  }


  public removeSubjectFromArray(section: string, ind) {
    const elementId = `#${section}${ind}`;
    const elementValue = $(elementId).val();

    if (!this.selectedSubjects.includes(elementValue)) {
      this.selectedSubjects.push(elementValue);
    } else {
      $(elementId).val('');
      $(elementId).addClass('alert alert-danger animated rubberBand');
      this.Alert.error('You have already selected this subject');
      setTimeout(() => {
        $(elementId).removeClass('alert alert-danger animated rubberBand');
      }, 2000);
    }

    // alert(this.applicantFormTabTwo[subjectName].value);
    // const formSubject = this.allFormSubjects[ind];
    // const subjectIndex = this.allFormSubjects.indexOf(formSubject);
    // this.allSubjects.splice(subjectName,1);

  }

  public removeChoiceFromHostArray(ind) {
    const elementId = `#subject_${ind}`;
    const elementValue = $(elementId).val();
    const selectedSubject = this.selectWhereId(this.allSubjects, 'id', elementValue);
    if (!this.selectedSubjects.includes(selectedSubject)) {
      this.selectedSubjects.push(selectedSubject);
    } else {
      $(elementId).val('');
      $(elementId).addClass('alert alert-danger animated rubberBand');
      this.Alert.error('You have already selected this subject');
      setTimeout(() => {
        $(elementId).removeClass('alert alert-danger animated rubberBand');
      }, 2000);
    }


    // const selectedIndex = this.allSubjects.indexOf(selectedSubject);
    // this.allSubjects.splice(selectedIndex, 1);
  }

  public finishResult() {

    const resultObject = {
      result: this.olevelResultsArray.value['results_array'][0],
      subjects: this.olevelSubjectsArray.value['subjects_array']
    };

    this.holdAllOlevelResults.push(resultObject);
    this.clearInputedResults();
    this.resetButtonsAndSelectedSubjects();
    this.scrollIntoView();
    console.log('Hold all Olevel results ', this.holdAllOlevelResults);

  }

  public getSubjectNameById(subjectId) {
    const id = (subjectId) ? +subjectId : null;
    return this.allSubjects.filter(subject => {
      return subject['id'] == id;
    })[0]
  }

  public resetButtonsAndSelectedSubjects() {
    this.addResultButton = true;
    this.finishResultButton = false;
    this.selectedSubjects = [];
  }


  public clearInputedResults() {
    const resultControls = <FormArray>this.olevelResultsArray.controls['results_array'];
    const subjectControls = <FormArray>this.olevelSubjectsArray.controls['subjects_array'];
    while (resultControls.length !== 0) {
      resultControls.removeAt(0);
    }
    while (subjectControls.length !== 0) {
      subjectControls.removeAt(0);
    }

  }

  private scrollIntoView(id = '#showTop') {
    setTimeout(() => {
      this.el.nativeElement.querySelector(id).scrollIntoView({behavior: 'smooth'});
    }, 300);
  }

  public setCourseOfStudy(courseIds: any[]): any[] {
    const first_choice = parseInt(courseIds[0]);
    const second_choice = parseInt(courseIds[1]);

    const choicesArray: any[] = [];

    choicesArray.push({
      first_choice: this.selectWhereId(this.allFirstCourses, 'id', first_choice),
      second_choice: this.selectWhereId(this.allSecondCourses, 'id', second_choice),
    });

    console.log('Courses from set course of study ', choicesArray);
    return choicesArray;
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action == 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }

  public getYears() {
    const minYear = 2000;
    const maxYear = new Date().getFullYear();
    const yearsArray: number[] = [];
    for (let y = minYear; y <= maxYear; y++) {
      this.yearsArray.push(y);
      // //console.log("All years ::", this.yearsArray);
    }

  }

  private getAllCountries() {
    this.schoolService.getAllCountries().subscribe(
      (countriesResponse) => {
        this.allCountries = countriesResponse;
        // console.log("returned countries", countriesResponse);
      },
      (error) => {
        this.Alert.error(`Sorry could not load countries`, error);
      }
    );
  }

  public getStateByCountryId(countryId) {
    this.load.states.loading = true;
    this.schoolService.getStateByCountryId(countryId).subscribe(
      (statesResponse) => {
        this.load.states.loading = false;
        if (this.activeApplicant.state_origin) {
          this.getSingleState(this.activeApplicant.state_origin);
        }
        this.allStates = statesResponse.states;
      },
      (error) => {
        this.load.states.loading = false;
        this.Alert.error(`Sorry could not load States`, error);
      }
    );
  }

  public getSingleState(state_id) {
    this.load.lgas.loading = true;
    this.schoolService.getSingleState(state_id).subscribe(
      (stateResponse) => {
        this.load.lgas.loading = false;
        this.lgas = stateResponse.lgas;
      },
      (error) => {
        this.load.lgas.loading = false;
        this.Alert.error(`Sorry could not load Local Governments`, error);
      }
    );
  }

  public selectWhereId(data: any[], search_key: string, id) {
    const dataItem: any[] = [];
    data.forEach(item => {
      const itemKey = parseInt(item[search_key]);
      const itemId = parseInt(id);
      if (itemKey == itemId) {
        dataItem.push(item);
      }
    });
    return dataItem[0];
  }

  public almightyPrint(id, title, isCard = true) {
    const that = this;
    this.printing = true;
    const prev_frame = $('#print_frame');
    if (prev_frame) {
      prev_frame.remove();
    }
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['id'] = 'print_frame';
    let newContent = (!isCard) ? contents.replace(this.buttonHtml, "").replace(this.ackHtml, "")
      : contents.replace(this.cardHtml, "").replace(this.cardButton, "");
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-1000000px', 'pointer-events': 'none', 'border': '2px solid black'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    frameDoc.document.write('<html><head><title>Print ' + title + '</title>');
    frameDoc.document.write('</head><body>');
    frameDoc.document.write('<link href="/assets/css/style.bundle.css" rel="stylesheet">');
    frameDoc.document.write('<link href="/assets/css/applicnt_form.css" rel="stylesheet">');
    frameDoc.document.write('<style>.logo-img {max-width: 100%;max-height: 200px;display: block;margin: auto;}</style>');
    frameDoc.document.write(newContent);
    // frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    frameDoc.onload = function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      // frame1.remove();
      that.printing = false;
    }
  }


  public removeItemsDuringPrint(trueNotFalse: boolean) {
    let printButton = document.getElementById("pageButton");
    let ackButton = document.getElementById("acklink");

    if (trueNotFalse) {
      // ackButton.style.visibility = 'hidden';
      // setTimeout(() => {
      //     printButton.style.visibility = 'hidden';
      // }, 1)

    }
    else {
      // ackButton.style.display = 'visible';
      // setTimeout(() => {
      //     printButton.style.display = 'visible';
      // }, 1000)

      //Print the page content
      // printButton.style.visibility = 'visible';
    }
  }


  public buttonHtml = `<button _ngcontent-c3="" class="btn btn-accent m-btn--pill m-btn--air float-right mr-3 ml-auto" id="pageButton">
            <!---->
            <!----><i _ngcontent-c3="" class="la la-print"></i>
            Print
          </button>`;
  public ackHtml = `<a _ngcontent-c3="" id="acklink"><u _ngcontent-c3="">Acknowledgement Card</u></a>`;
  public cardHtml = `<a _ngcontent-c3="" id="acklink"><u _ngcontent-c3="">Application Preview</u></a>`;
  public cardButton = `<button _ngcontent-c3="" class="btn btn-accent m-btn--pill m-btn--air float-right mr-3 ml-auto" id="pageButton">
            <!---->
            <!----><i _ngcontent-c3="" class="la la-print"></i>
            Print Card
          </button>`;


  /**
   * Serere Jegede reporting
   * */

  public openOLevelForm() {
    $('#m_accordion_1_item_1_head').trigger('click');
    this.showResultForm = true;
  }

  public goToNextTab() {
    $('#m_user_profile_tab_2').trigger('click');
  }

}
