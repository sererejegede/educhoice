import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ApplcantComponent} from './applcant/applcant.component';
import {RouterModule, Routes} from '@angular/router';
import {AdmissionLetterComponent} from './admission-letter/admission-letter.component';
import {PaymentComponent} from './payment/payment.component';
import {ApplicantDashboardComponent} from './applicant-dashboard.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ApplicationTypeComponent} from './application-type/application-type.component';
import {FormComponent} from './form/form.component';
import {RoleGuardService} from '../../../../services/role-guard.service';
import { OpenProgrammesComponent } from './open-programmes/open-programmes.component';
import { ApplicationFormPreviewComponent } from './application-form-preview/application-form-preview.component';
import {Angular4PaystackModule} from 'angular4-paystack';



const APPLICANT_ROUTES: Routes = [
    {
        path: '', component: ApplicantDashboardComponent,
         canActivate: [RoleGuardService],
        children: [
        {path: '', component: ApplcantComponent},
        {path: 'profile', component: ApplcantComponent},
        {path: 'payment', component: PaymentComponent},
        {path: 'form', component: FormComponent},
        {path: 'admission-letter', component: AdmissionLetterComponent}
    ]
    },

];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        Angular4PaystackModule,
        RouterModule.forChild(APPLICANT_ROUTES)
    ],
    declarations: [ApplcantComponent, AdmissionLetterComponent, PaymentComponent, ApplicantDashboardComponent, FormComponent, ApplicationFormPreviewComponent],
    exports: [
        RouterModule,
        ApplcantComponent,
        AdmissionLetterComponent,
        ApplicantDashboardComponent,
        PaymentComponent
    ],
    providers:[RoleGuardService]
})
export class ApplicantModule {
}
