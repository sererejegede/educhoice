import {Component, OnInit} from '@angular/core';
import {AdmissionsService} from "../../../../../services/api-service/admissions.service";
import {Cache} from "../../../../../utils/cache";
import {SchoolService} from "../../../../../services/school.service";
import {NotificationService} from "../../../../../services/notification.service";

@Component({
    selector: 'app-open-programmes',
    templateUrl: './open-programmes.component.html',
    styleUrls: ['./open-programmes.component.css']
})

export class OpenProgrammesComponent implements OnInit {
    public allActiveApplications;
    public appSettings;
    public schoolName='';
    public allOpenProgrames: any[] = [];
    public loadingOpenProgrammes = false;
    public portletClasses = [
        'm--bg-accent',
        'm--bg-primary',
        'm--bg-secondary',
        'm--bg-info',
    ]

    constructor(private  admissionService: AdmissionsService,
                private schoolService: SchoolService,
                private Alert: NotificationService) {
        if (Cache.get('app_settings')) {
            this.appSettings = Cache.get('app_settings')['settings'];
            this.schoolName = Cache.get('app_settings')['name'];

        }
        else {
            this.getAllSettings();
        }
    }

    ngOnInit() {
        this.getActiveApplications();
    }


    public async getActiveApplications() {
        this.loadingOpenProgrammes = true;
        await  this.admissionService.getAllActiveApplicantApplications().subscribe(
            (activeAppliationsResponse) => {
                this.loadingOpenProgrammes = false;
                this.allActiveApplications = activeAppliationsResponse;
                this.allActiveApplications.forEach((application) => {
                        if ((application['open_programme'].length > 0)) {
                            application['open_programme'].forEach((programme) => {
                                if (programme['is_active']) {

                                    // let randNumber = Math.floor(Math.random() * 3) + 3;

                                    programme['class'] = (programme['mode_of_entry'].toLowerCase() ==='jamb')
                                    ? 'm--bg-accent':'m--bg-info';
                                    programme['programme_name'] = application['name'];
                                    this.allOpenProgrames.push(programme);
                                }
                            })
                        }
                    },
                    (error) => {
                        this.loadingOpenProgrammes = false;
                        this.Alert.error('Sorry Could not load open Programes ', error);
                    }
                )
                console.log('All open programmes ', this.allOpenProgrames);
                console.log('All active applications ', activeAppliationsResponse);
            },
            (error) => {
                console.log('An error occurred ', error);
            },
            () => {
                // this.getAllProgrammes();
            }
        );
    }


    public setHeaderColor() {
        return {
            'background-color': this.appSettings['primary_color']
        }
    }


    public getAllSettings() {
        this.getAppSettings();
    }

    private getAppSettings() {
        this.schoolService.getAppSettings().subscribe((appSettingsResponse) => {
                console.log('Application setings ', appSettingsResponse)
                Cache.set('app_settings', appSettingsResponse);
                this.appSettings = Cache.get('app_settings')['settings'];
                this.schoolName = Cache.get('app_settings')['name'];


                console.log('Application settings from open programmes header ', this.appSettings)
            },
            (error) => {
                console.log('Could not load application settings ', error);
            })
    }
}
