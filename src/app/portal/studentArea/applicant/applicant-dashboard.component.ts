import {Component, OnInit, AfterViewInit, Input} from '@angular/core';
import {UserService} from "../../../../services/user.service";
import {Cache} from "../../../../utils/cache";
import {SchoolService} from "../../../../services/school.service";
import {EmitPaymentService} from "../../../../services/emit-payment-service";
import {AuthenticationService} from "../../../../services/authentication.service";
import {NotificationService} from "../../../../services/notification.service";
import {Router} from "@angular/router";
import {AdmissionsService} from "../../../../services/api-service/admissions.service";


declare let mLayout: any;

@Component({
  selector: 'app-applicant-dashboard',
  templateUrl: './applicant-dashboard.component.html',
  styleUrls: ['./applicant-dashboard.component.css']
})
export class ApplicantDashboardComponent implements OnInit, AfterViewInit {

  public authenticatedUser: any;
  public salutationName = 'You';
  public applicantId: number = 0;
  public appSettings: any = {};
  public schoolName = '';
  public applicationSubmitted: boolean = false;

  public applicant = {
    hasPaid: false,
    hasBeenAdmitted: false
  };

  constructor(private userService: UserService,
              private schoolService: SchoolService,
              private authService: AuthenticationService,
              private emitPaymentService: EmitPaymentService,
              private Alert: NotificationService,
              private admissionService: AdmissionsService,
              private router: Router) {
    emitPaymentService.changeEmitted$.subscribe(
      (text) => {
        if (text === 'true') {
          this.applicant.hasPaid = true;
        } else if (text === 'form_submitted') {
          this.applicationSubmitted = true;
        }
      });
    this.applicantId = this.userService.getAuthUser()['login']['user']['id'];
    this.getApplicantProfile(this.applicantId);
    if (Cache.get('app_settings')) {
      this.appSettings = Cache.get('app_settings')['settings'];
      this.schoolName = Cache.get('app_settings')['name'];
      console.log('App Settings', this.appSettings);
    } else {
      this.getAllSettings();
    }

  }

  ngOnInit() {


  }

  ngAfterViewInit() {
    mLayout.initHeader();
  }

  public getAllSettings() {
    this.getAppSettings();
  }

  private getAppSettings() {
    this.schoolService.getAppSettings().subscribe((appSettingsResponse) => {
        console.log('Application settings ', appSettingsResponse);
        Cache.set('app_settings', appSettingsResponse);
        this.appSettings = Cache.get('app_settings')['settings'];
        console.log('Application settings from admin hedr ', this.appSettings)
      },
      (error) => {
        console.log('Could not load application settings ', error);
      });
  }


  public setHeaderColor() {
    return {
      'background-color': this.appSettings['primary_color']
    };
  }

  public logUserOut() {
    this.authService.logout().subscribe(
      (response) => {
        console.log('Log out done!');
      },
      (error) => {
        console.log('Could not log user out');
        this.userService.logout();

      });
  }


  public getApplicantProfile(id) {

    this.admissionService.getApplicantProfile(id).subscribe(
      (applicantProfileResponse) => {
        this.authenticatedUser = applicantProfileResponse['applicant'];
        this.salutationName = this.authenticatedUser['name'].split(' ')[0];
        this.authenticatedUser['programme_object'] = applicantProfileResponse['programme'];


        this.applicant.hasPaid = (this.authenticatedUser['payment_status'].toString() === '1');
        this.applicant.hasBeenAdmitted = (this.authenticatedUser['admission_status'].toString() === '1');

        if (!this.applicant.hasPaid) {
          this.Alert.info('Please pay your application fee in order to complete your application!');
          this.router.navigateByUrl('applicant/payment');
        }
        if (this.authenticatedUser['applicant_profile']['application_form_status'].toString() === '1') {
          this.applicationSubmitted = true;
        }
        if (this.applicant.hasPaid && !this.applicationSubmitted) {
          this.Alert.warning('Please fill your application form');
          this.router.navigateByUrl('applicant/form');

        }
      },
      (error) => {
        console.log('Could not load applicant profile response', error);
      }
    );
  }
}
