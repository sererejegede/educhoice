import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../../../services/notification.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AdmissionsService} from '../../../../../services/api-service/admissions.service';
import {StaffConfigService} from '../../../../../services/api-service/staff-config.service';
import {UserService} from '../../../../../services/user.service';
import {Observable} from 'rxjs/Observable';
import {Cache} from '../../../../../utils/cache';
import {SchoolService} from '../../../../../services/school.service';
import {EmitPaymentService} from '../../../../../services/emit-payment-service';


@Component({
    selector: 'app-application-type',
    templateUrl: './application-type.component.html',
    styleUrls: ['./application-type.component.css']
})
export class ApplicationTypeComponent implements OnInit, AfterViewInit {

    public formFile;
    public filename = '';
    public schoolName = '';
    public appSettings;
    public sortProgrammes;
    public uploadMessage = 'Save';
    public uploading = false;
    public application_id = 0;
    public facultyName;
    public facultyNames;
    public applicantUpdateForm: FormGroup;
    public jambApplicantRegistrationForm: FormGroup;
    public DEApplicantRegistrationForm: FormGroup;
    public applicantBioDataForm: FormGroup;
    public otherApplicantForm: FormGroup;
    public manualJambForm: FormGroup;
    public applicantUtmeInfo: any = null;
    public applicantUpdated = false;
    public authenticatedUser: any;
    public applicantForms = {
        biodata: false
    };
    public allActiveApplications: any[] = [];
    public activeApplication: any = {};
    public application_status_id = 0;
    public allCountries: any[] = [];
    public allStates: any[] = [];
    public lgas: any[] = [];
    public allProgrammes: any[] = [];
    public allDepartments: any[] = [];
    public allFaculties: any[] = [];
    public allCourses: any[] = [];
    public load = {
        other_applicants: {
            loading: false,
            creating: false
        },
        departments: {
            loading: false,
            list: false
        },
        courses: {
            loading: false,
            list: false
        },
        states: {
            loading: false,
            list: false
        },
        lgas: {
            loading: false,
            list: false
        }
    };
    public tabTitles: any[] = ['', '', ''];
    public activePaneId = '';
    public feedback = {
        loadingStatus: false
    };
    loadingApplications = true;


    static applicantUpdateForm = function () {
        return {
            phone: ['', Validators.compose([Validators.required])],
            // password: ['', Validators.compose([Validators.required])],
            // rpassword: ['', Validators.compose([Validators.required])],
            // fullname: ['', Validators.compose([Validators.required])]
        };
    };

    static otherApplicantForm = function () {
        return {
            // faculty_id: ['', Validators.compose([Validators.required])],
            // department_id: ['', Validators.compose([Validators.required])],
            // course_of_study_id: ['', Validators.compose([Validators.required])],
            // application_status_id: ['', Validators.required],
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            password: ['', Validators.required],
            confirm_password: [''],
            other_names: ['', Validators.required],
            email: ['', Validators.compose([Validators.required, Validators.email])],
            // image: ['', Validators.required],
            // state_origin: ['', Validators.required],
            // lga: ['', Validators.required],
            // age: ['', Validators.required],
            // gender: ['', Validators.required],
            // phone: ['', Validators.required],
            // address: ['', Validators.required],
            // next_of_kin: ['', Validators.required],
            // kin_phone: ['', Validators.required],
            // kin_address: ['', Validators.required]
        };
    };
    static manualJambForm = function () {
        return {
            // faculty_id: ['', Validators.compose([Validators.required])],
            // department_id: ['', Validators.compose([Validators.required])],
            // course_of_study_id: ['', Validators.compose([Validators.required])],
            // application_status_id: ['', Validators.required],
            reg_no: ['', Validators.required],
            total_ume_score: ['', Validators.required],
            first_choice: ['', Validators.required],
            second_choice: ['', Validators.required]
        };
    };

    static applicantBioDataForm = function () {
        return {
            // programme_id: ['', Validators.compose([Validators.required])],
            course_of_study: '',
            name: ['', Validators.compose([Validators.required])],
            state_origin: ['', Validators.compose([Validators.required])],
            lga: ['', Validators.compose([Validators.required])],
            age: ['', Validators.compose([Validators.required])],
            gender: ['', Validators.compose([Validators.required])],
            phone: ['', Validators.compose([Validators.required])],
            address: ['', Validators.compose([Validators.required])],
            next_of_kin: ['', Validators.compose([Validators.required])],
            kin_phone: ['', Validators.compose([Validators.required])],
            kin_address: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],
            reg_no: ['', Validators.compose([Validators.required])]
        };
    };

    static jambApplicantRegistrationForm = function () {
        return {
            jamb_num: ['', Validators.compose([Validators.required])],
            // password: ['', Validators.compose([Validators.required])],
            // rpassword: ['', Validators.compose([Validators.required])],
            // fullname: ['', Validators.compose([Validators.required])]
        };
    };

    static DEApplicantRegistrationForm = function () {
        return {
            jamb_num: ['', Validators.compose([Validators.required])],
            // password: ['', Validators.compose([Validators.required])],
            // rpassword: ['', Validators.compose([Validators.required])],
            // fullname: ['', Validators.compose([Validators.required])]
        };
    };

    constructor(private fb: FormBuilder,
                private Alert: NotificationService,
                private router: Router,
                private  admissionService: AdmissionsService,
                private staffConfigService: StaffConfigService,
                private el: ElementRef,
                private  userService: UserService,
                private route: ActivatedRoute,
                private emitpaymentService: EmitPaymentService,
                private schoolService: SchoolService) {
        if (Cache.get('app_settings')) {
            this.appSettings = Cache.get('app_settings')['settings'];
            console.log("Application Type app settings ",this.appSettings);
            this.schoolName = Cache.get('app_settings')['name'];
        } else {
            this.getAllSettings();
        }
        this.jambApplicantRegistrationForm = this.fb.group(ApplicationTypeComponent.jambApplicantRegistrationForm());
        this.DEApplicantRegistrationForm = this.fb.group(ApplicationTypeComponent.DEApplicantRegistrationForm());
        this.applicantUpdateForm = this.fb.group((ApplicationTypeComponent.applicantUpdateForm()));
        this.applicantBioDataForm = this.fb.group((ApplicationTypeComponent.applicantBioDataForm()));
        this.otherApplicantForm = this.fb.group((ApplicationTypeComponent.otherApplicantForm()));
        this.manualJambForm = this.fb.group((ApplicationTypeComponent.manualJambForm()));
    }


    ngOnInit() {
        this.getAllApplicantFaculties();
        this.getAllCountries();
        this.route.params.subscribe(params => {
            // this.feedBack.loading = true;
            this.application_status_id = +params['id']; // (+) converts string 'id' to a number
            // this.feedBack.loader = true;
            this.getApplicationById(this.application_status_id);
            this.facultyName = Cache.get('app_settings')['settings']['config_faculty'];
            this.facultyNames = Cache.get('app_settings')['settings']['config_faculties'];
            // In a real app: dispatch action to load the details here.
        });
    }

    ngAfterViewInit() {
        // $('#link0').addClass('active');

    }

    private getAllCountries() {
        this.schoolService.getAllCountries().subscribe(
            (countriesResponse) => {
                this.allCountries = countriesResponse;
                // console.log("returned countries", countriesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load countries`, error);
            }
        );
    }

    public getStateByCountryId(countryId) {
        this.load.states.loading = true;
        this.schoolService.getStateByCountryId(countryId).subscribe(
            (statesResponse) => {
                this.load.states.loading = false;
                this.allStates = statesResponse.states;
            },
            (error) => {
                this.load.states.loading = false;
                this.Alert.error(`Sorry could not load States`, error);
            }
        );
    }

    public getSingleState(state_id) {
        this.load.lgas.loading = true;
        this.schoolService.getSingleState(state_id).subscribe(
            (stateResponse) => {
                this.load.lgas.loading = false;
                this.lgas = stateResponse.lgas;
            },
            (error) => {
                this.load.lgas.loading = false;
                this.Alert.error(`Sorry could not load Local Governments`, error);
            }
        );
    }

    public signUpJambApplicant() {
        this.feedback.loadingStatus = true;
        this.admissionService.getApplicantUtmeInfo(this.jambApplicantRegistrationForm.value).subscribe(
            (registrationResponse) => {
                this.feedback.loadingStatus = false;
                this.Alert.success(`${registrationResponse['name']}, please continue with your application`);
                this.processREgistrationResponse(registrationResponse);
                this.applicantBioDataForm.patchValue(registrationResponse);
                this.applicantUtmeInfo = registrationResponse;
                const userIsApplicant = this.applicantUtmeInfo['applicant'];
                //
                // if(userIsApplicant && userIsApplicant['payment_status']=='1'){
                //     this.router.navigateByUrl('/');
                //     Cache.set('bonafide_applicant','yes');
                // }
                //

                this.applicantForms.biodata = true;
                console.log('Registration response ', registrationResponse);
            },
            (error) => {
                this.feedback.loadingStatus = false;
                const errorMessage = (JSON.parse(error['_body'])['message']) || error;
                this.Alert.error(errorMessage);
            }
        );
    }

    /**
     *
     * @param img_input
     * @param img_tag
     * @returns {any}
     */
    private validateImage(img_input, img_tag): boolean {
        const profilePhotoId = $(`#${img_tag}`);
        const $linkId = $(`#${img_input}`);
        const $newLinkId = $linkId.val();
        const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
        $linkId.removeClass('alert alert-danger animated rubberBand');
        if (file_type && (file_type !== '.jpg') && (file_type !== '.png') && (file_type !== '.jpeg')) {
            $linkId.addClass('alert alert-danger animated rubberBand');
            this.Alert.error('You can only upload jpg, jpeg or png files');
            return false;
        }
        if (!this.formFile) {
            this.Alert.error('You must provide a photo');
            this.scrollIntoView('#profile_photo');
            profilePhotoId.addClass('alert alert-danger animated rubberBand');
            setTimeout(() => {
                profilePhotoId.removeClass('alert alert-danger animated rubberBand');
            }, 2000);
            return false;
        }
        return true;
    }

    public signUpOtherProcessApplicant(fileId) {
        if (this.manualJambForm.value['total_utme_score'] > 400) {
            return this.Alert.error("Jamb Score can not be greater than 400!");
        }
        this.load.other_applicants.creating = true;
        // if (!this.validateImage(fileId, 'profile_photo_other')) {
        //   return this.load.other_applicants.creating = false;
        // }

        const formFile = this.el.nativeElement.querySelector(`#${fileId}`);
        // console.log('Applicant Bio data form ', this.applicantBioDataForm.value);
        // this.otherApplicantForm.value['file_key'] = 'image';
        this.otherApplicantForm.value['application_id'] = this.application_status_id;
        this.otherApplicantForm.value['de_or_other'] = this.activeApplication['mode_of_entry'].toLowerCase()!=='jamb';
        const applicantForm = Object.assign(this.otherApplicantForm.value, this.manualJambForm.value);
        this.admissionService.createOtherProcessApplicant(applicantForm, formFile).subscribe(
            (successResponse) => {
                if (successResponse.message) {
                    console.log(successResponse);
                    this.load.other_applicants.creating = false;
                    this.Alert.success(successResponse.message);
                    Cache.set('bonafide_applicant', {
                        val: 'yes',
                        username: this.otherApplicantForm.value['email'],
                        password: successResponse.password
                    });
                    return setTimeout(() => {
                        this.router.navigate(['/']);
                    }, 1500);
                }
                // console.log('Other applicant process', successResponse);
                this.load.other_applicants.creating = false;
                return this.Alert.success('Success');
            },
            (error) => {
                console.log('Other applicant process', error);
                this.load.other_applicants.creating = false;
                this.Alert.error('An error occurred', error);
            });
    }

    public signUpDEApplicant() {
        this.feedback.loadingStatus = true;
        this.admissionService.getApplicantUtmeInfo(this.DEApplicantRegistrationForm.value).subscribe(
            (registrationResponse) => {
                this.feedback.loadingStatus = false;
                this.Alert.success(`${registrationResponse['name']}, please continue with your application`);
                this.processREgistrationResponse(registrationResponse);
                this.applicantBioDataForm.patchValue(registrationResponse);
                this.applicantUtmeInfo = registrationResponse;
                this.applicantForms.biodata = true;
                console.log('Registration response ', registrationResponse);
            },
            (error) => {
                this.feedback.loadingStatus = false;
                const errorMessage = (JSON.parse(error['_body'])['message']) || error;
                this.Alert.error(errorMessage);
                // this.Alert.error('Registration failed, please try again', error);
            }
        );
    }


    public flipRegistration(id: string) {
        if (id == 'signup') {
            const login = $('#m_login');
            login.removeClass('m-login--forget-password');
            login.removeClass('m-login--signin');

            login.addClass('m-login--signup');
            (<any>login.find('.m-login__signup')).animateClass('flipInX animated');
            return;
        } else {
            const login = $('#m_login');
            login.removeClass('m-login--forget-password');
            login.removeClass('m-login--signup');
            login.addClass('m-login--signin');
            (<any>login.find('.m-login__signin')).animateClass('flipInX animated');
        }

    }

    public showTab(ind) {

        this.application_status_id = this.allActiveApplications[ind]['id'];
        const paneClass = `.tab-content #m_user_profile_tab_`;
        const tabClass = `#link`;
        const btnId = `#link`;
        const prevBtn = `${btnId}${this.activePaneId}`;
        const prevTab = `${tabClass}${this.activePaneId}`;
        const previousPane = `${paneClass}${this.activePaneId}`;
        this.activePaneId = ind;
        const activePane = `${paneClass}${ind}`;
        const activeBtn = `${btnId}${ind}`;
        const activeTab = `${tabClass}${ind}`;
        $(previousPane).removeClass('active');
        $(prevTab).removeClass('active');
        $(activePane).toggleClass('active');
        $(activeTab).toggleClass('active');
    }

    public getAllApplicantProgrammes() {
        this.admissionService.getAllProgrammesForApplicant().subscribe(
            (allProgrammesResponse) => {
                allProgrammesResponse.forEach((response) => {
                    this.allProgrammes.push(response['programme']);
                });
                console.log('All programmes response from sjsh,nbdjkdvjk ', this.allProgrammes);

            },
            (error) => {
                this.Alert.error('Sorry an error occurred. Could not load active programmes');
                console.log('All programmes response ', error);
            });
    }

    public getAllApplicantFaculties() {
        this.admissionService.getAllApllicantFaculties().subscribe(
            (facultiesResponse) => {
                this.allFaculties = facultiesResponse;

                console.log('All Applicant Faculties ', facultiesResponse);
            },
            (error) => {
                console.log('An error occurred laoding faculties ', error);
            });
    }

    public getDepartmentByFacultyId(facultyId) {
        this.load.departments.list = true;
        this.load.departments.loading = true;
        this.allFaculties.forEach((faculty) => {
            if (faculty.id === parseInt(facultyId)) {
                this.allDepartments = faculty.departments;
            }
        });

    }

    public getCourseByDepartmentId(departmentId) {
        this.allDepartments.forEach(department => {
            if (department.id === parseInt(departmentId)) {
                this.allCourses = department.course_of_studies;
            }
        });
    }

    public updateApplicantBioData(fileId) {
        this.uploading = true;
        this.uploadMessage = 'Saving';
        const bulkFileId = `#${fileId}`;
        const profilePhotoId = $('#profile_photo');
        const formFile = this.el.nativeElement.querySelector(bulkFileId);
        const $linkId = $(bulkFileId);
        const $newLinkId = $linkId.val();
        const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
        $linkId.removeClass('alert alert-danger animated rubberBand');
        if (file_type && (file_type !== '.jpg') && (file_type !== '.png')) {
            $linkId.addClass('alert alert-danger animated rubberBand');
            return this.Alert.error('You can only upload csv, or xlsx files');
        }
        if (!this.formFile) {
            this.uploading = false;
            this.uploadMessage = 'Save';
            this.Alert.error('You must provide a photo');
            this.scrollIntoView('#profile_photo');

            profilePhotoId.addClass('alert alert-danger animated rubberBand');
            return setTimeout(() => {
                profilePhotoId.removeClass('alert alert-danger animated rubberBand');
            }, 2000);

        }
        console.log('Applicant Bio data form ', this.applicantBioDataForm.value);
        this.applicantBioDataForm.value['file_key'] = 'image';
        this.applicantBioDataForm.value['application_status_id'] = this.application_status_id;
        this.admissionService.createApplicantBio(formFile, this.applicantBioDataForm.value).subscribe(
            (biodataResponse) => {

                this.uploading = false;
                this.uploadMessage = 'Save';
                //    this.userService.setAuthUser(biodataResponse);
                this.Alert.success(`A mail has been sent to ${this.applicantBioDataForm.value['email']}. Login with your credentials to continue your application`);
                this.applicantUpdated = true;
                this.cacheApplicant();
                this.router.navigateByUrl('/');
                console.log('Biodata response ', biodataResponse);
            },
            (error) => {
                this.uploading = false;
                this.uploadMessage = 'Save';
                this.Alert.error('An error occurred, could not save your bio data', error);
            });
    }

    public processREgistrationResponse(data) {
        if (data['gender'].toUpperCase() == 'M') {
            data['gender'] = 'Male';
        } else {
            data['gender'] = 'Female';
        }
    }

    public getPhoto(e, preview) {
        this.formFile = e.target;
        this.filename = this.formFile.files[0].name;
        if (this.validateImage('studentPhotoOther', preview)) {
            this.previewImage(preview);
        }
    }

    private previewImage(preview) {
        if (this.formFile.files && this.formFile.files[0]) {
            const reader = new FileReader();
            reader.onload = (e) => {
                $(`#${preview}`).attr('src', e.target['result']);
            };
            reader.readAsDataURL(this.formFile.files[0]);
        }
    }

    public updateApplicant() {
    }

    public getAllSettings() {
        this.getAppSettings();
    }

    private getAppSettings() {
        this.schoolService.getAppSettings().subscribe((appSettingsResponse) => {
                console.log('Application setings ', appSettingsResponse);
                Cache.set('app_settings', appSettingsResponse);
                this.appSettings = Cache.get('app_settings')['settings'];
                this.schoolName = Cache.get('app_settings')['name'];

                console.log('Application settings from admin hedr ', this.appSettings);
            },
            (error) => {
                console.log('Could not load application settings ', error);
            });
    }

    public setHeaderColor() {
        return {
            'background-color': this.appSettings['primary_color']
        };
    }

    public getApplicationById(application_id) {
        this.loadingApplications = true;
        this.admissionService.getApplicationById(application_id).subscribe((activeApplicationResponse) => {
                this.activeApplication = activeApplicationResponse;
                this.loadingApplications = false;

                console.log('This active application from url ', this.activeApplication);
            },
            (error) => {
                this.loadingApplications = false;

                this.Alert.error('Sorry an error occurred. Could not load the selected application ', error);
            },
            () => {
                this.getAllApplicantProgrammes();
            }
        );
    }

    public logInApplicant(data) {
        this.feedback.loadingStatus = true;
        // if (localStorage.getItem('biodata')) {
        //     localStorage.setItem('biodata', null);
        // }
        this.admissionService.applicantLogin(data).subscribe(
            (loginResponse) => {
                this.feedback.loadingStatus = false;
                this.userService.setAuthUser(loginResponse.token);
                console.log('Authenticated applicant ', this.userService.getAuthUser());
                if (this.userService.getAuthUser()['login']['user_type'] == 'App\\Models\\School\\Applicant') {
                    this.router.navigateByUrl('/applicant');
                } else {
                    return this.Alert.error('Invalid credentials, please check and try again');
                }
                // this.Alert.success(`${this.applicantLoginForm.value}, you have successfully registered as an applicant`);
                console.log('Registration response ', loginResponse);
            },
            (error) => {
                this.feedback.loadingStatus = false;
                this.Alert.error('Login Failed. Please check your credentials and retry!', error);
            }
        );

        // setTimeout(() => {
        //     (this.applicantLoginForm.value['jamb_reg_no'] == 'bw001') ? this.router.navigateByUrl('/applicant') :
        //         this.Alert.error(`${this.applicantLoginForm.value['jamb_reg_no']} does not exist on the  jamb record!`);
        //     this.feedback.loadingStatus = false;
        // }, 3000);
    }

    public cacheApplicant() {
        Cache.set('beginner', {val: 'yes'});
    }

    private scrollIntoView(location) {
        setTimeout(() => {
            this.el.nativeElement.querySelector(location).scrollIntoView({behavior: 'smooth'});
        }, 300);
    }


}
