import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, AbstractControl, FormControl} from '@angular/forms';
import {UserService} from '../../../../../services/user.service';
import {AdmissionsService} from "../../../../../services/api-service/admissions.service";
import {NotificationService} from "../../../../../services/notification.service";
import {Cache} from "../../../../../utils/cache";
import {Router} from "@angular/router";
import {EmitPaymentService} from "../../../../../services/emit-payment-service";

@Component({
    selector: 'app-applcant',
    templateUrl: './applcant.component.html',
    styleUrls: ['./applcant.component.css']
})
export class ApplcantComponent implements OnInit {

    public formFile;
    public filename = '';
    public authenticatedUser;
    public activeApplicant: any;
    public updateMessage = 'Save';
    public facultyName;
    public facultyNames;
    public loading = false;
    public loadPhoto = false;
    public applicationHasBeenSubmitted = false;
    public applicantUpdateForm: FormGroup;
    public applicantId: number = 0;
    public allSubjects: any[] = [];

    constructor(private fb: FormBuilder,
                private userService: UserService,
                private admissionService: AdmissionsService,
                private Alert: NotificationService,
                private emitPaymentService: EmitPaymentService,
                private router: Router) {

        this.applicantId = this.userService.getAuthUser()['login']['user']['id'];
        this.getApplicantProfile(this.applicantId);
        this.getAllOlevelSubjects();
        console.log('Authenticated User from constructor', this.authenticatedUser);
        this.applicantUpdateForm = this.fb.group(ApplcantComponent.applicantUpdateForm());
    }


    static applicantUpdateForm = function () {
        return {
            kin_phone: ['', Validators.compose([Validators.required])],
            phone: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],
            // name: ['', Validators.compose([Validators.required])],
            address: ['', Validators.compose([Validators.required])],
            kin_address: ['', Validators.compose([Validators.required])],
            next_of_kin: ['', Validators.compose([Validators.required])],
        };
    };

    ngOnInit() {
        this.facultyName = Cache.get('app_settings')['settings']['config_faculty'];
        this.facultyNames = Cache.get('app_settings')['settings']['config_faculties'];
        // this.getApplicantProfile(this.authenticatedUser['id']);

    }


    public updateApplicant() {
        this.updateMessage = 'Saving';
        this.loading = true;
        this.admissionService.updateApplicantInfo(this.activeApplicant['reg_no'], this.applicantUpdateForm.value).subscribe((updateResponse) => {
                this.updateMessage = 'Save';
                this.loading = false;
                this.Alert.success('Your profile changes have been updated');
            },
            (error) => {
                this.updateMessage = 'Save';
                this.loading = false;
                this.Alert.error('An error occurred, could not update your profile', error);
            }
        );
    }


    public getPhoto(e) {
        this.formFile = e.target;
        this.filename = this.formFile.files[0].name;
        this.previewImage();
    }

    public toggleSelection() {
        this.loadPhoto = !this.loadPhoto
    }

    private previewImage() {
        if (this.formFile.files && this.formFile.files[0]) {
            const reader = new FileReader();
            reader.onload = (e) => {
                $('#student_photo').attr('src', e.target['result']);
            };
            reader.readAsDataURL(this.formFile.files[0]);
        }
        this.toggleSelection();
    }

    public getApplicantProfile(id) {

        this.admissionService.getApplicantProfile(id).subscribe(
            (applicantProfileResponse) => {
                console.log("Application profile response bhghjhjhjhjhjhdjh", applicantProfileResponse);
                this.activeApplicant = applicantProfileResponse['applicant'];
                this.applicationHasBeenSubmitted = (this.activeApplicant['applicant_profile']['application_form_status'].toString() === '1');

                this.applicantUpdateForm.patchValue(this.activeApplicant);
                if (this.applicationHasBeenSubmitted) {
                    this.applicantUpdateForm.disable();
                }
                if (this.activeApplicant['payment_status'].toString() === '0') {
                    // alert('Active applicant payment status '+this.activeApplicant['payment_status']);
                    this.Alert.info('Pay your fees to complete your application');
                    this.router.navigateByUrl('applicant/payment');
                }
                else {
                    this.emitPaymentService.emitChange('true')
                }

                console.log('Applicant profile response on applicant profile', applicantProfileResponse);
            },
            (error) => {
                console.log('Could not load applicant profile response', error);
            }
        );
    }
    public getAllOlevelSubjects() {
        this.admissionService.getAllOlevelSubjects().subscribe(
            (subjectsResponse) => {
                this.allSubjects = subjectsResponse;
                console.log('All applicant subjects centers ', subjectsResponse);
            },
            (error) => {
                console.log('An error occurred loading subjects centers ', error);
            }
        );
    }


    public getSubjectNameById(subjectId) {
        const id = (subjectId) ? +subjectId : null;
        return this.allSubjects.filter(subject => {
            return subject['id'] == id;
        })[0]
    }


}
