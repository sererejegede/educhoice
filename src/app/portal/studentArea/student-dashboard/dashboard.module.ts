import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {StudentProfileComponent} from './student-profile/student-profile.component';
import {StudentDashboardComponent} from './dashboard.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StudentCourseRegComponent} from './student-course-reg/student-course-reg.component';
import {DataTablesModule} from 'angular-datatables';
import {LayoutModule} from '../../../layouts/layout.module';
import {PaymentComponent} from './payments/paymentComponent';
import {CourseSessionComponent} from './course-session/course-session.component';
import {StudentHostelComponent} from './student-hostel/student-hostel.component';
import {StudentReservationComponent} from './student-reservation/student-reservation.component';
import {StudentProfileImageComponent} from './student-profile-image/student-profile-image.component';
import {HomePageComponent} from './home-page/home-page.component';
import {ResultComponent} from './result/result/result.component';
import {RoleGuardService} from "../../../../services/role-guard.service";
import {ExeatComponent} from './exeat/exeat.component';
import {TransferComponent} from './transfer/transfer.component';
import {DefermentApplicationComponent} from './deferment-application/deferment-application.component';
import {TranscriptApplicationComponent} from './transcript-application/transcript-application.component';
import {Angular4PaystackModule} from "angular4-paystack";
import { AdmissionLetterComponent } from './admission-letter/admission-letter.component';


const STUDENT_DASHBOARD_ROUTES: Routes = [
    {
        path: '',
        component: StudentDashboardComponent,
        canActivate: [RoleGuardService],
        children: [
            {path: '', component: HomePageComponent},
            {path: 'profile', component: StudentProfileComponent},
            {path: 'payment', component: PaymentComponent},
            {path: 'courses', component: CourseSessionComponent},
            {path: 'hostel', component: StudentHostelComponent},
            {path: 'exeat', component: ExeatComponent},
            {path: 'deferment', component: DefermentApplicationComponent},
            {path: 'reservation', component: StudentReservationComponent},
            {path: 'result', component: ResultComponent},
            {path: 'register-course/:id', component: StudentCourseRegComponent},
            {path: 'transfer', component: TransferComponent},
            {path: 'transcript', component: TranscriptApplicationComponent},
            {path: 'admission-letter', component: AdmissionLetterComponent},
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        LayoutModule,
        DataTablesModule,
        ReactiveFormsModule,
        Angular4PaystackModule,
        RouterModule.forChild(STUDENT_DASHBOARD_ROUTES)
    ],
    exports: [RouterModule,
        StudentProfileComponent
    ],
    declarations: [
        CourseSessionComponent,
        StudentDashboardComponent,
        StudentProfileComponent,
        StudentCourseRegComponent,
        PaymentComponent,
        StudentHostelComponent,
        StudentReservationComponent,
        StudentProfileImageComponent,
        HomePageComponent,
        ResultComponent,
        ExeatComponent,
        TransferComponent,
        DefermentApplicationComponent,
        TranscriptApplicationComponent,
        AdmissionLetterComponent,

    ],
    providers: [RoleGuardService]
})


export class StudentDashboardModule {

}
