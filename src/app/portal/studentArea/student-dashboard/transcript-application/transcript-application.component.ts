import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {NotificationService} from "../../../../../services/notification.service";
import {StudentServiceService} from "../../../../../services/api-service/student-service.service";
import {Cache} from "../../../../../utils/cache";
import {SECRET_PAYMENT_KEY} from "../../../../../utils/magic-methods";
import * as CryptoJS from "crypto-js";

declare const $: any;

@Component({
  selector: 'app-transcript-application',
  templateUrl: './transcript-application.component.html',
  styleUrls: ['./transcript-application.component.css']
})
export class TranscriptApplicationComponent implements OnInit {

  transcriptForm: FormGroup;
  payTranscriptFee: FormGroup;
  transcript_id: number;
  student_id: number;
  transcript_amount: any;
  transcriptRequest: any[] = [];
  allSessions: any[] = [];
  allDepartments: any[] = [];
  transcriptRequestId;
  loading = {
    creating: false,
    pay: false,
    listing: false,
    session: false,
  };

  static requestTranscript = function () {
    return {
      destination_institution: ['', Validators.compose([Validators.required])],
      destination_institution_address: ['', Validators.compose([Validators.required])],
      session_id: ['', Validators.compose([Validators.required])]
    };
  };

  static payTranscriptFeeForm = function () {
    return {
      amount: [''],
      payment_method: ['', Validators.compose([Validators.required])],
    };
  };

  constructor(private studentService: StudentServiceService,
              private notification: NotificationService,
              private router: Router,
              private fb: FormBuilder) {
    this.transcriptForm = this.fb.group(TranscriptApplicationComponent.requestTranscript());
    this.payTranscriptFee = this.fb.group(TranscriptApplicationComponent.payTranscriptFeeForm());
  }

  ngOnInit() {
    if (Cache.get('cbsp_student_profile')) {
      this.loading.session = true;
      this.allSessions = Cache.get(`cbsp_student_profile`).course_of_study.degree.programme.sessions;
      this.loading.session = false;
    }
    this.getStudentId();
  }

  private getStudentId() {
    if (Cache.get('cbsp_student_profile')) {
      this.student_id = Cache.get(`cbsp_student_profile`).id;
      this.getAllTranscriptApplications();
    }
  }

  requestTranscript() {
    this.loading.creating = true;
    this.studentService.transcriptRequest(this.transcriptForm.value)
        .subscribe(response => {
              this.getAllTranscriptApplications();
              this.loading.creating = false;
              this.notification.success('Transcript request successfully made. Please check the status later');
            },
            error => {
              this.loading.creating = false;
              this.notification.error('Could not complete request', error);
              console.log('Oh Snap, something happened', error);
            }
        );
  }

  private getTranscriptAmount() {
    this.studentService.getDepartmentTranscriptFee(this.transcript_id)
        .subscribe((amount) => {
          console.log(amount);
          this.transcript_amount = amount;
        }, (error) => {
          this.notification.error('Something went wrong. Please retry', error);
        });
  }

  public payTranscriptFees() {
    this.loading.pay = true;
    Cache.set('RETURN_URL', window.location.href);
    console.log('RETURN_URL', Cache.get('RETURN_URL'));
    this.payTranscriptFee.value['student_transcript_application_id'] = this.transcript_id;
    this.payTranscriptFee.value['amount'] = this.transcript_amount;
    this.studentService.payTranscriptFee(this.payTranscriptFee.value)
        .subscribe((response) => {
              this.loading.pay = false;
              // console.log(response);
              switch (this.payTranscriptFee.value['payment_method'].toLowerCase()) {
                case 'paystack':
                  $('#paystack').attr('href', response['response'].authorization_url)[0].click();
                  break;
                case 'custom':
                  break;
                case'paychoice':
                  // console.log('Emeka\'s response 1', response);
                  const transactionObject = {
                    tid: response['transaction_id'],
                    url: response['url']
                  };
                  // console.log('Emeka\'s response 2', response);
                  const encryptedTransaction = CryptoJS['AES'].encrypt(JSON.stringify(transactionObject), SECRET_PAYMENT_KEY);
                  this.router.navigate(['/pay-transact', {paychoice: encryptedTransaction}]);
                  break;
                default:
                  this.notification.warning('Payment not completed');
              }
              this.triggerModalOrOverlay('close', 'pay_transcript');
              // this.notification.success('Payment Successful');
            },
            error => {
              this.loading.pay = false;
              this.triggerModalOrOverlay('close', 'pay_transcript');
              this.notification.error('Something went wrong. Try again later', error);
              console.log('error ::', error);
            });
  }

  getAllTranscriptApplications() {
    this.loading.listing = true;
    this.studentService.getAllTranscriptApplications(this.student_id)
        .subscribe(transcriptResponse => {
              this.loading.listing = false;
              console.log('Transcript response from server', transcriptResponse);
              this.transcriptRequest = transcriptResponse.transcript;
            },
            error => {
              this.loading.listing = false;
              this.notification.error('Could not load transcripts. Please try again');
              console.log('Get transcript error', error);
            });
  }

  openTranscriptModal(transcript_id) {
    this.transcript_id = transcript_id;
    this.getTranscriptAmount();
    this.triggerModalOrOverlay('open', 'pay_transcript');
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }
}
