import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Cache} from '../../../../../utils/cache';
import {StudentServiceService} from '../../../../../services/api-service/student-service.service';
import {NotificationService} from '../../../../../services/notification.service';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import 'rxjs/add/operator/switchMap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {UserService} from '../../../../../services/user.service';
import * as jsPDF from 'jspdf';

declare const $;

@Component({
  selector: 'app-student-course-reg',
  templateUrl: './student-course-reg.component.html',
  styleUrls: ['./student-course-reg.component.css']
})
export class StudentCourseRegComponent implements OnInit {
  private id = 0;
  public user_details;
  public addMore = false;
  public appSettings: any = null;
  private viewStdCourses = [];
  public createForm: FormGroup;
  public extraUnitForm: FormGroup;
  private paramId = 0;
  public feedBack = {
    courseStatus: '',
    session_year: '',
    total_unit: 0,
    courses: null,
    registered: null,
    courseDetail: [],
    courseArray: [],
    print_loader: false,
    loader: false,
    loadingUnit: false,
    registerLoader: false
  };
  @ViewChild('download') el: ElementRef;
  //noinspection TypeScriptValidateTypes
  dtTrigger: Subject<any> = new Subject();
  dtOptions: DataTables.Settings = {};
  static formData = function () {
    return {
      semester: ['', Validators.compose([Validators.required])]
    };
  };

  static extraUnitData = function () {
    return {
      unitload: ['', Validators.compose([Validators.required])]
    };
  };


  constructor(private studentServiceService: StudentServiceService,
              private userService: UserService,
              private notification: NotificationService,
              private router: Router,
              private fb: FormBuilder,
              private route: ActivatedRoute) {
    this.createForm = this.fb.group(StudentCourseRegComponent.formData());
    this.extraUnitForm = this.fb.group(StudentCourseRegComponent.extraUnitData());
  }

  ngOnInit() {
    this.getRequiredId();
    this.appSettings = Cache.get('app_settings');
    // console.log("Cached application settings ", Cache.get('app_settings'));

  }

  public getRequiredId() {
    let semester = '';
    this.feedBack.courses = [];
    // console.log('user details ', Cache.get('cbsp_student_profile'));
    // this.user_details = this.userService.getAuthUser()['login']['user'];
    this.user_details = Cache.get('cbsp_student_profile');
    this.paramId = +this.route.snapshot.paramMap.get('id');
    this.viewStdCourses = Cache.get('viewStudentCourses');
    // console.log('Viewed  student Courses', this.viewStdCourses);
    if (this.createForm.value.semester) {
      semester = this.createForm.value.semester;
    } else {
      semester = this.viewStdCourses['semester'];
    }
    this.route.params.subscribe((response) => {
        this.paramId = response['id'];
      });
    this.feedBack.courseStatus = this.viewStdCourses['checkStatus']; // determine whether to view or register course
    if (this.feedBack.courseStatus === 'registerCourse') {
      this.onRegisterCourses(+this.paramId);
    } else if (this.feedBack.courseStatus === 'viewCourse') {
      this.viewStudentCourses(this.paramId, this.viewStdCourses['level'], semester);
    } else {
      this.router.navigate(['student/courses']);
    }
  }

  private checkCourseRegistration (id): boolean {
    const obj = {
      reg_id: this.paramId,
      ...this.viewStdCourses
    };
    this.studentServiceService.verifyRegistration(obj).subscribe(res => {
      // if (res && res.)
    }, err => {

    });
    const bool = false;
    return bool;
  }

  public onRegisterCourses(id) {
    this.checkCourseRegistration(id);
    this.id = id;
    this.feedBack.loader = true;
    const regCourse = Cache.get('viewStudentCourses');
    this.studentServiceService.getStudentCourses(id, regCourse['semester']).subscribe((response) => {
          // console.log(' courses to register :: ', response);
          this.feedBack.courseDetail = response;
          this.feedBack.courses = response;
          this.feedBack.courses.registered.forEach(registered_course => {
            this.feedBack.courseArray.push(registered_course.id);
            this.feedBack.total_unit += +registered_course.unit;
          });
          this.feedBack.registered = response.registered;
          this.feedBack.loader = false;
        },
        error => {
          this.feedBack.loader = false;
          this.notification.error('Unable to load course, please retry', error);
        });
  }

  public viewStudentCourses(id, level_id, session) {
    this.id = id;
    this.feedBack.total_unit = 0;
    this.feedBack.loader = true;
    this.studentServiceService.viewStudentCourses(id, level_id, session)
      .subscribe((response) => {
          // console.info('view courses :: ', response);
          this.feedBack.loader = false;
          this.feedBack.courses = response;
          if (response) {
            /*this.feedBack.courses.forEach(value => {
              this.feedBack.total_unit += +value['unit'];
            });*/
            this.feedBack.total_unit = response.courses.reduce((a, b) => a + parseInt(b['unit'], 10), 0)
            // this.feedBack.session_year = response['0']['session_year'];
          }

        },
        error => {
          this.feedBack.loader = false;
          this.notification.error('Unable to load student course(s), please reload', error);
        });
  }

  public onSelectCourse(course, event) {
    const index = this.feedBack.courseArray.indexOf(course.id);
    if (index === -1 && event.target.checked) {
      this.feedBack.courseArray.push(course.id);
      this.feedBack.total_unit += +course.unit;
    } else {
      this.feedBack.total_unit -= +course.unit;
      this.feedBack.courseArray.splice(index, 1);
    }
  }

  public unSelectCourse(course, courseIndex, event) {
    if (!event.target.checked) {
      this.feedBack.courses.to_register.push(this.feedBack.courses.registered[courseIndex]);
      this.feedBack.courses.registered.splice(courseIndex, 1);
      this.feedBack.total_unit -= +course.unit;
      const index = this.feedBack.courseArray.indexOf(course.id);
      this.feedBack.courseArray.splice(index, 1);
    }
  }

  public postExraUnit() {
    this.feedBack.loadingUnit = true;
    this.studentServiceService.applyExtraUnit(this.paramId, this.extraUnitForm.value.unitload, Cache.get('viewStudentCourses')['semester'])
      .subscribe((response) => {
          this.feedBack.loadingUnit = false;
          this.notification.success(response.message);
          this.extraUnitForm.reset();
          $('#extraUnitModal').modal('hide');
        },
        error => {
          this.feedBack.loadingUnit = false;
          if (error.status === 406) {
            this.notification.error(JSON.parse(error._body)['message']);
          } else {
            this.notification.error('Unable to apply for Extra Unit, Please try again');
          }
        });
  }

  public checkPartPayment (type = 'print_course_registration'): any {
    this.feedBack.print_loader = true;
    const data = {
      session_id: this.feedBack.courses['session']['id'],
      semester_id: this.feedBack.courses['semester']['id'],
      type: type
    };
    this.studentServiceService.checkPartPayment(data).subscribe(res => {
      this.feedBack.print_loader = false;
      return res;
    }, err => {
      this.feedBack.print_loader = false;
      this.notification.error('An error occurred', err);
      return {
        status: false,
        message: 'Network error'
      };
    });
  }

  public printSlip(id, title?) {
    // if (!this.checkPartPayment().status) {
    //   return this.notification.error(this.checkPartPayment().message);
    // }
    this.feedBack.print_loader = true;
    const that = this;
    const prev_frame = $('#print_frame');
    if (prev_frame) {
      prev_frame.remove();
    }
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-1000000px', 'pointer-events': 'none'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    // frameDoc.document.write('<html><head><title>' + title + '</title>');
    frameDoc.document.write('</head><body>');
    frameDoc.document.write('<link href="/assets/css/print.css" rel="stylesheet">');
    // frameDoc.document.write('<link href="./student-course-reg.component.css" rel="stylesheet">');
    frameDoc.document.write('<style>.img-div--custom img {max-width: 80px; height: auto;}</style>');
    frameDoc.document.write('<style>.water-mark {-webkit-filter: grayscale(100%);filter: grayscale(100%);opacity: .095;position: absolute;top: 0;right: 0;left: 0;bottom: 0;} .relative {position: relative;}</style>');
    frameDoc.document.write('<style>.img-div {width: 100px;height: 100px;margin: 0 auto;}.img-div img {max-width: 100%;max-height: 100px;display: block;margin: auto;} .text-center {text-align: center !important;} .font-weight-bold {font-weight: 700 !important; } .my-3 {margin-top: 1rem !important; }</style>');
    frameDoc.document.write('<style>.table {width: 100%; border-collapse: collapse !important; }.table td,.table th {background-color: #fff !important; }.table-bordered th,.table-bordered td {border: 1px solid #ddd !important; } .text-left{text-align:left}</style>');
    frameDoc.document.write('<style>.table-bordered{border:1px solid #ddd};.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>tbody>tr>td,.table-bordered>tfoot>tr>td{border:1px solid #ddd}.table-bordered>thead>tr>th,.table-bordered>thead>tr>td{border-bottom-width:2px}</style>');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    frameDoc.onload = function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      // frame1.remove();
      that.feedBack.print_loader = false;
    };
  }

  public postRegisterCourses () {
    const regCourse = Cache.get('viewStudentCourses');
    this.feedBack.registerLoader = true;
    const data = {
      courses: this.feedBack.courseArray,
      id: this.id,
      session: regCourse['session'],
      semester: regCourse['semester'],
      level: regCourse['level']
    };
    this.studentServiceService.postRegisterCourses(data).subscribe((response) => {
          // console.log('register courses :: ', response)
          this.feedBack.courses = response;
          this.notification.success(response.message || 'Course was successfully registered');
          this.feedBack.courseDetail = response;
          this.feedBack.courseStatus = 'viewCourse';
          Cache.set('viewStudentCourses', {
            'checkStatus': 'viewCourse',
            'level': this.viewStdCourses['level'],
            'semester': this.viewStdCourses['semester'],
            'session': this.viewStdCourses['session']
          });
          // this.viewStudentCourses(this.paramId, this.viewStdCourses['level'], this.viewStdCourses['session']);
          this.feedBack.registerLoader = false;
        },
        error => {
          this.feedBack.registerLoader = false;
          this.notification.error('Unable to load course, please retry again', error);
        });
  }

  openSemesterModal() {
    $('#addSemester').modal('show');
  }

  onNavToSession() {
    this.router.navigate(['/student/courses']);
  }

  navToSession() {
    this.router.navigate(['student/courses']);
  }


  openExtraUnitModal() {
    $('#extraUnitModal').modal('show');
  }

  onPreview() {
    $('#courseRegForm').modal('show');
  }


  print() {
    const pdf = new jsPDF('p', 'pt', 'A4');
    const printElement = {
      '#course': function (element, renderer) {
        return true;
      }
    };
    const content = this.el.nativeElement;
    pdf.fromHTML(content.innerHTML, 40, 0, {
      'width': 400,
      'elementHandlers': printElement
    });
    /* pdf.addHTML(this.el.nativeElement, function () {
      pdf.save('course_reg_form.pdf');
    });*/

    pdf.save('course_reg_form.pdf');
  }
}
