import {Component, OnInit} from '@angular/core';
import {Cache} from '../../../../../../utils/cache';
import {StudentServiceService} from '../../../../../../services/api-service/student-service.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../../../../services/notification.service';
import {SchoolService} from '../../../../../../services/school.service';
// console.log = () => {};
@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
  semesterResult: any[] = [];
  semesterSession = {
    session: '',
    semester: ''
  };
  // semester = {};
  public studentResultDetails: any;
  public appSettings;
  public sessions = [];
  public session_semesters = [];
  public load = {
    semester: false
  };
  public createGroup: FormGroup;
  fetching = false;
  static formData = function () {
    return {
      session: ['', Validators.required],
      semester: ['']
    };
  };

  constructor(private studentService: StudentServiceService,
              private fb: FormBuilder,
              private notification: NotificationService,
              private schoolService: SchoolService) {
    this.createGroup = this.fb.group(ResultComponent.formData());
  }

  ngOnInit() {
    this.sessions = Cache.get('cbsp_student_profile')['registrations'];
    this.appSettings = Cache.get('app_settings');
    console.log(Cache.get('cbsp_student_profile')['registrations']);
  }

  setSemesterAndSession() {
    for (let i = 0; i < this.sessions.length; i++) {
      if (this.sessions[i].id == this.createGroup.value.session) {
        this.semesterSession.session = this.sessions[i].year;
      }
    }
    if (this.createGroup.value.semester == 1) {
      this.semesterSession.semester = 'First Semester';
    } else if (this.createGroup.value.semester == 2) {
      this.semesterSession.semester = 'Second Semester';
    }
  }

  public getSessionSemesters(session_id) {
    console.log('session_id', session_id);
    this.sessions.forEach(session => {
      if (session.session.id.toString() === session_id.toString()) {
        console.log('sessino', session);
        this.session_semesters = session.session.session_semesters;
      }
    });
  }

  getSessionAndSemesterResult() {
    this.fetching = true;
    console.log('Form values :: ', this.createGroup.value);
    this.setSemesterAndSession();
    this.studentResultDetails = Object.assign(this.semesterSession, Cache.get('cbsp_student_profile'));
    console.log('Student result details', this.studentResultDetails);
    this.studentService.getSessionAndSemesterResult(this.createGroup.value.session, this.createGroup.value.semester)
        .subscribe((response) => {
              this.fetching = false;
              console.log('result :: ', response);
              this.semesterResult = response;
              if (this.semesterResult.length < 1) {
                this.notification.warning('Result not found');
              }
            },
            (error) => {
              this.fetching = false;
              this.notification.error('Something went wrong! Please retry');
            });
  }

  public printNau(id) {
    const title = '';
    const prev_frame = $('#print_frame');
    if (prev_frame) {
      prev_frame.remove();
    }
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['id'] = 'print_frame';
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-1000000px', 'pointer-events': 'none'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    frameDoc.document.write('<html><head><title>Print ' + title + '</title>');
    frameDoc.document.write('</head><body>');
    frameDoc.document.write('<link href="/assets/css/style.bundle.css" rel="stylesheet">');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    frameDoc.onload = function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      // frame1.remove();
    }
  }

  public computeStuff(results: any[], stuff): number {
    return results.reduce((a, b) => a + parseInt(b[stuff], 10), 0);
  }

}
