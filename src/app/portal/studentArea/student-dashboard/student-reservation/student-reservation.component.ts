import {Component, ElementRef, OnInit} from '@angular/core';
import {StudentServiceService} from '../../../../../services/api-service/student-service.service';
import {NotificationService} from '../../../../../services/notification.service';
import {Cache} from '../../../../../utils/cache';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as CryptoJS from 'crypto-js';
import {SECRET_PAYMENT_KEY} from "../../../../../utils/magic-methods";
import {StaffConfigService} from '../../../../../services/api-service/staff-config.service';
import {Subject} from 'rxjs/Subject';
import {environment} from "../../../../../environments/environment";
import {UserService} from "../../../../../services/user.service";

declare const $;

@Component({
  selector: 'app-student-reservation',
  templateUrl: './student-reservation.component.html',
  styleUrls: ['./student-reservation.component.css']
})
export class StudentReservationComponent implements OnInit {
  public id = 0;
  public payment_status = false;
  public viewDetails: any;
  public feedBack = {
    submitStatus: false,
    loader: false,
    print_loader: false,
    available_loader: false,
    payment_status_loader: true,
    showRegisterStatus: false,
    hostel_reserved: false,
    allResponse: [],
    availableHostels: [],
    viewDetails: [],
    allLevel: [],
    roomspaces: [],
    bedspaces: []
  };
  public selectedRoom: null;
  public selectedHostel = null;
  public student = null;
  public ETRANSACT_URL = environment.ETRANSACT_URL;
  public paystackRef = '';
  public ebillsTransactionId: any;
  public ebillsAmount: number = 0;
  public etransactObject = {
    submitting: false,
    show: false,
    AMOUNT: 0,
    DESCRIPTION: '',
    LOGO_URL: '',
    NOTIFICATION_URL: '',
    RESPONSE_URL: '',
    TERMINAL_ID: '',
    TRANSACTION_ID: '',
    CHECKSUM: 'CHECKSUM',
    COL1: 'COL1',
    PAY_REF: '',
    PAYMENT_TYPE: '',
    SESSION_YEAR: '',
    OTHER_INFO: ''
  }
  public allPaymentMethods: any[] = [];
  public schoolSettings: any = null;
  public createForm: FormGroup;
  public submitStatus = false;
  static formData = function () {
    return {
      room: [''],
      hostel_bed_space_id: ['', Validators.compose([Validators.required])],
      payment_method: [''],
    };
  };


  constructor(private studentServiceService: StudentServiceService,
              private notification: NotificationService,
              private fb: FormBuilder,
              private userService: UserService,
              private el: ElementRef,
              private router: Router) {
    this.createForm = this.fb.group(StudentReservationComponent.formData());
  }

  ngOnInit() {
    this.loadFunction();
    // console.log(Cache.get('cbsp_student_profile'));
    this.student = this.userService.getAuthUser()['login']['user'];
    if (Cache.get('cbsp_student_profile')) {
      const student_gender = Cache.get('cbsp_student_profile')['gender'];
      if ((!student_gender || student_gender === '') && !Cache.get('student_gender')) {
        this.router.navigate(['/student/profile']);
      }
    }
    this.schoolSettings = (Cache.get('app_settings') ? Cache.get('app_settings') : Cache.get('ping'));
    this.allPaymentMethods = this.schoolSettings['settings']['payment_method'];
  }

  private loadFunction() {
    this.getReservations();
    this.checkAccommodationFee();
    this.getAvailableHostel();
  }

  getReservations() {
    this.feedBack.allResponse = [];
    this.feedBack.loader = true;
    this.studentServiceService.getReservations()
      .subscribe((response) => {
          if (response.length) {
            const reservations = response.filter((hostel) => {
              return hostel.status.toString() === '1';
            });
            if (reservations.length > 0) {
              this.feedBack.hostel_reserved = true;
            }
          }
          this.feedBack.allResponse = response;
          this.feedBack.loader = false;
        },
        error => {
          this.feedBack.loader = false;
          this.notification.error('Unable to load reservation, please reload', error);
        });
  }

  public getAvailableHostel() {
    this.feedBack.available_loader = true;
    this.studentServiceService.getAvailableHostels()
      .subscribe((response) => {
          this.feedBack.availableHostels = response;
          this.feedBack.available_loader = false;
        },
        error => {
          this.feedBack.available_loader = false;
          this.notification.error('Unable to load Available Hostels, Please retry', error);
        });
  }

  private checkAccommodationFee () {
    this.feedBack.payment_status_loader = true;
    this.studentServiceService.getPaymentHistory('completed')
      .subscribe((res) => {
          if (res && res.length) {
            // console.log('Response is not empty');
            for (let j = 0; j < res.length; j++) {
              if (res[j].session_year.toString() === '2018') {
                // console.log('Session is 2018');
                if (res[j].student_transaction && res[j].student_transaction.length) {
                  // console.log('Student has made at least one transaction');
                  res[j].student_transaction.forEach(transaction => {
                    for (let i = 0; i < transaction.student_transaction_items.length; i++) {
                      if (transaction.student_transaction_items[i].fee.fee_item.id.toString() === '1') {
                        // console.log('Accommodation Fee present');
                        if (transaction.student_transaction_items[i].status.toString() === '1') {
                          this.payment_status = true;
                          this.feedBack.payment_status_loader = false;
                          return;
                        }
                      }
                    }
                  });
                }
              }
            }
          }
          this.feedBack.payment_status_loader = false;
        },
        (error) => {
          console.log('Payment History Error', error);
          this.feedBack.payment_status_loader = false;
        });
  }

  createFormModal() {
    // && !this.schoolSettings['settings']['hostel_fee_item_name'] use this later when name can be set to null
    this.feedBack.submitStatus = true;
    const reservationObject = this.createForm.value;
    reservationObject['etransact_response_url'] = `http://${window.location.host}/pay-transact`;
    reservationObject['payment_method'] =
      (this.schoolSettings['settings']['hostel_with_school_fee'].toString() === '0') ?
        reservationObject['payment_method'] : 'schoolfee';
    reservationObject['amount'] = this.selectedHostel.hostel_amount;
    reservationObject['hostel_session_id'] = this.selectedHostel['hostel_session'][0]['id'];
    this.studentServiceService.onbookHostel(this.id, reservationObject)
      .subscribe(response => {
          this.viewDetails = response;
          this.getReservations();
          // $('#openHostel').modal('hide');
          this.processPaymentByMethod(reservationObject['payment_method'], response);
          // this.notification.success('Hostel was successfully booked');
        },
        error => {
          // $('#openHostel').modal('hide');
          const formatted_error = (error.message === 'You have a pending reservation') ? 'You have already made a reservation!' : error;
          this.notification.error('Unable to book Hostel, please try again', formatted_error);
          this.feedBack.submitStatus = false;
        });
  }

  onbookHostel(id, hostel) {
    this.id = id;
    this.selectedHostel = hostel['hostel'];
    this.feedBack.bedspaces = [];
    this.feedBack.roomspaces = hostel['hostel']['rooms'];
    $('#openHostel').modal('show');
  }

  onbedSpace(roomId) {
    this.selectedRoom = this.selectWhereId(this.feedBack.roomspaces, 'id', roomId.target.value);
    console.log('Selected room', this.selectedRoom);
    this.feedBack.roomspaces.forEach(value => {
      if (+value.id === +roomId.target.value) {
        this.feedBack.bedspaces = value['bed_spaces'];
      }
    });
  }

  private selectWhereId(data: any[], search_key: string, id) {
    let dataItem: any[] = [];
    data.forEach(item => {
      let itemKey = parseInt(item[search_key]);
      let itemId = parseInt(id);
      if (itemKey === itemId) {
        dataItem.push(item);
      }
    });
    return dataItem[0];

  }

  public viewReservation (details) {
    details['loading'] = true;
    console.log(this.student);
    this.viewDetails = details;
    this.viewDetails['loading'] = true;
    $('#hostelReservation').modal('show');
    this.viewDetails['loading'] = false;
    details['loading'] = false;
  }

  public processPaymentByMethod(payment_method, data) {

    Cache.set('RETURN_URL', window.location.href);
    switch (payment_method.toLowerCase()) {
      case 'paystack':
        this.paystackRef = data['response'].transaction_id;
        console.log('paystack data response', data['response']);
        // payButton.attr('href', data['response'].authorization_url);
        const payButton = $('#paystack');
        setTimeout(() => {
          payButton[0].click();
        }, 10);
        break;
      case 'custom':
        this.triggerModalOrOverlay('close', 'openHostel');
        setTimeout(() => {
          this.router.navigateByUrl('/applicant/form');
        }, 2000);
        break;
      case 'paychoice':
        this.triggerModalOrOverlay('close', 'openHostel');
        const transactionObject = {
          tid: data['transaction_id'],
          url: data['url']
        };
        const encryptedTransaction = CryptoJS['AES'].encrypt(JSON.stringify(transactionObject), SECRET_PAYMENT_KEY);
        this.router.navigate(['/pay-transact', {paychoice: encryptedTransaction}]);
        break;
      case 'ebillspay':
        this.ebillsTransactionId = data.response;
        this.triggerModalOrOverlay('close', 'openHostel');
        this.submitStatus = false;
        break;
      case 'etransact':
        this.etransactObject = data;
        this.etransactObject['show'] = true;
        this.triggerModalOrOverlay('close', 'openHostel');

        // setTimeout(() => {

        // }, 10)
        break;
      case 'schoolfee':
        this.triggerModalOrOverlay('close', 'openHostel');
        this.notification.success('Reservation Successful!');
        break;

      default:
        this.notification.warning('No Payment Transacted');
    }

  }

  public payEtransactNow() {
    this.etransactObject['paying'] = true;
    this.el.nativeElement.querySelector('form#etransact_submit').submit();
  }

  public paystackPaymentCancel() {
    this.triggerModalOrOverlay('close', 'createPayment');
  }

  public paystackPaymentDone(event) {
    console.log('Payment event ', event);
    this.triggerModalOrOverlay('close', 'createPayment');
    this.router.navigate(['/pay-transact', event]);
  }

  public printSlip(id, imgUrl?: string): void {
    this.feedBack.print_loader = true;
    const that = this;
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-1000000px', 'pointer-events': 'none'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    // frameDoc.document.write('<html><head><title>' + title + '</title>');
    frameDoc.document.write('</head><body>');
    frameDoc.document.write('<style>.text-danger{color:red;}.text-success{color:green;}</style>');
    frameDoc.document.write('<style>.img-div {width: 100px;height: 100px;margin: 0 auto;}.img-div img {max-width: 100%;max-height: 100px;display: block;margin: auto;} .text-center {text-align: center !important;} .font-weight-bold {font-weight: 700 !important; } .my-3 {margin-top: 1rem !important; }</style>');
    frameDoc.document.write('<style>.water-mark {-webkit-filter: grayscale(100%);filter: grayscale(100%);opacity: .095;position: absolute;top: 0;right: 0;left: 0;bottom: 0;} .relative {position: relative;}</style>');
    frameDoc.document.write('<style>.text-warning{color:#8a6d3b}.text-warning:hover{color:#66512c}.text-danger{color:#a94442}.text-danger:hover{color:#843534}.text-success{color:#3c763d}.text-success:hover{color:#2b542c}</style>');
    frameDoc.document.write('<style>.etr-div {width: 100px;height: 100px;margin: 0 auto;}.etr-div img {max-width: 100%;max-height: 100px;display: block;margin: auto;} .text-center {text-align: center !important;} .font-weight-bold {font-weight: 700 !important; } .my-3 {margin-top: 1rem !important; }</style>');
    frameDoc.document.write('<style>.table {width: 100%; border-collapse: collapse !important; }.table td,.table th {background-color: #fff !important; }.table-bordered th,.table-bordered td {border: 1px solid #ddd !important; } .text-left{text-align:left}</style>');
    frameDoc.document.write('<style>table-bordered{border:1px solid #ddd}.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>tbody>tr>td,.table-bordered>tfoot>tr>td{border:1px solid #ddd}.table-bordered>thead>tr>th,.table-bordered>thead>tr>td{border-bottom-width:2px}</style>');

    frameDoc.document.write('<style>.form-group.m-form__group.row{margin-top: 10px}</style>')
    frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    setTimeout(function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      frame1.remove();
      that.feedBack.print_loader = false;
    }, 2300);
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    if (ind) {
      $('#iframe').remove();
    }
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

  // public setStatusColor(status: string) {
  //   if (status) {
  //     switch (status.toLowerCase()) {
  //       case "cancelled":
  //         return {color: 'red'};
  //
  //       case "successful":
  //         return {color: 'green'};
  //
  //       default:
  //         return {color: 'red'};
  //
  //     }
  //   }
  // }

}
