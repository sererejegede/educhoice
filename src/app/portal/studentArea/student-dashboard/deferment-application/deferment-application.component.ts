import {Component, OnInit, ViewChild} from "@angular/core";
import {StudentServiceService} from "../../../../../services/api-service/student-service.service";
import {NotificationService} from "../../../../../services/notification.service";
import {Cache} from "../../../../../utils/cache";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs/Subject";
import {DataTableDirective} from "angular-datatables";

declare const $: any;


@Component({
  selector: 'app-deferment-application',
  templateUrl: './deferment-application.component.html',
  styleUrls: ['./deferment-application.component.css']
})
export class DefermentApplicationComponent implements OnInit {

  defermentForm: FormGroup;
  updateDefermentForm: FormGroup;
  selectedDefermentRequest;
  defermentRequest: any[];
  defermentRequestId;
  // student_id;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  //noinspection TypeScriptValidateTypes
  dtTrigger: Subject<any> = new Subject();
  loading = {
    listing: false,
    creating: false,
    updating: false
  };
  idObject = {
    student_id: null
  };

  static defermentRequest = function () {
    return {
      purpose: ['', Validators.compose([Validators.required])],
      num_of_session_deferred: ['', Validators.compose([Validators.required])],
    }
  };

  static editDefermentRequest = function () {
    return {
      student_id: Cache.get('cbsp_student_profile').id,
      purpose: ['', Validators.compose([Validators.required])],
      num_of_session_deferred: ['', Validators.compose([Validators.required])],
    }
  };
  
  constructor(private studentService: StudentServiceService,
              private notification: NotificationService,
              private fb: FormBuilder) {
    this.defermentForm = this.fb.group(DefermentApplicationComponent.defermentRequest());
    this.updateDefermentForm = this.fb.group(DefermentApplicationComponent.editDefermentRequest());
  }

  ngOnInit() {
    if (Cache.get('cbsp_student_profile')) {
      this.idObject.student_id = Cache.get('cbsp_student_profile').id;
    }
    this.getAllDefermentRequests();
  }

  requestDeferment() {
    this.loading.creating = true;
    const obj = Object.assign(this.defermentForm.value, this.idObject);
    console.log('Before creating', obj);
    this.studentService.defermentRequest(obj)
        .subscribe(response => {
              this.defermentRequest = [];
              this.getAllDefermentRequests();
              this.loading.creating = false;
              this.notification.success('Deferment request successfully made. Please check the status later');
              this.rerender();
            },
            error => {
              this.loading.creating = false;
              this.notification.error('Could not process request. Please retry');
              console.log('Expected deferment error', error);
            }
        )
  }

  getAllDefermentRequests() {
    this.loading.listing = true;
    this.studentService.getAllDefermentRequests(this.idObject.student_id)
        .subscribe(defermentResponse => {
              this.loading.listing = false;
              console.log('Deferment response from server', defermentResponse);
              this.defermentRequest = defermentResponse;
              if (this.defermentRequest.length > 0) {
                this.dtTrigger.next();
              }
            },
            error => {
              this.loading.listing = false;
              this.notification.error('Could not load deferment requests. Please try again');
              console.log('Get deferment error', error);
            })
  }

  updateDefermentRequest() {
    this.loading.updating = true;
    console.log('Deferment request form update', this.updateDefermentForm.value);
    this.studentService.updateDefermentRequest(this.defermentRequestId, this.updateDefermentForm.value)
        .subscribe((response) => {
              this.triggerModalOrOverlay('close', 'updateDefermentRequest');
              this.loading.updating = false;
              this.notification.success('Successfully updated request');
              this.rerender();
              console.log('Deferment request update response', response)
            },
            (error =>{
              this.triggerModalOrOverlay('close', 'updateDefermentRequest');
              this.loading.updating = false;
              this.notification.error('Something went wrong, please try again');
              console.log('Deferment request update error', error);
            }));
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      // this.dtTrigger.next();
      this.getAllDefermentRequests();
    });
  }

  openSesame(ind){
    this.selectedDefermentRequest = this.defermentRequest[ind];
    if(this.selectedDefermentRequest.status != 2){
      this.notification.warning('You cannot edit this deferment request again');
    }
    else {
      this.triggerModalOrOverlay('open', 'updateDefermentRequest');
      this.defermentRequestId = this.defermentRequest[ind].id;
      this.updateDefermentForm.patchValue(this.selectedDefermentRequest);
    }
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    if (action === 'open') {
      $(`#${modalId}`).modal('show');
    }
    else if (action === 'close') {
      $(`#${modalId}`).modal('hide');
    }
  }

}
