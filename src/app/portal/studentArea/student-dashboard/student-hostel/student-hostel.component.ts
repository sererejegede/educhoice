import {Component, OnInit} from "@angular/core";
import {StudentServiceService} from "../../../../../services/api-service/student-service.service";
import {NotificationService} from "../../../../../services/notification.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {StudentReservationComponent} from "../student-reservation/student-reservation.component";

declare const $;

@Component({
  selector: 'app-student-hostel',
  templateUrl: './student-hostel.component.html',
  styleUrls: ['./student-hostel.component.css']
})
export class StudentHostelComponent implements OnInit {
  public id = 0;
  public feedBack = {
    submitStatus: false,
    loader: false,
    showRegisterStatus: false,
    allResponse: [],
    allLevel: [],
    roomspaces: [],
    viewDetails: [],
    bedspaces: []
  };
  public createForm: FormGroup;
  public submitStatus = false;
  static formData = function () {
    return {
      hostel_bed_space_id: ['', Validators.compose([Validators.required])],
      payment_method: ['', Validators.compose([Validators.required])],
    };
  };


  constructor(private studentServiceService: StudentServiceService,
              private notification: NotificationService,
              private fb: FormBuilder,
              private router: Router) {
    this.createForm = this.fb.group(StudentHostelComponent.formData());
  }

  ngOnInit() {
    this.loadFunction();
  }

  private loadFunction() {
    this.getAvailableHostel();
  }


  /**
   * get all available hostels
   */
  getAvailableHostel() {
    this.feedBack.loader = true;
    this.studentServiceService.getAvailableHostels()
        .subscribe((response) => {
              this.feedBack.allResponse = response;
              this.feedBack.loader = false;
            },
            error => {
              this.feedBack.loader = false;
              this.notification.error('Unable to load available hostel, please reload', error);
            });
  }

  onbookHostel(id, hostel) {
    this.id = id;
    this.feedBack.bedspaces = [];
    this.feedBack.roomspaces = hostel['hostel']['rooms'];
    $('#openHostel').modal('show');
  }

  onbedSpace(roomId) {
    this.feedBack.roomspaces.forEach(value => {
      if (+value.id === +roomId.target.value) {
        this.feedBack.bedspaces = value['bed_spaces'];
      }
    });
  }

  createFormModal() {
    this.feedBack.submitStatus = true;
    this.studentServiceService.onbookHostel(this.id, this.createForm.value)
        .subscribe(response => {
              this.feedBack.viewDetails = response;
              $('#openHostel').modal('hide');
              this.notification.success('Hostel was successfully booked');
              this.feedBack.submitStatus = false;
            },
            error => {
              $('#openHostel').modal('hide');
              this.notification.error('Unable to book Hostel, please try again', error);
              this.feedBack.submitStatus = false;
            });
  }
}
