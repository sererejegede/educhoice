import {Component, OnInit, AfterViewInit} from '@angular/core';
import {ScriptLoaderService} from '../../../../services/script-loader.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from "../../../../services/notification.service";
import {Router} from "@angular/router";
import {Cache} from "../../../../utils/cache";

@Component({
  selector: 'app-student-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class StudentDashboardComponent implements OnInit {
  loginForm: FormGroup;
  static dataForm = function () {
    return {
      username: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
    }
  };

  constructor(private fb: FormBuilder,
              private Alert:NotificationService,
              private router:Router) {
    this.loginForm = this.fb.group(StudentDashboardComponent.dataForm);
  }

  ngOnInit() {
    this.determineUserDefaultPass();
  }



    private determineUserDefaultPass() {
        if (Cache.get('USER_PASSWORD')) {
            this.Alert.info(`Please change your default password!`);
            setTimeout(() => {
                this.router.navigateByUrl('/change-password');
            },3000);
        }
    }

}
