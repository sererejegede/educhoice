import {Component, OnInit} from '@angular/core';
import {StudentServiceService} from '../../../../../services/api-service/student-service.service';
import {NotificationService} from '../../../../../services/notification.service';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Cache} from '../../../../../utils/cache';
import {UserService} from '../../../../../services/user.service';
import {SchoolService} from "../../../../../services/school.service";

declare const $;

@Component({
  selector: 'app-student-profile',
  templateUrl: './student-profile.component.html',
  styleUrls: ['./student-profile.component.css']
})
export class StudentProfileComponent implements OnInit {
  public sponsorForm: FormGroup;
  public parents;
  public showSponsor = false;
  public sponsorArray = [];
  public allCountries: any[] = [];
  public allStates: any[] = [];
  public lgas: any[] = [];
  public control: FormGroup;
  public profileDetail = [];
  public feedBack = {
    show_gender_update: false,
    submitStatus: false,
    sponsorStatus: false,
    profileStatus: true,
    birthStatus: true,
    addressStatus: true,
    kinStatus: true,
    maritalStatus: true,
    allResponse: null,
  };
  public loader = {
    country: false,
    state: false,
    lga: false,
  };
  private feedBackMirror = {
    submitStatus: false,
    sponsorStatus: false,
    profileStatus: true,
    birthStatus: true,
    addressStatus: true,
    kinStatus: true,
    maritalStatus: true,
    allResponse: null
  };
  public updateProfileForm: FormGroup;
  public parentsForm: FormGroup;

  static formData = function () {
    return {
      birth_of_date: ['', Validators.compose([Validators.required])],
      blood_group: ['', Validators.compose([Validators.required])],
      contact_address: ['', Validators.compose([Validators.required])],
      country_id: ['', Validators.compose([Validators.required])],
      course_of_study_id: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      first_name: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])],
      geno_type: ['', Validators.compose([Validators.required])],
      height: ['', Validators.compose([Validators.required])],
      home_address: ['', Validators.compose([Validators.required])],
      home_town: ['', Validators.compose([Validators.required])],
      last_name: ['', Validators.compose([Validators.required])],
      level_id: ['', Validators.compose([Validators.required])],
      lga_id: ['', Validators.compose([Validators.required])],
      marital_status: ['', Validators.compose([Validators.required])],
      matric_no: ['', Validators.compose([Validators.required])],
      mode_of_entry: ['', Validators.compose([Validators.required])],
      next_of_kin: ['', Validators.compose([Validators.required])],
      nok_address: ['', Validators.compose([Validators.required])],
      nok_relationship: ['', Validators.compose([Validators.required])],
      nok_tel: ['', Validators.compose([Validators.required])],
      other_names: ['', Validators.compose([Validators.required])],
      // parents_name: ['', Validators.compose([Validators.required])],
      passport: ['', Validators.compose([Validators.required])],
      place_of_birth: ['', Validators.compose([Validators.required])],
      religion: ['', Validators.compose([Validators.required])],
      state_id: ['', Validators.compose([Validators.required])],
      status: ['', Validators.compose([Validators.required])],
      telephone_home: ['', Validators.compose([Validators.required])],
      telephone_mobile: ['', Validators.compose([Validators.required])],
      // parents: new FormArray([])
    };
  };

  static parents = function () {
    return {
      relationship: ['', Validators.compose([Validators.required])],
      title: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      telephone: ['', Validators.compose([Validators.required])],
      religion: ['', Validators.compose([Validators.required])],
      religion_denomination: ['', Validators.compose([Validators.required])],
    }
  }

  constructor(private studentServiceService: StudentServiceService,
              private userservice: UserService,
              private notification: NotificationService,
              private schoolService: SchoolService,
              private fb: FormBuilder) {
    this.updateProfileForm = this.fb.group(StudentProfileComponent.formData());
    this.parentsForm = this.fb.group(StudentProfileComponent.parents());
    // this.updateProfileForm.addControl('parents', this.fb.array([]));
  }

  ngOnInit() {
    this.loadFunction();

    this.profileDetail = Cache.get('cbsp_student_profile');
    if (this.profileDetail) {
      this.feedBack.allResponse = this.profileDetail;
      this.parents = this.profileDetail['parents'][0];
      if (!this.profileDetail['gender']) {
        this.feedBack.show_gender_update = true;
        this.notification.info('Please update your gender in the "Birth Details" tab');
      }
        } else {
      this.loadFunction();
    }
  }


  private loadFunction() {
    this.getStudentDetails();
    this.getAllCountries();
  }

  getStudentDetails() {
    this.studentServiceService.getStudentProfile()
      .subscribe((response) => {
          this.feedBack.allResponse = response;
          // this.onAddSponsor(response);
          this.setProfileData(response);

        },
        error => {
          console.log('error from profile ::', error);
          this.notification.error('Unable to load student Profile, please reload');
        });
  }

  /**
   * update
   */
  public updateFormModal(first_time_edit?) {
    if (first_time_edit) {
      this.updateProfileForm.value['edit_status'] = 1;
    }
    this.feedBack.submitStatus = true;
    // this.updateProfileForm.value['parents'] = this.sponsorArray;
    // console.log('creation before :: ', this.updateProfileForm.value);
    const studentUpdateData = this.parentsForm.value;
    for (let key in studentUpdateData) {
      if (studentUpdateData[key] === "") {
        studentUpdateData[key] = '----'
        if (key === 'email') {
          studentUpdateData[key] = 'null@null.com';
        }
        if (key === 'telephone') {
          studentUpdateData[key] = '0000';
        }
      }
    }
    this.updateProfileForm.value['parents'] = [studentUpdateData];
    // studentUpdateData = Object.assign(studentUpdateData,this.updateProfileForm.value);
    this.studentServiceService.updateStudentProfile(this.updateProfileForm.value)
      .subscribe((response) => {
          this.notification.success('Update was successful');
          Cache.set('student_gender', response.gender);
          this.onAddSponsor(response);
          this.reset();
          this.feedBack.allResponse = response;
          Cache.remove('cbsp_student_profile');
          Cache.set('cbsp_student_profile', response);
          this.setProfileData(response);
          this.feedBack.submitStatus = false;
        },
        error => {
          this.feedBack.submitStatus = false;
          // this.onAddSponsor();
          //  this.updateProfileForm.patchValue(this.profileDetail);
          this.notification.error('Unable to Update your profile, please retry', error);
          //   console.log('error ::', error);
        },
        () => {
          this.resetButtons();
        });

  }

  private setProfileData(response) {
    Cache.set('cbsp_student_profile', response);
    this.updateProfileForm.patchValue(response);
    if (response.parents && response.parents.length) {
      this.parentsForm.patchValue(response['parents'][0]);
      this.parents = response['parents'][0];
    }
    if (response.country_id) {
      this.getStateByCountryId(response.country_id);
    }
  }

  reset() {
    this.showSponsor = false;
    this.feedBack.profileStatus = true;
    this.feedBack.birthStatus = true;
    this.feedBack.kinStatus = true;
    this.feedBack.maritalStatus = true;
    this.feedBack.addressStatus = true;
    this.feedBack.sponsorStatus = true;
    $('.kin').prop('disabled', true);
    $('.profile').prop('disabled', true);
    $('.birth').prop('disabled', true);
    $('.marital').prop('disabled', true);
    $('.address').prop('disabled', true);
    $('.sponsor').prop('disabled', true);
  }

  onEditProfile() {
    this.feedBack.profileStatus = !this.feedBack.profileStatus;
    if (this.feedBack.profileStatus === true) {
      $('.profile').prop('disabled', true);
    } else {
      $('.profile').prop('disabled', false);
    }
  }


  onEditBirth() {
    this.feedBack.birthStatus = !this.feedBack.birthStatus;
    if (this.feedBack.birthStatus === true) {
      $('.birth').prop('disabled', true);
    } else {
      $('.birth').prop('disabled', false);
    }
  }

  onEditKin() {
    this.feedBack.kinStatus = !this.feedBack.kinStatus;
    if (this.feedBack.kinStatus === true) {
      $('.kin').prop('disabled', true);
    } else {
      $('.kin').prop('disabled', false);
    }
  }


  onEditMarital() {
    this.feedBack.maritalStatus = !this.feedBack.maritalStatus;
    if (this.feedBack.maritalStatus === true) {
      $('.marital').prop('disabled', true);
    } else {
      $('.marital').prop('disabled', false);
    }
  }

  onEditAddress() {
    this.feedBack.addressStatus = !this.feedBack.addressStatus;
    if (this.feedBack.addressStatus === true) {
      $('.address').prop('disabled', true);
    } else {
      $('.address').prop('disabled', false);
    }
  }

  onEditSponsor() {
    this.showSponsor = !this.showSponsor;
    if (this.showSponsor === true) {
      $('.sponsor').prop('disabled', false);
    } else {
      $('.sponsor').prop('disabled', false);
    }
  }

  updateSponsor() {
    (<FormArray>this.updateProfileForm.get('parents')).push(this.sponsorForm.value);
    console.log('sponsor form :: ', this.updateProfileForm.value);
    //  this.sponsorArray.push(this.sponsorForm.value);
    /* this.studentServiceService.updateStudentProfile({parents :this.sponsorArray})
       .subscribe((response) => {
           this.notification.success('update was successful');
           Cache.set('cbsp_student_profile', response);
           this.updateProfileForm = this.fb.group(response);
           this.feedBack.submitStatus = false;
         },
         error => {
           this.feedBack.submitStatus = false;
           this.notification.error('Unable to Update your profile, please retry', error);
           //   console.log('error ::', error);
         });*/
    // this.updateFormModal();
  }

  onAddSponsor(profileData) {
    // this.control = this.generateControl();
    // this.control.patchValue(profileData)
    // const items = this.updateProfileForm.get('parents') as FormArray;
    // items.push(this.control);
  }


  private resetButtons() {
    for (let key in this.feedBack) {
      if (key !== 'allResponse') {
        this.feedBack[key] = this.feedBackMirror[key];
      }
    }
  }

  public setStudentGender(studentGender) {

    this.updateProfileForm.value['gender'] = studentGender;

  }

  private generateControl() {
    return this.fb.group({
      relationship: ['', Validators.compose([Validators.required])],
      title: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      telephone: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      religion: ['', Validators.compose([Validators.required])],
      religion_denomination: ['', Validators.compose([Validators.required])],
    });
  }

  private getAllCountries() {
    this.schoolService.getAllCountries().subscribe(
      (countriesResponse) => {
        this.allCountries = countriesResponse;
        // console.log("returned countries", countriesResponse);
      },
      (error) => {
        this.notification.error(`Sorry could not load countries`, error);
      }
    );
  }

  public getStateByCountryId(countryId) {
    this.loader.state = true;
    this.schoolService.getStateByCountryId(countryId).subscribe(
      (statesResponse) => {
        this.loader.state = false;
        this.allStates = statesResponse.states;
        if (this.feedBack.allResponse.state_id) {
          this.getSingleState(this.feedBack.allResponse.state_id);
        }
      },
      (error) => {
        this.loader.state = false;
        this.notification.error(`Sorry could not load States`, error);
      }
    );
  }

  public getSingleState(state_id) {
    this.loader.lga = true;
    this.schoolService.getSingleState(state_id).subscribe(
      (stateResponse) => {
        this.loader.lga = false;
        this.lgas = stateResponse.lgas;
      },
      (error) => {
        this.loader.lga = false;
        this.notification.error(`Sorry could not load Local Governments`, error);
      }
    );
  }
}
