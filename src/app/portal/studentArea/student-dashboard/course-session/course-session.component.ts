import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../../../../services/notification.service';
import {StudentServiceService} from '../../../../../services/api-service/student-service.service';
import {Router} from '@angular/router';
import {Cache} from '../../../../../utils/cache';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

declare const $;
// console.log = function () {};
@Component({
  selector: 'app-course-session',
  templateUrl: './course-session.component.html',
  styleUrls: ['./course-session.component.css']
})
export class CourseSessionComponent implements OnInit {
  public id;
  public level;
  public session;
  public schoolSettings: any;
  public currentYear = [];
  public registeredCourses: any[] = [];
  public user_details: any;
  public createForm: FormGroup;
  public coursesToReg = {
    all_reg: []
  };
  public feedBack = {
    allResponse: [],
    allCourses: [],
    courseArray: [],
    status: '',
    bool: false,
    exception: false,
    checking: false,
    semester: '1',
    student_reg_id: '',
    level_id: '',
    loader: false,
    printing: false,
    submitStatus: false,
    showViewCourse: false,
    showCourse: false,
    registerLoader: false,
    course_form_checker: false,
    course_form_bool: false,
  };
  public cardPrintOutDetails = {
    current_school: 'Bowen University',
    'FABOTAS COLLEGE': {
      name: 'FABOTAS COLLEGE',
      address: 'KM 8 IKERE ROAD, P. O. BOX 1473, ADO-EKITI, EKITI STATE.',
      subtitle: 'CLEARANCE FOR FIRST SEMESTER EXAMINATION 2018/2019 ACADEMIC SESSION',
    },
    'Crysto Uni': {
      name: 'Crysto Uni',
      address: 'Excellence',
      subtitle: 'CLEARANCE FOR FIRST SEMESTER EXAMINATION 2018/2019 ACADEMIC SESSION',
    },
    'Bowen University': {
      name: 'Bowen University',
      address: 'IWO, OSUN STATE',
      subtitle: 'CLASS ATTENDANCE/ EXAMINATION CARD'
    }
  };

  static formData = function () {
    return {
      semester: ['', Validators.compose([Validators.required])]
    };
  };

  constructor(private notification: NotificationService,
              private router: Router,
              private fb: FormBuilder,
              private studentServiceService: StudentServiceService) {
    this.createForm = this.fb.group(CourseSessionComponent.formData());
  }

  ngOnInit() {
    this.user_details = Cache.get('cbsp_student_profile');
    this.schoolSettings = Cache.get('ping') || Cache.get('app_settings');
    this.getStudentDetails();
    this.checkCourseForm();
    this.cardPrintOutDetails.current_school = this.schoolSettings.name;
    // console.log(this.schoolSettings);
  }

  /**
   * get all previous registered courses
   */
  getStudentDetails() {
    this.feedBack.loader = true;
    this.studentServiceService.getStudentCourseList().subscribe((response) => {
        console.log(response);
          const currentSession = (response.current) ? response.current.session_year : [];
          this.coursesToReg.all_reg = response.all_reg;
          this.coursesToReg['all_reg'].forEach((val, i) => {
            if (val.session_year === currentSession) {
              if (this.currentYear.length < 1) {
                this.currentYear.push(val);
                this.checkCourseRegistration();
                this.checkStudentException();
              }
              this.coursesToReg['all_reg'].splice(i, 1);
            }
          });
          this.feedBack.loader = false;
          // console.log('Current session response ',response)
        },
        error => {
          this.feedBack.loader = false;
          this.notification.error('Unable to load student course(s), please reload', error);
        });
  }

  public getStudentCourses(semester) {
    this.feedBack.printing = true;
    this.studentServiceService.getStudentCourses(this.user_details.last_registration.session_id, semester).subscribe(
      (res) => {
        setTimeout(() => {
          this.almightyPrint('course_form', 'Course Form');
        }, 500);
        this.registeredCourses = res.registered ? res.registered : [];
      }, (err) => {
        this.feedBack.printing = false;
        this.notification.error('Failed', err);
      });
  }

  private extractedSessionDetails(checkStatus, session) {
    this.feedBack.status = checkStatus;
    this.feedBack.student_reg_id = session.id;
    this.level = session.level_id;
    this.session = session.session_year;
    $('#addLevel').modal('show');
  }

  viewStudentCourses(session, status) {
    this.extractedSessionDetails(status, session);
  }

  private checkCourseRegistration (semester = this.feedBack.semester) {
    // Checking if student has registered before
    this.feedBack.checking = true;
    const session = this.currentYear[0];
    const obj = {
      reg_id: this.user_details.last_registration.id,
      'session': session.session_year,
      'semester': semester,
      'level': session.level_id
    };
    this.studentServiceService.verifyRegistration(obj).subscribe(res => {
      this.feedBack.checking = false;
      // console.log(res);
      this.feedBack.bool = !(res === 'true');
      console.log('It is ' + res + ' that this student has registered once');
      if (this.feedBack.bool) {
        this.regHasEnded();
      }
    }, err => {
      this.feedBack.checking = false;
      this.notification.error('An error occurred', err);
    });
    // console.log(this.feedBack.bool);
  }

  private regHasEnded () {
    /**
     * Don't bother checking if the student has registered once before
     **/
    ////// Checking if registration hasn't ended yet and student hasn't registered before /////
    // if (this.feedBack.bool) {
    const reg_end_date = this.currentYear[0].session.current_semester.reg_end_date;
    console.log('Registration end date', reg_end_date);
      if (reg_end_date) {
        this.feedBack.bool = (new Date(reg_end_date).getTime()) > (new Date().getTime());
        console.log('It is ' + !this.feedBack.bool + ' that course registration has ended');
      }
    // }
  }

  private checkStudentException () {
    this.feedBack.checking = true;
    const session = this.currentYear[0];
    // console.log('Session object  that should have semester', session);
    const obj = {
      'year': session.session_year,
      'semester': session.session.current_semester.semester,
      'type': 'registration'
    };
    this.studentServiceService.checkException(obj).subscribe(res => {
      this.feedBack.checking = false;
      this.feedBack.exception = res.exception;
    }, err => {
      this.feedBack.checking = false;
      this.notification.error('An error occurred', err);
    });
    // console.log(this.feedBack.bool);
    // console.log(this.feedBack.exception);
    // console.log(this.feedBack.exception || this.feedBack.bool);
  }

  public goToCourseReg(session, status, semester) {
    Cache.set('viewStudentCourses', {
      'checkStatus': status,
      'session': session.session_year,
      'semester': semester,
      'level': session.level_id
    });
    this.router.navigate(['student/register-course', session.id]);
  }

  public almightyPrint (id, title) {
    this.feedBack.printing = true;
    const that = this;
    const prev_frame = $('#print_frame');
    if (prev_frame) {
      prev_frame.remove();
    }
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['id'] = 'print_frame';
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-1000000px', 'pointer-events': 'none'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ?
      frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ?
        frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    frameDoc.document.write('<html><head><title>Print ' + title + '</title>');
    frameDoc.document.write('</head><body>');
    frameDoc.document.write('<link href="/assets/css/style.bundle.css" rel="stylesheet">');
    frameDoc.document.write('<style> .logo-img {\n' +
      '  max-width: 100%;\n' +
      '  max-height: 200px;\n' +
      '  display: block;\n' +
      '  margin: auto;\n' +
      '}</style> ');
    frameDoc.document.write('<style>body {font-size: 130%}</style>');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    frameDoc.onload = function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      // frame1.remove();
      that.feedBack.printing = false;
    }
  }

  private checkCourseForm () {
    this.feedBack.course_form_checker = true;
    const data = {
      reg_id: this.user_details.last_registration.id,
      semester: this.user_details.course_of_study.degree.programme.current_session.current_semester.semester
    };
    this.studentServiceService.checkCourseForm(data).subscribe(
      (res) => {
        this.feedBack.course_form_checker = false;
        this.feedBack.course_form_bool = (res === 'true');
      }, (err) => {
        this.feedBack.course_form_checker = false;

      });
  }
}
