import { Component, OnInit } from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-admission-letter',
  templateUrl: './admission-letter.component.html',
  styleUrls: ['./admission-letter.component.css']
})
export class AdmissionLetterComponent implements OnInit {

    public ckeditorContent = '';
    public userPhoto = '';
    public schoolLogo = '';
    public admissionLetterVariables = {
        school: {
            value: "{{schoolName}}",
            text: ""
        },

        student: {
            text: "",
            value: "{{studentName}}"
        },

        department: {
            text: "",
            value: "{{departmentName}}"
        },

        session: {
            text: "",
            value: "{{session}}"
        },
        matric_no:
            {
                text: "",
                value: "{{matricNo}}"
            },
        faculty: {
            text: "Faculty",
            value: "{{facultyName}}"
        }
    };

  constructor() { }

  ngOnInit() {
  }


    public setSchoolLogo():{background, 'background-size'}{
        return {
            background: `url(${this.schoolLogo}) no-repeat`,
            'background-size':'fit'
        }
    }


}
