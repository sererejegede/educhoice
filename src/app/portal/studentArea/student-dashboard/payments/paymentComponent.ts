import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import {StudentServiceService} from '../../../../../services/api-service/student-service.service';
import {NotificationService} from '../../../../../services/notification.service';
import {Cache} from '../../../../../utils/cache';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import * as CryptoJS from 'crypto-js';
import {SECRET_PAYMENT_KEY} from '../../../../../utils/magic-methods';
import {environment} from '../../../../../environments/environment';
import {DataTableDirective} from 'angular-datatables';
import {generateRandomString} from "../../../../../utils/magic-methods";

declare const $;

@Component({
  selector: 'app-payment',
  templateUrl: './paymentComponent.html',
  styleUrls: ['./paymentComponent.css']
})
export class PaymentComponent implements OnInit {
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  public feeToBeDisplayed: any[] = [];
  private registration = {
    level_id: null,
    session_id: null
  };
  public feeToBeDisplayedEmeka: any[] = [];
  public reQuery = {
    show: false,
    url: '',
    loading: false
  };
  private currentYear = 0;
  public showCustomInput = false;
  public visibleCustom = true;
  public pay_picture: any;
  public feesThatAreAlreadySpread = [];
  public allFeeKeys = [];
  public authStudent;
  public currentSession = [];
  private selectedId = [];
  public feeItem_Id = [];
  public fee_details: any;
  public feeInfo = [];
  public studentRegistrations = [];
  public sessionId = 0;
  public levelId = 0;
  public transactionFee = 0;
  public ebillsTransactionId: any;
  public ebillsTransactionCost = 0;
  public ebillsAmount = 0;
  public paystackRef = '';
  public noContentMessage = 'No Content Found';
  public ETRANSACT_URL = environment.ETRANSACT_URL;
  public XPRESSPAY_URL = environment.XPRESSPAY_URL;
  public XPRESSPAY_CALLBACK_URL = 'http://' + window.location.host + '/pay-transact';
  private paymentLogoUrls = {
    etransact: 'assets/images/logos/etranzact.png',
    paychoice: 'assets/images/logos/paychoice.png',
    paystack: 'assets/images/logos/paystack.png',

  };
  public paymentLogo = null;

  public etransactObject = {
    submitting: false,
    show: false,
    AMOUNT: 0,
    DESCRIPTION: '',
    LOGO_URL: '',
    NOTIFICATION_URL: '',
    RESPONSE_URL: '',
    TERMINAL_ID: '',
    TRANSACTION_ID: '',
    CHECKSUM: 'CHECKSUM',
    COL1: 'COL1',
    PAY_REF: '',
    PAYMENT_TYPE: '',
    SESSION_YEAR: '',
    OTHER_INFO: '',
    ASSET_LOGO: ''
  };
  public xpresspayObject: any;
  public feedBack = {
    submitStatus: false,
    loader: false,
    print_loader: false,
    file_is_valid: false,
    payment_status: '',
    loadFee: false,
    load_fee: false,
    showRegisterStatus: false,
    enablePartPayment: null,
    allResponse: [],
    payments: [],
    schoolDetails: null,
    viewDetails: null,
    payment_school_fee_auto_fill: 0,
    allFees: [],
    allLevel: []
  };
  public paymentHistory: any[] = [];
  public sessionPaymentHistory;
  public createForm: FormGroup;
  public submitStatus = false;
  public sessionsArray = [];
  static dataForm = function () {
    return {
      payment_method: ['', Validators.compose([Validators.required])],
      receipt: [''],
      fee_items: ['', Validators.compose([])]
    };
  };


  constructor(private studentServiceService: StudentServiceService,
              private notification: NotificationService,
              private fb: FormBuilder,
              private router: Router,
              private el: ElementRef) {
    this.createForm = this.fb.group(PaymentComponent.dataForm());
  }

  ngOnInit() {
    if (Cache.get('ping')) {
      this.feedBack.schoolDetails = Cache.get('ping');
      this.feedBack.payments = Cache.get('ping')['settings']['payment_method'];
    } else {
      this.feedBack.schoolDetails = Cache.get('app_settings');
      this.feedBack.payments = Cache.get('app_settings')['settings']['payment_method'];
    }
    this.loadFunction();
    this.getPaymentHistory();
  }

  private loadFunction() {
    // this.getStudentFee();
    this.feedBack.enablePartPayment = Cache.get('ping')['settings']['enable_part_payment'];
    this.feedBack['payment_school_fee_auto_fill'] = Cache.get('ping')['settings']['payment_school_fee_auto_fill'];
    this.feedBack['payment_school_fee_multiple'] = Cache.get('ping')['settings']['payment_school_fee_multiple'];
    if (Cache.get('cbsp_student_profile')) {
      this.authStudent = Cache.get('cbsp_student_profile');
      console.log('Authenticated Student ',this.authStudent);
      if (this.authStudent.registrations && this.authStudent.registrations.length) {
        this.studentRegistrations = this.authStudent.registrations;
      }
    }
    if (Cache.get('cbsp_student_profile')['course_of_study']) {
      this.feedBack.allLevel = Cache.get('cbsp_student_profile')['course_of_study']['degree']['programme']['levels'];
    } else {
      this.getStudent();
    }
  }


  getStudentFee() {
    this.feedBack.loader = true;
    this.studentServiceService.getStudentFee()
      .subscribe(response => {
          console.log('payment :: ', response);
          this.feedBack.allResponse = response.paidFee;
          this.feedBack.loader = false;
        },
        error => {
          this.feedBack.loader = false;
          if (error.message) {
            this.notification.error(error['message']);
          } else {
            this.notification.error('Unable to load Payment Fee');
          }

        });
  }

  public onSelectFeeParams() {
    this.feedBack.allFees = [];
    // if (level !== '' && session !== '') {
    this.feedBack.loader = true;
    this.studentServiceService.getStudentFeeByLevelSession(this.registration).subscribe(response => {
        this.fee_details = response;
        // console.log(response);
        const feesToPay: any[] = [];
        const allFees: any[] = [];
        const transactIds: any[] = [];
        this.levelId = response.level.id;
        this.sessionId = response.session.id;
        const allTransactionItems: any[] = [];
        const responseFeesObject = response.fees;
        let responseFees = [];
        let keys = [];
        for (const key in responseFeesObject) {
          responseFees.push(responseFeesObject[key]);
          keys.push(key);
        }
        this.allFeeKeys = keys;
        let pre_filled_fees = 0;
        let semesterFee = [];
        responseFees.forEach((responseFee) => {
          responseFee.paidFee.forEach((paid_fee) => {
            paid_fee.student_transaction_items.forEach((fee) => {
              allTransactionItems.push(fee);
              transactIds.push(fee.fee_id.toString());
            });
          });
        });
        responseFees.forEach((responseFee, semesterIndex) => {
          responseFee.feesToPay.forEach((feeToPay) => {
            if (!feeToPay.pay_in_full) {
              feeToPay.randomId = generateRandomString(8) + feeToPay.id;
            }
            if (feeToPay.amount_remaining && feeToPay.amount_remaining > 0) {
              feeToPay.semester = keys[semesterIndex].toString();
              semesterFee.push(feeToPay);
            }
            if (feeToPay.pay_in_full.toString() === '1' && feeToPay.amount_remaining && feeToPay.amount_remaining > 0) {
              pre_filled_fees += +feeToPay.amount_remaining;
              const tempo = {
                fee_id: +feeToPay.id,
                amount: +feeToPay.amount_remaining,
                semester: keys[semesterIndex].toString()
              };
              this.feeItem_Id.push(tempo);
              // this.feeInfo[semesterIndex].submit = false;
              this.feeToBeDisplayed.push({...tempo, ...{fee_name: feeToPay['fee_item']['name']}});
            }
          });
          allFees.push(semesterFee);
          this.feeInfo.push({
            total: pre_filled_fees,
            submit: false,
            loading: false
          });
          semesterFee = [];
        });
        // console.log(responseFees);
        this.feedBack.allFees = allFees;
        this.noContentMessage = (allFees.length === 0) ? 'All fees for this session have been paid.' : 'No Content Found';
        this.feedBack.loader = false;
      },
      error => {
        this.feedBack.loader = false;
        this.notification.error('Unable to load Payment Fee', error);
      },
      () => {
        // console.log('On Init fee Items ', this.feeItem_Id);
      }
    );
    // }

  }

  public validateForm(index) {
    if (!this.feeInfo[index].submit || this.createForm.invalid || !this.feeItem_Id.length) {
      if (this.createForm.get('payment_method').invalid) {
        return this.createForm.get('payment_method').markAsDirty();
      }
      return;
    } else if (this.createForm.value['payment_method'] === 'Manual') {
      this.payManual(index);
    } else {
      this.createFormModal(index);
    }
  }

  getPayFee(id) {
    this.studentServiceService.getPaidFee(id)
      .subscribe(response => {
          // console.log('fee item :: ', response);
        },
        error => {
          this.notification.error('Unable to load Payment Fee');
        });
  }

  getStudentDetails() {
    this.feedBack.loader = true;
    this.studentServiceService.getStudentCourseList()
      .subscribe((response) => {
          // console.log('response students :: ', response);
          //  this.feedBack.allResponse = response;
          const currentSession = response.current.session_year;
          // this.feedBack.allResponse = response;
          this.feedBack.allResponse['all_reg'].forEach((val, i) => {
            if (val.session_year === currentSession) {
              this.currentSession.push(val);
              this.feedBack.allResponse['all_reg'].splice(i, 1);
            }
          });
          this.dtTrigger.next();
          this.feedBack.loader = false;
        },
        error => {
          this.feedBack.loader = false;
          this.notification.error('Unable to load Student Course(s), please reload', error);
        });
  }

  getPaymentHistory() {
    this.feedBack.load_fee = true;
    this.studentServiceService.getPaymentHistory(this.feedBack.payment_status).subscribe((response) => {
        this.feedBack.load_fee = false;
        // console.log('Payment History Success', response);
        this.paymentHistory = response;
        this.paymentHistory = this.paymentHistory.reverse();
        this.paymentHistory.forEach(payment => {
          if (payment.student_transaction.length >= 0) {
            payment['sub_total'] = payment.student_transaction.reduce((a, b) => a + parseInt(b.amount, 10), 0);
          }
        });
        console.info(this.paymentHistory);
        this.sessionPaymentHistory = this.paymentHistory[0];
        this.refresh();
      },
      (error) => {
        this.feedBack.load_fee = false;
        this.notification.error('Could not load Payment History');
        // console.log('Payment History Error', error);
      });
  }

  onViewCourse(id) {
    Cache.set('student_id', id);
    this.router.navigate(['../student/reg/course']);
  }

  onStudentReg() {
    this.visibleCustom = true;
    this.onSelectFeeParams();
    if (Cache.get('cbsp_student_profile')['course_of_study']) {
      this.sessionsArray = Cache.get('cbsp_student_profile')['course_of_study']['degree']['programme']['sessions'];
    } else {
      this.getStudent();
    }
    $('#stdRegModal').modal('show');
  }

  getStudent() {
    this.studentServiceService.getStudentProfile()
      .subscribe((response) => {
          Cache.set('cbsp_student_profile', response);
          this.sessionsArray = Cache.get('cbsp_student_profile')['course_of_study']['degree']['programme']['sessions'];
          this.feedBack.allLevel = Cache.get('cbsp_student_profile')['course_of_study']['degree']['programme']['levels'];
        },
        error => {
          // console.log('error from profile ::', error);
          //  this.notification.error('Unable to load student Profile, please reload');
        });
  }

  public payManual(index) {
    this.feeInfo[index].loading = true;
    this.submitStatus = true;
  }

  createFormModal(i: number) {
    let semester = this.allFeeKeys[i];
    if (this.createForm.value['payment_method'].toLowerCase() === 'manual' && !this.feedBack.file_is_valid) {
      return this.notification.error('Image must be either .png, .jpg or .jpeg and must be less than 200kb');
    }
    this.feeInfo[i].loading = true;
    this.submitStatus = true;
    const feeItemsPerSemester = this.feeItem_Id.filter(fee => {
      return fee.semester == i ;// semester; // `0${i}`;
    });
    const feeItemsThatAreSpread = this.feesThatAreAlreadySpread.filter(fee => {
      return fee.semester == i;//semester;// `0${i}`;
    });
    // console.log('Fee items that were already spread!! ', this.feesThatAreAlreadySpread);
    // this.createForm.value.fee_items = this.feeItem_Id.concat(this.feesThatAreAlreadySpread);
    this.createForm.value.fee_items = feeItemsPerSemester.concat(feeItemsThatAreSpread);
    this.createForm.value['level_id'] = this.levelId;
    this.createForm.value['session_id'] = this.sessionId;
    this.createForm.value['semester'] = semester; //`0${i}`;
    // this.createForm.value['semester'] = this.feedBack.allFees.length - (i + 1);
    this.createForm.value['etransact_response_url'] = `http://${window.location.host}/pay-transact`;
    this.createForm.value['express_response_url'] = `http://${window.location.host}/pay-transact`;
    this.etransactObject['submitting'] = true;
    this.studentServiceService.makePayment(this.createForm.value).subscribe((response) => {
        this.submitStatus = false;
        /*const i = {
          "amount": "3000.0000",
          "callback-url" : null,
        "currency": "NGN",
        "customer-email" : "ruthlaraba01@gmail.com",
        "hash": "80005af20b6e7d09c46a4221f67233773d9b74cce038904e2a1ffed56b481613",
        "hash-type": "SHA256",
        "merchant-id": "00001",
        "product-desc": "School Fees Payment",
        "product-id": "123",
        "public-key": "1d8c210a3b1a5d32496204618cf5bd5a",
        "trans-id": "3061154896134352"
        };*/
        this.paymentHistory = this.paymentHistory.concat(response);
        this.feeInfo[i].total = 0;
        this.processPaymentByMethod(this.createForm.value['payment_method'], response);
        this.showCustomInput = false;
        this.etransactObject['submitting'] = false;
        this.feeInfo[i].loading = false;
        this.createForm.reset();
        this.selectedId = this.feeItem_Id = [];
      },
      error => {
        this.etransactObject['submitting'] = false;
        // console.log('error from course :: ', error);
        this.submitStatus = false;
        this.showCustomInput = false;
        this.feeInfo[i].loading = false;
        this.etransactObject['submitting'] = false;
        this.notification.error('Unable to make payment, please try again', error);
      });
  }

  public onSelectFee(id, amount, fee, semesterIndex) {
    const index = this.selectedId.indexOf(id);
    if (this.selectedId.length === 0) {
      this.feeInfo[semesterIndex].submit = false;
    }
    if (index > -1) {
      this.selectedId.splice(index, 1);
      this.feeItem_Id.splice(index, 1);
      this.feeInfo[semesterIndex].total -= +amount;
    } else {
      this.feeInfo[semesterIndex].submit = true;
      this.feeInfo[semesterIndex].total += +amount;
      this.selectedId.push(id);
      this.feeItem_Id.push({fee_id: +id, amount: +amount, semester: semesterIndex});
    }
  }

  public removeIfExist(id, amount, fee, semesterIndex) {
    this.selectedId = [];
    this.feeItem_Id = [];
    this.feeToBeDisplayed = [];
    for (let i = 0; i < this.feeInfo.length; i++) {
      this.feeInfo[i].submit = false;
      this.feeInfo[i].total = 0;
    }
    this.feeInfo[semesterIndex].submit = true;
    this.feeInfo[semesterIndex].total += +amount;
    this.selectedId.push(id);
    this.feeItem_Id.push({fee_id: +id, amount: +amount, semester: semesterIndex});
    this.feeToBeDisplayed.push({...{fee_id: +id, amount: +amount, semester: semesterIndex}, ...{fee_name: fee['fee_item']['name']}});
    /*if (index > -1) {
          this.selectedId.splice(index, 1);
          this.feeItem_Id.splice(index, 1);
          this.feeInfo[semesterIndex].total -= +amount;
        } else {
          this.feeInfo[semesterIndex].submit = true;
          this.feeInfo[semesterIndex].total += +amount;
          this.selectedId.push(id);
          this.feeItem_Id.push({fee_id: +id, amount: +amount, semester: semesterIndex});
        }*/
  }

  public onSelectPartFee(feeId, feeAmount, i, ind, fee) {
    const inputElement = $(`#part_pay${i}${ind}`);
    const index = this.selectedId.indexOf(feeId);
    const inputValue = inputElement.val();
    if (this.feedBack.payment_school_fee_auto_fill.toString() === '1') {
      this.enterCustomAmount(i, false);
    }
    // console.log('Input value ', inputValue);
    if (!inputValue) {
      if (index > -1) {
        this.selectedId.splice(index, 1);
        this.feeItem_Id.splice(index, 1);
      }
      return;
    }
    if (this.selectedId.length === 0) {
      this.feeInfo[i].submit = false;
    }

    if (parseInt(inputValue) > parseInt(feeAmount)) {
      inputElement.val(null);
      return this.notification.warning(`Input value is greater than fee amount!`);
    }
    if (index > -1) {
      if (!inputElement.val()) {
        this.selectedId.splice(index, 1);
        this.feeItem_Id.splice(index, 1);
        this.feeToBeDisplayed.splice(index, 1);
      } else {
        this.feeInfo[i].total -= this.feeItem_Id[ind].amount;
        setTimeout(() => {
        }, 0);
        this.feeItem_Id[ind].amount = +inputElement.val();
        this.feeToBeDisplayed[ind].amount = +inputElement.val();
        this.feeInfo[i].total += +inputElement.val();


      }

      // this.feeInfo[i].total -= +inputValue;
    } else {
      this.feeInfo[i].submit = true;
      this.feeInfo[i].total += +inputValue;
      const temp_fee_obj = {fee_id: +feeId, amount: +inputValue, semester: i};
      this.feeItem_Id.push(temp_fee_obj);
      this.feeToBeDisplayed.push({...temp_fee_obj, ...{fee_name: fee['fee_item']['name']}});
      this.selectedId.push(feeId);
      // alert(this.feeInfo[i].submit);
      // console.log('Selected Id ', this.selectedId);
      // console.log('Fee Item ID ', this.feeItem_Id);
      // console.log('cnolq;ewfpiikqw ', this.feeToBeDisplayed);

    }
  }

  public onViewItem(fee) {
    // console.log(fee);
    this.feedBack.viewDetails = fee;
    this.feedBack.viewDetails['session_year'] = this.sessionPaymentHistory['session_year'];
    $('#viewModal').modal('show');
  }

  public processPaymentByMethod(payment_method, data) {
    this.feeItem_Id = [];
    this.feesThatAreAlreadySpread = [];
    const paymentDetails = {
      school_logo: this.feedBack.schoolDetails.settings.logo,
      school_name: this.feedBack.schoolDetails.name,
      ...this.authStudent,
      feeItems: this.feeToBeDisplayed.concat(this.feeToBeDisplayedEmeka),
      fee_level: this.fee_details.level.name,
      fee_session: this.fee_details.session.year
    };
    Cache.set('paymentDetails', paymentDetails);
    this.triggerModalOrOverlay('close', 'stdRegModal');
    Cache.set('RETURN_URL', window.location.href);
    switch (payment_method.toLowerCase()) {
      case 'paystack':
        this.transactionFee = data['response'].transaction_fee;
        this.paystackRef = data['response'].transaction_id;
        // console.log('paystack data response', data['response']);
        // payButton.attr('href', data['response'].authorization_url);
        const payButton = $('#paystack');
        setTimeout(() => {
          payButton[0].click();
        }, 10);
        break;
      case 'custom':
        if (typeof(data) === 'string') {
          this.notification.success(data);
        } else {
          this.notification.success('Payment Transaction was successful');
        }
        // this.triggerModalOrOverlay('close', 'stdRegModal');
        break;
      case 'paychoice':
        const transactionObject = {
          tid: data['transaction_id'],
          tcost: data['transaction_cost'],
          url: data['url'],
        };
        const encryptedTransaction = CryptoJS['AES'].encrypt(JSON.stringify(transactionObject), SECRET_PAYMENT_KEY);
        this.router.navigate(['/pay-transact', {paychoice: encryptedTransaction}]);
        // this.triggerModalOrOverlay('close', 'stdRegModal');
        break;

      case 'ebillspay':
        this.ebillsTransactionId = data.response;
        this.ebillsTransactionCost = (data.transaction_cost) ? +data.transaction_cost : 0;
        data.items.forEach(item => {
          this.ebillsAmount += item.amount;
        });
        this.triggerModalOrOverlay('close', 'stdRegModal');
        break;

      case 'etransact':
      case 'etranzact':
        this.etransactObject = data;
        this.etransactObject['show'] = true;
        this.triggerModalOrOverlay('close', 'stdRegModal');
        break;
      case 'xpresspay':
      case 'expresspay':
        this.xpresspayObject = data;
        this.xpresspayObject['show'] = true;
        this.triggerModalOrOverlay('close', 'stdRegModal');
        break;
      case 'manual':
        this.notification.success('Payment has been sent for authorization. Check back later');
        break;
      default:
        this.notification.warning('No Payment Transacted!');
    }

  }

  public printSlip(id, title) {
    this.feedBack.print_loader = true;
    const that = this;
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-1000000px', 'pointer-events': 'none'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    // frameDoc.document.write('<html><head><title>' + title + '</title>');
    frameDoc.document.write('</head><body>');
    frameDoc.document.write('<link href="./paymentComponent.css" rel="stylesheet">');
    frameDoc.document.write('<style>.water-mark {-webkit-filter: grayscale(100%);filter: grayscale(100%);opacity: .095;position: absolute;top: 0;right: 0;left: 0;bottom: 0;} .relative {position: relative;}</style>');
    frameDoc.document.write('<style>.img-div {width: 100px;height: 100px;margin: 0 auto;}.img-div img {max-width: 100%;max-height: 100px;display: block;margin: auto;} .text-center {text-align: center !important;} .font-weight-bold {font-weight: 700 !important; } .my-3 {margin-top: 1rem !important; }</style>');
    frameDoc.document.write('<style>.table {width: 100%; border-collapse: collapse !important; }.table td,.table th {background-color: #fff !important; }.table-bordered th,.table-bordered td {border: 1px solid #ddd !important; } .text-left{text-align:left}.float-right{float:right!important}.float-left{float: left!important;}</style>');
    frameDoc.document.write('<style>table-bordered{border:1px solid #ddd}.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>tbody>tr>td,.table-bordered>tfoot>tr>td{border:1px solid #ddd}.table-bordered>thead>tr>th,.table-bordered>thead>tr>td{border-bottom-width:2px}</style>');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    setTimeout(function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      frame1.remove();
      that.feedBack.print_loader = false;
    }, 3000);
  }

  public paystackPaymentCancel() {
    this.notification.info('You aborted your payment process!');
    this.triggerModalOrOverlay('close', 'createPayment');
  }

  public paystackPaymentDone(event) {
    // console.log('Payment event ', event);
    this.triggerModalOrOverlay('close', 'createPayment');
    this.router.navigate(['/pay-transact', event]);
  }

  public setPaymentLogo(paymentMethod) {
    this.paymentLogo = this.paymentLogoUrls[paymentMethod.toLowerCase()];

  }

  public payEtransactNow() {
    this.etransactObject['paying'] = true;
    this.el.nativeElement.querySelector('form#etransact_submit').submit();
  }

  public payXpressPayNow() {
    this.xpresspayObject['paying'] = true;
    this.el.nativeElement.querySelector('form#xpresspay_submit').submit();
    this.xpresspayObject['paying'] = false;
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    if (ind) {
      $('#iframe').remove();
    }
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

  public setSemesterName(fees: any[]): string {
    let feeDescription = 'General Fees';
    if (fees.length) {
      switch (parseInt(fees[0].semester, 10)) {
        case 0:
          feeDescription = 'General Fees';
          break;
        case 1:
          feeDescription = '1st Semester Fees';
          break;
        case 2:
          feeDescription = '2nd Semester Fees';
          break;
        case 3:
          feeDescription = '3rd Semester Fees';
          break;
        default:
          feeDescription = 'New Semester Fees';
      }
    }
    // console.log('Fees description semester',fees[0].semester);
    return feeDescription;
  }

  public reRender(event): void {
    this.feedBack.payment_status = event.target.value;
    // Show the requery button only if the payment status is pending
    this.reQuery.show = (event.target.value.toUpperCase() === 'PENDING');
    // (event.target.value.toUpperCase() === 'PENDING') ? this.reQuery.show = true : this.reQuery.show = false;
    this.getPaymentHistory();
  }

  public changeSession(event) {
    const session_index = event.target.value;
    this.refresh();
    this.sessionPaymentHistory = this.paymentHistory[session_index];
  }

  private refresh() {
    if (this.sessionPaymentHistory['student_transaction'] && this.sessionPaymentHistory['student_transaction'].length >= 10) {
      if (this.dtElement.dtInstance) {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy();
        });
      }
      this.dtTrigger.next();
    }
  }

  public makeReQuery(transaction_id, index, etranzact?) {
    // const url = this.reQuery.url + transaction_id;
    this.sessionPaymentHistory['student_transaction'][index]['loading'] = true;
    this.studentServiceService.makeReQuery(transaction_id, etranzact).subscribe(res => {
      this.sessionPaymentHistory['student_transaction'][index]['loading'] = false;
      if (res.message) {
        this.notification.info(res.message);
        this.getPaymentHistory();
      } else {
        this.notification.warning('Please retry');
      }
    }, err => {
      this.notification.error('Could not make ReQuery', err);
      this.sessionPaymentHistory['student_transaction'][index]['loading'] = false;
    });
  }

  public setTotalAmount(): number {
    let totalFeeAmount = 0;
    this.feeItem_Id.forEach(feeItem => {
      totalFeeAmount += feeItem.amount;
    });

    return totalFeeAmount;
  }

  public enterCustomAmount(i, visible = true) {
    const el = document.getElementById('show_spread');
    if (el && el['checked']) {
      this.feedBack.allFees[i].forEach(fee => {
        fee.disable = visible;
        this.showCustomInput = visible;
      });
    }
    if (!visible) this.visibleCustom = visible;
    // else {
    //   this.feedBack.allFees[i].forEach(fee => {
    //     fee.disable = false;
    //     this.showCustomInput = false;
    //   });
    // }

  }

  public spreadAmount(i, fees: any[]) {
    // this.feeItem_Id = [];
    this.feeToBeDisplayedEmeka = [];

    // this.feeToBeDisplayed = [];
    this.feesThatAreAlreadySpread = [];
    // console.log("Feeeeeeeeesssssss ", fees);
    const inputAmount = parseInt($(`#custom_amount${i}`).val(), 10);
    this.feeInfo[i].total = inputAmount;
    let payInFullAmount = 0;
    let totalAmount = 0;
    this.feedBack.allFees[i].forEach(fee => {
      totalAmount += fee.amount_remaining;
      if (fee.pay_in_full) {
        payInFullAmount += fee.amount_remaining;
      }
    });
    let spreadableAmount = inputAmount - payInFullAmount;
    if (spreadableAmount < 0) {
      $(`#custom_amount${i}`).val(null);
      this.feeInfo[i].total = 0;
      return this.notification.warning('Total amount must be up to compulsory fees!');
    }
    if (inputAmount > totalAmount) {
      $(`#custom_amount${i}`).val(null);
      this.feeInfo[i].total = 0;
      return this.notification.warning('Amount is greater than your fees!');
    }
    let totalRequiredAmount = 0;
    let totalOptionalAmount = 0;
    const whole_fee = {
      optional: [],
      required: []
    };
    fees.forEach(fee => {
      $(`#${fee.randomId}`).val(null);
      if (fee.status.toString() === '1') {
        whole_fee.required.push(fee);
        totalRequiredAmount += +fee.amount_remaining;
      } else {
        whole_fee.optional.push(fee);
        totalOptionalAmount += +fee.amount_remaining;
      }
    });
    let alreadyFilledInAmount = 0;
    let incremental_spreading = 0;
    if (totalRequiredAmount > inputAmount) {
      const requiredSpreadPercentage = (spreadableAmount / (totalRequiredAmount - payInFullAmount)); // requiredAmountToSpreadOn
      // console.log('fees to be spread', fees);
      whole_fee.required.forEach(fee => {
        if (fee.randomId) {
          let requiredFeeValue = requiredSpreadPercentage * fee.amount_remaining;
          requiredFeeValue = +Number(requiredFeeValue).toFixed(2);
          incremental_spreading += requiredFeeValue;
          // requiredFeeValue = Math.round(requiredFeeValue);
          $(`#${fee.randomId}`).val(requiredFeeValue);
          const temp_fee_obj = {fee_id: +fee.id, amount: +requiredFeeValue, semester: i};
          alreadyFilledInAmount += +requiredFeeValue;
          this.feesThatAreAlreadySpread.push(temp_fee_obj);
          this.feeToBeDisplayedEmeka.push({...temp_fee_obj, ...{fee_name: fee['fee_item']['name']}});
        }
      });
    }
    if (totalRequiredAmount <= inputAmount) {
      whole_fee.required.forEach(fee => {
        if (fee.randomId) {
          $(`#${fee.randomId}`).val(fee.amount_remaining);
          // let fee.amount_remaining = requiredSpreadPercentage * fee.amount_remaining;
          fee.amount_remaining = +Number(fee.amount_remaining).toFixed(2);
          incremental_spreading += fee.amount_remaining;
          // fee.amount_remaining = Math.round(fee.amount_remaining);
          const temp_fee_obj = {fee_id: +fee.id, amount: +fee.amount_remaining, semester: `0${i}`};
          alreadyFilledInAmount += +fee.amount_remaining;
          this.feesThatAreAlreadySpread.push(temp_fee_obj);
          this.feeToBeDisplayedEmeka.push({...temp_fee_obj, ...{fee_name: fee['fee_item']['name']}});
        }
      });
      ///////////////////
      const optionalSpreadPercentage = ((inputAmount - totalRequiredAmount) / totalOptionalAmount); // requiredAmountToSpreadOn
      whole_fee.optional.forEach(fee => {
        let optionalFeeValue = optionalSpreadPercentage * fee.amount_remaining;
        optionalFeeValue = +Number(optionalFeeValue).toFixed(2);
        incremental_spreading += optionalFeeValue;
        // optionalFeeValue = Math.round(optionalFeeValue);
        $(`#${fee.randomId}`).val(optionalFeeValue);
        const temp_fee_obj = {fee_id: +fee.id, amount: +optionalFeeValue, semester: `0${i}`};
        this.feesThatAreAlreadySpread.push(temp_fee_obj);
        this.feeToBeDisplayedEmeka.push({...temp_fee_obj, ...{fee_name: fee['fee_item']['name']}});
      });
    }
    if (this.feesThatAreAlreadySpread.length) {
      this.feesThatAreAlreadySpread[0].amount += +(spreadableAmount - incremental_spreading);
      this.feeToBeDisplayedEmeka[0].amount += +(spreadableAmount - incremental_spreading);
    }
    this.feeInfo[i].submit = true;
  }

  public validateImage(img_input, event) {
    const $linkId = $(`#${img_input}`);
    const $newLinkId = $linkId.val();
    const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
    $linkId.removeClass('alert alert-danger animated rubberBand');
    if (file_type && (file_type !== '.jpg') && (file_type !== '.png') && (file_type !== '.jpeg')) {
      $linkId.addClass('alert alert-danger animated rubberBand');
      this.notification.error('You can only upload jpg, jpeg or png files');
      this.feedBack.file_is_valid = false;
    } else {
      this.pay_picture = event.target.files[0];
      if (this.pay_picture.size > 200 * 1024) {
        this.notification.error('Maximum allowed image size is 200kb');
        this.feedBack.file_is_valid = false;
      } else {
        const reader = new FileReader();
        reader.onloadend = () => {
          this.createForm.value['receipt'] = reader.result;
        };
        reader.readAsDataURL(this.pay_picture);
        this.feedBack.file_is_valid = true;
      }
    }
  }

  public setRegistration(regis) {
    /*if (regis === 'jintao') {
      this.registration.session_id = 17;
      this.registration.level_id = 4;
    }*/
    if (regis !== 'current') {
      if (this.studentRegistrations) {
        this.studentRegistrations.forEach(reg => {
          if (reg.session_id.toString() === regis.toString()) {
            this.registration.session_id = regis;
            this.registration.level_id = reg.level_id;
          }
        });
      }
    }
  }

}
