import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ScriptLoaderService} from '../../../../_services/script-loader.service';
import {Helpers} from '../../../../../utils/helpers';
import {UserService} from "../../../../../services/user.service";
import {Cache} from "../../../../../utils/cache";
import {StudentServiceService} from "../../../../../services/api-service/student-service.service";
import {NotificationService} from "../../../../../services/notification.service";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})

export class HomePageComponent implements OnInit {
  public user_details: any;
  public school_details: any;
  public dashboardData: any;
  public admissionLetter: any;
  private studentFee: any;
  public loader = {
    loading: false
  };

  constructor(private userService: UserService,
              private studentServiceService: StudentServiceService,
              private notification: NotificationService) {
  }

  ngOnInit() {
    this.user_details = Cache.get('cbsp_student_profile');
    this.school_details = Cache.get('ping') || Cache.get('app_settings');
    this.getStudentDashboard();
    this.checkForAdmissionLetter();
    this.getStudentFee();
  }

  private getStudentFee() {
    this.studentServiceService.getStudentFee().subscribe(res => {
      this.studentFee = res.fees;
    }, err => {
      // do something here later
    });
  }

  private checkForAdmissionLetter() {
    this.studentServiceService.checkForAdmissionLetter().subscribe((res) => {
        let keywords = [];
        let letter = '';
      if(res['letter']){
          keywords = res['letter']['body'].match(/{{(.*?)}}/g);
          letter = res['letter']['body'];
      }

      keywords.forEach(keyword => {
        switch (keyword) {
          case '{{studentName}}':
            letter = letter.replace(keyword, this.user_details.last_name + ' ' + this.user_details.first_name + ' ' + this.user_details.other_names);
            break;
          case '{{facultyName}}':
            letter = letter.replace(keyword, this.user_details.course_of_study.department.faculty.name);
            break;
          case '{{departmentName}}':
            letter = letter.replace(keyword, this.user_details.course_of_study.department.name);
            break;
          case '{{courseOfStudyName}}':
            letter = letter.replace(keyword, this.user_details.course_of_study.name);
            break;
          case '{{admissionDate}}':
            letter = letter.replace(keyword, new Date().toDateString());
            break;
          case '{{regNo}}':
            letter = letter.replace(keyword, this.user_details.matric_no);
            break;
          case '{{schoolLogo}}':
            letter = letter.replace(keyword, '<img src="' + this.school_details['settings']['logo'] + '">');
            break;
          case '{{registrarSignature}}':
            letter = letter.replace(keyword, '<img src="' + this.school_details['settings']['registrar_signature'] + '" width="100px" height="auto">');
            break;
          case '{{registrarName}}':
            letter = letter.replace(keyword, this.school_details['settings']['registrar_name']);
            break;
          case '{{feeAmount}}':
            letter = letter.replace(keyword, res.fee);
            break;
        }
      });
      this.admissionLetter = letter;
      // this.prepareLetter();
    }, (err) => {
      // this.notification.error()
    });
  }

  public prepareLetter() {
    this.loader.loading = true;
    const that = this;
    const frame1 = $('<iframe />');
    frame1[0]['id'] = 'print_frame';
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-100000', 'z-index': '-239'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    frameDoc.document.write('<html style="background-color: #fff3cd"><head><title>Admission Letter</title>');
    frameDoc.document.write('<style>.water-mark {\n' +
      '  -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */\n' +
      '  filter: grayscale(100%);\n' +
      '  opacity: .095;\n' +
      '  position: absolute;\n' +
      '  top: 50%;\n' +
      '  right: 0;\n' +
      '  left: 0;\n' +
      '  bottom: 0;\n' +
      '  transform: translateY(-50%);\n' +
      '}</style>');
    frameDoc.document.write('<style> p {margin: 0.5em !important;} </style>');
    frameDoc.document.write('</head><body>');
    // frameDoc.document.write('<link href="/assets/css/style.bundle.css" rel="stylesheet">');
    frameDoc.document.write('<div style="position: relative; padding: 20px; font-size: 85%">');
    frameDoc.document.write('<img src="' + this.school_details['settings']['logo'] + '" class="water-mark" width="100%">');
    frameDoc.document.write(this.admissionLetter);
    frameDoc.document.write('</div>');
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    setTimeout(function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      frame1.remove();
      that.loader.loading = false;
    }, 3000);
  }

  private getStudentDashboard() {
    this.studentServiceService.getStudentDashboard()
      .subscribe((dashboardResponse) => {
          this.dashboardData = dashboardResponse.results;
          // console.log('Student dashboard data :: ', dashboardResponse);
          // this.feedBack.allResponse = response;
        },
        error => {
          this.notification.error('Could not load student dashboard data ', error);
        });
  }


}
