import {Component, OnInit, ViewChild} from "@angular/core";
import {StudentServiceService} from "../../../../../services/api-service/student-service.service";
import {NotificationService} from "../../../../../services/notification.service";
import {Cache} from "../../../../../utils/cache";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs/Subject";
import {DataTableDirective} from "angular-datatables";

declare const $: any;

@Component({
  selector: 'app-exeat',
  templateUrl: './exeat.component.html',
  styleUrls: ['./exeat.component.css']
})
export class ExeatComponent implements OnInit {

  exeatForm: FormGroup;
  updateExeatForm: FormGroup;
  selectedExeatRequest;
  exeatRequest: any[];
  exeatRequestId;
  // student_id;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  //noinspection TypeScriptValidateTypes
  dtTrigger: Subject<any> = new Subject();
  loading = {
    listing: false,
    creating: false,
    updating: false
  };
  idObject = {
    student_id: null
  };

  static requestExeat = function () {
    return {
      purpose: ['', Validators.compose([Validators.required])],
      destination: ['', Validators.compose([Validators.required])],
      who_to_visit: ['', Validators.compose([Validators.required])],
      who_to_visit_relationship: ['', Validators.compose([Validators.required])],
      who_to_visit_contact: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(15)])],
      start_date: ['', Validators.compose([Validators.required])],
      end_date: ['', Validators.compose([Validators.required])],
    }
  };

  static editExeatRequest = function () {
    return {
      student_id: Cache.get('cbsp_student_profile').id,
      purpose: ['', Validators.compose([Validators.required])],
      destination: ['', Validators.compose([Validators.required])],
      who_to_visit: ['', Validators.compose([Validators.required])],
      who_to_visit_relationship: ['', Validators.compose([Validators.required])],
      who_to_visit_contact: ['', Validators.compose([Validators.required])],
      start_date: ['', Validators.compose([Validators.required])],
      end_date: ['', Validators.compose([Validators.required])],
    }
  };

  constructor(private studentService: StudentServiceService,
              private notification: NotificationService,
              private fb: FormBuilder) {
    this.exeatForm = this.fb.group(ExeatComponent.requestExeat());
    this.updateExeatForm = this.fb.group(ExeatComponent.editExeatRequest());
  }

  ngOnInit() {
    if (Cache.get('cbsp_student_profile')) {
      this.idObject.student_id = Cache.get('cbsp_student_profile').id;
    }
    this.getAllExeats();
  }

  requestExeat() {
    this.loading.creating = true;
    const obj = Object.assign(this.exeatForm.value, this.idObject);
    this.studentService.exeatRequest(obj)
        .subscribe(response => {
              this.exeatRequest = [];
              this.getAllExeats();
              this.loading.creating = false;
              this.notification.success('Exeat request successfully made. Please check the status later');
            },
            error => {
              this.loading.creating = false;
              this.notification.error('Oh Snap, something happened', error);
              console.log('Oh Snap, something happened', error);
            }
        )
  }

  getAllExeats() {
    this.loading.listing = true;
    this.studentService.getAllExeatRequests(this.idObject.student_id)
        .subscribe(exeatResponse => {
              this.loading.listing = false;
              console.log('Exeat response from serve', exeatResponse);
              this.exeatRequest = exeatResponse;
              if (this.exeatRequest.length > 0) {
                this.dtTrigger.next();
              }
            },
            error => {
              this.loading.listing = false;
              this.notification.error('Could not load exeats. Please try again');
              console.log('Get exeat error', error);
            })
  }

  updateExeatRequest() {
    this.loading.updating = true;
    console.log('Exeat request form update', this.updateExeatForm.value);
    this.studentService.updateExeatRequest(this.exeatRequestId, this.updateExeatForm.value)
        .subscribe((response) => {
          this.triggerModalOrOverlay('close', 'updateExeatRequest');
          this.loading.updating = false;
          this.notification.success('Successfully updated request');
          this.rerender();
          console.log('Exeat request update response', response)
        },
        (error =>{
          this.triggerModalOrOverlay('close', 'updateExeatRequest');
          this.loading.updating = false;
          this.notification.error('Something went wrong, please try again');
          console.log('Exeat request update error', error);
        }));
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      // this.dtTrigger.next();
      this.getAllExeats();
    });
  }

  openSesame(ind){
    this.selectedExeatRequest = this.exeatRequest[ind];
    if(this.selectedExeatRequest.status != 2){
      this.notification.warning('You cannot edit this exeat request again');
    }
    else
      this.triggerModalOrOverlay('open', 'updateExeatRequest');
    this.exeatRequestId = this.exeatRequest[ind].id;
    this.updateExeatForm.patchValue(this.selectedExeatRequest);

  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {

    if (action === 'open') {
      $(`#${modalId}`).modal('show');
    }
    else if (action === 'close') {
      $(`#${modalId}`).modal('hide');
    }

  }
}
