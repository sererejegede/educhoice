import {Component, OnInit} from '@angular/core';
import {StudentServiceService} from '../../../../../services/api-service/student-service.service';
import {NotificationService} from '../../../../../services/notification.service';
import {FormBuilder} from '@angular/forms';
import {UserService} from "../../../../../services/user.service";
import {Cache} from "../../../../../utils/cache";

@Component({
  selector: 'app-student-profile-image',
  templateUrl: './student-profile-image.component.html',
  styleUrls: ['./student-profile-image.component.css']
})
export class StudentProfileImageComponent implements OnInit {
  public profileImg = '';
  public formFile;
  public feedBack = {
    submitStatus: false,
    can_upload: true,
    unread: true,
    setting_passport: false,
    passport_is_set: false,
    profileStatus: true,
    birthStatus: true,
    addressStatus: true,
    kinStatus: true,
    maritalStatus: true,
    allResponse: null
  };

  constructor(private studentServiceService: StudentServiceService,
              private userService: UserService,
              private notification: NotificationService) {
  }

  ngOnInit() {
    this.loadFunction();
    if (Cache.get('cbsp_student_profile')['applicant']) {
      this.profileImg = Cache.get('cbsp_student_profile')['applicant']['profile_image'];
    }
  }


  private loadFunction() {
    this.getStudentDetails();
    // this.onEditProfile();
    if (Cache.get('cbsp_student_profile')) {
      this.feedBack.allResponse = Cache.get('cbsp_student_profile');
      // console.log(this.feedBack.allResponse['passport']);
      this.feedBack.can_upload = !this.feedBack.allResponse['passport'];
      this.setDefaultPassport();
      // console.log('studentertykbv s sjhsjhbs ',this.feedBack.allResponse)
    } else {
      this.getStudentDetails();
    }
  }

  private setDefaultPassport() {
    if (this.feedBack.allResponse['passport']) {
      this.feedBack.can_upload = false;
      return this.profileImg = this.feedBack.allResponse['passport'];
    } else {
      if (this.feedBack.allResponse['gender'] && this.feedBack.allResponse['gender'].toLowerCase() === 'male') {
        this.profileImg = './assets/images/users/blank_male.jpg';
      } else if (this.feedBack.allResponse['gender'] && this.feedBack.allResponse['gender'].toLowerCase() === 'female') {
        this.profileImg = './assets/images/users/blank_female.jpg';
      }
    }
  }

  public getStudentDetails() {
    this.studentServiceService.getStudentProfile()
      .subscribe((response) => {
          // console.log('student profileresponse :: ', response);
          this.feedBack.allResponse = response;
          Cache.set('cbsp_student_profile', response);
          this.setDefaultPassport();
          // console.log(this.feedBack.allResponse['passport']);
          this.feedBack.can_upload = !this.feedBack.allResponse['passport'];
        },
        error => {
          // console.log('error from profile ::', error);
          this.notification.error('Unable to load student Profile, please reload', error);
        });
  }

  public getPhoto() {
    document.getElementById('passport').click();
  }

  public previewImage(event) {
    const reader = new FileReader();
    this.formFile = event.target;
    reader.onload = (e) => {
      // $('#student_photo').attr('src', e.target['result']);
      this.profileImg = e.target['result'];
    };
    reader.readAsDataURL(this.formFile.files[0]);
  }

  public setProfilePic() {
    this.feedBack.setting_passport = true;
    this.studentServiceService.setProfileImage(this.formFile).subscribe(
      (res) => {
        this.feedBack.passport_is_set = true;
        this.feedBack.setting_passport = false;
        this.notification.success('Passport successfully updated');
        this.getStudentDetails();
      }, (err) => {
        // this.feedBack.passport_is_set = true;
        this.feedBack.setting_passport = false;
        this.notification.error('Could not upload. Please try again later', err);
      });
  }


}
