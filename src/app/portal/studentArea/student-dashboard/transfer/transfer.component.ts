import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NotificationService} from "../../../../../services/notification.service";
import {StudentServiceService} from "../../../../../services/api-service/student-service.service";
import {Cache} from "../../../../../utils/cache";
import {SECRET_PAYMENT_KEY} from "../../../../../utils/magic-methods";
import * as CryptoJS from 'crypto-js';
import { Router} from "@angular/router";

declare const $: any;

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferComponent implements OnInit {

  transferForm: FormGroup;
  payTransferFee: FormGroup;
  transfer_id: number;
  transfer_amount: any;
  transferRequest: any[] = [];
  allFaculties: any[] = [];
  allDepartments: any[] = [];
  allCourseOfStudy: any[] = [];
  allLevels: any[] = [];
  transferRequestId;
  loading = {
    creating: false,
    pay: false,
    listing: false,
    faculty: false,
    department: false,
    course: false,
    level: false,
  };

  static requestTransfer = function () {
    return {
      to_course_id: ['', Validators.compose([Validators.required])],
      to_level_id: ['', Validators.compose([Validators.required])]
    }
  };

  static payTransferFeeForm = function () {
    return {
      amount: [''],
      payment_method: ['', Validators.compose([Validators.required])],
    };
  };

  constructor(private studentService: StudentServiceService,
              private notification: NotificationService,
              private router: Router,
              private fb: FormBuilder) {
    this.transferForm = this.fb.group(TransferComponent.requestTransfer());
    this.payTransferFee = this.fb.group(TransferComponent.payTransferFeeForm());
  }

  ngOnInit() {
    this.getAllFaculties();
    this.getAllTransfers();
    this.getSingleCourseOfStudy();
  }

  getAllFaculties() {
    this.loading.faculty = true;
    this.studentService.getAllFaculty()
        .subscribe((response) => {
              this.loading.faculty = false;
              console.log('In All faculties');
              this.allFaculties = response;
              console.log('Faculties', response);
            },
            (error) => {
              this.loading.faculty = false;
              console.log('Something happened::', error);
              this.notification.error('Network error. Please reload page');

            }
        )
  }

  getSingleDepartment($event) {
    this.loading.department = true;
    this.allFaculties.forEach((faculty) => {
      if (faculty.id == $event) {
        this.allDepartments = faculty['departments'];
        this.loading.department = false;
      }
    });
    console.log('Department', this.allDepartments);
  }

  getCourseOfStudy($event) {
    this.loading.course = true;
    this.studentService.getCourseOfStudyByDepartmentId($event)
        .subscribe(response => {
              this.loading.course = false;
              console.log('Course of Study', response);
              this.allCourseOfStudy = response;

            },
            error => {
              this.loading.course = false;
              this.notification.error('Course of Study could not be loaded', error)
            }
        )
  }

  getSingleCourseOfStudy() {
    this.loading.level = true;
    // Cache.get
    if (Cache.get('cbsp_student_profile')) {
      this.loading.level = false;
      this.allLevels = Cache.get('cbsp_student_profile')['course_of_study']['degree']['programme']['levels']
    }
  }

  requestTransfer() {
    this.loading.creating = true;
    this.studentService.transferRequest(this.transferForm.value)
        .subscribe(response => {
              // this.exeatRequest = [];
              this.getAllTransfers();
              this.loading.creating = false;
              this.notification.success('Transfer request successfully made. Please check the status later');
            },
            error => {
              this.loading.creating = false;
              this.notification.error('Could not complete request', error);
              console.log('Oh Snap, something happened', error);
            }
        )
  }

  private getTransferAmount(){
    this.studentService.getDepartmentTransferFee(this.transfer_id)
        .subscribe((amount) => {
          console.log(amount);
          this.transfer_amount = amount
        }, (error) => {
          this.notification.error('Something went wrong. Please retry', error)
        });
  }

  public payTransferFees() {
    this.loading.pay = true;
   NotificationService.setOrGetReturnUrl();
    this.payTransferFee.value['student_transfer_application_id'] = this.transfer_id;
    this.payTransferFee.value['amount'] = this.transfer_amount;
    this.studentService.payTransferFee(this.payTransferFee.value)
        .subscribe((response) => {
              this.loading.pay = false;
              // console.log(response);
              switch(this.payTransferFee.value['payment_method'].toLowerCase()){
                case 'paystack':
                  $('#paystack').attr('href', response['response'].authorization_url)[0].click();
                  break;
                case 'custom':
                  break;
                case'paychoice':
                  // console.log('Emeka\'s response 1', response);
                  const transactionObject = {
                    tid: response['transaction_id'],
                    url: response['url']
                  };
                  // console.log('Emeka\'s response 2', response);
                  const encryptedTransaction = CryptoJS['AES'].encrypt(JSON.stringify(transactionObject), SECRET_PAYMENT_KEY);
                  this.router.navigate(['/pay-transact', {paychoice: encryptedTransaction}]);
                  break;
                default:
                  this.notification.warning('Payment not completed');
              }
              this.triggerModalOrOverlay('close', 'pay_transfer');
              // this.notification.success('Payment Successful');
            },
            error => {
              this.loading.pay = false;
              this.triggerModalOrOverlay('close', 'pay_transfer');
              this.notification.error('Something went wrong. Try again later', error);
              console.log('error ::', error);
            });
  }

  getAllTransfers() {
    this.loading.listing = true;
    this.studentService.getAllTransferRequests()
        .subscribe(transferResponse => {
              this.loading.listing = false;
              console.log('Transfer response from server', transferResponse);
              this.transferRequest = transferResponse;
            },
            error => {
              this.loading.listing = false;
              this.notification.error('Could not load transfers. Please try again');
              console.log('Get transfer error', error);
            })
  }

  // rerender(): void {
  //     this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
  //         // Destroy the table first
  //         dtInstance.destroy();
  //         // Call the dtTrigger to rerender again
  //         // this.dtTrigger.next();
  //         this.getAllTransfers();
  //     });
  // }

  openTransferModal(transfer_id){
    this.transfer_id = transfer_id;
    this.getTransferAmount();
    this.triggerModalOrOverlay('open', 'pay_transfer');
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }

}
