import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {StudentDashboardModule} from './student-dashboard/dashboard.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StudentDashboardComponent} from './student-dashboard/dashboard.component';

const STUDENT_ROUTES: Routes = [];


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    StudentDashboardModule,
    RouterModule.forChild(STUDENT_ROUTES),
  ],
  exports: [
    RouterModule,
  ],
  declarations: [
  ]
})


export class StudentRoutingModule {
}
