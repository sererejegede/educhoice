import {Component, OnInit, AfterViewChecked} from '@angular/core';
import {FormBuilder, FormGroup, Validators, AbstractControl} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../../services/user.service';
import {AuthenticationService} from '../../../../services/authentication.service';
import {NotificationService} from '../../../../services/notification.service';
import {environment} from '../../../../environments/environment';
import {AdmissionsService} from '../../../../services/api-service/admissions.service';
import {StudentServiceService} from '../../../../services/api-service/student-service.service';
import {Cache} from '../../../../utils/cache';
import {LogoutServiceService} from '../../../../services/api-service/login-service.service';
import {SchoolService} from '../../../../services/school.service';
import {EmitPaymentService} from '../../../../services/emit-payment-service';

@Component({
  selector: 'app-student-login',
  templateUrl: './student-login.component.html',
  styleUrls: ['./student-login.component.css']
})
export class StudentLoginComponent implements OnInit {
  public feedback = {
    loadingStatus: false
  };
  public showPassword = {
    student: false,
    applicant: false
  };
  public switch_login = '';
  public logo = '';
  public ping;
  public userType: boolean;
  public appSettings: any = {};
  public loginForm: FormGroup;
  public applicantLoginForm: FormGroup;
  public IsBonafideApplicant: boolean;
  public authenticatedUser: any = {};
  public applicant = {
    hasPaid: false,
    password: null,
    username: null
  };

  static userLoginForm = function () {
    return {
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    };
  };
  static applicantLoginForm = function () {
    return {
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
    };
  };


  constructor(private fb: FormBuilder,
              private Alert: NotificationService,
              private userService: UserService,
              private schoolService: SchoolService,
              private loginService: LogoutServiceService,
              private studentServiceService: StudentServiceService,
              private router: Router,
              private route: ActivatedRoute,
              private authService: AuthenticationService,
              private admissionService: AdmissionsService,
              private emitPaymentService: EmitPaymentService) {

    if (window.location.hostname === environment.DEFAULT_DOMAIN) {
      router.navigateByUrl('/home');
    }
    // this.IsBonafideApplicant = Cache.get('bonafide_applicant');

    this.loginForm = this.fb.group(StudentLoginComponent.userLoginForm());
    this.applicantLoginForm = this.fb.group(StudentLoginComponent.applicantLoginForm());
  }


  ngOnInit() {
    this.getAppSettings();
    this.getPingStudent();
    if (this.userService.getAuthUser()) {
      if (this.userService.getAuthUser()['login']['user_type'] === 'App\\Models\\School\\Student') {
        this.router.navigateByUrl('/student');
      } else {
        this.router.navigateByUrl('/');
      }
    }

    const userIsBonafideApplicant = Cache.get('bonafide_applicant');
    const userIsBeginnerApplicant = Cache.get('beginner');
    this.applicant.username = userIsBonafideApplicant['username'];
    this.applicant.password = userIsBonafideApplicant['password'];
    console.log(userIsBonafideApplicant);
    if ((userIsBonafideApplicant && userIsBonafideApplicant['val'] === 'yes') || (userIsBeginnerApplicant && userIsBeginnerApplicant['val'] === 'yes')) {
      this.flipRegistration('signup');
    }

  }


  public signin() {
    this.feedback.loadingStatus = true;
    this.getAppSettings();
    if (this.userService.getAuthUser()) {
      this.authService.logout().subscribe(
        (logResponse) => {
          this.logStudentIn();
        },
        (error) => {
          // console.log('There was an error :: ', error);
          this.Alert.error(`Unable to to login, please check your credentials and retry`, error);
          this.feedback.loadingStatus = false;
        });
    } else {
      this.logStudentIn();
    }
    // this.setBgImage();
  }

  // private setBgImage() {
  //   console.log(this.ping['settings']['bg_image']);
  //   if (this.ping['settings']['bg_image']) {
  //     const staffStyle = $(`.overlay`);
  //     staffStyle.css({background: `linear-gradient(rgba(0, 0, 0, 0.55), rgba(67, 34, 167, 0.7)), rgba(0, 0, 0, 0.55) url(${this.ping['settings']['bg_image']}) no-repeat center`, backgroundSize: 'cover'});
  //   }
  // }

  public logInApplicant() {
    this.feedback.loadingStatus = true;
    if (localStorage.getItem('biodata')) {
      // localStorage.setItem('biodata', null);
    }
    this.applicantLoginForm.value['user_type'] = 'applicant';
    this.admissionService.applicantLogin(this.applicantLoginForm.value).subscribe(
      (loginResponse) => {
        Cache.remove('bonafide_applicant');
        this.feedback.loadingStatus = false;
        this.userService.setAuthUser(loginResponse.token);
        const applicantId = this.userService.getAuthUser()['login']['user']['id'];
        Cache.set('LOGOUT_URL', '/');

        this.getApplicantProfile(applicantId);
      },
      (error) => {
        this.feedback.loadingStatus = false;
        this.Alert.error('Login Failed. Please check your credentials and retry!', error);
      }
    );
  }


  public flipRegistrationDiv(id: string) {
    if (id === 'signup') {
      const login = $('#m_login');
      login.removeClass('m-login--forget-password');
      login.removeClass('m-login--signin');

      login.addClass('m-login--signup');
      (<any>login.find('.m-login__signup')).animateClass('flipInX animated');
      return;
    } else {
      this.applicant.password = this.applicant.username = null;
      const login = $('#m_login');
      login.removeClass('m-login--forget-password');
      login.removeClass('m-login--signup');
      login.addClass('m-login--signin');
      (<any>login.find('.m-login__signin')).animateClass('flipInX animated');
    }

  }

  public logStudentIn() {
    this.feedback.loadingStatus = true;
    const studentLoginObject = this.loginForm.value;
    studentLoginObject['user_type'] = 'student';
    this.authService.login(studentLoginObject).subscribe(
      (response) => {
        this.feedback.loadingStatus = false;
        this.userService.setAuthUser(response.token);
        this.userType = this.userService.getAuthUser();
        // console.log('Auth user token ', this.userService.getAuthUser());
        // ['login']['user_type'].split(`\\`).includes('Student');
        Cache.set('LOGOUT_URL', '/');
        if (studentLoginObject['password'].toLowerCase() === 'password') {
          Cache.set('USER_PASSWORD', true);
        }
        this.getStudentDetails();
      },
      (error) => {
        this.feedback.loadingStatus = false;
        this.Alert.error(`Unable to to login, please check your credentials and retry`, error);
      });
  }

  getStudentDetails() {
    this.feedback.loadingStatus = true;
    this.studentServiceService.getStudentProfile()
      .subscribe((response) => {
          Cache.set('cbsp_student_profile', response);
          this.feedback.loadingStatus = false;
          this.feedback.loadingStatus = false;
          return (this.userType) ? this.router.navigateByUrl('/student') : this.Alert.error('Invalid Login Credentials');
        },
        error => {
          this.Alert.error('Unable to load student Profile, please reload', error);
        });
  }

  getPingStudent() {
    this.loginService.pingDomain()
      .subscribe((response) => {
          Cache.set('ping', response);
          if (Cache.get('ping')) {
            this.ping = Cache.get('ping');
            this.logo = Cache.get('ping')['settings']['logo'];
          }
        },
        error => {
          console.log('error from profile ::', error);
        });
  }


  public flipRegistration(id: string) {
    if (id === 'signup') {
      const login = $('#m_login');
      login.removeClass('m-login--forget-password');
      login.removeClass('m-login--signin');

      login.addClass('m-login--signup');
      (<any>login.find('.m-login__signup')).animateClass('flipInX animated');
      return;
    } else {
      Cache.remove('bonafide_applicant');
      const login = $('#m_login');
      login.removeClass('m-login--forget-password');
      login.removeClass('m-login--signup');
      login.addClass('m-login--signin');
      (<any>login.find('.m-login__signin')).animateClass('flipInX animated');
    }
  }


  private getAppSettings() {
    this.schoolService.getAppSettings().subscribe((appSettingsResponse) => {
        Cache.set('app_settings', appSettingsResponse);
        this.appSettings = Cache.get('app_settings')['settings'];
      },
      (error) => {
        console.log('Could not load application settings ', error);
      });
  }


  public getApplicantProfile(id) {

    this.admissionService.getApplicantProfile(id).subscribe(
      (applicantProfileResponse) => {
        this.authenticatedUser = applicantProfileResponse['applicant'];
        this.authenticatedUser['programme_object'] = applicantProfileResponse['programme'];
        this.applicant.hasPaid = (this.authenticatedUser['payment_status'] === '1');
        (this.authenticatedUser['payment_status'].toString() === '1') ? this.router.navigateByUrl('/applicant/form') : this.router.navigateByUrl('/applicant');
        // this.router.navigateByUrl('/applicant');

      },
      (error) => {
        console.log('Could not load applicant profile response', error);
      }
    );
  }

  public setRouteParam(userType = 'student') {
    const userRouteParam = (userType === 'student') ? 'yst3l' : 'xap2k';
    this.router.navigate(['/forgot-password', {txhref: userRouteParam}]);
  }

  public switchLogin(what) {
    this.switch_login = what;
    what === 'student' ? this.flipRegistration('signin') : this.flipRegistration('signup');
  }

}
