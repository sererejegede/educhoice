import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import * as utils from '../../../../../utils/util';
import {Faculty} from '../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../interfaces/department.interface';
import {Course} from '../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../services/school.service';
import {UserService} from '../../../../../services/user.service';
import {NotificationService} from '../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../services/api-service/staff-config.service';
import {AdmissionsService} from "../../../../../services/api-service/admissions.service";
import {MagicMethods} from "../../../../../utils/magic-methods";
import {AuthenticationService} from "../../../../../services/authentication.service";

declare const $: any;

@Component({
  selector: 'app-acceptance-fee',
  templateUrl: './acceptance-fee.component.html',
  styleUrls: ['./acceptance-fee.component.css']
})
export class AcceptanceFeeComponent implements OnInit {

  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);

  public allLevels: any[] = [];
  public allChristianDenominations: any[] = [
    {
      name: "Anglican",
      value: "Anglican"
    },
    {
      name: "Baptist",
      value: "Baptist"
    },
    {
      name: "Catholic",
      value: "Catholic"
    },
    {
      name: "Methodist",
      value: "Methodist"
    },
    {
      name: "Presbyterian",
      value: "Presbyterian"
    }
  ];
  public allDenominations: any[] = [];
  public allLgas: any[] = [];
  public overlay = utils.overlay;
  public allAcceptanceFees: any[] = [];
  public updatedAcceptanceFee: any;
  public allFaculties: Faculty[] = [];
  public allDepartments: Department[] = [];
  public allCourses: Course[] = [];
  public allProgrammes: any[] = [];
  public allSessions: any[] = [];
  public allCreateOrUpdateDepartments: any[] = [];
  public loadSessions: boolean = false;
  public allStates: any[] = [];
  public allCountries: any[] = [];
  public authenticatedUser: any;
  public myAcceptanceFees: any[] = [];
  public myEditedAcceptanceFee: any = {};
  public createAcceptanceFeeForm: FormGroup;
  public updateAcceptanceFeeForm: FormGroup;
  public editedIndex: number;
  public load = {
    requesting: {
      list: true,
      create: false
    },
    message: {
      create: 'Create',
      update: 'Update'
    },
    departments: {
      list: false,
      loading: false
    },
    courses: {
      list: false,
      loading: false
    },
    AcceptanceFees: {
      list: false,
      loading: false
    },
    lgas: {
      list: false,
      loading: false
    }

  };


  static createAcceptanceFeeForm = function () {
    return {
      session_id: ['', Validators.compose([Validators.required])],
      amount: ['', Validators.compose([Validators.required])],
      programme_id: ['', Validators.compose([Validators.required])],
      faculty_id: ['', Validators.compose([Validators.required])],
      department_id: ['', Validators.compose([Validators.required])]
    };
    // email,first_name,last_name,course_of_study_id
  };


  static updateAcceptanceFeeForm = function () {

    return {
      session_id: ['', Validators.compose([Validators.required])],
      amount: ['', Validators.compose([Validators.required])],
      programme_id: ['', Validators.compose([Validators.required])],
      faculty_id: ['', Validators.compose([Validators.required])],
      department_id: ['', Validators.compose([Validators.required])]

    };

  };

  constructor(private _script: ScriptLoaderService,
              private userService: UserService,
              private schoolService: SchoolService,
              private fb: FormBuilder,
              private staffConfigService: StaffConfigService,
              private authService: AuthenticationService,
              private admissionsService: AdmissionsService,
              private Alert: NotificationService) {
    // this.authenticatedUser = this.userService.getAuthUser().login.user;
    this.createAcceptanceFeeForm = this.fb.group(AcceptanceFeeComponent.createAcceptanceFeeForm());
    this.updateAcceptanceFeeForm = this.fb.group(AcceptanceFeeComponent.updateAcceptanceFeeForm());
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
    // console.log('Authenticated user ', this.authenticatedUser);
  }

  ngOnInit() {
    this.getAcceptanceFee();
    // this.getAllAcceptanceFees();
    this.getAllLevels();


  }

  // ngAfterViewInit() {
  //     this._script.loadScripts('app-AcceptanceFees',
  //         ['assets/AcceptanceFee_assets/demo/demo4/base/scripts.bundle.js', 'assets/AcceptanceFee_assets/app/js/dashboard.js']);

  // }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }


  public async createAcceptanceFee() {

    this.load.requesting.create = true;
    this.load.message.create = 'Creating...';

    console.log('Create AcceptanceFee object ', this.createAcceptanceFeeForm.value);
    await this.admissionsService.createOrUpdateAcceptanceFee(this.createAcceptanceFeeForm.value).subscribe(
      (createdAcceptanceFeeResponse) => {
        this.load.message.create = 'Create';
        this.Alert.success(`Acceptance fee created successfully\n`);
        this.createAcceptanceFeeForm = this.fb.group(AcceptanceFeeComponent.createAcceptanceFeeForm());
        this.load.requesting.create = false;
        this.triggerModalOrOverlay('close', 'createAcceptanceFee');
        createdAcceptanceFeeResponse['status'] = '1';
        createdAcceptanceFeeResponse['faculty'] =
          this.selectWhereId(this.allFaculties, 'id', createdAcceptanceFeeResponse['department']['faculty_id']);

        this.allAcceptanceFees.unshift(createdAcceptanceFeeResponse);
        console.log('Newly created AcceptanceFee ', createdAcceptanceFeeResponse);
      },
      (error) => {
        this.load.message.create = 'Create';
        // this.triggerModal('close','createSchool');
        console.log('Eroorroroor ', error);
        this.load.requesting.create = false;
        this.Alert.error(`Could not create ${this.createAcceptanceFeeForm.value.first_name}`, error);
      }
    );
  }

  public updateAcceptanceFee() {
    this.load.requesting.create = true;
    this.load.message.update = 'Updating...';

    this.admissionsService.createOrUpdateAcceptanceFee(this.updateAcceptanceFeeForm.value, this.updatedAcceptanceFee.id).subscribe(
      (updatedAcceptanceFeeResponse) => {
        this.load.message.update = 'Update';
        this.updateAcceptanceFeeForm = this.fb.group(AcceptanceFeeComponent.updateAcceptanceFeeForm());
        this.Alert.success(`Acceptance fee for ${this.updatedAcceptanceFee.programme.name} ${this.updatedAcceptanceFee.session.year} updated successfully\n`);
        updatedAcceptanceFeeResponse['faculty'] = this.selectWhereId(this.allFaculties, 'id', updatedAcceptanceFeeResponse['department']['faculty_id']);
        updatedAcceptanceFeeResponse['faculty_id'] = updatedAcceptanceFeeResponse['faculty']['id'];
        this.allAcceptanceFees[this.editedIndex] = updatedAcceptanceFeeResponse;
        this.load.requesting.create = false;
        this.triggerModalOrOverlay('close', 'updateAcceptanceFee');

        console.log('Updated school ', this.updateAcceptanceFeeForm);
      },
      (error) => {
        this.load.message.update = 'Update';
        this.load.requesting.create = false;
        this.Alert.error(`Could not update ${this.updateAcceptanceFeeForm.value.first_name}`, error);
      }
    );
  }

  public getDenomination(value) {
    this.allDenominations = (value === 'Christianity') ? this.allChristianDenominations : [];
  }


  public getAllFaculties() {
    this.schoolService.getAllFaculties().subscribe(
      (facultiesResponse) => {
        this.allFaculties = facultiesResponse;
        this.getAllSessions();
      },
      (error) => {
        this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
      }
    );
  }

  /**
   * This method returns a single faculty which contains a list of departments
   */
  public getDepartmentByByFacultyId(facultyId) {
    this.load.departments.list = true;
    this.load.departments.loading = true;

    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
      (departmentResponse) => {
        this.load.departments.loading = false;

        this.allCreateOrUpdateDepartments = departmentResponse.departments;
        console.log('returned departments', departmentResponse);
      },
      (error) => {
        this.load.departments.list = false;
        this.load.departments.loading = false;

        this.Alert.error(`Sorry could not load departments`, error);
      }
    );
  }

  public getAllProgrammes() {
    this.staffConfigService.getAllProgramme().subscribe((programmeResponse) => {
        this.allProgrammes = programmeResponse.data;
        this.allAcceptanceFees.forEach((acceptanceFee) => {
          acceptanceFee['faculty'] =
            this.selectWhereId(this.allFaculties, 'id', acceptanceFee['department']['faculty_id']);
          acceptanceFee['faculty_id'] = acceptanceFee['faculty']['id'];
          acceptanceFee['session'] =
            this.selectWhereId(this.allSessions, 'id', acceptanceFee['session_id']);
        });
      },
      (error) => {
        this.Alert.error(`Sorry could not load Programmes`, error);
      }
    );
  }

  public getSessionsByProgrammeId(programmeId) {
    this.loadSessions = true;
    this.schoolService.getSessionsByProgrammeId(programmeId).subscribe(
      (sessionsResponse) => {
        this.loadSessions = false;
        this.allSessions = sessionsResponse.data;
      },
      (error) => {
        this.loadSessions = false;
        console.log('session by programme', error);
      });
  }


  public async getAllCourses() {
    await this.schoolService.getAllCourses().subscribe(
      (allCoursesResponse) => {
        this.allCourses = allCoursesResponse.data;
        this.updatedAcceptanceFee.loading = false;
      },
      (error) => {
        this.updatedAcceptanceFee.loading = false;
        this.Alert.error('Sorry, could not load school courses', error);
      });
  }

  public getCourseByDepartmentId(departmentId) {
    this.load.courses.list = true;
    this.load.courses.loading = true;
    this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
      (coursesResponse) => {
        this.load.courses.loading = false;
        this.allCourses = coursesResponse.course_of_studies;
      },
      (error) => {
        this.load.courses.list = false;
        this.load.courses.loading = false;
        this.Alert.error(`Sorry could not load courses`, error);
      }
    );
  }

  public getAllSessions() {
    this.staffConfigService.getAllSession().subscribe(
      (allSessionsResponse) => {
        this.allSessions = allSessionsResponse.data;
        // this.allDepartment();
      },
      (error) => {
        console.log('Could not load levels');
      });
  }

  public getAllLevels() {
    this.staffConfigService.getAllLevel().subscribe(
      (levelsResponse) => {
        this.allLevels = levelsResponse.data;
        console.log(levelsResponse);
      },
      (error) => {
        console.log('Could not load levels');
      });
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    if (ind >= 0) {
      this.editedIndex = ind;
      this.allAcceptanceFees[ind].loading = true;
      this.updatedAcceptanceFee = this.allAcceptanceFees[ind];
      console.log('This edited acceptance fee ', this.updatedAcceptanceFee)
      this.updateAcceptanceFeeForm.patchValue(this.updatedAcceptanceFee);
      console.log('AcceptanceFee management object ', this.updatedAcceptanceFee);
      // this.updatedAcceptanceFee.loading=false
    }
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

  public selectWhereId(data: any[], key, id) {
    let dataItem: any[] = [];
    data.forEach(item => {
      let itemKey = parseInt(item[key]);
      let itemId = parseInt(id);
      if (itemKey === itemId) {
        dataItem.push(item);
      }
    });
    return dataItem[0];
  }

  public getAcceptanceFee() {
    this.load.requesting.list = true;
    this.admissionsService.getAcceptanceFee().subscribe(
      (acceptanceFeeResponse) => {
        this.load.requesting.list = false;
        this.allAcceptanceFees = acceptanceFeeResponse.data;
        this.getAllFaculties();
        console.log(this.allAcceptanceFees);
      },
      (error) => {
        this.load.requesting.list = false;
        this.Alert.error('An error occurred.', error);
      });
  }

}
