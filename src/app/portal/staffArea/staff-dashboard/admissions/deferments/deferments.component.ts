import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import * as utils from '../../../../../../utils/util';
import {Faculty} from '../../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../../interfaces/department.interface';
import {Course} from '../../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../../services/school.service';
import {UserService} from '../../../../../../services/user.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {AdmissionsService} from '../../../../../../services/api-service/admissions.service';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {AuthenticationService} from "../../../../../../services/authentication.service";

declare const $: any;

@Component({
    selector: 'app-deferments',
    templateUrl: './deferments.component.html',
    styleUrls: ['./deferments.component.css']
})
export class DefermentsComponent implements OnInit {

    el: ElementRef;
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject();
    dtElement: DataTableDirective = new DataTableDirective(this.el);


    public allLevels: any[] = [];
    public yearsArray: number[] = [];
    public allLgas: any[] = [];
    public allProgrammes: any[] = [];
    public overlay = utils.overlay;
    public allDeferments = [];
    public updatedDeferments: any;
    public allFaculties: Faculty[] = [];
    public allDepartments: Department[] = [];
    public Defermentstatus;
    public allCourses: Course[] = [];
    public allStates: any[] = [];
    public allCountries: any[] = [];
    public authenticatedUser: any;
    public myDefermentss: any[] = [];
    public myEditedDeferments: any = {};
    public createDefermentsForm: FormGroup;
    public updateDefermentsForm: FormGroup;
    public editedIndex: number;
    public load = {
        requesting: {
            list: true,
            create: false
        },
        message: {
            create: "Create",
            update: "Update"
        },
        departments: {
            list: false,
            loading: false
        },

        states: {
            list: false,
            loading: false
        },

        lgas: {
            list: false,
            loading: false
        },
        courses: {
            list: false,
            loading: false
        },
        Defermentss: {
            list: false,
            loading: false
        }

    }


    static createDefermentsForm = function () {
        return {
            name: ['', Validators.compose([Validators.required])],
            level_id: ['', Validators.compose([Validators.required])],
            profile_strenght: ['', Validators.compose([Validators.required])],
            year: ['', Validators.compose([Validators.required])],
            amount: ['', Validators.compose([Validators.required])],
            start_date: ['', Validators.compose([Validators.required])],
            end_date: ['', Validators.compose([Validators.required])],
            programme_id: ['', Validators.compose([Validators.required])],
            description: ['', Validators.compose([Validators.required])],
            mode_of_entry: ['', Validators.compose([Validators.required])]


        }
        // email,first_name,last_name,course_of_study_id
    }


    static updateDefermentsForm = function () {
        return {

            name: ['', Validators.compose([Validators.required])],
            level_id: ['', Validators.compose([Validators.required])],
            profile_strenght: ['', Validators.compose([Validators.required])],
            year: ['', Validators.compose([Validators.required])],
            amount: ['', Validators.compose([Validators.required])],
            start_date: ['', Validators.compose([Validators.required])],
            end_date: ['', Validators.compose([Validators.required])],
            programme_id: ['', Validators.compose([Validators.required])],
            description: ['', Validators.compose([Validators.required])],
            mode_of_entry: ['', Validators.compose([Validators.required])]


        }
    }

    constructor(private _script: ScriptLoaderService,
                private userService: UserService,
                private schoolService: SchoolService,
                private admissionsService: AdmissionsService,
                private authService: AuthenticationService,
                private staffConfigService: StaffConfigService,
                private fb: FormBuilder,
                private Alert: NotificationService) {
        // this.authenticatedUser = this.userService.getAuthUser().login.user;
        this.createDefermentsForm = this.fb.group(DefermentsComponent.createDefermentsForm());
        this.updateDefermentsForm = this.fb.group(DefermentsComponent.updateDefermentsForm());
        this.dtOptions = {
            pagingType: 'full_numbers',
        };
        console.log("Authenticated user ", this.authenticatedUser);
    }

    ngOnInit() {
        this.getallDeferments();
        // this.getAllCountries();
        // this.getAllFaculties();
        this.getYears();


    }

    // ngAfterViewInit() {
    //     this._script.loadScripts('app-Defermentss',
    //         ['assets/Deferments_assets/demo/demo4/base/scripts.bundle.js', 'assets/Deferments_assets/app/js/dashboard.js']);

    // }

    public iAmPermitted(routeName):boolean{
        return this.authService.iAmPermitted(routeName);
    }

    public getallDeferments() {
        this.load.requesting.list = true;
        this.admissionsService.getAllAdmissionDeferments().subscribe(
            (allDefermentsResponse) => {
                this.load.requesting.list = false;
                this.allDeferments = allDefermentsResponse.data;
                // this.getAllProgrammes();
                // this.getAllLevels();
                if (this.allDeferments.length > 0) {
                    this.dtTrigger.next();
                }
                console.log("All Defermentss response", this.allDeferments)
            },
            (error) => {

                this.load.requesting.list = false;

            }
        )
    }

    public async createDeferments() {

        this.load.requesting.create = true;
        this.load.message.create = "Creating...";

        console.log("Create Deferments object ", this.createDefermentsForm.value);
        await this.admissionsService.createAdmissionDeferments(this.createDefermentsForm.value).subscribe(
            (createdDefermentsResponse) => {
                this.load.message.create = "Create";
                this.Alert.success(`${this.createDefermentsForm.value.name} created successfully\n`);
                this.createDefermentsForm = this.fb.group(DefermentsComponent.createDefermentsForm());
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'createDeferments');
                createdDefermentsResponse = this.patchDefermentsWith([createdDefermentsResponse], this.allProgrammes, {
                    id: 'programme_id',
                    name: 'programme_name'
                })
                createdDefermentsResponse = this.patchDefermentsWith(createdDefermentsResponse, this.allLevels, {
                    id: 'level_id',
                    name: 'level_name'
                });
                createdDefermentsResponse['is_active'] = "0";
                this.allDeferments.push(createdDefermentsResponse[0]);
                console.log("Newly created Deferments ", createdDefermentsResponse)
            },
            (error) => {
                this.load.message.create = "Create";
                // this.triggerModal('close','createSchool');
                console.log("Eroorroroor ", error);
                this.load.requesting.create = false;
                this.Alert.error(`Could not create ${this.createDefermentsForm.value.name}`, error)
            }
        )
    }

    public updateDeferments() {
        this.load.requesting.create = true;
        this.load.message.update = "Updating...";
        this.admissionsService.updateAdmissionDeferment(this.updateDefermentsForm.value, this.updatedDeferments.id).subscribe(
            (updatedDefermentsResponse) => {
                this.load.message.update = "Update";
                this.updateDefermentsForm = this.fb.group(DefermentsComponent.updateDefermentsForm());
                this.Alert.success(`${this.updateDefermentsForm.value.name} updated successfully\n`);
                updatedDefermentsResponse = this.patchDefermentsWith([updatedDefermentsResponse], this.allProgrammes, {
                    id: 'programme_id',
                    name: 'programme_name'
                })
                updatedDefermentsResponse = this.patchDefermentsWith(updatedDefermentsResponse, this.allLevels, {
                    id: 'level_id',
                    name: 'level_name'
                });
                // updatedDefermentsResponse[0]['is_active']
                const applicationIsActive: boolean = this.allDeferments[this.editedIndex]['is_active'].toString() == '1';
                alert(applicationIsActive);
                const applicationDeadline = this.updateDefermentsForm.value.end_date;
                if (!applicationIsActive && !this.deadlineHasPassed(applicationDeadline)) {
                    this.Alert.info('Consider Activating this application status');
                }
                this.allDeferments[this.editedIndex] = updatedDefermentsResponse[0];
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'updateDeferments');


                console.log("Updated Application ", updatedDefermentsResponse)
            },
            (error) => {
                this.load.message.update = "Update";
                this.load.requesting.create = false;
                this.Alert.error(`Could not update ${this.updateDefermentsForm.value.first_name}`, error)
            }
        )
    }


    public getAllFaculties() {
        this.schoolService.getAllFaculties().subscribe(
            (facultiesResponse) => {
                console.log("All faculties ", facultiesResponse);
                this.allFaculties = facultiesResponse;
            },
            (error) => {
                this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
            }
        )
    }

    /**
     * This method returns a single faculty which contains a list of departments
     */
    public getDepartmentByByFacultyId(facultyId) {
        this.load.departments.list = true;
        this.load.departments.loading = true;

        this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
            (departmentResponse) => {
                this.load.departments.loading = false;

                this.allDepartments = departmentResponse.departments;
                console.log("returned departments", departmentResponse);
            },
            (error) => {
                this.load.departments.list = false;
                this.load.departments.loading = false;

                this.Alert.error(`Sorry could not load departments`, error)
            }
        )
    }

    public async getAllCourses() {
        await this.schoolService.getAllCourses().subscribe(
            (allCoursesResponse) => {
                this.allCourses = allCoursesResponse.data;
                this.updatedDeferments.loading = false;
            },
            (error) => {
                this.updatedDeferments.loading = false;
                this.Alert.error("Sorry, could not load school courses", error);
            })
    }

    public getCourseByDepartmentId(departmentId) {
        this.load.courses.list = true;
        this.load.courses.loading = true;
        this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
            (coursesResponse) => {
                this.load.courses.loading = false;

                this.allCourses = coursesResponse.course_of_studies;
                console.log("returned courses", coursesResponse);
            },
            (error) => {
                this.load.courses.list = false;
                this.load.courses.loading = false;
                this.Alert.error(`Sorry could not load courses`, error)
            }
        )
    }

    public getAllCountries() {
        this.schoolService.getAllCountries().subscribe(
            (countriesResponse) => {
                this.allCountries = countriesResponse;
                console.log("returned countries", countriesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load countries`, error)
            }
        )
    }

    public getStateByCountryId(countryId) {
        this.load.states.loading = true;
        this.schoolService.getStateByCountryId(countryId).subscribe(
            (statesResponse) => {
                this.load.states.loading = false;
                this.allStates = statesResponse.states;
                console.log("returned States", statesResponse);
            },
            (error) => {
                this.load.states.loading = false;

                this.Alert.error(`Sorry could not load States for
                 ${this.selectWhereId(this.allCountries, 'id', countryId).name}`,
                    error)
            }
        )
    }

    public getAllProgrammes() {
        this.staffConfigService.getAllProgramme().subscribe(
            (programmeResponse) => {
                this.allProgrammes = programmeResponse.data;
                this.allDeferments = this.patchDefermentsWith(this.allDeferments, this.allProgrammes, {
                    id: "programme_id",
                    name: "programme_name"
                });
                console.log("returned States", programmeResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load Programmes`, error)
            }
        )
    }

    public patchDefermentsWith(trasformData, parentData, key: { 'id', 'name' }) {
        parentData.forEach((dataItem) => {
            trasformData.forEach((transform) => {
                if (transform[key['id']] == dataItem.id) {
                    transform[key['name']] = dataItem.name
                }
            });
        });
        return trasformData;
    }


    public getLgaByStateId(stateId) {
        this.load.lgas.loading = true;
        let dataItem = this.selectWhereId(this.allStates, 'id', stateId)
        console.log("Select state where id is ", dataItem);
        this.admissionsService.getLgaByStateId(stateId).subscribe(
            (lgasResponse) => {
                this.load.lgas.loading = false;

                this.allLgas = lgasResponse.lgas;
                console.log("returned lgas", lgasResponse);
            },
            (error) => {
                this.load.lgas.loading = false;
                this.Alert.error(`Sorry could not load Lgas for`, error);
                // ${this.selectWhereId(this.allStates, stateId).name}
            }
        )
    }


    public getAllLevels() {
        this.staffConfigService.getAllLevel().subscribe(
            (levelsResponse) => {
                this.allLevels = levelsResponse.data;
                this.allDeferments = this.patchDefermentsWith(this.allDeferments, this.allLevels, {
                    id: 'level_id',
                    name: 'level_name'
                })
                console.log(levelsResponse);
            },
            (error) => {
                console.log("Could not load levels");
            }
        )
    }

    public getYears() {
        let minYear = 2000;
        let maxYear = new Date().getFullYear();
        let yearsArray: number[] = [];
        for (let y = minYear; y <= maxYear; y++) {
            this.yearsArray.push(y);
            // //console.log("All years ::", this.yearsArray);
        }

    }


    public logStaffIn() {
        this.schoolService.logStaffIn().subscribe(
            (loginResponse) => {
                this.userService.setAuthUser(loginResponse.token);


            }
        )
    }

    public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        if (ind >= 0) {
            this.allDeferments[ind].loading = true;
            this.editedIndex = ind;
            this.updatedDeferments = this.allDeferments[ind];
            this.updateDefermentsForm.patchValue(this.updatedDeferments);
            console.log("Deferments management object ", this.updatedDeferments);

            // this.updatedDeferments.loading=false

        }
        (action === "open") ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
        // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
        // });

    }

    public selectWhereId(data: any[], key, id) {
        let dataItem: any[] = [];
        data.forEach(item => {
            let itemKey = parseInt(item[key]);
            let itemId = parseInt(id);
            if (itemKey === itemId) {
                dataItem.push(item);
            }
        });
        return dataItem[0];

    }

    public toggleDefermentstatus(ind, action) {
        let toggleMessage = (action === 'approve') ? 'Approve' : 'Decline';
        if (toggleMessage === 'Approve') {
            this.allDeferments[ind].approveloading = true;
        }
        else {
            this.allDeferments[ind].rejectloading = true;
        }
        const studentId = this.allDeferments[ind].id;
        this.admissionsService.toggleDefermentstatus(studentId, action).subscribe(
            (toggleResponse) => {
                toggleResponse['status'] = toggleResponse['status'].toString();
                const studentName = this.allDeferments[ind]['student']['matric no'];
                const defermentDuration = this.allDeferments[ind]['duration'];
                this.Alert.success(`${studentName}'s ${defermentDuration} deferment has been ${toggleMessage}d!`);
                this.allDeferments[ind] = toggleResponse;
                this.setLoader(ind, toggleMessage);
                console.log('Toggle response ', toggleResponse)
            },
            (error) => {
                this.setLoader(ind, toggleMessage);
                this.allDeferments[ind].loading = false;
                this.Alert.error(`An error occurred. Could not ${toggleMessage} this deferment`);

            })
    }

    private setLoader(ind, action) {
        if (action === 'Approve') {
            this.allDeferments[ind].approveloading = false;
        }
        else {
            this.allDeferments[ind].rejectloading = false;
        }
    }


    public deadlineHasPassed(endDate): boolean {
        const rightnow = new Date();
        let resolveEndDate = endDate.split('-').join(',').toString();
        resolveEndDate = new Date(resolveEndDate);
        return (rightnow >= resolveEndDate);
    }

}
