import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import * as utils from '../../../../../../utils/util';
import {Faculty} from '../../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../../interfaces/department.interface';
import {Course} from '../../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../../services/school.service';
import {UserService} from '../../../../../../services/user.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {AdmissionsService} from '../../../../../../services/api-service/admissions.service';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {Cache} from "../../../../../../utils/cache";
import {AuthenticationService} from "../../../../../../services/authentication.service";

declare const $: any;

@Component({
    selector: 'app-jamb-record',
    templateUrl: './jamb-record.component.html',
    styleUrls: ['./jamb-record.component.css']
})
export class JambRecordComponent implements OnInit {


    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject();
    dtElement: DataTableDirective = new DataTableDirective(this.el);

    public overlay = utils.overlay;
    public allJambRecords: any[] = [];
    public updatedJambRecord: any;
    public enableSessionIput: boolean = false;
    public allFaculties: Faculty[] = [];
    public allDepartments: Department[] = [];
    public allCourses: Course[] = [];
    public allStates: any[] = [];
    public allCountries: any[] = [];
    public allSessions: any[] = [];
    public allProgrammes: any[] = [];
    public authenticatedUser: any;
    public myJambRecords: any[] = [];
    public myEditedJambRecord: any = {};
    public createJambRecordForm: FormGroup;
    public updateJambRecordForm: FormGroup;
    public editedIndex: number;
    public loadSessions: boolean = false;
    public load = {
        requesting: {
            list: false,
            create: false
        },
        message: {
            create: 'Create',
            update: 'Update'
        },
        departments: {
            list: false,
            loading: false
        },
        courses: {
            list: false,
            loading: false
        },
        JambRecords: {
            list: false,
            loading: false
        }

    };


    static createJambRecordForm = function () {
        return {
            session: ['', Validators.compose([Validators.required])],
            type: ['', Validators.compose([Validators.required])],
            csv: ['', Validators.compose([])]

        };
        // email,first_name,last_name,course_of_study_id
    };


    static updateJambRecordForm = function () {
        return {
            email: ['', Validators.compose([Validators.required])],
            first_name: ['', Validators.compose([Validators.required])],
            middle_name: '',
            last_name: ['', Validators.compose([Validators.required])],
            faculty_id: ['', Validators.compose([Validators.required])],
            department_id: ['', Validators.compose([Validators.required])],
            course_of_study_id: ['', Validators.compose([Validators.required])],
            country_id: ['', Validators.compose([Validators.required])],
            state_id: '',
            matric_no: ['', Validators.compose([Validators.required])]
        };
    };

    constructor(private _script: ScriptLoaderService,
                private userService: UserService,
                private schoolService: SchoolService,
                private admissionsService: AdmissionsService,
                private authService: AuthenticationService,
                private fb: FormBuilder,
                private Alert: NotificationService,
                private  el: ElementRef,
                private staffConfigService: StaffConfigService) {
        // this.authenticatedUser = this.userService.getAuthUser().login.user;
        this.createJambRecordForm = this.fb.group(JambRecordComponent.createJambRecordForm());
        this.updateJambRecordForm = this.fb.group(JambRecordComponent.updateJambRecordForm());
        // this.dtOptions = {
        //   pagingType: 'full_numbers'
        // };

        console.log('Authenticated user ', this.authenticatedUser);
    }

    ngOnInit() {
        // if (!this.isCache()) {
        //   this.getAllJambRecords();
        //   this.dtTrigger.next();
        //     this.isCache()
        // }

        // this.getAllJambRecords();
        // this.getAllSessions();
        this.getAllProgrammes();
        this.getAllFaculties();
    }

    public iAmPermitted(routeName):boolean{
        return this.authService.iAmPermitted(routeName);
    }
    // ngAfterViewInit() {
    //     this._script.loadScripts('app-JambRecords',
    //         ['assets/JambRecord_assets/demo/demo4/base/scripts.bundle.js', 'assets/JambRecord_assets/app/js/dashboard.js']);

    // }

    // public isCache(){
    //   let recordIsCached: boolean = false;
    //   const cachedAllJambRecords = Cache.get('cachedAllJambRecords');
    //   if (cachedAllJambRecords && cachedAllJambRecords.length > 0) {
    //     recordIsCached = true;
    //     this.load.requesting.list = false;
    //     this.allJambRecords = cachedAllJambRecords;
    //     // this.dtTrigger.next();
    //     console.log('All CachedJambRecords response', cachedAllJambRecords);
    //   }
    //   return recordIsCached;
    // }

    public getAllJambRecords(value) {

        this.load.requesting.list = true;
        this.enableSessionIput = false;
        const data = {session_id: value};
        this.admissionsService.getAllJambRecords(data).subscribe(
            (allJambRecordsResponse) => {
                // Cache.set('cachedAllJambRecords', allJambRecordsResponse);
                this.load.requesting.list = false;
          this.allJambRecords = allJambRecordsResponse;
                // [0]['jamb_records']
                if (this.allJambRecords.length > 0) {
                    this.dtTrigger.next();
                }
                // this.isCache();
                console.log('All JambRecords response', allJambRecordsResponse);
            },
            (error) => {
                this.Alert.error('Could not load jamb records', error);
                this.load.requesting.list = false;

            }
        );
    }

    public async createJambRecord(fileId) {

        this.load.requesting.create = true;
        this.load.message.create = 'Creating...';
        const idsObject = this.createJambRecordForm.value;
        idsObject['cc_id'] = idsObject['curriculum_course_id'];
        idsObject['session_id'] = 1;
        console.log('Create JambRecord object ', this.createJambRecordForm.value);
        const bulkFileId = `#${fileId}`;
        const formFile = this.el.nativeElement.querySelector(bulkFileId);
        console.log('Form File ', formFile);
        const $linkId = $(bulkFileId);
        const $newLinkId = $linkId.val();
        const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
        // const file_type = '';
        $linkId.removeClass('alert alert-danger animated rubberBand');
        if (file_type && (file_type !== '.csv') && (file_type !== '.docx') && (file_type !== '.xls')) {
            $linkId.addClass('alert alert-danger animated rubberBand');
            this.load.requesting.create = false;

            return this.Alert.error('You can only upload csv, docx or xls files');
        }
        await this.admissionsService.uploadJambRecord(formFile, this.createJambRecordForm.value).subscribe(
            (fileResponse) => {
                this.allJambRecords = [];
                this.Alert.success(`Jamb Records uploaded successfully\n`);
                this.createJambRecordForm = this.fb.group(JambRecordComponent.createJambRecordForm());
                this.allJambRecords = fileResponse;
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'jambRecordTemplate');
                console.log('File upload Response', fileResponse);
            },
            (error) => {
                this.load.requesting.create = false;
                this.Alert.error('An error occurred uploading file', error);
            }
        );

        // await this.admissionsService.UpdateJambRecord(this.createJambRecordForm.value, 1).subscribe(
        //     (createdJambRecordResponse) => {
        //         this.load.message.create = 'Create';
        //         this.Alert.success(`${this.createJambRecordForm.value.name} created successfully\n`);
        //         this.createJambRecordForm = this.fb.group(JambRecordComponent.createJambRecordForm());
        //         this.load.requesting.create = false;
        //         this.triggerModalOrOverlay('close', 'createJambRecord');
        //         this.myJambRecords.push(createdJambRecordResponse);
        //         console.log('Newly created school ', createdJambRecordResponse);
        //     },
        //     (error) => {
        //         this.load.message.create = 'Create';
        //         // this.triggerModal('close','createSchool');
        //         console.log('Eroorroroor ', error);
        //         this.load.requesting.create = false;
        //         this.Alert.error(`Could not create ${this.createJambRecordForm.value.name}`, error);
        //     }
        // );
    }

    public updateJambRecord() {
        this.load.requesting.create = true;
        this.load.message.update = 'Updating...';

        this.admissionsService.UpdateJambRecord(this.updateJambRecordForm.value, this.updatedJambRecord.id).subscribe(
            (updatedJambRecordResponse) => {
                this.load.message.update = 'Update';
                this.updateJambRecordForm = this.fb.group(JambRecordComponent.updateJambRecordForm());
                this.Alert.success(`${this.updateJambRecordForm.value.name} updated successfully\n`);
                this.allJambRecords[this.editedIndex] = updatedJambRecordResponse;
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'updateJambRecord');

                console.log('Updated school ', this.updateJambRecordForm);
            },
            (error) => {
                this.load.message.update = 'Update';
                this.load.requesting.create = false;
                this.Alert.error(`Could not update ${this.updateJambRecordForm.value.first_name}`, error);
            }
        );
    }


    public getAllFaculties() {
        this.schoolService.getAllFaculties().subscribe(
            (facultiesResponse) => {
                console.log('All faculties ', facultiesResponse);
                this.allFaculties = facultiesResponse;
            },
            (error) => {
                this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
            }
        );
    }


    // public uploadJambRecordTemplate(fileId) {
    //     const idsObject = this.uploadTemplateForm.value;
    //     idsObject['cc_id'] = idsObject['curriculum_course_id'];
    //     idsObject['session_id'] = this.sessionId;
    //     const bulkFileId = `#${fileId}`;
    //     const formFile = this.el.nativeElement.querySelector(bulkFileId);
    //     console.log('Form File ', formFile);
    //     const $linkId = $(bulkFileId);
    //     const $newLinkId = $linkId.val();
    //     const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
    //     // const file_type = '';
    //     $linkId.removeClass('alert alert-danger animated rubberBand');
    //     if (file_type && (file_type !== '.pdf') && (file_type !== '.docx') && (file_type !== '.doc')) {
    //         $linkId.addClass('alert alert-danger animated rubberBand');
    //         return this.Alert.error('You can only upload pdf, docx or doc files');
    //     }
    //     this.admissionsService.uploadJambRecord(formFile, idsObject).subscribe(
    //         (fileResponse) => {
    //             console.log('File upload Response', fileResponse);
    //         },
    //         (error) => {
    //             this.Alert.error('An error occurred uploading file', error);
    //         }
    //     );
    //
    //     // if (!this.checkNullFile(formFile)) {
    //     //   $linkId.addClass("alert alert-danger animated rubberBand");
    //     //   return this.Alert.error("Please choose a file!");
    //     // }
    //     // alert(formFile.files[0].size)
    //
    // }

    /**
     * This method returns a single faculty which contains a list of departments
     */
    public getDepartmentByByFacultyId(facultyId) {
        this.load.departments.list = true;
        this.load.departments.loading = true;

        this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
            (departmentResponse) => {
                this.load.departments.loading = false;

                this.allDepartments = departmentResponse.departments;
                console.log('returned departments', departmentResponse);
            },
            (error) => {
                this.load.departments.list = false;
                this.load.departments.loading = false;

                this.Alert.error(`Sorry could not load departments`, error);
            }
        );
    }

    public getSessionsByProgrammeId(programmeId) {
        this.loadSessions = true;
        this.enableSessionIput = true;
        this.schoolService.getSessionsByProgrammeId(programmeId).subscribe(
            (sessionsResponse) => {
                this.loadSessions = false;

                this.allSessions = sessionsResponse.data;
                console.log('All sessions by programme Id ', sessionsResponse);
            },
            (error) => {
                this.loadSessions = false;

                console.log('An eror occured getting session ', error)
            }
        )
    }


    public async getAllCourses() {
        await this.schoolService.getAllCourses().subscribe(
            (allCoursesResponse) => {
                this.allCourses = allCoursesResponse.data;
                this.updatedJambRecord.loading = false;
            },
            (error) => {
                this.updatedJambRecord.loading = false;
                this.Alert.error('Sorry, could not load school courses', error);
            });
    }

    public getCourseByDepartmentId(departmentId) {
        this.load.courses.list = true;
        this.load.courses.loading = true;
        this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
            (coursesResponse) => {
                this.load.courses.loading = false;

                this.allCourses = coursesResponse.course_of_studies;
                console.log('returned courses', coursesResponse);
            },
            (error) => {
                this.load.courses.list = false;
                this.load.courses.loading = false;
                this.Alert.error(`Sorry could not load courses`, error);
            }
        );
    }

    public getAllProgrammes() {
        this.staffConfigService.getAllProgramme().subscribe(
            (programmeResponse) => {
                this.allProgrammes = programmeResponse.data;
                console.log('returned programmes', programmeResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load Programmes`, error);
            }
        );
    }

    public getAllCountries() {
        this.schoolService.getAllCountries().subscribe(
            (countriesResponse) => {
                this.allCountries = countriesResponse;
                console.log('returned countries', countriesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load countries`, error);
            }
        );
    }

    public getStateByCountryId(countryId) {
        this.schoolService.getStateByCountryId(countryId).subscribe(
            (statesResponse) => {
                this.allStates = statesResponse.states;
                console.log('returned States', statesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load States`, error);
            }
        );
    }


    public logStaffIn() {
        this.schoolService.logStaffIn().subscribe(
            (loginResponse) => {
                this.userService.setAuthUser(loginResponse.token);
            }
        );
    }

    public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        if (ind >= 0) {
            this.updatedJambRecord = this.allJambRecords[ind];
            const objectKeys = Object.keys(this.updatedJambRecord);
            const subjects = [];
            objectKeys.forEach((key) => {
                const searchKey = key.split('_');
                if (searchKey[1] === 'score' && searchKey[0] !== 'eng') {
                    subjects.push({
                        'subj_name': this.updatedJambRecord[searchKey[0]],
                        'subj_score': this.updatedJambRecord[key]
                    });
                }
            });
            subjects.push({'subj_name': 'ENG', 'subj_score': this.updatedJambRecord['eng_score']});
            this.updatedJambRecord.subj_list = subjects;
            console.log('Subjects filtered ', subjects);
            console.log('JambRecord management object ', this.updatedJambRecord);
            console.log('Updated jamb record ', this.updatedJambRecord);
            // this.updatedJambRecord.loading=false

        }

        (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
        // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
        // });

    }


}
