import {Component, OnInit} from '@angular/core';
import {Cache} from "../../../../../../utils/cache";
import {ActivatedRoute} from "@angular/router";
import * as utils from '../../../../../../utils/util';
import {AdmissionsService} from "../../../../../../services/api-service/admissions.service";
import {NotificationService} from "../../../../../../services/notification.service";

console.log = () => {};
@Component({
  selector: 'app-putme-candidate',
  templateUrl: './putme-candidate.component.html',
  styleUrls: ['./putme-candidate.component.css']
})
export class PutmeCandidateComponent implements OnInit {

  public appSettings: any;
  public viewedPutmeCandidate: any = {};
  public courseChoice: any = {};
  public PutmeCandidateResults;
  public allDenominations: any[] = [];
  public allLgas: any[] = [];
  public overlay = utils.overlay;
  public allStudents: any[] = [];
  public allSubjects: any[] = [];
  public allLevels: any[] = [];
  public allFaculties: any[] = [];
  public allDepartments: any[] = [];
  public allCourses: any[] = [];
  public allStates: any[] = [];
  public allCountries: any[] = [];
  public authenticatedUser: any;
  public editedIndex: number;
  public candidateId: number;
  public load = {
    requesting: {
      list: true,
      create: false
    },
    message: {
      create: 'Create',
      update: 'Update'
    },
    departments: {
      list: false,
      loading: false
    },
    courses: {
      list: false,
      loading: false
    },
    students: {
      list: false,
      loading: false
    },
    lgas: {
      list: false,
      loading: false
    }

  };


  constructor(private activeRoute: ActivatedRoute,
              private admissionService: AdmissionsService,
              private Alert: NotificationService) {
    // if (!Cache.get('viewed_putme')) {
    //     window.history.back();
    // }
    // else {
    //     // this.viewedPutmeCandidate = Cache.get('viewed_putme');
    //     // this.PutmeCandidateResults =
    //     //     this.viewedPutmeCandidate['applicant']['applicant_profile']['olevel'][0];
    //     console.log('Viewed candidate ', this.viewedPutmeCandidate);
    // }

  }

  ngOnInit() {
    this.appSettings = Cache.get('ping') || Cache.get('app_settings');
    console.info(this.appSettings);
    this.getAllOlevelSubjects();
    this.activeRoute.params.subscribe(params => {
      this.candidateId = +params['id']; // (+) converts string 'id' to a number
      this.getPutmeCandidateById(this.candidateId);
    });

  }


  public getPutmeCandidateById(id) {
    this.admissionService.getPutmeCandidateById(id).subscribe(
      (candidateResponse) => {
        this.viewedPutmeCandidate = candidateResponse;
        this.courseChoice = (this.viewedPutmeCandidate['applicant'] && this.viewedPutmeCandidate['applicant']['applicant_profile'] && this.viewedPutmeCandidate['applicant']['applicant_profile']['course_of_study_choice']) ? this.viewedPutmeCandidate['applicant']['applicant_profile']['course_of_study_choice'][0] : {};
        console.log("Candidate response ", candidateResponse);
        this.PutmeCandidateResults =
          (this.viewedPutmeCandidate['applicant']['applicant_profile'] &&
            this.viewedPutmeCandidate['applicant']['applicant_profile']['olevel']) ?
            this.viewedPutmeCandidate['applicant']['applicant_profile']['olevel'] : [];
      },
      (error) => {
        this.Alert.error(`Could not load records for this candidate`);
        window.history.back();
      }
    )
  }

  public selectWhereId(data: any[], search_key: string, id) {
    let dataItem: any[] = [];
    data.forEach(item => {
      let itemKey = parseInt(item[search_key]);
      let itemId = parseInt(id);
      if (itemKey === itemId) {
        dataItem.push(item);
      }
    });
    return dataItem[0];

  }


  public getAllOlevelSubjects() {
    this.admissionService.getAllOlevelSubjects().subscribe(
      (subjectsResponse) => {
        this.allSubjects = subjectsResponse;
        console.log('All applicant subjects centers ', subjectsResponse)
      },
      (error) => {
        console.log('An error occurred loading subjects centers ', error)
      }
    )
  }

  public goBack() {
    window.history.back();
  }

  public setCandidateStatus(): string {
    if (this.viewedPutmeCandidate['applicant'] && this.viewedPutmeCandidate['applicant']['interview_status']) {
      switch (this.viewedPutmeCandidate['applicant']['interview_status'].toString()) {
        case '0':
          return 'Pending';
        case '1':
          return 'Scheduled';
        case '2':
          return 'Interviewed'
        default :
          return '-'
      }
    }

    return 'Unknown';

  }

  public triggerModalOrOverlay(action, modalId) {

  }
}
