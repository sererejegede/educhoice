import {Component, ElementRef, OnInit} from '@angular/core';
import {AdmissionsService} from "../../../../../../services/api-service/admissions.service";
import {NotificationService} from "../../../../../../services/notification.service";
import {FormGroup} from "@angular/forms";

import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';
import {SchoolService} from "../../../../../../services/school.service";
import {AuthenticationService} from "../../../../../../services/authentication.service";

@Component({
    selector: 'app-applicant-student-migration',
    templateUrl: './applicant-student-migration.component.html',
    styleUrls: ['./applicant-student-migration.component.css']
})
export class ApplicantStudentMigrationComponent implements OnInit {

    el: ElementRef;
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject();
    dtElement: DataTableDirective = new DataTableDirective(this.el);


    public allApplicantsForStudentMigration: any[] = [];
    public allCoursesOfStudy: any[] = [];
    public selectedCourseId:number = 0;
    public enableCourseInput:boolean = false;

    public applicantMigrationForm: FormGroup;
    public holdMigrationList: any[] = [];
    public load = {
        requesting: {
            list: true,
            create: false
        },
        message: {
            create: 'Migrate',
            update: 'Update'
        },
        departments: {
            list: false,
            loading: false
        },

        states: {
            list: false,
            loading: false
        },

        lgas: {
            list: false,
            loading: false
        },
        courses: {
            list: false,
            loading: false
        },
        Putmes: {
            list: false,
            loading: false
        }

    };


    constructor(private admissionService: AdmissionsService,
                private Alert: NotificationService,
                private authService:AuthenticationService,
                private schoolService:SchoolService) {
    }

    ngOnInit() {
        this.getApplicantsForMigration();
        this.getAllCourses();
    }

    public iAmPermitted(routeName):boolean{
        return this.authService.iAmPermitted(routeName);
    }

    public getApplicantsForMigration() {
        this.load.requesting.list = true;
        this.admissionService.getApplicantsForMigration().subscribe(
            (waitingApplicants) => {
                this.load.requesting.list = false;
                this.allApplicantsForStudentMigration = waitingApplicants;
                this.allApplicantsForStudentMigration.forEach(
                    (student) => {
                        const fullName = student['name'].split(' ');
                        student['first_name'] = fullName[0];
                        student['last_name'] = fullName[1];
                        student['other_name'] = (fullName.length === 3) ? fullName[2] : 'None Captured';
                        student['phone'] = student['phone'];
                        student['email'] = student['email'];
                        student['first_choice'] = student['applicant_profile']['course_of_study_choice'][0]['first_choice']['name'];
                        student['second_choice'] = student['applicant_profile']['course_of_study_choice'][0]['second_choice']['name'];
                        student['jamb_choice'] = student['cors_abbr'];
                        student['jamb_score'] = student['total_ume_score'];
                        student['admission_status'] = (student['admission_status'] === '1') ? 'Admitted' : 'Pending';
                    }
                )
                console.log('Applicants for student migration ', waitingApplicants);
            },
            (error) => {
                this.load.requesting.list = false;
                this.Alert.error('An error occurred, could not load applicants for migration');
            }
        )
    }




    public migrateApplicantsToStudents() {

        this.load.requesting.create = true;
        this.load.message.create = 'Migrating';
        const bulkMigration = {
            applicant_ids: this.holdMigrationList
        }
        this.admissionService.migrateApplicantToStudent(this.selectedCourseId, bulkMigration).subscribe((migratedApplicants) => {
                // this.allApplicantsForStudentMigration = waitingApplicants.data;
                this.load.requesting.create = false;
                this.load.message.create = 'Migrate';
                this.enableCourseInput = !this.enableCourseInput;
                console.log('Applicants for student migration ', migratedApplicants);
            },
            (error) => {
                this.load.requesting.create = false;
                this.load.message.create = 'Migrate';
                this.Alert.error('An error occurred, could not load applicants for migration');
            }
        )
    }

    public stashApplicantForMigration(ind) {
        if (!this.holdMigrationList.includes(this.allApplicantsForStudentMigration[ind].id)) {
            this.holdMigrationList.push(this.allApplicantsForStudentMigration[ind].id);
        }
        else {
            const applicantIndex = this.holdMigrationList.indexOf(this.allApplicantsForStudentMigration[ind].id);
            this.holdMigrationList.splice(applicantIndex, 1)
        }

        console.log('Applicants stashed for migration ', this.holdMigrationList);
    }


    public async getAllCourses() {
        await this.schoolService.getAllCourses().subscribe(
            (allCoursesResponse) => {
                this.allCoursesOfStudy = allCoursesResponse.data;
            },
            (error) => {
                this.Alert.error('Sorry, could not load school courses', error);
            });
    }

    public setCourseOfStudy(id) {

        this.selectedCourseId = id;
    }

    public enableCourseOfStudyInput(){
        this.enableCourseInput = !this.enableCourseInput;
    }
}
