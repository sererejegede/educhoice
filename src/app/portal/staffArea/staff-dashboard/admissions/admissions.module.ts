import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {JambRecordComponent} from './jamb-record/jamb-record.component';
import {PutmeComponent} from './putme/putme.component';
import {AdmissionListComponent} from './admission-list/admission-list.component';
import {AdmissionLetterComponent} from './admission-letter/admission-letter.component';
import {RouterModule, Routes} from '@angular/router';
import {StaffDashboardComponent} from '../dashboard-component.component';
import {ApplicantsComponent} from './applicants/applicants.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {ApplicationsComponent} from './applications/applications.component';
import {DragulaModule} from "ng2-dragula";
import {EditorModule} from '@tinymce/tinymce-angular';
import {FroalaEditorModule, FroalaViewModule} from 'angular2-froala-wysiwyg';
import {DataTablesModule} from "angular-datatables";
import {CKEditorModule} from "ng2-ckeditor";
import {LetterTypesComponent} from './admission-letter/letter-types/letter-types.component';
import {DefermentsComponent} from './deferments/deferments.component';
import {PutmeCandidateComponent} from './putme-candidate/putme-candidate.component';
import {PipeModule} from "../../../../../shared/modules/pipe.module";
import {ApplicantStudentMigrationComponent} from './applicant-student-migration/applicant-student-migration.component';

const ADMISSIONS_ROUTES: Routes = [
    {
        path: 'admissions', component: StaffDashboardComponent,
        children: [
            {path: 'jamb-record', component: JambRecordComponent},
            {path: 'list', component: AdmissionListComponent},
            {path: 'letter', component: AdmissionLetterComponent},
            {path: 'letter/preview', component: LetterTypesComponent},
            {path: 'candidate', component: PutmeComponent},
            {path: 'candidate/:id', component: PutmeCandidateComponent},
            {path: 'applicants', component: ApplicantsComponent},
            {path: 'applications', component: ApplicationsComponent},
            {path: 'deferments', component: DefermentsComponent},
            {path: 'migration', component: ApplicantStudentMigrationComponent}
        ]
    }

];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        DragulaModule,
        EditorModule,
        DataTablesModule,
        CKEditorModule,
        PipeModule,
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot(),
        RouterModule.forChild(ADMISSIONS_ROUTES)
    ],
    declarations: [
        ApplicantsComponent,
        JambRecordComponent,
        PutmeComponent,
        AdmissionListComponent,
        AdmissionLetterComponent,
        ApplicationsComponent,
        LetterTypesComponent,
        DefermentsComponent,
        PutmeCandidateComponent,
        ApplicantStudentMigrationComponent
    ],
    exports: [
        ApplicantsComponent,
        JambRecordComponent,
        PutmeComponent,
        AdmissionListComponent,
        AdmissionLetterComponent
    ]
})
export class AdmissionsModule {
}
