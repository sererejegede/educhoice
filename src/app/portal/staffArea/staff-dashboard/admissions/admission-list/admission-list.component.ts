import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as utils from '../../../../../../utils/util';
import {Faculty} from '../../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../../interfaces/department.interface';
import {Course} from '../../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../../services/school.service';
import {UserService} from '../../../../../../services/user.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {AdmissionsService} from '../../../../../../services/api-service/admissions.service';
import {StaffConfigService} from "../../../../../../services/api-service/staff-config.service";
import {AuthenticationService} from "../../../../../../services/authentication.service";

declare const $: any;


@Component({
    selector: 'app-admission-list',
    templateUrl: './admission-list.component.html',
    styleUrls: ['./admission-list.component.css']
})
export class AdmissionListComponent implements OnInit {

    @ViewChild(DataTableDirective)
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject();
    dtElement: DataTableDirective = new DataTableDirective(this.el);
 

    public overlay = utils.overlay;
    public allAdmissionLists: any[] = [];
    public updatedAdmissionList: any;
    public allFaculties: Faculty[] = [];
    public allSessions: any[] = [];
    public allDepartments: Department[] = [];
    public allCourses: Course[] = [];
    public allStates: any[] = [];
    public allCountries: any[] = [];
    public authenticatedUser: any;
    public myAdmissionLists: any[] = [];
    public myEditedAdmissionList: any = {};
    public createAdmissionListForm: FormGroup;
    public updateAdmissionListForm: FormGroup;
    public editedIndex: number;
    public templateUrl: string = '';
    public load = {
        requesting: {
            list: true,
            create: false
        },
        message: {
            create: "Create",
            update: "Update"
        },
        departments: {
            list: false,
            loading: false
        },
        courses: {
            list: false,
            loading: false
        },
        AdmissionLists: {
            list: false,
            loading: false
        }

    }


    static createAdmissionListForm = function () {
        return {
            email: ['', Validators.compose([Validators.required])],
            first_name: ['', Validators.compose([Validators.required])],
            middle_name: '',
            last_name: ['', Validators.compose([Validators.required])],
            faculty_id: ['', Validators.compose([Validators.required])],
            department_id: ['', Validators.compose([Validators.required])],
            course_of_study_id: ['', Validators.compose([Validators.required])],
            country_id: ['', Validators.compose([Validators.required])],
            state_id: '',
            matric_no: ['', Validators.compose([Validators.required])]

        }
        // email,first_name,last_name,course_of_study_id
    }


    static updateAdmissionListForm = function () {
        return {
            session_year: ['', Validators.compose([Validators.required])],

        }
    }

    constructor(private _script: ScriptLoaderService,
                private userService: UserService,
                private schoolService: SchoolService,
                private admissionsService: AdmissionsService,
                private authService: AuthenticationService,
                private fb: FormBuilder,
                private el: ElementRef,
                private Alert: NotificationService,
                private staffConfigService: StaffConfigService) {
        // this.authenticatedUser = this.userService.getAuthUser().login.user;
        this.createAdmissionListForm = this.fb.group(AdmissionListComponent.createAdmissionListForm());
        this.updateAdmissionListForm = this.fb.group(AdmissionListComponent.updateAdmissionListForm());
        this.dtOptions = {
            pagingType: 'full_numbers',
        };
        console.log("Authenticated user ", this.authenticatedUser);
    }

    ngOnInit() {
        this.getAllAdmissionLists();
        this.getAllSessions()
    }

    // ngAfterViewInit() {
    //     this._script.loadScripts('app-AdmissionLists',
    //         ['assets/AdmissionList_assets/demo/demo4/base/scripts.bundle.js', 'assets/AdmissionList_assets/app/js/dashboard.js']);

    // }


    public iAmPermitted(routeName):boolean{
        return this.authService.iAmPermitted(routeName);
    }

    public getAllSessions() {
        this.staffConfigService.getAllSession().subscribe(
            (allSessionsResponse) => {
                this.allSessions = allSessionsResponse.data;
                console.log('All sessions response ', allSessionsResponse);
            },
            (error) => {
                console.log('Could not load sessions');
            }
        );
    }


    public getAllAdmissionLists() {
        this.load.requesting.list = true;
        this.admissionsService.getAllAdmissions().subscribe(
            (allAdmissionListsResponse) => {
                this.load.requesting.list = false;
              this.allAdmissionLists = allAdmissionListsResponse.data;
                console.log('All admission listss shollal ',allAdmissionListsResponse);
                if (this.allAdmissionLists.length > 0) {
                    this.dtTrigger.next();
                }
                console.log("All AdmissionLists response", allAdmissionListsResponse)
            },
            (error) => {

                this.load.requesting.list = false;

            }
        )
    }

    public async createAdmissionList() {

        this.load.requesting.create = true;
        this.load.message.create = "Creating...";

        console.log("Create AdmissionList object ", this.createAdmissionListForm.value);
        await this.admissionsService.UpdateAdmission(this.createAdmissionListForm.value, 1).subscribe(
            (createdAdmissionListResponse) => {
                this.load.message.create = "Create";
                this.Alert.success(`${this.createAdmissionListForm.value.name} created successfully\n`);
                this.createAdmissionListForm = this.fb.group(AdmissionListComponent.createAdmissionListForm());
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'createAdmissionList');
                this.myAdmissionLists.push(createdAdmissionListResponse);
                console.log("Newly created school ", createdAdmissionListResponse)
            },
            (error) => {
                this.load.message.create = "Create";
                // this.triggerModal('close','createSchool');
                console.log("Eroorroroor ", error);
                this.load.requesting.create = false;
                this.Alert.error(`Could not create ${this.createAdmissionListForm.value.name}`, error)
            }
        )
    }

    public updateAdmissionList() {
        this.load.requesting.create = true;
        this.load.message.update = "Updating...";

        this.admissionsService.UpdateAdmission(this.updateAdmissionListForm.value, this.updatedAdmissionList.id).subscribe(
            (updatedAdmissionListResponse) => {
                this.load.message.update = "Update";
                this.updateAdmissionListForm = this.fb.group(AdmissionListComponent.updateAdmissionListForm());
                this.Alert.success(`${this.updateAdmissionListForm.value.name} updated successfully\n`);
                this.allAdmissionLists[this.editedIndex] = updatedAdmissionListResponse;
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'updateAdmissionList')

                console.log("Updated school ", this.updateAdmissionListForm)
            },
            (error) => {
                this.load.message.update = "Update";
                this.load.requesting.create = false;
                this.Alert.error(`Could not update ${this.updateAdmissionListForm.value.first_name}`, error)
            }
        )
    }



    public async createAdmissionListRecord(fileId) {

        this.load.requesting.create = true;
        this.load.message.create = 'Creating...';
        console.log('Create PUTME object ', this.updateAdmissionListForm.value);
        const bulkFileId = `#${fileId}`;
        const formFile = this.el.nativeElement.querySelector(bulkFileId);
        console.log('Form File ', formFile);
        const $linkId = $(bulkFileId);
        const $newLinkId = $linkId.val();
        const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
        // const file_type = '';
        $linkId.removeClass('alert alert-danger animated rubberBand');
        if (file_type && (file_type !== '.csv') && (file_type !== '.docx') && (file_type !== '.xls')&& (file_type !== '.xlsx')) {
            $linkId.addClass('alert alert-danger animated rubberBand');
            this.load.requesting.create = false;
            return this.Alert.error('You can only upload csv, xls, xlsx files');
        }
        await this.admissionsService.importAdmission(formFile, this.updateAdmissionListForm.value).subscribe(
            (fileResponse) => {
                // this.allPutmes = this.formatPutmeList(fileResponse['records']); uncomment this after Dammy fixes import response format
                this.Alert.success(`Admission list has been uploaded successfully\n`);
                this.updateAdmissionListForm = this.fb.group(AdmissionListComponent.updateAdmissionListForm());
                this.allAdmissionLists = [];    
                this.getAllAdmissionLists();
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'updateAdmissionList');
                console.log('File upload Response', fileResponse);
            },
            (error) => {
                this.load.requesting.create = false;
                this.Alert.error('An error occurred uploading file', error);
            }
        );
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.getAllAdmissionLists();
        });
      }

    //
    //     // await this.admissionsService.UpdateJambRecord(this.createJambRecordForm.value, 1).subscribe(
    //     //     (createdJambRecordResponse) => {
    //     //         this.load.message.create = 'Create';
    //     //         this.Alert.success(`${this.createJambRecordForm.value.name} created successfully\n`);
    //     //         this.createJambRecordForm = this.fb.group(JambRecordComponent.createJambRecordForm());
    //     //         this.load.requesting.create = false;
    //     //         this.triggerModalOrOverlay('close', 'createJambRecord');
    //     //         this.myJambRecords.push(createdJambRecordResponse);
    //     //         console.log('Newly created school ', createdJambRecordResponse);
    //     //     },
    //     //     (error) => {
    //     //         this.load.message.create = 'Create';
    //     //         // this.triggerModal('close','createSchool');
    //     //         console.log('Eroorroroor ', error);
    //     //         this.load.requesting.create = false;
    //     //         this.Alert.error(`Could not create ${this.createJambRecordForm.value.name}`, error);
    //     //     }
    //     // );
    // }


    public getAllFaculties() {
        this.schoolService.getAllFaculties().subscribe(
            (facultiesResponse) => {
                console.log("All faculties ", facultiesResponse);
                this.allFaculties = facultiesResponse;
                console.log('All fuculties response on admission list ', this.allFaculties);
            },
            (error) => {
                this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
            }
        )
    }

    public downloadTemplate(ind, buttonId: string) {
        this.admissionsService.getAdmissionListTemplate().subscribe((templateUrl) => {
                // this.feedBack.loader = false;
                this.templateUrl = templateUrl;

                const downloadButton = $(`#${buttonId}`);
                downloadButton.attr('href', this.templateUrl);
                downloadButton[0].click();
                console.log('Download button', downloadButton);
            },
            (error) => {
                // this.load.download.loading = false;
                this.Alert.error('An error occured, could not download template', error);
            });
    }

    /**
     * This method returns a single faculty which contains a list of departments
     */
    public getDepartmentByByFacultyId(facultyId) {
        this.load.departments.list = true;
        this.load.departments.loading = true;

        this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
            (departmentResponse) => {
                this.load.departments.loading = false;

                this.allDepartments = departmentResponse.departments;
                console.log("returned departments", departmentResponse);
            },
            (error) => {
                this.load.departments.list = false;
                this.load.departments.loading = false;

                this.Alert.error(`Sorry could not load departments`, error)
            }
        )
    }

    public async getAllCourses() {
        await this.schoolService.getAllCourses().subscribe(
            (allCoursesResponse) => {
                this.allCourses = allCoursesResponse.data;
                this.updatedAdmissionList.loading = false;
            },
            (error) => {
                this.updatedAdmissionList.loading = false;
                this.Alert.error("Sorry, could not load school courses", error);
            })
    }

    public getCourseByDepartmentId(departmentId) {
        this.load.courses.list = true;
        this.load.courses.loading = true;
        this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
            (coursesResponse) => {
                this.load.courses.loading = false;

                this.allCourses = coursesResponse.course_of_studies;
                console.log("returned courses", coursesResponse);
            },
            (error) => {
                this.load.courses.list = false;
                this.load.courses.loading = false;
                this.Alert.error(`Sorry could not load courses`, error)
            }
        )
    }

    public getAllCountries() {
        this.schoolService.getAllCountries().subscribe(
            (countriesResponse) => {
                this.allCountries = countriesResponse;
                console.log("returned countries", countriesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load countries`, error)
            }
        )
    }

    public getStateByCountryId(countryId) {
        this.schoolService.getStateByCountryId(countryId).subscribe(
            (statesResponse) => {
                this.allStates = statesResponse.states;
                console.log("returned States", statesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load States`, error)
            }
        )
    }


    public logStaffIn() {
        this.schoolService.logStaffIn().subscribe(
            (loginResponse) => {
                this.userService.setAuthUser(loginResponse.token);
                this.getAllAdmissionLists();
                this.getAllCountries();
                this.getAllFaculties();


            }
        )
    }

    public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        if (ind >= 0) {
            this.allAdmissionLists[ind].loading = true;
            this.getAllCourses();
            this.updatedAdmissionList = this.allAdmissionLists[ind];
            const AdmissionListManagementObject = {
                first_name: this.updatedAdmissionList.first_name,
                last_name: this.updatedAdmissionList.last_name,
                middle_name: "",
                matric_no: this.updatedAdmissionList.matric_no,
                email: this.updatedAdmissionList.email,
                course_of_study_id: this.updatedAdmissionList.course_of_study.id,
                department_id: this.updatedAdmissionList.course_of_study.department.id,
                faculty_id: this.updatedAdmissionList.course_of_study.department.faculty.id,
                country_id: 1,
                state_id: 2
            }
            this.updateAdmissionListForm.patchValue(AdmissionListManagementObject);
            console.log("AdmissionList management object ", AdmissionListManagementObject);

            // this.updatedAdmissionList.loading=false

        }
        (action === "open") ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
        // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
        // });

    }


}
