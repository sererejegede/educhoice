import {Component, OnInit, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import * as utils from '../../../../../../../utils/util';
import {Faculty} from '../../../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../../../interfaces/department.interface';
import {Course} from '../../../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../../../services/school.service';
import {UserService} from '../../../../../../../services/user.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {AdmissionsService} from '../../../../../../../services/api-service/admissions.service';
import {StaffConfigService} from "../../../../../../../services/api-service/staff-config.service";
import {Cache} from "../../../../../../../utils/cache";
import {DomSanitizer} from '@angular/platform-browser';
import * as JSPDF from 'jspdf';

declare const $: any;
declare const wysihtml5: any;

@Component({
    selector: 'app-letter-types',
    templateUrl: './letter-types.component.html',
    styleUrls: ['./letter-types.component.css']
})
export class LetterTypesComponent implements OnInit {

    @ViewChild('content') content: ElementRef;
    public overlay = utils.overlay;
    public allAdmissionLetters: any[] = [];
    public selectedAdmissionVariables: any[] = [];
    public updatedAdmissionLetter: any;
    public appSettings: any;
    public imgUrl: any;
    public allFaculties: Faculty[] = [];
    public allDepartments: Department[] = [];
    public allCourses: Course[] = [];
    public allProgrammes: any[] = [];
    public allStates: any[] = [];
    public allCountries: any[] = [];
    public authenticatedUser: any;
    public admissionLetter: FormGroup;
    public editedIndex: number;
    public feedBack = {
        print_loader: false
    }
    public admissionLetterVariables = {
        school: {
            value: "{{schoolName}}",
            text: ""
        },

        student: {
            text: "",
            value: "{{studentName}}"
        },

        department: {
            text: "",
            value: "{{departmentName}}"
        },

        session: {
            text: "",
            value: "{{session}}"
        },
        matric_no:
            {
                text: "",
                value: "{{matricNo}}"
            },
        faculty: {
            text: "Faculty",
            value: "{{facultyName}}"
        }
    };

    public load = {
        requesting: {
            list: true,
            create: false
        },
        message: {
            create: 'Create',
            update: 'Update'
        },
        departments: {
            list: false,
            loading: false
        },
        courses: {
            list: false,
            loading: false
        },
        AdmissionLetters: {
            list: false,
            loading: false
        }

    };
    ckeditorContent = '';
    public options: Object = {
        placeholderText: 'vuymbkjn',
        charCounterCount: false,
        dragInline: true
    };


    static admissionLetter = function () {
        return {
            programme_id: ['', Validators.compose([Validators.required])],

        };
    };

    constructor(private userService: UserService,
                private schoolService: SchoolService,
                private admissionsService: AdmissionsService,
                private fb: FormBuilder,
                private Alert: NotificationService,
                private sanitizer: DomSanitizer,
                private staffConfigService: StaffConfigService) {

        this.appSettings = Cache.get('app_settings');

        this.admissionLetter = this.fb.group(LetterTypesComponent.admissionLetter());
        console.log('Authenticated user ', this.authenticatedUser);

    }

    ngOnInit() {
        this.getAllAdmissionLetters();
        this.getAllProgrammes();
    }


    public getAllAdmissionLetters() {
        this.load.requesting.list = true;
        this.admissionsService.getAllAdmissionLetters()
            .subscribe((allAdmissionLettersResponse) => {
                    console.log(allAdmissionLettersResponse);
                    this.load.requesting.list = false;
                    this.allAdmissionLetters = allAdmissionLettersResponse['data'];
                    console.log('All AdmissionLetters response', allAdmissionLettersResponse);
                },
                (error) => {
                    console.log('Admission letters error', error);
                    this.load.requesting.list = false;
                }
            );
    }

    public getAllFaculties() {
        this.schoolService.getAllFaculties().subscribe(
            (facultiesResponse) => {
                console.log('All faculties ', facultiesResponse);
                this.allFaculties = facultiesResponse;
            },
            (error) => {
                this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
            }
        );
    }

    public async getAllProgrammes() {

        await  this.staffConfigService.getAllProgramme().subscribe((allProgrammesResponse) => {
                this.allProgrammes = allProgrammesResponse['data'];
                // console.log('All programmes response ', allProgrammesResponse);
                // console.log('All applications and programmes');
            },
            (error) => {
                console.log('All programmes response ', error);
            });
    }

    /**
     * This method returns a single faculty which contains a list of departments
     */
    public getDepartmentByByFacultyId(facultyId) {
        this.load.departments.list = true;
        this.load.departments.loading = true;

        this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
            (departmentResponse) => {
                this.load.departments.loading = false;

                this.allDepartments = departmentResponse.departments;
                console.log('returned departments', departmentResponse);
            },
            (error) => {
                this.load.departments.list = false;
                this.load.departments.loading = false;

                this.Alert.error(`Sorry could not load departments`, error);
            }
        );
    }

    public async getAllCourses() {
        await this.schoolService.getAllCourses().subscribe(
            (allCoursesResponse) => {
                this.allCourses = allCoursesResponse.data;
                this.updatedAdmissionLetter.loading = false;
            },
            (error) => {
                this.updatedAdmissionLetter.loading = false;
                this.Alert.error('Sorry, could not load school courses', error);
            });
    }

    public getCourseByDepartmentId(departmentId) {
        this.load.courses.list = true;
        this.load.courses.loading = true;
        this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
            (coursesResponse) => {
                this.load.courses.loading = false;

                this.allCourses = coursesResponse.course_of_studies;
                console.log('returned courses', coursesResponse);
            },
            (error) => {
                this.load.courses.list = false;
                this.load.courses.loading = false;
                this.Alert.error(`Sorry could not load courses`, error);
            }
        );
    }

    public getAllCountries() {
        this.schoolService.getAllCountries().subscribe(
            (countriesResponse) => {
                this.allCountries = countriesResponse;
                console.log('returned countries', countriesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load countries`, error);
            }
        );
    }

    public getStateByCountryId(countryId) {
        this.schoolService.getStateByCountryId(countryId).subscribe(
            (statesResponse) => {
                this.allStates = statesResponse.states;
                console.log('returned States', statesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load States`, error);
            }
        );
    }

    public showInput(programme_id) {
        let letterBody = (this.selectWhereId(this.allAdmissionLetters, 'programme_id', programme_id)) ? this.selectWhereId(this.allAdmissionLetters, 'programme_id', programme_id)['body'] : "";
        this.admissionLetterVariables.school.text = "Bowen";
        this.admissionLetterVariables.department.text = "Medical Mathematics";
        this.admissionLetterVariables.faculty.text = "Medmath";
        this.admissionLetterVariables.student.text = "John Doe";
        this.admissionLetterVariables.matric_no.text = "U2009/poyky/65";
        this.admissionLetterVariables.session.text = '1824';
        for (let key in this.admissionLetterVariables) {
            if (letterBody.includes(this.admissionLetterVariables[key]['value'])) {
                console.log(true, this.admissionLetterVariables[key]['value'])
                letterBody = letterBody.replace(this.admissionLetterVariables[key]['value'], this.admissionLetterVariables[key]['text']);
            }
        }
        console.log('letet gdggdh ', letterBody);

        this.ckeditorContent = (letterBody) ? letterBody : '<b>No admission letter has been created for this programme.</b>';
    }

    // public printLetter() {
    //     console.log('This app settings ', this.appSettings);
    //     const imgUrl = this.appSettings['settings']['logo'];
    //     this.imgUrl = this.sanitizer.bypassSecurityTrustResourceUrl(imgUrl);
    //    //  let letterDoc = new JSPDF();
    //    //  let specialElementHandlers = {
    //    //      '#editor':  (element, renderer)=> {
    //    //          return true;
    //    //      }
    //    //  };
    //    //  let content = this.content.nativeElement;
    //    //  letterDoc.fromHTML(content.innerHTML, 15, 15, {
    //    //      'width': 190,
    //    //      'elementHandlers': specialElementHandlers
    //    //  });
    //    //  // letterDoc.save('testletter.pdf');
    //    // let printDocument = window.open(letterDoc.output('bloburl'), '_blank');
    //     // alert(letterDownload)
    //     // const printableDiv = $(printArea);
    //     // window.document.write(printableDiv.html());
    //     // window.document.write('testletter.pdf');
    //     // printDocument.print();
    //
    // }
    public printLetter(id, title?) {
        this.feedBack.print_loader = true;
        const that = this;
        const contents = $('#' + id).html();
        const frame1 = $('<iframe />');
        frame1[0]['name'] = 'frame1';
        frame1.css({'position': 'absolute', 'top': '-1000000px', 'pointer-events': 'none'});
        $('body').append(frame1);
        const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
        frameDoc.document.open();
        // frameDoc.document.write('<html><head><title>' + title + '</title>');
        frameDoc.document.write('</head><body>');
        frameDoc.document.write('<link href="./paymentComponent.css" rel="stylesheet">');
        frameDoc.document.write('<style>.water-mark {-webkit-filter: grayscale(100%);filter: grayscale(100%);opacity: .095;position: absolute;top: 0;right: 0;left: 0;bottom: 0;} .relative {position: relative;}</style>');
        frameDoc.document.write('<style>.img-div {width: 100px;height: 100px;margin: 0 auto;}.img-div img {max-width: 100%;max-height: 100px;display: block;margin: auto;} .text-center {text-align: center !important;} .font-weight-bold {font-weight: 700 !important; } .my-3 {margin-top: 1rem !important; }</style>');
        frameDoc.document.write('<style>.table {width: 100%; border-collapse: collapse !important; }.table td,.table th {background-color: #fff !important; }.table-bordered th,.table-bordered td {border: 1px solid #ddd !important; } .text-left{text-align:left}</style>');
        frameDoc.document.write('<style>table-bordered{border:1px solid #ddd}.table-bordered>thead>tr>th,.table-bordered>tbody>tr>th,.table-bordered>tfoot>tr>th,.table-bordered>thead>tr>td,.table-bordered>tbody>tr>td,.table-bordered>tfoot>tr>td{border:1px solid #ddd}.table-bordered>thead>tr>th,.table-bordered>thead>tr>td{border-bottom-width:2px}</style>');
        frameDoc.document.write(contents);
        frameDoc.document.write('</body>');
        frameDoc.document.write('</html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames['frame1'].focus();
            window.frames['frame1'].print();
            frame1.remove();
            that.feedBack.print_loader = false;
        }, 3000);
    }


    public logStaffIn() {
        this.schoolService.logStaffIn().subscribe(
            (loginResponse) => {
                this.userService.setAuthUser(loginResponse.token);


            }
        );
    }

    public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        if (ind) {
            this.saveVariables();
        }
        (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
        // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
        // });

    }

    public setLetterLogo(){
        return {
            background: `url(${this.appSettings.settings.logo}) no-repeat`,
            'background-size':'fit'
        }
    }


    public saveVariables() {
        this.ckeditorContent = this.selectedAdmissionVariables.toString();
        if (this.selectedAdmissionVariables.length === 0) {
            return this.Alert.error('You have not selected any admission variables!');
        }
        this.triggerModalOrOverlay('close', 'imageInfo');
    }

    public onChange(newEvent) {
        console.log('Editor on changed ', newEvent);

    }


    public onBlur(newEvent) {
        console.log('Editor on blur ', newEvent);

    }


    public onReady(newEvent) {
        console.log('Editor ready changed ', newEvent);

    }

    public onEditorChange(newEvent) {
        console.log('Editor changed ', newEvent);
    }

    public selectWhereId(data: any[], key, id) {
        let dataItem: any[] = [];
        data.forEach(item => {
            let itemKey = parseInt(item[key]);
            let itemId = parseInt(id);
            if (itemKey === itemId) {
                dataItem.push(item);
            }
        });
        return dataItem[0];

    }
}
