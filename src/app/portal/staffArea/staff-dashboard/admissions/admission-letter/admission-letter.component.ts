import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import * as utils from '../../../../../../utils/util';
import {Faculty} from '../../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../../interfaces/department.interface';
import {Course} from '../../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../../services/school.service';
import {UserService} from '../../../../../../services/user.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {AdmissionsService} from '../../../../../../services/api-service/admissions.service';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {AuthenticationService} from '../../../../../../services/authentication.service';

declare const $: any;
@Component({
  selector: 'app-admission-letter',
  templateUrl: './admission-letter.component.html',
  styleUrls: ['./admission-letter.component.css']
})
export class AdmissionLetterComponent implements OnInit {

  public overlay = utils.overlay;
  public allAdmissionLetters: any[] = [];
  public selectedAdmissionVariables: any[] = [];
  public updatedAdmissionLetter: any;
  public allFaculties: Faculty[] = [];
  public allDepartments: Department[] = [];
  public allCourses: Course[] = [];
  public allProgrammes: any[] = [];
  public allProgrammeIds: number[] = [];
  public allStates: any[] = [];
  public allCountries: any[] = [];
  public authenticatedUser: any;
  public selectedAdmissionLetter: any;
  public myAdmissionLetters: any[] = [];
  public myEditedAdmissionLetter: any = {};
  public createAdmissionLetterForm: FormGroup;
  public admissionLetter: FormGroup;
  public updateAdmissionLetterForm: FormGroup;
  public editedIndex: number;
  programmeId;
  public admissionLetterVariables = [
    {
      label: 'School Name',
      value: '{{schoolName}}'
    },
    {
      label: 'Student Name',
      value: '{{studentName}}'
    },
    {
      label: 'Department',
      value: '{{departmentName}}'
    },
    {
      label: 'Course of Study',
      value: '{{courseOfStudyName}}'
    },
    {
      label: 'Fee Amount',
      value: '{{feeAmount}}'
    },
    {
      label: 'Session',
      value: '{{session}}'
    },
    {
      label: 'Matriculation Number',
      value: '{{matricNo}}'
    },
    {
      label: 'Faculty',
      value: '{{facultyName}}'
    }
  ];


  // public options: any = {
  //     removeOnSpill: true
  // };
  public load = {
    requesting: {
      list: true,
      create: false
    },
    message: {
      create: 'Create',
      update: 'Update'
    },
    departments: {
      list: false,
      loading: false
    },
    courses: {
      list: false,
      loading: false
    },
    AdmissionLetters: {
      list: false,
      loading: false
    }

  };
  ckeditorContent = '';
  public options: Object = {
    placeholderText: 'vuymbkjn',
    charCounterCount: false,
    dragInline: true
  };
  public showInputText = '';
  public testText = '';

  static createAdmissionLetterForm = function () {
    return {
      email: ['', Validators.compose([Validators.required])],
      first_name: ['', Validators.compose([Validators.required])],
      middle_name: '',
      last_name: ['', Validators.compose([Validators.required])],
      faculty_id: ['', Validators.compose([Validators.required])],
      department_id: ['', Validators.compose([Validators.required])],
      course_of_study_id: ['', Validators.compose([Validators.required])],
      country_id: ['', Validators.compose([Validators.required])],
      state_id: '',
      matric_no: ['', Validators.compose([Validators.required])]

    };
    // email,first_name,last_name,course_of_study_id
  };


  static updateAdmissionLetterForm = function () {
    return {
      email: ['', Validators.compose([Validators.required])],
      first_name: ['', Validators.compose([Validators.required])],
      middle_name: '',
      last_name: ['', Validators.compose([Validators.required])],
      faculty_id: ['', Validators.compose([Validators.required])],
      department_id: ['', Validators.compose([Validators.required])],
      course_of_study_id: ['', Validators.compose([Validators.required])],
      country_id: ['', Validators.compose([Validators.required])],
      state_id: '',
      matric_no: ['', Validators.compose([Validators.required])]
    };
  };

  static admissionLetter = function () {
    return {
      programme_id: ['', Validators.compose([Validators.required])],

    };
  };

  constructor(private authService: AuthenticationService,
              private userService: UserService,
              private schoolService: SchoolService,
              private admissionsService: AdmissionsService,
              private fb: FormBuilder,
              private Alert: NotificationService,
              private staffConfigService: StaffConfigService) {
    // this.authenticatedUser = this.userService.getAuthUser().login.user;
    // this.dragulaService.setOptions(
    //     'first-bag',
    //     {copy: true}
    // );
    this.createAdmissionLetterForm = this.fb.group(AdmissionLetterComponent.createAdmissionLetterForm());
    this.updateAdmissionLetterForm = this.fb.group(AdmissionLetterComponent.updateAdmissionLetterForm());
    this.admissionLetter = this.fb.group(AdmissionLetterComponent.admissionLetter());
    console.log('Authenticated user ', this.authenticatedUser);

  }

  ngOnInit() {
    this.getAllAdmissionLetters();
    this.getAllProgrammes();
  }

  ngAfterViewInit() {


  }



  public getAllAdmissionLetters() {
    this.load.requesting.list = true;
    this.admissionsService.getAllAdmissionLetters().subscribe(
        (allAdmissionLettersResponse) => {
          this.load.requesting.list = false;
          this.allAdmissionLetters = allAdmissionLettersResponse;
const programmeIds: any[] = [];
this.allAdmissionLetters['data'].forEach((admissionLetter) => {
  programmeIds.push(admissionLetter.programme_id);
});
this.allProgrammeIds = programmeIds;
          console.log('All AdmissionLetters response', allAdmissionLettersResponse);
        },
        (error) => {

          this.load.requesting.list = false;

        }
    );
  }

  public async createAdmissionLetter(editorContent) {

    this.load.requesting.create = true;
    this.load.message.create = 'Creating...';
    let admissionLetterId = 0;
let message = 'created';
    const programmeId = (this.allProgrammeIds.includes(parseInt(editorContent['programme_id']))) ? editorContent['programme_id'] : '';
      if (programmeId) {
        admissionLetterId = this.selectWhereId(this.allAdmissionLetters['data'], 'programme_id', programmeId)['id'];
        message = 'updated';
    }

    console.log('Create AdmissionLetter object ', editorContent);
    await this.admissionsService.createAdmissionLetter(editorContent, admissionLetterId
    ).subscribe(
        (createdAdmissionLetterResponse) => {
          this.load.message.create = 'Create';
          this.Alert.success(`Admission Template ${message} successfully\n`);
          this.allAdmissionLetters = [];
          this.getAllAdmissionLetters();
          // this.createAdmissionLetterForm = this.fb.group(LetterTypesComponent.createAdmissionLetterForm());
          this.load.requesting.create = false;
          // this.triggerModalOrOverlay('close', 'createAdmissionLetter');
          // this.myAdmissionLetters.push(createdAdmissionLetterResponse);
          this.ckeditorContent = createdAdmissionLetterResponse['body'];
          console.log('Newly created school ', createdAdmissionLetterResponse);
        },
        (error) => {
          this.load.message.create = 'Create';
          // this.triggerModal('close','createSchool');
          console.log('Eroorroroor ', error);
          this.load.requesting.create = false;
          this.Alert.error(`Could not create admission template`, error);
        }
    );
  }

  public updateAdmissionLetter() {
    this.load.requesting.create = true;
    this.load.message.update = 'Updating...';

    this.admissionsService.UpdateAdmission(this.updateAdmissionLetterForm.value, this.updatedAdmissionLetter.id).subscribe(
        (updatedAdmissionLetterResponse) => {
          this.load.message.update = 'Update';
          this.updateAdmissionLetterForm = this.fb.group(AdmissionLetterComponent.updateAdmissionLetterForm());
          this.Alert.success(`${this.updateAdmissionLetterForm.value.name} updated successfully\n`);
          this.allAdmissionLetters[this.editedIndex] = updatedAdmissionLetterResponse;
          this.load.requesting.create = false;
          this.triggerModalOrOverlay('close', 'updateAdmissionLetter');

          console.log('Updated school ', this.updateAdmissionLetterForm);
        },
        (error) => {
          this.load.message.update = 'Update';
          this.load.requesting.create = false;
          this.Alert.error(`Could not update ${this.updateAdmissionLetterForm.value.first_name}`, error);
        }
    );
  }


  public getAllFaculties() {
    this.schoolService.getAllFaculties().subscribe(
        (facultiesResponse) => {
          console.log('All faculties ', facultiesResponse);
          this.allFaculties = facultiesResponse;
        },
        (error) => {
          this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
        }
    );
  }

  public async getAllProgrammes() {

    await  this.staffConfigService.getAllProgramme().subscribe((allProgrammesResponse) => {
          this.allProgrammes = allProgrammesResponse['data'];
          console.log('All programmes response ', allProgrammesResponse);

          console.log('All applications and programmes');

        },
        (error) => {
          console.log('All programmes response ', error);
        });
  }

  /**
   * This method returns a single faculty which contains a list of departments
   */
  public getDepartmentByByFacultyId(facultyId) {
    this.load.departments.list = true;
    this.load.departments.loading = true;

    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
        (departmentResponse) => {
          this.load.departments.loading = false;

          this.allDepartments = departmentResponse.departments;
          console.log('returned departments', departmentResponse);
        },
        (error) => {
          this.load.departments.list = false;
          this.load.departments.loading = false;

          this.Alert.error(`Sorry could not load departments`, error);
        }
    );
  }

  public async getAllCourses() {
    await this.schoolService.getAllCourses().subscribe(
        (allCoursesResponse) => {
          this.allCourses = allCoursesResponse.data;
          this.updatedAdmissionLetter.loading = false;
        },
        (error) => {
          this.updatedAdmissionLetter.loading = false;
          this.Alert.error('Sorry, could not load school courses', error);
        });
  }

  public getCourseByDepartmentId(departmentId) {
    this.load.courses.list = true;
    this.load.courses.loading = true;
    this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
        (coursesResponse) => {
          this.load.courses.loading = false;

          this.allCourses = coursesResponse.course_of_studies;
          console.log('returned courses', coursesResponse);
        },
        (error) => {
          this.load.courses.list = false;
          this.load.courses.loading = false;
          this.Alert.error(`Sorry could not load courses`, error);
        }
    );
  }

  public getAllCountries() {
    this.schoolService.getAllCountries().subscribe(
        (countriesResponse) => {
          this.allCountries = countriesResponse;
          console.log('returned countries', countriesResponse);
        },
        (error) => {
          this.Alert.error(`Sorry could not load countries`, error);
        }
    );
  }

  public getStateByCountryId(countryId) {
    this.schoolService.getStateByCountryId(countryId).subscribe(
        (statesResponse) => {
          this.allStates = statesResponse.states;
          console.log('returned States', statesResponse);
        },
        (error) => {
          this.Alert.error(`Sorry could not load States`, error);
        }
    );
  }

  public showInput(id) {
    this.programmeId = id;
    //  let letterBody = this.selectWhereId(this.allAdmissionLetters,
    //       'programme_id',this.programmeId)['body'];
    //
    let letterBody = '';
    this.allAdmissionLetters['data'].forEach((letter) => {
      // console.log('Programme Id:::', letter.programme_id, 'Letter body', letter.body);
      if (letter.programme_id.toString() === id.toString()) {
        letterBody = letter.body;
      }
    });
    // this.showInputText = letterBody;
    this.ckeditorContent = letterBody;
  }

  displayLetter(value) {
    if (this.allAdmissionLetters['programme_id'] === value) {
      this.selectedAdmissionLetter = this.allAdmissionLetters['body'];
    }
  }

  public logStaffIn() {
    this.schoolService.logStaffIn().subscribe(
        (loginResponse) => {
          this.userService.setAuthUser(loginResponse.token);


        }
    );
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    if (ind) {
      this.saveVariables();
    }
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

  public selectAdmissionVariable(ind) {
    const admissionVariable = this.admissionLetterVariables[ind].value;
    const variableIndex = this.selectedAdmissionVariables.indexOf(admissionVariable);
    //noinspection TypeScriptUnresolvedFunction
    if (!this.selectedAdmissionVariables.includes(admissionVariable)) {
      this.selectedAdmissionVariables.push(admissionVariable);
    } else {
      this.selectedAdmissionVariables.splice(variableIndex, 1);

    }
  }

  public saveContent(ck) {
    const contentValue = ck['_value'];
    if (this.ckeditorContent.length > 0) {
      const admissionTemplateObject = {
        programme_id: this.programmeId,
        body: contentValue
      };
      console.log(admissionTemplateObject.programme_id);
      // console.log("Ck object value from save content", contentValue);
      this.createAdmissionLetter(admissionTemplateObject);
    }

  }

  public saveVariables() {
    this.ckeditorContent += this.selectedAdmissionVariables.toString();
    if (this.selectedAdmissionVariables.length === 0) {
      return this.Alert.error('You have not selected any admission variables!');
    }
    this.triggerModalOrOverlay('close', 'imageInfo');
  }

    public selectWhereId(data: any[], search_key, id) {
        const dataItem: any[] = [];
        data.forEach(item => {
            const itemKey = parseInt(item[search_key]);
            const itemId = parseInt(id);
            if (itemKey === itemId) {
                dataItem.push(item);
            }
        });
        return dataItem[0];

    }

  public onChange(newEvent) {
    // console.log('Edito on changed ', newEvent);

  }


  public onBlur(newEvent) {
    // console.log('Editor on blur ', newEvent);

  }


  public onReady(newEvent) {
    // console.log('Editor ready changed ', newEvent);

  }

  public onEditorChange(newEvent) {
    // console.log('Editor changed ', newEvent);
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

}
