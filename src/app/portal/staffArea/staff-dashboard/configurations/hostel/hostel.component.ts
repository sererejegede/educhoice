import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {AuthenticationService} from '../../../../../../services/authentication.service';
import {Cache} from '../../../../../../utils/cache';
import {JSONPARSE} from '../../../../../../utils/json-parse';
import {Router} from "@angular/router";
import {SchoolService} from "../../../../../../services/school.service";

declare const $;

@Component({
  selector: 'app-hostel',
  templateUrl: './hostel.component.html',
  styleUrls: ['./hostel.component.css']
})
export class HostelComponent implements OnInit {
  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);


  public createForm: FormGroup;
  public optionalSettingsForm: FormGroup;
  public configAssignments: any[] = [];

  public id: number;
  public allFaculties: any[] = [];
  public allLgas: any[] = [];
  public allDepartments: any[] = [];
  public allCourses: any[] = [];
  public allDegrees: any[] = [];
  public allLevels: any[] = [];
  public loadSessions: boolean = false;
  public allSession: any[] = [];
  public allStates: any[] = [];
  public allCountries: any[] = [];
  public allSessions: any[] = [];
  public allProgrammes: any[] = [];
  public allProgrammeTypes: any[] = [];


  public countryId = 0;
  public departmentId = 0;
  public facultyId = 0;
  public programmeId = 0;
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    allHostel: [],
    moduleName: 'Hostel',
    formType: 'Create',
    loader: false,
    submitStatus: false,
    showUpdateButton: false,
    deleteStatus: false
  };
  public hostelSettingObject: any = {};
  public load = {
    requesting: {
      list: false,
      create: false,
      listItems: false
    },
    fee: {
      transcript: false
    },
    message: {
      create: 'Create',
      update: 'Update'
    },
    departments: {
      list: false,
      loading: false
    },
    session: {
      list: false,
      loading: false
    },
    states: {
      loading: false
    },
    lgas: {
      loading: false
    },
    courses: {
      list: false,
      loading: false
    },
    fees: {
      list: false,
      loading: false,
    },
    degree: {
      list: false
    },
    part_payment: {
      list: false,
      create: false,
      update: false
    }
  };
  static formData = function () {
    return {
      hostel_name: ['', Validators.compose([Validators.required])],
      hostel_capacity: ['', Validators.compose([Validators.required])],
      hostel_address: [''],
      hostel_gender: ['', Validators.compose([Validators.required])],
      hostel_amount: ['', Validators.compose([Validators.required])],
    };
  };

  static optionalSettingsForm = function () {
    return {
      country_id: '',
      state_id: '',
      degree_id: '',
      level_id: '',
      department_id: '',
      faculty_id: '',
      course_of_study_id: '',
      programme_id: '',
      mode_of_entry: '',
      student_type: '',
      programme_type_id: '',
    }
  }


  ngOnInit() {
    this.loadFunction();
  }

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private authService: AuthenticationService,
              private schoolService: SchoolService,
              private router: Router,
              private fb: FormBuilder) {
    this.createForm = this.fb.group(HostelComponent.formData());
    this.optionalSettingsForm = this.fb.group(HostelComponent.optionalSettingsForm());
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
  }

  loadFunction() {
    this.getAllHostel();
    this.getAllFaculties();
    this.getAllCourses();
    this.getAllCountries();
    this.getAllProgrammes();
    this.getAllLevels();
    this.getAllDegrees();
    this.allProgrammeType();
  }

  /**
   * creating
   */
  public createFormModal() {
    this.feedBack.submitStatus = true;
    this.createForm.value['hostel_gender'] = (this.createForm.value['hostel_gender'] == 'general') ? '' : this.createForm.value['hostel_gender'];

    const assignmentsObject = {
      assignments: this.configAssignments
    };

    const hostelObject = Object.assign(assignmentsObject, this.createForm.value);
    this.staffConfigService.createAllHostel(hostelObject)
      .subscribe((response) => {
          (this.feedBack.allResponse.length === 0) ? this.feedBack.allResponse.push(response) : this.feedBack.allResponse.unshift(response);
          this.configAssignments = [];
          this.createForm.enable();
          this.notification.success(this.feedBack.moduleName + ' was created successfully');
          this.callBackFunction();
        },
        error => {
          this.feedBack.submitStatus = false;
          const error_message = JSONPARSE(error._body, 'date_start') || 'Unable to Create ' + this.feedBack.moduleName + ', please retry';
          this.notification.error(error_message, error);
        });
  }

  private callBackFunction() {
    this.feedBack.submitStatus = false;
    this.createForm.reset();
    this.closeModal();
  }

  /**
   * update
   */
  public updateFormModal() {
    this.feedBack.submitStatus = true;
    this.createForm.value['hostel_gender'] = (this.createForm.value['hostel_gender'] === 'general') ? '' : this.createForm.value['hostel_gender'];
    const to_send = {...this.createForm.value, ...{assignments: this.configAssignments}};
    this.staffConfigService.updateAllHostel(this.id, to_send)
      .subscribe((response) => {
          this.feedBack.allResponse.forEach((val, i) => {
            if (val.id === this.id) {
              this.feedBack.allResponse[i] = response;
              this.notification.success(this.feedBack.moduleName + ' was updated successfully');
            }
          });
          this.callBackFunction();
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to Update ' + this.feedBack.moduleName + ', please retry', error);
        });
  }

  /**
   * listing all current session
   */
  public getAllHostel() {
    this.feedBack.loader = true;
    this.staffConfigService.getAllHostel1()
      .subscribe((response) => {
          this.feedBack.allResponse = response.data;
          if (this.feedBack.allResponse.length > 0) {
            this.dtTrigger.next();
          }
          this.feedBack.loader = false;
        },
        error => {
          this.notification.error('Unable to load Hostels, please retry');
          this.feedBack.loader = false;
        });
  }


  /**
   * editting data
   */
  onEdit(data) {
    this.id = data.id;
    data['hostel_gender'] = (!data['hostel_gender']) ? 'general' : data['hostel_gender'];
    data['gender'] = data['hostel_gender'].toLowerCase();
    this.feedBack.formType = 'Update';
    this.createForm = this.fb.group(data);
    this.openModal('Update');
    this.feedBack.showUpdateButton = true;
  }

  /**
   * FOR Viewing details
   */
  onView(data) {
    this.feedBack.viewDetails = data;
    $('#viewModal').modal('show');
  }


  /**
   * open modal
   */
  openModal(type = 'Create') {
    if (type === 'Create') {
      this.createForm.reset({
          hostel_name: '',
          hostel_gender: '',
          hostel_capacity: '',
          hostel_address: '',
          hostel_amount: ''
        },
      );
      this.feedBack.formType = 'Create';
    }
    $('#openModal').modal({show: true, backdrop: 'static', keyboard: false});
  }

  /**
   * close modal
   */
  closeModal() {
    $('#openModal').modal('hide');
    this.feedBack.formType = 'Create';
    this.feedBack.showUpdateButton = false;
  }


  onViewRooms(id) {
    this.router.navigate(['../staff/config/room', id]);
  }

  onDeleteRoom(id) {
    this.id = id;
    this.feedBack.deleteStatus = true;
    this.staffConfigService.deleteHostel(id)
      .subscribe((response) => {
          this.feedBack.allResponse.forEach((val, i) => {
            if (val.id === id) {
              this.notification.success('Hostel deleted successfully');
              const index = this.feedBack.allResponse.indexOf(this.feedBack.allResponse[i]);
              this.feedBack.allResponse.splice(index, 1);
              this.feedBack.deleteStatus = false;
            }
          });
        },
        error => {
          this.feedBack.deleteStatus = false;
          this.notification.error('Unable to delete Hostel', error);
        });
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal({
      show: true,
      backdrop: 'static',
      keyboard: false
    }) : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

  public getDepartmentByFacultyId(facultyId) {
    this.facultyId = facultyId;
    this.load.departments.list = true;
    this.load.departments.loading = true;
    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
      (departmentResponse) => {
        this.load.departments.loading = false;
        this.allDepartments = departmentResponse.departments;
        // console.log('returned departments', departmentResponse);
      },
      (error) => {
        this.load.departments.list = false;
        this.load.departments.loading = false;

        this.notification.error(`Sorry could not load departments`, error);
      }
    );
  }


  public getCourseByDepartmentId(departmentId) {
    this.departmentId = departmentId;
    this.load.courses.list = true;
    this.load.courses.loading = true;
    this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
      (coursesResponse) => {
        this.load.courses.loading = false;

        this.allCourses = coursesResponse.course_of_studies;
        // console.log('returned courses', coursesResponse);
      },
      (error) => {
        this.load.courses.list = false;
        this.load.courses.loading = false;
        this.notification.error(`Sorry could not load courses`, error);
      }
    );
  }


  public async getAllCourses() {
    await this.schoolService.getAllCourses().subscribe(
      (allCoursesResponse) => {
        this.allCourses = allCoursesResponse.data;
        // this.updatedFee.loading = false;
      },
      (error) => {
        // this.updatedFee.loading = false;
        this.notification.error('Sorry, could not load school courses', error);
      });
  }

  public getAllCountries() {
    this.schoolService.getAllCountries().subscribe(
      (countriesResponse) => {
        this.allCountries = countriesResponse;
        // console.log("returned countries", countriesResponse);
      },
      (error) => {
        this.notification.error(`Sorry could not load countries`, error);
      }
    );
  }

  public getStateByCountryId(countryId) {
    this.countryId = countryId;
    this.load.states.loading = true;
    this.schoolService.getStateByCountryId(countryId).subscribe(
      (statesResponse) => {
        this.load.states.loading = false;
        this.allStates = statesResponse.states;
        // console.log('returned States', statesResponse);
      },
      (error) => {
        this.load.states.loading = false;
        this.notification.error(`Sorry could not load States`, error);
      }
    );
  }

  public getAllLevels() {
    this.staffConfigService.getAllLevel().subscribe(
      (levelsResponse) => {
        this.allLevels = levelsResponse.data;
        // console.log(levelsResponse);
      },
      (error) => {
        // console.log('Could not load levels');
      }
    );
  }

  public getAllProgrammes() {
    this.staffConfigService.getAllProgramme().subscribe(
      (programmeResponse) => {
        this.allProgrammes = programmeResponse.data;
        // console.log('returned programmes', programmeResponse);
      },
      (error) => {
        this.notification.error(`Sorry could not load Programmes`, error);
      }
    );
  }

  public getAllFaculties() {
    this.schoolService.getAllFaculties().subscribe(
      (facultiesResponse) => {
        // console.log('All faculties ', facultiesResponse);
        this.allFaculties = facultiesResponse;
      },
      (error) => {
        this.notification.error('Sorry, could not load faculties. Please reload page.', error);
      }
    );
  }

  public getAllDegrees() {
    this.load.degree.list = true;
    this.staffConfigService.getAllDegree()
      .subscribe((response) => {
          this.allDegrees = response.data;
          this.load.degree.list = false;
        },
        error => {
          this.notification.error('Could not load degrees ', error);
          this.load.degree.list = false;

        });
  }

  public allProgrammeType() {
    this.feedBack.loader = true;
    this.staffConfigService.getAllProgrammeType()
      .subscribe((response) => {
          console.log('response programme type ::', response);
          // Cache.set('all_programme_types', response.data);
          this.allProgrammeTypes = response.data;

        },
        error => {
          this.notification.error('Unable to load ' + this.feedBack.moduleName + ', please retry', error);
        });
  }


  public getProgrammeById(programmeId) {
    this.loadSessions = true;
    this.programmeId = programmeId;
    this.staffConfigService.getProgramme(this.programmeId)
      .subscribe((response) => {
          this.loadSessions = false;
          this.allLevels = response.levels
          // this.viewedProgramme = response; // Cache.get('faculty.' + this.id);
          // console.log('Viewed programme response ', response);
        },
        error => {
          this.loadSessions = false;
          this.notification.error('Could not load Programme', error);

        });
  }

  public addAssignment() {
    this.configAssignments.push(this.optionalSettingsForm.value);
    (this.createForm.valid) ? this.createForm.disable() : this.createForm.enable();
    this.optionalSettingsForm.reset();

    console.log('Configuration Assisgnments ', this.configAssignments);
  }

  public removeAssignment(index) {
    this.configAssignments.splice(index, 1);
  }

  public optionalSettingsIsEmpty(): boolean {
    const formContainsValueArray = [];
    const formValue = this.optionalSettingsForm.value;
    for (let key in formValue) {
      if (formValue[key]) {
        formContainsValueArray.push(formValue[key])
      }
    }
    return (formContainsValueArray.length == 0);
  }

  public selectWhereId(data: any[], search_key: string, id) {
    let dataItem: any[] = [];
    data.forEach(item => {
      let itemKey = parseInt(item[search_key]);
      let itemId = parseInt(id);
      if (itemKey === itemId) {
        dataItem.push(item);
      }
    });
    return dataItem[0];
  }


}
