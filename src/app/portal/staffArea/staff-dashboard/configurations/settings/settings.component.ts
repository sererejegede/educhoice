import {AfterViewChecked, Component, ElementRef, OnInit} from "@angular/core";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {StaffConfigService} from "../../../../../../services/api-service/staff-config.service";
import {NotificationService} from "../../../../../../services/notification.service";
import {AuthenticationService} from '../../../../../../services/authentication.service';
import {Cache} from "../../../../../../utils/cache";
import {EmitSettingsService} from "../../../../../../services/emitSettings.service";

declare const $;

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit, AfterViewChecked {

  public createForm: FormGroup;
  public allSettingsDisplay: any[] = [];
  private multiple_name;
  private multiple_names: any[] = [];
  private files = [];
  private countSubmits: number = 0;
  private totalSubmits: number = 0;
  public fileTypeIsValid = true;
  private multiple_object = [];
  private selectedPaymentMethods: any[] = [];
  private multiple_elements_values: any[] = [];
  private multiple_elements: any[] = [];
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    allSchoolType: [],
    formType: 'Create',
    loader: false,
    once_checked: false,
    submitStatus: false,
    moduleName: 'Settings',
    showUpdateButton: false
  };

  static formData = function () {
    return {
      part_payment_type: ['', Validators.compose([Validators.required])],
      part_payment_ratio: ['', Validators.compose([Validators.required])],
    };
  };

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private authService: AuthenticationService,
              private el: ElementRef,
              private fb: FormBuilder,
              private emitSettingsService: EmitSettingsService) {
    this.allSettings();

    this.createForm = new FormGroup({});
    // this.createForm = this.fb.group(SettingsComponent.formData());

  }

  ngOnInit() {
    this.loadFunction();
  }

  ngAfterViewChecked(): void {
    if (this.feedBack.once_checked) {
      this.checkCheckBoxes();
      this.feedBack.once_checked = false;
    }
  }

  loadFunction() {
  }

  private checkCheckBoxes() {
    const checkboxes = [];
      this.multiple_elements.forEach((multiple_element) => {
      checkboxes.push(document.querySelectorAll(`[name = ${multiple_element.name}`));
    });
    checkboxes.forEach((checkbox, i) => {
      if (this.multiple_elements.length > 0) {
        this.multiple_elements.forEach(multiple_element => {
          checkbox.forEach(check => {
            multiple_element.value.forEach(el => {
              if (check.value === el) {
                check.checked = true;
              }
            });
          });
        });
      }
    });
    this.multiple_object.forEach((single, i) => {
      this.files[i] = this.el.nativeElement.querySelectorAll(`[name = ${single.name}]`);
    });
  }

  private cookObject() {
    this.multiple_elements.forEach((multiple_element, i) => {
      this.createForm.value[multiple_element.name] = multiple_element.value;
    });
    // this.createForm.value[this.multiple_name] = this.selectedPaymentMethods;
    const settingsObjects = [];
    this.files.forEach((file, i) => {
      settingsObjects[i] = {
        data: this.createForm.value,
        formFile: file[0],
        name: this.multiple_object[i].name
      };
    });
    return settingsObjects;
  }

  public createFormModal() {
    this.feedBack.submitStatus = true;
    this.totalSubmits = this.cookObject().length;
    for (let i = 0; i < this.cookObject().length; i++) {
      const cookedObject = this.cookObject()[i].data;
      for (let key in cookedObject) {
        if (cookedObject[key] === true) {
          cookedObject[key] = 1;
        }
        else if (cookedObject[key] === false) {
          cookedObject[key] = 0;
        }
      }
      this.saveSettings(i);
    }

  }

  private saveSettings(cookedObjectIndex) {
    this.staffConfigService.createSettingsFile(this.cookObject()[cookedObjectIndex])
        .subscribe((response) => {
              this.feedBack.submitStatus = false;
              this.countSubmits += 1;
              if (this.countSubmits === 1) {
                Cache.set('app_settings', response);
                this.emitSettingsService.emitChange(response.settings.logo);
              }
              this.countSubmits = (this.countSubmits === this.totalSubmits) ? 0 : this.countSubmits;
              this.notification.success(this.feedBack.moduleName + ' successfully saved');
              this.scrollIntoView();
            },
            error => {
              this.feedBack.submitStatus = false;
              this.notification.error('Unable to Create ' + this.feedBack.moduleName + ', please retry', error);
            });
  }

  public allSettings() {
    this.feedBack.loader = true;
    this.staffConfigService.getAllSettings()
        .subscribe((response) => {
              Cache.set('all_settings', response);
              this.feedBack.allResponse = Cache.get('all_settings');
              this.processSettings(this.feedBack.allResponse);
            },
            error => {
              this.notification.error('Unable to load ' + this.feedBack.moduleName + ', please retry', error);
              this.feedBack.loader = false;
            });
  }

  public processSettings(settingsObject) {
    const allSettingsDisplay = [];
    for (let key in settingsObject) {
      const description = settingsObject[key]['desc'];
      const icon = settingsObject[key]['icon'];
      const elements = settingsObject[key]['elements'];
      const title = settingsObject[key]['title'];
      const displayObject = {
        desc: description,
        icon: icon,
        elements: elements,
        title: title
      };
      allSettingsDisplay.push(displayObject);
    }
    this.allSettingsDisplay = allSettingsDisplay;
    this.allSettingsDisplay.forEach(setting => {
      setting['elements'].forEach((element, i) => {
        if (element['rules'] === 'required') {
          this.createForm.addControl(element.name, new FormControl(element.value, Validators.required));
        } else {
          this.createForm.addControl(element.name, new FormControl(element.value));
        }
        if (element['type'] === 'file') {
          this.multiple_object.push(element);
          this.createForm.removeControl(element.name);
        }
        if (element['multiple']) {
          this.multiple_names.push(element['name']);
          this.multiple_elements.push(element);
          // this.selectedPaymentMethods = element['value'];
        }
      });
    });
    this.feedBack.loader = false;
    this.feedBack.once_checked = true;
  }

  public addToArray (name) {
    const checkboxes = this.el.nativeElement.querySelectorAll(`input[name=${name}]`);
    this.multiple_elements.forEach(multiple_element => {
      if (multiple_element.name === name) {
        multiple_element.value = [];
      }
    });
    this.multiple_elements.forEach((multiple_element, i) => {
      checkboxes.forEach(box => {
        if (multiple_element.name === box.name && box.checked) {
          multiple_element.value.push(box.value);
        }
      });
    });
  }

  public validateFile(fileNameId) {
    const $linkId = $(`#${fileNameId}`);
    const $newLinkId = $linkId.val();
    const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
    $linkId.removeClass('alert alert-danger animated rubberBand');
    if (file_type && (file_type !== '.png') && (file_type !== '.jpg') && (file_type !== '.jpeg')) {
      $linkId.addClass('alert alert-danger animated rubberBand');
      this.notification.error('You can only upload png, jpg or jpeg files');
      this.fileTypeIsValid = false;
      return;
      // $linkId.removeClass('alert alert-danger animated rubberBand');
    }
    this.fileTypeIsValid = true;
  }

  private scrollIntoView() {
    setTimeout(() => {
      this.el.nativeElement.querySelector('#showTop').scrollIntoView({behavior: 'smooth'});
    }, 300);
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {

    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }


}
