import {Component, OnInit, ViewChild} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {StaffConfigService} from "../../../../../../services/api-service/staff-config.service";
import {NotificationService} from "../../../../../../services/notification.service";
import {AuthenticationService} from '../../../../../../services/authentication.service';
import {Subject} from "rxjs/Subject";
import {DataTableDirective} from "angular-datatables";

declare const $: any;

@Component({
    selector: 'app-screening-center',
    templateUrl: './screening-center.component.html',
    styleUrls: ['./screening-center.component.css']
})
export class ScreeningCenterComponent implements OnInit {

    dtTrigger: Subject<any> = new Subject();
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;
    createCenter: FormGroup;
    updateCenter: FormGroup;
    centerResponse: any[] = [];
    selectedScreeningCenter;
    loader = {
        creating: false,
        listing: false,
        updating: false
    };
    static createScreeningCenterForm = function () {
        return {
            name: ['', Validators.compose([Validators.required])],
            location: ['', Validators.compose([Validators.required])]
        }
    };

    static updateScreeningCenterForm = function () {
        return {
            name: ['', Validators.compose([Validators.required])],
            location: ['', Validators.compose([Validators.required])]
        }
    };

    constructor(private fb: FormBuilder,
                private staffConfigService: StaffConfigService,
                private authService: AuthenticationService,
                private notification: NotificationService) {
        this.createCenter = this.fb.group(ScreeningCenterComponent.createScreeningCenterForm());
        this.updateCenter = this.fb.group(ScreeningCenterComponent.updateScreeningCenterForm())
    }

    ngOnInit() {
        this.getAllScreeningCenter();
    }

    createScreeningCenter() {
        this.loader.creating = true;
        this.staffConfigService.createScreeningCentre(this.createCenter.value)
            .subscribe((response) => {
                    this.loader.creating = false;
                this.centerResponse.unshift(response);
                    console.log('Create Screening Center', response);
                    this.triggerModalOrOverlay('close', 'createScreeningCenter');
                    this.notification.success('Success!');
                    this.rerender();
                },
                (error => {
                    this.loader.creating = false;
                    this.notification.error('Could not create', error);
                    console.log('Create Screening Center Error', error);
                }))
    }

    getAllScreeningCenter() {
        this.loader.listing = true;
        this.staffConfigService.getAllScreeningCenters()
            .subscribe((response) => {
                    this.loader.listing = false;
                    console.log('Screening Center', response);
                    this.centerResponse = response['data'];
                    if(this.centerResponse.length > 0) {
                        this.dtTrigger.next();
                    }
                },
                (error) => {
                    this.loader.listing = false;
                    this.notification.error('Could not load Screening Center', error)
                })

    }

    updateScreeningCenter() {
        this.loader.updating = true;
        this.staffConfigService.updateScreeningCentre(this.updateCenter.value, this.selectedScreeningCenter.id)
            .subscribe((response) => {
                    this.loader.updating = false;
                    console.log('Update Screening Center', response);
                    this.triggerModalOrOverlay('close', 'updateScreeningCenter');
                    this.notification.success('Success!');
                    this.rerender();
                },
                (error => {
                    this.loader.updating = false;
                    this.notification.error('Could not update', error);
                    console.log('Update Screening Center Error', error);
                }))
    }

    editModal(ind) {
        this.selectedScreeningCenter = this.centerResponse[ind];
        this.triggerModalOrOverlay('open', 'updateScreeningCenter');
        this.updateCenter.patchValue(this.selectedScreeningCenter);
    }

    rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            // this.dtTrigger.next();
            this.getAllScreeningCenter();
        });
    }

    public iAmPermitted(routeName): boolean {
        return this.authService.iAmPermitted(routeName);
    }

    triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    }

}
