import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {Cache} from '../../../../../../utils/cache';
import {JSONPARSE} from '../../../../../../utils/json-parse';
import {AuthenticationService} from "../../../../../../services/authentication.service";

declare const $: any;

@Component({
    selector: 'app-active-hour',
    templateUrl: './active-hour.component.html',
    styleUrls: ['./active-hour.component.css']
})
export class ActiveHourComponent implements OnInit {

    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject();
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    public createForm: FormGroup;
    private id: number;
    private editedIndex: number;
    public feedBack = {
        allResponse: [],
        viewDetails: [],
        programmeType: [],
        years: [],
        allCurrentSession: [],
        moduleName: 'Active Hour',
        formType: 'Create',
        currentYear: 2016,
        startLoader: false,
        loader: false,
        submitStatus: false,
        showUpdateButton: false
    };
    static formData = function () {
        return {
            name: ['', Validators.compose([Validators.required])],
            begin_time: ['', Validators.compose([Validators.required])],
            end_time: ['', Validators.compose([Validators.required])],
            enabled: ['', Validators.compose([Validators.required])],
        };
    };


    ngOnInit() {
        this.loadFunction();
    }

    constructor(private staffConfigService: StaffConfigService,
                private notification: NotificationService,
                private authService: AuthenticationService,
                private fb: FormBuilder,
                private el: ElementRef) {
        this.createForm = this.fb.group(ActiveHourComponent.formData());
        this.dtOptions = {
            pagingType: 'full_numbers',
        };
    }

    loadFunction() {
        this.getAllActiveHour();
    }

    /**
     * creating
     */
    public createFormModal() {
        this.feedBack.submitStatus = true;
        this.staffConfigService.createActiveHour(this.createForm.value)
            .subscribe((response) => {
                    (this.feedBack.allResponse.length === 0) ? this.feedBack.allResponse.push(response) : this.feedBack.allResponse.unshift(response);
                    this.notification.success(this.feedBack.moduleName + ' has gone for authorization');
                    this.callBackFunction();
                    this.rerender();
                },
                error => {
                    this.feedBack.submitStatus = false;
                    // const error_message = JSONPARSE(error._body, 'date_start') || 'Unable to Create ' + this.feedBack.moduleName + ', please retry';
                    this.notification.error('Unable to Create ' + this.feedBack.moduleName + ', please retry', error);
                });
    }

    private callBackFunction() {
        this.feedBack.submitStatus = false;
        this.createForm.reset();
        this.closeModal();
    }

    /**
     * update
     */
    public updateFormModal() {
        this.feedBack.submitStatus = true;
        this.staffConfigService.updateActiveHour(this.id, this.createForm.value)
            .subscribe((response) => {
                    // console.log('response update ::', response);
                    this.feedBack.allResponse[this.editedIndex] = response;
                    this.notification.success(this.feedBack.moduleName + ' has been sent for authorization. Please check back later');
                    this.rerender();
                    this.callBackFunction();

                },
                error => {
                    this.feedBack.submitStatus = false;
                    this.notification.error('Unable to Update ' + this.feedBack.moduleName + ', please retry', error);
                    //   console.log('error ::', error);
                });
    }

    /**
     * listing all current session
     */
    public getAllActiveHour() {
        this.feedBack.loader = true;
        this.staffConfigService.getAllActiveHour()
            .subscribe((response) => {
                    console.log('response current session ::', response);
                    Cache.set('current_Session', response);
                    this.feedBack.allCurrentSession = response;
                    if (this.feedBack.allCurrentSession['data'].length > 0) {
                        this.dtTrigger.next();
                    }
                    this.feedBack.loader = false;
                    console.log(this.feedBack.moduleName, ':: ', response);
                },
                error => {
                    this.notification.error(`Unable to load ${this.feedBack.moduleName}, please retry`);
                    this.feedBack.loader = false;
                    console.log('error', error);
                });
    }


    /**
     * editting data
     */
    onEdit(data, i?) {
        this.editedIndex = i;
        this.id = data.id;
        this.openModal();
        this.feedBack.formType = 'Update';
        this.createForm = this.fb.group(data);
        this.feedBack.showUpdateButton = true;
    }

    /**
     * FOR Viewing details
     */
    onView(data) {
        this.feedBack.viewDetails = data;
        $('#viewModal').modal('show');
    }


    /**
     * open modal
     */
    openModal() {
        $('#openModal').modal('show');
    }

    /**
     * close modal
     */
    closeModal() {
        $('#openModal').modal('hide');
        this.feedBack.formType = 'Create';
        this.feedBack.showUpdateButton = false;
    }

    public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
        // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
        // });

    }

    private rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            // Call the dtTrigger to rerender again
            // this.dtTrigger.next();
            this.getAllActiveHour();
        });
    }

    public iAmPermitted(routeName): boolean {
        return this.authService.iAmPermitted(routeName);
    }

}
