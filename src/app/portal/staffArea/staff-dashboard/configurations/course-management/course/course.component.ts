import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CopingsService} from '../../../../../../../services/copings.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {CourseManagementServiceService} from '../../../../../../../services/api-service/course-management-service.service';
import {Cache} from '../../../../../../../utils/cache';
import {AuthenticationService} from '../../../../../../../services/authentication.service';
import {Paginator} from "../../../../../../../utils/paginator";

declare const $;

@Component({
  selector: 'app-course',
  templateUrl: 'course.component.html',
  styles: []
})
export class CourseComponent implements OnInit, AfterViewInit {

  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);
  public permissions = [
    'school.course-management.course.view',
    'school.course-management.course.update'
  ];
  public search_model: Subject<string> = new Subject<string>();
  public paginator = {
    per_page: 10,
    page_number: null,
    search: '',
    pages: [1, 2, 3, 4, 5]
  };
  public createForm: FormGroup;
  public courseSubstitutionForm: FormGroup;
  public courseSubstitutions: any = {};
  public courseSubstitution: any = {};
  private id: number;
  public courses: any;
  public courseObject: any;
  public loader = {
    loading_sub: false,
    submitting_sub: false,
    updating_sub: false,
    sub_title: 'Create'
  };
  public feedBack = {
    allResponse: [],
    viewDetails: {},
    programmeType: [],
    levelList: [],
    programmeList: [],
    facultyList: [],
    sessionList: [],
    processData: [],
    moduleName: 'Course',
    checkEmptyLevel: false,
    course_index: 0,
    departmentList: [],
    loadDepartment: false,
    checkEmptyDepartment: false,
    loadLevel: false,
    formType: 'Create',
    loader: false,
    submitStatus: false,
    showUpdateButton: false
  };
  static formData = function () {
    return {
      title: ['', Validators.compose([Validators.required])],
      code: ['', Validators.compose([Validators.required])],
      unit: ['', Validators.compose([Validators.required])],
      department_id: ['', Validators.compose([Validators.required])],
      faculty_id: ['', Validators.compose([Validators.required])],
      level_id: ['', Validators.compose([Validators.required])],
      programme_id: ['', Validators.compose([Validators.required])],
      semester: ['', Validators.compose([Validators.required])],
    };
  };
  static courseSubstitution = function () {
    return {
      session_id: ['', Validators.compose([Validators.required])],
      course_code: ['', Validators.compose([Validators.required])],
      substitute_code: ['', Validators.compose([Validators.required])]
    };
  };


  constructor(private courseManagementService: CourseManagementServiceService,
              private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private authService: AuthenticationService,
              private tablePager: Paginator,
              private copings: CopingsService,
              private fb: FormBuilder) {
    this.createForm = this.fb.group(CourseComponent.formData());
    this.courseSubstitutionForm = this.fb.group(CourseComponent.courseSubstitution());
    this.search_model.debounceTime(500)
      .distinctUntilChanged()
      .subscribe(search => {
        this.paginator.search = search;
        this.allCourse();
        this.paginator.page_number = null;
      });
  }


  ngOnInit() {
    this.loadFunction();
  }

  loadFunction() {
    this.allFaculty();
    this.allCourse();
    this.allProgramme();
    this.getSessions();
    this.getCourseSubstitutions();
  }

  public changePerPage(num) {
    this.tablePager.changePerPage(num);
    this.allCourse();
  }

  public navigate(page) {
    this.tablePager.navigate(page);
    this.allCourse();
  }

  private setPagination() {
    this.tablePager.laravelPaginationObject = this.courses;
    this.tablePager.setPagination();
    this.tablePager.paginator = this.paginator;
  }

  public createFormModal() {
    this.feedBack.submitStatus = true;
    this.courseManagementService.createCourse(this.createForm.value)
      .subscribe((response) => {
          (this.courses.data.length === 0) ? this.courses.data.push(response) : this.courses.data.unshift(response);
          this.notification.success(this.feedBack.moduleName + ' was created successfully');
          this.callBackFunction();
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to Create ' + this.feedBack.moduleName + ', please retry', error);
        });
  }

  public createCourseSubstitution() {
    this.loader.submitting_sub = true;
    this.courseManagementService.createCourseSubstitution(this.courseSubstitutionForm.value)
      .subscribe((response) => {
          this.loader.submitting_sub = false;
          this.substitutionModal('hide');
          this.courseSubstitutions['data'].push(response);
          // (this.courses.data.length === 0) ? this.courses.data.push(response) : this.courses.data.unshift(response);
          this.notification.success('Success!');
        },
        error => {
          this.loader.submitting_sub = false;
          this.notification.error('An error occurred', error);
        });
  }

  public updateCourseSubstitution() {
    this.courseManagementService.updateCourseSubstitution(this.courseSubstitutionForm.value, this.courseSubstitution.id)
      .subscribe((response) => {
          this.loader.submitting_sub = false;
          this.substitutionModal('hide');
          this.courseSubstitutions['data'].splice(this.courseSubstitutions['data'].indexOf(this.courseSubstitution), 1, response);
          this.notification.success('Success!');
        },
        error => {
          this.loader.submitting_sub = false;
          this.notification.error('An error occurred', error);
        });
    this.loader.submitting_sub = true;
  }

  private getCourseSubstitutions() {
    this.loader.loading_sub = true;
    this.courseManagementService.getCourseSubstitutions().subscribe((res) => {
      this.loader.loading_sub = false;
      this.courseSubstitutions = res;
    }, (err) => {
      this.loader.loading_sub = false;
      this.notification.error('Error occurred', err);
    });
  }

  public deleteCourseSubstitution(sub, index) {
    sub['deleting'] = true;
    this.courseManagementService.deleteCourseSubstitution(sub.id).subscribe((res) => {
      sub['deleting'] = false;
      this.courseSubstitutions['data'].splice(index, 1);
      this.notification.success('Deleted successfully');
    }, (err) => {
      sub['deleting'] = false;
      this.notification.error('Could not delete', err);
    });
  }

  private callBackFunction() {
    Cache.set('all_Course', this.courses.data);
    this.feedBack.submitStatus = false;
    this.createForm.reset();
    this.closeModal();
  }

  public updateFormModal() {
    this.feedBack.submitStatus = true;
    this.courseManagementService.updateCourse(this.id, this.createForm.value)
      .subscribe((response) => {
          // this.allCompanies.splice(this.editedIndex, 1, result.body)
          this.courses.data.splice(this.feedBack.course_index, 1, response);
          this.notification.success(this.feedBack.moduleName + ' was updated successfully');
          //   this.courses.data.forEach((val, i) => {
          //     if (val.id === this.id) {
          //       this.courses.data[i] = response;
          //       this.notification.success(this.feedBack.moduleName + ' was updated successfully');
          //     }
          //   });
          this.callBackFunction();
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to Update ' + this.feedBack.moduleName + ', please retry', error);
        });
  }

  public allCourse() {
    this.feedBack.loader = true;
    this.courseManagementService.getAllCourse(this.paginator).subscribe((response) => {
        // Cache.set('all_Course', response.data);
        this.courses = response;
        this.setPagination();
        this.feedBack.loader = false;
      },
      error => {
        this.notification.error('Unable to load ' + this.feedBack.moduleName + ', please retry');
        this.feedBack.loader = false;
      });
  }

  public allFaculty() {
    this.staffConfigService.getFacultyList()
      .subscribe((response) => {
          this.feedBack.facultyList = response.data;
        },
        error => {
          this.notification.error('Unable to load Faculties', error);
        });
  }

  public getSessions() {
    this.staffConfigService.getSessionList()
      .subscribe((response) => {
          this.feedBack.sessionList = response || response.data;
        },
        error => {
          this.notification.error('Unable to load faculties, please retry', error);
        });
  }

  public getFaculty(id) {
    this.feedBack.loadDepartment = true;
    this.staffConfigService.getFaculty(id)
      .subscribe((response) => {
          this.feedBack.checkEmptyDepartment = true;
          this.feedBack.departmentList = response.departments;
          this.feedBack.loadDepartment = false;
        },
        error => {
          this.feedBack.checkEmptyDepartment = true;
          this.feedBack.loadDepartment = false;
          this.notification.error('Unable to load deparmtent, please retry');
        });
  }

  public allProgramme() {
    this.staffConfigService.getAllProgramme()
      .subscribe((response) => {
          this.feedBack.programmeList = response.data;
        },
        error => {
          this.notification.error('Unable to load programme, please retry');
        });
  }

  public getProgramme(id) {
    this.feedBack.loadLevel = true;
    for (let i = 0; i < this.feedBack.programmeList.length; i++) {
      if (this.feedBack.programmeList[i].id === parseInt(id)) {
        this.feedBack.loadLevel = false;
        this.feedBack.levelList = this.feedBack.programmeList[i]['levels']
      }
      this.feedBack.loadLevel = false;
    }
    // this.staffConfigService.getProgramme(id)
    //     .subscribe((response) => {
    //             this.feedBack.checkEmptyLevel = true;
    //             this.feedBack.levelList = response.levels;
    //             this.feedBack.loadLevel = false;
    //         },
    //         error => {
    //             this.feedBack.checkEmptyLevel = true;
    //             this.feedBack.loadLevel = false;
    //             this.notification.error('Unable to load level, please retry');
    //         });
  }

  onEdit(data, permission_id?) {
    if (!this.iAmPermitted(permission_id)) {
      return this.notification.warning('You do not have the permission to Edit! Contact the System Admin');
    }
    this.feedBack.course_index = this.courses.data.indexOf(data);
    this.id = data.id;
    this.feedBack.processData = this.copings.deepCopy(data);
    this.getProgramme(data.level.programme_id);
    this.getFaculty(data.department.faculty_id);
    data['department_id'] = this.feedBack.processData['department']['id'];
    data['faculty_id'] = this.feedBack.processData['department']['faculty_id'];
    data['programme_id'] = this.feedBack.processData['level']['programme_id'];
    data['level_id'] = this.feedBack.processData['level']['id'];
    this.createForm.patchValue(data);
    this.feedBack.formType = 'Update';
    this.feedBack.showUpdateButton = true;
    this.openModal();
    // this.createForm = this.fb.group(this.feedBack.processData);
  }

  /**
   * FOR Viewing details
   */
  onView(data) {
    this.courseObject = data;
    $('#viewModal').modal('show');
  }


  /**
   * open modal
   */
  openModal(form_type?: string) {
    // if (this.feedBack.showUpdateButton === false) {
    //   this.createForm.reset();
    //   this.feedBack.levelList = [];
    //   this.feedBack.departmentList = [];
    // }
    if (form_type === 'create') {
      this.createForm.reset();
      this.feedBack.showUpdateButton = false;
      this.feedBack.formType = 'Create';
    }
    $('#openModal').modal('show');
  }

  public substitutionModal(action: string, data?) {
    if (data) {
      this.courseSubstitution = data;
      this.courseSubstitutionForm.patchValue(data);
      this.loader.sub_title = 'Update';
    } else {
      this.courseSubstitutionForm.reset();
      this.loader.sub_title = 'Create';
    }
    $('#course_substitution_modal').modal(action);
  }

  /**
   * close modal
   */
  closeModal() {
    $('#openModal').modal('hide');
    this.feedBack.formType = 'Create';
    this.feedBack.showUpdateButton = false;
  }


  /**
   * get the department id
   * @param {Event} $event
   */
  extractId(event: any) {
    this.feedBack.checkEmptyDepartment = false;
    this.feedBack.departmentList = [];
    const id = event.target.value;
    this.getFaculty(id);
  }

  extractProgrammeId(event) {
    this.feedBack.loadLevel = true;
    this.feedBack.levelList = [];
    const id = event.target.value;
    this.getProgramme(id);
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  public toRemoveByPermission() {
    this.permissions.forEach(permission => {
      if (permission && !this.iAmPermitted(permission)) {
        document.getElementById(permission).style.display = 'none';
      }
    });
  }

  ngAfterViewInit(): void {
    this.toRemoveByPermission();
  }
}
