import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {StaffConfigService} from "../../../../../../../../services/api-service/staff-config.service";
import {ActivatedRoute} from "@angular/router";
import {NotificationService} from "../../../../../../../../services/notification.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SchoolService} from "../../../../../../../../services/school.service";
import {Subject} from "rxjs/Subject";
import {DataTableDirective} from "angular-datatables";
import {CourseManagementServiceService} from "../../../../../../../../services/api-service/course-management-service.service";
import {AdministationServiceService} from "../../../../../../../../services/api-service/administation-service.service";
import {AuthenticationService} from "../../../../../../../../services/authentication.service";

declare const $: any;

@Component({
  selector: 'app-curriculum-view',
  templateUrl: './course-of-study-view.component.html',
  styleUrls: ['./course-of-study-view.component.css']
})
export class CourseOfStudyViewComponent implements OnInit, AfterViewInit {

  private course_of_study_id: number;
  public dtTrigger: Subject<any> = new Subject;
  public dtTrigger2: Subject<any> = new Subject;
  public courseOfStudyObject;
  public allCurriculum: any[] = [];
  public departmentStaff: any[] = [];
  public courseAdvisers: any[] = [];
  public sessionSemesters: any[] = [];
  @ViewChild(DataTableDirective)
  private dtElement: DataTableDirective;
  public curriculumFilterForm: FormGroup;
  public createCurriculum: FormGroup;
  public createCourseAdviser: FormGroup;
  public permissions = {
    'school.profile.course.adviser.add': false,
    'school.profile.course.adviser.list': true,
    'school.course-management.curriculum.create': false,
    'school.course-management.curriculum.list': false,
    'school.course-management.curriculum.update': false,
    'school.course-management.curriculum.courses': false,
  };
  public load = {
    course_of_study_get: false,
    filtering: false,
    show: false,
    semester: false,
    getStaff: false,
    get_courseAdvisers: false,
    setAdviser: false,
    create: false,
    update: false,
    once: false
  };
  public curriculum: any;

  static curriculumFilter = function () {
    return {
      level_id: [0, Validators.required],
      session_id: [0, Validators.required],
      semester: [0, Validators.required]
    }
  };

  static courseAdviser = function () {
    return {
      level_id: ['', Validators.required],
      session_id: ['', Validators.required],
      staff_id: ['', Validators.required]
    }
  };

  static createCurriculum = function () {
    return {
      level_id: ['', Validators.required],
      max_unit: ['', Validators.required],
      min_unit: ['', Validators.required],
      session_id: ['', Validators.required],
      semester: ['', Validators.required],
    }
  };

  constructor(private staffConfigService: StaffConfigService,
              private router: ActivatedRoute,
              private fb: FormBuilder,
              private authService: AuthenticationService,
              private schoolService: SchoolService,
              private administration: AdministationServiceService,
              private courseManagementService: CourseManagementServiceService,
              private notification: NotificationService) {
    this.curriculumFilterForm = this.fb.group(CourseOfStudyViewComponent.curriculumFilter());
    this.createCurriculum = this.fb.group(CourseOfStudyViewComponent.createCurriculum());
    this.createCourseAdviser = this.fb.group(CourseOfStudyViewComponent.courseAdviser())
  }

  ngOnInit() {
    this.course_of_study_id = this.router.snapshot.params['id'];
    this.getSingleCourseOfStudy();
    this.getCourseAdvisers();
  }

  private getSingleCourseOfStudy() {
    this.load.course_of_study_get = true;
    this.staffConfigService.getSingleCourseOfStudy(this.course_of_study_id)
      .subscribe((response) => {
        this.load.course_of_study_get = false;
        this.toRemoveByPermission();
        this.courseOfStudyObject = response;
        console.log(response);
        this.getStaffByDepartment();
      }, (error) => {
        this.load.course_of_study_get = false;
        this.notification.error('Could not connect. Please retry', error);
      })
  }

  private getStaffByDepartment() {
    this.load.getStaff = true;
    this.load.show = true;
    this.staffConfigService.getStaffByDepartment(this.courseOfStudyObject.department.id)
      .subscribe((response) => {
          this.load.getStaff = false;
          this.departmentStaff = response;
          console.log(response);
        },
        error => {
          this.load.getStaff = false;
          this.notification.error('Unable to load Department Staff, please retry', error);
        });
  }

  getCourseAdvisers() {
    this.load.get_courseAdvisers = true;
    this.staffConfigService.getCourseAdvisers(this.course_of_study_id)
      .subscribe((response) => {
        this.load.get_courseAdvisers = false;
        this.toRemoveByPermission();
        this.courseAdvisers = response;
        if (this.courseAdvisers.length > 0) {
          this.dtTrigger2.next();
        }
      }, (error) => {
        this.load.get_courseAdvisers = false;
        this.notification.error('Could not load Course Advisers. Please retry', error)
      });
  }

  public getSessionSemesters(session_id) {
    this.load.semester = true;
    this.staffConfigService.getSingleSession(session_id)
      .subscribe((response) => {
        this.load.semester = false;
        this.sessionSemesters = response['session_semesters'];
        console.log(this.sessionSemesters);
      }, (error) => {
        this.load.semester = false;
        this.notification.error('Could not load Semesters. Please retry', error)
      });
  }

  public openCurriculumModal (curriculum?, index?) {
    this.createCurriculum.reset();
    this.curriculum = null;
    if (curriculum) {
      this.curriculum = curriculum;
      this.curriculum['index'] = index;
      this.createCurriculum.patchValue(curriculum);
    }
    this.triggerModalOrOverlay('open', 'createCurriculumModal');
  }

  public setCourseAdviser() {
    this.load.setAdviser = true;
    this.createCourseAdviser.value['course_of_study_id'] = this.course_of_study_id;
    this.administration.setCourseAdviser(this.createCourseAdviser.value)
      .subscribe((response) => {
        this.load.setAdviser = false;
        this.notification.success('Successful!');
        this.triggerModalOrOverlay('close', 'createCurriculumModal');
        console.log(response);
      }, (error) => {
        this.load.setAdviser = false;
        this.notification.error('Could not create. Please retry', error);
        this.triggerModalOrOverlay('close', 'createCurriculumModal');
      });
  }

  public curriculumCreate() {
    this.load.create = true;
    this.createCurriculum.value['course_of_study_id'] = this.course_of_study_id;
    this.courseManagementService.createCurriculum(this.createCurriculum.value)
      .subscribe((response) => {
        this.load.create = false;
        this.allCurriculum.unshift(this.getLevelName([response]));
        this.notification.success('Successful!');
        this.triggerModalOrOverlay('close', 'createCurriculumModal');
      }, (error) => {
        this.load.create = false;
        this.notification.error('Could not create. Please retry', error);
        this.triggerModalOrOverlay('close', 'createCurriculumModal');
      });
  }

  public curriculumUpdate () {
    this.load.update = true;
    this.createCurriculum.value['course_of_study_id'] = this.course_of_study_id;
    this.courseManagementService.updateCurriculum(this.curriculum.id, this.createCurriculum.value)
      .subscribe((response) => {
        this.load.update = false;
        this.allCurriculum.splice(this.curriculum.index, 1, this.getLevelName([response]));
        this.addSemesterName();
        this.notification.success('Successful!');
        this.triggerModalOrOverlay('close', 'createCurriculumModal');
      }, (error) => {
        this.load.update = false;
        this.notification.error('Could not create. Please retry', error);
        this.triggerModalOrOverlay('close', 'createCurriculumModal');
      });
  }

  private getLevelName(data) {
    // if () {
      this.courseOfStudyObject['degree']['programme']['levels'].forEach(level => {
        data.forEach(resp => {
          if (parseInt(level.id, 10) === parseInt(resp.level_id, 10)) {
            resp['level'] = level.name;
          }
        });
      });
      if (data.length === 1) {
        return data[0];
      }
    // }
  }

  public getCurriculumByCourseOfStudy() {
    this.load.filtering = true;
    this.schoolService.getFilteredCourseOfStudy(this.course_of_study_id, this.curriculumFilterForm.get('level_id').value, this.curriculumFilterForm.get('session_id').value, this.curriculumFilterForm.get('semester').value)
      .subscribe((response) => {
        this.load.filtering = false;
        // this.courseOfStudyObject['degree']['programme']['levels'].forEach(level => {
        //   response.forEach(resp => {
        //     if (parseInt(level.id, 10) === parseInt(resp.level_id, 10)) {
        //       resp['level'] = level.name;
        //     }
        //   });
        // });
        this.getLevelName(response);
        this.allCurriculum = response;
        this.addSemesterName();
        this.customRerender();
        this.load.once = true;
        console.log(response);
      }, (error) => {
        this.notification.error('Could not load Curriculum. Please retry', error);
        this.load.filtering = false;
        console.log(error);
      });
  }

  private customRerender() {
    if (this.load.once) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
        this.dtTrigger.next();
      });
    } else if (this.allCurriculum.length >= 1) {
      this.dtTrigger.next();
    }
  }

  private addSemesterName() {
    this.allCurriculum.forEach(curriculum => {
      this.sessionSemesters.forEach(sessionSemester => {
        if (parseInt(curriculum.semester, 10) === parseInt(sessionSemester.semester, 10)) {
          curriculum['semester_name'] = sessionSemester.name;
        }
      });
    });
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }

  private toRemoveByPermission () {
    for (const permission in this.permissions) {
      this.permissions[permission] = this.iAmPermitted(permission);
    }
  }

  ngAfterViewInit(): void {
    this.toRemoveByPermission();
  }

}
