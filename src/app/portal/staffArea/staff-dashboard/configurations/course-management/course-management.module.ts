import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CourseComponent} from './course/course.component';
import {CurriculumComponent} from './curriculum/curriculum.component';
import {CurriculumCourseComponent} from './curriculum-course/curriculum-course.component';
import {RouterModule, Routes} from '@angular/router';
import {StaffDashboardComponent} from '../../dashboard-component.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CourseManagementServiceService} from '../../../../../../services/api-service/course-management-service.service';
import {DataTablesModule} from "angular-datatables";
import { CourseOfStudyViewComponent } from './curriculum/course-of-study-view/course-of-study-view.component';
import { CourseViewComponent } from './course-view/course-view.component';
import {MultiselectDropdownModule} from "angular-2-dropdown-multiselect";

const ROUTINGS: Routes = [
    {
        path: 'config',
        component: StaffDashboardComponent,
        children: [
            {path: 'course-mgt', component: CourseComponent},
            {path: 'curriculum', component: CurriculumComponent},
            {path: 'course-of-study/:id', component: CourseOfStudyViewComponent},
            {path: 'course-curriculum/:id', component: CurriculumCourseComponent},
            {path: 'course/:id', component: CourseViewComponent}
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MultiselectDropdownModule,
        DataTablesModule,
        RouterModule.forChild(ROUTINGS)
    ],
    exports: [RouterModule],
    declarations: [
        CourseComponent,
        CurriculumComponent,
        CurriculumCourseComponent,
        CourseOfStudyViewComponent,
        CourseViewComponent
    ],
    providers: [
        CourseManagementServiceService
    ]
})
export class CourseManagementModule {
}
