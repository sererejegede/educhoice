import { Component, OnInit } from '@angular/core';
import {CourseManagementServiceService} from '../../../../../../../services/api-service/course-management-service.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {ActivatedRoute} from '@angular/router';
import {IMultiSelectSettings, IMultiSelectTexts} from 'angular-2-dropdown-multiselect';
import {AuthenticationService} from '../../../../../../../services/authentication.service';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-course-view',
  templateUrl: './course-view.component.html',
  styleUrls: ['./course-view.component.css']
})
export class CourseViewComponent implements OnInit {

  public courseOfStudyObject;
  public curriculumCourse;
  public allDepartmentStaff: any[] = [];
  public courseLecturers: any[] = [];
  private course_id: number;
  public createCourseLecturerForm: FormGroup;
  public loading = {
    pageIsLoaded: false,
    curriculum_course: false,
    course_of_study: false,
    staff: false,
    lecturer: false,
    addLecturer: false
  };

  // Settings configuration
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true
  };

// Text configuration
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    searchEmptyResult: 'Nothing found...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Select Lecturer',
    allSelected: 'All selected',
  };

  constructor(private courseManagementService: CourseManagementServiceService,
              private router: ActivatedRoute,
              private authService: AuthenticationService,
              private notification: NotificationService,
              private fb: FormBuilder,
              private staffConfigService: StaffConfigService) {
    this.createCourseLecturerForm = this.fb.group({
      lecturer_ids: ['']
    });
  }

  ngOnInit() {
    this.course_id = this.router.snapshot.params['id'];
    this.getSingleCourse();
    this.getLecturersByCourse();
  }

  private getSingleCourse() {
    this.loading.curriculum_course = true;
    this.courseManagementService.getSingleCourse(this.course_id)
        .subscribe((response) => {
          this.loading.curriculum_course = false;
          this.curriculumCourse = response;
          if (response.curriculum.course_of_study_id) {
            this.getSingleCourseOfStudy(response.curriculum.course_of_study_id);
          }
        }, (error) => {
          this.loading.curriculum_course = false;
          this.notification.error('Something went wrong. Please retry', error);
        });
  }

  // public allCurriculumCourseById() {
  //   this.loading.curriculum_course = true;
  //   this.courseManagementService.getAllCurriculumcoursebyId(this.curriculum_id)
  //       .subscribe((response) => {
  //             this.details.curriculum_course = response;
  //             this.details.course_of_study_id = response['course_of_study_id'];
  //             this.loading.curriculum_course = false;
  //             if(!this.loading.pageIsLoaded){
  //               this.getSingleCourseOfStudy();
  //             }
  //           },
  //           error => {
  //             this.notification.error('Something went wrong. Please retry');
  //             this.loading.curriculum_course = false;
  //           });
  // }

  private getSingleCourseOfStudy(course_of_study_id) {
    this.loading.course_of_study = true;
    this.staffConfigService.getSingleCourseOfStudy(course_of_study_id)
        .subscribe((response) => {
          this.loading.course_of_study = false;
          this.courseOfStudyObject = response;
          this.getStaffByDepartment();
        }, (error) => {
          this.loading.course_of_study = false;
          this.notification.error('Could not connect. Please retry', error);
        });
  }

  private getStaffByDepartment() {
    this.loading.staff = true;
    this.staffConfigService.getStaffByDepartment(this.courseOfStudyObject['department']['id'])
        .subscribe((response) => {
      const testArray: any[] = [];
              response.forEach((staff, i) => {
                staff['name'] = staff.fullname;
                if (parseInt(staff.type, 10) === 1) {
                  testArray.push(staff);
                }
              });
              this.allDepartmentStaff = testArray;
              this.loading.staff = false;
            },
            error => {
              this.loading.staff = false;
              this.notification.error('Staff list could not be loaded. Please retry', error);
            });
  }

  public assignLecturerToCourse(staff_ids) {
    this.loading.addLecturer = true;
    const forSending = {
      lecturer_ids: staff_ids['model']
    };
    this.staffConfigService.assignLecturerToCourse(this.course_id, forSending)
        .subscribe(() => {
              this.loading.addLecturer = false;
              this.getLecturersByCourse();
              // this.rerender('staff');
              this.notification.success('Success!');
            },
            error => {
              this.loading.addLecturer = false;
              this.notification.error('Something happened', error);
            });
  }

  public deleteLecturerCourse(lecturer_course, index) {
    this.courseLecturers[index]['deleting'] = true;
    // console.log(lecturer_course);
    this.staffConfigService.deleteLecturerCourse(lecturer_course.id).subscribe(() => {
      this.courseLecturers[index]['deleting'] = false;
      this.courseLecturers.splice(index, 1);
    }, (err) => {
      this.courseLecturers[index]['deleting'] = false;
      this.notification.error('Could not complete action', err);
    });
  }

  private getLecturersByCourse() {
    this.loading.lecturer = true;
    this.staffConfigService.getLecturersByCurriculumCourse(this.course_id)
        .subscribe((response) => {
              this.courseLecturers = response;
              const emptyArray: any[] = [];
              this.courseLecturers.forEach((staff) => {
                emptyArray.push(staff.id);
              });
              this.createCourseLecturerForm.controls['lecturer_ids'].setValue(emptyArray);
              this.loading.lecturer = false;
            },
            error => {
              this.loading.lecturer = false;
              this.notification.error('Lecturer list could not be loaded. Please retry', error);
            });
  }


  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

}
