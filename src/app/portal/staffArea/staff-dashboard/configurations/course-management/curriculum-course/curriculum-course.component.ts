import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {NotificationService} from '../../../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {CourseManagementServiceService} from '../../../../../../../services/api-service/course-management-service.service';
import {AuthenticationService} from '../../../../../../../services/authentication.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';

declare const $;

@Component({
  selector: 'app-curriculum-course',
  templateUrl: 'curriculum_course.component.html',
  styles: []
})
export class CurriculumCourseComponent implements OnInit, AfterViewInit {

  public dtTrigger: Subject<any> = new Subject;
  @ViewChild(DataTableDirective)
  private dtElement: DataTableDirective;
  public createCourseForm: FormGroup;
  private course_of_study_id: number;
  public allProgrammes: any[] = [];
  public programmeLevels: any[] = [];
  public permissions = {
    'school.course-management.curriculum-course.create': false,
    'school.course-management.curriculum-course.update': true,
    'school.course-management.curriculum-course.delete': true
  };
  private id: number;
  private urlId: number;
  public selectedCourse = {
    title: '',
    code: '',
    unit: '',
    reg_limit: null
  };
  public templateUrl = '';
  public courseOfStudyObject;
  public load = {
    course_of_study_get: false
  };
  public loading = {
    title: 'Create',
    course: false,
    pageIsLoaded: false,
    update: false,
    create: false,
    updateButton: false
  };
  public drillingParams = {
    faculties: {
      loading: false,
      values: []
    },
    departments: {
      loading: false,
      values: []
    },
    departmentalCourses: {
      loading: false,
      values: []
    }
  };

  public feedBack = {
    curriculum_course: {
      index: -1,
      id: 0
    },
    allResponse: null,
    moduleName: 'Curriculum Course',
    allCurriculum: null,
    loadCourseById: false,
    loader: false,
  };

  static courseFormData = function () {
    return {
      title: ['', Validators.compose([Validators.required])],
      code: ['', Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])],
      unit: ['', Validators.compose([Validators.required])],
      reg_limit: [''],
      faculty: ['', Validators.compose([Validators.required])],
      department: ['', Validators.compose([Validators.required])],
      course_id: ['', Validators.compose([Validators.required])]
    };
  };


  constructor(private courseManagementService: CourseManagementServiceService,
              private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private authService: AuthenticationService,
              private route: ActivatedRoute,
              private fb: FormBuilder) {
    this.createCourseForm = this.fb.group(CurriculumCourseComponent.courseFormData());
  }

  ngOnInit() {
    this.urlId = +this.route.snapshot.paramMap.get('id');
    this.load.course_of_study_get = true;
    this.getAllFaculties();
    this.allCurriculumcoursebyId();
  }

  private getSingleCourseOfStudy() {
    this.loading.pageIsLoaded = true;
    console.log(this.course_of_study_id);
    this.staffConfigService.getSingleCourseOfStudy(this.course_of_study_id)
        .subscribe((response) => {
          this.load.course_of_study_get = false;
          this.courseOfStudyObject = response;
          console.log(response);
        }, (error) => {
          this.load.course_of_study_get = false;
          this.notification.error('Could not connect. Please retry', error);
        });
  }

  public getDepartmentsByFacultyId($event) {
    this.drillingParams.departments.loading = true;
    this.staffConfigService.getFaculty($event)
        .subscribe(response => {
              this.drillingParams.departments.loading = false;
              this.drillingParams.departments.values = response['departments'];
              console.log('Departments', response);
            },
            error => {
              this.drillingParams.departments.loading = false;
              console.log('Something went wrong', error);
              this.notification.error('Something went wrong');
            }
        );
  }

  public getCourseByDepartment(department_id) {
    this.drillingParams.departmentalCourses.loading = true;
    this.staffConfigService.getCoursesByDepartment(department_id)
        .subscribe((response) => {
              this.drillingParams.departmentalCourses.loading = false;
              console.log(response);
              this.drillingParams.departmentalCourses.values = response;
            // document.getElementById('preloader').classList.add('hide');
            },
            error => {
              this.drillingParams.departmentalCourses.loading = false;
              console.log(error);
              this.notification.error('Unable to load Departmental Courses, please retry');
            }, () => {
            setTimeout(() => $('.selectpicker').selectpicker('refresh'));
          });
  }

  public prefillCourse(id) {
    this.drillingParams.departmentalCourses.values.forEach(course => {
      if (parseInt(course.id, 10) === parseInt(id, 10)) {
        // this.selectedCourse.title = course.title;
        // this.selectedCourse.code = course.code;
        // this.selectedCourse.unit = course.unit;
        this.createCourseForm.controls['title'].setValue(course.title);
        this.createCourseForm.controls['code'].setValue(course.code);
        this.createCourseForm.controls['unit'].setValue(course.unit);
      }
    });
  }
// this.createCourseForm.value.title
  // this.createCourseForm.value.code
  // this.createCourseForm.value.unit
  private getAllFaculties() {
    this.drillingParams.faculties.loading = true;
    this.staffConfigService.getAllFaculty()
        .subscribe((response) => {
              this.drillingParams.faculties.loading = false;
              this.drillingParams.faculties.values = response.data;
            },
            (error) => {
              this.drillingParams.faculties.loading = false;
              this.notification.error('Unable to load Degrees, please retry', error);
            });
  }

  /**
   * creating
   */
  public createCurriculumCourse() {
    this.loading.create = true;
    this.createCourseForm.value['curriculum_id'] = this.urlId;
    this.courseManagementService.createCurriculumcourse(this.createCourseForm.value)
        .subscribe((response) => {
              this.loading.create = false;
              // console.log(response);
              // this.rerender();
            this.feedBack.allResponse['curriculum_courses'].unshift(response);
              this.triggerModalOrOverlay('close', 'course_modal');
              this.notification.success('Created Successfully');
            },
            error => {
              this.loading.create = false;
              this.triggerModalOrOverlay('close', 'course_modal');
              this.notification.error('Something happened', error);
              console.log('error ::', error);
            });
  }

  public updateCurriculumCourse () {
    this.loading.update = true;
    this.createCourseForm.value['curriculum_id'] = this.urlId;
    this.courseManagementService.updateCurriculumcourse(this.feedBack.curriculum_course.id, this.createCourseForm.value).subscribe(res => {
      this.loading.update = false;
      if (this.feedBack.curriculum_course.index !== -1) {
        this.feedBack.allResponse['curriculum_courses'].splice(this.feedBack.curriculum_course.index, 1, res);
      }
      this.triggerModalOrOverlay('close', 'course_modal');
      this.notification.success('Updated successfully!');
    }, err => {
      this.loading.update = false;
      this.triggerModalOrOverlay('close', 'course_modal');
      this.notification.error('Could not update', err);
    });
  }

  public editCourse (course) {
    this.feedBack.curriculum_course.index = this.feedBack.allResponse['curriculum_courses'].indexOf(course);
    this.feedBack.curriculum_course.id = course.id;
    this.createCourseForm.patchValue(course);
    this.loading.title = 'Update';
    this.loading.updateButton = true;
    this.triggerModalOrOverlay('open', 'course_modal');
  }

  public deleteCurriculumCourse (course_id, course_index) {
    this.feedBack.allResponse['curriculum_courses'][course_index]['deleting'] = true;
    this.courseManagementService.deleteCurriculumCourse(course_id).subscribe(res => {
      this.feedBack.allResponse['curriculum_courses'][course_index]['deleting'] = false;
      // console.log(res);
      this.feedBack.allResponse['curriculum_courses'].splice(course_index, 1);
      this.notification.success('Deleted Successfully');
    }, err => {
      this.feedBack.allResponse['curriculum_courses'][course_index]['deleting'] = false;
      console.log(err);
      this.notification.error('Could not delete', err);
    });
  }

  public newCourse () {
    this.createCourseForm.reset();
    this.loading.title = 'Create';
    this.loading.updateButton = false;
    this.triggerModalOrOverlay('open', 'course_modal');
  }

  public allCurriculumcoursebyId() {
    this.feedBack.loader = true;
    this.courseManagementService.getAllCurriculumcoursebyId(this.urlId)
      .subscribe((response) => {
          this.feedBack.loader = false;
          this.toRemoveByPermission();
      // console.log('Curriculum course response:::', response);
          this.feedBack.allResponse = response;
          this.course_of_study_id = response['course_of_study_id'];
          if (this.feedBack.allResponse['curriculum_courses'].length > 0) {
            this.dtTrigger.next();
          }
          if (!this.loading.pageIsLoaded) {
            this.getSingleCourseOfStudy();
          }
        },
        error => {
          this.load.course_of_study_get = false;
          this.notification.error('Unable to load ' + this.feedBack.moduleName + ', please retry');
          this.feedBack.loader = false;
          console.log('error', error);
        });
  }



  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
      (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }


    public iAmPermitted(routeName): boolean {
        return this.authService.iAmPermitted(routeName);
    }

  private toRemoveByPermission () {
    for (const permission in this.permissions) {
      this.permissions[permission] = this.iAmPermitted(permission);
    }
  }

  ngAfterViewInit(): void {
    this.toRemoveByPermission();
  }

  rerender(): void {
    // this.feedBack.allResponse = [];
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      // this.dtTrigger.next();
      this.allCurriculumcoursebyId();
    });
  }
}
