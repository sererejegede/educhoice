import {Component, OnInit, ViewChild} from '@angular/core';
import {NotificationService} from '../../../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {ActivatedRoute} from '@angular/router';
import {AuthenticationService} from '../../../../../../../services/authentication.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs/Subject';
import {CourseManagementServiceService} from '../../../../../../../services/api-service/course-management-service.service';
import {DataTableDirective} from 'angular-datatables';
import {IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts} from 'angular-2-dropdown-multiselect';
import {AdministationServiceService} from '../../../../../../../services/api-service/administation-service.service';

declare const $: any;

@Component({
  selector: 'app-department-view',
  templateUrl: './department-view.component.html',
  styleUrls: ['./department-view.component.css']
})
export class DepartmentViewComponent implements OnInit {
  allCourseOfStudy: any;
  allProgrammes: any[] = [];
  allDegrees: any[] = [];
  allStaff: any[] = [];
  allHODs: any[] = [];
  programmeLevels: any[] = [];
  allDepartmentalCourses: any[] = [];
  allDepartmentStaff: any[] = [];
  modal_title;
  createForm: FormGroup;
  createDepartmentStaffForm: FormGroup;
  createCourseForm: FormGroup;
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  @ViewChild(DataTableDirective)
  dtElement2: DataTableDirective;
  dtTrigger: Subject<any> = new Subject;
  dtTrigger2: Subject<any> = new Subject;
  loading = {
    course: false,
    staff: false,
    addStaff: false,
    courseofstudy: false,
    setHOD: false,
    update: false,
    create: false,
    updateButton: false
  };
  private departmentId;
  private courseOfStudyId;
  private courseId;

  // This is for iAmPermitted
  public permissions = {
      'school.assign_hod': false,
      'school.admission.report.course.list_courses_by_crs': false,
      'school.course-management.course.byDepartment': false,
      'school.staff_by_department': false,
      'school.view_department_hods': false,
      'school.course-of-study.create': false,
      'school.course-management.course.create': false,
      'school.course-of-study.update': true,
      'school.course-management.course.update': false,
      'school.staff_by_department.update': true
  };

  // Settings configuration
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true
  };

// Text configuration
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'staff selected',
    searchPlaceholder: 'Find',
    searchEmptyResult: 'Nothing found...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Select Staff',
    allSelected: 'All selected',
  };

  static formData = function () {
    return {
      name: ['', Validators.compose([Validators.required])],
      code: ['', Validators.compose([Validators.required])],
      duration: ['', Validators.compose([Validators.required])],
      degree_id: ['', Validators.compose([Validators.required])],
      programme_type_id: ['', Validators.compose([Validators.required])],
    };
  };

  static courseFormData = function () {
    return {
      title: ['', Validators.compose([Validators.required])],
      code: ['', Validators.compose([Validators.required])],
      unit: ['', Validators.compose([Validators.required])],
      level_id: ['', Validators.compose([Validators.required])],
      semester: ['', Validators.compose([Validators.required])],
      programme: ''
    };
  };


  constructor(private notification: NotificationService,
              private staffConfigService: StaffConfigService,
              private courseManagementService: CourseManagementServiceService,
              private authService: AuthenticationService,
              private administration: AdministationServiceService,
              private fb: FormBuilder,
              private router: ActivatedRoute) {
    this.createForm = this.fb.group(DepartmentViewComponent.formData());
    this.createCourseForm = this.fb.group(DepartmentViewComponent.courseFormData());
    this.createDepartmentStaffForm = this.fb.group({
      lecturer_ids: ['']
    });
  }

  ngOnInit() {
    this.departmentId = this.router.snapshot.params['id'];
    this.getProgrammes();
    this.getDegrees();
    this.getCourseOfStudy();
    this.getCourseByDepartment();
    this.getAllStaff();
    this.getStaffByDepartment();
    this.getDepartmentHODs();
  }

  // Getting all staff to be able to add staff later
  getAllStaff() {
    this.loading.staff = true;
    this.staffConfigService.getAllStaff()
        .subscribe(response => {
              this.loading.staff = false;
              this.toRemoveByPermission();
              this.allStaff = response;
              this.allStaff.forEach(staff => {
                staff['name'] = staff.fullname;
              });
              console.log('Staff', response);
            },
            error => {
              this.loading.staff = false;
              this.notification.error('Network error. Please retry', error);
            }
        );
  }

  private getDepartmentHODs() {
    this.staffConfigService.getDepartmentHODs(this.departmentId)
        .subscribe((response) => {
          this.toRemoveByPermission();
          this.allHODs = response;
        }, (error) => {
          console.log(error);
          this.notification.error('Could not load HODs. Please retry', error);
        });
  }

  getCourseOfStudy() {
    this.allCourseOfStudy = [];
    this.loading.courseofstudy = true;
    this.staffConfigService.getCourseOfStudyById(this.departmentId)
        .subscribe(response => {
              this.loading.courseofstudy = false;
              this.toRemoveByPermission();
              // console.log('Course of Study', response);
              this.allCourseOfStudy = response;
            },
            error => {
              this.loading.courseofstudy = false;
              this.notification.error('Course of Study could not be loaded', error);
            }
        );
  }

  public createCourseOfStudy() {
    this.loading.create = true;
    const obj = {
      department_id: this.departmentId
    };
    const forSending = Object.assign(this.createForm.value, obj);
    this.staffConfigService.createCourse(forSending)
        .subscribe((response) => {
              this.loading.create = false;
              console.log(response);
              this.getCourseOfStudy();
              this.triggerModalOrOverlay('close', 'course_of_study_modal');
              this.notification.success('Created Successfully');
            },
            error => {
              this.loading.create = false;
              this.triggerModalOrOverlay('close', 'course_of_study_modal');
              this.notification.error('Something happened', error);
              console.log('error ::', error);
            });
  }

  public updateCourseOfStudy() {
    this.loading.update = true;
    // const obj = {
    //   department_id: this.departmentId
    // };
    // const forSending = Object.assign(this.createForm.value, obj);
    this.createForm.value['department_id'] = this.departmentId;
    this.staffConfigService.updateCourse(this.createForm.value, this.courseOfStudyId)
        .subscribe((response) => {
              this.loading.update = false;
              console.log(response);
              this.getCourseOfStudy();
              this.triggerModalOrOverlay('close', 'course_of_study_modal');
              this.notification.success('Updated Successfully');
            },
            error => {
              this.loading.update = false;
              this.triggerModalOrOverlay('close', 'course_of_study_modal');
              this.notification.error('Something happened', error);
              console.log('error ::', error);
            });
  }

  public createCourse() {
    this.loading.create = true;
    this.createCourseForm.value['department_id'] = this.departmentId;
    console.log(this.createCourseForm.value);
    this.courseManagementService.createCourse(this.createCourseForm.value)
        .subscribe((response) => {
              this.loading.create = false;
              this.allDepartmentalCourses.unshift(response);
              this.rerender('course');
              this.triggerModalOrOverlay('close', 'course_modal');
              this.notification.success('Created Successfully');
            },
            error => {
              this.loading.create = false;
              this.triggerModalOrOverlay('close', 'course_modal');
              this.notification.error('Something happened', error);
              console.log('error ::', error);
            });
  }

  public updateCourse() {
    this.loading.update = true;
    const obj = {
      department_id: this.departmentId
    };
    const forSending = {...obj, ...this.createCourseForm.value};
    this.courseManagementService.updateCourse(this.courseId, forSending)
        .subscribe((response) => {
              this.loading.update = false;
              console.log(response);
              this.rerender('course');
              this.getCourseOfStudy();
              this.triggerModalOrOverlay('close', 'course_modal');
              this.notification.success('Updated Successfully');
            },
            error => {
              this.loading.update = false;
              this.triggerModalOrOverlay('close', 'course_modal');
              this.notification.error('Something happened', error);
              console.log('error ::', error);
            });
  }

  //Setting HOD
  setHOD(staffId, acting) {
    if (!window.confirm('Are you sure you want Change HOD')) {
      return;
    }
    this.loading.setHOD = true;
    console.log(acting.checked);
    const forSending = {
      department_id: this.departmentId,
      staff_id: staffId,
      status: 1
    };
    if (acting.checked) {
      forSending.status = 2;
    }
    this.administration.setHOD(forSending)
        .subscribe((response) => {
              this.loading.setHOD = false;
              console.log('Set HOD Success', response);
              this.notification.success('Success');
            },
            (error) => {
              this.loading.setHOD = false;
              console.log('Set HOD Error', error);
              this.notification.error('Something went wrong. Please retry');
            });
  }

  // Department Staff
  private getStaffByDepartment() {
    this.loading.staff = true;
    this.staffConfigService.getStaffByDepartment(this.departmentId)
        .subscribe((response) => {
              // console.log(response);
            this.loading.staff = false;
            this.toRemoveByPermission();
            this.allDepartmentStaff = response;
              if (this.allDepartmentStaff.length > 0) {
                this.dtTrigger2.next();
              }
              const selectedLecturerIds: any[] = [];
              this.allDepartmentStaff.forEach((staff) => {
                // console.log(staff.id);
                selectedLecturerIds.push(staff.id);
              });
              this.createDepartmentStaffForm.controls['lecturer_ids'].setValue(selectedLecturerIds);
            },
          error => {
            this.loading.staff = false;
            this.notification.error('Staff list could not be loaded. Please retry', error);
            console.log('error ::', error);
          });
  }

  public addStaffToDepartment() {
    this.loading.addStaff = true;
    this.staffConfigService.addStaffToDepartment(this.departmentId, this.createDepartmentStaffForm.value)
        .subscribe((response) => {
              this.loading.addStaff = false;
              console.log(response);
              this.rerender('staff');
              this.notification.success('Success!');
            },
            error => {
              this.loading.addStaff = false;
              this.notification.error('Something happened', error);
              console.log('error ::', error);
            });
  }

  public getProgrammes() {
    this.staffConfigService.getProgrammeList()
      .subscribe((response) => {
          this.allProgrammes = response.data;
        },
        error => {
          this.notification.error('Unable to load Programmes, please retry');
        });
  }

  getLevelFromProgramme(programme_id) {
    this.allProgrammes.forEach((programme) => {
      if (parseInt(programme.id) === parseInt(programme_id)) {
        this.programmeLevels = programme['levels'];
      }
    });
  }

  public getDegrees() {
    this.staffConfigService.getDegreeList()
        .subscribe((response) => {
              this.allDegrees = response.data;
            },
            error => {
              this.notification.error('Unable to load Degrees, please retry');
            });
  }

  private getCourseByDepartment() {
    this.loading.course = true;
    this.staffConfigService.getCoursesByDepartment(this.departmentId)
      .subscribe((response) => {
        this.loading.course = false;
        this.toRemoveByPermission();
        this.allDepartmentalCourses = response;
        if (this.allDepartmentalCourses.length > 0) {
          this.dtTrigger.next();
        }
        },
        error => {
          this.loading.course = false;
          console.log(error);
          this.notification.error('Unable to load Departmental Courses, please retry');
        });
  }

  public courseOfStudyUpdateModal(courseOfStudy) {
    this.modal_title = 'Update Course of Study';
    this.loading.updateButton = true;
    this.courseOfStudyId = courseOfStudy.id;
    this.triggerModalOrOverlay('open', 'course_of_study_modal');
    this.createForm = this.fb.group(courseOfStudy);
  }

  public courseOfStudyCreateModal() {
    this.modal_title = 'Create Course of Study';
    this.loading.updateButton = false;
    this.createForm.reset();
    this.triggerModalOrOverlay('open', 'course_of_study_modal');
  }

  public courseUpdateModal(course) {
    this.modal_title = 'Update Course';
    this.loading.updateButton = true;
    this.courseId = course.id;
    this.createCourseForm.patchValue(course);
    // this.createCourseForm = this.fb.group(course);
    this.triggerModalOrOverlay('open', 'course_modal');
  }

  public courseCreateModal() {
    this.modal_title = 'Create Course';
    this.loading.updateButton = false;
    this.createCourseForm.reset();
    this.triggerModalOrOverlay('open', 'course_modal');
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  rerender(listType): void {
    switch (listType) {
      case 'course':
        this.allDepartmentalCourses = [];
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
            this.getCourseByDepartment();
          });
        }
        break;
      case 'staff':
        this.allDepartmentStaff = [];
        if (this.dtElement.dtInstance) {
          this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            dtInstance.destroy();
          });
          this.getStaffByDepartment();
        }
        break;
      default: return;
    }
    // if(listType == 'course_of_study'){
    //   this.allDepartmentalCourses = [];
    //   this.getCourseByDepartment();
    // }
  }

  /*public toRemoveByPermission () {
    this.permissions.forEach(permission => {
      if (permission && !this.iAmPermitted(permission)) {
        document.getElementById(permission).style.display = 'none';
        const class_permissions = document.getElementsByClassName(permission);
        for (let i = 0; i < class_permissions.length; i++) {
          // class_permissions[i].style.display = 'none';
        }
        // console.log('Not permitted', permission);
      } else {
        // console.log('Permitted', permission);
      }
    });
  }*/
  private toRemoveByPermission () {
    for (const permission in this.permissions) {
      this.permissions[permission] = this.iAmPermitted(permission);
    }
  }
}
