import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {DepartmentsComponent} from './departments.component';
import { DepartmentViewComponent } from './department-view/department-view.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule
    ],
    declarations: [
        // DepartmentsComponent
    // DepartmentViewComponent
    ]
})
export class DepartmentsModule { }
