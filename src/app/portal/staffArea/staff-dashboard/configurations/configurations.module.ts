import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {CourseOfStudyComponent} from './course-of-study/course-of-study.component';
import {DegreeComponent} from './degree/degree.component';
import {DepartmentsComponent} from './departments/departments.component';
import {LevelComponent} from './level/level.component';
import {ProgrammeComponent} from './programme/programme.component';
import {SchoolTypeComponent} from './school-type/school-type.component';
import {SessionComponent} from './session/current-session.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StaffDashboardComponent} from '../dashboard-component.component';
import {FacultyComponent} from './faculties/faculty.component';
import {CourseComponent} from './course-management/course/course.component';
import {CurriculumCourseComponent} from './course-management/curriculum-course/curriculum-course.component';
import {CurriculumComponent} from './course-management/curriculum/curriculum.component';
import {CourseManagementModule} from './course-management/course-management.module';
import {ActiveHourComponent} from './active-hour/active-hour.component';
import {AllSessionComponent} from './session/all-session.component';
import {HostelComponent} from './hostel/hostel.component';
import {RoomComponent} from './room/room.component';
import {BedspaceComponent} from './bedspace/bedspace.component';
import {DataTablesModule} from 'angular-datatables';
import {HostelSessionComponent} from './hostel-session/hostel-session.component';
import {ReservationComponent} from '../reservation/reservation/reservation.component';
import {ReservationModule} from '../reservation/reservation.module';
import {PaymentComponent} from '../payment/payment/payment.component';
import {PaymentModule} from '../payment/payment.module';
import {HostelAllocationModule} from '../hostel-allocation/hostel-allocation.module';
import {HostelAllocationComponent} from '../hostel-allocation/hostel-allocation/hostel-allocation.component';
import {CgpaConfigComponent} from './cgpa-config/cgpa-config.component';
import {FacultyViewComponent} from './faculties/faculty-view/faculty-view-component';
import {ProgrammeViewComponent} from './programme/programme-view/programme-view.component';
import {ProbationLimitComponent} from './probation-limit/probation-limit.component';
import {SettingsComponent} from './settings/settings.component';
import {ZoneComponent} from './zone/zone.component';
import {MatricNoGenerationComponent} from './matric-no-generation/matric-no-generation.component';
import {DragulaModule} from "ng2-dragula";
import { ScreeningCenterComponent } from './screening-center/screening-center.component';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {SessionViewComponent} from "./session/session-view/session-view.component";
import {DepartmentViewComponent} from "./departments/department-view/department-view.component";
import { ProgrammeTypeComponent } from './programme-type/programme-type.component';


const CONFIG_ROUTES: Routes = [
    {
        path: 'config',
        component: StaffDashboardComponent,
        children: [
            {path: 'course-of-study', component: CourseOfStudyComponent},
            {path: 'degree', component: DegreeComponent},
            {path: 'department', component: DepartmentsComponent},
            {path: 'department/:id', component: DepartmentViewComponent},
            {path: 'faculty', component: FacultyComponent},
            {path: 'faculty/:id', component: FacultyViewComponent},
            {path: 'level', component: LevelComponent},
            {path: 'programme-type', component: ProgrammeTypeComponent},
            {path: 'programme', component: ProgrammeComponent},
            {path: 'programme/:id', component: ProgrammeViewComponent},
            {path: 'school-type', component: SchoolTypeComponent},
            {path: 'session', component: SessionComponent},
            {path: 'all-session', component: AllSessionComponent},
            {path: 'active-hour', component: ActiveHourComponent},
            {path: 'hostel', component: HostelComponent},
            {path: 'hostel-session', component: HostelSessionComponent},
            {path: 'reservation', component: ReservationComponent},
            {path: 'hostel-allocation', component: HostelAllocationComponent},
            {path: 'room/:id', component: RoomComponent},
            {path: 'payment', component: PaymentComponent},
            {path: 'bedspace/:id', component: BedspaceComponent},
            {path: 'cgpa', component: CgpaConfigComponent},
            {path: 'probation', component: ProbationLimitComponent},
            {path: 'settings', component: SettingsComponent},
            {path: 'zone', component: ZoneComponent},
            {path: 'matric', component: MatricNoGenerationComponent},
            {path: 'screening-center', component: ScreeningCenterComponent},
            {path: 'session/:id', component: SessionViewComponent}
        ]
    }
];

@NgModule({
  imports: [
    CommonModule,
    ReservationModule,
    PaymentModule,
    HostelAllocationModule,
    FormsModule,
    ReactiveFormsModule,
    CourseManagementModule,
    DataTablesModule,
    DragulaModule,
    MultiselectDropdownModule,
    RouterModule.forChild(CONFIG_ROUTES)
  ],
  declarations: [
    DegreeComponent,
    FacultyComponent,
   // FacultyViewComponent,
    LevelComponent,
    CourseOfStudyComponent,
    ProgrammeComponent,
   // ProgrammeViewComponent,
    SchoolTypeComponent,
    SessionComponent,
    DepartmentsComponent,
    ActiveHourComponent,
    AllSessionComponent,
    HostelComponent,
    RoomComponent,
    BedspaceComponent,
    HostelSessionComponent,
    CgpaConfigComponent,
    ProbationLimitComponent,
    SettingsComponent,
    MatricNoGenerationComponent,
    ZoneComponent,
    ScreeningCenterComponent,
      SessionViewComponent,
      ProgrammeTypeComponent,
  ],
  exports: [
    DegreeComponent,
    LevelComponent,
    ReservationModule,
    FacultyComponent,
    // FacultyViewComponent,
    ProgrammeComponent,
      ProgrammeTypeComponent,
    // ProgrammeViewComponent,
    CourseOfStudyComponent,
    SchoolTypeComponent,
    SessionComponent,
    DepartmentsComponent,
    RouterModule
  ]
})
export class ConfigurationsModule {
}
