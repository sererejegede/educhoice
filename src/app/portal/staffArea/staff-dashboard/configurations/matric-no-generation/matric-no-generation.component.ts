import {Component, ElementRef, OnDestroy, OnInit} from "@angular/core";
import {DragulaService} from "ng2-dragula";
import {StaffConfigService} from "../../../../../../services/api-service/staff-config.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NotificationService} from "../../../../../../services/notification.service";
import {AuthenticationService} from "../../../../../../services/authentication.service";

declare const $: any;

@Component({
    selector: 'app-matric-no-generation',
    templateUrl: './matric-no-generation.component.html',
    styleUrls: ['./matric-no-generation.component.css']
})
export class MatricNoGenerationComponent implements OnInit, OnDestroy {

  selectedMatricFormatVariables: any = [];
  selectedMatricFormatVariables2: any = [];
  matricFormatVariables = [
    {
      label: "Year(4)",
      test: "2018",
      value: "YYYY"
    },
    {
      label: "Year(3)",
      test: "018",
      value: "YYY"
    },
    {
      label: "Year(2)",
      test: "18",
      value: "YY"
    },
    {
      label: "FacultyCode",
      test: "SCI",
      value: "FAC"
    },
    {
      label: "Department Code",
      test: "PHY",
      value: "DEP"
    },
    {
      label: "Course of Study Code",
      test: "EPH",
      value: "COD"
    },
    {
      label: "SerialNumber",
      test: "1289",
      value: "SN"
    }
  ];
  allProgrammes: any;
  matricNumberFormats: any[] = [];
  selectedFormat: any;
  createForm: FormGroup;
  updateForm: FormGroup;
  selector: string;
  loader = {
    creating: false,
    requesting: false
  };
  serialNumberConfig = [
    {
      label: 'Default',
      value: 'default'
    },
    {
      label: 'Faculty',
      value: 'faculty'
    },
    {
      label: 'Department',
      value: 'department'
    },
    {
      label: 'Course',
      value: 'course'
    }
  ];


  constructor(private dragula: DragulaService,
              private _el: ElementRef,
              private staffConfigService: StaffConfigService,
              private authService: AuthenticationService,
              private fb: FormBuilder,
              private notification: NotificationService) {
    dragula.setOptions('container2', {move: true});
    dragula.dropModel.subscribe(value => this.onDropModel(value.slice(1)));
    this.createForm = this.fb.group(MatricNoGenerationComponent.createMatricFormat());
    this.updateForm = this.fb.group(MatricNoGenerationComponent.updateMatricFormat())
  }

  static createMatricFormat = function () {
    return {
      programme_id: ['', Validators.compose([Validators.required])],
      serial_number_type: ['', Validators.compose([Validators.required])]
    }
  };

  static updateMatricFormat = function () {
    return {
      programme_id: ['', Validators.compose([Validators.required])],
      serial_number_type: ['', Validators.compose([Validators.required])]
    }
  };

  ngOnInit() {
    this.listMatricFormats();
  }

  ngOnDestroy() {
    if (this.dragula.find('container2') !== undefined) {
      this.dragula.destroy('container2')
    }
  }

  getMatricFormat() {
    const matric = {
      programme_id: this.createForm.value.programme_id,
      serial_number_type: this.createForm.value.serial_number_type,
      format: this.selectedMatricFormatVariables,
      separator: this._el.nativeElement.querySelector('#selector').value
    };
    console.log('Generated', matric);
    return matric;
  }

  private onDropModel(args) {
    let [el, target, source] = args;
  }

  public getAllProgrammes() {
    this.loader.requesting = true;
    this.staffConfigService.getAllProgramme()
        .subscribe((allProgrammesResponse) => {
              this.allProgrammes = allProgrammesResponse['data'];
              this.allProgrammes.forEach((program) => {
                this.matricNumberFormats.forEach((format) => {
                  if( parseInt(program['id']) === parseInt(format['programme_id'])){
                    format['programObject'] = program;
                  }
                })
              });
              this.loader.requesting = false;
              console.log('All programmes response ', allProgrammesResponse);
            },
            (error) => {
              console.log('All programmes response ', error);
            });
  }

  saveFormat() {
    this.loader.creating = true;
    console.log('Matric Number Format', this.getMatricFormat());
    this.staffConfigService.createMatricNumber(this.getMatricFormat())
        .subscribe((response) => {
              this.loader.creating = false;
              this.notification.success('Successfully created');
              this.triggerModalOrOverlay('close', 'openModal');
              console.log('Matriculation Number Generation success', response);
          this.listMatricFormats();
            },
            (error) => {
              this.loader.creating = false;
              this.notification.error('Could not create Matriculation Number format! Try again', error);
              this.triggerModalOrOverlay('close', 'openModal');
              console.log('Matriculation Number Generation Error', error)
            })
  }

  listMatricFormats() {
    this.loader.requesting = true;
    this.staffConfigService.listMatricNumberFormats()
      .subscribe((response) => {
        console.log(response);
        this.matricNumberFormats = response['data'];
        this.getAllProgrammes();
        this.loader.requesting = false;
      },
      (error) => {
        this.loader.requesting = false;
        this.notification.error('Something went wrong, try again', error);
        console.log('Matriculation Number Listing ', error)
      })
  }

  editFormat(ind){
    this.selectedFormat = this.matricNumberFormats[ind];
    this.createForm.reset();
    this.updateForm.patchValue(this.selectedFormat);
    this.triggerModalOrOverlay('open', 'openModal');
    console.log('Selected Format', this.selectedFormat);
  }

  removeFormat(ind){
    this.selectedFormat = this.matricNumberFormats[ind];
    console.log('Selected Format', this.selectedFormat);
  }

  selectMatricFormatVariable(ind) {
    const admissionVariable = this.matricFormatVariables[ind].value;
    const test = this.matricFormatVariables[ind].test;
    // console.log('Matric format Variables', admissionVariable);
    const variableIndex = this.selectedMatricFormatVariables.indexOf(admissionVariable);
    if (!this.selectedMatricFormatVariables.includes(admissionVariable)) {
      this.selectedMatricFormatVariables.push(admissionVariable);
      console.log('Format Variables', this.selectedMatricFormatVariables);
    }
    else {
      this.selectedMatricFormatVariables.splice(variableIndex, 1);

    }
    const variableIndex2 = this.selectedMatricFormatVariables2.indexOf(test);
    if (!this.selectedMatricFormatVariables2.includes(test)) {
      this.selectedMatricFormatVariables2.push(test);
      console.log('Format Variables2', this.selectedMatricFormatVariables2);
    }
    else {
      this.selectedMatricFormatVariables2.splice(variableIndex2, 1);

    }
  }

  openModal(){
    this.triggerModalOrOverlay('open', 'openModal')
  }

  triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }

  public iAmPermitted(routeName):boolean{
      return this.authService.iAmPermitted(routeName);
  }

}
