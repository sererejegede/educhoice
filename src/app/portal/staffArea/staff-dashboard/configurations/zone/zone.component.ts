import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {AuthenticationService} from '../../../../../../services/authentication.service';
import {Cache} from '../../../../../../utils/cache';
import {Router} from '@angular/router';
import {StudentServiceService} from '../../../../../../services/api-service/student-service.service';
import {resetDataForMultiSelect} from '../../../../../../utils/util';
import {IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts} from "angular-2-dropdown-multiselect";

declare const $;


@Component({
  selector: 'app-zone',
  templateUrl: './zone.component.html',
  styleUrls: ['./zone.component.css']
})
export class ZoneComponent implements OnInit {

  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);
// Text configuration

  public createForm: FormGroup;
  public stateForm: FormGroup;
  public selectedStates: any;
  private id: number;
  optionsModel = [];
  allStates: IMultiSelectOption[];
// Settings configuration
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 3,
    displayAllSelectedText: true
  };

// Text configuration
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    searchEmptyResult: 'Nothing found...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Select',
    allSelected: 'All selected',
  };

  public feedBack = {
    allResponse: [],
    statesArray: [],
    zoneName: '',
    allCountry: [],
    allStates: [],
    viewDetails: [],
    facultyType: [],
    moduleName: 'Zone',
    formType: 'Create',
    loader: false,
    showUpdate: false,
    submitStatus: false,
    showUpdateButton: false
  };

  static formData = function () {
    return {
      name: ['', Validators.compose([Validators.required])],
    };
  };

  static stateData = function () {
    return {
      stateForm: []
    };
  };


  constructor(private staffConfigService: StaffConfigService,
              private studentService: StudentServiceService,
              private router: Router,
              private notification: NotificationService,
              private authService: AuthenticationService,
              private fb: FormBuilder) {
    this.createForm = this.fb.group(ZoneComponent.formData());
    this.stateForm = this.fb.group(ZoneComponent.stateData());
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
  }

  ngOnInit() {
    this.loadFunction();
  this.stateForm.controls['stateForm'].valueChanges
      .subscribe((response) => {
        this.feedBack.statesArray = response;
          console.log('this feedBack statesArray :: ', this.feedBack.statesArray);
      });
  }

  loadFunction() {
    this.getZone();
    this.getCountry();
  }

  /**
   * creating degree
   */
  public createFormModal() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.postZone(this.createForm.value)
      .subscribe((response) => {
          (this.feedBack.allResponse.length === 0) ? this.feedBack.allResponse.push(response) : this.feedBack.allResponse.unshift(response);
          this.notification.success(this.feedBack.moduleName + ' was created successfully');
          this.callBackFunction();
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to Create ' + this.feedBack.moduleName + ', please retry');
        });
  }

  private callBackFunction() {
    this.feedBack.submitStatus = false;
    this.createForm.reset();
    this.closeModal();
  }

  /**
   * update degree
   */
  public updateFormModal() {
    this.feedBack.submitStatus = true;
    console.log('update form :: ', this.createForm.value);
    this.staffConfigService.updateZone(this.id, this.createForm.value)
      .subscribe((response) => {
          this.feedBack.allResponse.forEach((val, i) => {
            if (val.id === this.id) {
              this.feedBack.allResponse[i] = response;
              this.notification.success(this.feedBack.moduleName + ' was updated successfully');
            }
          });
          this.callBackFunction();
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to Update ' + this.feedBack.moduleName + ', please retry', error);
        });
  }

  /**
   * update degree
   */
  public updateState() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.postZoneStates(this.id, {state: this.feedBack.statesArray})
      .subscribe((response) => {
         console.log('response from state id :: ', response)
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to Update states, please retry', error);
        });
  }

  /**
   * listing all
   */
  public getZone() {
    this.feedBack.loader = true;
    this.staffConfigService.getZone()
      .subscribe((response) => {
          console.log('all zones :: ', response)
          this.feedBack.allResponse = response.data;
          if (this.feedBack.allResponse.length > 0) {
            this.dtTrigger.next();
          }
          this.feedBack.loader = false;
        },
        error => {
          this.notification.error('Unable to load ' + this.feedBack.moduleName + ', please retry');
          this.feedBack.loader = false;
        });
  }

  /**
   * listing all country
   */
  public getCountry() {
    this.staffConfigService.getCountry()
      .subscribe((response) => {
          console.log('all country :: ', response);
          this.feedBack.allCountry = response;
        },
        error => {
          this.notification.error('Unable to load country, please retry');
        });
  }

  /**
   * listing all country
   */
  public getCountryById(event) {
    const id = event.target.value;
    this.staffConfigService.getCountryById(id)
      .subscribe((response) => {
          this.allStates = response.states;
          // this.feedBack.allStates = resetDataForMultiSelect(response.states, 'name');
          console.log('all state  :: ', this.feedBack.allStates)

        },
        error => {
          this.notification.error('Unable to load state, please retry');
        });
  }


  /**
   * editting data
   */
  onEdit(data) {
    this.id = data.id;
    this.feedBack.showUpdate = true;
    this.feedBack.formType = 'Update';
    this.createForm = this.fb.group(data);
    this.feedBack.showUpdateButton = true;
    this.openModal();

  }


  /**
   * FOR Viewing details
   */
  onView(data) {
    this.feedBack.viewDetails = data;
    $('#viewModal').modal('show');
  }


  /**
   * open modal
   */
  openModal() {
    this.feedBack.showUpdate = false;
    if (this.feedBack.showUpdateButton === true) {
      this.createForm.reset();
      this.feedBack.showUpdateButton = false;
      this.feedBack.formType = 'Create';
    }
    $('#openModal').modal('show');
  }

  /**
   * close modal
   */
  closeModal() {
    $('#openModal').modal('hide');
    this.feedBack.formType = 'Create';
    this.feedBack.showUpdateButton = false;
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {

    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === 'open') ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  viewReport(id) {
    this.router.navigate(['/staff/reports/department', id]);
  }

  addState(data) {
    this.id = data.id;
    this.feedBack.zoneName = data.name;
    $('#stateModal').modal('show');
  }
}
