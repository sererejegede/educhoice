import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgrammeTypeComponent } from './programme-type.component';

describe('ProgrammeTypeComponent', () => {
  let component: ProgrammeTypeComponent;
  let fixture: ComponentFixture<ProgrammeTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgrammeTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammeTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
