import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {Cache} from '../../../../../../utils/cache';
import {JSONPARSE} from '../../../../../../utils/json-parse';
import {AuthenticationService} from '../../../../../../services/authentication.service';


declare const $: any;

@Component({
  selector: 'app-probation-limit',
  templateUrl: './probation-limit.component.html',
  styles: []
})
export class ProbationLimitComponent implements OnInit {

  public createForm: FormGroup;
  private id: number;
  public allSessions: any[] = [];
  public allProgrammes: any[] = [];
  allProbationLimitConfig;
  dtOptions: DataTables.Settings = {};
  //noinspection TypeScriptValidateTypes
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    programmeType: [],
    showUpdateButton: false,
    years: [],
    allCurrentSession: [],
    moduleName: 'Probation Limit Configuraton',
    formType: 'Create',
    submitStatus: false,
    loader: false,
  };

  static formData = function () {
    return {
      min: ['', Validators.compose([Validators.required])],
      max: ['', Validators.compose([Validators.required])],
      title: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(150)])],
      semester: ['', Validators.compose([Validators.required])],
      programme_id: [''],
      session_id: [''],
      probation_limit_id: ['']
    };
  };


  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private authService: AuthenticationService,
              private fb: FormBuilder,
              private el: ElementRef) {
    this.createForm = this.fb.group(ProbationLimitComponent.formData());
  }

  private initNewDegreeClass() {
    return this.fb.group({
      degree_class: ['', Validators.compose([Validators.required])]
    });
  }

  //For new form field
  //Add new form field
  addNewDegreeClass(){
    const control = <FormArray>this.createForm.controls['newDegreeClass'];
    control.push(this.initNewDegreeClass());
  }
  //Delete form field
  deleteRow(index: number){
    const control = <FormArray>this.createForm.controls['newDegreeClass'];
    control.removeAt(index);
  }

  public getProbationLimitConfigurations() {
    this.feedBack.loader = true;
    this.staffConfigService.getAllProbationLimitConfigurations()
        .subscribe((response) => {
              Cache.set('probation_limit_config', response);
              this.allProbationLimitConfig = response;
              if( this.allProbationLimitConfig['data'].length > 0){
                this.dtTrigger.next();
              }
              this.feedBack.loader = false;
              console.log(this.feedBack.moduleName, ':: ', response);
            },
            error => {
              this.notification.error('Unable to load Probation Limit Configuration, please retry', error);
              this.feedBack.loader = false;
              console.log('error', error);
            });
  }

  public getAllSessions() {
    this.staffConfigService.getAllSession().subscribe(
        (allSessionsResponse) => {
          this.allSessions = allSessionsResponse.data;
          console.log('All sessions response ', allSessionsResponse);
        },
        (error) => {
          console.log('Could not load Sessions');
        }
    );
  }

  public getAllPrograms(){
    this.staffConfigService.getAllProgramme().subscribe(
        (allProgrammesResponse) => {
          this.allProgrammes = allProgrammesResponse.data;
          console.log('All Programmes Response :::', allProgrammesResponse)
        },
        (error) => {
          console.log('Could not load Programmes')
        }
    )
  }

  ngOnInit() {
    this.getProbationLimitConfigurations();
    this.getAllSessions();
    this.getAllPrograms();
  }

  private callBackFunction() {
    this.feedBack.submitStatus = false;
    this.createForm.reset();
    this.closeModal();
  }

  updateFormModal() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.updateProbationLimit(this.id, this.createForm.value)
        .subscribe((response) => {
          this.feedBack.allCurrentSession['data'] = response;
          console.log(response);
          this.notification.success('Successfully updated');
          this.callBackFunction();
        },
        error => {
        this.feedBack.submitStatus = false;
        const error_message = JSONPARSE(error._body, 'date_start') || 'Unable to Update, please retry ';
        this.notification.error(error_message);
        });
  }

  public createFormModal() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.createProbationLimit(this.createForm.value)
        .subscribe((response) => {
          (this.feedBack.allResponse.length === 0) ? this.feedBack.allResponse.push(response) : this.feedBack.allResponse.unshift(response);
          this.notification.success(this.feedBack.moduleName + ' was created successfully');
          this.callBackFunction();
        },
        error => {
          this.feedBack.submitStatus = false;
          // const error_message = JSONPARSE(error._body, 'date_start') || 'Unable to Create ' + this.feedBack.moduleName + ', please retry';
          this.notification.error(`Unable to Create ${this.feedBack.moduleName}, please retry`, error);
        });
  }

  onEdit(data) {
    this.id = data.id;
    this.openModal();
    this.feedBack.formType = 'Update';
    this.createForm = this.fb.group(data);
    this.feedBack.showUpdateButton = true;
  }

  /**
   * FOR Viewing details
   */
  onView(data) {
    this.feedBack.viewDetails = data;
    $('#viewModal').modal('show');
  }


  /**
   * open modal
   */
  openModal() {
    this.feedBack.formType = 'Create';
    $('#openModal').modal('show');
  }

  /**
   * close modal
   */
  closeModal() {
    $('#openModal').modal('hide');
    this.feedBack.formType = 'Create';
    this.feedBack.showUpdateButton = false;
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

    public iAmPermitted(routeName):boolean{
        return this.authService.iAmPermitted(routeName);
    }


}
