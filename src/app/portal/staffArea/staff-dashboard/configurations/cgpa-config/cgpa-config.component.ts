import {Component, OnInit, AfterViewInit, ElementRef, ViewChild} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {FormBuilder, FormGroup, Validators, FormArray} from '@angular/forms';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {Cache} from '../../../../../../utils/cache';
import {JSONPARSE} from '../../../../../../utils/json-parse';
import {AuthenticationService} from '../../../../../../services/authentication.service';

declare const $: any;

@Component({
  selector: 'app-cgpa-config',
  templateUrl: './cgpa-config.component.html',
  styles: []
})
export class CgpaConfigComponent implements OnInit {

  public createForm: FormGroup;
  private id: number;
  dtOptions: DataTables.Settings = {};
  //noinspection TypeScriptValidateTypes
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  // dtElement: DataTableDirective = new DataTableDirective(this.el);
  allCgpaConfiguration: any[] = [];
  cgpaConfiguration: any;
  public modifiedObject = {
    degree_classes: [],
    grades: []
  };
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    programmeType: [],
    showUpdateButton: false,
    years: [],
    allCurrentSession: [],
    moduleName: 'CGPA Configuration',
    formType: 'Create',
    submitStatus: false,
    loader: false,
  };

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private authService: AuthenticationService,
              private fb: FormBuilder,
              private el: ElementRef) {
    this.createForm = this.fb.group({
      label: ['', Validators.compose([Validators.required])],
      max_point: ['', Validators.compose([Validators.required])],
      pass_mark_type: ['', Validators.compose([Validators.required])],
      pass_mark_score: ['', Validators.compose([Validators.required])],
      degree_class: this.fb.array([this.initNewDegreeClass()]),
      grade: this.fb.array([this.initGrade()])
    });
  }

  private initNewDegreeClass() {
    return this.fb.group({
      degree_name: ['', Validators.compose([Validators.required])],
      degree_lower: ['', Validators.compose([Validators.required])],
      degree_upper: ['', Validators.compose([Validators.required])]
    });
  }

  private initGrade() {
    return this.fb.group({
      grade_name: ['', Validators.compose([Validators.required])],
      grade_unit: ['', Validators.compose([Validators.required])],
      grade_lower: ['', Validators.compose([Validators.required])],
      grade_upper: ['', Validators.compose([Validators.required])]
    });
  }

  getConfigData() {
    const cgpa = {
      max_point: this.createForm.value.max_point,
      pass_mark: {
        type: this.createForm.value.pass_mark_type,
        score: this.createForm.value.pass_mark_score
      },
      grades: {},
      degree_class: {}
    };
    for (let i = 0; i < this.createForm.value.degree_class.length; i++) {
      cgpa.degree_class[this.createForm.value.degree_class.map(deg => deg.degree_name)[i]] = [this.createForm.value.degree_class[i].degree_lower, this.createForm.value.degree_class[i].degree_upper];
    }
    for (let j = 0; j < this.createForm.value.grade.length; j++) {
      cgpa.grades[this.createForm.value.grade.map(grad => grad.grade_name)[j]] = {
        unit: this.createForm.value.grade[j].grade_unit,
        score: {
          min: this.createForm.value.grade[j].grade_lower,
          max: this.createForm.value.grade[j].grade_upper
        }
      };
    }
    return cgpa;
  }

  // For new form field
  // Add new form field
  addNewDegreeClass() {
    const control = <FormArray>this.createForm.controls['degree_class'];
    control.push(this.initNewDegreeClass());
  }

  // Delete form field
  deleteRow(index: number) {
    const control = <FormArray>this.createForm.controls['degree_class'];
    control.removeAt(index);
  }

  addGrade() {
    const control = <FormArray>this.createForm.controls['grade'];
    control.push(this.initGrade());
  }

  // Delete form field
  deleteGrade(index: number) {
    const control = <FormArray>this.createForm.controls['grade'];
    control.removeAt(index);
  }

  public getAllCgpaConfigurations() {
    this.feedBack.loader = true;
    this.staffConfigService.getAllCgpaConfigurations()
      .subscribe((response) => {
          Cache.set('current_Session', response);
          this.allCgpaConfiguration = response;
          if (this.allCgpaConfiguration['data'].length > 0) {
            this.dtTrigger.next();
          }
          this.feedBack.loader = false;
        },
        error => {
          this.notification.error('Unable to load Current Session, please retry');
          this.feedBack.loader = false;
          console.log('error', error);
        });
  }

  ngOnInit() {
    this.getAllCgpaConfigurations();
  }

  private callBackFunction() {
    this.feedBack.submitStatus = false;
    this.createForm.reset();
    this.closeModal();
  }

  public createFormModal() {
    // console.log(this.createForm.value);

    this.feedBack.submitStatus = true;
    this.staffConfigService.createCgpaConfiguration(this.getConfigData())
      .subscribe((response) => {
          (this.feedBack.allResponse.length === 0) ? this.feedBack.allResponse.push(response) : this.feedBack.allResponse.unshift(response);
          this.notification.success(this.feedBack.moduleName + ' was created successfully');
          this.callBackFunction();
          this.rerender();
        },
        error => {
          this.feedBack.submitStatus = false;
          const error_message = JSONPARSE(error._body, 'date_start') || 'Unable to Create ' + this.feedBack.moduleName + ', please retry';
          this.notification.error(error_message);
        });
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      // Or call your getAll... method
      this.getAllCgpaConfigurations();
    });
  }

  onEdit(data) {
    this.id = data.id;
    this.openModal();
    this.feedBack.formType = 'Update';
    this.createForm = this.fb.group(data);
    this.feedBack.showUpdateButton = true;
  }

  /**
   * FOR Viewing details
   */
  onView(data) {
    this.feedBack.viewDetails = data;
    $('#viewModal').modal('show');
  }


  /**
   * open modal
   */
  openModal() {
    $('#openModal').modal('show');
  }

  /**
   * close modal
   */
  closeModal() {
    $('#openModal').modal('hide');
    this.feedBack.formType = 'Create';
    this.feedBack.showUpdateButton = false;
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }


  CgpaView(cgpa) {
    this.cgpaConfiguration = cgpa;
    this.modifiedObject.degree_classes = [];
    this.modifiedObject.grades = [];
    const degree_class = cgpa.degree_class;
    const grades = cgpa.grades;
    const degree_class_arr = Object.keys(degree_class).map((key) => {
      return {[key]: degree_class[key]};
    });
    const grades_arr = Object.keys(grades).map((c) => {
      return {[c]: grades[c]};
    });
    degree_class_arr.forEach(dc => {
      this.modifiedObject.degree_classes.push(Object.entries(dc));
    });
    grades_arr.forEach(gd => {
      this.modifiedObject.grades.push(Object.entries(gd));
    });
    this.triggerModalOrOverlay('open', 'viewModal');
  }

}
