import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../../../../../../services/authentication.service';

declare const $: any;

@Component({
  selector: 'app-session-view',
  templateUrl: './session-view.component.html',
  styleUrls: ['./session-view.component.css']
})
export class SessionViewComponent implements OnInit {

  public currentSessionId: number = 0;
  public currentSession: any;
  allCourseOfStudy: any[] = [];
  allProgrammes: any[] = [];
  allDegrees: any[] = [];
  createForm: FormGroup;
  public loadMessage = 'Start';
  loading = {
    semester: false,
    update: false,
    create: false,
    updateButton: false
  };
  private departmentId;
  private courseOfStudyId;

  static createForm = function () {
    return {
      name: ['', Validators.required],
      reg_start_date: ['', Validators.required],
      reg_end_date: ['', Validators.required]
    };
  };

  constructor(private activeRoute: ActivatedRoute,
              private staffConfigService: StaffConfigService,
              private Alert: NotificationService,
              private fb: FormBuilder,
              private authService: AuthenticationService) {
    this.createForm = this.fb.group(SessionViewComponent.createForm());
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      this.currentSessionId = +params['id']; // (+) converts string 'id' to a number
      this.getCurrentSessionById(this.currentSessionId);
      // In a real app: dispatch action to load the details here.
    });
  }


  public getCurrentSessionById(currentSessionId) {
    this.loading.semester = true;
    this.staffConfigService.getSessionById(currentSessionId).subscribe(
      (sessionResponse) => {
        this.currentSession = sessionResponse;
        this.setCurrentSemester();
        // console.log('Current Session response', this.currentSession);
      },
      (error) => {
        this.loading.semester = false;
        this.Alert.error('An error occurred, could not load current session ', error)
      });
  }

  public startNewSemester() {
    this.loadMessage = 'Starting';
    this.loading.create = true;
    this.staffConfigService.startSessionSemester(this.currentSessionId, this.createForm.value).subscribe((startSemesterSessionResponse) => {
        this.loadMessage = 'Start';
        this.loading.create = false;
        this.currentSession = startSemesterSessionResponse;
        this.triggerModalOrOverlay('close', 'semester');
        this.setCurrentSemester();
        // console.log('Current semester session start response', startSemesterSessionResponse);
        // console.log('Currnet semesterrte ',currentSemester)
      },
      (error) => {
        this.loading.create = false;
        this.loadMessage = 'Start';
        this.Alert.error('An error occurred, could not start new semester ', error);
      });
  }

  public updateSessionSemester() {
    this.loadMessage = 'Updating';
    this.loading.update = true;
    this.staffConfigService.startSessionSemester(this.currentSessionId, this.createForm.value).subscribe(
      (res) => {
        this.loadMessage = 'Start';
        this.loading.update = false;
        this.currentSession = res;
        this.triggerModalOrOverlay('close', 'semester');
        this.setCurrentSemester();
      },
      (error) => {
        this.loading.update = false;
        this.loadMessage = 'Start';
        this.Alert.error('An error occurred, could not start new semester ', error);
      });
  }

  public setCurrentSemester() {
    this.loading.semester = false;
    const currentSemesterId = (this.currentSession['current_semester']['id']) ? this.currentSession['current_semester']['id'] : '0';
    const numberOfSessionSemesters = this.currentSession['session_semesters'].length;
    const numberOfLegibleProgramSemesters = parseInt(this.currentSession['programme']['semesters'], 10) + 1;

    this.selectWhereId(this.currentSession['session_semesters'], 'id', currentSemesterId)['is_current'] = true;
    this.selectWhereId(this.currentSession['session_semesters'], 'id', currentSemesterId)['date_end'] = 'In Progress';
    this.currentSession.year = `${this.currentSession.year} / ${parseInt(this.currentSession.year, 10) + 1}`;

    this.currentSession['can_create_semester'] = (numberOfSessionSemesters < numberOfLegibleProgramSemesters);
  }

  public editSessionSemester (session_semester) {
    this.loadMessage = 'Update';
    this.triggerModalOrOverlay('open', 'semester');
    this.createForm = this.fb.group(session_semester);
  }

  public openModal() {
    this.loadMessage = 'Create';
    this.createForm.reset();
    this.triggerModalOrOverlay('open', 'semester');
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  public selectWhereId(data: any[], search_key: string, id) {
    let dataItem: any[] = [];
    data.forEach(item => {
      let itemKey = parseInt(item[search_key]);
      let itemId = parseInt(id);
      if (itemKey === itemId) {
        dataItem.push(item);
      }
    });
    return dataItem[0];

  }

}
