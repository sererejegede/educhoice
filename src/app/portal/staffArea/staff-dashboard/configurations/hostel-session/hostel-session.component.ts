import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {AuthenticationService} from '../../../../../../services/authentication.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {Cache} from '../../../../../../utils/cache';
import {JSONPARSE} from '../../../../../../utils/json-parse';
import {Router} from "@angular/router";

declare const $;

@Component({
  selector: 'app-hostel-session',
  templateUrl: './hostel-session.component.html',
  styleUrls: ['./hostel-session.component.css']
})
export class HostelSessionComponent implements OnInit, AfterViewInit {
  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);


  public createForm: FormGroup;
  public filterForm: FormGroup;
  public id: number;
  public feedBack = {
    allHostels: [],
    allResponse: [],
    filterArray: [],
    viewDetails: [],
    allHostel: [],
    moduleName: 'Hostel Session',
    formType: 'Create',
    loader: false,
    filterStatus: false,
    viewFilterSearch: false,
    submitStatus: false,
    showUpdateButton: false,
    deleteStatus: false
  };
  public permissions = [
    'school.hostel.hostel.session.session.store',
  ];
  static formData = function () {
    return {
      // close_date: ['', Validators.compose([])],
      hostel_id: ['', Validators.compose([Validators.required])],
      open_date: ['', Validators.compose([Validators.required])],
      close_date: [''],
    };
  };

  static filterData = function () {
    return {
      // close_date: ['', Validators.compose([])],
      hostel_id: ['', Validators.compose([Validators.required])],
    };
  };

  ngOnInit() {
    this.loadFunction();
  }

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private authService: AuthenticationService,
              private router: Router,
              private fb: FormBuilder) {
    this.createForm = this.fb.group(HostelSessionComponent.formData());
    this.filterForm = this.fb.group(HostelSessionComponent.filterData());
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
  }

  loadFunction() {
    this.getAllHostelSession();
    this.getAllHostel();
  }

  /**
   * creating
   */
  public createFormModal() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.createAllHostelSession(this.createForm.value)
      .subscribe((response) => {
          (this.feedBack.allResponse.length === 0) ? this.feedBack.allResponse.push(response) : this.feedBack.allResponse.unshift(response);
          this.notification.success(this.feedBack.moduleName + ' was created successfully');
          this.callBackFunction();
        },
        error => {
          this.feedBack.submitStatus = false;
          const error_message = JSONPARSE(error._body, 'date_start') || 'Unable to Create ' + this.feedBack.moduleName + ', please retry';
          this.notification.error(error_message);
        });
  }

  private callBackFunction() {
    this.feedBack.submitStatus = false;
    this.createForm.reset();
    this.closeModal();
  }

  /**
   * update
   */
  public updateFormModal() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.updateAllHostelSession(this.id, this.createForm.value)
      .subscribe((response) => {
          this.feedBack.allResponse.forEach((val, i) => {
            if (val.id === this.id) {
              this.feedBack.allResponse[i] = response;
              this.notification.success(this.feedBack.moduleName + ' was updated successfully');
            }
          });
          this.callBackFunction();
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to Update ' + this.feedBack.moduleName + ', please retry', error);
        });
  }

  /**
   * listing all current session
   */
  public getAllHostelSession() {
    this.feedBack.loader = true;
    this.staffConfigService.getAllHostelSession()
      .subscribe((response) => {
          console.log('value :: ', response.data);
          this.feedBack.allResponse = response.data;
          if (this.feedBack.allResponse.length > 0) {
            this.dtTrigger.next();
          }
          this.feedBack.loader = false;
        },
        error => {
          this.notification.error('Unable to load Current Session, please retry');
          this.feedBack.loader = false;
        });
  }


  /**
   * editting data
   */
  onEdit(data) {
    this.id = data.id;
    this.feedBack.formType = 'Update';
    this.openModal();
    this.createForm = this.fb.group(data);
    this.feedBack.showUpdateButton = true;
  }

  startHostelSession () {
    this.feedBack.formType = 'Create';
    this.openModal();
    this.createForm.reset();
    this.feedBack.showUpdateButton = false;
  }

  /**
   * FOR Viewing details
   */
  onView(data) {
    this.feedBack.viewDetails = data;
    $('#viewModal').modal('show');
  }


  /**
   * open modal
   */
  openModal() {
    $('#openModal').modal('show');
  }

  /**
   * close modal
   */
  closeModal() {
    $('#openModal').modal('hide');
    this.feedBack.formType = 'Create';
    this.feedBack.showUpdateButton = false;
  }


  onViewRooms(id) {
    this.router.navigate(['../staff/config/room', id]);
  }

  onDeleteRoom(id) {
    this.id = id;
    this.feedBack.deleteStatus = true;
    this.staffConfigService.deleteHostelSession(id)
      .subscribe((response) => {
          this.feedBack.allResponse.forEach((val, i) => {
            if (val.id === id) {
              this.notification.success('Hostel deleted successfully');
              const index = this.feedBack.allResponse.indexOf(this.feedBack.allResponse[i]);
              this.feedBack.allResponse.splice(index, 1);
              this.feedBack.deleteStatus = false;
            }
          });
        },
        error => {
          this.feedBack.deleteStatus = false;
          this.notification.error('Unable to delete Hostel', error);
        });
  }

  /**
   * listing all current session
   */
  public getAllHostel() {
    this.staffConfigService.getAllHostel1()
      .subscribe((response) => {
          console.log(response.data);
          this.feedBack.allHostels = response.data;
        },
        error => {
          this.notification.error('Unable to load hostel, please retry');
          this.feedBack.loader = false;
        });
  }

  openFilter() {
    this.filterForm.reset();
    $('#filterModal').modal('show');
  }

  filter(event) {
    const id = event.target.value;
    this.feedBack.filterStatus = true;
    this.feedBack.viewFilterSearch = true;
    this.feedBack.filterArray = [];
    this.feedBack.allResponse.forEach((val) => {
      if (+val['hostel']['id'] === +id) {
        this.feedBack.filterArray.push(val);
      }
    });
    setTimeout(() => {
      this.feedBack.viewFilterSearch = false;
    }, 8000);
  }

  filterSession() {
    this.feedBack.viewFilterSearch = true;
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  allSession() {
    this.feedBack.filterStatus = false;
    this.feedBack.viewFilterSearch = false;
  }

  public toRemoveByPermission () {
    this.permissions.forEach(permission => {
      if (permission && !this.iAmPermitted(permission)) {
        document.getElementById(permission).style.display = 'none';
      }
    });
  }

  ngAfterViewInit(): void {
    this.toRemoveByPermission();
  }
}
