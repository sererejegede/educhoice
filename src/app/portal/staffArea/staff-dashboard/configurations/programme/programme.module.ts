import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgrammeComponent } from './programme.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ProgrammeViewComponent } from './programme-view/programme-view.component';



@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
    ],
    declarations: [
     // ProgrammeComponent
    // ProgrammeViewComponent
    ]
})
export class ProgrammeModule { }
