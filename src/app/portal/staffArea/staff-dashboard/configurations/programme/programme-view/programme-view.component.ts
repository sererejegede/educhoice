import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NotificationService} from '../../../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';

@Component({
  selector: 'app-programme-view',
  templateUrl: './programme-view.component.html',
  styleUrls: ['./programme-view.component.css']
})
export class ProgrammeViewComponent implements OnInit {
  public programme: any;
  public id: number;
  public newDegree: string;
  public newLevel = {
    name: '',
    order: ''
  };
  public showForm = false;
  public showLevelForm = false;

  public feedBack = {
    loading: false,
    moduleName: 'Programme',
    formType: 'Create',
    addingDegree: false,
    addingLevel: false
  };
  public viewDetails;

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.feedBack.loading = true;
      this.id = +params['id']; // (+) converts string 'id' to a number
      this.getProgramme();
    });
  }

  private getProgramme() {
    this.staffConfigService.getProgramme(this.id)
      .subscribe((response) => {
          this.programme = response; // Cache.get('faculty.' + this.id);
          this.feedBack.loading = false;
        },
        error => {
          this.notification.error('Could not load Programme', error);
          this.feedBack.loading = false;
        });
  }

  public addDegreeToProgramme() {
    this.feedBack.addingDegree = true;
    const degree = {
      name: this.newDegree,
      programme_id: this.id
    };
    this.staffConfigService.createDegree(degree)
      .subscribe((response) => {
          this.feedBack.addingDegree = false;
          if (this.programme.degrees) {
          (this.programme['degrees'].length === 0) ? this.programme['degrees'].push(response) : this.programme['degrees'].unshift(response);
        }
          this.showForm = false;
          this.notification.success('Success!');
        },
        error => {
          this.showForm = false;
          this.feedBack.addingDegree = false;
          this.notification.error('Unable to add Degree, please retry');
        });
  }

  public addLevelToProgramme() {
    this.feedBack.addingLevel = true;
    const level = {...this.newLevel, ...{programme_id: this.id}};
    // console.log(level);
    this.staffConfigService.createLevel(level)
      .subscribe((response) => {
          this.feedBack.addingLevel = false;
          if (this.programme.degrees) {
            (this.programme['levels'].length === 0) ? this.programme['levels'].push(response) : this.programme['levels'].unshift(response);
          }
          this.showForm = false;
          this.notification.success('Success!');
        },
        error => {
          this.feedBack.addingLevel = false;
          this.showForm = false;
          this.notification.error('Unable to add Level, please retry', error);
        });
  }


}
