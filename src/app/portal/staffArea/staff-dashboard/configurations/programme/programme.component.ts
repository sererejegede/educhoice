import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {Cache} from '../../../../../../utils/cache';
import {Router} from "@angular/router";
import {AuthenticationService} from "../../../../../../services/authentication.service";

declare const $;

@Component({
  selector: 'app-programme',
  templateUrl: './programme.component.html',
  styles: []
})
export class ProgrammeComponent implements OnInit {

  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);

  public createForm: FormGroup;
  private id: number;
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    programDetails: [],


    currentSemester: [],
    currentSession: [],
    session: [],
    moduleName: 'Programme',
    formType: 'Create',
    loader: false,
    submitStatus: false,
    currentSemesterLoader: false,
    SessionLoader: false,
    currentSessionLoader: false,
    showUpdateButton: false
  };
  public allCgpaConfiguration: any[] = [];
  static formData = function () {
    return {
      name: ['', Validators.compose([Validators.required])],
      semesters: ['', Validators.compose([Validators.required])],
      c_g_p_a_config_id: ['', Validators.compose([Validators.required])]
    };
  };

  constructor(private staffConfigService: StaffConfigService,
              private router: Router,
              private authService:AuthenticationService,
              private notification: NotificationService,
              private fb: FormBuilder) {
    this.createForm = this.fb.group(ProgrammeComponent.formData());
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
  }

  ngOnInit() {
    this.loadFunction();
    this.getAllCgpaConfigurations();
  }

  loadFunction() {
    this.allProgramme();
  }

  /**
   * creating degree
   */
  public getAllCgpaConfigurations() {
    this.staffConfigService.getAllCgpaConfigurations()
      .subscribe((response) => {
          this.allCgpaConfiguration = response.data;
        },
        error => {
          this.notification.error('Unable to load CGPA configurations, please retry');
          console.log('error', error);
        });
  }

  public createFormModal() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.createProgramme(this.createForm.value)
      .subscribe((response) => {
          (this.feedBack.allResponse.length === 0) ? this.feedBack.allResponse.push(response) : this.feedBack.allResponse.unshift(response);
          // this.feedBack.allResponse.unshift(response);
          this.notification.success(this.feedBack.moduleName + ' was created successfully');
          this.callBackFunction();
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to Create ' + this.feedBack.moduleName + ', please retry');
        });
  }

  private callBackFunction() {
    Cache.set('all_programmes', this.feedBack.allResponse);
    this.feedBack.submitStatus = false;
    this.createForm.reset();
    this.closeModal();
  }

  /**
   * creating degree
   */
  public updateFormModal() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.updateProgramme(this.id, this.createForm.value)
      .subscribe((response) => {
          this.feedBack.allResponse.forEach((val, i) => {
            if (val.id === this.id) {
              this.feedBack.allResponse[i] = response;
              this.notification.success(this.feedBack.moduleName + ' was updated successfully');
            }
          });
          this.callBackFunction();
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to Update ' + this.feedBack.moduleName + ', please retry');
        });
  }

  /**
   * listing all programme
   */
  public allProgramme() {
    this.feedBack.loader = true;
    this.staffConfigService.getAllProgramme()
      .subscribe((response) => {
          this.feedBack.allResponse = response['data'];
          if (this.feedBack.allResponse.length > 0) {
            this.dtTrigger.next();
          }
          this.feedBack.loader = false;
        },
        error => {
          this.feedBack.loader = false;
          this.notification.error('Unable to load Programme', error);
        });
  }

  /**
   * listing all degree
   */
  public getAllProgrammeById(id) {
    this.feedBack.currentSessionLoader = true;
    this.staffConfigService.getAllProgrammeById(id)
      .subscribe((response) => {
          this.feedBack.programDetails = response;
          this.feedBack.currentSessionLoader = false;
        },
        error => {
          this.feedBack.currentSessionLoader = false;
          this.notification.error('Unable to load Programme ', error);
        });
  }

  /**
   * getting the current semester
   */
  getCurrentSemester(programme) {
    this.feedBack.currentSemesterLoader = true;
    this.staffConfigService.getCurrentSemester(programme)
      .subscribe((response) => {
          this.feedBack.currentSemester = response;
          this.feedBack.currentSemesterLoader = false;
        },
        error => {
          this.feedBack.currentSemesterLoader = false;
          this.notification.error('Unable to get current semester');
        });
  }

  /**
   * getting the current session
   */
  getCurrentSession(programme) {
    this.feedBack.currentSessionLoader = true;
    this.staffConfigService.getCurrentSession(programme)
      .subscribe((response) => {
          this.feedBack.currentSessionLoader = false;
          this.feedBack.currentSession = response;
        },
        error => {
          this.feedBack.currentSessionLoader = false;
          this.notification.error('Unable to get current session');
        });
  }

  /**
   * getting the  session
   */
  getSession(programme) {
    this.feedBack.SessionLoader = true;
    this.staffConfigService.getSession(programme)
      .subscribe((response) => {
          this.feedBack.SessionLoader = false;
          this.feedBack.session = response.data;
        },
        error => {
          this.feedBack.SessionLoader = false;
          this.notification.error('Unable to get current session');
        });
  }

  /**
   * editting data
   */
  onEdit(data) {
    this.id = data.id;
    this.openModal();
    this.feedBack.formType = 'Update';
    // this.createForm = this.fb.group(data);
    this.createForm.patchValue(data);
    this.feedBack.showUpdateButton = true;
  }

    /**
   * open modal
   */
  openModal() {
    if (this.feedBack.showUpdateButton === true) {
      this.createForm.reset();
      this.feedBack.showUpdateButton = false;
      this.feedBack.formType = 'Create';
    }
    $('#openModal').modal('show');
  }

  /**
   * close modal
   */
  closeModal() {
    $('#openModal').modal('hide');
    this.feedBack.formType = 'Create';
    this.feedBack.showUpdateButton = false;
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {

    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

    public iAmPermitted(routeName): boolean {
        return this.authService.iAmPermitted(routeName);
    }

  onViewReport(id) {
    this.router.navigate(['/staff/reports/admission-by-programme', id]);
  }
}
