import { Component, OnInit } from '@angular/core';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {ActivatedRoute} from '@angular/router';
import {Cache} from '../../../../../../../utils/cache';
import {AuthenticationService} from '../../../../../../../services/authentication.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AdministationServiceService} from "../../../../../../../services/api-service/administation-service.service";
declare const $;

@Component({
  selector: 'app-faculty-view',
  templateUrl: './faculty-view-component.html',
  styleUrls: ['./faculty-view-component.css']
})
export class FacultyViewComponent implements OnInit {
  private department_id: number;
  public id: number;
  public faculty: any;
  public facultyDeans: any[] = [];
  public facultyStaff: any[] = [];
  public message: string;
  public modal_title: string;
  public  loading = false;
  public createForm: FormGroup;
  public feedBack = {
    facultyType: [],
    formType: 'Create',
    loading: false,
    setDean: false,
    creating: false,
    loadFacultyStaff: false,
    past_deans: false,
    submitStatus: false,
    showUpdateButton: false,
    enableChangeFaculty: false
  };
  static formData = function () {
    return {
      name: ['', Validators.compose([Validators.required])],
      code: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(3)])],
    };
  };


  constructor( private staffConfigService: StaffConfigService,
               private notification: NotificationService,
               private authService: AuthenticationService,
               private route: ActivatedRoute,
               private fb: FormBuilder,
               private administration: AdministationServiceService) {
    this.createForm = this.fb.group(FacultyViewComponent.formData());
  }

  ngOnInit() {
    this.facultyType();
    this.route.params.subscribe(params => {
      this.feedBack.loading = true;
      this.id = +params['id']; // (+) converts string 'id' to a number
      // this.feedBack.loader = true;
      this.loadFaculty();
      this.getStaffByFaculty();
      this.getPastDeans();
// In a real app: dispatch action to load the details here.
    });
  }

  public facultyType() {
    this.staffConfigService.getFacultyList()
      .subscribe((response) => {
          this.feedBack.facultyType = response.data;
        },
        error => {
          this.notification.error('Unable to load School Type, please retry', error);
        });
  }

  private getStaffByFaculty() {
    this.feedBack.loadFacultyStaff = true;
    this.staffConfigService.getStaffByFaculty(this.id)
      .subscribe((response) => {
            this.feedBack.loadFacultyStaff = false;
            this.facultyStaff = response;
        },
        error => {
          this.feedBack.loadFacultyStaff = false;
          this.notification.error('Unable to load Faculty Staff, please retry', error);
        });
  }

  private getPastDeans() {
    this.feedBack.past_deans = true;
    this.staffConfigService.getFacultyDeans(this.id)
        .subscribe((response) => {
              this.feedBack.past_deans = false;
              this.facultyDeans = response;
            },
            error => {
              this.feedBack.past_deans = false;
              this.notification.error('Unable to load Past Deans, please retry', error);
            });
  }

  public createDepartmentFormModal() {
    this.feedBack.submitStatus = true;
    this.createForm.value['faculty_id'] = this.id;
    this.staffConfigService.createDepartment(this.createForm.value)
      .subscribe((response) => {
          (this.faculty.departments.length === 0) ? this.faculty.departments.push(response) : this.faculty.departments.unshift(response);
          this.notification.success(response.name + ' department was created successfully');
          this.feedBack.submitStatus = false;
          this.createForm.reset();
          this.closeModal();
        },
        error => {
          this.feedBack.submitStatus = false;
          // const msg = error.message ? error.message : 'Unknown error';
          this.notification.error('Could not create. Please retry', error);
        });
  }

  public updateDepartmentFormModal() {
    // this.feedBack.submitStatus = true;

    this.staffConfigService.updateDepartment( this.department_id, this.createForm.value)
      .subscribe((response) => {
          this.faculty.departments.forEach((val, i) => {
            if (val.id === this.department_id) {
              this.faculty.departments[i] = response;
            }

          });
          this.notification.success('Department was updated successfully');
          this.closeModal();
        },
        error => {
          // const msg = error.message ? error.message : 'Unknown Error occurred';
          this.notification.error('Could not update. Please retry', error);
          this.closeModal();
        });
  }

  onEdit(data) {
    this.department_id = data.id;
    $('#openModal').modal('show');
    console.log(data);
    this.modal_title = `Update ${data.name}`;
    this.createForm = this.fb.group(data);
    this.feedBack.showUpdateButton = true;
  }

  openCreateModal() {
    this.modal_title = `Create Department`;
    this.createForm.reset();
    $('#openModal').modal('show');
  }

  closeModal() {
    $('#openModal').modal('hide');
    this.feedBack.formType = 'Create';
    this.feedBack.showUpdateButton = false;
  }

  setDean(staffId){
    this.feedBack.setDean = true;
    console.log(staffId);
    const forSending = {
      faculty_id: this.id,
      staff_id: staffId
    };
    this.administration.setDean(forSending)
      .subscribe((response) => {
        this.feedBack.setDean = false;
        console.log('Set Dean Success', response);
        this.notification.success('Success');
      },
      (error) => {
        this.feedBack.setDean = false;
        console.log('Set Dean Error', error);
        this.notification.error('Something went wrong. Please retry');
      })
  }

  private loadFaculty() {
    this.feedBack.loading = true;
    this.staffConfigService.getFaculty(this.id)
      .subscribe((response) => {
          // Cache.set('faculty.' + this.id, response.data);
        console.log(response)
          this.faculty =  response; // Cache.get('faculty.' + this.id);
          if (this.faculty.length > 0) {
            // this.dtTrigger.next();
          }
          // this.feedBack.loader = false;
          this.feedBack.loading = false;
        },
        error => {
          const msg = error.message ? error.message : 'Unknown error: Can\'t load faculty';
          this.message = msg;
          this.notification.error(msg, error);
          this.feedBack.loading = false;
        });
  }

  public iAmPermitted(routeName): boolean {
        return this.authService.iAmPermitted(routeName);
  }
}
