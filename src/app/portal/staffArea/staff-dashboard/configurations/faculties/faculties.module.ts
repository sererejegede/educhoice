import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FacultyComponent} from "./faculty.component";
import {ReactiveFormsModule} from "@angular/forms";
import { FacultyViewComponent } from './faculty-view/faculty-view-component';


@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule
    ],
    declarations: [
       // FacultyComponent

    // FacultyViewComponent
    ]
})
export class FacultiesModule { }
