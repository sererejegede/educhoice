import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';
import {Cache} from "../../../../../utils/cache";
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import * as utils from '../../../../../utils/util';
import {Faculty} from '../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../interfaces/department.interface';
import {Course} from '../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../services/school.service';
import {UserService} from '../../../../../services/user.service';
import {NotificationService} from '../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../services/api-service/staff-config.service';
import {AdmissionsService} from "../../../../../services/api-service/admissions.service";
import {AuthenticationService} from "../../../../../services/authentication.service";

declare const $: any;


@Component({
    selector: 'app-transfer-applications',
    templateUrl: './transfer-applications.component.html',
    styleUrls: ['./transfer-applications.component.css']
})

export class TransferApplicationsComponent implements OnInit {

    el: ElementRef;
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject();
    dtElement: DataTableDirective = new DataTableDirective(this.el);

    public allDenominations: any[] = [];
    public allLgas: any[] = [];
    public overlay = utils.overlay;
    public allTransfers: any[] = [];
    public updatedTransfer: any;
    public allFaculties: Faculty[] = [];
    public allDepartments: Department[] = [];
    public allCourses: Course[] = [];
    public allStates: any[] = [];
    public allCountries: any[] = [];
    public authenticatedUser: any;
    public myTransfers: any[] = [];
    public myEditedTransfer: any = {};
    public createTransferForm: FormGroup;
    public updateTransferForm: FormGroup;
    public editedIndex: number;
    public load = {
        requesting: {
            list: true,
            create: false
        },
        message: {
            create: 'Create',
            update: 'Update'
        },
        departments: {
            list: false,
            loading: false
        },
        courses: {
            list: false,
            loading: false
        },
        Transfers: {
            list: false,
            loading: false
        },
        lgas: {
            list: false,
            loading: false
        }

    };

    static createTransferForm = function () {
        return {
            email: ['', Validators.compose([Validators.required, Validators.email])],
            first_name: ['', Validators.compose([Validators.required])],
            other_names: '',
            last_name: ['', Validators.compose([Validators.required])],
            faculty_id: ['', Validators.compose([Validators.required])],
            department_id: ['', Validators.compose([Validators.required])],
            course_of_study_id: ['', Validators.compose([Validators.required])],
            country_id: ['', Validators.compose([Validators.required])],
            state_id: '',
            level_id: ['', Validators.compose([Validators.required])],
            matric_no: ['', Validators.compose([Validators.required])],
            lga_id: ['', Validators.compose([Validators.required])],
            birth_of_date: ['', Validators.compose([Validators.required])],
            gender: ['', Validators.compose([Validators.required])],
            religion: ['', Validators.compose([Validators.required])],
            denomination: ['', Validators.compose([Validators.required])],
            home_town: ['', Validators.compose([Validators.required])],
            marital_status: ['', Validators.compose([Validators.required])],
            middle_name: ''

        };
        // email,first_name,last_name,course_of_study_id
    };


    static updateTransferForm = function () {

        return {
            email: ['', Validators.compose([Validators.required, Validators.email])],
            first_name: ['', Validators.compose([Validators.required])],
            other_names: '',
            last_name: ['', Validators.compose([Validators.required])],
            faculty_id: ['', Validators.compose([Validators.required])],
            department_id: ['', Validators.compose([Validators.required])],
            course_of_study_id: ['', Validators.compose([Validators.required])],
            country_id: ['', Validators.compose([Validators.required])],
            state_id: '',
            level_id: ['', Validators.compose([Validators.required])],
            matric_no: ['', Validators.compose([Validators.required])],
            lga_id: ['', Validators.compose([Validators.required])],
            birth_of_date: ['', Validators.compose([Validators.required])],
            gender: ['', Validators.compose([Validators.required])],
            religion: ['', Validators.compose([Validators.required])],
            denomination: '',
            home_town: ['', Validators.compose([Validators.required])],
            marital_status: ['', Validators.compose([Validators.required])],
            middle_name: ''
        };
    };


    constructor(private _script: ScriptLoaderService,
                private userService: UserService,
                private schoolService: SchoolService,
                private fb: FormBuilder,
                private authService: AuthenticationService,
                private staffConfigService: StaffConfigService,
                private admissionsService: AdmissionsService,
                private Alert: NotificationService) {
        // this.authenticatedUser = this.userService.getAuthUser().login.user;
        this.createTransferForm = this.fb.group(TransferApplicationsComponent.createTransferForm());
        this.updateTransferForm = this.fb.group(TransferApplicationsComponent.updateTransferForm());
        this.dtOptions = {
            pagingType: 'full_numbers',
        };
        console.log('Authenticated user ', this.authenticatedUser);
    }

    ngOnInit() {
        this.getAllStudentTransferApplications();
        // this.loadTransferStatuses();
    }

    public getAllStudentTransferApplications() {
        this.load.requesting.list = true;
        this.schoolService.getAllStudentTransferApplications().subscribe(
            (allTransfersResponse) => {
                this.load.requesting.list = false;
                this.allTransfers = allTransfersResponse.data;
                console.log('All transfers response ', allTransfersResponse)
                if (this.allTransfers.length > 0) {
                    this.dtTrigger.next();
                }
                console.log('All Transfers ', this.allTransfers);
            },
            (error) => {
                console.log('Could not load list of Transfers')
                this.load.requesting.list = false;

            }
        );
    }

    public toggleApplicationStatus(ind, action?) {
        let newApplicationStatus = ( action === 'approve');
        let toggleMessage = (newApplicationStatus) ? 'Approve' : 'Decline';
        if (newApplicationStatus) {
            this.allTransfers[ind].approveloading = true;
        }
        else {
            this.allTransfers[ind].rejectloading = true;
        }
        const applicationId = this.allTransfers[ind].id;
        this.schoolService.toggleApplicationStatus(newApplicationStatus, applicationId).subscribe(
            (toggleResponse) => {
                if (toggleResponse['message']) {
                    return this.Alert.success(`${toggleResponse['message']}`);
                }
                this.allTransfers[ind]['status'] = toggleResponse['status'].toString();
                this.Alert.success(`${this.allTransfers[ind]['student']['first_name']} ${this.allTransfers[ind]['year']} ${toggleMessage}d!`);
                console.log('Toggle response ', toggleResponse)
            },
            (error) => {
                this.Alert.error(`An error occurred. Could not ${toggleMessage} ${this.allTransfers[ind]['name']}`);
                console.log("Toggle error ", error)
            })
    }

    public completeTransferApplication(ind) {
        const transferApplicationId = this.allTransfers[ind].id;
        this.schoolService.completeTransferApplication(transferApplicationId).subscribe((applicationsResponse) => {
                this.Alert.success('Application transfer completed');
                console.log('Application transfer response ', applicationsResponse);
            },
            (error) => {
                this.Alert.error('Could not complete student transfer. Please retry', error);
            })

    }

    loadTransferStatuses(){
        this.schoolService.getTransferStatuses().subscribe((statusesResponse) => {

                console.log('Application statustess response ', statusesResponse);
            },
            (error) => {
                this.Alert.error('Could not complete student transfer. Please retry', error);
            })
    }

    public iAmPermitted(routeName):boolean{
        return this.authService.iAmPermitted(routeName);
    }


}
