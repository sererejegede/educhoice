import {Component, OnInit, AfterViewInit} from '@angular/core';
import {AdministationServiceService} from '../../../../../services/api-service/administation-service.service';
import {ScriptLoaderService} from '../../../../_services/script-loader.service';
import * as Dashboard from '../../../../../assets/js/runScript';
import {Cache} from '../../../../../utils/cache';

@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.css']
})
export class DashboardHomeComponent implements OnInit, AfterViewInit {


  public dashboardData: any;
  public ratios = {
    suspended: 0,
    expelled: 0,
    graduated: 0,
    current: 0
  };
  public loader = false;

  constructor(private administrationService: AdministationServiceService,
              private scriptService: ScriptLoaderService) {
  }

  ngOnInit() {
    this.loadMyDashboard();

  }

  ngAfterViewInit() {
    // this.scriptService.load('app-dashboard-home', 'assets/js/dashboard.js');
  }


  public loadMyDashboard() {
    if (Cache.get('dashboard_data')) {
      this.dashboardData = Cache.get('dashboard_data');
      return this.doSomething();
    }
    this.loader = true;
    this.administrationService.loadMyDashboard().subscribe(
      (dashboardResponse) => {
        Cache.set('dashboard_data', dashboardResponse);
        this.dashboardData = dashboardResponse;
        this.doSomething();
        this.loader = false;
        console.log('My dashboard response ', dashboardResponse);
      },
      (error) => {
        this.loader = false;
        console.log('An error occurred loading dashboard ', error);
      }
    );
  }

  private doSomething () {
    const suspendedRatio = (this.dashboardData.students.total > 0) ? (this.dashboardData.students.suspended / this.dashboardData.students.total) * 100 : 0;
    const expelledRatio = (this.dashboardData.students.total > 0) ? (this.dashboardData.students.expelled / this.dashboardData.students.total) * 100 : 0;
    const graduatedRatio = (this.dashboardData.students.total > 0) ? (this.dashboardData.students.graduated / this.dashboardData.students.total) * 100 : 0;
    const currentRatio = (this.dashboardData.students.total > 0) ? (this.dashboardData.students.current / this.dashboardData.students.total) * 100 : 0;
    this.ratios = {
      suspended: suspendedRatio,
      expelled: expelledRatio,
      graduated: graduatedRatio,
      current: currentRatio
    };
    this.renderDashboardCharts();
  }

  public renderDashboardCharts() {
    const volumeTransactionLabels: any[] = [];
    const valueTransactionLabels: any[] = [];
    const volumeCounts: number[] = [];
    const valueCounts: number[] = [];
    const studentsOverView = {
      graduated: this.dashboardData.students.graduated,
      suspended: this.dashboardData.students.suspended,
      expelled: this.dashboardData.students.expelled,
      current: this.dashboardData.students.current
    };
    for (const key in this.dashboardData.transactions) {
      let volumeCount = 0;
      let valueCount = 0;
      const volume = this.dashboardData.transactions[key]['volume'];
      const value = this.dashboardData.transactions[key]['value'];

      const volumeLabel = `${key.split('_').join(' ')} pending (${volume.pending})
              completed (${volume.completed}) failed (${volume.failed})`;

      const valueLabel = `${key.split('_').join(' ')} pending (${value.pending})
              completed (${value.completed}) failed (${value.failed})`;

      volumeTransactionLabels.push(volumeLabel);
      valueTransactionLabels.push(valueLabel);

      volumeCount = volume.pending + volume.completed + volume.failed;
      valueCount = value.pending + value.completed + value.failed;
      volumeCounts.push(volumeCount);
      valueCounts.push(valueCount);
    }
    Dashboard.studentsOverView(studentsOverView);
    Dashboard.feeDetails('fees_by_volume', volumeTransactionLabels, volumeCounts);
    Dashboard.feeDetails('fees_by_value', valueTransactionLabels, valueCounts);
  }

  public setProgressBars(valueOrVolume) {
    const barWidth = `${valueOrVolume}%`;
    return {
      width: barWidth
    };

  }

}
