import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs/Subject";
import {DataTableDirective} from "angular-datatables";
import {NotificationService} from "../../../../../../services/notification.service";
import {SchoolService} from "../../../../../../services/school.service";
import {StaffConfigService} from "../../../../../../services/api-service/staff-config.service";
import {Subscription} from "rxjs/Subscription";

declare const $: any;

@Component({
  selector: 'app-email-sms',
  templateUrl: './email-sms.component.html',
  styleUrls: ['./email-sms.component.css']
})
export class EmailSmsComponent implements OnInit {

  dtTrigger: Subject<any> = new Subject();
  dtTrigger2: Subject<any> = new Subject();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  public tableWasOnceRendered = {
    student: false,
    staff: false
  };
  public allFaculties: any[] = [];
  public departmentList: any[] = [];
  public allStudents: any[] = [];
  public allStaff: any[] = [];
  public composeMessage: boolean = false;
  public messageType: string;
  public allDepartments: any[] = [];
  public allCoursesofStudy: any[] = [];
  public allSession: any[] = [];
  public allLevels: any[] = [];
  public ids: number[] = [];
  public smsVariables: any = [];
  public emailVariables: any = [];
  private subscription = {
    student: new Subscription
  };
  messageForm: FormGroup;
  studentFilterForm: FormGroup;
  staffFilterForm: FormGroup;
  smsForm: FormGroup;
  formType = '';
  miscellaneous = {
    unselect: false,
    was_once_checked: false,
    approval: false
  };
  load = {
    message: false,
    student: {
      get: false,
      list: false
    },
    staff: {
      get: false,
      list: false
    },
    departments: {
      list: false,
      loading: false
    },
    courses: {
      list: false,
      loading: false
    },
    session: {
      list: false,
      loading: false
    },
    level: {
      list: false,
      loading: false
    },
  };

  static messageFormGroup = function () {
    return {
      type: '',
      user_type: '',
      user_ids: '',
      sender: ['', Validators.required]
    };
  };
  static studentFilter = function () {
    return {
      faculty_id: [''],
      department_id: [''],
      course_of_study_id: [''],
      level_id: [''],
      gender: [''], // = male/female
      year: [''] // = session year
    };
  };

  static staffFilter = function () {
    return {
      type: '',
      faculty_id: '',
      department_id: '',
    };
  };

  constructor(private fb: FormBuilder,
              private schoolService: SchoolService,
              private staffConfigService: StaffConfigService,
              private el: ElementRef,
              private notification: NotificationService) {
    this.messageForm = this.fb.group(EmailSmsComponent.messageFormGroup());
    this.studentFilterForm = this.fb.group(EmailSmsComponent.studentFilter());
    this.staffFilterForm = this.fb.group(EmailSmsComponent.staffFilter());
    this.smsForm = this.fb.group({sms: ''});
  }

  ngOnInit() {
    this.getAllFaculties();
  }

  public setMessageType(value) {
    // if (value === 'both') {
    //   this.messageForm.get('message_type').setValue(['email', 'sms']);
    // }
    this.messageType = value;
  }

  public setFilterFormType(value) {
    this.formType = value;
    this.smsVariables = (this.formType === 'student') ? this.studentSmsVariables : this.staffSmsVariables;
    this.emailVariables = (this.formType === 'student') ? this.studentEmailVariables : this.staffEmailVariables;
    // this.subscription.student.unsubscribe();
  }

  /** Boilerplate from @StudentComponent*/
  public getAllFaculties() {
    this.schoolService.getAllFaculties().subscribe(
        (facultiesResponse) => {
          console.log('All faculties ', facultiesResponse);
          this.allFaculties = facultiesResponse;
        },
        (error) => {
          this.notification.error('Sorry, could not load faculties. Please reload page.', error);
        }
    );
  }

  public getAllStudents() {
    this.load.student.list = true;
    this.allStudents = [];
    this.subscription.student = this.schoolService.getAllFilteredStudents(this.studentFilterForm.value).subscribe(
        (allStudentsResponse) => {
          this.load.student.list = false;
          this.allStudents = allStudentsResponse;
          if (this.allStudents.length > 0) {
            if (this.tableWasOnceRendered.student) {
              console.log('Was table once rendered?', this.tableWasOnceRendered.student);
            } else {
              this.dtTrigger.next();
              this.tableWasOnceRendered.student = true;
            }
          }
          console.log('All Students ', this.allStudents);
        },
        (error) => {
          this.notification.error('Could not load students list');
          console.log('Could not load list of students', error);
          this.load.student.list = false;
        }
    );
  }

  public getAllStaff() {
    this.load.staff.list = true;
    this.allStaff = [];
    this.schoolService.getAllFilteredStaff(this.staffFilterForm.value)
        .subscribe(response => {
          console.log('Filtered staff', response);
          this.allStaff = response;
          this.load.staff.list = false;
          if (this.allStaff.length > 0) {
            if (this.tableWasOnceRendered.staff) {
              console.log('Was table once rendered?', this.tableWasOnceRendered);
            } else {
              this.dtTrigger2.next();
              this.tableWasOnceRendered.staff = true;
            }
          }
        }, error => {
          this.load.staff.list = false;
          this.notification.error('An error occurred, could not load staff', error);
          console.log('Filter staff error', error);
        });
  }

  public getDepartmentByFacultyId(facultyId) {
    this.load.departments.list = true;
    this.load.departments.loading = true;

    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
        (departmentResponse) => {
          this.load.departments.loading = false;

          this.allDepartments = departmentResponse.departments;
          console.log('returned departments', departmentResponse);
        },
        (error) => {
          this.load.departments.list = false;
          this.load.departments.loading = false;

          this.notification.error(`Sorry could not load departments`, error);
        }
    );
  }

  public getCourseByDepartmentId(departmentId) {
    this.load.courses.list = true;
    this.load.courses.loading = true;
    this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
        (coursesResponse) => {
          this.load.courses.loading = false;

          this.allCoursesofStudy = coursesResponse.course_of_studies;
          console.log('returned courses', coursesResponse);
        },
        (error) => {
          this.load.courses.list = false;
          this.load.courses.loading = false;
          this.notification.error(`Sorry could not load courses`, error);
        }
    );
  }

  public getSingleCourseOfStudy($event) {
    this.load.level.loading = true;
    this.load.session.loading = true;
    this.staffConfigService.getSingleCourseOfStudy($event)
        .subscribe((response) => {
              this.load.level.loading = false;
              this.load.session.loading = false;
              this.allLevels = response['degree']['programme']['levels'];
              this.allSession = response['degree']['programme']['sessions'];
              console.log('Levels', response);
            },
            (error) => {
              this.load.level.loading = false;
              this.load.session.loading = false;
              console.log('Level error', error);
              // this.Alert.error('Course of Study could not be loaded', error)
            });
  }

  /** Boilerplate from @StudentComponent*/

  resultIdCheckbox(event, dataType) {
    let data: any[] = [];
    switch (dataType) {
      case 'student':
        data = this.allStudents;
        break;
      case 'staff':
        data = this.allStaff;
        break;
    }
    if (event.target.checked) {
      data.forEach(datum => {
        datum['checked'] = true;
      });
    } else {
      data.forEach(datum => {
        datum['checked'] = false;
      });
    }
    this.composeMessage = true;
  }

  public logIds(ind, dataType) {
    let data: any[] = [];
    switch (dataType) {
      case 'student':
        data = this.allStudents;
        break;
      case 'staff':
        data = this.allStaff;
        break;
    }
    this.ids = [];
    data[ind].checked = !data[ind].checked;


  }

  public renderStudents(): void {
    if (this.tableWasOnceRendered.student) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            this.tableWasOnceRendered.student = false;
            // Call the dtTrigger to rerender again
          },

          (rejection) => {
            console.log('Render student error ', rejection)
            ;
          });
    }
    this.getAllStudents();

  }

  renderStaff() {
    if (this.tableWasOnceRendered.staff) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            this.tableWasOnceRendered.staff = false;
            // Call the dtTrigger to rerender again
          },
          (rejection) => {
            console.log('Why ah was rejected ', rejection);
          });
    }
    this.getAllStaff();
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }


  /***********EMEKA'S AREA ****************************/
  public staffEmailVariables = [
    {
      label: "Full Name",
      value: "{{fullname}}"
    },
    {
      label: "Email",
      value: "{{email}}"
    },
    {
      label: "Employee ID",
      value: "{{employee_id}}"
    }
  ];

  public studentEmailVariables = [
    {
      label: "First Name",
      value: "{{first_name}}"
    },
    {
      label: "Last Name",
      value: "{{last_name}}"
    },
    {
      label: "Matric No",
      value: "{{matric_no}}"
    }
  ];

  public staffSmsVariables = [
    {
      label: "Full Name",
      value: "{{fullname}}"
    },
    {
      label: "Email",
      value: "{{email}}"
    },
    {
      label: "Employee ID",
      value: "{{employee_id}}"
    }
  ];
  public studentSmsVariables = [
    {
      label: "Full Name",
      value: "{{fullname}}"
    },
    {
      label: "Matric No",
      value: "{{matric_no}}"
    }
  ];

  public selectedEmailVariables: any[] = [];
  public selectedSmsVariables: any[] = [];
  public smsAreaContent: string = '';
  public ckeditorContent = '';

  public saveVariables() {
    this.ckeditorContent = this.selectedEmailVariables.toString();
    if (this.selectedEmailVariables.length === 0) {
      return this.notification.error('You have not selected any Email variables!');
    }
    this.triggerModalOrOverlay('close', 'emailVar');
  }

  public selectEmailVariable(ind) {
    const emailVariable = this.emailVariables[ind].value;
    const variableIndex = this.selectedEmailVariables.indexOf(emailVariable);
    //noinspection TypeScriptUnresolvedFunction
    if (!this.selectedEmailVariables.includes(emailVariable)) {
      this.selectedEmailVariables.push(emailVariable)
    }
    else {
      this.selectedEmailVariables.splice(variableIndex, 1);

    }
  }

  public selectSmsVariable(ind) {
    const smsVariable = this.smsVariables[ind].value;
    const variableIndex = this.selectedSmsVariables.indexOf(smsVariable);
    //noinspection TypeScriptUnresolvedFunction
    if (!this.selectedSmsVariables.includes(smsVariable)) {
      this.selectedSmsVariables.push(smsVariable)
    }
    else {
      this.selectedSmsVariables.splice(variableIndex, 1);

    }
  }

  public saveSmsVariables() {
    this.smsAreaContent = this.selectedSmsVariables.toString();
    if (this.selectedSmsVariables.length === 0) {
      return this.notification.error('You have not selected any Sms variables!');
    }
    this.triggerModalOrOverlay('close', 'smsVar');
  }

  private prepareMessageObject() {
    /** Empty array of ids*/
    this.ids = [];
    const messageObject = {};
    /** Array of Ids*/
    let data: any[] = [];
    switch (this.formType) {
      case 'student':
        data = this.allStudents;
        break;
      case 'staff':
        data = this.allStaff;
        break;
    }
    data.forEach((check, j) => {
      if (check.checked) {
        this.ids.push(data[j].id);
      }
    });

    /** Message type array*/
    if (this.messageType === 'both') {
      messageObject['type'] = ['email', 'sms'];
    } else {
      messageObject['type'] = [this.messageType];
    }

    /**Others
     *
     * */
    {

    }
    messageObject['content'] = {
      text: {
        sender: this.messageForm.get('sender').value,
        message: this.smsForm.value['sms'],
      }
      ,
      email: {
        sender: this.messageForm.get('sender').value,
        subject: $('#email_subject').val(),
        // subject: this.el.nativeElement.querySelector('#email_subject').value,
        body: this.ckeditorContent
      }
    };
    messageObject['user_type'] = this.formType;
    messageObject['user_ids'] = this.ids;
    messageObject['send'] = 1;
    return messageObject;
  }

  public sendMessage() {
    // this.prepareMessageObject();
    console.log(this.prepareMessageObject());
    console.log(this.el.nativeElement.querySelector('#email_subject').value);
    this.load.message = true;
    this.schoolService.sendMessage(this.prepareMessageObject()).subscribe(
        (response) => {
          this.load.message = false;
          console.log('Message Sent', response);
          this.notification.success('Message sent successfully');
        },
        (error) => {
          this.load.message = false;
          this.notification.error(`Message not sent!`, error);
        }
    );
  }
}
