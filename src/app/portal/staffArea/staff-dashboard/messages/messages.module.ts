import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {EmailSmsComponent} from "./email-sms/email-sms.component";
import {RouterModule, Routes} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {CKEditorModule} from "ng2-ckeditor";
import {FroalaEditorModule, FroalaViewModule} from "angular2-froala-wysiwyg";

export const MESSAGES_ROUTES: Routes = [
    {
        path: '',
        component: EmailSmsComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,//EMEKAS ADDITION
        ReactiveFormsModule,
        DataTablesModule,
        CKEditorModule, //EMEKAS ADDITION
        FroalaEditorModule.forRoot(),
        FroalaViewModule,
        RouterModule.forChild(MESSAGES_ROUTES)
    ],
    declarations: [
        EmailSmsComponent
    ]
})
export class MessagesModule {
}
