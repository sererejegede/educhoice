import {Component, ElementRef, OnInit} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {Cache} from '../../../../../../utils/cache';
import {JSONPARSE} from '../../../../../../utils/json-parse';
import {Router} from '@angular/router';

declare const $;

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {
  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);


  public createForm: FormGroup;
  public id: number;
  public hostels: any;
  public hostel: any;
  public room: any;
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    allHostel: [],
    moduleName: 'Reservation',
    formType: 'Create',
    loader: false,
    submitStatus: false,
    showUpdateButton: false,
    deleteStatus: false,
    room_loader: false,
    bedSpace_loader: false
  };

  static reservation = () => {
    return {
      matric_no: ['', Validators.required],
      hostel_bed_space_id: ['', Validators.required],
    };
  };


  ngOnInit() {
    this.loadFunction();
  }

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private router: Router,
              private fb: FormBuilder) {
    this.createForm = this.fb.group(ReservationComponent.reservation());
  }

  loadFunction() {
    this.getAllReservation();
    this.getAllHostels();
  }


  private getAllHostels() {
    this.staffConfigService.getAllHostel1().subscribe(res => {
      this.hostels = res;
    }, err => {
      console.log(err);
    });
  }

  public makeReservation() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.makeReservation(this.createForm.value).subscribe(res => {
      this.feedBack.submitStatus = false;
      this.reset();
      this.notification.success('Reservation made successfully');
    }, err => {
      this.feedBack.submitStatus = false;
      this.notification.error('Could not make reservation', err);
    });
  }

  private reset () {
    this.createForm.reset();
    this.hostel = {};
    // $('#reservationModal').modal('hide');
  }

  public getHostelRooms(hostel_id) {
    this.feedBack.room_loader = true;
    this.staffConfigService.newGetHostel(hostel_id).subscribe(res => {
      this.feedBack.room_loader = false;
      // console.log(res);
      this.hostel = res;
    }, err => {
      this.feedBack.room_loader = false;
      this.notification.error('Could not load rooms', err);
      console.log(err);
    });
  }

  public getAllReservation() {
    this.feedBack.loader = true;
    this.staffConfigService.getReservations()
      .subscribe((response) => {
          this.feedBack.allResponse = response;
          if (this.feedBack.allResponse.length > 0) {
            this.dtTrigger.next();
          }
          this.feedBack.loader = false;
        },
        error => {
          this.notification.error('Unable to load Current Session, please retry', error);
          this.feedBack.loader = false;
        });
  }

  public openReservationModal() {
    $('#reservationModal').modal('show');
  }

  public chooseRoom(room_id) {
    this.hostel.rooms.forEach(room => {
      if (room.id.toString() === room_id) {
        this.room = room;
      }
    });
  }

  viewReservation(details) {
    this.feedBack.viewDetails = details;
    $('#hostelReservation').modal('show');
  }
}
