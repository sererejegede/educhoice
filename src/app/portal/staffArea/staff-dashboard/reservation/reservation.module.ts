import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservationComponent } from './reservation/reservation.component';
import {ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";
import {RouterModule, Routes} from "@angular/router";
import {StaffDashboardComponent} from "../dashboard-component.component";
import {HostelAllocationComponent} from "../hostel-allocation/hostel-allocation/hostel-allocation.component";
import {PaymentComponent} from "../payment/payment/payment.component";


const RESERVATION_ROUTE: Routes = [
  {
    path: 'hostel',
    component: StaffDashboardComponent, children: [
    {path: 'reservation', component: ReservationComponent},
    {path: 'allocation', component: HostelAllocationComponent},
    {path: 'payment', component: PaymentComponent},
  ]
  }
];

@NgModule({
  imports: [
    ReactiveFormsModule,
    DataTablesModule,
    CommonModule,
    RouterModule.forChild(RESERVATION_ROUTE)
  ],
  declarations: [ReservationComponent]
})
export class ReservationModule { }
