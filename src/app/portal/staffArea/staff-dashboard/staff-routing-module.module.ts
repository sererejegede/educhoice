import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {StaffLoginComponent} from '../staff-authentication/staff-login.component';
import {StaffDashboardComponent} from './dashboard-component.component';
import {DashboardHomeComponent} from './dashboard-home/dashboard-home.component';
import {StudentsComponent} from './students/students.component';
import {ResultsComponent} from './results/results.component';
import {ReportsComponent} from './reports/reports.component';
import {StaffComponent} from './staff/staff.component';
import {HostelsComponent} from './hostels/hostels.component';
import {FeesComponent} from './fees/fees.component';
import {CoursesComponent} from './courses/courses.component';
import {FeeItemsComponent} from './fee-items/fee-items.component';
import {StudentExceptionsComponent} from './student-exceptions/student-exceptions.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ConfigurationsModule} from './configurations/configurations.module';
import {AdministrationModule} from './administration/administration.module';
import {StaffDashboardModule} from './dashboard.module';
import {AdmissionsModule} from './admissions/admissions.module';
import {HomeComponent} from '../../../home/home.component';
import {DepartmentsComponent} from './departments/departments.component';
import {StaffConfigurationsComponent} from './configurations/configurations.component';
import {DataTablesModule} from "angular-datatables";
import {AdminHeaderNavComponent} from "../../../layouts/admin-header-nav/admin-header-nav.component";
import {ReportsModule} from "./reports/reports.module";
import {AllApplicantsComponent} from "./reports/all-applicants/all-applicants.component";
import {MessagesModule} from "./messages/messages.module";



const STAFF_ROUTES: Routes = [
  {path: '', component: StaffLoginComponent},
  {
    path: 'dashboard', component: StaffDashboardComponent,
    children: [
      {path: '', component: DashboardHomeComponent},
      {path: 'students', component: StudentsComponent},
      // {path: 'results', component: ResultsComponent},
      {path: 'reports', component: ReportsComponent},
      {path: 'staff', component: StaffComponent},
      {path: 'hostels', component: HostelsComponent},
      {path: 'fees', component: FeesComponent},
      {path: 'departments', component: DepartmentsComponent},
      {path: 'courses', component: CoursesComponent},
      {path: 'fee-items', component: FeeItemsComponent},
      {path: 'student-exceptions', component: StudentExceptionsComponent},
    ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReportsModule,
    ReactiveFormsModule,
    ConfigurationsModule,
    AdministrationModule,
    StaffDashboardModule,
    AdmissionsModule,
    DataTablesModule,
    RouterModule.forChild(STAFF_ROUTES)
  ],
  exports: [
    RouterModule,
    // StaffLoginComponent,
    // DashboardHomeComponent
  ],
  declarations: [
    // StaffLoginComponent,
    // CoursesComponent,
    // DepartmentsComponent,
    // FeesComponent,
    // FeeItemsComponent,
    // HostelsComponent,
    // ResultsComponent,
    // StudentsComponent,
    // StaffDashboardComponent,
    // StaffComponent,
    // StaffConfigurationsComponent,
    // StudentExceptionsComponent,
    // DashboardHomeComponent
  ]
})


export class StaffRoutingModule {
}
