import {Component, ElementRef, OnInit} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';
import {Faculty} from '../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../interfaces/department.interface';
import {Course} from '../../../../../interfaces/course.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../../../services/user.service';
import {SchoolService} from '../../../../../services/school.service';
import {StaffConfigService} from '../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../services/notification.service';
import * as utils from '../../../../../utils/util';
import {isNullOrUndefined} from 'util';
import {AdmissionsService} from '../../../../../services/api-service/admissions.service';

declare const $: any;

@Component({
  selector: 'app-fees',
  templateUrl: './fees.component.html',
  styleUrls: ['./fees.component.css']
})
export class FeesComponent implements OnInit {

  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtTrigger2: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);
  // public allFaculties: Array<Select2OptionData>;

  public allFeeItems: any[] = [];
  public allLevels: any[] = [];
  public yearsArray: number[] = [];
  public overlay = utils.overlay;
  public fetchingFees = false;
  public feesWhereSetBySemester = false;
  public allFees: any[] = [];
  public updatedFee: any;
  public modals = {
    modal_title: null,
    action: null,
    object: null
  };
  public updatedFeeItems: any;
  public allFaculties: Faculty[] = [];
  public allLgas: any[] = [];
  public allDepartments: Department[] = [];
  public allCourses: Course[] = [];
  public allFeesIds: any[] = [];
  public allDegrees: any[] = [];
  public allSession: any[] = [];
  public allTranscriptFees: any[] = [];
  public allAcceptanceFees: any[] = [];
  public allPartPaymentConfig: any[] = [];
  public allTransferFees: any[] = [];
  public allStates: any[] = [];
  public allCountries: any[] = [];
  public allSessions: any[] = [];
  public allProgrammes: any[] = [];
  public countryId = 0;
  public departmentId = 0;
  public facultyId = 0;
  public programmeId = 0;
  public allTestFees: any[] = [];
  public authenticatedUser: any;
  public createFeeForm: FormGroup;
  public updateFeeForm: FormGroup;
  public fetchFees: FormGroup;
  public feesForm: FormGroup;
  public partPaymentForm: FormGroup;
  public createFeeItemsForm: FormGroup;
  public updateFeeItemsForm: FormGroup;
  public editedIndex: number;
  public editedFeeItemIndex: number;
  public testFeePayment = false;
  public selectedFeeItem: any = 0;
  public showCreateFeeItem = false;
  public itemsForAssignment = {
    country: false,
    state: false,
    level: false,
    faculty: false,
    department: false,
    course: false,
    gender: false
  };
  public assignmentKey = '';
  public load = {
    requesting: {
      list: false,
      create: false,
      listItems: false
    },
    fee: {
      transcript: false
    },
    programme: false,
    message: {
      create: 'Create',
      update: 'Update'
    },
    departments: {
      list: false,
      loading: false
    },
    session: {
      list: false,
      loading: false
    },
    states: {
      loading: false
    },
    lgas: {
      loading: false
    },
    courses: {
      list: false,
      loading: false
    },
    fees: {
      list: false,
      loading: false,
    },
    degree: {
      list: false
    },
    part_payment: {
      list: false,
      create: false,
      update: false
    }
  };
  public newName: string;

  static createFeeForm = function () {
    return {
      amount: ['', Validators.compose([Validators.required])],
      fee_item_id: ['', Validators.compose([Validators.required])],
      session_year: ['', Validators.compose([Validators.required])],
      country_id: '',
      state_id: '',
      level_id: '',
      department_id: '',
      faculty_id: '',
      gender: '',
      course_of_study_id: '',
      programme_id: '',
      semester: '',
      mode_of_entry: '',
      student_type: '',
      status: '1',
      is_penalty: ''
    };
  };

  static partPaymentForm = function () {
    return {
      amount: [''],
      type: ['', Validators.compose([Validators.required])],
      value: ['', Validators.compose([Validators.required])],
      session_year: ['', Validators.compose([Validators.required])],
      country_id: '',
      state_id: '',
      level_id: '',
      department_id: '',
      // programme_type_id: '',
      student_id: '',
      faculty_id: '',
      gender: '',
      course_of_study_id: '',
      programme_id: '',
      semester: '',
      mode_of_entry: '',
      student_type: '',
      degree_id: '',
      lga_id: ''
    };
  };

  static fetchFees = function () {
    return {
      country_id: '',
      state_id: '',
      level_id: '',
      department_id: '',
      faculty_id: '',
      gender: '',
      course_of_study_id: '',
      semester: '',
      mode_of_entry: '',
      student_type: '',
      programme_id: '',
      session_year: '',
      status: '',
      is_penalty: ''
    };
  };

  static updateFeeForm = function () {
    return {
      amount: ['', Validators.compose([Validators.required])],
      fee_item_id: ['', Validators.compose([Validators.required])],
      session_year: ['', Validators.compose([Validators.required])],
      country_id: '',
      state_id: '',
      level_id: '',
      department_id: '',
      faculty_id: '',
      gender: '',
      course_of_study_id: '',
      programme_id: '',
      semester: '',
      mode_of_entry: '',
      student_type: '',
      status: '1',
      is_penalty: '0'
    };
  };

  static createFeeItemsForm = function () {
    return {
      name: ['', Validators.compose([Validators.required])],
    };
  };

  static updateFeeItemsForm = function () {
    return {
      name: ['', Validators.compose([Validators.required])],
    };
  };

  static feesForm = function () {
    return {
      faculty_id: [''],
      programme_id: [1],
      course_of_study_id: [''],
      department_id: ['', Validators.compose([Validators.required])],
      session_id: ['', Validators.compose([Validators.required])],
      amount: ['', Validators.compose([Validators.required])]
    };
  };

  constructor(private userService: UserService,
              private schoolService: SchoolService,
              private admissionsService: AdmissionsService,
              private fb: FormBuilder,
              private staffConfigService: StaffConfigService,
              private Alert: NotificationService) {
    // this.authenticatedUser = this.userService.getAuthUser().login.user;
    this.createFeeForm = this.fb.group(FeesComponent.createFeeForm());
    this.updateFeeForm = this.fb.group(FeesComponent.updateFeeForm());
    this.createFeeItemsForm = this.fb.group(FeesComponent.createFeeItemsForm());
    this.updateFeeItemsForm = this.fb.group(FeesComponent.updateFeeItemsForm());
    this.feesForm = this.fb.group(FeesComponent.feesForm());
    this.fetchFees = this.fb.group(FeesComponent.fetchFees());
    this.partPaymentForm = this.fb.group(FeesComponent.partPaymentForm());
    // console.log('Authenticated user ', this.authenticatedUser);
  }

  ngOnInit() {
    this.getAllFeeItems();
    this.getAllFaculties();
    this.getAllCourses();
    this.getAllCountries();
    this.getAllProgrammes();
    this.getAllLevels();
    this.getTranscriptFees();
    this.getTransferFees();
    this.getAcceptanceFees();
    this.getAllDegrees();
    this.getSessionsByProgrammeId();
  }

  public getAllFees() {
    this.fetchingFees = true;
    this.load.requesting.list = true;
    this.schoolService.getAllFees().subscribe(
      (allFeesResponse) => {
        this.load.requesting.list = false;
        this.fetchingFees = false;
        if (allFeesResponse.data) {
          this.allFees = allFeesResponse.data;
        } else {
          this.allFees = allFeesResponse;
        }
        this.allFees.forEach((fee) => {
          this.allFeesIds.push(fee.id);
          this.allFeeItems.forEach((feeItem) => {
            const feeItemId = parseInt(feeItem.id);
            const feeFeeItemId = parseInt(fee.fee_item_id);
            if (feeItemId === feeFeeItemId) {
              fee.fee_item_name = feeItem.name;
            }
          });
        });

        if (this.allFees.length > 0) {
          this.dtTrigger.next();
        }
        // console.log('All fees response', allFeesResponse);
      },
      (error) => {
        this.fetchingFees = false;
        this.load.requesting.list = false;
      }
    );
  }


  public getAllFeeItems() {
    this.load.requesting.listItems = true;
    this.schoolService.getAllFeeItems().subscribe(
      (allFeeItemsResponse) => {
        this.load.requesting.list = false;
        this.load.requesting.listItems = false;
        this.allFeeItems = allFeeItemsResponse.data;

        // console.log('All fee items response', allFeeItemsResponse);
      },
      (error) => {
        this.load.requesting.list = false;
        this.load.requesting.listItems = false;

      }
    );
    // if (this.allFeeItems.length > 0) {
    //     this.dtTrigger.next();
    // }
  }

  public async createFee(testFeePayment = false) {
    this.load.requesting.create = true;
    this.fetchingFees = true;
    this.load.message.create = 'Creating...';
    const formFeeObject = (testFeePayment) ? this.deepCopy(this.fetchFees.value) : this.deepCopy(this.createFeeForm.value);
    // console.log('Fee object ', formFeeObject);
    formFeeObject['is_penalty'] = (formFeeObject['is_penalty']) ? '1' : '0';
    await this.schoolService.createOrUpdateFee(formFeeObject, testFeePayment).subscribe(
      (createdFeeResponse) => {
        this.load.message.create = 'Create';
        this.fetchingFees = false;
        this.load.requesting.create = false;
        const allTestFees = [];
        let keysInResponse = 0;
        if (testFeePayment) {
          this.testFeePayment = true;
          createdFeeResponse.forEach(response => {
            keysInResponse += 1;
            for (const key in response) {
              allTestFees.push(response[key]);
            }
          });
          this.allTestFees = allTestFees;
          if (keysInResponse > 1) {
            this.feesWhereSetBySemester = true;
          }
          // console.log('Test fees response ', createdFeeResponse)

          return;
        }
        this.countryId = this.facultyId = this.programmeId = this.departmentId = 0;
        this.allFees.unshift(createdFeeResponse);
        // this.Alert.success(`A fee of ${this.createFeeForm.value.amount} has been Sent for Authorization`);
        this.Alert.success(`Fee of ${this.createFeeForm.value.amount} created successfully`);
        this.createFeeForm = this.fb.group(FeesComponent.createFeeForm());
        this.createFeeForm.reset();
        this.triggerModalOrOverlay('close', 'createFee');
        // createdFeeResponse['fee_item'] = this.selectedFeeItem['name'];
        createdFeeResponse['status'] = '0';

        // this.allFees.push(createdFeeResponse);
        // console.log('Newly created fee ', createdFeeResponse);
      },
      (error) => {
        this.load.message.create = 'Create';
        this.fetchingFees = false;
        // this.triggerModal('close','createSchool');
        // console.log('Eroorroroor ', error);
        this.load.requesting.create = false;
        this.Alert.error(`Could not create ${this.createFeeForm.value.name}`, error);
      }
    );
  }

  public testFee() {
    this.fetchingFees = true;
    const createFeeFormObject = Object.keys(this.fetchFees.value);
    const numberofKeys = createFeeFormObject.length;
    let emptyFields = 0;
    for (const key in this.fetchFees.value) {
      if (!this.fetchFees.value[key]) {
        emptyFields += 1;
      }
    }
    return (emptyFields === numberofKeys) ? this.getAllFees() : this.createFee(true);
  }

  public selectFeeItem(id) {
    // alert(id);
    const feeItemIds = [];
    this.allFeeItems.forEach((feeItem) => {
      const feeItemId = parseInt(feeItem.id);
      if (feeItemId === parseInt(id)) {
        feeItemIds.push(feeItemId);
      }
    });
    this.selectedFeeItem = feeItemIds[0];
  }

  public getLgaByStateId(stateId) {
    this.load.lgas.loading = true;
    const dataItem = this.selectWhereId(this.allStates, 'id', stateId);
    // console.log("Select state where id is ", dataItem);
    this.admissionsService.getLgaByStateId(stateId).subscribe(
      (lgasResponse) => {
        this.load.lgas.loading = false;

        this.allLgas = lgasResponse.lgas;
        // console.log("returned lgas", lgasResponse);
      },
      (error) => {
        this.load.lgas.loading = false;
        this.Alert.error(`Sorry could not load Lgas for`, error);
        // ${this.selectWhereId(this.allStates, stateId).name}
      }
    );
  }

  public updateFee() {
    this.load.requesting.create = true;
    this.load.message.update = 'Updating...';

    this.schoolService.createOrUpdateFee(this.updateFeeForm.value, this.updatedFee.id).subscribe(
      (updatedFeeResponse) => {
        this.load.message.update = 'Update';
        this.updateFeeForm = this.fb.group(FeesComponent.updateFeeForm());
        this.Alert.success(`Fee amount for ${this.updatedFee.fee_item_name} updated as ${updatedFeeResponse.amount} naira\n`);
        this.allFeeItems.forEach((feeItem) => {
          if (parseInt(updatedFeeResponse.fee_item_id) === parseInt(feeItem.id)) {
            updatedFeeResponse.fee_item_name = feeItem.name;
          }
        });
        this.allFees[this.editedIndex] = updatedFeeResponse;
        this.load.requesting.create = false;
        this.triggerModalOrOverlay('close', 'updateFee');

        // console.log('Updated fee item ', updatedFeeResponse);
      },
      (error) => {
        this.load.message.update = 'Update';
        this.load.requesting.create = false;
        this.Alert.error(`Could not update ${this.updateFeeForm.value.first_name}`, error);
      }
    );
  }

  public createFeeItems() {
    this.load.requesting.create = true;
    this.load.message.create = 'Creating...';
    this.schoolService.createOrUpdateFeeItems(this.createFeeItemsForm.value).subscribe(
      (createdFeeItemsResponse) => {
        this.load.message.create = 'Create';
        this.Alert.success(`${this.createFeeItemsForm.value.name} created successfully\n`);
        this.createFeeItemsForm = this.fb.group(FeesComponent.createFeeItemsForm());
        this.load.requesting.create = false;
        this.showCreateFeeItem = false;
        this.allFeeItems.unshift(createdFeeItemsResponse.name);
        // console.log('Newly created fee item ', createdFeeItemsResponse);
      },
      (error) => {
        this.load.message.create = 'Create';
        // this.triggerModal('close','createSchool');
        // console.log('Eroorroroor ', error);
        this.load.requesting.create = false;
        this.Alert.error(`An error occurred. Could not create ${this.createFeeItemsForm.value.name}`);
      }
    );
  }

  public updateFeeItems(feeItem) {
    this.load.requesting.create = true;
    this.load.message.update = 'Updating...';
    // console.log(this.updateFeeItemsForm.value);
    this.schoolService.createOrUpdateFeeItems(this.updateFeeItemsForm.value, feeItem.id)
      .subscribe(
        (updatedFeeItemsResponse) => {
          this.load.message.update = 'Update';
          this.allFeeItems[this.allFeeItems.indexOf(feeItem)] = updatedFeeItemsResponse;
          this.load.requesting.create = false;
          feeItem['update'] = false;
        },
        (error) => {
          this.load.message.update = 'Update';
          this.load.requesting.create = false;
          feeItem['update'] = false;
          // console.log(error);
        }
      );
  }

  private getTranscriptFees() {
    this.allTranscriptFees = [];
    this.schoolService.getTranscriptFee()
      .subscribe((response) => {
        this.allTranscriptFees = response;
        // console.log(response);
      }, (error) => {
        this.Alert.error('Could not load Transcript Fee', error);
      });
  }

  private getTransferFees() {
    this.allTransferFees = [];
    this.schoolService.getTransferFee().subscribe((response) => {
        this.allTransferFees = response;
        // console.log(response);
      }, (error) => {
        this.Alert.error('Could not load Transcript Fee', error);
      });
  }

  public getAcceptanceFees () {
    this.load.requesting.list = true;
    this.admissionsService.getAcceptanceFee().subscribe((res) => {
        this.load.requesting.list = false;
        this.allAcceptanceFees = res.data;
      }, (error) => {
        this.load.requesting.list = false;
        this.Alert.error('An error occurred.', error);
      });
  }

  public feesCreateOrUpdate() {
    this.load.fee.transcript = true;
    // console.log(this.feesForm.value);
    switch (this.modals.action) {
      case 'create_transcript':
        this.schoolService.createTranscriptFee(this.feesForm.value).subscribe((response) => {
            this.load.fee.transcript = false;
            this.Alert.success(`Successful!`);
            this.allTranscriptFees.unshift(response);
            this.triggerModalOrOverlay('close', 'feeCRUD');
            // console.log(response);
          }, (error) => {
            this.load.fee.transcript = false;
            // console.log('', error);
            this.Alert.error(`Could not create. Please retry`, error);
            this.triggerModalOrOverlay('close', 'feeCRUD');
          });
        break;
      case 'update_transcript':
        this.schoolService.updateTranscriptFee(this.feesForm.value, this.modals.object['id']).subscribe((response) => {
              this.load.fee.transcript = false;
              this.Alert.success(`Successful!`);
              this.allTranscriptFees.splice(this.allTranscriptFees.indexOf(this.modals.object), 1, response);
              this.triggerModalOrOverlay('close', 'feeCRUD');
              // console.log(response);
            }, (error) => {
              this.load.fee.transcript = false;
              // console.log('', error);
              this.Alert.error(`Could not create. Please retry`, error);
              this.triggerModalOrOverlay('close', 'feeCRUD');
            });
        break;
      case 'create_transfer':
        this.schoolService.createTransferFee(this.feesForm.value).subscribe((response) => {
              this.load.fee.transcript = false;
              this.Alert.success(`Successful!`);
            this.allTransferFees.unshift(response);
              this.triggerModalOrOverlay('close', 'feeCRUD');
              // console.log(response);
            }, (error) => {
              this.load.fee.transcript = false;
              // console.log('', error);
              this.Alert.error(`Could not create. Please retry`, error);
              this.triggerModalOrOverlay('close', 'feeCRUD');
            });
        break;
      case 'update_transfer':
        this.schoolService.updateTransferFee(this.feesForm.value, this.modals.object['id']).subscribe((response) => {
              this.load.fee.transcript = false;
              this.Alert.success(`Successful!`);
            this.allTransferFees.splice(this.allTransferFees.indexOf(this.modals.object), 1, response);
              this.triggerModalOrOverlay('close', 'feeCRUD');
              // console.log(response);
            }, (error) => {
              this.load.fee.transcript = false;
              // console.log('', error);
              this.Alert.error(`Could not create. Please retry`, error);
              this.triggerModalOrOverlay('close', 'feeCRUD');
            });
        break;
      case 'create_acceptance':
        this.admissionsService.createOrUpdateAcceptanceFee(this.feesForm.value).subscribe((response) => {
          this.load.fee.transcript = false;
          this.Alert.success(`Successful!`);
          this.allAcceptanceFees.unshift(response);
          this.triggerModalOrOverlay('close', 'feeCRUD');
          // console.log(response);
        }, (error) => {
          this.load.fee.transcript = false;
          // console.log('', error);
          this.Alert.error(`Could not create. Please retry`, error);
          this.triggerModalOrOverlay('close', 'feeCRUD');
        });
        break;
      case 'update_acceptance':
        this.admissionsService.createOrUpdateAcceptanceFee(this.feesForm.value, this.modals.object['id']).subscribe((response) => {
          this.load.fee.transcript = false;
          this.Alert.success(`Successful!`);
          this.allAcceptanceFees.splice(this.allAcceptanceFees.indexOf(this.modals.object), 1, response);
          this.triggerModalOrOverlay('close', 'feeCRUD');
          // console.log(response);
        }, (error) => {
          this.load.fee.transcript = false;
          // console.log('', error);
          this.Alert.error(`Could not create. Please retry`, error);
          this.triggerModalOrOverlay('close', 'feeCRUD');
        });
        break;
      default:
        this.load.fee.transcript = false;
    }
  }

  public createPartPaymentFee() {
    this.load.part_payment.create = true;
    this.schoolService.createPartPaymentFee(this.partPaymentForm.value)
      .subscribe(
        (response) => {
          this.load.part_payment.create = false;
          this.Alert.success(`Successful!`);
          this.triggerModalOrOverlay('close', 'part_payment_modal');
          // console.log(response);
          this.allPartPaymentConfig.unshift(response);
        },
        (error) => {
          this.load.part_payment.create = false;
          // console.log('', error);
          this.Alert.error(`Could not create. Please retry`, error);
          this.triggerModalOrOverlay('close', 'part_payment_modal');
        }
      );
  }

  public updatePartPaymentFee() {
    this.load.part_payment.create = true;
    // console.log(this.partPaymentForm.value);
    this.schoolService.updatePartPaymentFee(this.modals['part_payment_id'], this.partPaymentForm.value)
      .subscribe(
        (response) => {
          this.load.part_payment.create = false;
          this.Alert.success(`Successful!`);
          this.triggerModalOrOverlay('close', 'part_payment_modal');
          // console.log(response);
          this.modals.action = null;
          this.allPartPaymentConfig.splice(this.modals['index'], 1, response);
        },
        (error) => {
          this.load.part_payment.create = false;
          // console.log('', error);
          this.Alert.error(`Could not update. Please retry`, error);
          this.triggerModalOrOverlay('close', 'part_payment_modal');
        }
      );
  }

  public getFilteredPartPayment() {
    this.load.part_payment.list = true;
    this.schoolService.getFilteredPartPayments(this.partPaymentForm.value)
      .subscribe(
        (response) => {
          this.allPartPaymentConfig = response;
          this.load.part_payment.list = false;
          // console.log(response);
        },
        (error) => {
          this.load.part_payment.list = false;
          // console.log('Get All Part Payments', error);
          this.Alert.error(`Could not load Part payments. Please retry`, error);
        }
      );
  }

  public getAllFaculties() {
    this.schoolService.getAllFaculties().subscribe(
      (facultiesResponse) => {
        // console.log('All faculties ', facultiesResponse);
        this.allFaculties = facultiesResponse;
      },
      (error) => {
        this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
      }
    );
  }

  /**
   * This method returns a single faculty which contains a list of departments
   */
  public getDepartmentByFacultyId(facultyId) {
    this.facultyId = facultyId;
    this.load.departments.list = true;
    this.load.departments.loading = true;
    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
      (departmentResponse) => {
        this.load.departments.loading = false;
        this.allDepartments = departmentResponse.departments;
        // console.log('returned departments', departmentResponse);
      },
      (error) => {
        this.load.departments.list = false;
        this.load.departments.loading = false;

        this.Alert.error(`Sorry could not load departments`, error);
      }
    );
  }


  public getCourseByDepartmentId(departmentId) {
    this.departmentId = departmentId;
    this.load.courses.list = true;
    this.load.courses.loading = true;
    this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
      (coursesResponse) => {
        this.load.courses.loading = false;

        this.allCourses = coursesResponse.course_of_studies;
        // console.log('returned courses', coursesResponse);
      },
      (error) => {
        this.load.courses.list = false;
        this.load.courses.loading = false;
        this.Alert.error(`Sorry could not load courses`, error);
      }
    );
  }


  public async getAllCourses() {
    await this.schoolService.getAllCourses().subscribe(
      (allCoursesResponse) => {
        this.allCourses = allCoursesResponse.data;
        // this.updatedFee.loading = false;
      },
      (error) => {
        // this.updatedFee.loading = false;
        this.Alert.error('Sorry, could not load school courses', error);
      });
  }

  public getAllCountries() {
    this.schoolService.getAllCountries().subscribe(
      (countriesResponse) => {
        this.allCountries = countriesResponse;
        // console.log("returned countries", countriesResponse);
      },
      (error) => {
        this.Alert.error(`Sorry could not load countries`, error);
      }
    );
  }

  public getStateByCountryId(countryId) {
    this.countryId = countryId;
    this.load.states.loading = true;
    this.schoolService.getStateByCountryId(countryId).subscribe(
      (statesResponse) => {
        this.load.states.loading = false;
        this.allStates = statesResponse.states;
        // console.log('returned States', statesResponse);
      },
      (error) => {
        this.load.states.loading = false;
        this.Alert.error(`Sorry could not load States`, error);
      }
    );
  }

  public getAllLevels() {
    this.staffConfigService.getAllLevel().subscribe(
      (levelsResponse) => {
        this.allLevels = levelsResponse.data;
        // console.log(levelsResponse);
      },
      (error) => {
        // console.log('Could not load levels');
      }
    );
  }

  public getAllProgrammes() {
    this.load.programme = true;
    this.staffConfigService.getAllProgramme().subscribe((programmeResponse) => {
        this.load.programme = false;
        this.allProgrammes = programmeResponse.data;
      },
      (error) => {
        this.load.programme = false;
        this.Alert.error(`Sorry could not load Programmes`, error);
      });
  }

  public getProgrammeById(programmeId) {
    this.load.session.loading = true;
    this.programmeId = programmeId;
    this.staffConfigService.getProgramme(this.programmeId)
      .subscribe((response) => {
          this.load.session.loading = false;
          this.allLevels = response.levels
          // this.viewedProgramme = response; // Cache.get('faculty.' + this.id);
          // console.log('Viewed programme response ', response);
        },
        error => {
          this.load.session.loading = false;
          this.Alert.error('Could not load Programme', error);

        });
  }

  /**
   *
   * @param $event
   */
  getSingleCourseOfStudy($event) {
    this.load.session.loading = true;
    this.staffConfigService.getSingleCourseOfStudy($event)
      .subscribe((response) => {
          this.load.session.loading = false;
          this.allSession = response['degree']['programme']['sessions'];
          // console.log('Levels', response);
        },
        (error) => {
          this.load.session.loading = false;
          // console.log('Level error', error);
          this.Alert.error('Course of Study could not be loaded', error);
        });
  }

  public getAllSessions() {
    this.staffConfigService.getAllSession().subscribe(
      (allSessionsResponse) => {
        this.allSessions = allSessionsResponse.data;
        // console.log('All sessions response ', allSessionsResponse);
      },
      (error) => {
        // console.log('Could not load sessions');
      }
    );
  }

  public logStaffIn() {
    this.schoolService.logStaffIn().subscribe(
      (loginResponse) => {
        this.userService.setAuthUser(loginResponse.token);

      }
    );
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number, type?: string) {
    this.load.message.create = 'Create';
    if (ind >= 0) {
      this.allFees[ind].loading = true;
      this.editedFeeItemIndex = ind;
      this.editedIndex = ind;
      this.updatedFee = this.allFees[ind];
      // console.log('Updated fee in trigger modal ', this.updatedFee);
      this.updateFeeForm.patchValue(this.updatedFee);
      // console.log('Fee management object ', this.updatedFee);
      this.updatedFee.loading = false;

    }
    if (type) {
      (action === 'open') ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
      });
      return;
    }
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');

  }

  public openTestFeeModal(index) {
    this.updatedFee = this.allTestFees[index];
    this.updateFeeForm.patchValue(this.updatedFee);
    this.triggerModalOrOverlay('open', 'updateFee');
  }

  public openCreateFeeModal(index?) {
    this.testFeePayment = false;
    this.triggerModalOrOverlay('open', 'createFee');
  }

  public toggleFeeItem() {
    this.showCreateFeeItem = !this.showCreateFeeItem;
  }

  public setAssignmentItem(key) {
    this.itemsForAssignment[this.assignmentKey] = false;
    this.itemsForAssignment[key] = true;
    this.assignmentKey = key;
  }


  public getSessionsByProgrammeId (programmeId = 1) {
    this.load.session.loading = true;
    this.schoolService.getSessionsByProgrammeId(programmeId).subscribe(
      (sessionsResponse) => {
        this.load.session.loading = false;
        this.allSessions = sessionsResponse.data;
        // console.log('All sessions by programme Id ', sessionsResponse);
      },
      (error) => {
        this.load.session.loading = false;
        // console.log('An eror occured getting session ', error);
      }
    );
  }

  public refreshModal() {
    this.partPaymentForm.reset();
    this.modals.action = null;
    this.triggerModalOrOverlay('open', 'part_payment_modal');
  }

  public feeModals(type, action, updateObject?) {
    this.feesForm.reset({programme_id: 1});
    if (updateObject) {
      this.modals.object = updateObject;
      // console.log(updateObject);
    }
    if (type === 'transcript') {
      if (action === 'create') {
        this.modals.modal_title = 'Create Transcript Fee';
        this.modals.action = 'create_transcript';
        this.triggerModalOrOverlay('open', 'feeCRUD');
      } else if (action === 'update') {
        this.modals.modal_title = 'Update Transcript Fee';
        this.modals.action = 'update_transcript';
        // this.feesForm = this.fb.group(updateObject);
        this.feesForm.patchValue(updateObject);
        this.modals.object['id'] = updateObject.id;
        this.triggerModalOrOverlay('open', 'feeCRUD');
      }
    } else if (type === 'transfer') {
      if (action === 'create') {
        this.modals.modal_title = 'Create Transfer Fee';
        this.modals.action = 'create_transfer';
        this.triggerModalOrOverlay('open', 'feeCRUD');
      } else if (action === 'update') {
        this.modals.modal_title = 'Update Transfer Fee';
        this.modals.action = 'update_transfer';
        // this.feesForm = this.fb.group(updateObject);
        this.feesForm.patchValue(updateObject);
        this.modals.object['id'] = updateObject.id;
        this.triggerModalOrOverlay('open', 'feeCRUD');
      }
    } else if (type === 'acceptance') {
      if (action === 'create') {
        this.modals.modal_title = 'Create Acceptance Fee';
        this.modals.action = 'create_acceptance';
        this.triggerModalOrOverlay('open', 'feeCRUD');
      } else if (action === 'update') {
        this.modals.modal_title = 'Update Acceptance Fee';
        this.modals.action = 'update_acceptance';
        // this.feesForm = this.fb.group(updateObject);
        this.feesForm.patchValue(updateObject);
        this.modals.object['id'] = updateObject.id;
        this.triggerModalOrOverlay('open', 'feeCRUD');
      }
    } else if (type === 'part_payment') {
      this.modals.action = 'update_part_payment';
      this.modals['index'] = this.allPartPaymentConfig.indexOf(updateObject);
      this.partPaymentForm.patchValue(updateObject);
      this.triggerModalOrOverlay('open', 'part_payment_modal');
    }
  }

  public toggleUpdate(feeItem) {
    if (!isNullOrUndefined(this.editedFeeItemIndex)) {
      this.allFeeItems[this.editedFeeItemIndex]['update'] = false;
    }
    feeItem['update'] = true;
    this.updateFeeItemsForm.patchValue(feeItem);
    this.editedFeeItemIndex = this.allFeeItems.indexOf(feeItem);
  }


  public selectWhereId(data: any[], search_key: string, id) {
    const dataItem: any[] = [];
    data.forEach(item => {
      const itemKey = parseInt(item[search_key]);
      const itemId = parseInt(id);
      if (itemKey === itemId) {
        dataItem.push(item);
      }
    });
    return dataItem[0];
  }


  public getAllDegrees() {
    this.load.degree.list = true;
    this.staffConfigService.getAllDegree()
      .subscribe((response) => {
          this.allDegrees = response.data;
          this.load.degree.list = false;
        },
        error => {
          this.Alert.error('Could not load degrees ', error);
          this.load.degree.list = false;

        });
  }

  public deepCopy(content: any): any {
    return JSON.parse(JSON.stringify(content));
  }
}
