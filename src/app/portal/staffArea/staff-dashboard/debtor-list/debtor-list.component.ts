import {Component, OnInit, ViewChild} from '@angular/core';
import {NotificationService} from "../../../../../services/notification.service";
import {StaffConfigService} from "../../../../../services/api-service/staff-config.service";
import {Cache} from "../../../../../utils/cache";
import {CONSTANTS} from "../../../../../utils/constants";
import {Subject} from "../../../../../../node_modules/rxjs";
import {DataTableDirective} from "angular-datatables";

@Component({
  selector: 'app-debtor-list',
  templateUrl: './debtor-list.component.html',
  styleUrls: ['./debtor-list.component.css']
})
export class DebtorListComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  public dtTrigger: Subject<any> = new Subject();
  public appSettings: any;
  public loader = {
    uploading: false,
    downloading: false,
    loading: false,
  };
  public debtorsList: any[] = [];

  constructor(private notification: NotificationService,
              private staffConfigService: StaffConfigService) { }

  ngOnInit() {
    this.appSettings = Cache.get('app_settings') || Cache.get('ping');
    this.getAllDebtors();
  }

  private getAllDebtors() {
    this.loader.loading = true;
    this.staffConfigService.getAllDebtors().subscribe((res) => {
      this.loader.loading = false;
      this.debtorsList = res;
      if (this.debtorsList.length) {
        this.dtTrigger.next();
      }
    }, (err) => {
      this.loader.loading = false;
      this.notification.error(CONSTANTS.DEFAULT_ERROR_MESSAGE, err);
    });
  }

  public downloadDebtorsListTemplate(buttonId: string) {
    this.loader.downloading = true;
    // this.selectedCurriculumCourseId = this.feedBack.allResponse['curriculum_courses'][ind].id;
    this.staffConfigService.downloadDebtorsListTemplate().subscribe((url) => {
          this.loader.downloading = false;
          // const templateUrl = url;
          const downloadButton = $(`#${buttonId}`);
          downloadButton.attr('href', url);
          downloadButton[0].click();
          // console.log('Download button', downloadButton);
        },
        (error) => {
          this.loader.downloading = false;
          this.notification.error('An error occured, could not download template', error);
        });
  }

  public uploadDebtorsList(fileId) {
    this.loader.uploading = true;
    const bulkFileId = `#${fileId}`;
    const formFile = document.getElementById(fileId);
    console.info('Form File ', formFile);
    const $linkId = $(bulkFileId);
    const $newLinkId = $linkId.val();
    const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
    $linkId.removeClass('alert alert-danger animated rubberBand');
    if (file_type && (file_type !== '.xlsx')) {
      $linkId.addClass('alert alert-danger animated rubberBand');
      this.loader.uploading = false;
      return this.notification.error('You can only upload xlsx files');
    }
    this.staffConfigService.uploadDebtorsList(formFile).subscribe((res) => {
          this.loader.uploading = false;
          this.reRender();
          this.notification.success(res.message || 'Success.');
        },
        (error) => {
          this.loader.uploading = false;
          this.notification.error('An error occurred uploading file', error);
        });
  }

  public removeDebtor(debtor, index) {
    this.debtorsList[index]['deleting'] = true;
    this.staffConfigService.removeDebtor(debtor.id).subscribe((res) => {
      this.debtorsList[index]['deleting'] = false;
      this.notification.success(res.message || CONSTANTS.DEFAULT_SUCCESS_MESSAGE);
      this.debtorsList.splice(index, 1);
    }, (err) => {
      this.debtorsList[index]['deleting'] = false;
      this.notification.error(CONSTANTS.DEFAULT_ERROR_MESSAGE, err);
    });
  }

  private reRender(): void {
    this.debtorsList = [];
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      // this.dtTrigger.next();
      this.getAllDebtors();
    });
  }
}
