import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AdministationServiceService} from "../../../../../../../services/api-service/administation-service.service";
import {NotificationService} from "../../../../../../../services/notification.service";

@Component({
    selector: 'app-group-view',
    templateUrl: './group-view.component.html',
    styleUrls: ['./group-view.component.css']
})
export class GroupViewComponent implements OnInit {
    private groupId: number;
    public viewedGroup: any = null;

    constructor(private activeRoute: ActivatedRoute,
                private adminService: AdministationServiceService,
                private Alert: NotificationService) {

        this.activeRoute.params.subscribe(params => {
            this.groupId = +params['id'];
            this.getGroupById();
        })
    }

    ngOnInit() {

    }

    private getGroupById() {
        this.adminService.getGroupById(this.groupId).subscribe(
            (groupResponse) => {
                this.viewedGroup = groupResponse;
                console.log('Viewed group',this.viewedGroup);
            },
            (error) => {
                this.Alert.error('Could not load selected group', error);
            }
        )
    }

}
