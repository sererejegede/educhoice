import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import * as utils from '../../../../../../utils/util';
import {Faculty} from '../../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../../interfaces/department.interface';
import {Course} from '../../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../../services/school.service';
import {UserService} from '../../../../../../services/user.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {AdministationServiceService} from '../../../../../../services/api-service/administation-service.service';
import {IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts} from 'angular-2-dropdown-multiselect';
import {AuthenticationService} from '../../../../../../services/authentication.service';
import {Cache} from '../../../../../../utils/cache';

declare const $: any;

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit, AfterViewInit {

  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);


  public overlay = utils.overlay;
  public allGroups: any[] = [];
  public allTasks: any[] = [];
  public updatedGroup: any;
  public viewedGroup: any;
  public allActiveHours: any[] = [];
  public allFaculties: Faculty[] = [];
  public allDepartments: Department[] = [];
  public selectedUpdateGroup: any[] = [];
  public allCourses: Course[] = [];
  public allStates: any[] = [];
  public allCountries: any[] = [];
  public authenticatedUser: any;
  public myGroups: any[] = [];
  public myEditedGroup: any = {};
  public createGroupForm: FormGroup;
  public updateGroupForm: FormGroup;
  public editedIndex: number;
  public load = {
    requesting: {
      list: true,
      create: false
    },
    message: {
      create: 'Create',
      update: 'Update'
    },
    departments: {
      list: false,
      loading: false
    },
    courses: {
      list: false,
      loading: false
    },
    Groups: {
      list: false,
      loading: false
    }

  };
  public permissions = {
    id: [
      'school.administration.group.store',
    ],
    class: [
      'school.administration.group.update'
    ]
  };

  public allPermissions: IMultiSelectOption[];
// Settings configuration
  mySettings: IMultiSelectSettings = {
    enableSearch: true,
    checkedStyle: 'fontawesome',
    buttonClasses: 'btn btn-default btn-block',
    dynamicTitleMaxItems: 2,
    displayAllSelectedText: true
  };

// Text configuration
  myTexts: IMultiSelectTexts = {
    checkAll: 'Select all',
    uncheckAll: 'Unselect all',
    checked: 'item selected',
    checkedPlural: 'items selected',
    searchPlaceholder: 'Find',
    searchEmptyResult: 'Nothing found...',
    searchNoRenderText: 'Type in search box to see results...',
    defaultTitle: 'Select',
    allSelected: 'All selected',
  };


  // public mySettings = {
  //     text: 'Select Task(s)',
  //     selectAllText: 'Select All',
  //     unSelectAllText: 'UnSelect All',
  //     classes: 'myclass custom-class',
  //     searchPlaceholderText: 'Filter Tasks',
  //     enableSearchFilter: true,
  //     badgeShowLimit: 3
  // };

  public newSettings = {
    text: 'Select Task(s)',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    classes: 'myclass custom-class',
    searchPlaceholderText: 'Filter Tasks',
    enableSearchFilter: true,
    badgeShowLimit: 2
  };


  static createGroupForm = function () {
    return {
      holiday_login: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      weekend_login: '',
      tasks: '',
      active_hour_id: ''

    };
    // email,first_name,last_name,course_of_study_id
  };


  static updateGroupForm = function () {
    return {
      holiday_login: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      weekend_login: '',
      tasks: '',
      active_hour_id: ''
    };
  };

  constructor(private _script: ScriptLoaderService,
              private userService: UserService,
              private schoolService: SchoolService,
              private administrateService: AdministationServiceService,
              private authService: AuthenticationService,
              private fb: FormBuilder,
              private Alert: NotificationService) {
    // this.authenticatedUser = this.userService.getAuthUser().login.user;
    this.createGroupForm = this.fb.group(GroupsComponent.createGroupForm());
    this.updateGroupForm = this.fb.group(GroupsComponent.updateGroupForm());
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
    // console.log('Authenticated user ', this.authenticatedUser);
  }

  ngOnInit() {
    this.getAllGroups();
    this.getAllActiveHours();
    // this.getAllPermissions();
    this.getAllTasks();
  }

  // ngAfterViewInit() {
  //     this._script.loadScripts('app-Groups',
  //         ['assets/Group_assets/demo/demo4/base/scripts.bundle.js', 'assets/Group_assets/app/js/dashboard.js']);

  // }

  public getAllGroups() {
    this.load.requesting.list = true;
    this.administrateService.getAllGroups().subscribe(
      (allGroupsResponse) => {
        this.load.requesting.list = false;
        this.allGroups = allGroupsResponse.data;
        if (this.allGroups.length > 0) {
          this.dtTrigger.next();
        }
        // console.log("All Groups response", allGroupsResponse);
      },
      (error) => {

        this.load.requesting.list = false;

      }
    );
  }

  public async createGroup() {

    this.load.requesting.create = true;
    this.load.message.create = 'Creating...';

    console.log('Create Group object ', this.createGroupForm.value);
    const taskIds: any[] = this.createGroupForm.value['tasks'];
    taskIds.splice(0, 1);
    this.createGroupForm.value['tasks'] = taskIds;
    await this.administrateService.createOrUpdateGroup(this.createGroupForm.value).subscribe(
      (createdGroupResponse) => {
        this.load.message.create = 'Create';
        this.Alert.success(`${this.createGroupForm.value.name} has been sent for authorization\n`);
        this.createGroupForm = this.fb.group(GroupsComponent.createGroupForm());
        this.load.requesting.create = false;
        this.triggerModalOrOverlay('close', 'createGroup');
        // this.allGroups.unshift(createdGroupResponse);
        console.log('Newly created school ', createdGroupResponse);
      },
      (error) => {
        this.load.message.create = 'Create';
        // this.triggerModal('close','createSchool');
        console.log('Eroorroroor ', error);
        this.load.requesting.create = false;
        this.Alert.error(`Could not create ${this.createGroupForm.value.name}`, error);
      }
    );
  }

  public updateGroup() {
    this.load.requesting.create = true;
    this.load.message.update = 'Updating...';

    this.administrateService.createOrUpdateGroup(this.updateGroupForm.value, this.updatedGroup.id).subscribe(
      (updatedGroupResponse) => {
        this.load.message.update = 'Update';
        this.Alert.success(`${this.updateGroupForm.value.name} has been sent for authorization\n`);
        this.updateGroupForm = this.fb.group(GroupsComponent.updateGroupForm());
        this.allGroups[this.editedIndex] = updatedGroupResponse;
        this.load.requesting.create = false;
        this.triggerModalOrOverlay('close', 'updateGroup');

        console.log('Updated school ', this.updateGroupForm);
      },
      (error) => {
        this.load.message.update = 'Update';
        this.load.requesting.create = false;
        this.Alert.error(`Could not update ${this.updateGroupForm.value.name}`, error);
      }
    );
  }


  public getAllFaculties() {
    this.schoolService.getAllFaculties().subscribe(
      (facultiesResponse) => {
        console.log('All faculties ', facultiesResponse);
        this.allFaculties = facultiesResponse;
      },
      (error) => {
        this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
      }
    );
  }

  /**
   * This method returns a single faculty which contains a list of departments
   */
  public getDepartmentByByFacultyId(facultyId) {
    this.load.departments.list = true;
    this.load.departments.loading = true;

    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
      (departmentResponse) => {
        this.load.departments.loading = false;

        this.allDepartments = departmentResponse.departments;
        // console.log("returned departments", departmentResponse);
      },
      (error) => {
        this.load.departments.list = false;
        this.load.departments.loading = false;

        this.Alert.error(`Sorry could not load departments`, error);
      }
    );
  }

  public showChange(event) {
    // console.log('Dropdown select eventhfjfjf ', event)
  }

  public async getAllCourses() {
    await this.schoolService.getAllCourses().subscribe(
      (allCoursesResponse) => {
        this.allCourses = allCoursesResponse.data;
        this.updatedGroup.loading = false;
      },
      (error) => {
        this.updatedGroup.loading = false;
        this.Alert.error('Sorry, could not load school courses', error);
      });
  }

  public getCourseByDepartmentId(departmentId) {
    this.load.courses.list = true;
    this.load.courses.loading = true;
    this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
      (coursesResponse) => {
        this.load.courses.loading = false;

        this.allCourses = coursesResponse.course_of_studies;
        // console.log("returned courses", coursesResponse);
      },
      (error) => {
        this.load.courses.list = false;
        this.load.courses.loading = false;
        this.Alert.error(`Sorry could not load courses`, error);
      });
  }

  public getAllCountries() {
    this.schoolService.getAllCountries().subscribe(
      (countriesResponse) => {
        this.allCountries = countriesResponse;
        // console.log("returned countries", countriesResponse);
      },
      (error) => {
        this.Alert.error(`Sorry could not load countries`, error);
      });
  }


  public getAllPermissions() {
    this.load.requesting.list = true;
    this.administrateService.getAllPermissions().subscribe(
      (allPermissionsResponse) => {
        this.load.requesting.list = false;
        const permissions = allPermissionsResponse['data'];
        // console.log('All permissions on load ', allPermissionsResponse)
        permissions.forEach((permission) => {
          const tasks = permission['tasks'];
          tasks.forEach((task) => {
            this.allPermissions.push(task);
          });
        });
        if (this.allPermissions.length > 0) {
          this.dtTrigger.next();
        }
        // console.log("All Permissions response", allPermissionsResponse);
      },
      (error) => {
        this.Alert.error('Could not load permissions');
        this.load.requesting.list = false;
      });
  }

  public getAllTasks() {
    this.load.requesting.list = true;
    this.administrateService.getAllTasks().subscribe(
      (allTasksResponse) => {
        this.load.requesting.list = false;
        this.allTasks = allTasksResponse;
        // console.log('Formatted tasks BEFOREfor reset constant ', this.allTasks)

        // this.allTasks = utils.resetDataForMultiSelect(allTasks, 'class');

        // console.log('Formatted tasks for reset constant ', this.allTasks)
        // if (this.allTasks.length > 0) {
        //     this.dtTrigger.next();
        // }
        // console.log("All Tasks response", allTasksResponse)
      },
      (error) => {
        this.load.requesting.list = false;

      }
    );
  }


  public getStateByCountryId(countryId) {
    this.schoolService.getStateByCountryId(countryId).subscribe(
      (statesResponse) => {
        this.allStates = statesResponse.states;
        // console.log("returned States", statesResponse);
      },
      (error) => {
        this.Alert.error(`Sorry could not load States`, error);
      });
  }


  public logStaffIn() {
    this.schoolService.logStaffIn().subscribe(
      (loginResponse) => {
        this.userService.setAuthUser(loginResponse.token);
      });
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    if (ind >= 0) {
      this.allGroups[ind].loading = true;
      this.updatedGroup = this.allGroups[ind];
      this.viewedGroup = this.allGroups[ind];
      console.log(this.updatedGroup);
      const editedGroupTasks: any[] = [];
      this.updatedGroup.tasks.forEach((groupTask) => {
        editedGroupTasks.push(groupTask['id']);
      });
      this.viewedGroup.tasks = JSON.parse(JSON.stringify(editedGroupTasks));

      const GroupManagementObject = {
        name: this.updatedGroup.name,
        weekend_login: this.updatedGroup.weekend_login,
        holiday_login: this.updatedGroup.holiday_login,
        active_hour_id: this.updatedGroup.active_hour.id,
        group_id: this.updatedGroup.id,
        tasks: (this.updatedGroup.tasks) ? editedGroupTasks : []
      };
      this.updateGroupForm.patchValue(GroupManagementObject);
      // console.log("Group management object ", GroupManagementObject);

      // this.updatedGroup.loading=false

    }
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

  public getAllActiveHours() {
    this.schoolService.getAllActiveHours().subscribe(
      (activeHourResponse) => {
        this.allActiveHours = activeHourResponse.data;
      },
      (error) => {
        this.Alert.error('Could not load active hours', error);
      });
  }

  private toRemoveByPermission () {
    if (this.permissions.id.length) {
      this.permissions.id.forEach(permission => {
        if (permission && !this.iAmPermitted(permission)) {
          const temp = document.getElementById(permission);
          temp.parentNode.removeChild(temp);
        }
      });
    }
    if (this.permissions.class.length) {
      this.permissions.class.forEach(permission => {
        if (permission && !this.iAmPermitted(permission)) {
          const temp_elements = document.getElementsByClassName(permission);
          for (let i = 0; i < temp_elements.length; i++) {
            temp_elements[i].parentNode.removeChild(temp_elements[i]);
          }
        }
      });
    }
  }

  ngAfterViewInit(): void {
    this.toRemoveByPermission();
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

}


