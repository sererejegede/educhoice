import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {PermissionsComponent} from './permissions/permissions.component';
import {AuthorizationsComponent} from './authorizations/authorizations.component';
import {TasksComponent} from './tasks/tasks.component';
import {GroupsComponent} from './groups/groups.component';
import {ModulesComponent} from './modules/modules.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {StaffDashboardComponent} from '../dashboard-component.component';
import {DataTablesModule} from "angular-datatables";
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {PipeModule} from "../../../../../shared/modules/pipe.module";
import {GroupViewComponent} from './groups/group-view/group-view.component';


const ADMINISTRATIVE_ROUTES: Routes = [
    {
        path: 'administration', component: StaffDashboardComponent,
        children: [
            {path: 'permissions', component: PermissionsComponent},
            {path: 'authorizations', component: AuthorizationsComponent},
            {path: 'tasks', component: TasksComponent},
            {path: 'groups', component: GroupsComponent},
            {path: 'groups/:id', component: GroupViewComponent},
            {path: 'modules', component: ModulesComponent}


        ]
    }

]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        DataTablesModule,
        PipeModule,
        MultiselectDropdownModule,
        AngularMultiSelectModule,
        RouterModule.forChild(ADMINISTRATIVE_ROUTES)
    ],
    declarations: [
        PermissionsComponent,
        AuthorizationsComponent,
        TasksComponent,
        GroupsComponent,
        ModulesComponent,
        GroupViewComponent
    ],
    exports: [
        PermissionsComponent,
        AuthorizationsComponent,
        TasksComponent,
        GroupsComponent,
        ModulesComponent,
        RouterModule
    ]
})
export class AdministrationModule {
}
