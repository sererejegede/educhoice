import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import * as utils from '../../../../../../utils/util';
import {Faculty} from '../../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../../interfaces/department.interface';
import {Course} from '../../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../../services/school.service';
import {UserService} from '../../../../../../services/user.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {AdministationServiceService} from '../../../../../../services/api-service/administation-service.service';
import {AuthenticationService} from '../../../../../../services/authentication.service';

declare const $: any;
// console.log = function () {};

@Component({
  selector: 'app-authorizations',
  templateUrl: './authorizations.component.html',
  styleUrls: ['./authorizations.component.css']
})
export class AuthorizationsComponent implements OnInit, AfterViewInit {

  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);

  public overlay = utils.overlay;
  public allAuthorizations: any[] = [];
  public updatedAuthorization: any;
  public allFaculties: Faculty[] = [];
  public allDepartments: Department[] = [];
  public allCourses: Course[] = [];
  public allStates: any[] = [];
  public allCountries: any[] = [];
  public authenticatedUser: any;
  public myAuthorizations: any[] = [];
  public myEditedAuthorization: any = {};
  public createAuthorizationForm: FormGroup;
  public updateAuthorizationForm: FormGroup;
  public declineCommentForm: FormGroup;
  public editedIndex: number;
  public declinedIndex = 0;
  public load = {
    requesting: {
      list: true,
      create: false
    },
    message: {
      create: 'Submit',
      update: 'Update'
    },
    departments: {
      list: false,
      loading: false
    },
    courses: {
      list: false,
      loading: false
    },
    Authorizations: {
      list: false,
      loading: false
    }

  };
  public permissions = {
    id: [],
    class: [
      'school.administration.authorize.reject',
      'school.administration.authorize.approve'
    ]
  };


  static createAuthorizationForm = function () {
    return {
      email: ['', Validators.compose([Validators.required])],
      fullname: ['', Validators.compose([Validators.required])],
      group_id: '',
      employee_id: '',
      active_hour_id: ''

    };
    // email,first_name,last_name,course_of_study_id
  };

  static declineCommentForm = function () {
    return {
      comment: ['', Validators.compose([Validators.required])]
    };
  };


  static updateAuthorizationForm = function () {
    return {
      email: ['', Validators.compose([Validators.required])],
      fullname: ['', Validators.compose([Validators.required])],
      group_id: '',
      employee_id: ['', Validators.compose([Validators.required])],
      active_hour_id: ['', Validators.compose([Validators.required])]
    };
  };

  constructor(private _script: ScriptLoaderService,
              private userService: UserService,
              private schoolService: SchoolService,
              private administrateService: AdministationServiceService,
              private authService: AuthenticationService,
              private fb: FormBuilder,
              private Alert: NotificationService) {
    // this.authenticatedUser = this.userService.getAuthUser().login.user;
    this.createAuthorizationForm = this.fb.group(AuthorizationsComponent.createAuthorizationForm());
    this.updateAuthorizationForm = this.fb.group(AuthorizationsComponent.updateAuthorizationForm());
    this.declineCommentForm = this.fb.group(AuthorizationsComponent.declineCommentForm());
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
    console.log('Authenticated user ', this.authenticatedUser);
  }

  ngOnInit() {
    this.getAllAuthorizations();
    this.getAllAndEveryAuthorization();
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  // ngAfterViewInit() {
  //     this._script.loadScripts('app-Authorizations',
  //         ['assets/Authorization_assets/demo/demo4/base/scripts.bundle.js', 'assets/Authorization_assets/app/js/dashboard.js']);

  // }

  public getAllAuthorizations() {
    this.load.requesting.list = true;
    this.administrateService.getAllAuthorizations().subscribe(
      (allAuthorizationsResponse) => {
        this.load.requesting.list = false;
        this.allAuthorizations = allAuthorizationsResponse.data;
        if (this.allAuthorizations.length > 0) {
          this.dtTrigger.next();
        }
        console.log('All Authorizations response', allAuthorizationsResponse);
      },
      (error) => {

        this.load.requesting.list = false;

      }
    );
  }

  public async createAuthorization() {

    this.load.requesting.create = true;
    this.load.message.create = 'Creating...';

    console.log('Create Authorization object ', this.createAuthorizationForm.value);
    await this.administrateService.createOrUpdateAuthorization(this.createAuthorizationForm.value).subscribe(
      (createdAuthorizationResponse) => {
        this.load.message.create = 'Create';
        this.Alert.success(`${this.createAuthorizationForm.value.name} created successfully\n`);
        this.createAuthorizationForm = this.fb.group(AuthorizationsComponent.createAuthorizationForm());
        this.load.requesting.create = false;
        this.triggerModalOrOverlay('close', 'createAuthorization');
        this.myAuthorizations.push(createdAuthorizationResponse);
        console.log('Newly created school ', createdAuthorizationResponse);
      },
      (error) => {
        this.load.message.create = 'Create';
        // this.triggerModal('close','createSchool');
        console.log('Eroorroroor ', error);
        this.load.requesting.create = false;
        this.Alert.error(`Could not create ${this.createAuthorizationForm.value.name}`, error);
      }
    );
  }

  public updateAuthorization() {
    this.load.requesting.create = true;
    this.load.message.update = 'Updating...';

    this.administrateService.createOrUpdateAuthorization(this.updateAuthorizationForm.value, this.updatedAuthorization.id).subscribe(
      (updatedAuthorizationResponse) => {
        this.load.message.update = 'Update';
        this.updateAuthorizationForm = this.fb.group(AuthorizationsComponent.updateAuthorizationForm());
        this.Alert.success(`${this.updateAuthorizationForm.value.name} updated successfully\n`);
        this.allAuthorizations[this.editedIndex] = updatedAuthorizationResponse;
        this.load.requesting.create = false;
        this.triggerModalOrOverlay('close', 'updateAuthorization');

        console.log('Updated school ', this.updateAuthorizationForm);
      },
      (error) => {
        this.load.message.update = 'Update';
        this.load.requesting.create = false;
        this.Alert.error(`Could not update ${this.updateAuthorizationForm.value.first_name}`, error);
      }
    );
  }


  public getAllFaculties() {
    this.schoolService.getAllFaculties().subscribe(
      (facultiesResponse) => {
        console.log('All faculties ', facultiesResponse);
        this.allFaculties = facultiesResponse;
      },
      (error) => {
        this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
      }
    );
  }

  /**
   * This method returns a single faculty which contains a list of departments
   */
  public getDepartmentByByFacultyId(facultyId) {
    this.load.departments.list = true;
    this.load.departments.loading = true;

    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
      (departmentResponse) => {
        this.load.departments.loading = false;

        this.allDepartments = departmentResponse.departments;
        console.log('returned departments', departmentResponse);
      },
      (error) => {
        this.load.departments.list = false;
        this.load.departments.loading = false;

        this.Alert.error(`Sorry could not load departments`, error);
      }
    );
  }

  public async getAllCourses() {
    await this.schoolService.getAllCourses().subscribe(
      (allCoursesResponse) => {
        this.allCourses = allCoursesResponse.data;
        this.updatedAuthorization.loading = false;
      },
      (error) => {
        this.updatedAuthorization.loading = false;
        this.Alert.error('Sorry, could not load school courses', error);
      });
  }

  public getCourseByDepartmentId(departmentId) {
    this.load.courses.list = true;
    this.load.courses.loading = true;
    this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
      (coursesResponse) => {
        this.load.courses.loading = false;

        this.allCourses = coursesResponse.course_of_studies;
        console.log('returned courses', coursesResponse);
      },
      (error) => {
        this.load.courses.list = false;
        this.load.courses.loading = false;
        this.Alert.error(`Sorry could not load courses`, error);
      }
    );
  }

  public getAllCountries() {
    this.schoolService.getAllCountries().subscribe(
      (countriesResponse) => {
        this.allCountries = countriesResponse;
        console.log('returned countries', countriesResponse);
      },
      (error) => {
        this.Alert.error(`Sorry could not load countries`, error);
      }
    );
  }

  public getStateByCountryId(countryId) {
    this.schoolService.getStateByCountryId(countryId).subscribe(
      (statesResponse) => {
        this.allStates = statesResponse.states;
        console.log('returned States', statesResponse);
      },
      (error) => {
        this.Alert.error(`Sorry could not load States`, error);
      }
    );
  }

  public toggleAuthorizationStatus(ind, action) {
    const toggleMessage = (action === 'approve') ? 'Authorize' : 'Decline';
    let declineComment = '';
    if (toggleMessage === 'Authorize') {
      this.allAuthorizations[ind].approveloading = true;
    } else {
      this.allAuthorizations[ind].rejectloading = true;
      declineComment = this.declineCommentForm.value;
    }
    const authorization_id = this.allAuthorizations[ind].id;
    this.administrateService.toggleAuthorizationStatus(authorization_id, action, declineComment).subscribe(
      (toggleResponse) => {
        const authorizationAction = this.allAuthorizations[ind].action;
        this.Alert.success(`${authorizationAction} has been ${toggleMessage}d!`);

        this.unSetLoader(ind, toggleMessage);
        console.log('Toggle response ', toggleResponse);
      },
      (error) => {
        this.unSetLoader(ind, toggleMessage, error);
        this.allAuthorizations[ind].loading = false;
        this.Alert.error(`An error occurred. Could not ${toggleMessage} this request`, error);

      });
  }

  private unSetLoader(ind, action, error?) {
    if (error) {
      this.allAuthorizations[ind].approveloading = false;
      this.allAuthorizations[ind].rejectloading = false;
      return;
    }
    if (action === 'Authorize') {
      this.allAuthorizations[ind].approveloading = false;
      this.allAuthorizations[ind].status = 2;
    } else {
      this.allAuthorizations[ind].rejectloading = false;
      this.allAuthorizations[ind].status = 3;
      this.triggerModalOrOverlay('close', 'declineComment');

    }
  }


  public logStaffIn() {
    this.schoolService.logStaffIn().subscribe(
      (loginResponse) => {
        this.userService.setAuthUser(loginResponse.token);


      }
    );
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    if (ind >= 0) {
      this.allAuthorizations[ind].loading = true;
      this.declinedIndex = ind;
    }

    // this.updatedAuthorization.loading=false


    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

  public getAllAndEveryAuthorization() {
    return this.administrateService.getAllAndEveryAuthorization().subscribe(
      (everyAuthResponse) => {
        console.log('All and every authorization ', everyAuthResponse);

      },
      (error) => {
        console.log('All and every authorization ', error);
      });
  }

  private toRemoveByPermission () {
    if (this.permissions.id.length) {
      this.permissions.id.forEach(permission => {
        if (permission && !this.iAmPermitted(permission)) {
          const temp = document.getElementById(permission);
          temp.parentNode.removeChild(temp);
        }
      });
    }
    if (this.permissions.class.length) {
      this.permissions.class.forEach(permission => {
        if (permission && !this.iAmPermitted(permission)) {
          const temp_elements = document.getElementsByClassName(permission);
          for (let i = 0; i < temp_elements.length; i++) {
            temp_elements[i].parentNode.removeChild(temp_elements[i]);
          }
        }
      });
    }
  }

  ngAfterViewInit(): void {
    this.toRemoveByPermission();
  }

}


