import {Component, OnInit, ElementRef} from '@angular/core';
import {Validators, FormGroup, FormBuilder, FormArray} from '@angular/forms';
import {Faculty} from '../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../interfaces/department.interface';
import {SchoolService} from '../../../../../services/school.service';
import {UserService} from '../../../../../services/user.service';
import {NotificationService} from '../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../services/api-service/staff-config.service';
import {ResultsService} from '../../../../../services/api-service/results.service';
import {AuthenticationService} from '../../../../../services/authentication.service';
import {Cache} from '../../../../../utils/cache';

declare const $;

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  public id;
  public miscellaneous = {
    unselect: false,
    was_once_checked: false,
    approval: false
  };
  public resultDetails = {
    faculty: '',
    department: '',
    course_of_study: '',
    session: '',
    semester: '',
    curriculum_course: {}
  };
  public add_extra = false;
  public allLevels: any[] = [];
  public lecturerCourses: any[] = [];
  public allSession: any[] = [];
  public allSemester: any[] = [];
  public allResults: any[] = [];
  public allFaculties: Faculty[] = [];
  public allDepartments: Department[] = [];
  public allCourseOfStudy: any[] = [];
  public allCurriculumCourses: any[] = [];
  public authenticatedUser = {
    group_id: 0,
    user_object: null
  };
  public createForm: FormGroup;
  public addResultForm: FormGroup;
  public previousResultForm: FormGroup;
  public editable = false;
  public permissions = {
    'school.course-management.result.hodApproval': false,
    'school.course-management.result.deanApproval': false,
    'school.course-management.result.seneateApproval': false,
    'school.course-management.result.hodMultipleApproval': false,
    'school.course-management.result.deanMultipleApproval': false,
    'school.course-management.result.seneateMultipleApproval': false,
    'school.course-management.result.publishMultipleResult': false,
    'school.course-management.result.create.multiple': false,
    'school.course-management.result.upload': false,
    'school.course-management.result.curriculum_course': false,
    'school.course-management.curriculum.courses': false
  };
  public abbreviations = [
    {short: 'TC', long: 'TOTAL CREDIT'},
    {short: 'TWGP', long: 'TOTAL WEIGHTED GRADE POINTS'},
    {short: 'GPA', long: 'GRADE POINT AVERAGE'},
    {short: 'CTC', long: 'CUMMULATIVE TOTAL CREDIT'},
    {short: 'CTWGP', long: 'CUMMULATIVE TOTALWEIGHTED GRADE POINT POPOINTPOINTS'},
    {short: 'CGPA', long: 'CUMMULATIVE GRADE POINT AVERAGE'},
    {short: 'REMM', long: 'REMARKS'},
    {short: 'NA', long: 'NOT APPLICABLE'},
    {short: 'ABS', long: 'ABSENT WITH PERMISSION'},
    {short: 'RNA', long: 'RESULT NOT AVAILABLE'},
    {short: 'W', long: 'WITHDRAWAL'},
    {short: 'VW', long: 'VOLUNTARY WITHDRAWAL'},
    {short: 'PRI', long: 'PROBATION 1'},
    {short: 'PR2', long: 'PROBATION 2'},
    {short: 'NR', long: 'NOT REGISTERED FOR'},
    {short: 'GS', long: 'IN GOOD STANDING'},
    {short: 'NGS', long: 'NOT IN GOOD STANDING'},
    {short: 'SDC', long: 'STUDENT DISCIPLINARY COMMITTEE'},
    {short: 'DHR', long: 'DEANS HONOURS ROLL'},
    {short: 'VCL', long: 'VICE CHANCELLOR’S LIST'},
    {short: 'NP', long: 'NO PAYMENT'},
    {short: 'SOS', long: 'SUSPENSION OF STUDIES'},
  ];
  public dummyPermission = {
    lecturer: false,
    hod: false,
    dean: false,
    senate: false,
    publish: false
  };
  public curriculum_course_id: number;
  public filename = '';
  public load = {
    approve: false,
    decline: false,
    approveMultiple: {
      hod: false,
      dean: false,
      senate: false,
      publish: false,
      unpublish: false
    },
    declineMultiple: {
      hod: false,
      dean: false,
      senate: false,
      publish: false,
      unpublish: false
    },
    getting_lecturer_courses: false,
    requesting: {
      list: true,
      create: false,
      print: false,
      table: false
    },
    uploading: false,
    download: false,
    faculties: false,
    departments: {
      list: false,
      loading: false
    },
    courses: false,
    level: false,
    session: false,
    semester: false,
    curriculum_course: false
  };

  static formData = function () {
    return {
      faculty_id: ['', Validators.compose([Validators.required])],
      department_id: ['', Validators.compose([Validators.required])],
      course_of_study_id: ['', Validators.compose([Validators.required])],
      level_id: ['', Validators.compose([Validators.required])],
      session_id: ['', Validators.compose([Validators.required])],
      semester: ['', Validators.compose([Validators.required])],
      curriculum_course_id: ['', Validators.compose([Validators.required])]
    };
  };
  public appSettings: any;

// BIO 101 = {level : 1, session : 15, semester : 1, faculty : 2, course_of_study : 8, department: 6, curriculum_course : 16486}


  private resultBuilder(index?) {
    if (this.allResults[index] && this.allResults[index].result) {
      return this.fb.group({
        ca: [this.allResults[index].result.ca * 1 || ''],
        exam: [this.allResults[index].result.exam * 1 || ''],
        comment: [this.allResults[index].result.comment * 1 || '']
      });
    } else {
      return this.fb.group({
        ca: ['', Validators.max(30)],
        exam: ['', Validators.max(70)],
        comment: ['']
      });
    }
  }

  private previousResults() {
    return this.fb.group({
      matric_no: [''],
      ca: ['', Validators.max(30)],
      exam: ['', Validators.max(70)],
      comment: ['']
    });
  }


  constructor(private schoolService: SchoolService,
              private fb: FormBuilder,
              private authService: AuthenticationService,
              private staffConfigService: StaffConfigService,
              private resultService: ResultsService,
              private userService: UserService,
              private Alert: NotificationService,
              private el: ElementRef) {
    this.createForm = this.fb.group(ResultsComponent.formData());
    this.addResultForm = this.fb.group({
      scores: this.fb.array([this.resultBuilder()])
    });
    this.previousResultForm = this.fb.group({
      extras: this.fb.array([this.previousResults()])
    });
  }

  ngOnInit() {
    this.authenticatedUser.user_object = this.userService.getAuthUser();
    this.authenticatedUser.group_id = (Cache.get('user_groups')) ? Cache.get('user_groups') : 0;
    // console.log(this.authenticatedUser);
    this.appSettings = Cache.get('app_settings');
    this.getAllSession();
    this.getAllFaculties();
    this.toRemoveByPermission();
  }

  private getAllSession() {
    this.load.session = true;
    this.staffConfigService.getSessionList().subscribe((res) => {
      this.load.session = false;
      this.allSession = res;
    }, (err) => {
      this.load.session = false;
      this.Alert.error('An error occurred', err);
    });
  }

  public getAllFaculties() {
    this.load.faculties = true;
    this.schoolService.getAllFaculties().subscribe(
      (facultiesResponse) => {
        this.load.faculties = false;
        this.allFaculties = facultiesResponse;
      },
      (error) => {
        this.load.faculties = false;
        this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
      },
    );
  }

  /**
   * This method returns a single faculty which contains a list of departments
   */
  public getDepartmentByByFacultyId(facultyId) {
    this.load.departments.list = true;
    this.load.departments.loading = true;
    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
      (departmentResponse) => {
        this.load.departments.loading = false;
        this.load.departments.list = false;
        this.allDepartments = departmentResponse.departments;
        this.resultDetails.faculty = departmentResponse.name;
      },
      (error) => {
        this.load.departments.list = false;
        this.load.departments.loading = false;

        this.Alert.error(`Sorry could not load departments`, error);
      });
  }

  getCourseOfStudy($event) {
    this.load.courses = true;
    this.staffConfigService.getCourseOfStudyById($event)
      .subscribe(response => {
          this.load.courses = false;
          this.allCourseOfStudy = response['course_of_studies'];
          this.resultDetails.department = response['name'];
        },
        error => {
          this.load.courses = false;
          this.Alert.error('Course of Study could not be loaded', error);
        }
      );
  }

  public getCurriculumCourses() {
    this.load.curriculum_course = true;
    this.staffConfigService.getCurriculumCourses(this.createForm.controls['course_of_study_id'].value, this.createForm.controls['level_id'].value, this.createForm.controls['session_id'].value, this.createForm.controls['semester'].value)
      .subscribe(responseData => {
          this.load.curriculum_course = false;
          this.allCurriculumCourses = responseData;
        },
        error => {
          this.load.curriculum_course = false;
          // console.log('Curriculum Courses loading  error', error);
        });
  }

  setCurriculumCourseId() {
    this.createForm.value['curriculum_course_id'] = this.createForm.value.curriculum_course_id;
  }

  public putResult() {
    // this.setCurriculumCourseId();
    this.allResults = [];
    // if(this.miscellaneous.was_once_checked){
    this.addResultForm.removeControl('scores');
    // }
    this.load.requesting.table = true;
    this.resultService.getResultByCurriculumCourse(this.createForm.value['curriculum_course_id'])
      .subscribe((response) => {
          this.load.requesting.table = false;
          this.allResults = response;
          // if(!this.miscellaneous.was_once_checked){
          this.repeatResultField();
          // }
          if (this.allResults.length < 1) {
            this.Alert.warning('No result found');
          } else {
            this.scrollIntoView('#resultUploadForm');
          }
        },
        (error) => {
          this.load.requesting.table = false;
          this.Alert.error('Could not load Results. Check connection and try again');
          // console.log('Result by curriculum error', error);
        });
  }

  public repeatResultField() {
    // this.miscellaneous.was_once_checked = true;
    /*
    let i = 1;
    if (this.addResultForm.controls['scores']) {
      while (i < this.allResults.length) {
        i++;
        const control = <FormArray>this.addResultForm.controls['scores'];
        control.push(this.resultBuilder(i));
      }
    } else {
      this.addResultForm.addControl('scores', this.fb.array([this.resultBuilder()]));
      while (i < this.allResults.length) {
        i++;
        const control = <FormArray>this.addResultForm.controls['scores'];
        control.push(this.resultBuilder(i));
      }
    }
    */
    if (this.addResultForm.controls['scores']) {
      for (let i = 0; i < this.allResults.length; i++) {
        const control = <FormArray>this.addResultForm.controls['scores'];
        if (i < this.allResults.length - 1) {
          control.push(this.resultBuilder(i + 1));
        }
      }
    } else {
      this.addResultForm.addControl('scores', this.fb.array([this.resultBuilder(0)]));
      for (let i = 0; i < this.allResults.length; i++) {
        const control = <FormArray>this.addResultForm.controls['scores'];
        if (i < this.allResults.length - 1) {
          control.push(this.resultBuilder(i + 1));
        }
        // console.log(i, this.allResults[i], this.addResultForm.value);
      }
    }
  }

  public prepareMultipleResult() {
    const studentRegistrationIds: number[] = [];
    const checks = this.el.nativeElement.querySelectorAll('[name = check]');
    // console.info(checks);
    /*for (let i = 0; i < checks.length; i++) {
      if(checks[i]['checked'])
    } */
    this.el.nativeElement.querySelectorAll('[name = check]').forEach((check, j) => {
      if (check.checked) {
        studentRegistrationIds.push(this.allResults[j].id);
      }
    });
    const dataArray = [];
    const dataObj = {
      data: []
    };
    for (let i = 0; i < this.allResults.length; i++) {
      const resultObj = {};
      for (let j = 0; j < studentRegistrationIds.length; j++) {
        if (this.allResults[i].id == studentRegistrationIds[j]) {
          resultObj['student_course_registration_id'] = this.allResults[i]['id'];
          resultObj['curriculum_course_id'] = this.allResults[i]['curriculum_course_id'];
          resultObj['student_registration_id'] = this.allResults[i]['student_registration_id'];
          resultObj['ca'] = this.addResultForm.value.scores[i].ca;
          resultObj['exam'] = this.addResultForm.value.scores[i].exam;
          resultObj['comment'] = this.addResultForm.value.scores[i].comment;
          dataArray.push(resultObj);
        }
      }
    }
    dataObj.data = dataArray;
    // console.log('Result to be imputed', dataArray);
    const override = this.el.nativeElement.querySelector('#force_upload');
    if (override) {
      dataObj['override'] = (override.checked) ? 1 : 0;
    }
    return dataObj;
  }

  public makeEditable() {
    this.editable = !!this.el.nativeElement.querySelector('#force_upload').checked;
    // this.editable = true;
  }

  public inputMultipleResult(save?) {
    if (!window.confirm(`Are you sure you want ${save ? 'save' : 'submit'}`)) {
      return;
    }
    if (this.previousResultForm.invalid || this.addResultForm.invalid) {
      return this.Alert.warning('Maximum for CA and Exam are 30 and 70 respectively');
    }
    this.load.requesting.create = true;
    const final = {
      ...this.prepareMultipleResult(),
      ...this.previousResultForm.value
    };
    final['status'] = save ? 0 : null;
    // return console.info('Final final', final);
    this.resultService.inputMultipleResult(this.createForm.value['curriculum_course_id'], final)
      .subscribe((response) => {
          this.load.requesting.create = false;
          this.Alert.success(response.message || 'Success');
          const reloadExtra = (response.message) ? response.message.includes('failed') : false;
          this.reloadTable(reloadExtra);
          // console.log('Input Multiple Success', response);
        },
        (error) => {
          this.load.requesting.create = false;
          this.Alert.error('Could not upload result', error);
          // console.log('Input Multiple Error', error);
        });
  }

  private scrollIntoView(location) {
    setTimeout(() => {
      this.el.nativeElement.querySelector(location).scrollIntoView({behavior: 'smooth'});
    }, 300);
  }

  public getSingleCourseOfStudy($event) {
    this.load.level = true;
    this.load.session = true;
    this.staffConfigService.getSingleCourseOfStudy($event)
      .subscribe((response) => {
          this.load.level = false;
          this.load.session = false;
          this.allLevels = response['degree']['programme']['levels'];
          this.allSession = response['degree']['programme']['sessions'];
          this.resultDetails.course_of_study = response.name;
        },
        (error) => {
          this.load.level = false;
          this.load.session = false;
          // console.log('Level error', error);
          // this.Alert.error('Course of Study could not be loaded', error)
        });
  }

  public getSessionSemesters(session_id) {
    this.load.semester = true;
    this.staffConfigService.getSingleSession(session_id)
      .subscribe((response) => {
        this.load.semester = false;
        this.allSemester = response['session_semesters'];
        this.resultDetails.session = response.name;
      }, (error) => {
        this.load.semester = false;
        this.Alert.error('Could not load Semesters. Please retry', error);
      });
  }

  public setSemester(semester) {
    this.allSemester.forEach(sem => {
      if (sem.semester.toString() === semester) {
        this.resultDetails.semester = sem ? sem.name : null;
      }
    })
  }

  /**
   *  File Upload and Download taken from curriculum course
   * **/
  public downloadTemplate(buttonId: string) {
    this.load.download = true;
    // this.selectedCurriculumCourseId = this.feedBack.allResponse['curriculum_courses'][ind].id;
    this.resultService.downloadResultTemplate(this.createForm.value['curriculum_course_id'])
      .subscribe((url) => {
          this.load.download = false;
          // const templateUrl = url;
          const downloadButton = $(`#${buttonId}`);
          downloadButton.attr('href', url);
          downloadButton[0].click();
          // console.log('Download button', downloadButton);
        },
        (error) => {
          this.load.download = false;
          this.Alert.error('An error occured, could not download template', error);
        });
  }

  public uploadResultTemplate(fileId) {
    if (!window.confirm('Are you sure you want submit')) {
      return;
    }
    this.load.uploading = true;
    const bulkFileId = `#${fileId}`;
    const formFile = this.el.nativeElement.querySelector(bulkFileId);
    // console.log('Form File ', formFile);
    const $linkId = $(bulkFileId);
    const $newLinkId = $linkId.val();
    const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
    $linkId.removeClass('alert alert-danger animated rubberBand');
    if (file_type && (file_type !== '.pdf') && (file_type !== '.docx') && (file_type !== '.doc') && (file_type !== '.xlsx')) {
      $linkId.addClass('alert alert-danger animated rubberBand');
      this.load.uploading = false;
      return this.Alert.error('You can only upload pdf, docx or doc files');
    }
    this.resultService.uploadResultTempate(formFile, this.createForm.value['curriculum_course_id'])
      .subscribe((fileResponse) => {
          this.load.uploading = false;
          this.reloadTable();
          // console.log('File upload Response', fileResponse);
          this.Alert.success('Result uploaded successfully.');
        },
        (error) => {
          this.load.uploading = false;
          this.Alert.error('An error occurred uploading file');
          // console.log('File upload error', error);
        }
      );

  }

  public hodApproval(status, resultIndex) {
    (status == 1) ? this.allResults[resultIndex]['approve'] = true : this.allResults[resultIndex]['decline'] = true;
    // console.log('Result Id', this.allResults[resultIndex]['result'].id);
    this.resultService.hodApproval(this.allResults[resultIndex]['result'].id, status)
      .subscribe((response) => {
          this.allResults[resultIndex]['approve'] = false;
          this.allResults[resultIndex]['decline'] = false;
          this.reloadTable();
          // console.log('HOD Approval response', response);
          (status == 1) ? this.Alert.success('Successfully Approved!') : this.Alert.success('Successfully Declined!');
        },
        (error) => {
          this.allResults[resultIndex]['approve'] = false;
          this.allResults[resultIndex]['decline'] = false;
          // console.log('HOD Approval response', error);
          (status == 1) ? this.Alert.error('Could not Approve!', error) : this.Alert.error('Could not Decline!', error);
        });
  }

  public deanApproval(status, resultIndex) {
    (status == 1) ? this.allResults[resultIndex]['approve'] = true : this.allResults[resultIndex]['decline'] = true;
    // this.allResults[resultIndex]['toggleLoading'] = true;
    // console.log('Result Id', this.allResults[resultIndex].id);
    this.resultService.deanApproval(this.allResults[resultIndex]['result'].id, status)
      .subscribe((response) => {
          this.allResults[resultIndex]['approve'] = false;
          this.allResults[resultIndex]['decline'] = false;
          this.reloadTable();
          // console.log('Dean Approval response', response);
          (status == 1) ? this.Alert.success('Successfully Approved!') : this.Alert.success('Successfully Declined!');
        },
        (error) => {
          this.allResults[resultIndex]['approve'] = false;
          this.allResults[resultIndex]['decline'] = false;
          // console.log('Dean Approval response', error);
          (status == 1) ? this.Alert.error('Could not Approve!', error) : this.Alert.error('Could not Decline!', error);
        });
  }

  public senateApproval(status, resultIndex) {
    (status == 1) ? this.allResults[resultIndex]['approve'] = true : this.allResults[resultIndex]['decline'] = true;
    // this.allResults[resultIndex]['toggleLoading'] = true;
    // console.log('Result Id', this.allResults[resultIndex]['result'].id);
    this.resultService.senateApproval(this.allResults[resultIndex]['result'].id, status)
      .subscribe((response) => {
          this.allResults[resultIndex]['approve'] = false;
          this.allResults[resultIndex]['decline'] = false;
          this.reloadTable();
          // console.log('Senate Approval response', response);
          (status == 1) ? this.Alert.success('Successfully Approved!') : this.Alert.success('Successfully Declined!');
        },
        (error) => {
          this.allResults[resultIndex]['approve'] = false;
          this.allResults[resultIndex]['decline'] = false;
          // console.log('Senate Approval response', error);
          (status == 1) ? this.Alert.error('Could not Approve!', error) : this.Alert.error('Could not Decline!', error);
        });
  }

  public publishResult(status, resultIndex) {
    (status == 1) ? this.allResults[resultIndex]['approve'] = true : this.allResults[resultIndex]['decline'] = true;
    // this.allResults[resultIndex]['toggleLoading'] = true;
    // console.log('Result Id', this.allResults[resultIndex]['result'].id);
    this.resultService.publishResult(this.allResults[resultIndex]['result'].id, status)
      .subscribe((response) => {
          this.allResults[resultIndex]['approve'] = false;
          this.allResults[resultIndex]['decline'] = false;
          // console.log('HOD Approval response', response);
          (status == 1) ? this.Alert.success('Successfully Published!') : this.Alert.success('Successfully Declined!');
        },
        (error) => {
          this.allResults[resultIndex]['approve'] = false;
          this.allResults[resultIndex]['decline'] = false;
          // console.log('HOD Approval response', error);
          (status == 1) ? this.Alert.error('Could not Publish!', error) : this.Alert.error('Could not Decline!', error);
        });
  }

  public resultIdCheckbox(event) {
    const checkboxes = this.el.nativeElement.querySelectorAll('[name = check]');
    if (event.target.checked) {
      for (let i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = true;
      }
    } else {
      for (let i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = false;
      }
    }
  }

  public getChecked() {
    const checkboxes = this.el.nativeElement.querySelectorAll('[name = check]');
    const selectedCheckboxesArray: number[] = [];
    checkboxes.forEach((check, j) => {
      if (check.checked) {
        if (this.miscellaneous.approval && this.allResults[j]['result']) {
          check['selectedIds'] = this.allResults[j]['result'].id;
          selectedCheckboxesArray.push(check['selectedIds']);
        }
      }

      console.log(`It is ${check.checked} that ${this.allResults[j]['result']} is checked!`);


    });
    // console.log('Selected Checkboxes', selectedCheckboxesArray);
    return {
      result_ids: selectedCheckboxesArray
    };
  }

  public hodApproveMultiple(status) {
    this.miscellaneous.approval = true;
    (status.toString() === '1') ? this.load.approveMultiple.hod = true : this.load.declineMultiple.hod = true;
    this.resultService.hodMultipleApproval(this.createForm.value['curriculum_course_id'], status, this.getChecked())
      .subscribe((success) => {
          this.load.approveMultiple.hod = false;
          this.load.declineMultiple.hod = false;
          this.reloadTable();
          // console.log('HOD Approval Multiple Success', success);
          this.Alert.success('Success!');
        },
        (error) => {
          this.load.approveMultiple.hod = false;
          this.load.declineMultiple.hod = false;
          this.Alert.error('Error! Try again.', error);
          // console.log('HOD Approval Multiple Error', error);
        });
  }

  public deanApproveMultiple(status) {
    this.miscellaneous.approval = true;
    (status == 1) ? this.load.approveMultiple.dean = true : this.load.declineMultiple.dean = true;
    this.resultService.deanMultipleApproval(this.createForm.value['curriculum_course_id'], status, this.getChecked())
      .subscribe((success) => {
          this.load.approveMultiple.dean = false;
          this.load.declineMultiple.dean = false;
          this.reloadTable();
          // console.log('Dean Approval Multiple Success', success);
          this.Alert.success('Success!');
        },
        (error) => {
          this.load.approveMultiple.dean = false;
          this.load.declineMultiple.dean = false;
          this.Alert.error('Error! Try again.');
          // console.log('Dean Approval Multiple Error', error);
        });
  }

  public senateApproveMultiple(status) {
    this.miscellaneous.approval = true;
    (status == 1) ? this.load.approveMultiple.senate = true : this.load.declineMultiple.senate = true;
    this.resultService.senateMultipleApproval(this.createForm.value['curriculum_course_id'], status, this.getChecked())
      .subscribe((success) => {
          this.load.approveMultiple.senate = false;
          this.load.declineMultiple.senate = false;
          this.reloadTable();
          // console.log('Senate Approval Multiple Success', success);
          this.Alert.success('Success!');
        },
        (error) => {
          this.load.approveMultiple.senate = false;
          this.load.declineMultiple.senate = false;
          this.Alert.error('Error! Try again.');
          // console.log('Senate Approval Multiple Error', error);
        });
  }

  public publishMultiple(status, publish = true) {
    (status == 1 && publish) ? this.load.approveMultiple.publish = true : this.load.declineMultiple.publish = true;
    (status == 1 && !publish) ? this.load.approveMultiple.unpublish = true : this.load.declineMultiple.unpublish = true;

    this.resultService.publishMultipleResult(this.createForm.value['curriculum_course_id'], this.getChecked(), publish)
      .subscribe((success) => {
          this.load.approveMultiple.publish = this.load.declineMultiple.publish = this.load.approveMultiple.unpublish = this.load.declineMultiple.unpublish = false;
          this.reloadTable();
          // console.log('Publish Multiple Success', success);
          this.Alert.success('Success!');
        },
        (error) => {
          this.load.approveMultiple.publish = this.load.declineMultiple.publish = this.load.approveMultiple.unpublish = this.load.declineMultiple.unpublish = false;

          this.Alert.error('Error! Try again.', error);
          // console.log('Publish Multiple Error', error);
        });
  }

  public setPermission(course_id, courses) {
    this.dummyPermission.hod = false;
    this.dummyPermission.dean = false;
    this.dummyPermission.senate = false;
    this.dummyPermission.publish = false;
    let selected_curriculum_course: any = {};
    courses.forEach((course) => {
      if (course.id.toString() === course_id.toString()) {
        selected_curriculum_course = course;
        this.resultDetails.curriculum_course = course;
        // console.info('selected_curriculum_course', selected_curriculum_course);
      }
    });
    const current_hod = selected_curriculum_course.curriculum.course_of_study.department.current_hod;
    const current_dean = selected_curriculum_course.curriculum.course_of_study.department.faculty.current_dean;
    if (current_hod && current_hod.id.toString() === this.authenticatedUser.user_object.login.user_id.toString() || this.authenticatedUser.user_object.login.id.toString() === '1') {
      this.dummyPermission.hod = true;
    }
    if (current_dean && current_dean.id.toString() === this.authenticatedUser.user_object.login.user_id.toString() || this.authenticatedUser.user_object.login.id.toString() === '1') {
      this.dummyPermission.dean = true;
    }
  }

  public getFileName(e) {
    this.filename = e.target.files[0].name;
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  public reloadTable(reloadExtra?) {
    this.allResults = [];
    if (reloadExtra) {
      this.previousResultForm.reset();
    }
    this.putResult();
  }

  public addMore() {
    this.add_extra = true;
    // const control = <FormArray>this.createForm.controls['grade'];
    const control = <FormArray>this.previousResultForm.controls['extras'];
    control.push(this.previousResults());
  }

  public teste() {
    console.log(this.previousResultForm.value);
  }

  public getLecturerCourses() {
    const data = this.createForm.value;
    if (data.semester) {
      this.load.getting_lecturer_courses = true;
      this.resultService.getStaffCourses(data.session_id, data.semester).subscribe(
        (res) => {
          this.load.getting_lecturer_courses = false;
          this.lecturerCourses = res;
        }, (err) => {
          this.load.getting_lecturer_courses = false;
        });
    }
  }

  public printNau(id, title) {
    this.load.requesting.print = true;
    const that = this;
    // Internet Explorer 6-11
    const isIE = /*@cc_on!@*/false || !!document['documentMode'];
    // Edge 20+
    const isEdge = !isIE && !!window['StyleMedia'];
    // Chrome 1 - 71
    const isChrome = !!window['chrome'] && (!!window['chrome'].webstore || !!window['chrome'].runtime);
    console.info('Chrome', isChrome);
    console.info('Edge', isEdge);
    console.info('IE', isIE);
    const prev_frame = $('#print_frame');
    if (prev_frame) {
      prev_frame.remove();
    }
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['id'] = 'print_frame';
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-10000px', 'pointer-events': 'none'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    frameDoc.document.write('<html><head><title>Print ' + title + '</title>');
    frameDoc.document.write('</head><body>');
    frameDoc.document.write('<link href="/assets/css/print.css" rel="stylesheet">');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    frameDoc.onload = function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      // frame1.remove();
      that.load.requesting.print = false;
    }
  }

  private toRemoveByPermission() {
    for (const permission in this.permissions) {
      this.permissions[permission] = this.iAmPermitted(permission);
    }
  }
}
