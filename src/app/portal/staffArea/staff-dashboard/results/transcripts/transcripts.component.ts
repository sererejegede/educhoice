import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';
import {Cache} from "../../../../../../utils/cache";
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import * as utils from '../../../../../../utils/util';
import {Faculty} from '../../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../../interfaces/department.interface';
import {Course} from '../../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../../services/school.service';
import {UserService} from '../../../../../../services/user.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {AdmissionsService} from "../../../../../../services/api-service/admissions.service";
import {AuthenticationService} from "../../../../../../services/authentication.service";
import {ResultsService} from "../../../../../../services/api-service/results.service";

declare const $: any;

@Component({
  selector: 'app-transcripts',
  templateUrl: './transcripts.component.html',
  styleUrls: ['./transcripts.component.css']
})
export class TranscriptsComponent implements OnInit {

    el: ElementRef;
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject();
    dtElement: DataTableDirective = new DataTableDirective(this.el);

    public allDenominations: any[] = [];
    public allLgas: any[] = [];
    public overlay = utils.overlay;
    public allTranscripts: any[] = [];
    public updatedTranscript: any;
    public allFaculties: Faculty[] = [];
    public allDepartments: Department[] = [];
    public allCourses: Course[] = [];
    public allStates: any[] = [];
    public allCountries: any[] = [];
    public authenticatedUser: any;
    public myTranscripts: any[] = [];
    public myEditedTranscript: any = {};
    public createTranscriptFeeForm: FormGroup;
    public updateTranscriptForm: FormGroup;
    public editedIndex: number;
    public load = {
        requesting: {
            list: false,
            create: false
        },
        message: {
            create: 'Create',
            update: 'Update'
        },
        departments: {
            list: false,
            loading: false
        },
        courses: {
            list: false,
            loading: false
        },
        Transcripts: {
            list: false,
            loading: false
        },
        lgas: {
            list: false,
            loading: false
        }

    };

    static createTranscriptFeeForm = function () {
        return {
            other_names: ['', Validators.compose([Validators.required])],
            first_name: ['', Validators.compose([Validators.required])],


        };
        // email,first_name,last_name,course_of_study_id
    };

    constructor(
      private _script: ScriptLoaderService,
      private userService: UserService,
      private schoolService: SchoolService,
      private resultService: ResultsService,
      private fb: FormBuilder,
      private authService: AuthenticationService,
      private staffConfigService: StaffConfigService,
      private admissionsService: AdmissionsService,
      private Alert: NotificationService) {
      this.createTranscriptFeeForm = this.fb.group(TranscriptsComponent.createTranscriptFeeForm())
    }

  ngOnInit() {
    this.getAllStudentTranscriptApplications();
  }

    public getAllStudentTranscriptApplications() {
        this.load.requesting.list = true;
        this.schoolService.getAllStudentTranscriptApplications().subscribe(
            (allTranscriptsResponse) => {
                this.load.requesting.list = false;
                this.allTranscripts = allTranscriptsResponse.data;
                console.log('All Transcripts response ', allTranscriptsResponse)
                if (this.allTranscripts.length > 0) {
                    this.dtTrigger.next();
                }
                console.log('All Transcripts ', this.allTranscripts);
            },
            (error) => {
                console.log('Could not load list of Transcripts')
                this.load.requesting.list = false;

            }
        );
    }


    public async createTranscriptFee() {
        this.load.requesting.create = true;
        this.load.message.create = 'Creating...';

        console.log('Create student object ', this.createTranscriptFeeForm.value);
        await this.resultService.createTranscriptFee(this.createTranscriptFeeForm.value).subscribe(
            (createdStudentResponse) => {
                this.load.message.create = 'Create';
                this.Alert.success(`${this.createTranscriptFeeForm.value.first_name} created successfully\n`);
                this.createTranscriptFeeForm = this.fb.group(TranscriptsComponent.createTranscriptFeeForm());
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'createFee');
                createdStudentResponse['status'] = '1';
                // this.allTranscripts.unshift(createdStudentResponse);
                console.log('Newly created student ', createdStudentResponse);
            },
            (error) => {
                this.load.message.create = 'Create';
                // this.triggerModal('close','createSchool');
                console.log('Eroorroroor ', error);
                this.load.requesting.create = false;
                this.Alert.error(`Could not create ${this.createTranscriptFeeForm.value.first_name}`, error);
            }
        );
    }


    public toggleApplicationStatus(ind, action?) {
        let newApplicationStatus = ( action === 'approve');
        let toggleMessage = (newApplicationStatus) ? 'Approve' : 'Decline';
        if (newApplicationStatus) {
            this.allTranscripts[ind].approveloading = true;
        }
        else {
            this.allTranscripts[ind].rejectloading = true;
        }
        const applicationId = this.allTranscripts[ind].id;
        this.schoolService.toggleApplicationTranscriptStatus(newApplicationStatus, applicationId).subscribe(
            (toggleResponse) => {
                if (toggleResponse['message']) {
                    return this.Alert.success(`${toggleResponse['message']}`);
                }
                this.allTranscripts[ind]['status'] = toggleResponse['status'].toString();
                this.Alert.success(`${this.allTranscripts[ind]['student']['first_name']} ${this.allTranscripts[ind]['year']} ${toggleMessage}d!`);
                console.log('Toggle response ', toggleResponse)
            },
            (error) => {
                this.Alert.error(`An error occurred. Could not ${toggleMessage} ${this.allTranscripts[ind]['name']}`);
                console.log("Toggle error ", error)
            })
    }

    public completeTranscriptApplication(ind) {
        const TranscriptApplicationId = this.allTranscripts[ind].id;
        this.schoolService.completeTranscriptApplication(TranscriptApplicationId).subscribe((applicationsResponse) => {
                this.Alert.success('Application Transcript completed');
                console.log('Application Transcript response ', applicationsResponse);
            },
            (error) => {
                this.Alert.error('Could not complete student Transcript. Please retry', error);
            })

    }

    public iAmPermitted(routeName):boolean{
        return this.authService.iAmPermitted(routeName);
    }



    public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        if (ind >= 0) {

        }
        (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
        // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
        // });

    }

}
