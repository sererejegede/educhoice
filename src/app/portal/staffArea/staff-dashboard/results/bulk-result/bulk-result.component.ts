import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {NotificationService} from "../../../../../../services/notification.service";
import {SchoolService} from "../../../../../../services/api-service/school.service";
import {StaffConfigService} from "../../../../../../services/api-service/staff-config.service";
import {ResultsService} from "../../../../../../services/api-service/results.service";
import {Cache} from "../../../../../../utils/cache";
import {UserService} from "../../../../../../services/user.service";
import {AuthenticationService} from "../../../../../../services/authentication.service";

@Component({
  selector: 'app-bulk-result',
  templateUrl: './bulk-result.component.html',
  styleUrls: ['./bulk-result.component.css']
})
export class BulkResultComponent implements OnInit {

  public auth_user: any = {};
  public filterForm: FormGroup;
  public faculties: any[] = [];
  public departments: any[] = [];
  public courses_of_study: any[] = [];
  public levels: any[] = [];
  public sessions: any[] = [];
  public semesters: any[] = [];
  public what_to_do: string;
  public loader = {
    loading: false,
    faculties: false,
    departments: false,
    cos: false,
    level_session: false,
    approve: false,
    publish: false,
    semester: false
  };
  public unpublishResults:boolean = false;
  public permissions = {
    'school.course-management.result.senateApproveFilteredResults': false,
    'school.course-management.result.publishFilteredResults': false
  };

  private static formData = function () {
    return {
      faculty_id: ['', Validators.compose([Validators.required])],
      department_id: [''],
      course_of_study_id: [''],
      level_id: [''],
      session_id: ['', Validators.compose([Validators.required])],
      semester: ['', Validators.compose([Validators.required])]
    };
  };

  constructor(private fb: FormBuilder,
              private notification: NotificationService,
              private schoolService: SchoolService,
              private resultService: ResultsService,
              private authService: AuthenticationService,
              private userService: UserService,
              private staffConfigService: StaffConfigService) {
    this.filterForm = this.fb.group(BulkResultComponent.formData());
  }

  ngOnInit() {
    this.getAllFaculties();
    this.getProgramme();
    this.auth_user.group_id = (Cache.get('user_groups')) ? Cache.get('user_groups') : 0;
    this.auth_user.user = this.userService.getAuthUser();
    console.info(this.auth_user);
    this.toRemoveByPermission();
  }

  private getProgramme() {
    this.staffConfigService.getProgramme(1).subscribe(res => {
      this.sessions = res.sessions;
    }, err => {
      this.notification.error('Something went wrong', err);
    })
  }

  private getAllFaculties() {
    this.loader.faculties = true;
    this.schoolService.getAllFaculties().subscribe(
      (facultiesResponse) => {
        this.loader.faculties = false;
        this.faculties = facultiesResponse;
      },
      (error) => {
        this.loader.faculties = false;
        this.notification.error('An error occurred', error);
      },
    );
  }

  public getDepartmentByByFacultyId(facultyId) {
    this.loader.departments = true;
    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe((res) => {
        this.loader.departments = false;
        this.departments = res.departments;
        this.auth_user.isDean = res.current_dean ? this.auth_user.user.login.id === res.current_dean.id : false;
      },
      (error) => {
        this.loader.departments = false;
        this.notification.error(`An error occurred`, error);
      });
  }

  public getDepartment($event) {
    this.loader.cos = true;
    this.staffConfigService.getCourseOfStudyById($event)
      .subscribe(response => {
          this.loader.cos = false;
          this.courses_of_study = response['course_of_studies'];
        },
        error => {
          this.loader.cos = false;
          this.notification.error('Course of Study could not be loaded', error);
        }
      );
  }

  public getSingleCourseOfStudy($event) {
    this.loader.level_session = true;
    this.staffConfigService.getSingleCourseOfStudy($event).subscribe((response) => {
          this.loader.level_session = false;
          this.levels = response['degree']['programme']['levels'];
          this.sessions = response['degree']['programme']['sessions'];
        },
        (error) => {
          this.loader.level_session = false;
          // console.log('Level error', error);
          // this.Alert.error('Course of Study could not be loaded', error)
        });
  }

  public getSessionSemesters(session_id) {
    this.loader.semester = true;
    this.staffConfigService.getSingleSession(session_id).subscribe((response) => {
        this.loader.semester = false;
        this.semesters = response['session_semesters'];
      }, (error) => {
        this.loader.semester = false;
        // this.notification.error('Could not load Semesters. Please retry', error);
      });
  }

  public senateApprove() {
    this.loader.approve = true;
    this.processData();
    this.resultService.senateApprove(this.filterForm.value).subscribe(res => {
      this.loader.approve = false;
      this.notification.success(res.message || 'Success!')
    }, err => {
      this.loader.approve = false;
      this.notification.error('An error ocurred', err);
    })
  }

  public publish() {
    this.loader.approve = true;
    this.processData();
    this.resultService.publishAll(this.filterForm.value,this.unpublishResults).subscribe(res => {
      this.loader.approve = false;
      this.notification.success(res.message || 'Success!');
    }, err => {
      this.loader.approve = false;
      this.notification.error('An error occurred', err);
    })
  }

  public setUnpublish(unpublishValue){
    switch(unpublishValue){
        case 'publish':
          this.what_to_do = 'publish';
            this.unpublishResults = false;
          break;
        case  'approve':
          this.what_to_do = 'approve';
            this.unpublishResults = false;
          break;
        case 'unpublish':
            this.what_to_do = 'unpublish';
          this.unpublishResults = true;
          break;
        default:
          return;
    }


  }

  private processData() {
    this.sessions.forEach(session => {
      if (this.filterForm.value.session_id === session.id.toString()) {
        this.filterForm.value['session_year'] = session.year;
      }
    });
    for (const form_field in this.filterForm.value) {
      if (!this.filterForm.value[form_field]) {
        delete this.filterForm.value[form_field];
      }
    }
  }

  private iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  private toRemoveByPermission () {
    for (const permission in this.permissions) {
      this.permissions[permission] = this.iAmPermitted(permission);
    }
  }
}
