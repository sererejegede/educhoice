import {Component, ElementRef, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {TableExport} from '../../../../../../utils/table-export';
import {Cache} from "../../../../../../utils/cache";
import {ResultsService} from "../../../../../../services/api-service/results.service";

@Component({
  selector: 'app-result-templates',
  templateUrl: './result-templates.component.html',
  styleUrls: ['./result-templates.component.css']
})
export class ResultTemplatesComponent implements OnInit {

  createForm: FormGroup;
  filterCourseForm: FormGroup;
  public printId = {
    id: 'mss1print',
    title: 'MSS1',
    landscape: true
  };
  public schoolSettings: any;
  public allFaculties: any[] = [];
  public allDepartments: any[] = [];
  public allCourseOfStudy: any[] = [];
  public allSession: any[] = [];
  public allLevels: any[] = [];
  public allCurriculumCourses: any[] = [];
  public allResults: any[] = [];
  public selectedCourses: any[] = [];
  public toPrint: any[] = [];
  public allSemester: any[] = [];
  public finalLevel: any = {};
  public mattersArising: any = {};
  public resultDetails: any = {
    faculty: '',
    department: '',
    programme: '',
    course_of_study: '',
    level: '',
    level_id: '',
    hod: '',
    dean: '',
    session: '',
    semester: '',
    showResult: false
  };
  public load = {
    requesting: {
      list: true,
      create: false,
      print: false,
      table: false
    },
    departments: {
      list: false,
      loading: false
    },
    courses: {
      list: false,
      loading: false
    },
    level: {
      list: false,
      loading: false
    },
    session: {
      list: false,
      loading: false
    },
    semester: {
      list: false,
      loading: false
    }
  };
  public abbreviations = [
    {short: 'TC', long: 'TOTAL CREDIT'},
    {short: 'TWGP', long: 'TOTAL WEIGHTED GRADE POINTS'},
    {short: 'GPA', long: 'GRADE POINT AVERAGE'},
    {short: 'CTC', long: 'CUMULATIVE TOTAL CREDIT'},
    {short: 'CTWGP', long: 'CUMULATIVE TOTAL WEIGHTED GRADE POINT'},
    {short: 'CGPA', long: 'CUMULATIVE GRADE POINT AVERAGE'},
    {short: 'REMM', long: 'REMARKS'},
    {short: 'NA', long: 'NOT APPLICABLE'},
    {short: 'ABS', long: 'ABSENT WITH PERMISSION'},
    {short: 'RNA', long: 'RESULT NOT AVAILABLE'},
    {short: 'W', long: 'WITHDRAWAL'},
    {short: 'VW', long: 'VOLUNTARY WITHDRAWAL'},
    {short: 'PRI', long: 'PROBATION 1'},
    {short: 'PR2', long: 'PROBATION 2'},
    {short: 'NR', long: 'NOT REGISTERED FOR'},
    {short: 'GS', long: 'IN GOOD STANDING'},
    {short: 'NGS', long: 'NOT IN GOOD STANDING'},
    {short: 'SDC', long: 'STUDENT DISCIPLINARY COMMITTEE'},
    {short: 'DHR', long: 'DEANS HONOURS ROLL'},
    {short: 'VCL', long: 'VICE CHANCELLOR’S LIST'},
    {short: 'NP', long: 'NO PAYMENT'},
    {short: 'SOS', long: 'SUSPENSION OF STUDIES'},
    {short: 'EXP', long: 'EXPELLED'},
  ];
  public awards = [
    'Pro-Chancellor\'s Prize (Awarded to the Graduating student with a minimum of 4.80)',
    'Vice-Chancellor\'s Prize (Awarded to the best graduating student with a minimum of 4.60)',
    'Faculty Prize (Awarded to the best graduating student with highest CGPA but not less than 4.50)',
    'Departmental Prize (Awarded to the best graduating student with CGPA of not less than 4.25)',
    'Dean\'s Honours Roll (CGPA of 4.40 and above)'
  ];

  static formData = function () {
    return {
      level_id: ['', Validators.required],
      session_id: ['', Validators.required],
      semester: ['', Validators.required],
      faculty_id: [''],
      course_of_study_id: ['', Validators.required],
      department_id: ['']
      // level_id: [1],
      // session_id: [15],
      // semester: [1],
      // faculty_id: [''],
      // course_of_study_id: [8],
      // department_id: ['']
    };
  };

  constructor(private fb: FormBuilder,
              private staffConfigService: StaffConfigService,
              private resultService: ResultsService,
              private Alert: NotificationService,
              private _el: ElementRef) {
    this.createForm = this.fb.group(ResultTemplatesComponent.formData());
    this.filterCourseForm = this.fb.group({course_codes: ''});
  }

  ngOnInit() {
    this.schoolSettings = Cache.get('ping') || Cache.get('app_settings');
    this.getAllFaculties();
    // this.getFilteredResult();
    // this.getAllSession();
  }

  public getAllFaculties() {
    this.staffConfigService.getAllFaculty()
      .subscribe(response => {

          this.allFaculties = response['data'];
          // console.log('Faculties', this.allFaculties);
        },
        error => {
          this.Alert.error('Network error. Please reload page', error);
          console.log('Something happened::', error);
        });
  }

  public getAllDepartment($event) {
    this.saveResultDetailsForFaculty($event);
    this.load.departments.loading = true;
    this.staffConfigService.getFaculty($event)
      .subscribe(response => {
          this.load.departments.loading = false;
          this.allDepartments = response['departments'];
          // console.log('Departments', response)
        },
        error => {
          this.load.departments.loading = false;
          console.log('Something went wrong', error);
          this.Alert.error('Something went wrong');
        });
  }

  public getCourseOfStudy($event) {
    this.saveResultDetailsForDepartment($event);
    this.load.courses.loading = true;
    this.staffConfigService.getCourseOfStudyById($event)
      .subscribe(response => {
          this.load.courses.loading = false;
          // console.log('Course of Study', response);
          // console.log('Levels', this.allLevels);
          this.allCourseOfStudy = response['course_of_studies'];

        },
        error => {
          this.load.courses.loading = false;
          this.Alert.error('Course of Study could not be loaded', error);
        });
  }

  public getSingleCourseOfStudy($event) {
    this.load.level.loading = true;
    this.load.session.loading = true;
    const that = this;
    this.allCourseOfStudy.forEach(function (course_of_study) {
      if (course_of_study.id == $event) {
        that.resultDetails.course_of_study = course_of_study['name'];
      }
    });
    this.staffConfigService.getSingleCourseOfStudy($event)
      .subscribe((response) => {
          this.load.level.loading = false;
          this.load.session.loading = false;
          this.allLevels = response['degree']['programme']['levels'];
          if (!this.allLevels.find(level => level.id === 'spillover')) {
            this.allLevels.push({id: 'spillover', name: 'Spill Over'});
          }
          this.resultDetails.programme = response['degree']['programme']['name'];
          this.allSession = response['degree']['programme']['sessions'];
          // console.log('Levels', response);
        },
        (error) => {
          this.load.level.loading = false;
          this.load.session.loading = false;
          console.log('Level error', error);
          // this.Alert.error('Course of Study could not be loaded', error)
        });
  }

  public processSummary(item, value, grade?) {
    switch (item) {
      case 'average':
      case 'total':
      case 'passes':
      case 'min':
      case 'max':
        return this.allResults['summary'][item][value];
      case 'grade_summary': {
        return this.allResults['summary'][item][value][grade] || '0';
      }
    }
  }

  public getFilteredResult() {
    this.load.requesting.create = true;
    this.resultService.getFilteredResult(this.createForm.value).subscribe(responseData => {
        this.load.requesting.create = false;
        this.resultDetails.showResult = true;
        this.allResults = responseData;
        this.scrollIntoView();
        this.computeGrading(responseData);
        this.toPrint = JSON.parse(JSON.stringify(responseData));
        this.getSelectedCourses();
        this.getGradList();
        this.getMattersArising();
        this.filterCourseForm.controls['course_codes'].setValue(this.allResults['selected_courses']);
      },
      error => {
        this.load.requesting.create = false;
        this.Alert.error('Could not load result', error);
      });
  }

  private getGradList() {
    this.finalLevel = {};
    this.staffConfigService.getGraduatingList(this.createForm.value).subscribe((res) => {
      this.finalLevel = res;
    }, (err) => {
      this.Alert.error('An error occurred', err);
    });
  }

  private getMattersArising() {
    this.mattersArising = {};
    this.resultService.getMattersArising(this.createForm.value).subscribe((res) => {
      this.mattersArising = res;
      if (this.mattersArising['A. STUDENTS ON PROBATION']) {
        if (this.mattersArising['A. STUDENTS ON PROBATION']['Probation I']) {
          this.mattersArising['A. STUDENTS ON PROBATION']['Probation I'] = this.prepareObject(this.mattersArising['A. STUDENTS ON PROBATION']['Probation I']);
        }
        if (this.mattersArising['A. STUDENTS ON PROBATION']['Probation II']) {
          this.mattersArising['A. STUDENTS ON PROBATION']['Probation II'] = this.prepareObject(this.mattersArising['A. STUDENTS ON PROBATION']['Probation II']);
        }
      }
      if (this.mattersArising['B. WITHDRAWAL LIST']) {
        if (this.mattersArising['B. WITHDRAWAL LIST']['On Academic Basis']) {
          this.mattersArising['B. WITHDRAWAL LIST']['On Academic Basis'] = this.prepareObject(this.mattersArising['B. WITHDRAWAL LIST']['On Academic Basis']);
        }
        if (this.mattersArising['B. WITHDRAWAL LIST']['Voluntary Withdrawal']) {
          this.mattersArising['B. WITHDRAWAL LIST']['Voluntary Withdrawal'] = this.prepareObject(this.mattersArising['B. WITHDRAWAL LIST']['Voluntary Withdrawal']);
        }
      }
      if (this.mattersArising['C. STUDENTS WITH OUTSTANDING CREDITS GREATER THAN 12']) {
        this.mattersArising['C. STUDENTS WITH OUTSTANDING CREDITS GREATER THAN 12'] = this.prepareObject(this.mattersArising['C. STUDENTS WITH OUTSTANDING CREDITS GREATER THAN 12']);
      }
      if (this.mattersArising['D. STUDENTS WITH CGPA NOT LESS THAN 4.25']) {
        this.mattersArising['D. STUDENTS WITH CGPA NOT LESS THAN 4.25'] = this.prepareObject(this.mattersArising['D. STUDENTS WITH CGPA NOT LESS THAN 4.25']);
      }
      if (this.mattersArising['E. STUDENTS WITH AWARDS']){
        this.awards.forEach(award => {
          if (this.mattersArising['E. STUDENTS WITH AWARDS'][award]) {
            this.mattersArising['E. STUDENTS WITH AWARDS'][award] = this.prepareObject(this.mattersArising['E. STUDENTS WITH AWARDS'][award]);
          }
        });
      }
    }, (err) => {
      this.Alert.error('An error occurred', err);
    });
  }

  private prepareObject(obj) {
    let arr = [];
    for (const i in obj) {
      arr.push({level: i, students: obj[i]});
    }
    return arr;
  }

  private computeGrading(responseData) {
    const grading = {
      degree_class: [],
      grades: []
    };
    for (const key in responseData.cgpa_config.degree_class) {
      responseData.cgpa_config.degree_class[key][0] = parseFloat(responseData.cgpa_config.degree_class[key][0]).toFixed(2);
      responseData.cgpa_config.degree_class[key][1] = parseFloat(responseData.cgpa_config.degree_class[key][1]).toFixed(2);
      grading['degree_class'].push({key: key, value: responseData.cgpa_config.degree_class[key]});
      // console.log({key: key, value: grading.degree_class[key]});
    }
    for (const key in responseData.cgpa_config.grades) {
      grading['grades'].push({key: key, value: responseData.cgpa_config.grades[key]});
      // console.log({key: key, value: grading.degree_class[key]});
    }
    this.allResults['grading'] = grading;
  }

  private getSelectedCourses() {
    if (this.allResults['allCourses'].length) {
      this.selectedCourses = this.allResults['allCourses'].filter(course => {
        return this.allResults['selected_courses'].includes(course.code);
      });
    }
    // console.log(this.selectedCourses);
  }

  public filterSelectedCourses() {
    this.selectedCourses = this.allResults['allCourses'].filter(course => {
      return this.filterCourseForm.value['course_codes'].includes(course.code);
    });
    // console.log(this.selectedCourses);
  }

  private scrollIntoView() {
    setTimeout(() => {
      this._el.nativeElement.querySelector('#showResult').scrollIntoView({behavior: 'smooth'});
    }, 300);
  }

  public saveResultDetailsForSession($event) {
    this.load.semester.loading = true;
    const that = this;
    this.allSession.forEach(function (session) {
      if (session.id == $event) {
        that.resultDetails.session = session['name'] || session['year'];
      }
    });
    this.staffConfigService.getSingleSession($event)
      .subscribe((response) => {
        this.load.semester.loading = false;
        this.allSemester = response['session_semesters'];
      }, (error) => {
        this.load.semester.loading = false;
        this.Alert.error('Could not load Semesters. Please retry', error);
      });
  }

  public saveResultDetailsForFaculty($event) {
    const that = this;
    this.allFaculties.forEach(function (faculty) {
      if (faculty.id == $event) {
        that.resultDetails.faculty = faculty['name'];
        that.resultDetails.dean = faculty.current_dean ? faculty.current_dean.fullname : '';
      }
    });
  }

  public saveResultDetailsForDepartment($event) {
    const that = this;
    this.allDepartments.forEach(function (department) {
      if (department.id == $event) {
        that.resultDetails.department = department['name'];
        that.resultDetails.hod = department.current_hod ? department.current_hod.fullname : '';
      }
    });
  }

  public saveResultDetailsForSemester($event) {
    // console.log(this.allSemester);
    this.allSemester.forEach(semester => {
      if ($event.toString() === semester.semester.toString()) {
        this.resultDetails.semester = semester.name;
      }
    });
  }

  public saveResultDetailsForLevel($event) {
    const that = this;
    this.allLevels.forEach(function (level) {
      if (level.id == $event) {
        that.resultDetails.level = level['name'];
        that.resultDetails.level_id = level['id'];
      }
    });
  }

  public generateArrays(obj) {
    return Object.keys(obj).map((key) => {
      return obj[key];
    });
  }

  getStudentResult(results, course) {
    for (const i in results) {
      const result = results[i];
      if (course.code == result.curriculum_course.code) {
        if (result.comment && isNaN(parseInt(result.comment, 10))) {
          return result.comment;
        } else if (result.score === null) {
          return 'RNA';
        }
        return `${result.score}${result.grade}`;
      }
    }
    if (course.typeName === 'Core' && (course.curriculum && course.curriculum.level_id.toString() === this.resultDetails.level_id.toString())) {
      return 'NR';
    }
    return 'NA';
  }

  public downloadAsCSV() {
    TableExport.downloadCSV('#downloadCsv', '#mss1csv', 'mss1.csv');
    // console.log(TableExport.downloadCSV('#downloadCsv', '#mss1csv', 'mss1.csv'))
  }

  public checkRegisteredSemester(session, semester, registered_semesters) {
    for (const i in registered_semesters) {
      const registered_semester = registered_semesters[i];
      if (registered_semester['session_year'] == session.year && registered_semester['semester'] == semester.semester) {
        return 'Yes';
      }
    }
    return 'No';
  }

  public changePrintId(print_id, title, landscape?) {
    this.printId.id = print_id;
    this.printId.title = title;
    this.printId.landscape = !!landscape;
  }

  public printNau(id, title) {
    this.load.requesting.print = true;
    const that = this;
    // Internet Explorer 6-11
    const isIE = /*@cc_on!@*/false || !!document['documentMode'];
    // Edge 20+
    const isEdge = !isIE && !!window['StyleMedia'];
    // Chrome 1 - 71
    const isChrome = !!window['chrome'] && (!!window['chrome'].webstore || !!window['chrome'].runtime);
    console.info('Chrome', isChrome);
    console.info('Edge', isEdge);
    console.info('IE', isIE);
    const prev_frame = $('#print_frame');
    if (prev_frame) {
      prev_frame.remove();
    }
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['id'] = 'print_frame';
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-10000px', 'pointer-events': 'none'});
    // frame1.css({'position': 'absolute', 'top': '100', 'width': '100%', 'height': '1000px', 'background-color': 'white'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    frameDoc.document.write('<html><head><title>Print ' + title + '</title>');
    if (this.allResults['allCourses'].length >= 30) {
      frameDoc.document.write('<style>.font-60 {font-size: 60% !important;} .font-50 {font-size: 50% !important;}</style>');
    }
    // frameDoc.document.write('<style>.font-65 {font-size: 65% !important;} .font-80 {font-size: 80% !important;}</style>');
    frameDoc.document.write('</head><body>');
    frameDoc.document.write('<link href="/assets/css/print.css" rel="stylesheet">');
    // frameDoc.document.write('<style type="text/css" media="print">body{-webkit-transform: rotate(-90deg);-moz-transform:rotate(-90deg);filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);}</style>');
    frameDoc.document.write('<style>{@media print{@page {size: landscape}}} </style>');
    frameDoc.document.write('<style>.page-break{page-break-before: always;} </style>');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    frameDoc.onload = function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      // frame1.remove();
      that.load.requesting.print = false;
    }
  }

  public updateChecked() {
    console.log('toPrint before', this.toPrint);
    this.toPrint['student'] = [];
    const checkboxes = document.querySelectorAll('[name = check_print]');
    for (let i = 0; i < checkboxes.length; i++) {
      if (checkboxes[i]['checked']) {
        if (this.allResults['student'][i]) {
          this.toPrint['student'].push(this.allResults['student'][i]);
        }
      }
    }
    // console.log('toPrint afetr', this.toPrint);
  }

}
