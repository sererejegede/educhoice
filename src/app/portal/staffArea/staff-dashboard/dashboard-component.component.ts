import {Component, OnInit, AfterViewInit} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';
import {UserService} from '../../../../services/user.service';
import {AuthenticationService} from '../../../../services/authentication.service';
import {SchoolService} from "../../../../services/school.service";
import {Cache} from "../../../../utils/cache";
import {NotificationService} from "../../../../services/notification.service";

declare const $;


@Component({
    selector: 'app-dashboard-component',
    templateUrl: './dashboard-component.component.html',
    styleUrls: ['./dashboard-component.component.css']
})

export class StaffDashboardComponent implements OnInit, AfterViewInit {

    public activeLinkId: string;
    public fullUrlPath: string;
    private redirected:boolean = false;
    public allStudents: any[] = [];
    public siteLocation = {
        parentUrl: '',
        childUrl: ''
    };


    constructor(private router: Router,
                private userService: UserService,
                private schoolService: SchoolService,
                private Alert: NotificationService,
                private authService: AuthenticationService) {
        let fullPathUrl = window.location.pathname;
        const urlParts = fullPathUrl.split('/');
        if ((urlParts.length === 3) && (urlParts[1] === 'staff')) {
            this.siteLocation.parentUrl = 'Dashboard';
            this.siteLocation.childUrl = 'Home';
        }
        this.router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                // alert('path = '+event);
                fullPathUrl = event.url;
                const urlParts = fullPathUrl.split('/');
                if ((urlParts.length > 3)) {
                    this.siteLocation.parentUrl = urlParts[2];
                    this.siteLocation.parentUrl = (this.siteLocation.parentUrl === 'config') ? 'Configuration' : this.siteLocation.parentUrl;
                    this.siteLocation.childUrl = urlParts[3];
                }
            }
            // if (Cache.get('app_settings')['settings']['bg_image']) {
            //     $(`.m-table--head-bg-brand thead tr`).css({backgroundColor: `${Cache.get('app_settings')['settings']['secondary_color']}`});
            // }

        });


    }

    ngOnInit() {
        // // console.log('auth user details :: ', this.userService.getAuthUser());
        this.determineUserDefaultPass();

        // alert('Portal student Component!!');
    }

    ngAfterViewInit() {

    }

    /*public logUserOut() {
        this.authService.logout('stay').subscribe(
            (response) => {
               // console.log('Log out done!');
            },
            (error) => {
                // console.log('Could not log user out');
                this.userService.logout('stay');

            }
        );

    }*/

    public getAllStudents() {

        this.schoolService.getAllStudents().subscribe(
            (allStudentsResponse) => {
                this.allStudents = allStudentsResponse;
                Cache.set('all_students', allStudentsResponse)
                // console.log('All Students ', this.allStudents);
            },
            (error) => {
                // console.log('Could not load list of students')


            }
        );
    }

    private determineUserDefaultPass() {
        if (Cache.get('USER_PASSWORD')) {
            this.Alert.info(`Please change your default password!`);
            setTimeout(() => {
                this.router.navigateByUrl('/change-password');
            },3000);
        }
    }






}
