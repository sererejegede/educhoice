import {Component, OnInit, AfterViewInit, ElementRef, ViewChild} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';
import {Cache} from '../../../../../utils/cache';
import {Validators, FormGroup, FormBuilder, FormControl} from '@angular/forms';
import * as utils from '../../../../../utils/util';
import {Faculty} from '../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../interfaces/department.interface';
import {Course} from '../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../services/school.service';
import {UserService} from '../../../../../services/user.service';
import {NotificationService} from '../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../services/api-service/staff-config.service';
import {AdmissionsService} from '../../../../../services/api-service/admissions.service';
import {ActivatedRoute} from '@angular/router';
import {StudentServiceService} from '../../../../../services/api-service/student-service.service';
import {denominations, abbreviations} from "../../../../../utils/constants";


declare const $: any;

@Component({
  selector: 'app-student-view',
  templateUrl: './student-view.component.html',
  styleUrls: ['./student-view.component.css']
})
export class StudentViewComponent implements OnInit {


  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  public allLevels: any[] = [];
  public allSemesters: any[] = [];
  public allChristianDenominations = denominations;
  public setSemester = '';
  public showPasscode = false;
  private activeUserDetails = null;
  public oneTimePassCode: any = null;
  public setResultSemester = '';
  public facultyName = '';
  public facultyNames = '';
  public allDenominations: any[] = [];
  public allLgas: any[] = [];
  public allFees: any[] = [];
  public allFeeItems: any[] = [];
  public allSessions: any[] = [];
  public allProgrammes: any[] = [];
  public loadingResult = false;
  public loadSessions = false;
  public loader = false;
  public showResults = false;
  public overlay = utils.overlay;
  public allStudents: any[] = [];
  public searchTerm = new FormControl();
  public searchDiv = false;
  public searching = false;
  public tableWasOnceRendered = false;
  public searchResult: any[] = [];
  public noName = '';
  public viewedStudentsProgrammeId;
  public allViewedResults: any[] = [];
  public updatedStudent: any;
  public allFaculties: Faculty[] = [];
  public allDepartments: Department[] = [];
  public allCourses: Course[] = [];
  public allStates: any[] = [];
  public allStudentTransfers: any[] = [];
  public allCountries: any[] = [];
  public authenticatedUser: any;
  public viewedStudent: any;
  public studentResults: any[] = [];
  public studentId = 0;
  public selectedSessionId = 0;
  public createStudentForm: FormGroup;
  public updateStudentForm: FormGroup;
  public transferStudentForm: FormGroup;
  public getStudentResultForm: FormGroup;
  public createStudentExceptionForm: FormGroup;
  public assignFeeToStudentForm: FormGroup;
  public courseRegForm: FormGroup;
  public addMore = false;
  public appSettings: any;
  public courses: any = {
    registered: [],
    registered_ids: [],
    total_unit: 0,
    loading: false,
    registering: false,
    current_session: {},
    registration: {}
  };
  public semester: number;
  public editedIndex: number;
  public load = {
    requesting: {
      list: true,
      create: false
    },
    message: {
      create: 'Create',
      update: 'Update'
    },
    departments: {
      list: false,
      loading: false
    },
    courses: {
      list: false,
      loading: false
    },
    students: {
      list: false,
      loading: false
    },
    lgas: {
      list: false,
      loading: false
    },
    generating: false,
    status_changing: false,
    resetting_password: false,
    printing: false,
    getting_session: false
  };
  public result_is_transcript = false;
  public abbreviations = abbreviations;

  static transferStudentForm = function () {
    return {
      to_course_id: ['', Validators.compose([Validators.required, Validators.email])],
      to_level_id: ['', Validators.compose([Validators.required])],
    };
  };

  static courseRegFormControls = function () {
    return {
      session: ['', Validators.compose([Validators.required])],
      semester: ['', Validators.compose([Validators.required])]
    };
  };

  static getStudentResultForm = function () {
    return {
      session: [''],
      semester: ['']
    };
  };

  static createStudentExceptionForm = function () {
    return {
      session_year: ['', Validators.compose([Validators.required])],
      status: ['', Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])],


    };
    // email,first_name,last_name,course_of_study_id
  };

  static assignFeeToStudentForm = function () {
    return {
      amount: ['', Validators.compose([Validators.required])],
      fee_item_id: ['', Validators.compose([Validators.required])],
      session_year: ['', Validators.compose([Validators.required])],
    };
    // email,first_name,last_name,course_of_study_id
  };

  static createStudentForm = function () {
    return {
      email: ['', Validators.compose([Validators.required, Validators.email])],
      first_name: ['', Validators.compose([Validators.required])],
      other_names: '',
      last_name: ['', Validators.compose([Validators.required])],
      faculty_id: ['', Validators.compose([Validators.required])],
      department_id: ['', Validators.compose([Validators.required])],
      course_of_study_id: ['', Validators.compose([Validators.required])],
      country_id: ['', Validators.compose([Validators.required])],
      state_id: '',
      level_id: ['', Validators.compose([Validators.required])],
      matric_no: ['', Validators.compose([Validators.required])],
      lga_id: ['', Validators.compose([Validators.required])],
      birth_of_date: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])],
      religion: ['', Validators.compose([Validators.required])],
      denomination: ['', Validators.compose([Validators.required])],
      home_town: ['', Validators.compose([Validators.required])],
      marital_status: ['', Validators.compose([Validators.required])]

    };
    // email,first_name,last_name,course_of_study_id
  };

  static updateStudentForm = function () {
    return {
      email: ['', Validators.compose([Validators.required, Validators.email])],
      first_name: ['', Validators.compose([Validators.required])],
      other_names: '',
      last_name: ['', Validators.compose([Validators.required])],
      faculty_id: ['', Validators.compose([Validators.required])],
      department_id: ['', Validators.compose([Validators.required])],
      course_of_study_id: ['', Validators.compose([Validators.required])],
      country_id: ['', Validators.compose([Validators.required])],
      state_id: '',
      level_id: ['', Validators.compose([Validators.required])],
      matric_no: ['', Validators.compose([Validators.required])],
      lga_id: ['', Validators.compose([Validators.required])],
      birth_of_date: ['', Validators.compose([Validators.required])],
      gender: ['', Validators.compose([Validators.required])],
      religion: ['', Validators.compose([Validators.required])],
      denomination: '',
      home_town: ['', Validators.compose([Validators.required])],
      marital_status: ['', Validators.compose([Validators.required])]

    };

  };

  constructor(private _script: ScriptLoaderService,
              private userService: UserService,
              private schoolService: SchoolService,
              private studentService: StudentServiceService,
              private fb: FormBuilder,
              private staffConfigService: StaffConfigService,
              private admissionsService: AdmissionsService,
              private activeRoute: ActivatedRoute,
              private Alert: NotificationService) {
    this.searchStudent();
    this.createStudentForm = this.fb.group(StudentViewComponent.createStudentForm());
    this.updateStudentForm = this.fb.group(StudentViewComponent.updateStudentForm());
    this.transferStudentForm = this.fb.group(StudentViewComponent.transferStudentForm());
    this.getStudentResultForm = this.fb.group(StudentViewComponent.getStudentResultForm());
    this.createStudentExceptionForm = this.fb.group(StudentViewComponent.createStudentExceptionForm());
    this.assignFeeToStudentForm = this.fb.group(StudentViewComponent.assignFeeToStudentForm());
    this.courseRegForm = this.fb.group(StudentViewComponent.courseRegFormControls());
    this.activeUserDetails = this.userService.getAuthUser()['login'];
    // console.log("Active user etails ", this.activeUserDetails);

    this.dtOptions = {
      pagingType: 'full_numbers',
    };
    // console.log('Authenticated user ', this.authenticatedUser);
  }

  private searchStudent() {
    this.searchTerm.valueChanges
      .debounceTime(400)
      .subscribe(data => {
        this.searchDiv = true;
        this.searching = true;
        this.schoolService.searchStudentByName(data).subscribe(response => {
            this.searching = false;
            this.searchResult = response['data'];
            if (this.searchResult.length === 0) {
              this.noName = `No student was found with the name '${this.searchTerm.value}'`;

            }
          },
          (error) => {
            this.Alert.error(`Sorry your search for ${this.searchTerm.value} failed`, error);
          });
      });
  }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => {
      this.studentId = +params['id']; // (+) converts string 'id' to a number
      this.getStudentById(this.studentId);
      this.getStudentSessions(this.studentId);
      this.appSettings = Cache.get('app_settings') || Cache.get('ping');
      this.facultyName = Cache.get('app_settings')['settings']['config_faculty'];
      this.facultyNames = Cache.get('app_settings')['settings']['config_faculties'];
    });
    // this.getAllStudents();
    this.getAllCourses();
    this.getAllCountries();
    this.getAllFaculties();
    this.getAllLevels();
    this.getAllProgrammes();
    this.getAllFees();
    this.getAllStudentExceptions();
  }

  // ngAfterViewInit() {
  //     this._script.loadScripts('app-students',
  //         ['assets/student_assets/demo/demo4/base/scripts.bundle.js', 'assets/student_assets/app/js/dashboard.js']);

  // }

  public getAllStudents() {
    this.load.requesting.list = true;
    this.schoolService.getAllStudents().subscribe(
      (allStudentsResponse) => {
        this.load.requesting.list = false;
        if (allStudentsResponse.data) {
          this.allStudents = allStudentsResponse.data;

        } else {
          this.allStudents = allStudentsResponse;

        }
        if (this.allStudents.length > 0) {
        }
        console.log('All Students ', this.allStudents);
      },
      (error) => {
        this.load.requesting.list = false;
      }
    );
  }

  public changeStudentStatus (status) {
    this.load.status_changing = true;
    this.schoolService.changeStudentStatus(this.studentId, parseInt(status, 10)).subscribe(res => {
      this.load.status_changing = false;
      if (res.status) {
        this.viewedStudent['status'] = res.status;
      }
      this.Alert.success(res.message || 'Status changed successfully');
    }, err => {
      this.load.status_changing = false;
      this.Alert.error('Could not change status', err);
    });
  }

  public async createStudent() {
    this.load.requesting.create = true;
    this.load.message.create = 'Creating...';
    // console.log('Create student object ', this.createStudentForm.value);
    await this.schoolService.createStudent(this.createStudentForm.value).subscribe(
      (createdStudentResponse) => {
        this.load.message.create = 'Create';
        this.Alert.success(`${this.createStudentForm.value.first_name} created successfully\n`);
        this.createStudentForm = this.fb.group(StudentViewComponent.createStudentForm());
        this.load.requesting.create = false;
        this.triggerModalOrOverlay('close', 'createStudent');
        createdStudentResponse['status'] = '1';
        this.allStudents.unshift(createdStudentResponse);
        console.log('Newly created student ', createdStudentResponse);
      },
      (error) => {
        this.load.message.create = 'Create';
        // this.triggerModal('close','createSchool');
        console.log('Eroorroroor ', error);
        this.load.requesting.create = false;
        this.Alert.error(`Could not create ${this.createStudentForm.value.first_name}`, error);
      }
    );
  }

  public setSelectedSemesters(value) {
    this.selectedSessionId = value;
    this.allSemesters = this.selectWhereId(this.allSessions, 'id', value)['session_semesters'];

    console.log('All semesters ', this.allSemesters);

  }

  public async assignFeeToCurrentStudent() {

    this.load.requesting.create = true;
    this.load.message.create = 'Creating...';

    console.log('Create fee object ', this.assignFeeToStudentForm.value);
    const data = this.assignFeeToStudentForm.value;
    data['student_id'] = this.viewedStudent.id;
    await this.schoolService.createOrUpdateFee(data).subscribe(
      (createdFeeResponse) => {
        this.load.message.create = 'Create';
        this.Alert.success(`A fee of ${this.assignFeeToStudentForm.value.amount} has been Sent for Authorization`);
        this.assignFeeToStudentForm = this.fb.group(StudentViewComponent.assignFeeToStudentForm());
        this.load.requesting.create = false;

      },
      (error) => {
        this.load.message.create = 'Create';
        // this.triggerModal('close','createSchool');
        console.log('Eroorroroor ', error);
        this.load.requesting.create = false;
        this.Alert.error(`Could not create ${this.assignFeeToStudentForm.value.name}`, error);
      }
    );
  }

  public updateStudent() {
    this.load.requesting.create = true;
    this.load.message.update = 'Updating...';

    this.schoolService.updateStudent(this.updateStudentForm.value, this.updatedStudent.id).subscribe(
      (updatedStudentResponse) => {
        this.load.message.update = 'Update';
        this.updateStudentForm = this.fb.group(StudentViewComponent.updateStudentForm());
        this.Alert.success(`${this.updateStudentForm.value.first_name} updated successfully\n`);
        this.allStudents[this.editedIndex] = updatedStudentResponse;
        this.load.requesting.create = false;
        this.triggerModalOrOverlay('close', 'updateStudent');

        console.log('Updated school ', this.updateStudentForm);
      },
      (error) => {
        this.load.message.update = 'Update';
        this.load.requesting.create = false;
        this.Alert.error(`Could not update ${this.updateStudentForm.value.first_name}`, error);
      }
    );
  }

  public getDenomination(value) {
    this.allDenominations = (value === 'Christianity') ? this.allChristianDenominations : [];
  }

  public hideSearchDiv() {
    this.searchDiv = false;
  }


  public getAllFaculties() {
    this.schoolService.getAllFaculties().subscribe(
      (facultiesResponse) => {
        // console.log('All faculties ', facultiesResponse);
        this.allFaculties = facultiesResponse;
      },
      (error) => {
        this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
      }
    );
  }

  /**
   * This method returns a single faculty which contains a list of departments
   */
  public getDepartmentByByFacultyId(facultyId) {
    this.load.departments.list = true;
    this.load.departments.loading = true;

    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
      (departmentResponse) => {
        this.load.departments.loading = false;

        this.allDepartments = departmentResponse.departments;
        console.log('returned departments', departmentResponse);
      },
      (error) => {
        this.load.departments.list = false;
        this.load.departments.loading = false;

        this.Alert.error(`Sorry could not load departments`, error);
      }
    );
  }

  public getAllCourses() {

    this.schoolService.getAllCourses().subscribe(
      (allCoursesResponse) => {
        this.allCourses = allCoursesResponse;
        // console.log('All Courses from hereejdhhd ', allCoursesResponse);
        // this.updatedStudent.loading = false;
      },
      (error) => {
        // this.updatedStudent.loading = false;
        this.Alert.error('Sorry, could not load school courses', error);
      });
  }

  public getCourseByDepartmentId(departmentId) {
    this.load.courses.list = true;
    this.load.courses.loading = true;
    this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
      (coursesResponse) => {
        this.load.courses.loading = false;

        this.allCourses = coursesResponse.course_of_studies;
        console.log('returned courses', coursesResponse);
      },
      (error) => {
        this.load.courses.list = false;
        this.load.courses.loading = false;
        this.Alert.error(`Sorry could not load courses`, error);
      }
    );
  }

  public getAllCountries() {
    this.schoolService.getAllCountries().subscribe(
      (countriesResponse) => {
        this.allCountries = countriesResponse;
        // console.log('returned countries', countriesResponse);
      },
      (error) => {
        this.Alert.error(`Sorry could not load countries`, error);
      }
    );
  }

  public getStateByCountryId(countryId) {
    this.schoolService.getStateByCountryId(countryId).subscribe(
      (statesResponse) => {
        this.allStates = statesResponse.states;
        console.log('returned States', statesResponse);
      },
      (error) => {
        this.Alert.error(`Sorry could not load States`, error);
      }
    );
  }

  public getAllLevels() {
    this.staffConfigService.getAllLevel().subscribe(
      (levelsResponse) => {
        this.allLevels = levelsResponse.data;
        // console.log('All levels fro just now', levelsResponse);
      },
      (error) => {
        console.log('Could not load levels');
      }
    );
  }

  public getAllFees() {
    this.load.requesting.list = true;
    this.schoolService.getAllFees().subscribe(
      (allFeesResponse) => {
        this.load.requesting.list = false;
        this.allFees = allFeesResponse.data;
        this.getAllFeeItems();
        // console.log("All fees response", allFeesResponse)
      },
      (error) => {
        this.load.requesting.list = false;
      });

  }

  public getAllFeeItems() {
    this.schoolService.getAllFeeItems().subscribe(
      (allFeeItemsResponse) => {
        this.load.requesting.list = false;
        this.allFeeItems = allFeeItemsResponse.data;
        this.allFees.forEach((fee) => {
          this.allFeeItems.forEach((feeItem) => {
            const feeItemId = parseInt(feeItem.id);
            const feeFeeItemId = parseInt(fee.fee_item_id);
            if (feeItemId === feeFeeItemId) {
              fee.fee_item_name = feeItem.name;
            }
          });
        });
        console.log('All fee items response', allFeeItemsResponse);
      },
      (error) => {
        this.load.requesting.list = false;
      });
  }


  public logStaffIn() {
    this.schoolService.logStaffIn().subscribe(
      (loginResponse) => {
        this.userService.setAuthUser(loginResponse.token);

      }
    );
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    if (ind >= 0) {
      this.allStudents[ind].loading = true;
      this.editedIndex = ind;
      this.getAllCourses();
      this.updatedStudent = this.allStudents[ind];
      const studentManagementObject = {
        first_name: this.updatedStudent.first_name,
        last_name: this.updatedStudent.last_name,
        middle_name: this.updatedStudent.middle_name,
        matric_no: this.updatedStudent.matric_no,
        email: this.updatedStudent.email,
        course_of_study_id: this.updatedStudent.course_of_study.id,
        department_id: this.updatedStudent.course_of_study.department.id,
        faculty_id: this.updatedStudent.course_of_study.department.faculty.id,
        country_id: 1,
        state_id: 2
      };
      this.updateStudentForm.patchValue(this.viewedStudent);
      const patchUpdateObject = {
        faculty_id: ''
      };
      console.log('Student management object ', this.updatedStudent);

      // this.updatedStudent.loading=false

    }
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
    // });

  }

  public getStudentById(studentId) {
    this.loader = true;
    this.schoolService.getStudentById(studentId).subscribe(
      (studentResponse) => {
        this.loader = false;
        this.viewedStudent = studentResponse;
        this.computeGrading(studentResponse);
        // this.getSessionSemesters();
        const studentRegistrations = this.viewedStudent.registrations;
        studentRegistrations.forEach((result) => {
          const courseRegistration = result.course_registrations;
          courseRegistration.forEach((registration) => {
            const singleResult = registration.curriculum_course;
            singleResult['session_year'] = result.session_year;
            this.studentResults.push(singleResult);
          });
          // const singleResult = result.course_registrations.curriculum_course;
        });
        this.allStudentTransfers = this.viewedStudent['transfers'];
        this.viewedStudentsProgrammeId = this.viewedStudent['course_of_study']['degree']['programme_id'];
        this.getSessionsByProgrammeId(this.viewedStudentsProgrammeId);
        this.viewedStudent['parents'] = (this.viewedStudent['parents']) ? this.viewedStudent['parents'][0] : {};
        // console.log('This viewed student ', this.viewedStudent);
        // console.log('This student has results ', this.studentResults);
      },
      (error) => {
        this.loader = false;
        this.Alert.error(`Could not load this student's record`, error);
      }
    );
  }

  public transferCurrentStudent() {
    this.load.requesting.create = true;
    let transferData = {
      from_level_id: this.viewedStudent.level_id,
      from_course_id: this.viewedStudent.course_of_study_id,
      student_id: this.studentId
    };
    transferData = Object.assign(transferData, this.transferStudentForm.value);
    this.schoolService.creatStudentTransfer(transferData).subscribe(
      (studentTransferResponse) => {
        this.load.requesting.create = false;
        console.log(`Student transfer response ${studentTransferResponse}`);
        this.Alert.success(`${this.viewedStudent.first_name} has been successfully transferred`);
      },
      (error) => {
        this.load.requesting.create = false;
        this.Alert.error(`There ws an error transferring ${this.viewedStudent.first_name}`, error);

      }
    );
  }

  public setSelectedSemester(semesterId) {
    const semester = this.selectWhereId(this.allSemesters, 'id', semesterId);
    this.setSemester = semester['name'];
    this.setResultSemester = semester['semester'];
  }

  /*public getStudentResult() {
    this.loadingResult = true;
    this.showResults = false;
    this.allViewedResults = [];
    if (this.viewedStudent.id) {
      // data['semester'] = this.setResultSemester;
      this.studentService.getStudentResults(this.viewedStudent.id).subscribe((resultResponse) => {
          this.loadingResult = false;
          this.showResults = true;
          this.allViewedResults = resultResponse;
          this.processComputation();
        },
        (error) => {
          this.loadingResult = false;
          this.showResults = false;
          this.Alert.error(`Could not load results`, error);
        });
    }
  }*/
  public getStudentResult(is_transcript = false) {

    this.result_is_transcript = is_transcript;
    this.loadingResult = true;
    this.showResults = false;
    this.allViewedResults = [];
    if (this.viewedStudent.id) {
      // data['semester'] = this.setResultSemester;
      this.studentService.getStudentResults(this.viewedStudent.id).subscribe((resultResponse) => {
          this.loadingResult = false;
          this.showResults = true;
          this.allViewedResults = resultResponse;
          this.processComputation();
        },
        (error) => {
          this.loadingResult = false;
          this.showResults = false;
          this.Alert.error(`Could not load results`, error);
        });
    }
  }


  public getSessionsByProgrammeId(programmeId) {
    this.loadSessions = true;
    this.schoolService.getSessionsByProgrammeId(programmeId).subscribe(
      (sessionsResponse) => {
        this.loadSessions = false;

        this.allStudentTransfers.forEach((studentTransfer) => {
          studentTransfer['session_year'] = this.selectWhereId(this.allSessions, 'id', studentTransfer['session_id']).year;
        });
        // console.log('All sessions by programme Id ', sessionsResponse);
      },
      (error) => {
        this.loadSessions = false;

        console.log('An eror occured getting session ', error);
      }
    );
  }


  public getStudentSessions(studentId) {
    this.schoolService.getStudentRegistrationsForSessions(studentId).subscribe((sessionResponse) => {
        const allSessions: any[] = [];
        sessionResponse.forEach(response => {
          if (response.session) {
            allSessions.push(response.session);
          }
        });
        this.allSessions = allSessions;
        // this.Alert.success(`Result response loaded `);
        // console.log('This student sessions response ', allSessions);
      },
      (error) => {
        this.Alert.error('Could not load this result', error);
      }
    );
  }

  public getAllProgrammes() {
    this.staffConfigService.getAllProgramme().subscribe(
      (programmeResponse) => {
        this.allProgrammes = programmeResponse.data;
        // console.log('returned programmes', programmeResponse);
      },
      (error) => {
        this.Alert.error(`Sorry could not load Programmes`, error);
      }
    );
  }


  public getLgaByStateId(stateId) {
    this.load.lgas.loading = true;
    const dataItem = this.selectWhereId(this.allStates, 'id', stateId);
    console.log('Select state where id is ', dataItem);
    this.admissionsService.getLgaByStateId(stateId).subscribe(
      (lgasResponse) => {
        this.load.lgas.loading = false;

        this.allLgas = lgasResponse.lgas;
        console.log('returned lgas', lgasResponse);
      },
      (error) => {
        this.load.lgas.loading = false;
        this.Alert.error(`Sorry could not load Lgas for`, error);
        // ${this.selectWhereId(this.allStates, stateId).name}
      }
    );
  }

  public selectWhereId(data: any[], search_key: string, id) {
    const dataItem: any[] = [];
    data.forEach(item => {
      const itemKey = parseInt(item[search_key]);
      const itemId = parseInt(id);
      if (itemKey === itemId) {
        dataItem.push(item);
      }
    });
    return dataItem[0];

  }

  public renderStudentResult(): void {
    if (this.tableWasOnceRendered) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.tableWasOnceRendered = false;
          // Call the dtTrigger to rerender again

        },
        (rejection) => {
          console.log('Why ah was rejected ', rejection);
        });
    }

    this.getStudentResult();
  }


  public setGradeColor(score) {
    return (score && score < 40) ? {'color': 'red'} : {};
  }


  public getAllStudentExceptions() {
    this.schoolService.getAllStudentExceptions().subscribe(
      (exceptionsResponse) => {
        console.log('All student exceptions ', exceptionsResponse);
      },
      (error) => {
        // console.log('An error occurred loading student exceptions ', error)
      }
    );
  }

  public createStudentException() {
    this.load.requesting.create = true;
    this.load.message.create = 'Creating...';
    // console.log('Create student object ', this.createStudentExceptionForm.value);
    const data = this.createStudentExceptionForm.value;
    data['student_id'] = this.viewedStudent.id;
     this.schoolService.createOrUpdateStudentException(data).subscribe(
      (createdStudentExceptionResponse) => {
        this.load.message.create = 'Create';
        this.Alert.success(`Success`);
        this.createStudentExceptionForm = this.fb.group(StudentViewComponent.createStudentExceptionForm());
        this.load.requesting.create = false;
        this.triggerModalOrOverlay('close', 'createStudentException');
        // this.allStudentExceptions.push(createdStudentExceptionResponse);
        // console.log("Newly created student ", createdStudentExceptionResponse)
      },
      (error) => {
        this.load.message.create = 'Create';
        // this.triggerModal('close','createSchool');
        console.log('Eroorroroor ', error);
        this.load.requesting.create = false;
        this.Alert.error(`Could not create ${this.createStudentExceptionForm.value.type}`, error);
      }
    );
  }

  public generateOneTimePass () {
    this.load.generating = true;
    const userDetails = {
      user_type: 'student',
      username: this.viewedStudent['matric_no']
    };
    this.studentService.generateOneTimePass(userDetails).subscribe((passCodeResponse) => {
        this.load.generating = false;
        this.showPasscode = true;
        this.oneTimePassCode = passCodeResponse['pass_code'];
      },
      (error) => {
        this.load.generating = false;
        this.Alert.error('Could not generate pass code!', error);
      });
  }

  public resetPassword () {
    this.load.resetting_password = true;
    this.studentService.resetPassword(this.viewedStudent['matric_no']).subscribe((res) => {
        this.load.resetting_password = false;
        if (res.toString() === 'true') {
          this.Alert.success('Password reset success');
        }
      },
      (error) => {
        this.load.resetting_password = false;
        this.Alert.error('Could not reset password!', error);
      });
  }

  public getSessionSemesters (session_id) {
    this.load.getting_session = true;
    this.studentService.getSingleSession(session_id).subscribe(res => {
      this.load.getting_session = false;
      this.courses.current_session = res;
    }, err => {
      this.load.getting_session = false;
    });
    this.extractRegistration(session_id);
  }

  private extractRegistration(session_id) {
    if (this.viewedStudent.registrations && this.viewedStudent.registrations.length) {
      this.viewedStudent.registrations.forEach(reg => {
        if (reg.session_id.toString() === session_id) {
          this.courses.registration = reg;
        }
      });
    }
  }

  public staffListStudentCourses () {
    // console.log(this.viewedStudent);
    // const reg_id = this.courses.current_session.id;
    console.log(this.courses.registration);
    if (!this.viewedStudent['registrations'].length) {
      return this.Alert.warning('Student is yet to pay');
    }
    this.courses = {
      registered: [],
      registered_ids: [],
      total_unit: 0,
      // reg_id: 0,
      loading: false,
      registering: false,
      current_session: this.courses.current_session,
      registration: this.courses.registration
    };
    this.courses.loading = true;
    this.studentService.staffListStudentCourses(this.courses.registration.id, this.courseRegForm.value['semester']).subscribe(res => {
      this.courses.loading = false;
      this.courses = {...this.courses, ...res};
      this.courses.registered.forEach(registered_course => {
        this.courses.registered_ids.push(registered_course.id);
        this.courses.total_unit += +registered_course.unit;
      });
      // console.log(this.courses);
    }, err => {
      this.courses.loading = false;
      console.log(err);
      this.Alert.error('Could not load courses\n Please retry', err);
    });
  }

  public selectCourse (course, event) {
    // console.log(this.courses);
    const index = this.courses.registered_ids.indexOf(course.id);
    if (index === -1 && event.target.checked) {
      this.courses.registered_ids.push(course.id);
      this.courses.total_unit += +course.unit;
    } else {
      this.courses.total_unit -= +course.unit;
      this.courses.registered_ids.splice(index, 1);
    }
  }

  public unSelectCourse (course, courseIndex, event) {
    if (!event.target.checked) {
      this.courses.to_register.push(this.courses.registered[courseIndex]);
      this.courses.registered.splice(courseIndex, 1);
      this.courses.total_unit -= +course.unit;
      const index = this.courses.registered_ids.indexOf(course.id);
      this.courses.registered_ids.splice(index, 1);
    }
  }

  public registerCourses () {
    this.courses.registering = true;
    if (!this.viewedStudent['registrations'].length) {
      return this.Alert.warning('Student is yet to pay');
    }
    this.studentService.staffRegisterCourses(this.courses.registration.id, this.courseRegForm.value['semester'], this.courses.registered_ids).subscribe(
      (res) => {
        this.courses.registering = false;
        this.Alert.success(res.message || 'Course(s) registered successfully');
        this.staffListStudentCourses();
        // this.courses.registered = res.courses;
        // this.cleanUp(res);
      }, (err) => {
        this.courses.registering = false;
        this.Alert.error('Could not register', err);
      });
  }

  private cleanUp (data) {
    const to_reg = [];
    // console.log('registered', data.courses);
    // console.log('to register', this.courses.to_register);
    let found = true;
    this.courses.to_register.forEach(course => {
      data.courses.forEach(c => {
        if (course.id !== c.id) {
          found = false;
          // to_reg.push(course);
        }
      });
      to_reg.push(course);
    });
    this.courses.to_register = to_reg;
    console.log('registered', data.courses);
    console.log('to register', this.courses.to_register);
    // console.log('after', data.courses, this.courses.to_register);
  }

  public almightyPrint(id, title) {
    const that = this;
    this.load.printing = true;
    const prev_frame = $('#print_frame');
    if (prev_frame) {
      prev_frame.remove();
    }
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['id'] = 'print_frame';
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-1000000px', 'pointer-events': 'none'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    frameDoc.document.write('<html><head><title>Print ' + title + '</title>');
    frameDoc.document.write('</head><body>');
    frameDoc.document.write('<link href="/assets/css/style.bundle.css" rel="stylesheet">');
    frameDoc.document.write('<link href="/assets/css/studentview.css" rel="stylesheet">');
    frameDoc.document.write('<style>.logo-img {max-width: 100%;max-height: 200px;display: block;margin: auto;}</style>');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    frameDoc.onload = function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      // frame1.remove();
      that.load.printing = false;
    }
  }

  private processComputation() {
    let cg = {
      tc: 0,
      twgp: 0
    };
    for (let i = 0; i < this.allViewedResults.length; i++) {
      let session_result = this.allViewedResults[i];
      session_result.semestersData = session_result.semestersData.sort((x, y) => x .semester_info.semester - y.semester_info.semester );
      for (let j = 0; j < session_result.semestersData.length; j++) {
        let semester_result = session_result.semestersData[j];
        if (j > 0) {
          semester_result['current_tc'] = session_result.semestersData[j - 1].current_tc + this.computeStuff(semester_result.result, 'cu', true);
          semester_result['current_twgp'] = session_result.semestersData[j - 1].current_twgp + this.computeStuff(semester_result.result, 'cp', true);
        } else {
          semester_result['current_tc'] = this.computeStuff(semester_result.result, 'cu') + cg.tc;
          semester_result['current_twgp'] = this.computeStuff(semester_result.result, 'cp') + cg.twgp;
        }
        if (j === session_result.semestersData.length - 1) {
          cg.tc = semester_result['current_tc'];
          cg.twgp = semester_result['current_twgp'];
        }
      }
    }
    // console.info(this.allViewedResults);
  }

  public computeStuff(results: any[], stuff, t?): number {
    let accumulator = 0;
    for (let k = 0; k <= results.length; k++) {
      if (results[k]) {
        accumulator += +results[k][stuff];
      }
    }

    return accumulator;
    /*return results.reduce(function (a, b) {

      return a + parseInt(b[stuff], 10);
    }, 0);*/
  }

  public semesterHasResult(results: any[]): boolean {
    if (results.length) {
      if (results && results[0] && results[0].id) {
        if (results && results[1] && results[1].id) {
          if (results && results[2] && results[2].id) {
            return true;
          } else
          return false;
        } else
        return false;
      } else
      return false;
    } else
    return false;
  }

  /**
   * Process the grading from the student object
   * @param responseData
   */
  private computeGrading(responseData) {
    const grading = {
      degree_class: [],
      grades: []
    };
    for (const key in responseData.cgpa_config.degree_class) {
      responseData.cgpa_config.degree_class[key][0] = parseFloat(responseData.cgpa_config.degree_class[key][0]).toFixed(2);
      responseData.cgpa_config.degree_class[key][1] = parseFloat(responseData.cgpa_config.degree_class[key][1]).toFixed(2);
      grading['degree_class'].push({key: key, value: responseData.cgpa_config.degree_class[key]});
      // console.log({key: key, value: grading.degree_class[key]});
    }
    for (const key in responseData.cgpa_config.grades) {
      grading['grades'].push({key: key, value: responseData.cgpa_config.grades[key]});
      // console.log({key: key, value: grading.degree_class[key]});
    }
    this.viewedStudent['grading'] = grading;
  }
}

