import {Component, OnInit, ViewChild} from "@angular/core";
import {AdministationServiceService} from "../../../../../services/api-service/administation-service.service";
import {NotificationService} from "../../../../../services/notification.service";
import {UserService} from "../../../../../services/user.service";
import {Subject} from "rxjs/Subject";
import {DataTableDirective} from 'angular-datatables';

declare const $: any;

@Component({
  selector: 'app-exeat-permission',
  templateUrl: './exeat-permission.component.html',
  styleUrls: ['./exeat-permission.component.css']
})
export class ExeatPermissionComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  //noinspection TypeScriptValidateTypes
  dtTrigger: Subject<any> = new Subject();
  public allExeatRequests: any[] = [];
  selectedExeatRequest;
  staff_id;
  fetching = false;
  loading = false;

  constructor(private administration: AdministationServiceService,
              private notification: NotificationService,
              private userService: UserService) {
  }

  ngOnInit() {
    console.log('Auth User', this.userService.getAuthUser());
    this.staff_id = this.userService.getAuthUser()['login']['id'];
    this.getAllExeatRequests();
  }

  getAllExeatRequests() {
    this.fetching = true;
    this.administration.getAllExeatRequests()
        .subscribe(response => {
            this.fetching = false;
              console.log(response);
              this.allExeatRequests = response['data'];
              if (this.allExeatRequests.length > 0) {
                this.dtTrigger.next();
              }
            },
            error => {
              this.fetching = false;
              this.notification.error('Could not load Exeat Requests');
              console.log('Exeat Request error', error);
            });
  }

  acceptRequest(ind) {
    this.loading = true;
    this.selectedExeatRequest = this.allExeatRequests[ind];
      this.allExeatRequests[ind].requestloading = true;
    this.administration.approveExeatRequest(this.selectedExeatRequest.id)
        .subscribe(response => {
              this.loading = false;
                this.allExeatRequests[ind].requestloading = false;

                this.notification.success('Successful');
              this.rerender();
              console.log('Response of success', response)
            },
            error => {
              this.loading = false;
                this.allExeatRequests[ind].requestloading = false;

                this.notification.error('Failed, try again');
              console.log('Response of error', error)
            })
  }

  declineRequest(ind) {
    console.log(ind);
    this.loading = true;
    this.selectedExeatRequest = this.allExeatRequests[ind];
      this.allExeatRequests[ind].requestloading = true;
    console.log(this.selectedExeatRequest);
    this.administration.declineExeatRequest(this.selectedExeatRequest.id)
        .subscribe(response => {
              this.loading = false;
                this.allExeatRequests[ind].requestloading = false;
              this.notification.success('Successful');
              this.rerender();
              console.log('Response of success', response)
            },
            error => {
              this.loading = false;
                this.allExeatRequests[ind].requestloading = false;
              this.notification.error('Failed, try again');
              console.log('Response of error', error)
            })
  }

  viewExeat(exeatIndex){
    this.selectedExeatRequest = this.allExeatRequests[exeatIndex];
    this.triggerModalOrOverlay('open', 'viewModal');
  }

  rerender(): void {
    this.allExeatRequests = [];
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      // this.dtTrigger.next();
      this.getAllExeatRequests();
    });
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }

}
