import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterviewComponent } from './interview/interview.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {RoleGuardService} from '../../../../../services/role-guard.service';
import {StaffDashboardComponent} from '../dashboard-component.component';
import { VerifyComponent } from './verify/verify.component';
import { InterviewedApplicantComponent } from './interviewed-applicant/interviewed-applicant.component';

const ROUTINGS: Routes = [
  {
    path: 'interview',
    component: StaffDashboardComponent,
    canActivate: [RoleGuardService],
    children: [
      {path: 'invite', component: InterviewComponent},
      {path: 'scheduled', component: VerifyComponent},
      {path: 'interviewed', component: InterviewedApplicantComponent},
    ]
  }
];
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(ROUTINGS)
  ],
  declarations: [InterviewComponent, VerifyComponent, InterviewedApplicantComponent]
})
export class InterviewModule { }
