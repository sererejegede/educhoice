import {Component, OnInit} from '@angular/core';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

declare const $;

@Component({
  selector: 'app-interviewed-applicant',
  templateUrl: './interviewed-applicant.component.html',
  styleUrls: ['./interviewed-applicant.component.css']
})
export class InterviewedApplicantComponent implements OnInit {
  public createForm: FormGroup;
  public viewedPutmeCandidate;
  public allCourses:any[]=[];
  public allLevels:any[]=[];
  public feedBack = {
    allResponse: [],
    applicantsArray: [],
    moduleName: 'Interviewed Applicants',
    loader: false,
    submitStatus: false
  };

  public static formData = function () {
    return {
      email: ['', Validators.compose([Validators.required])],
    };
  };

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.get_all_interviewed_applicants();
  }

  public get_all_interviewed_applicants() {
    this.feedBack.loader = true;
    this.staffConfigService.get_all_interviewed_applicants()
      .subscribe(response => {
          console.log(response);
          this.feedBack.allResponse = response;
          this.feedBack.loader = false;
        },
        error => {
          this.feedBack.loader = false;
          this.notification.error('Unable Load Applicants');
        });
  }

  onSelectCourse(id) {
    const index = this.feedBack.applicantsArray.indexOf(id);
    if (index === -1) {
      this.feedBack.applicantsArray.push(id);
    } else {
      this.feedBack.applicantsArray.splice(index, 1);
    }
  }

  postVerify() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.postVerify(this.createForm.value.email)
      .subscribe(response => {
          console.log('response => ', response);
          this.viewedPutmeCandidate = response;
          $('#openInvite').modal('hide');
          setTimeout(()=>{
            $('#openApplication').modal('show');
          },2000);

          this.createForm.reset();
          this.feedBack.submitStatus = false;
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to  post Invite');
        });
  }

  onView(){
    $('#openApplication').modal('show');
  }

}
