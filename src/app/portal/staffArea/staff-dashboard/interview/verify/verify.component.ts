import {Component, OnInit} from '@angular/core';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

declare const $;

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.css']
})
export class VerifyComponent implements OnInit {
  public createForm: FormGroup;
  public id: number;
  public createFormAfter: FormGroup;
  public viewedPutmeCandidate;
  public feedBack = {
    allResponse: [],
    applicantsArray: [],
    moduleName: 'Scheduled Applicants',
    loader: false,
    submitStatus: false,
    allDepartments: [],
    levelList: [],
    programmeList: [],
    facultyList: [],
    departmentList: [],
    courseList: [],
    sessionList: [],
    loadDepartment: false,
    loadCourse: false,
    checkEmptyLevel: false,
    checkEmptyDepartment: false,
    checkEmptyCourse: false,
    loadLevel: false,
    formType: 'Create',
    showUpdateButton: false
  };

  public static formData = function () {
    return {
      email: ['', Validators.compose([Validators.required])],
    };
  };

  public static formDataAfter = function () {
    return {
      comment: ['', Validators.compose([Validators.required])],
      status: ['', Validators.compose([])],
      course_of_study_id: ['', Validators.compose([])],
    };
  };

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private fb: FormBuilder) {
    this.createForm = this.fb.group(VerifyComponent.formData());
    this.createFormAfter = this.fb.group(VerifyComponent.formDataAfter());
  }

  ngOnInit() {
    this.allFaculty();
    this.getDepartment();
    this.allProgramme();
    this.get_all_scheduled_applicants();
  }

  public get_all_scheduled_applicants() {
    this.feedBack.loader = true;
    this.staffConfigService.get_all_scheduled_applicants()
      .subscribe(response => {
          console.log("All scheduled applicants ",response);
          this.feedBack.allResponse = response;
          this.feedBack.loader = false;
        },
        error => {
          this.feedBack.loader = false;
          this.notification.error('Unable Load Applicants');
        });
  }

  onSelectCourse(id) {
    const index = this.feedBack.applicantsArray.indexOf(id);
    if (index === -1) {
      this.feedBack.applicantsArray.push(id);
    } else {
      this.feedBack.applicantsArray.splice(index, 1);
    }
  }

  postVerify() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.postVerify(this.createForm.value.email)
      .subscribe(response => {
          console.log('response =>fgcghcghg ', response);
          this.viewedPutmeCandidate = response;
          (response.name)?this.notification.success(`${response.name} verified!`):this.notification.error(`${this.createForm.value.email} not verified!`)
          $('#openInvite').modal('hide');
          setTimeout(() => {
            $('#openApplication').modal('show');
          }, 1000);

          this.createForm.reset();
          this.feedBack.submitStatus = false;
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to  post Invite');
        });
  }

  after_interview() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.after_interview(this.id, this.createFormAfter.value.course_of_study_id,
      this.createFormAfter.value.status, {'comment': this.createFormAfter.value.comment} )
      .subscribe(response => {
          console.log('response => ', response);
          this.notification.success('FeedBack was successfully submitted');
          this.feedBack.allResponse.forEach((val, i) => {
            if (val.id === this.id) {
              this.feedBack.allResponse.splice(i, 1);
            }
          });
          $('#openAfter').modal('hide');
          this.createFormAfter.reset();
          this.feedBack.submitStatus = false;
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to  post Invite');
        });
  }

  openModal() {
    // this.id = id;
    $('#openInvite').modal('show');
  }

  /**
   * listing all faculties
   */
  public allFaculty() {
    this.staffConfigService.getFacultyList()
      .subscribe((response) => {
          this.feedBack.facultyList = response.data;
        },
        error => {
          this.notification.error('Unable to load faculties, please retry');
        });
  }

  /**
   * getting a particular department
   */
  public getFaculty(id) {
    this.feedBack.loadDepartment = true;
    this.staffConfigService.getFaculty(id)
      .subscribe((response) => {
          this.feedBack.checkEmptyDepartment = true;
          this.feedBack.loadDepartment = false;
          this.feedBack.departmentList = response.departments;
        },
        error => {
          this.feedBack.loadDepartment = false;
          this.notification.error('Unable to load deparmtent, please retry');
        });
  }

  /**
   * getting a particular course
   */
  public getCourse(id) {
    this.feedBack.loadCourse = true;
    this.staffConfigService.getDepartment(id)
      .subscribe((response) => {
          this.feedBack.checkEmptyCourse = true;
          this.feedBack.courseList = response.course_of_studies;
          this.feedBack.loadCourse = false;
        },
        error => {
          this.feedBack.checkEmptyCourse = true;
          this.feedBack.loadCourse = false;
          this.notification.error('Unable to load deparmtent, please retry');
        });
  }

  /**
   * listing all programme
   */
  public allProgramme() {
    this.staffConfigService.getAllProgramme()
      .subscribe((response) => {
          this.feedBack.programmeList = response.data;
        },
        error => {
          this.notification.error('Unable to load programme, please retry');
        });
  }


  /**
   * editting data
   */
  onEdit(data) {
    this.extracted_View_Update_info(data);
    this.feedBack.showUpdateButton = true;
    // this.createForm = this.fb.group(this.feedBack.viewDetails);
    this.feedBack.formType = 'Update';
    $('#openModal').modal('show');
  }

  /**
   * FOR Viewing details
   */
  onView(data) {
    this.extracted_View_Update_info(data);
    $('#viewModal').modal('show');
  }

  /**
   * listing all department
   */
  public getDepartment() {
    this.staffConfigService.getDepartmentList()
      .subscribe((response) => {
          this.feedBack.allDepartments = response;
          console.log(' faculty:: ', response.data);
        },
        error => {
          this.notification.error('Unable to load faculties, please retry');
        });
  }


  private extracted_View_Update_info(data) {
    this.extractDepartmentById(data);

  }

  /*extractProgrammeById(id) {
    this.feedBack.programmeList.forEach((val) => {
      if (val.id === parseInt(id)) {
        this.feedBack.viewDetails['programme'] = val;
        this.feedBack.viewDetails['programme_id'] = val.id;
      }
    });
  }
*/
  extractDepartmentById(data) {
    const id = +data['course_of_study'].department_id;
    this.feedBack.allDepartments['data'].forEach((val) => {
      if (val.id === id) {
        // this.feedBack.viewDetails['department'] = val;
        // this.feedBack.viewDetails['department_id'] = val.id;
        this.extractFacultyById(val.faculty_id);
      }
    });
  }

  extractFacultyById(id) {
    this.feedBack.facultyList.forEach((val) => {
      if (val.id === +id) {
       //  this.feedBack.viewDetails['faculty'] = val;
       //  this.feedBack.viewDetails['faculty_id'] = val.id;
      }
    });
  }


  /**
   * close modal
   */
  closeModal() {
    $('#openModal').modal('hide');
    this.feedBack.formType = 'Create';
    this.feedBack.showUpdateButton = false;
  }


  /**
   * get the department id
   * @param {Event} $event
   */
  extractId(event) {
    this.feedBack.checkEmptyDepartment = false;
    this.feedBack.checkEmptyCourse = false;
    this.feedBack.departmentList = [];
     this.feedBack.courseList = [];
    const id = event.target.value;
    this.getFaculty(id);
  }

  extractProgrammeId(event) {
    this.feedBack.checkEmptyLevel = false;

    this.feedBack.sessionList = [];
    this.feedBack.levelList = [];
    const id = event.target.value;
   // this.getProgramme(id);
  }

  extractCourseId(event) {
    this.feedBack.checkEmptyCourse = false;
    this.feedBack.courseList = [];
    const id = event.target.value;
    this.getCourse(id);
  }


  openAfter(id) {
    this.id = id;
    $('#openAfter').modal('show');
  }
}
