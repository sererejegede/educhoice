import {Component, OnInit} from '@angular/core';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

declare const $;

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.css']
})
export class InterviewComponent implements OnInit {
  public createForm: FormGroup;
  public feedBack = {
    allResponse: [],
    applicantsArray: [],
    moduleName: 'Applicants',
    loader: false,
    submitStatus: false
  };

  public static formData = function () {
    return {
      date: ['', Validators.compose([Validators.required])],
      time: ['', Validators.compose([Validators.required])],
      venue: ['', Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])],
    };
  };

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private fb: FormBuilder) {
    this.createForm = this.fb.group(InterviewComponent.formData());
  }

  ngOnInit() {
    this.getInterviewapplicant();
  }

  public getInterviewapplicant() {
    this.feedBack.loader = true;
    this.staffConfigService.getApplicantInterview()
      .subscribe(response => {
          console.log(response);
          this.feedBack.allResponse = response;
          this.feedBack.loader = false;
        },
        error => {
          this.feedBack.loader = false;
          this.notification.error('Unable Load Applicants');
        });
  }

  onSelectCourse(id) {
    const index = this.feedBack.applicantsArray.indexOf(id);
    if (index === -1) {
      this.feedBack.applicantsArray.push(id);
    } else {
      this.feedBack.applicantsArray.splice(index, 1);
    }
  }

  postInvite() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.postInvite(this.createForm.value.date, this.createForm.value.time,
      this.createForm.value.venue, this.createForm.value.type, {'applicant_ids': this.feedBack.applicantsArray})
      .subscribe(response => {
          this.feedBack.allResponse.forEach((val, i) => {
            this.feedBack.applicantsArray.forEach((applicant_id, applicant_i) => {
              if (val.id === applicant_id) {
                this.feedBack.allResponse.splice(i, 1);
              }
            });
          });
          this.createForm.reset();
          $('#openInvite').modal('hide');
          this.feedBack.submitStatus = false;
          this.notification.success('Invite was successfully sent');
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to  post Invite');
        });
  }

  openModal() {
    $('#openInvite').modal('show');
  }
}
