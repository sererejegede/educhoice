import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {DataTableDirective} from "angular-datatables";
import {Subject} from "rxjs/Rx";
import * as magicMethods from "../../../../../utils/magic-methods";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import * as utils from "../../../../../utils/util";
import {Department} from "../../../../../interfaces/department.interface";
import {Course} from "../../../../../interfaces/course.interface";
import {SchoolService} from "../../../../../services/school.service";
import {UserService} from "../../../../../services/user.service";
import {StaffConfigService} from "../../../../../services/api-service/staff-config.service";
import {NotificationService} from "../../../../../services/notification.service";


declare const $: any;

@Component({
  selector: 'app-staff-component',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit {

  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  //noinspection TypeScriptValidateTypes
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  private tableWasOnceRendered = false;
  public id = 0;
  public semesterId = 0;
  public staffId = 0;
  public overlay = utils.overlay;
  public allStaff: any[] = [];
  public updatedStaff: any;
  public allGroups: any[] = [];
  public allDepartments: Department[] = [];
  public allStaffTypes: any[] = [
    {
      name: 'Academic',
      value: '1'
    },
    {
      name: 'Non - Academic',
      value: '0'
    }

  ];
  public allCourses: Course[] = [];
  public allStates: any[] = [];
  public allCountries: any[] = [];
  public allActiveHours: any[] = [];
  public authenticatedUser: any;
  public myStaffs: any[] = [];
  public myEditedStaff: any = {};
  public createStaffForm: FormGroup;
  public updateStaffForm: FormGroup;
  public assignDepartmentForm: FormGroup;
  public assignCourseForm: FormGroup;
  public staffFilterForm: FormGroup;
  public editedIndex: number;
  public loadStaff = false;
  public deleteStatus = false;
  public staffDetails;
  public curriculumCourseList = [];
  public semester = '';
  public showSession = false;
  public showCurriculum = false;
  public checkEmptyCurriculum = true;


  public facultyList = [];
  public departmentList = [];
  public loadDepartment = false;
  public checkEmptyDepartment = false;
  public assignLoader = false;
  public hideAssignmentStatus = false;
  public hideCourseStatus = false;
  public loadLevel = false;
  public checkEmptyLevel = false;
  public levelList = [];
  public programmeList = [];
  public sessionList = [];
  public checkEmptyCourse = false;
  public loadCourse = false;
  public courseList = [];
  public course_curriculumList = [];
  public load = {
    requesting: {
      list: false,
      create: false
    },
    message: {
      create: 'Create',
      update: 'Update'
    },
    departments: {
      list: false,
      loading: false
    },
    courses: {
      list: false,
      loading: false
    },
    Staffs: {
      list: false,
      loading: false
    }

  };

  static assignDeptformData = function () {
    return {
      department_id: ['', Validators.compose([Validators.required])],
      faculty_id: ['', Validators.compose([Validators.required])],
    };
  };

  static assignCourseformData = function () {
    return {
      level_id: ['', Validators.compose([Validators.required])],
      session_id: ['', Validators.compose([Validators.required])],
      department_id: ['', Validators.compose([Validators.required])],
      course_of_study_id: ['', Validators.compose([Validators.required])],
      curriculum_id: ['', Validators.compose([])],
      curriculum_course_id: ['', Validators.compose([Validators.required])],
    };
  };

  static createStaffForm = function () {
    return {
      email: ['', Validators.compose([Validators.required, Validators.email])],
      fullname: ['', Validators.compose([Validators.required])],
      group_id: '',
      employee_id: ['', Validators.compose([Validators.required])],
      active_hour_id: ['', Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])]

    };
    // email,first_name,last_name,course_of_study_id
  };

  static updateStaffForm = function () {
    return {

      email: ['', Validators.compose([Validators.required, Validators.email])],
      fullname: ['', Validators.compose([Validators.required])],
      group_id: '',
      employee_id: ['', Validators.compose([Validators.required])],
      active_hour_id: ['', Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])],

      // department_id: ['', Validators.compose([Validators.required])],
      // course_of_study_id: ['', Validators.compose([Validators.required])],
      // country_id: ['', Validators.compose([Validators.required])],
      // state_id: '',
      // matric_no: ['', Validators.compose([Validators.required])]
    };
  };

  static staffFilter = function () {
    return {
      type: '',
      faculty_id: '',
      department_id: '',
    };
  };

  constructor(private userService: UserService,
              private schoolService: SchoolService,
              private notification: NotificationService,
              private staffConfigService: StaffConfigService,
              private fb: FormBuilder,
              private Alert: NotificationService) {
    // this.authenticatedUser = this.userService.getAuthUser().login.user;
    this.createStaffForm = this.fb.group(StaffComponent.createStaffForm());
    this.updateStaffForm = this.fb.group(StaffComponent.updateStaffForm());
    this.assignDepartmentForm = this.fb.group(StaffComponent.assignDeptformData());
    this.assignCourseForm = this.fb.group(StaffComponent.assignCourseformData());
    this.staffFilterForm = this.fb.group(StaffComponent.staffFilter());
  }

  ngOnInit() {
    this.allFaculty();
    // this.getAllStaff();
    this.getAllGroups();
    this.getAllActiveHours();
  }


  // ngAfterViewInit() {
  //     this._script.loadScripts('app-staff-component',
  //         ['assets/demo/default/custom/components/datatables/base/html-table.js']);

  // }


  public getAllStaff() {
    this.load.requesting.list = true;
    this.allStaff = [];
    this.schoolService.getAllFilteredStaff(this.staffFilterForm.value)
        .subscribe(response => {
          // console.log('Filtered staff', response);
          this.allStaff = response;
          this.load.requesting.list = false;
          if (this.allStaff.length > 0) {
            if (this.tableWasOnceRendered) {
              // console.log('Was table once rendered?', this.tableWasOnceRendered);
            } else {
              this.dtTrigger.next();
              this.tableWasOnceRendered = true;
            }
          }
        }, error => {
          this.load.requesting.list = false;
          this.Alert.error('An error occurred, could not load staff', error);
          // console.log('Filter staff error', error);
        });
    }



  public createStaff() {
    this.load.requesting.create = true;
    this.load.message.create = 'Creating...';
    this.schoolService.createOrUpdateStaff(this.createStaffForm.value).subscribe(
        (createdStaffResponse) => {
          this.load.message.create = 'Create';
          const staffGroupName = magicMethods.selectWhereId(this.allGroups, 'id', this.createStaffForm.value['group_id'])['name'];
          this.Alert.success(`Your request to create ${this.createStaffForm.value.fullname} as ${staffGroupName} has been sent for authorization\n`);
          this.createStaffForm = this.fb.group(StaffComponent.createStaffForm());
          this.load.requesting.create = false;
          // console.log('All staff responses ss', createdStaffResponse);
          this.triggerModalOrOverlay('close', 'createStaff');
        },
        (error) => {
          this.load.message.create = 'Create';
          this.load.requesting.create = false;
          this.Alert.error(`Could not create ${this.createStaffForm.value.fullname}`, error);
        }
    );
  }

  public updateStaff() {
    this.load.requesting.create = true;
    this.load.message.update = 'Updating...';
    this.schoolService.createOrUpdateStaff(this.updateStaffForm.value, this.updatedStaff.id).subscribe(
        (updatedStaffResponse) => {
          this.load.message.update = 'Update';
          this.updateStaffForm = this.fb.group(StaffComponent.updateStaffForm());
          this.Alert.success(`${this.updateStaffForm.value.fullname} has been sent for authorization\n`);
          this.allStaff[this.editedIndex] = updatedStaffResponse;
          // console.log('Updated staff response ', updatedStaffResponse);
          this.load.requesting.create = false;
          this.triggerModalOrOverlay('close', 'updateStaff');
        },
        (error) => {
          this.load.message.update = 'Update';
          this.load.requesting.create = false;
          this.Alert.error(`Could not update ${this.updateStaffForm.value.first_name}`, error);
        }
    );
  }


  public getAllGroups() {
    this.schoolService.getAllGroups().subscribe(
        (groupsResponse) => {
          this.allGroups = groupsResponse.data;
        },
        (error) => {
          this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
          this.updatedStaff.loading = false;
        }
    );
  }

  /**
   * This method returns a single faculty which contains a list of departments
   */
  public getDepartmentByByFacultyId(facultyId) {
    this.load.departments.list = true;
    this.load.departments.loading = true;

    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
        (departmentResponse) => {
          this.load.departments.loading = false;

          this.allDepartments = departmentResponse.departments;
        },
        (error) => {
          this.load.departments.list = false;
          this.load.departments.loading = false;

          this.Alert.error(`Sorry could not load departments`, error);
        }
    );
  }

  public async getAllCourses() {
    await this.schoolService.getAllCourses().subscribe(
        (allCoursesResponse) => {
          this.allCourses = allCoursesResponse.data;
          this.updatedStaff.loading = false;
        },
        (error) => {
          this.Alert.error('Sorry, could not load school courses', error);
          this.updatedStaff.loading = false;

        });
  }

  public getCourseByDepartmentId(departmentId) {
    this.load.courses.list = true;
    this.load.courses.loading = true;
    this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
        (coursesResponse) => {
          this.load.courses.loading = false;
          this.allCourses = coursesResponse.course_of_studies;
        },
        (error) => {
          this.load.courses.list = false;
          this.load.courses.loading = false;
          this.Alert.error(`Sorry could not load courses`, error);
        }
    );
  }

  public getAllCountries() {
    this.schoolService.getAllCountries().subscribe(
        (countriesResponse) => {
          this.allCountries = countriesResponse;
        },
        (error) => {
          this.Alert.error(`Sorry could not load countries`, error);
        }
    );
  }

  public getStateByCountryId(countryId) {
    this.schoolService.getStateByCountryId(countryId).subscribe(
        (statesResponse) => {
          this.allStates = statesResponse.states;
        },
        (error) => {
          this.Alert.error(`Sorry could not load States`, error);
        }
    );
  }

  public logStaffIn() {
    this.schoolService.logStaffIn().subscribe(
        (loginResponse) => {
          this.userService.setAuthUser(loginResponse.token);

        }
    );
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    if (ind >= 0) {
      this.allStaff[ind].loading = true;
      this.updatedStaff = this.allStaff[ind];
      const StaffManagementObject = {
        fullname: this.updatedStaff.fullname,
        email: this.updatedStaff.email,
        type: this.updatedStaff.type,
        employee_id: this.updatedStaff.employee_id,
        active_hour_id: (this.updatedStaff.group) ? (this.updatedStaff.group.active_hour.id) : null,
        group_id: (this.updatedStaff.group) ? this.updatedStaff.group.id : null
      };
      // console.log('Updated staff ', StaffManagementObject)

      this.updateStaffForm.patchValue(StaffManagementObject);
      this.updatedStaff.loading = false;
    }
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');


  }

  public getAllActiveHours() {
    this.schoolService.getAllActiveHours().subscribe(
        (activeHourResponse) => {
          this.allActiveHours = activeHourResponse.data;
        },
        (error) => {
          this.Alert.error('Could not load active hours', error);
        }
    );
  }

  public getStaff(staff) {
    if (staff['group']) {
      this.id = staff.id;
      this.staffId = staff.id;
      this.loadStaff = true;
      this.schoolService.getStaffById(staff.id)
          .subscribe((response) => {
                this.loadStaff = false;
                this.staffDetails = response;
                $('#myView').modal('show');
              },
              (error) => {
                this.loadStaff = false;
                this.Alert.error('Unable to load staff details');
              });
    } else {
      this.notification.error('Non Academic Staff is not allowed to Assign Course');
    }
  }

  /**
   * getting a particular faculty
   */

  public getFaculty(id) {
    this.loadDepartment = true;
    this.staffConfigService.getFaculty(id)
        .subscribe((response) => {
              this.checkEmptyDepartment = true;
              this.departmentList = response.departments;
              this.loadDepartment = false;
            },
            error => {
              this.checkEmptyDepartment = true;
              this.loadDepartment = false;
              this.Alert.error('Unable to load deparmtent, please retry');
            });
  }

  /**
   * Assignned department
   */
  public assignDepartment() {
    this.assignLoader = true;
    this.assignDepartmentForm.value['staff_id'] = this.id;
    this.schoolService.assignDepartment(this.assignDepartmentForm.value)
        .subscribe((response) => {
              this.assignLoader = false;
              this.hideAssignmentStatus = false;
              this.staffDetails['departments'].unshift(response.department);
              this.Alert.success('Department assignment was  successful');
            },
            error => {
              this.hideAssignmentStatus = false;
              this.assignLoader = false;
              if (error.status === +406 || error.status === +409) {
                this.notification.error(JSON.parse(error._body)['message']);
              } else {
                this.Alert.error('Unable to assign a department, please retry');
              }
            });
  }

  /**
   * getting a particular course
   */
  public getCourse(id) {
    this.loadCourse = true;
    this.staffConfigService.getDepartment(id)
        .subscribe((response) => {
              this.checkEmptyCourse = true;
              this.courseList = response.course_of_studies;
              this.loadCourse = false;
            },
            error => {
              this.checkEmptyCourse = true;
              this.loadCourse = false;
              this.Alert.error('Unable to load deparmtent, please retry');
            });
  }


  /**
   * Assignned department
   */
  public assignCourse() {
    this.assignLoader = true;
    this.assignCourseForm.value['staff_id'] = this.staffId;
    this.schoolService.assignCourse(this.assignCourseForm.value)
        .subscribe((response) => {
              this.assignLoader = false;
              this.hideAssignmentStatus = false;
              this.assignCourseForm.reset();
              this.Alert.success('Course assignment was  successful');
            },
            error => {
              this.hideAssignmentStatus = false;
              this.assignLoader = false;
              this.Alert.error('Unable to assign a department, please retry');
            });
  }

  /**
   * listing all faculties
   */
  public allFaculty() {
    this.staffConfigService.getFacultyList()
        .subscribe((response) => {
              this.facultyList = response.data;
            },
            error => {
              this.Alert.error('Unable to load faculties, please retry');
            });
  }

  /**
   * get the department id
   * @param {Event} $event
   */
  extractId(event: any) {
    this.checkEmptyDepartment = false;
    this.checkEmptyCourse = false;
    this.departmentList = [];
    this.courseList = [];
    const id = event.target.value;
    this.getFaculty(id);
  }


  /**
   * getting a particular level
   */
  public getProgramme(id) {
    this.loadLevel = true;
    this.staffConfigService.getProgramme(id)
        .subscribe((response) => {
              this.checkEmptyLevel = true;
              this.levelList = response.levels;
              this.sessionList = response.sessions;
              this.loadLevel = false;
            },
            error => {
              this.loadLevel = false;
              this.Alert.error('Unable to load level, please retry');
            });
  }

  extractCourseId(event) {
    this.checkEmptyCourse = false;
    this.courseList = [];
    const id = event.target.value;
    this.showCurriculum = false;
    this.checkEmptyCurriculum = true;
    this.course_curriculumList = [];
    this.checkEmptyLevel = false;
    this.showSession = false;
    this.levelList = [];
    this.sessionList = [];
    this.loadLevel = false;
    this.getCourse(id);
  }

  extractProgrammeId(event) {
    this.checkEmptyLevel = false;
    this.sessionList = [];
    this.levelList = [];
    const id = event.target.value;
    this.getProgramme(id);
  }

  openAssignment() {
    this.hideAssignmentStatus = !this.hideAssignmentStatus;
  }

  openCourse() {
    this.hideCourseStatus = !this.hideCourseStatus;
  }

  ongetCurriculum() {
    this.showCurriculum = true;
    this.checkEmptyCurriculum = true;
    this.course_curriculumList = [];
    this.assignCourseForm.value.semester = this.semester;
    const session_id = this.assignCourseForm.value.session_id;
    const semester = this.assignCourseForm.value.semester;
    const level = this.assignCourseForm.value.level_id;
    const course_of_study_id = this.assignCourseForm.value.course_of_study_id;
    if (level !== '' || session_id !== '') {
      this.schoolService.getFilteredCourseOfStudy(course_of_study_id, level, session_id, this.semesterId)
          .subscribe((response) => {
                this.semesterId = 0;
                this.course_curriculumList = response[0]['curriculum_courses'];
                this.showCurriculum = false;
                this.checkEmptyCurriculum = false;
              },
              error => {
                this.checkEmptyCurriculum = false;
                this.showCurriculum = false;
                this.Alert.error('Unable to load course, please retry', error);
              });
    }
  }

  /**
   * deleting assignned department
   * @param id
   */
  ondeleteDepartment(id) {
    this.id = id;
    this.deleteStatus = true;
    this.schoolService.deleteDepartment(id)
        .subscribe((response) => {
          this.staffDetails['departments'].forEach((val, i) => {
                if (val.id === id) {
                  const index = this.staffDetails['departments'].indexOf(this.staffDetails['departments'][i]);
                  this.staffDetails['departments'].splice(index, 1);
                  // delete this.staffDetails['departments'][i];
                  this.deleteStatus = false;
                }
              },
              error => {
                this.deleteStatus = false;
                this.Alert.error('Unable to remove department, please retry', error);
              });
        });
  }


  /**
   * fetching course of study by id
   * @param event
   */
  getCourseById(event) {
    // console.log('cos id :: ', event.target.value)
    this.showCurriculum = false;
    this.checkEmptyCurriculum = true;
    this.course_curriculumList = [];
    this.checkEmptyLevel = false;
    this.showSession = false;
    this.levelList = [];
    this.sessionList = [];
    this.loadLevel = true;
    this.showSession = false;
    const id = event.target.value;
    this.staffConfigService.getCourseOfStudyById(id)
        .subscribe((response) => {
              this.semester = response.degree.programme.semesters;
              this.getProgramme(response.degree.programme_id);
            },
            error => {
              this.loadLevel = false;
              this.Alert.error('Unable to load course of study, please retry', error);
            });
  }


  onSelectCourse_of_study(event) {
    const courseId = event.target.value;
    this.curriculumCourseList = [];
    this.curriculumCourseList = this.course_curriculumList[courseId]['curriculum_courses'];
    /*this.course_curriculumList.forEach((val, id) => {
     if (val.id === courseId) {
     console.log('val curr :: ', val);
     this.curriculumCourseList = val['curriculum_courses'];
     }
     })*/
  }

  openSession() {
    this.showSession = true;
    const session_id = this.assignCourseForm.value.session_id;
    const semester = this.assignCourseForm.value.semester;
    const level = this.assignCourseForm.value.level_id;
    if (level !== '' || session_id !== '') {
      this.ongetCurriculum();
    }
  }

  ongetSemester(event) {
    this.semesterId = event.target.value;
  }

  renderStaff(){
    if (this.tableWasOnceRendered) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
            // Destroy the table first
            dtInstance.destroy();
            this.tableWasOnceRendered = false;
            // Call the dtTrigger to rerender again
          },
          (rejection) => {
            // console.log('Why ah was rejected ', rejection)
          });
    }
    this.getAllStaff();
  }

}
