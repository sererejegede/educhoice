import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationService} from '../../../../../../services/notification.service';
import {StudentServiceService} from '../../../../../../services/api-service/student-service.service';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {SchoolService} from '../../../../../../services/school.service';
import {Subject} from 'rxjs/Subject';

declare const $;


@Component({
  selector: 'app-hostel-allocation',
  templateUrl: './hostel-allocation.component.html',
  styleUrls: ['./hostel-allocation.component.css']
})
export class HostelAllocationComponent implements OnInit {

  dtTrigger: Subject<any> = new Subject();
  public id = 0;
  public hostel_id = 0;
  public feedBack = {
    formType: 'Create',
    emptyBedspace: '',
    emptyBedspaceStatus: false,
    emptyRoomStatus: false,
    emptyRoom: '',
    moduleName: 'Hostel Allocation',
    showUpdateButton: false,
    submitStatus: false,
    loader: false,
    // roomStatus: false,
    allResponse: [],
    allStudents: [],
    availableHostels: [],
    allLevel: [],
    roomspaces: [],
    viewDetails: [],
    bedspaces: []
  };
  public createForm: FormGroup;
  public submitStatus = false;
  static formData = function () {
    return {
      hostel_bed_space_id: ['', Validators.compose([Validators.required])],
      hostel_id: ['', Validators.compose([])],
      hostel_session_id: ['', Validators.compose([Validators.required])],
      student_id: ['', Validators.compose([Validators.required])],
      payment_method: ['', Validators.compose([Validators.required])],
    };
  };


  constructor(private studentServiceService: StudentServiceService,
              private staffConfigService: StaffConfigService,
              private schoolService: SchoolService,
              private notification: NotificationService,
              private fb: FormBuilder,
              private router: Router) {
    this.createForm = this.fb.group(HostelAllocationComponent.formData());
  }

  ngOnInit() {
    this.loadFunction();
  }

  private loadFunction() {
    this.getAllocations();
  }


  /**
   * get all available hostels
   */
  getAllocations() {
    this.feedBack.loader = true;
    this.staffConfigService.getAllocation()
      .subscribe((response) => {
      console.log('allocation response :: ', response);
          this.feedBack.allResponse = response.data;
          this.feedBack.loader = false;
        },
        error => {
          this.feedBack.loader = false;
          this.notification.error('Unable to load available hostel, please reload', error);
        });
  }

  extractId(event) {
    this.id = +event.target.value;
    this.onbookHostel(this.id);
  }

  onbookHostel(id) {
    this.id = id;
    let hostel = [];
    this.feedBack.emptyRoom = '';
    this.feedBack.emptyBedspace = '';
    // this.feedBack.roomStatus = false;
    this.feedBack.emptyBedspaceStatus = false;
    this.feedBack.bedspaces = [];
    this.feedBack.roomspaces = [];
    this.feedBack.availableHostels.forEach(val => {
      if (+val.id === this.id) {
        hostel = val;
      }
    });
    if (hostel['hostel']['rooms'].length > 0) {
      this.feedBack.roomspaces = hostel['hostel']['rooms'];
      this.hostel_id = hostel['hostel_id'];
      this.feedBack.emptyRoomStatus = true;
    } else {
      this.feedBack.emptyRoomStatus = false;
      this.feedBack.emptyRoom = 'There is no Room';
    }
  }

  openModel() {
    this.createForm.reset();
    this.feedBack.emptyBedspaceStatus = false;
    this.feedBack.emptyRoomStatus = false;
    $('#openHostel').modal('show');
  }

  onbedSpace(roomId) {
    this.feedBack.emptyBedspace = '';
    this.feedBack.bedspaces = [];
    this.feedBack.emptyBedspaceStatus = false;
    this.feedBack.roomspaces.forEach(value => {
      if (+value.id === +roomId.target.value) {
        this.feedBack.bedspaces = value['bed_spaces'];
        this.feedBack.emptyBedspaceStatus = true;
        if (this.feedBack.bedspaces.length === 0) {
          this.feedBack.emptyBedspaceStatus = false;
          this.feedBack.emptyBedspace = 'There is no Bed Space';
        }
      }
    });
  }

  createFormModal() {
    this.feedBack.submitStatus = true;
    // this.createForm.value['hostel_id'] = this.hostel_id;
    // console.log('form details :: ', this.createForm.value);
    this.staffConfigService.createHostelAllocation(this.id, this.createForm.value)
      .subscribe(response => {
          this.feedBack.allResponse.unshift(response);
          $('#hostelReservation').modal('show');
          this.notification.success('Hostel Allocation was successful');
          this.feedBack.submitStatus = false;
        },
        error => {
          this.notification.error('Unable to Allocate Hostel, please try again', error);
          this.feedBack.submitStatus = false;
        });
  }

  viewAllocation(details) {
    this.feedBack.viewDetails = details;
    $('#hostelAllocation').modal('show');
  }

  /**
   * editting data
   */
  onEdit(data) {
    this.id = data.id;
    this.feedBack.emptyBedspaceStatus = true;
    this.feedBack.emptyRoomStatus = true;
    this.onbookHostel(+data.hostel_session_id);
    // this.feedBack.roomStatus = true;
    $('#openHostel').modal('show');
    this.feedBack.formType = 'Update';
    this.createForm = this.fb.group(data);
    this.feedBack.showUpdateButton = true;
  }

  /**
   * update degree
   */
  public updateFormModal() {
    this.feedBack.submitStatus = true;
    this.staffConfigService.updateHostelAllocation(this.id, this.createForm.value)
      .subscribe((response) => {
          this.feedBack.allResponse.forEach((val, i) => {
            if (val.id === this.id) {
              this.feedBack.allResponse[i] = response;
              this.notification.success(this.feedBack.moduleName + ' was updated successfully');
            }
          });
        },
        error => {
          this.feedBack.submitStatus = false;
          this.notification.error('Unable to Update ' + this.feedBack.moduleName + ', please retry');
        });
  }
}
