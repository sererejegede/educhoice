import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HostelAllocationComponent } from './hostel-allocation/hostel-allocation.component';
import {ReactiveFormsModule} from "@angular/forms";
import {DataTablesModule} from "angular-datatables";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DataTablesModule
  ],
  declarations: [HostelAllocationComponent]
})
export class HostelAllocationModule { }
