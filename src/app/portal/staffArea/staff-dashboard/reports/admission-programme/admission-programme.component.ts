import {Component, ElementRef, OnInit} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {Cache} from '../../../../../../utils/cache';
import {JSONPARSE} from '../../../../../../utils/json-parse';
import {ActivatedRoute, Router} from '@angular/router';
import {Select2OptionData} from "ng2-select2";

declare const $;


@Component({
  selector: 'app-admission-programme',
  templateUrl: './admission-programme.component.html',
  styleUrls: ['./admission-programme.component.css']
})
export class AdmissionProgrammeComponent implements OnInit {
  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);
  public exampleData: Array<Select2OptionData>;

  public createForm: FormGroup;
  public id: number;
  public feedBack = {
    allResponse: [],
    allProgramme: [],
    viewDetails: [],
    allHostel: [],
    moduleName: '',
    formType: 'Create',
    loader: false,
    submitStatus: false,
    showUpdateButton: false,
    deleteStatus: false
  };


  ngOnInit() {
    this.loadFunction();
  }

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder) {
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
  }

  loadFunction() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getReportByProgrammeId(this.id);
    this.getAllProgrammes();
  }

  /**
   * listing all applications
   */
  public getReportByProgrammeId(id) {
    this.feedBack.loader = true;
    this.staffConfigService.getReportByProgrammeId(id)
      .subscribe((response) => {
          console.log('report by program id ::', response);
          this.feedBack.allResponse = response;
          if (this.feedBack.allResponse.length > 0) {
            this.dtTrigger.next();
          }
          this.feedBack.loader = false;
        },
        error => {
          this.notification.error('Unable to load Programme(s), please retry');
          this.feedBack.loader = false;
        });
  }

  /**
   * get all programmess
   */
  public getAllProgrammes() {
    this.staffConfigService.getAllProgramme()
      .subscribe((response) => {
          this.feedBack.allProgramme = response.data;
        },
        (error) => {
          this.notification.error('Unable to load Students', error);
        }
      );
  }

  viewReservation(details) {
    this.feedBack.viewDetails = details;
    $('#hostelReservation').modal('show');
  }

  navigateToReport() {
    this.router.navigate(['staff/reports/transaction']);
  }

  onSelectDept(event) {
    this.getReportByProgrammeId(event.target.value);
    this.feedBack.allResponse = [];
    this.feedBack.moduleName = '';
  }
}
