import {Component, OnInit, ViewChild} from '@angular/core';
import {ReportServiceService} from "../../../../../../services/api-service/report-service.service";
import {SchoolService} from "../../../../../../services/api-service/school.service";
import {CONSTANTS} from "../../../../../../utils/constants";
import {NotificationService} from "../../../../../../services/notification.service";
import {CSVExportService} from "../../../../../../services/c-sv-export.service";
import {DataTableDirective} from "angular-datatables";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subject} from "../../../../../../../node_modules/rxjs";
import {StaffConfigService} from "../../../../../../services/api-service/staff-config.service";

@Component({
    selector: 'app-fee-item',
    templateUrl: './fee-item.component.html',
    styles: []
})
export class FeeItemComponent implements OnInit {

    @ViewChild(DataTableDirective)
    public dtElement: DataTableDirective;
    public dtTrigger: Subject<any> = new Subject();

    public fee_items: any[] = [];
    public feeReport: any[] = [];
    public sessions: any[] = [];
    public totalFeeAmount: number = 0;
    public totalAmountPaid: number = 0;
    public feedBack = {
        allResponse: [],
        viewDetails: [],
        allSession: [],
        allProgrammes: [],
        programmeLevels: [],
        faculties: [],
        departments: [],
        courses_of_study: [],
        moduleName: '',
        formType: 'Create',
        loader: false,
        excelling: false,
        current_session: null,
        show_filter: true,
        rendered: false,
        status: 'completed'
    };

    public loader = {
        faculty: false,
        department: false,
        courses_of_study: false,
        session: false,
        level: false,
        programme: false,
        loading_fee_items: false,
        session_loader: false,
        loading: false,
        excelling: false,
        rendered: false,
        fee_item_name: ''
    };
    public filterForm: FormGroup;
    public previousLengthWasMoreThanZero: boolean = false;
    private static filterFormControls = () => {
        return {
            programme_id: [1],
            session_year: ['', Validators.required],
            level_id: [''],
            course_of_study_id: [''],
            faculty_id: [''],
            department_id: [''],
            status: ['completed', Validators.required],
            fee_item_id: ['', Validators.required]
        };
    }

    /*
     private static filterFormControls = () => {
     return {
     session_year: ['', Validators.required],
     fee_item_id: ['', Validators.required]
     };
     }*/

    constructor(private staffConfigService: StaffConfigService, private reportService: ReportServiceService,
                private schoolService: SchoolService,
                private notification: NotificationService,
                private fb: FormBuilder,
                private excelService: CSVExportService) {
        this.filterForm = this.fb.group(FeeItemComponent.filterFormControls());
        // this.fb.group(FeeItemComponent.filterFormControls());
    }

    ngOnInit() {
        this.getAllFeeItems();
        this.allSession();
        this.allProgrammes();
        this.programmeById();
        this.allFaculties();
    }

    private getAllSessions() {
        this.loader.session_loader = true;
        this.schoolService.getAllSession().subscribe(
            (res) => {
                this.loader.session_loader = false;
                this.sessions = res;
            }, (err) => {
                this.loader.session_loader = false;
                this.notification.error('Something happened', err);
            });
    }

    private getAllFeeItems() {
        this.loader.loading_fee_items = true;
        this.schoolService.getAllFeeItems(true).subscribe((res) => {
            this.loader.loading_fee_items = false;
            this.fee_items = res;
        }, (err) => {
            this.loader.loading_fee_items = false;
            this.notification.error(CONSTANTS.DEFAULT_ERROR_MESSAGE, err);
        });
    }

    public getFeeItemReport() {
        this.loader.loading = true;
        this.feedBack.rendered = false;
        this.feeReport = [];
        let totalAmountPaid = 0;
        let totalFeeAmount = 0;
        this.reportService.byFeeItem(this.filterForm.value).subscribe((res) => {
            this.loader.loading = false;
            this.feeReport = res;
            this.feeReport.forEach((fee) => {
                totalAmountPaid += +fee['amount'];
                totalFeeAmount += +fee['amount_to_pay'];
            });
            this.feedBack.rendered = true;
            this.totalAmountPaid = totalAmountPaid;
            this.totalFeeAmount = totalFeeAmount;
            this.previousLengthWasMoreThanZero = this.feeReport.length > 0;
            if (this.previousLengthWasMoreThanZero) {
                this.dtTrigger.next();
            }
            this.loader.rendered = true;
        }, (err) => {
            this.loader.loading = false;
            this.feedBack.rendered = false;
            this.notification.error(CONSTANTS.DEFAULT_ERROR_MESSAGE, err);
        });
    }

    public reRender(): void {
        if (this.loader.rendered && this.previousLengthWasMoreThanZero) {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                // Destroy the table first
                dtInstance.destroy();
                // Call the dtTrigger to rerender again
                // this.dtTrigger.next();
                this.getFeeItemReport();
            });
        } else {
            this.getFeeItemReport();
        }
    }

    public exportAsXLSX() {
        this.loader.excelling = true;
        const formatted_data = [];
        this.feeReport.forEach(r => {
            const amount = +r.amount;
            const amount_to_pay = +r.amount_to_pay;
            formatted_data.push({
                'Name': r.student_name,
                'Course of Study': r.course_of_study,
                'Level': r.level,
                'Fee Amount': amount.toFixed(2),
                'Amount Paid': amount_to_pay.toFixed(2),
                'Date Generated': r.created_at,
                'Date Paid': r.updated_at,
            });
        });
        // console.log(formatted_data);
        this.excelService.exportAsExcelFile(formatted_data, this.loader.fee_item_name + ' Report');
        this.loader.excelling = false;
    }

    public setFeeItemName(fee_item_id) {
        this.fee_items.forEach(fee_item => {
            if (fee_item.id.toString() === fee_item_id) {
                this.loader.fee_item_name = fee_item.name;
            }
        });
    }

    public allSession() {
        this.loader.session = true;
        this.staffConfigService.getAllSession()
            .subscribe((response) => {
                    this.loader.session = false;
                    this.feedBack.allSession = response.data;
                },
                error => {
                    this.loader.session = false;
                    this.notification.error('Unable to load Sessions, please retry', error);
                });
    }

    public allProgrammes() {
        this.loader.programme = true;
        this.staffConfigService.getAllProgramme()
            .subscribe((response) => {
                    this.loader.programme = false;
                    this.feedBack.allProgrammes = response.data;
                },
                error => {
                    this.loader.programme = false;
                    this.notification.error('Unable to load Programmes, please retry', error);
                });
    }

    public allFaculties() {
        this.loader.faculty = true;
        this.staffConfigService.getAllFaculty()
            .subscribe((response) => {
                    this.loader.faculty = false;
                    this.feedBack.faculties = response.data;
                },
                error => {
                    this.loader.faculty = false;
                    this.notification.error('Unable to load Faculties, please retry', error);
                });
    }

    public facultyDepartments(faculty_id) {
        this.loader.department = true;
        this.staffConfigService.getFaculty(faculty_id)
            .subscribe((response) => {
                    this.loader.department = false;
                    if (response.departments) {
                        this.feedBack.departments = response.departments;
                    }
                },
                error => {
                    this.loader.department = false;
                    this.notification.error('Unable to load Departments, please retry', error);
                });
    }

    public departmentCoursesOfStudy(department_id) {
        this.loader.courses_of_study = true;
        this.staffConfigService.getCourseOfStudyById(department_id)
            .subscribe((response) => {
                    this.loader.courses_of_study = false;
                    if (response.course_of_studies) {
                        this.feedBack.courses_of_study = response.course_of_studies;
                    }
                },
                error => {
                    this.loader.courses_of_study = false;
                    this.notification.error('Unable to load Courses of study, please retry', error);
                });
    }

    public programmeById(id = 1) {
        this.loader.level = true;
        this.staffConfigService.getProgramme(id)
            .subscribe((response) => {
                    this.loader.level = false;
                    this.feedBack.programmeLevels = response.levels;
                },
                error => {
                    this.loader.level = false;
                    this.notification.error('Unable to load Programmes, please retry', error);
                });
    }

}
