import {Component, ElementRef, OnInit} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {Cache} from '../../../../../../utils/cache';
import {JSONPARSE} from '../../../../../../utils/json-parse';
import {ActivatedRoute, Router} from '@angular/router';

declare const $;
@Component({
  selector: 'app-course-course-of-study',
  templateUrl: './course-course-of-study.component.html',
  styleUrls: ['./course-course-of-study.component.css']
})
export class CourseCourseOfStudyComponent implements OnInit {
  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);


  public createForm: FormGroup;
  public id: number;
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    allDepartments: [],
    allHostel: [],
    moduleName: '',
    formType: 'Create',
    loader: false,
    submitStatus: false,
    loadDepartment: false,
    showUpdateButton: false,
    deleteStatus: false
  };


  ngOnInit() {
    this.loadFunction();
  }

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder) {
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
  }

  loadFunction() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getReportByCourseOfStudyId(this.id);
    this.allDepartment();
  }

  /**
   * listing all applications
   */
  public getReportByCourseOfStudyId(id) {
    this.feedBack.loader = true;
    this.staffConfigService.getReportByCOSDepartmentId(id)
      .subscribe((response) => {
          console.log('report by cos id ::', response);
           this.feedBack.allResponse = response;
          if (this.feedBack.allResponse.length > 0) {
            this.feedBack.moduleName = ' / Course of Study / ' + response[0]['department']['name'];
              this.dtTrigger.next();
          }
          this.feedBack.loader = false;
        },
        error => {
          this.notification.error('Unable to load Current Session, please retry');
          this.feedBack.loader = false;
        });
  }

  /**
   * listing all departments
   */
  public allDepartment() {
    this.feedBack.loadDepartment = true;
    this.staffConfigService.getAllDepartment()
      .subscribe((response) => {
          this.feedBack.allDepartments = response.data;
          this.feedBack.loadDepartment = false;
        },
        error => {
          this.notification.error('Unable to load departments please retry');
          this.feedBack.loadDepartment = false;
        });
  }

  viewReservation(details) {
    this.feedBack.viewDetails = details;
    $('#hostelReservation').modal('show');
  }

  navigateToReport() {
    this.router.navigate(['staff/reports/transaction']);
  }

  onSelectDept(event) {
    this.getReportByCourseOfStudyId(event.target.value);
    this.feedBack.allResponse = [];
    this.feedBack.moduleName = '';
  }
}
