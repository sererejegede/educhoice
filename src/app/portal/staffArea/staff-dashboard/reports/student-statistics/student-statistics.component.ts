import { Component, OnInit } from '@angular/core';
import {SchoolService} from "../../../../../../services/api-service/school.service";
import {ReportServiceService} from "../../../../../../services/api-service/report-service.service";
import {NotificationService} from "../../../../../../services/notification.service";
import {Cache} from "../../../../../../utils/cache";
import {CONSTANTS} from "../../../../../../utils/constants";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-student-statistics',
  templateUrl: './student-statistics.component.html',
  styleUrls: ['./student-statistics.component.css']
})
export class StudentStatisticsComponent implements OnInit {

  public appSettings: any;
  public studentStats: any;
  // public studentStats = {
  //   "session_year": 2018,
  //   "semester_name": "First Semester",
  //   "data": [
  //     {
  //       "faculty_name": "FACULTY OF AGRICULTURE",
  //       "courses_of_study": [
  //         {
  //           "name": "Crop Production & Soil Management",
  //           "levels": [
  //             {
  //               "name": "100L(Year 1)",
  //               "male": 12,
  //               "female": 9
  //             },
  //             {
  //               "name": "200L(Year 2)",
  //               "male": 10,
  //               "female": 11
  //             }
  //           ]
  //         },
  //         {
  //           "name": "Forestry And Environmental Technology",
  //           "levels": [
  //             {
  //               "name": "100L(Year 1)",
  //               "male": 12,
  //               "female": 9
  //             },
  //             {
  //               "name": "200L(Year 2)",
  //               "male": 10,
  //               "female": 11
  //             }
  //           ]
  //         }
  //       ]
  //     },
  //     {
  //       "faculty_name": "FACULTY OF EDUCATION",
  //       "courses_of_study": [
  //         {
  //           "name": "English Language(Ed)",
  //           "levels": [
  //             {
  //               "name": "100L(Year 1)",
  //               "male": 12,
  //               "female": 9
  //             },
  //             {
  //               "name": "200L(Year 2)",
  //               "male": 10,
  //               "female": 11
  //             }
  //           ]
  //         },
  //         {
  //           "name": "Chemistry (Ed)",
  //           "levels": [
  //             {
  //               "name": "100L(Year 1)",
  //               "male": 12,
  //               "female": 9
  //             },
  //             {
  //               "name": "200L(Year 2)",
  //               "male": 10,
  //               "female": 11
  //             }
  //           ]
  //         }
  //       ]
  //     }
  //   ]
  // };
  public sessions: any[] = [];
  public semesters: any[] = [];
  public form_data = {
    session_year: '',
    session_name: '',
    semester_name: '',
    semester: ''
  };
  public feedBack = {
    loader: false,
    session_loader: false,
    semester_loader: false,
    print_loader: false
  };
  constructor(private notification: NotificationService,
              private reportService: ReportServiceService,
              private schoolService: SchoolService,) { }

  ngOnInit() {
    this.appSettings = Cache.get('app_settings');
    this.getAllSession();
    // this.getStudentStatistics();
  }

  private getAllSession() {
    this.feedBack.session_loader = true;
    this.schoolService.getAllSession().subscribe(
      (res) => {
        this.feedBack.session_loader = false;
        this.sessions = res;
      }, (err) => {
        this.feedBack.session_loader = false;
        this.notification.error('Something happened', err);
      });
  }

  public getSessionSemesters(session_year) {
    this.feedBack.semester_loader = true;
    let session_id = 0;
    this.sessions.forEach(session => {
      if(session.year.toString() === session_year) {
        session_id = session.id;
        this.form_data.session_name = session ? session.name : null;
      }
    });
    this.schoolService.getSingleSession(session_id).subscribe((response) => {
      this.feedBack.semester_loader = false;
      this.semesters = response['session_semesters'];
    }, (error) => {
      this.feedBack.semester_loader = false;
      // this.notification.error('Could not load Semesters. Please retry', error);
    });
  }

  public setSemester(semester) {
    this.semesters.forEach(sem => {
      if (sem.semester.toString() === semester) {
        this.form_data.semester_name = sem ? sem.name : null;
      }
    })
  }

  public getStudentStatistics() {
    this.studentStats = null;
    this.feedBack.loader = true;
    this.reportService.studentStatistics(this.form_data).subscribe((res) => {
      this.feedBack.loader = false;
      this.studentStats = res;
      this.studentStats.data.forEach(faculty => {
        let sub_total = 0;
        faculty.course_of_studies.forEach(cos => {
          sub_total += this.computeTotal(cos.levels);
        });
        faculty['sub_total'] = sub_total;
      });
    }, (err) => {
      this.feedBack.loader = false;
      this.notification.error(CONSTANTS.DEFAULT_ERROR_MESSAGE, err);
    });
  }

  public computeTotal(iterables: any[], gender?: string, year?: number) {
    let total = 0;
    if (gender) {
      iterables.forEach(iterable => {
        if (gender === 'both') {
          total += iterable.levels[year]['male'] + iterable.levels[year]['female'];
          return total;
        }
        total += iterable.levels[year][gender];
      });
      return total;
    }
    iterables.forEach(iterable => {
      total += iterable.male + iterable.female;
      // total += level.female;
    });
    return total;
  }

  public printNau(id, title) {
    this.feedBack.print_loader = true;
    const that = this;
    // Internet Explorer 6-11
    const isIE = /*@cc_on!@*/false || !!document['documentMode'];
    // Edge 20+
    const isEdge = !isIE && !!window['StyleMedia'];
    // Chrome 1 - 71
    const isChrome = !!window['chrome'] && (!!window['chrome'].webstore || !!window['chrome'].runtime);
    console.info('Chrome', isChrome);
    console.info('Edge', isEdge);
    console.info('IE', isIE);
    const prev_frame = $('#print_frame');
    if (prev_frame) {
      prev_frame.remove();
    }
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['id'] = 'print_frame';
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-10000px', 'pointer-events': 'none'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    frameDoc.document.write('<html><head><title>Print ' + title + '</title>');
    frameDoc.document.write('</head><body>');
    frameDoc.document.write('<link href="/assets/css/print.css" rel="stylesheet">');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    frameDoc.onload = function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      // frame1.remove();
      that.feedBack.print_loader = false;
    }
  }

}
