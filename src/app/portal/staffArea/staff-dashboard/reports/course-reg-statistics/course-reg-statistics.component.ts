import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {DataTableDirective} from "angular-datatables";
import {Subject} from "../../../../../../../node_modules/rxjs";
import {SchoolService} from "../../../../../../services/api-service/school.service";
import {ReportServiceService} from "../../../../../../services/api-service/report-service.service";
import {NotificationService} from "../../../../../../services/notification.service";
import {Cache} from "../../../../../../utils/cache";

@Component({
  selector: 'app-course-reg-statistics',
  templateUrl: './course-reg-statistics.component.html',
  styleUrls: ['./course-reg-statistics.component.css']
})
export class CourseRegStatisticsComponent implements OnInit {

  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  //noinspection TypeScriptValidateTypes
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  public appSettings: any;
  public courseRegStats: any[] = [];
  public faculties: any[] = [];
  public departments: any[] = [];
  public coursesOfStudy: any[] = [];
  public levels: any[] = [];
  public sessions: any[] = [];
  public semesters: any[] = [];
  public filterForm: FormGroup;
  public statDetails = {
    faculty: null,
    department: null,
    course_of_study: null,
    session: null,
    semester: null,
  };
  public feedBack = {
    loader: false,
    excelling: false,
    session_loader: false,
    faculty_loader: false,
    department_loader: false,
    course_loader: false,
    level_loader: false,
    semester_loader: false,
    print_loader: false,
    rendered: false
  };
  private static filter = () => {
    return {
      // programme_id: [1],
      session_year: ['', Validators.required],
      semester: [''],
      level_id: [''],
      // type: [''],
      faculty_id: [''],
      department_id: [''],
      course_of_study_id: [''],
      // course_code: ['']
    };
  }

  constructor(private fb: FormBuilder,
              private notification: NotificationService,
              private reportService: ReportServiceService,
              private schoolService: SchoolService,) {
    this.filterForm = this.fb.group(CourseRegStatisticsComponent.filter());
  }

  ngOnInit() {
    this.getAllSession();
    this.getAllFaculties();
    this.appSettings = Cache.get('app_settings');
    this.getAllSession();
  }

  private getAllSession() {
    this.feedBack.session_loader = true;
    this.schoolService.getAllSession().subscribe(
      (res) => {
        this.feedBack.session_loader = false;
        this.sessions = res;
      }, (err) => {
        this.feedBack.session_loader = false;
        this.notification.error('Something happened', err);
      });
  }

  private getAllFaculties() {
    this.feedBack.faculty_loader = true;
    this.schoolService.getAllFaculties().subscribe(
      (facultiesResponse) => {
        this.feedBack.faculty_loader = false;
        this.faculties = facultiesResponse;
      },
      (error) => {
        this.feedBack.faculty_loader = false;
        this.notification.error('Sorry, could not load faculties. Please reload page.', error);
      },
    );
  }

  public getDepartmentByByFacultyId(facultyId) {
    this.feedBack.department_loader = true;
    this.statDetails.faculty = facultyId ? this.selectWhereId(this.faculties, 'id', facultyId).name : null;
    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
      (departmentResponse) => {
        this.feedBack.department_loader = false;
        this.departments = departmentResponse.departments;
      },
      (error) => {
        this.feedBack.department_loader = false;

        // this.notification.error(`Sorry could not load departments`, error);
      });
  }

  public getCourseOfStudy($event) {
    this.feedBack.course_loader = true;
    this.statDetails.department = $event ? this.selectWhereId(this.departments, 'id', $event).name : null;
    this.schoolService.getCourseByDepartmentId($event).subscribe(response => {
          this.feedBack.course_loader = false;
          this.coursesOfStudy = response['course_of_studies'];
        },
        error => {
          this.feedBack.course_loader = false;
          // this.notification.error('Course of Study could not be loaded', error);
        }
      );
  }

  public getSingleCourseOfStudy($event) {
    this.feedBack.level_loader = true;
    // this.feedBack.session_loader = true;
    this.statDetails.course_of_study = $event ? this.selectWhereId(this.coursesOfStudy, 'id', $event).name : null;
    this.schoolService.getSingleCourseOfStudy($event).subscribe((response) => {
          this.feedBack.level_loader = false;
          // this.feedBack.session_loader = false;
          this.levels = response['degree']['programme']['levels'];
          // this.sessions = response['degree']['programme']['sessions'];
        },
        (error) => {
          this.feedBack.level_loader = false;
          // this.feedBack.session_loader = false;
          // console.log('Level error', error);
          // this.Alert.error('Course of Study could not be loaded', error)
        });
  }

  public getSessionSemesters(session_year) {
    this.feedBack.semester_loader = true;
    let session_id = 0;
    this.statDetails.session = null;
    this.sessions.forEach(session => {
      if(session.year.toString() === session_year) {
        session_id = session.id;
        this.statDetails.session = session ? session.name : null;
      }
    });
    this.schoolService.getSingleSession(session_id).subscribe((response) => {
        this.feedBack.semester_loader = false;
        this.semesters = response['session_semesters'];
      }, (error) => {
        this.feedBack.semester_loader = false;
        // this.notification.error('Could not load Semesters. Please retry', error);
      });
  }

  public setSemester(semester) {
    this.semesters.forEach(sem => {
      if (sem.semester.toString() === semester) {
        this.statDetails.semester = sem ? sem.name : null;
      }
    })
  }

  public getCourseRegStatistics() {
    this.feedBack.loader = true;
    this.courseRegStats = [];
    this.reportService.courseRegistrationStatistics(this.filterForm.value).subscribe(
      (res) => {
        this.courseRegStats = res;
        // this.setPagination();
        this.feedBack.loader = false;
        // this.stats.total_paid = (response.current_page === 1) ? this.sessionReport.transaction_total : this.stats.total_paid;
      },
      error => {
        this.notification.error('Unable to load Report', error);
        this.feedBack.loader = false;
      });
  }

  private selectWhereId(data: any[], search_key: string, id) {
    const dataItem: any[] = [];
    data.forEach(item => {
      const itemKey = parseInt(item[search_key]);
      const itemId = parseInt(id);
      if (itemKey === itemId) {
        dataItem.push(item);
      }
    });
    return dataItem[0];
  }

  public printNau(id, title) {
    this.feedBack.print_loader = true;
    const that = this;
    // Internet Explorer 6-11
    const isIE = /*@cc_on!@*/false || !!document['documentMode'];
    // Edge 20+
    const isEdge = !isIE && !!window['StyleMedia'];
    // Chrome 1 - 71
    const isChrome = !!window['chrome'] && (!!window['chrome'].webstore || !!window['chrome'].runtime);
    console.info('Chrome', isChrome);
    console.info('Edge', isEdge);
    console.info('IE', isIE);
    const prev_frame = $('#print_frame');
    if (prev_frame) {
      prev_frame.remove();
    }
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['id'] = 'print_frame';
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-10000px', 'pointer-events': 'none'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    frameDoc.document.write('<html><head><title>Print ' + title + '</title>');
    frameDoc.document.write('</head><body>');
    frameDoc.document.write('<link href="/assets/css/print.css" rel="stylesheet">');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    frameDoc.onload = function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      // frame1.remove();
      that.feedBack.print_loader = false;
    }
  }

}
