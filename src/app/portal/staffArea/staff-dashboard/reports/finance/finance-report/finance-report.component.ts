import {Component, OnInit, AfterViewInit} from '@angular/core';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../../services/notification.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Cache} from '../../../../../../../utils/cache';
import {TsCompilerAotCompilerTypeCheckHostAdapter} from '@angular/compiler-cli/src/transformers/compiler_host';
import {SchoolService} from "../../../../../../../services/school.service";
import {ReportServiceService} from "../../../../../../../services/api-service/report-service.service";
import {AuthenticationService} from "../../../../../../../services/authentication.service";

@Component({
  selector: 'app-finance-report',
  templateUrl: './finance-report.component.html',
  styleUrls: ['./finance-report.component.css']
})
export class FinanceReportComponent implements OnInit, AfterViewInit {
  public permissions = {
    'school.report.student.bySession': false,
    'school.report.booked.hostels': false,
    'school.report.student.byDateRange': false,
    'school.report.student.byMethod': false,
    'school.course_reg_report': false,
    'school.report.special_report.create': false,
    'school.result.payment_by_fee_item': false,
    'school.course_reg_report_stat': false,
    'school.admission.student_statistics': false,
  };
  public showSession = true;
  public feedBack = {
    moduleName: 'Reports/transaction',
    loading: false,
    selection: '',
    allSession: [],
    allDepartments: [],
    allProgramme: [],
    allStudents: [],
    allCourseofStudy: [],
    loadSession: false,
    loadDepartment: false,
    loadCos: false
  };
  public payments = [];
  public studentForm: FormGroup;
  public departmentForm: FormGroup;
  public cosForm: FormGroup;
  public programmeForm: FormGroup;

  /*static modelData = function () {
    return {
      payment: ['', Validators.compose([Validators.required])]
    };
  };

  static sessionData = function () {
    return {
      session: ['', Validators.compose([Validators.required])]
    };
  };

  static dateData = function () {
    return {
      fromdate: ['', Validators.compose([Validators.required])],
      todate: ['', Validators.compose([Validators.required])]
    };
  };

  static studentData = function () {
    return {
      student: ['', Validators.compose([Validators.required])]
    };
  };

  static departmentData = function () {
    return {
      department: ['', Validators.compose([Validators.required])]
    };
  };

  static cosData = function () {
    return {
      cos: ['', Validators.compose([Validators.required])]
    };
  };

  static programmeData = function () {
    return {
      programme: ['', Validators.compose([Validators.required])]
    };
  };*/

  constructor(private staffConfigService: StaffConfigService,
              private schoolService: SchoolService,
              private reportService: ReportServiceService,
              private notification: NotificationService,
              private router: Router,
              private authService: AuthenticationService,
              private fb: FormBuilder) {
    /*this.modeForm = this.fb.group(FinanceReportComponent.modelData());
    this.sessionForm = this.fb.group(FinanceReportComponent.sessionData());
    this.dateForm = this.fb.group(FinanceReportComponent.dateData());
    this.studentForm = this.fb.group(FinanceReportComponent.studentData());
    this.departmentForm = this.fb.group(FinanceReportComponent.departmentData());
    this.cosForm = this.fb.group(FinanceReportComponent.cosData());
    this.programmeForm = this.fb.group(FinanceReportComponent.programmeData());*/
  }

  ngOnInit() {
    this.loadFunction();
  }

  loadFunction() {
    if (Cache.get('ping')) {
      this.payments = Cache.get('ping')['settings']['payment_method'];
    }
    /*this.allSession();
    this.allCourseofStudy();
    this.allDepartment();
    this.getAllStudents();
    this.getAllProgrammes();
    this.onSelect('Student Transaction');*/
  }

  /**
   * listing all session
   */
  public allSession() {
    this.feedBack.loading = true;
    this.staffConfigService.getAllSession()
      .subscribe((response) => {
          this.feedBack.allSession = response.data;
          this.feedBack.loading = false;
        },
        error => {
          this.notification.error('Unable to load session, please retry');
          this.feedBack.loading = false;
        });
  }

  /**
   * listing all departments
   */
  public allDepartment() {
    this.feedBack.loading = true;
    this.staffConfigService.getAllDepartment()
      .subscribe((response) => {
          this.feedBack.allDepartments = response.data;
          this.feedBack.loading = false;
        },
        error => {
          this.notification.error('Unable to load departments please retry');
          this.feedBack.loading = false;
        });
  }

  /**
   * listing all course of study
   */
  public allCourseofStudy() {
    this.feedBack.loading = true;
    this.staffConfigService.getAllCourse()
      .subscribe((response) => {
          this.feedBack.loading = false;
          this.feedBack.allCourseofStudy = response.data;
        },
        error => {
          this.feedBack.loading = false;
          this.notification.error('Unable to load ' + this.feedBack.moduleName + ', please retry');
        });
  }

  /**
   * get all students
   */
  public getAllStudents() {
    this.feedBack.loading = true;
    this.schoolService.getAllStudents().subscribe(
      (response) => {
        this.feedBack.loading = false;
        this.feedBack.allStudents = response.data;
      },
      (error) => {
        this.feedBack.loading = false;
        this.notification.error('Unable to load Students', error);
      }
    );
  }

  /**
   * get all programmess
   */
  public getAllProgrammes() {
    this.feedBack.loading = true;
    this.staffConfigService.getAllProgramme()
      .subscribe((response) => {
          this.feedBack.loading = false;
          this.feedBack.allProgramme = response.data;
        },
        (error) => {
          this.feedBack.loading = false;
          this.notification.error('Unable to load Students', error);
        }
      );
  }

  /*onSubmitStudent() {
    this.router.navigate(['staff/reports/transaction/student', this.studentForm.value.student]);
  }

  onSubmitDepartment() {
    this.router.navigate(['staff/reports/transaction/department', this.departmentForm.value.department]);
  }

  onSubmitCos() {
    this.router.navigate(['staff/reports/transaction/course-of-study', this.cosForm.value.cos]);
  }

  onSubmitCourseByDepartment() {
    this.router.navigate(['staff/reports/department', this.departmentForm.value.department]);
  }

  onSubmitCourseOfStudyByDepartment() {
    this.router.navigate(['staff/reports/course-of-study', this.departmentForm.value.department]);
  }*/

  onSelect(event) {
    // window.scrollTo(0, 500);
    this.feedBack.selection = event;
    Cache.set('report_name', this.feedBack.selection);
    this.router.navigate(['/staff/reports']);
    this.reportService.selectReport.next(event);
  }

  onSubmitProgramme() {
    this.router.navigate(['staff/reports/admission-by-programme', this.programmeForm.value.programme]);
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  private toRemoveByPermission () {
    for (const permission in this.permissions) {
      this.permissions[permission] = this.iAmPermitted(permission);
    }
  }

  ngAfterViewInit(): void {
    this.toRemoveByPermission();
  }


}
