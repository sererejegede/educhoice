import {Component, ElementRef, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from '../../../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';
import {ReportServiceService} from '../../../../../../../services/api-service/report-service.service';
import {SchoolService} from '../../../../../../../services/school.service';

declare const $;

@Component({
  selector: 'app-student-range-payment',
  templateUrl: './student-range-payment.component.html',
  styleUrls: ['./student-range-payment.component.css']
})
export class StudentRangePaymentComponent implements OnInit {
  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);


  public createForm: FormGroup;
  public id: number;
  public feedBack = {
    allResponse: [],
    allStudents: [],
    viewDetails: [],
    moduleName: '',
    formType: 'Create',
    loader: false,
    submitStatus: false,
    showUpdateButton: false,
    deleteStatus: false
  };


  ngOnInit() {
    this.loadFunction();
  }

  constructor(private staffConfigService: StaffConfigService,
              private reportService: ReportServiceService,
              private schoolService: SchoolService,
              private notification: NotificationService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder) {
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
  }

  loadFunction() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getReportByStudent(this.id);
    this.getAllStudents();
  }

  /**
   * get all students
   */
  public getAllStudents() {
    this.schoolService.getAllStudents().subscribe(
      (response) => {
        this.feedBack.allStudents = response.data;
      },
      (error) => {
        this.notification.error('Unable to load Students', error);
      }
    );
  }

  /**
   * listing all applications
   */
  public getReportByStudent(id) {
    this.feedBack.loader = true;
    this.reportService.onGetStudent(id)
      .subscribe((response) => {
          this.feedBack.allResponse = response;
          if (this.feedBack.allResponse.length > 0) {
            this.dtTrigger.next();
          }
          this.feedBack.loader = false;
        },
        error => {
          this.notification.error('Unable to load Current Session, please retry');
          this.feedBack.loader = false;
        });
  }
  onView(data){
    this.feedBack.viewDetails = data;
    $('#viewModal').modal('show');
  }

  navigateToReport() {
    this.router.navigate(['staff/reports/transaction']);
  }

  onSelectDept(event) {
    this.getReportByStudent(event.target.value);
    this.feedBack.allResponse = [];
    this.feedBack.moduleName = '';
  }
}
