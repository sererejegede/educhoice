import {Component, ElementRef, OnInit} from '@angular/core';
import {NotificationService} from '../../../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';
import {ReportServiceService} from '../../../../../../../services/api-service/report-service.service';
import {Paginator} from '../../../../../../../utils/paginator';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CSVExportService} from '../../../../../../../services/c-sv-export.service';

declare const $;

@Component({
  selector: 'app-date-range-payment',
  templateUrl: './date-range-payment.component.html',
  styleUrls: ['./date-range-payment.component.css']
})
export class DateRangePaymentComponent implements OnInit {
  public search_model: Subject<string> = new Subject<string>();
  public paginator = {
    per_page: 10,
    page_number: null,
    search: '',
    pages: [1, 2, 3, 4, 5]
  };
  public stats = {
    total_paid: 0
  };
  public id: number;
  public rangeReport: any;
  public dateFilterForm: FormGroup;
  public singleRangeReport: any;
  public feedBack = {
    allResponse: [],
    loadDepartment: false,
    allDepartments: [],
    allProgrammes: [],
    programmeLevels: [],
    programme_loader: false,
    level_loader: false,
    excelling: false,
    moduleName: '',
    formType: 'Create',
    loader: false,
    submitStatus: false,
    showUpdateButton: false,
    deleteStatus: false
  };
  public date = {
    from: null,
    to: null
  };

  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);

  static dateReportFilter = () => {
    return {
      programme_id: [1],
      level_id: [''],
      to: [new Date().toISOString().split('T')[0], Validators.required],
      from: [new Date().toISOString().split('T')[0], Validators.required]
    };
  }


  ngOnInit() {
    this.loadFunction();
  }

  constructor(private staffConfigService: StaffConfigService,
              private reportService: ReportServiceService,
              private notification: NotificationService,
              private fb: FormBuilder,
              private tablePager: Paginator,
              private excelService: CSVExportService) {
    this.dateFilterForm = this.fb.group(DateRangePaymentComponent.dateReportFilter());
  }

  public allProgrammes() {
    this.feedBack.programme_loader = true;
    this.staffConfigService.getAllProgramme()
      .subscribe((response) => {
          this.feedBack.programme_loader = false;
          this.feedBack.allProgrammes = response.data;
        },
        error => {
          this.feedBack.programme_loader = false;
          this.notification.error('Unable to load Programmes, please retry', error);
        });
  }

  public programmeById (id = 1) {
    this.feedBack.level_loader = true;
    this.staffConfigService.getProgramme(id)
      .subscribe((response) => {
          this.feedBack.level_loader = false;
          this.feedBack.programmeLevels = response.levels;
        },
        error => {
          this.feedBack.level_loader = false;
          this.notification.error('Unable to load Programmes, please retry', error);
        });
  }

  public changePerPage (num) {
    this.tablePager.changePerPage(num);
    this.getReportByDateRange();
  }

  public navigate (page) {
    this.tablePager.navigate(page);
    this.getReportByDateRange();
  }

  public setPagination () {
    this.tablePager.laravelPaginationObject = this.rangeReport;
    this.tablePager.setPagination();
    this.tablePager.paginator = this.paginator;
  }

  private loadFunction() {
    this.allProgrammes();
    this.programmeById();
    this.getReportByDateRange();
  }

  public getReportByDateRange() {
    // if (this.date.to === null) {
    //   return;
    // }
    // if (this.date.from === null) {
    //   return this.notification.warning('Please select a Payment Method');
    // }
    this.feedBack.loader = true;
    this.reportService.onGetFromDate(this.dateFilterForm.value, this.paginator)
      .subscribe((response) => {
          this.rangeReport = response;
          this.setPagination();
          if (this.rangeReport.last_page < 5) {
            this.setPagination();
          }
          this.stats.total_paid = (response.current_page === 1) ? this.rangeReport.transaction_total : this.stats.total_paid;
          this.feedBack.loader = false;
        },
        error => {
          this.notification.error('Unable to fetch Report, please retry', error);
          this.feedBack.loader = false;
        });
  }

  public exportAsXLSX() {
    this.feedBack.excelling = true;
    this.reportService.onGetFromDateNoPaginate(this.dateFilterForm.value).subscribe(res => {
      // console.log(res);
      const formatted_data = [];
      if (res && res.length) {
        res.forEach(r => {
          formatted_data.push({
            'Matric Number': r.matric_no,
            'Transaction ID': r.transaction_no,
            'Amount': parseInt(r.amount, 10),
            'Date Generated': r.created_at,
            'Date Paid': r.updated_at,
            'Payment Method': r.payment_method,
            'Department': r.name
          });
        });
      }
      this.excelService.exportAsExcelFile(formatted_data, 'Date Transaction Report');
      this.feedBack.excelling = false;
    }, err => {
      this.notification.error('Error Generating Excel Sheet.');
      this.feedBack.excelling = false;
    });
  }

  public reRender(): void {
    // (type === 'from') ? this.date.from = event.target.value : this.date.to = event.target.value;
    this.paginator.page_number = 1;
    this.getReportByDateRange();
  }

  onView(data) {
    this.singleRangeReport = data;
    $('#viewModal').modal('show');
  }

  onOpen() {
    $('#myModal').modal('show');
  }

  // onSubmitDate() {
  //   this.feedBack.allResponse = [];
  //   this.feedBack.moduleName = '';
  // }
}
