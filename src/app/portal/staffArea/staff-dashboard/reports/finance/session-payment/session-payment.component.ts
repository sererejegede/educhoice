import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {NotificationService} from '../../../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';
import {ReportServiceService} from '../../../../../../../services/api-service/report-service.service';
import {isNull} from 'util';
import {Cache} from '../../../../../../../utils/cache';
import {environment} from '../../../../../../../environments/environment';
import {Paginator} from '../../../../../../../utils/paginator';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CSVExportService} from '../../../../../../../services/c-sv-export.service';

declare const $;

@Component({
  selector: 'app-session-payment',
  templateUrl: './session-payment.component.html',
  styleUrls: ['./session-payment.component.css']
})
export class SessionPaymentComponent implements OnInit {
  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  public stats = {
    total_paid: 0
  };
  public paginator = {
    per_page: 10,
    page_number: null,
    search: '',
    pages: [1, 2, 3, 4, 5]
  };
  public reQuery = {
    show: false,
    url: '',
    loading: false
  };
  public sessionReport: any;
  public sessionReportFilterForm: FormGroup;
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    allSession: [],
    allProgrammes: [],
    programmeLevels: [],
    faculties: [],
    departments: [],
    courses_of_study: [],
    moduleName: '',
    formType: 'Create',
    loader: false,
    excelling: false,
    current_session: null,
    show_filter: true,
    status: 'completed'
  };
  public loader = {
    faculty: false,
    department: false,
    courses_of_study: false,
    session: false,
    level: false,
    programme: false
  };
  public search_model: Subject<string> = new Subject<string>();

  private static sessionReportFilter = () => {
    return {
      programme_id: [1],
      session_year: ['', Validators.required],
      level_id: [''],
      course_of_study_id: [''],
      faculty_id: [''],
      department_id: [''],
      status: ['completed', Validators.required]
    };
  }

  constructor(private staffConfigService: StaffConfigService,
              private reportService: ReportServiceService,
              private notification: NotificationService,
              private fb: FormBuilder,
              private tablePager: Paginator,
              private excelService: CSVExportService) {
    this.sessionReportFilterForm = this.fb.group(SessionPaymentComponent.sessionReportFilter());
    this.search_model.debounceTime(500)
      .distinctUntilChanged()
      .subscribe(search => {
        this.paginator.search = search;
        this.paginator.page_number = 1;
        this.getReportBySession();
      });
  }

  ngOnInit() {
    this.allSession();
    this.allProgrammes();
    this.programmeById();
    this.allFaculties();
  }

  public changePerPage (num) {
    this.tablePager.changePerPage(num);
    this.getReportBySession();
  }

  public navigate (page) {
    this.tablePager.navigate(page);
    this.getReportBySession();
  }

  private setPagination () {
    this.tablePager.laravelPaginationObject = this.sessionReport;
    this.tablePager.setPagination();
    this.tablePager.paginator = this.paginator;
  }

  public allSession() {
    this.loader.session = true;
    this.staffConfigService.getAllSession()
      .subscribe((response) => {
          this.loader.session = false;
          this.feedBack.allSession = response.data;
        },
        error => {
          this.loader.session = false;
          this.notification.error('Unable to load Sessions, please retry', error);
        });
  }

  public allProgrammes () {
    this.loader.programme = true;
    this.staffConfigService.getAllProgramme()
      .subscribe((response) => {
          this.loader.programme = false;
          this.feedBack.allProgrammes = response.data;
        },
        error => {
          this.loader.programme = false;
          this.notification.error('Unable to load Programmes, please retry', error);
        });
  }

  public allFaculties () {
    this.loader.faculty = true;
    this.staffConfigService.getAllFaculty()
      .subscribe((response) => {
          this.loader.faculty = false;
          this.feedBack.faculties = response.data;
        },
        error => {
          this.loader.faculty = false;
          this.notification.error('Unable to load Faculties, please retry', error);
        });
  }

  public facultyDepartments (faculty_id) {
    this.loader.department = true;
    this.staffConfigService.getFaculty(faculty_id)
      .subscribe((response) => {
          this.loader.department = false;
          if (response.departments) {
            this.feedBack.departments = response.departments;
          }
        },
        error => {
          this.loader.department = false;
          this.notification.error('Unable to load Departments, please retry', error);
        });
  }

  public departmentCoursesOfStudy (department_id) {
    this.loader.courses_of_study = true;
    this.staffConfigService.getCourseOfStudyById(department_id)
      .subscribe((response) => {
          this.loader.courses_of_study = false;
          if (response.course_of_studies) {
            this.feedBack.courses_of_study = response.course_of_studies;
          }
        },
        error => {
          this.loader.courses_of_study = false;
          this.notification.error('Unable to load Courses of study, please retry', error);
        });
  }

  public programmeById (id = 1) {
    this.loader.level = true;
    this.staffConfigService.getProgramme(id)
      .subscribe((response) => {
          this.loader.level = false;
          this.feedBack.programmeLevels = response.levels;
        },
        error => {
          this.loader.level = false;
          this.notification.error('Unable to load Programmes, please retry', error);
        });
  }

  public getReportBySession() {
    // if (isNull(this.sessionReportFilterForm.value.session_id)) {
    //   return this.notification.warning('Please select a session');
    // }
    this.feedBack.loader = true;
    // const filter = {session: this.feedBack.current_session, status: this.feedBack.status};
    this.reportService.onGetSession(this.sessionReportFilterForm.value, this.paginator)
      .subscribe((response) => {
          // console.log('Session Transactions', response);
          // if (response.data) {
          //   response.data.reverse();
          // }
          this.sessionReport = response;
          this.setPagination();
          this.feedBack.loader = false;
          // this.stats.total_paid = this.sessionReport.transaction_total;
            this.stats.total_paid = (response.current_page === 1) ? this.sessionReport.transaction_total : this.stats.total_paid;
          // setTimeout(this.getStats(), 3000);
        },
        error => {
          this.notification.error('Unable to load Report', error);
          this.feedBack.loader = false;
        });
  }

  public exportAsXLSX() {
    this.feedBack.excelling = true;
    this.reportService.sessionReportStats(this.sessionReportFilterForm.value).subscribe(res => {
      const formatted_data = [];
      if (res && res.length) {
        res.forEach(r => {
          formatted_data.push({
            'First Name': r.first_name,
            'Last Name': r.last_name,
            'Other Names': r.other_names,
            'Matric Number': r.matric_no,
            'Course of Study': r.course_of_study,
            'Department': r.department,
            'Level': r.level,
            'Transaction ID': r.transaction_no,
            'Amount': parseInt(r.amount, 10),
            'Date Generated': r.created_at,
            'Date Paid': r.updated_at,
            'Payment Method': r.payment_method,
          });
        });
      }
      // console.log(formatted_data);
      this.excelService.exportAsExcelFile(formatted_data, 'Session Transaction Report');
      this.feedBack.excelling = false;
    }, err => {
      this.notification.error('Error Generating Excel Sheet.');
      this.feedBack.excelling = false;
    });
  }

  public makeReQuery (transaction_id, index) {
    // const url = this.reQuery.url + transaction_id;
    this.sessionReport.data[index]['loading'] = true;
    this.reportService.makeReQuery(transaction_id).subscribe(res => {
      this.sessionReport.data[index]['loading'] = false;
      (res.message) ? this.notification.info(res.message) : this.notification.warning('Please retry');
    }, err => {
      this.sessionReport.data[index]['loading'] = false;
      this.notification.error('Could not make ReQuery', err);
    });
  }

  public reRender(): void {
    // if (type) {
    //   this.feedBack.current_session = event.target.value;
    // } else {
    //   this.feedBack.status = event.target.value;
    //   (event.target.value.toUpperCase() === 'PENDING') ? this.reQuery.show = true : this.reQuery.show = false;
    // }
    this.paginator.page_number = 1;
    this.getReportBySession();
  }

}
