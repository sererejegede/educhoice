import {Component, ElementRef, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from '../../../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';
import {Cache} from '../../../../../../../utils/cache';
import {ReportServiceService} from "../../../../../../../services/api-service/report-service.service";
import {Paginator} from "../../../../../../../utils/paginator";

declare const $;

@Component({
  selector: 'app-mode-payment',
  templateUrl: './mode-payment.component.html',
  styleUrls: ['./mode-payment.component.css']
})
export class ModePaymentComponent implements OnInit {
  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);
  public search_model: Subject<string> = new Subject<string>();
  public paginator = {
    per_page: 10,
    page_number: null,
    search: '',
    pages: [1, 2, 3, 4, 5]
  };
  public createForm: FormGroup;
  public id: number;
  public paymentMethods = [];
  public paymentMethodReport;
  public paymentMethodReportView;
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    allSession: [],
    session_loader: false,
    current_session: null,
    loadDepartment: false,
    allDepartments: [],
    moduleName: '',
    formType: 'Create',
    loader: false,
    submitStatus: false,
    showUpdateButton: false,
    deleteStatus: false,
    payment_method: ''
  };


  ngOnInit() {
    this.loadFunction();
  }

  constructor(private staffConfigService: StaffConfigService,
              private reportService: ReportServiceService,
              private notification: NotificationService,
              private router: Router,
              private tablePager: Paginator) {
    this.search_model.debounceTime(500)
      .distinctUntilChanged()
      .subscribe(search => {
        this.paginator.search = search;
        this.paginator.page_number = 1;
        this.getReportByMode();
      });
  }

  public changePerPage (num) {
    this.tablePager.changePerPage(num);
    this.getReportByMode();
  }

  public navigate (page) {
    this.tablePager.navigate(page);
    this.getReportByMode();
  }

  private setPagination () {
    this.tablePager.laravelPaginationObject = this.paymentMethodReport;
    this.tablePager.setPagination();
    this.tablePager.paginator = this.paginator;
  }

  loadFunction() {
    if (Cache.get('app_settings') && Cache.get('app_settings')['settings'] && Cache.get('app_settings')['settings']['payment_method']) {
      this.paymentMethods = Cache.get('app_settings')['settings']['payment_method'];
    }
    this.allSession();
  }

  public allSession() {
    this.feedBack.session_loader = true;
    this.staffConfigService.getAllSession()
      .subscribe((response) => {
          this.feedBack.session_loader = false;
          this.feedBack.allSession = response.data;
        },
        error => {
          this.feedBack.session_loader = false;
          this.notification.error('Unable to load Sessions, please retry', error);
        });
  }

  public getReportByMode() {
    if (this.feedBack.payment_method === '') {
      return this.notification.warning('Please select a Payment Method');
    }
    this.feedBack.loader = true;
    const filter = {payment_method: this.feedBack.payment_method, session: this.feedBack.current_session};
    this.reportService.onGetPaymentMethod(filter, this.paginator).subscribe((response) => {
        // this.feedBack.allResponse = (response.data) ? response.data : response;
      this.paymentMethodReport = response;
      this.setPagination();
        this.feedBack.loader = false;
      },
      error => {
        this.notification.error('Unable to load Current Session, please retry');
        this.feedBack.loader = false;
      });
  }


  viewReservation(details) {
    this.feedBack.viewDetails = details;
    $('#hostelReservation').modal('show');
  }

  navigateToReport() {
    this.router.navigate(['staff/reports/transaction']);
  }

  public reRender(event, type): void {
    (type) ? this.feedBack.current_session = event.target.value : this.feedBack.payment_method = event.target.value;
    this.paginator.page_number = 1;
    this.getReportByMode();
  }

  onView(data) {
    console.log(data);
    this.paymentMethodReportView = data;
    $('#viewModal').modal('show');
  }
}
