import {Component, ElementRef, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from '../../../../../../../services/notification.service';
import {ReportServiceService} from '../../../../../../../services/api-service/report-service.service';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {Subject} from 'rxjs/Subject';
import {DataTableDirective} from 'angular-datatables';

declare const $;

@Component({
  selector: 'app-course-of-study-payment',
  templateUrl: './course-of-study-payment.component.html',
  styleUrls: ['./course-of-study-payment.component.css']
})
export class CourseOfStudyPaymentComponent implements OnInit {
  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);


  public createForm: FormGroup;
  public id: number;
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    allHostel: [],
    allCourseofStudy: [],
    moduleName: '',
    formType: 'Create',
    loader: false,
    submitStatus: false,
    showUpdateButton: false,
    deleteStatus: false
  };


  ngOnInit() {
    this.loadFunction();
  }

  constructor(private staffConfigService: StaffConfigService,
              private reportService: ReportServiceService,
              private notification: NotificationService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder) {
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
  }

  loadFunction() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getReportByCos(this.id);
    this.allCourseofStudy();
  }

  /**
   * listing all applications
   */
  public getReportByCos(id) {
    this.feedBack.loader = true;
    this.reportService.courseOfStudyFinance(id)
      .subscribe((response) => {
          console.log('report by cos finance ::', response);
          this.feedBack.allResponse = response;
          if (this.feedBack.allResponse.length > 0) {
            this.feedBack.moduleName = response[0]['department']['name'] + ' Report';
            this.dtTrigger.next();
          }
          this.feedBack.loader = false;
        },
        error => {
          this.notification.error('Unable to load Current Session, please retry');
          this.feedBack.loader = false;
        });
  }

  /**
   * listing all course of study
   */
  public allCourseofStudy() {
    this.staffConfigService.getAllCourse()
      .subscribe((response) => {
          this.feedBack.allCourseofStudy = response.data;
        },
        error => {
          this.notification.error('Unable to load ' + this.feedBack.moduleName + ', please retry');
        });
  }
  viewReservation(details) {
    this.feedBack.viewDetails = details;
    $('#hostelReservation').modal('show');
  }

  navigateToReport() {
    this.router.navigate(['staff/reports/transaction']);
  }

  onSelectDept(event) {
    this.getReportByCos(event.target.value);
    this.feedBack.allResponse = [];
    this.feedBack.moduleName = '';
  }
}
