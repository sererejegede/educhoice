import { Component, OnInit } from '@angular/core';
import { ReportServiceService } from '../../../../../../../services/api-service/report-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotificationService } from '../../../../../../../services/notification.service';
import { StaffConfigService } from '../../../../../../../services/api-service/staff-config.service';
import { Paginator } from '../../../../../../../utils/paginator';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-payment-statistics',
  templateUrl: './payment-statistics.component.html',
  styleUrls: ['./payment-statistics.component.css']
})
export class PaymentStatisticsComponent implements OnInit {

  public paymentStatisticsReport: any;
  public paymentStatisticsFilterForm: FormGroup;
  public search_model: Subject<string> = new Subject<string>();
  public paginator = {
    per_page: 10,
    page_number: null,
    search: '',
    pages: [1, 2, 3, 4, 5]
  };
  public masterObject = {
    semesters: [],
    sessions: [],
    loader: false,
    show_filter: false,
    creating: false
  };
  public loader = {
    faculty: false,
    department: false,
    courses_of_study: false,
    session: false,
    semester: false,
    level: false,
    programme: false
  };

  private static paymentStatisticsFilter = () => {
    return {
      session_id: ['', Validators.required],
      semester: ['', Validators.required],
      name: ['', Validators.required],
      from_date: [''],
      to_date: ['']
    };
  }

  constructor(private reportService: ReportServiceService,
              private fb: FormBuilder,
              private tablePager: Paginator,
              private staffConfigService: StaffConfigService,
              private notification: NotificationService) {
    this.paymentStatisticsFilterForm = this.fb.group(PaymentStatisticsComponent.paymentStatisticsFilter());
    this.search_model.debounceTime(500)
      .distinctUntilChanged()
      .subscribe(search => {
        this.paginator.search = search;
        this.paginator.page_number = 1;
        this.listSpecialReports();
      });
  }

  ngOnInit() {
    this.getAllSession();
    this.listSpecialReports();
  }

  public changePerPage (num) {
    this.tablePager.changePerPage(num);
    this.listSpecialReports();
  }

  public navigate (page) {
    this.tablePager.navigate(page);
    this.listSpecialReports();
  }

  private setPagination () {
    this.tablePager.laravelPaginationObject = this.paymentStatisticsReport;
    this.tablePager.setPagination();
    this.tablePager.paginator = this.paginator;
  }

  private getAllSession () {
    this.loader.session = true;
    this.staffConfigService.getSessionList().subscribe((res) => {
      this.loader.session = false;
      this.masterObject.sessions = res;
    }, (err) => {
      this.loader.session = false;
      this.notification.error('An error occurred', err);
    });
  }

  public getSessionSemesters(session_id) {
    this.loader.semester = true;
    this.staffConfigService.getSingleSession(session_id)
      .subscribe((response) => {
        this.loader.semester = false;
        this.masterObject.semesters = response['session_semesters'];
      }, (error) => {
        this.loader.semester = false;
        this.notification.error('Could not load Semesters. Please retry', error);
      });
  }

  public createSpecialReport() {
    this.masterObject.creating = true;
    const special_report_data = {
      name: this.paymentStatisticsFilterForm.value['name'],
      report_type: (this.paymentStatisticsFilterForm.value['from_date'] && this.paymentStatisticsFilterForm.value['to_date']) ? 'App\\Models\\School\\SpecialReport\\StudentsPaymentRange' : 'App\\Models\\School\\SpecialReport\\StudentsPayment',
      request: {
        session_id: this.paymentStatisticsFilterForm.value['session_id'],
        semester: this.paymentStatisticsFilterForm.value['semester'],
        from_date: this.paymentStatisticsFilterForm.value['from_date'],
        to_date: this.paymentStatisticsFilterForm.value['to_date']
      }
    };
    this.reportService.createSpecialReport(special_report_data).subscribe(
      (res) => {
        this.masterObject.creating = false;
        this.notification.success(res.message || 'Successful!');
        this.paymentStatisticsReport.data.push(res);
        this.masterObject.show_filter = false;
      }, (err) => {
        this.masterObject.creating = false;
        this.notification.error('An error occurred', err);
      });
  }

  private listSpecialReports() {
    this.masterObject.loader = true;
    this.reportService.listSpecialReports().subscribe(
      (res) => {
        this.masterObject.loader = false;
        this.paymentStatisticsReport = res;
        this.setPagination();
      }, (err) => {
        this.masterObject.loader = false;
        this.notification.error('Could not load reports', err);
      });
  }

  public getSpecialReport() {
    this.reportService.getSpecialReport(5).subscribe(
      (res) => {

      }, (err) => {

      });
  }

  public downloadSpecialReport(report_id, index) {
    this.paymentStatisticsReport.data[index]['downloading'] = true;
    this.reportService.downloadSpecialReport(report_id).subscribe(
      (res) => {
        this.paymentStatisticsReport.data[index]['downloading'] = false;
        const link = document.getElementById('reportDownload');
        link.setAttribute('href', res);
        link.click();
      }, (err) => {
        this.paymentStatisticsReport.data[index]['downloading'] = false;

      });
  }

}
