import {Component, ElementRef, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationService} from '../../../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../../../services/api-service/staff-config.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';
import {ReportServiceService} from '../../../../../../../services/api-service/report-service.service';

declare const $;

@Component({
  selector: 'app-department-payment',
  templateUrl: './department-payment.component.html',
  styleUrls: ['./department-payment.component.css']
})
export class DepartmentPaymentComponent implements OnInit {
  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);


  public createForm: FormGroup;
  public id: number;
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    loadDepartment: false,
    allDepartments: [],
    moduleName: '',
    formType: 'Create',
    loader: false,
    submitStatus: false,
    showUpdateButton: false,
    deleteStatus: false
  };


  ngOnInit() {
    this.loadFunction();
  }

  constructor(private staffConfigService: StaffConfigService,
              private reportService: ReportServiceService,
              private notification: NotificationService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder) {
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
  }

  loadFunction() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.getReportByDepartment(this.id);
    this.allDepartment();
  }

  /**
   * listing all applications
   */
  public getReportByDepartment(id) {
    this.feedBack.loader = true;
    this.staffConfigService.getReportByDepartmentId(id)
      .subscribe((response) => {
          this.feedBack.allResponse = response;
          // this.feedBack.moduleName = response[0]['department']['name'] + ' Report';
          if (this.feedBack.allResponse.length > 0) {
            this.feedBack.moduleName = ' / Course / ' + response[0]['department']['name'];
            this.dtTrigger.next();
          }
          this.feedBack.loader = false;
        },
        error => {
          this.notification.error('Unable to load Current Session, please retry');
          this.feedBack.loader = false;
        });
  }

  /**
   * listing all departments
   */

  public allDepartment() {
    this.feedBack.loadDepartment = true;
    this.staffConfigService.getAllDepartment()
      .subscribe((response) => {
          this.feedBack.allDepartments = response.data;
          this.feedBack.loadDepartment = false;
        },
        error => {
          this.notification.error('Unable to load departments please retry');
          this.feedBack.loadDepartment = false;
        });
  }

  viewReservation(details) {
    this.feedBack.viewDetails = details;
    $('#hostelReservation').modal('show');
  }

  navigateToReport() {
    this.router.navigate(['staff/reports/transaction']);
  }

  onSelectDept(event) {
    this.getReportByDepartment(event.target.value);
    this.feedBack.allResponse = [];
    this.feedBack.moduleName = '';
  }
}
