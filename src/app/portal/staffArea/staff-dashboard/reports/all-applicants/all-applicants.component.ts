import {Component, ElementRef, OnInit} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {Cache} from '../../../../../../utils/cache';
import {JSONPARSE} from '../../../../../../utils/json-parse';
import {Router} from "@angular/router";

declare const $;

@Component({
  selector: 'app-all-applicants',
  templateUrl: './all-applicants.component.html',
  styleUrls: ['./all-applicants.component.css']
})
export class AllApplicantsComponent implements OnInit {
  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  dtElement: DataTableDirective = new DataTableDirective(this.el);


  public createForm: FormGroup;
  public id: number;
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    allHostel: [],
    moduleName: 'All Applicants',
    formType: 'Create',
    loader: false,
    submitStatus: false,
    showUpdateButton: false,
    deleteStatus: false
  };


  ngOnInit() {
    this.loadFunction();
  }

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private router: Router,
              private fb: FormBuilder) {
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
  }

  loadFunction() {
    this.getAllApplicants();
  }

  /**
   * listing all applications
   */
  public getAllApplicants() {
    this.feedBack.loader = true;
    this.staffConfigService.getApplicantReports()
      .subscribe((response) => {
      console.log('applicant :: ', response);
          this.feedBack.allResponse = response;
          if (this.feedBack.allResponse.length > 0) {
            this.dtTrigger.next();
          }
          this.feedBack.loader = false;
        },
        error => {
          this.notification.error('Unable to load Current Session, please retry');
          this.feedBack.loader = false;
        });
  }
  view(details) {
    this.feedBack.viewDetails = details;
    $('#hostelReservation').modal('show');
  }
}
