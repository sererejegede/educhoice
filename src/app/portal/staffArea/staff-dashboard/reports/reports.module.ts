import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsComponent } from './reports.component';
import { AllApplicantsComponent } from './all-applicants/all-applicants.component';
import { RouterModule, Routes } from '@angular/router';
import { StaffDashboardComponent } from '../dashboard-component.component';
import { AdmissionProgrammeComponent } from './admission-programme/admission-programme.component';
import { AdmissionPaymentComponent } from './admission-payment/admission-payment.component';
import { AdmittedStudentsComponent } from './admitted-students/admitted-students.component';
import { CourseDepartmentComponent } from './course-department/course-department.component';
import { CourseCourseOfStudyComponent } from './course-course-of-study/course-course-of-study.component';
import { ModePaymentComponent } from './finance/mode-payment/mode-payment.component';
import { SessionPaymentComponent } from './finance/session-payment/session-payment.component';
import { DateRangePaymentComponent } from './finance/date-range-payment/date-range-payment.component';
import { StudentRangePaymentComponent } from './finance/student-range-payment/student-range-payment.component';
import { DepartmentPaymentComponent } from './finance/department-payment/department-payment.component';
import { CourseOfStudyPaymentComponent } from './finance/course-of-study-payment/course-of-study-payment.component';
import { FinanceReportComponent } from './finance/finance-report/finance-report.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ReportFilterComponent } from './report-filter/report-filter.component';
import { Select2Module } from 'ng2-select2';
import { DataTablesModule } from 'angular-datatables';
import { HostelReportComponent } from './hostel-report/hostel-report.component';
import { CourseRegistrationComponent } from './course-registration/course-registration.component';
import { SchoolService } from '../../../../../services/api-service/school.service';
import { PaymentStatisticsComponent } from './finance/payment-statistics/payment-statistics.component';
import { FeeItemComponent } from './fee-item/fee-item.component';
import { CourseRegStatisticsComponent } from './course-reg-statistics/course-reg-statistics.component';
import { StudentStatisticsComponent } from './student-statistics/student-statistics.component';

const REPORT_ROUTES: Routes = [
  {
    path: 'reports', component: StaffDashboardComponent, children: [
    {path: '', component: FinanceReportComponent, children: [
      // {path: '', component: ReportFilterComponent},
      {path: 'mode', component: ModePaymentComponent},
      {path: 'course-reg', component: CourseRegistrationComponent},
      {path: 'course-reg-stat', component: CourseRegStatisticsComponent},
      {path: 'fee-item', component: FeeItemComponent},
      // {path: 'session', component: SessionPaymentComponent},
      {path: 'date', component: DateRangePaymentComponent},
      {path: 'student/:id', component: StudentRangePaymentComponent},
      {path: 'department/:id', component: DepartmentPaymentComponent},
      {path: 'course-of-study/:id', component: CourseOfStudyPaymentComponent},
      {path: 'all-applicants', component: AllApplicantsComponent},
      {path: 'admission-by-programme/:id', component: AdmissionProgrammeComponent},
      {path: 'admission-by-payment', component: AdmissionPaymentComponent},
      {path: 'admitted-students', component: AdmittedStudentsComponent},
      {path: 'department/:id', component: CourseDepartmentComponent},
      {path: 'course-of-study/:id', component: CourseCourseOfStudyComponent},
      {path: 'hostel', component: HostelReportComponent},
      {path: 'payment-stats', component: PaymentStatisticsComponent},
      {path: 'student-stats', component: StudentStatisticsComponent},

    ]}
  ]
  }
  ];

@NgModule({
  imports: [
    CommonModule,
    Select2Module,
    ReactiveFormsModule,
    FormsModule,
    DataTablesModule,
    RouterModule.forChild(REPORT_ROUTES)
  ],
  declarations: [
    ReportsComponent,
    AllApplicantsComponent,
    AdmissionProgrammeComponent,
    AdmissionPaymentComponent,
    AdmittedStudentsComponent,
    CourseDepartmentComponent,
    CourseCourseOfStudyComponent,
    ModePaymentComponent,
    SessionPaymentComponent,
    DateRangePaymentComponent,
    StudentRangePaymentComponent,
    DepartmentPaymentComponent,
    CourseOfStudyPaymentComponent,
    FinanceReportComponent,
    ReportFilterComponent,
    HostelReportComponent,
    CourseRegistrationComponent,
    PaymentStatisticsComponent,
    FeeItemComponent,
    CourseRegStatisticsComponent,
    StudentStatisticsComponent
  ],
  providers: [
    SchoolService
  ]

})
export class ReportsModule {
}
