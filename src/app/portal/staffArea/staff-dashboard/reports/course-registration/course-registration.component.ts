import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Paginator} from '../../../../../../utils/paginator';
import {NotificationService} from '../../../../../../services/notification.service';
import {ReportServiceService} from '../../../../../../services/api-service/report-service.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SchoolService} from '../../../../../../services/api-service/school.service';
import {Subject} from "../../../../../../../node_modules/rxjs";
import {DataTableDirective} from "angular-datatables";
import {CSVExportService} from "../../../../../../services/c-sv-export.service";

@Component({
  selector: 'app-course-registration',
  templateUrl: './course-registration.component.html',
  styleUrls: ['./course-registration.component.css']
})
export class CourseRegistrationComponent implements OnInit {

  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  //noinspection TypeScriptValidateTypes
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  /*public courseRegReport: any = {
    data: []
  };*/
  public courseRegs: any[] = [];
  public coursesOfStudy: any[] = [];
  public levels: any[] = [];
  public sessions: any[] = [];
  public filterForm: FormGroup;
  public feedBack = {
    loader: false,
    excelling: false,
    session_loader: false,
    course_loader: false,
    level_loader: false,
    rendered: false
  };
  public paginator = {
    per_page: 10,
    page_number: null,
    search: '',
    pages: [1, 2, 3, 4, 5]
  };

  private static filter = () => {
    return {
      programme_id: [1],
      session_year: ['', Validators.required],
      level_id: [''],
      type: [''],
      course_of_study_id: [''],
      course_code: ['']
    };
  }

  constructor(private tablePager: Paginator,
              private notification: NotificationService,
              private reportService: ReportServiceService,
              private schoolService: SchoolService,
              private excelService: CSVExportService,
              private fb: FormBuilder) {
    this.filterForm = this.fb.group(CourseRegistrationComponent.filter());
  }

  ngOnInit() {
    this.getAllCoursesOfStudy();
    this.getAllLevels();
    this.getAllSession();
  }

  private getAllCoursesOfStudy() {
    this.feedBack.course_loader = true;
    this.schoolService.getAllCoursesOfStudy().subscribe(
      (res) => {
        this.feedBack.course_loader = false;
        this.coursesOfStudy = res;
      }, (err) => {
        this.feedBack.course_loader = false;
        this.notification.error('Something happened', err);
      });
  }

  private getAllLevels() {
    this.feedBack.level_loader = true;
    this.schoolService.getAllLevels().subscribe(
      (res) => {
        this.feedBack.level_loader = false;
        this.levels = res;
      }, (err) => {
        this.feedBack.level_loader = false;
        this.notification.error('Something happened', err);
      });
  }

  private getAllSession() {
    this.feedBack.session_loader = true;
    this.schoolService.getAllSession().subscribe(
      (res) => {
        this.feedBack.session_loader = false;
        this.sessions = res;
      }, (err) => {
        this.feedBack.session_loader = false;
        this.notification.error('Something happened', err);
      });
  }

  public changePerPage(num) {
    this.tablePager.changePerPage(num);
    this.getCourseRegReport();
  }

  /*private setPagination() {
    this.tablePager.laravelPaginationObject = this.courseRegReport;
    this.tablePager.setPagination();
    this.tablePager.paginator = this.paginator;
  }*/

  public navigate(page) {
    this.tablePager.navigate(page);
    this.getCourseRegReport();
  }

  public getCourseRegReport() {
    this.feedBack.loader = true;
    this.courseRegs = [];
    this.reportService.courseRegistration(this.paginator, this.filterForm.value).subscribe(
      (res) => {
        // this.courseRegReport = res;
        if (res && res.length) {
          this.feedBack.rendered = true;
          this.courseRegs = res;
          this.dtTrigger.next();
        }
        // this.setPagination();
        this.feedBack.loader = false;
        // this.stats.total_paid = (response.current_page === 1) ? this.sessionReport.transaction_total : this.stats.total_paid;
      },
      error => {
        this.notification.error('Unable to load Report', error);
        this.feedBack.loader = false;
      });
  }

  reRender(): void {
    if (this.feedBack.rendered) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        // Destroy the table first
        dtInstance.destroy();
        // Call the dtTrigger to rerender again
        // this.dtTrigger.next();
        this.getCourseRegReport();
      });
    } else {
      this.getCourseRegReport();
    }
  }

  public exportAsXLSX() {
    this.feedBack.excelling = true;
    const formatted_data = [];
    this.courseRegs.forEach(r => {
      formatted_data.push({
        'First Name': r.first_name,
        'Last Name': r.last_name,
        'Other Names': r.other_names,
        'Matriculation Number': r.matric_no,
        'Course of Study': r.course_of_study.name,
        'Level': (r.level && r.level.name) ? r.level.name : '',
        'LGA': (r.lga && r.lga.name) ? r.lga.name : '',
        'State': (r.state && r.state.name) ? r.state.name : '',
        'First Semester Credits': r.first_semester_total,
        'Total Semester Credits': r.second_semester_total,
        'Total Credit Registered': r.total_credit_registered,
        'Date Registered': r.created_at,
      });
    });
    // console.log(formatted_data);
    this.excelService.exportAsExcelFile(formatted_data, 'Course Registration Report');
    this.feedBack.excelling = false;
  }

}
