import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {StaffConfigService} from "../../../../../../services/api-service/staff-config.service";
import {DataTableDirective} from "angular-datatables";
import {NotificationService} from "../../../../../../services/notification.service";
import {Subject} from "rxjs/Subject";
import {ReportServiceService} from "../../../../../../services/api-service/report-service.service";
import {Paginator} from "../../../../../../utils/paginator";
import {FormBuilder, FormGroup} from "@angular/forms";
import {CSVExportService} from "../../../../../../services/c-sv-export.service";

declare const $;

@Component({
  selector: 'app-hostel-report',
  templateUrl: './hostel-report.component.html',
  styleUrls: ['./hostel-report.component.css']
})
export class HostelReportComponent implements OnInit {

  el: ElementRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  public paginator = {
    per_page: 10,
    page_number: null,
    search: '',
    pages: [1, 2, 3, 4, 5]
  };
  public hostelView: any;
  public hostelReport: any;
  public hostelFilterForm: FormGroup;
  public feedBack = {
    allResponse: [],
    viewDetails: [],
    allHostel: [],
    moduleName: '',
    formType: 'Create',
    loader: false,
    excelling: false,
    hostel_loader: false,
  };
  public search_model: Subject<string> = new Subject<string>();

  static hostel_filter = () => {
    return {
      type: 'allocated',
      hostel_id: null,
      gender: null
    };
  }

  ngOnInit() {
    this.allHostel();
    this.getReportByHostel();
  }

  constructor(private staffConfigService: StaffConfigService,
              private reportService: ReportServiceService,
              private notification: NotificationService,
              private fb: FormBuilder,
              private tablePager: Paginator,
              private excelService: CSVExportService) {
    this.search_model.debounceTime(500)
      .distinctUntilChanged()
      .subscribe(search => {
        this.paginator.search = search;
        this.getReportByHostel();
        this.paginator.page_number = null;
      });
    this.hostelFilterForm = this.fb.group(HostelReportComponent.hostel_filter());
  }

  public changePerPage (num) {
    this.tablePager.changePerPage(num);
    this.getReportByHostel();
  }

  public navigate (page) {
    this.tablePager.navigate(page);
    this.getReportByHostel();
  }

  private setPagination () {
    this.tablePager.laravelPaginationObject = this.hostelReport;
    this.tablePager.setPagination();
    this.tablePager.paginator = this.paginator;
  }

  public allHostel() {
    this.feedBack.hostel_loader = true;
    this.staffConfigService.getAllHostel1()
      .subscribe((response) => {
          this.feedBack.hostel_loader = false;
          this.feedBack.allHostel = response.data;
        },
        error => {
          this.feedBack.hostel_loader = false;
          this.notification.error('Unable to load Hostels, please retry', error);
        });
  }

  public getReportByHostel() {
    this.feedBack.loader = true;
    this.reportService.getHostel(this.hostelFilterForm.value, this.paginator).subscribe((response) => {
      // this.feedBack.allResponse = response;
      this.hostelReport = response;
      this.setPagination();
      this.feedBack.loader = false;
      },
      error => {
        this.notification.error('Unable to load Report', error);
        this.feedBack.loader = false;
      });
  }

  public deleteAllocation (allocation) {
    allocation['deleting'] = true;
    // const index = this.hostelReport['data'].indexOf(allocation);
    const index = this.feedBack.allResponse.indexOf(allocation);
    this.reportService.deleteAllocation(allocation.id).subscribe(res => {
      allocation['deleting'] = false;
      this.notification.success(`Deleted successfully!`);
      this.feedBack.allResponse.splice(index, 1);
    }, err => {
      allocation['deleting'] = false;
      this.notification.error('An Error occurred. Please retry', err);
    });
  }

  public exportAsXLSX () {
    this.feedBack.excelling = true;
    this.reportService.hostelReportStats(this.hostelFilterForm.value).subscribe(res => {
      const formatted_data = [];
      if (res && res.length) {
        res.forEach(r => {
          formatted_data.push({
            'Name': r.student.last_name + ' ' + r.student.first_name,
            'Matric Number': r.student.matric_no,
            'Transaction ID': r.transaction_id,
            'Hostel': (r && r.bed_space && r.bed_space.room &&  r.bed_space.room.hostel && r.bed_space.room.hostel.hostel_name) ? r.bed_space.room.hostel.hostel_name : '-',
            'Room': (r && r.bed_space && r.bed_space.room && r.bed_space.room.room_name) ? r.bed_space.room.room_name : '-',
            'Bed Space': (r && r.bed_space && r.bed_space.label) ? r.bed_space.label : '-',
            'Date Paid': r.updated_at,
          });
        });
      }
      console.log(formatted_data);
      this.excelService.exportAsExcelFile(formatted_data, 'Hostel Allocation Report');
      this.feedBack.excelling = false;
    }, err => {
      this.notification.error('Error Generating Excel Sheet.');
      this.feedBack.excelling = false;
    });
  }

  public reRender(event): void {
    // this.feedBack.config.type = event.target.value;
    this.getReportByHostel();
  }

  public viewHostel (hostel) {
    this.hostelView = hostel;
    this.triggerModalOrOverlay('open', 'hostelAllocation');
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }


}
