import {Component, OnInit} from '@angular/core';
import {Cache} from '../../../../../../utils/cache';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {NotificationService} from '../../../../../../services/notification.service';
import {SchoolService} from '../../../../../../services/school.service';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {ReportServiceService} from "../../../../../../services/api-service/report-service.service";

@Component({
  selector: 'app-report-filter',
  templateUrl: './report-filter.component.html',
  styleUrls: ['./report-filter.component.css']
})
export class ReportFilterComponent implements OnInit {
  public feedBack = {
    moduleName: 'Reports',
    selection: 'Session Transaction',
    noStudent: '',
    loading: false,
    studentloading: false,
    allSession: [],
    allDepartments: [],
    allProgramme: [],
    allStudents: [],
    allCourseofStudy: [],
    loadSession: false,
    loadDepartment: false,
    loadCos: false
  };

  public searchTerm = new FormControl();
  public payments = [];
  public studentList = [];
  public modeForm: FormGroup;
  public sessionForm: FormGroup;
  public dateForm: FormGroup;
  public studentForm: FormGroup;
  public departmentForm: FormGroup;
  public cosForm: FormGroup;
  public programmeForm: FormGroup;

  static modelData = function () {
    return {
      payment: ['', Validators.compose([Validators.required])]
    };
  };

  static sessionData = function () {
    return {
      session: ['', Validators.compose([Validators.required])]
    };
  };

  static dateData = function () {
    return {
      fromdate: ['', Validators.compose([Validators.required])],
      todate: ['', Validators.compose([Validators.required])]
    };
  };

  static studentData = function () {
    return {
      student: ['', Validators.compose([Validators.required])]
    };
  };

  static departmentData = function () {
    return {
      department: ['', Validators.compose([Validators.required])]
    };
  };

  static cosData = function () {
    return {
      cos: ['', Validators.compose([Validators.required])]
    };
  };

  static programmeData = function () {
    return {
      programme: ['', Validators.compose([Validators.required])]
    };
  };

  constructor(private staffConfigService: StaffConfigService,
              private schoolService: SchoolService,
              private reportService: ReportServiceService,
              private notification: NotificationService,
              private router: Router,
              private fb: FormBuilder) {
    this.modeForm = this.fb.group(ReportFilterComponent.modelData());
    this.sessionForm = this.fb.group(ReportFilterComponent.sessionData());
    this.dateForm = this.fb.group(ReportFilterComponent.dateData());
    this.studentForm = this.fb.group(ReportFilterComponent.studentData());
    this.departmentForm = this.fb.group(ReportFilterComponent.departmentData());
    this.cosForm = this.fb.group(ReportFilterComponent.cosData());
    this.programmeForm = this.fb.group(ReportFilterComponent.programmeData());

    this.searchTerm.valueChanges
     .debounceTime(500)
      .distinctUntilChanged()
     .subscribe(data => {
       this.studentList = [];
       this.feedBack.studentloading = true;
       this.schoolService.searchStudentByName(data).subscribe(response => {
          this.studentList = response['data'];
          this.feedBack.studentloading = false;
          if(this.studentList.length === 0){
            this.feedBack.noStudent = 'No Student with such name';
          }
       });
     });
  }

  ngOnInit() {
    this.loadFunction();
  }

  loadFunction() {
    if (Cache.get('ping')) {
      this.payments = Cache.get('ping')['settings']['payment_method'];
    }

    this.reportService.selectReport
      .subscribe(response => {
        this.router.navigate(['staff/reports']);
        this.onSelect(response);
       });
    this.onSelect(Cache.get('report_name'));
    this.allSession();
    this.allCourseofStudy();
    this.allDepartment();
   // this.getAllStudents();
    this.getAllProgrammes();
  }

  /**
   * listing all session
   */
  public allSession() {
    this.feedBack.loading = true;
    this.staffConfigService.getAllSession()
      .subscribe((response) => {
          this.feedBack.allSession = response.data;
          this.feedBack.loading = false;
        },
        error => {
          this.notification.error('Unable to load session, please retry');
          this.feedBack.loading = false;
        });
  }

  /**
   * listing all departments
   */
  public allDepartment() {
    this.feedBack.loading = true;
    this.staffConfigService.getAllDepartment()
      .subscribe((response) => {
          this.feedBack.allDepartments = response.data;
          this.feedBack.loading = false;
        },
        error => {
          this.notification.error('Unable to load departments please retry');
          this.feedBack.loading = false;
        });
  }

  /**
   * listing all course of study
   */
  public allCourseofStudy() {
    this.feedBack.loading = true;
    this.staffConfigService.getAllCourse()
      .subscribe((response) => {
          this.feedBack.loading = false;
          this.feedBack.allCourseofStudy = response.data;
        },
        error => {
          this.feedBack.loading = false;
          this.notification.error('Unable to load ' + this.feedBack.moduleName + ', please retry');
        });
  }

  /**
   * get all students
   */
  public getAllStudents() {
    this.feedBack.loading = true;
    this.schoolService.getAllStudents().subscribe(
      (response) => {
        console.log('all students :: ', response);
        this.feedBack.loading = false;
        this.feedBack.allStudents = response;
      },
      (error) => {
        this.feedBack.loading = false;
        this.notification.error('Unable to load Students', error);
      }
    );
  }

  /**
   * get all programmess
   */
  public getAllProgrammes() {
    this.feedBack.loading = true;
    this.staffConfigService.getAllProgramme()
      .subscribe((response) => {
          this.feedBack.loading = false;
          this.feedBack.allProgramme = response.data;
        },
        (error) => {
          this.feedBack.loading = false;
          this.notification.error('Unable to load Students', error);
        }
      );
  }


  onSubmitMode() {
    Cache.set('payment_mode', this.modeForm.value.payment);
    this.router.navigate(['staff/reports/mode']);
  }

  onSubmitSession() {
    this.router.navigate(['staff/reports/session', this.sessionForm.value.session]);
  }

  onSubmitDate() {
    Cache.set('formdate', {'fromdate': this.dateForm.value.fromdate, 'toDate': this.dateForm.value.todate});
    this.router.navigate(['staff/reports/date']);
  }

  onSubmitStudent(id) {
    // this.router.navigate(['staff/reports/student', this.studentForm.value.student]);
    this.router.navigate(['staff/reports/student', id]);
  }

  onSubmitDepartment() {
    this.router.navigate(['staff/reports/department', this.departmentForm.value.department]);
  }

  onSubmitCos() {
    this.router.navigate(['staff/reports/course-of-study', this.cosForm.value.cos]);
  }

  onSubmitCourseByDepartment() {
    this.router.navigate(['staff/reports/department', this.departmentForm.value.department]);
  }

  onSubmitCourseOfStudyByDepartment() {
    this.router.navigate(['staff/reports/course-of-study', this.departmentForm.value.department]);
  }

  onSelect(event) {
    this.feedBack.selection = event;
  }

  onSubmitProgramme() {
    this.router.navigate(['staff/reports/admission-by-programme', this.programmeForm.value.programme]);
  }

}
