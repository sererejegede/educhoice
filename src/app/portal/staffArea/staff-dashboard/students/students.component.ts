import {Component, ElementRef, OnInit, ViewChild} from "@angular/core";
import {DataTableDirective} from "angular-datatables";
import {Subject} from "rxjs/Rx";

import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import * as utils from "../../../../../utils/util";
import {Faculty} from "../../../../../interfaces/faculty.inteface";
import {Department} from "../../../../../interfaces/department.interface";
import {Course} from "../../../../../interfaces/course.interface";
import {ScriptLoaderService} from "../../../../../services/script-loader.service";
import {SchoolService} from "../../../../../services/school.service";
import {UserService} from "../../../../../services/user.service";
import {NotificationService} from "../../../../../services/notification.service";
import {StaffConfigService} from "../../../../../services/api-service/staff-config.service";
import {AdmissionsService} from "../../../../../services/api-service/admissions.service";
import {AuthenticationService} from "../../../../../services/authentication.service";
import {CSVExportService} from "../../../../../services/c-sv-export.service";
import {Cache} from "../../../../../utils/cache";
import {CONSTANTS} from "../../../../../utils/constants";


declare const $: any;

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {


  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  // dtElement: DataTableDirective = new DataTableDirective(this.el);
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  public allLevels: any[] = [];
  public allSession: any[] = [];
  public allChristianDenominations: any[] = [
    {
      name: "Anglican",
      value: "Anglican"
    },
    {
      name: "Baptist",
      value: "Baptist"
    },
    {
      name: "Catholic",
      value: "Catholic"
    },
    {
      name: "Methodist",
      value: "Methodist"
    },
    {
      name: "Presbyterian",
      value: "Presbyterian"
    }
  ];
  public allDenominations: any[] = [];
  public allLgas: any[] = [];
  public overlay = utils.overlay;
  public allStudents: any[] = [];
  public studentStats: any[] = [];
  public updatedStudent: any;
  public tableWasOnceRendered: boolean = false;
  public showStatsForm: boolean = false;
  public templateUrl: string = '';
  public allFaculties: Faculty[] = [];
  public allDepartments: Department[] = [];
  public allCoursesofStudy: Course[] = [];
  public allStates: any[] = [];
  public allCountries: any[] = [];
  public authenticatedUser: any;
  public createStudentForm: FormGroup;
  public updateStudentForm: FormGroup;
  public studentBulkUploadForm: FormGroup;
  public studentFilterForm: FormGroup;
  public studentStatsForm: FormGroup;
  public editedIndex: number;
  public appSettings: any;
  public searchResult: any[] = [];
  public studentSearch = {
    loading: false,
    done: false
  };
  public searchTerm = new FormControl();

  public load = {
    requesting: {
      list: false,
      create: false
    },
    download: false,
    uploading: false,
    statistics: false,
    message: {
      create: 'Create',
      update: 'Update'
    },
    departments: {
      list: false,
      loading: false
    },
    courses: {
      list: false,
      loading: false
    },
    students: {
      list: false,
      loading: false
    },
    level: {
      list: false,
      loading: false
    },
    session: {
      list: false,
      loading: false
    },
    lgas: {
      list: false,
      loading: false
    },
    loading_states: false,
    excelling: false
  };

  static createStudentForm = function () {
    return {
      email: ['', Validators.compose([Validators.required, Validators.email])],
      first_name: ['', Validators.compose([Validators.required])],
      other_names: '',
      last_name: ['', Validators.compose([Validators.required])],
      faculty_id: ['', Validators.compose([Validators.required])],
      department_id: ['', Validators.compose([Validators.required])],
      course_of_study_id: ['', Validators.compose([Validators.required])],
      country_id: [''],
      state_id: '',
      level_id: ['', Validators.compose([Validators.required])],
      matric_no: [''],
      lga_id: [''],
      birth_of_date: [''],
      gender: ['', Validators.compose([Validators.required])],
      religion: [''],
      denomination: [''],
      home_town: [''],
      marital_status: ['']

    };
    // email,first_name,last_name,course_of_study_id
  };

  static updateStudentForm = function () {
    return {
      email: ['', Validators.compose([Validators.required, Validators.email])],
      first_name: ['', Validators.compose([Validators.required])],
      other_names: [''],
      last_name: ['', Validators.compose([Validators.required])],
      // faculty_id: ['', Validators.compose([Validators.required])],
      // department_id: ['', Validators.compose([Validators.required])],
      // course_of_study_id: ['', Validators.compose([Validators.required])],
      country_id: [''],
      state_id: [''],
      // level_id: ['', Validators.compose([Validators.required])],
      matric_no: [''],
      lga_id: [''],
      birth_of_date: [''],
      gender: [''],
      religion: [''],
      denomination: [''],
      home_town: [''],
      marital_status: ['']
    };

  };

  static studentBulkUploadForm = function () {
    return {
      student_bulk: ['', Validators.compose([Validators.required])]
    };
  };

  static studentFilter = function () {
    return {
      faculty_id: [''],
      department_id: [''],
      course_of_study_id: [''],
      level_id: [''],
      gender: [''], // = male/female
      year: [''] // = session year
    };
  };

  static studentStatsFilter = () => {
    return {
      session_year: ['', Validators.required]
    }
  };

  constructor(private _script: ScriptLoaderService,
              private userService: UserService,
              private  el: ElementRef,
              private schoolService: SchoolService,
              private authService: AuthenticationService,
              private fb: FormBuilder,
              private staffConfigService: StaffConfigService,
              private admissionsService: AdmissionsService,
              private Alert: NotificationService,
              private excelService: CSVExportService) {
    // this.authenticatedUser = this.userService.getAuthUser().login.user;
    this.searchStudent();
    this.createStudentForm = this.fb.group(StudentsComponent.createStudentForm());
    this.updateStudentForm = this.fb.group(StudentsComponent.updateStudentForm());
    this.studentBulkUploadForm = this.fb.group(StudentsComponent.studentBulkUploadForm());
    this.studentFilterForm = this.fb.group(StudentsComponent.studentFilter());
    this.studentStatsForm = this.fb.group(StudentsComponent.studentStatsFilter());
    this.dtOptions = {
      pagingType: 'full_numbers',
    };
  }

  ngOnInit() {
    // this.getAllStudents();
    this.getAllCountries();
    this.getAllFaculties();
    this.getAllDepartments();
    this.getAllCoursesOfStudy();
    this.getAllSessionList();
    this.getAllLevelList();
    this.appSettings = Cache.get('app_settings') || Cache.get('ping');
  }

  private searchStudent() {
    // if (this.searchTerm.value) {
      this.searchTerm.valueChanges
        .debounceTime(400)
        .subscribe(data => {
          this.studentSearch.loading = true;
          this.schoolService.searchStudentByName(data).subscribe(response => {
              this.studentSearch.loading = false;
              this.studentSearch.done = true;
              this.searchResult = response['data'];
            },
            (error) => {
              this.Alert.error(`Sorry your search for ${this.searchTerm.value} failed`, error);
            });
        });
    // }
  }

  // ngAfterViewInit() {
  //     this._script.loadScripts('app-students',
  //         ['assets/student_assets/demo/demo4/base/scripts.bundle.js', 'assets/student_assets/app/js/dashboard.js']);

  // }

  public getAllStudents() {
    this.load.requesting.list = true;
    this.allStudents = [];
    this.schoolService.getAllFilteredStudents(this.studentFilterForm.value).subscribe(
      (allStudentsResponse) => {
        this.load.requesting.list = false;
        this.allStudents = allStudentsResponse;
        // this.renderStudents();
        if (this.allStudents.length > 0) {
          // this.renderStudents();
          if (this.tableWasOnceRendered) {
            // console.log('Was table once rendered?', this.tableWasOnceRendered);
            // this.rerender();
          } else {
            this.dtTrigger.next();
            this.tableWasOnceRendered = true;
          }
          // this.rerender();
        }
        // console.log('All Students ', this.allStudents);
      },
      (error) => {
        this.Alert.error('Could not load students list');
        console.log('Could not load list of students', error);
        this.load.requesting.list = false;
      }
    );
  }

  public getStudentStatistics() {
    this.load.statistics = true;
    this.schoolService.studentStatistics(this.studentStatsForm.value).subscribe((res) => {
      this.load.statistics = false;
      this.studentStats = res;
      this.showStatsForm = false;
    }, (err) => {
      this.load.statistics = false;
      this.Alert.error(CONSTANTS.DEFAULT_ERROR_MESSAGE, err);
    });
  }

  public async createStudent() {
    this.load.requesting.create = true;
    this.load.message.create = 'Creating';
    await this.schoolService.createStudent(this.createStudentForm.value).subscribe(
      (createdStudentResponse) => {
        this.load.message.create = 'Create';
        this.load.requesting.create = false;
        this.Alert.success(`${this.createStudentForm.value.first_name} created successfully\n`);
        this.createStudentForm = this.fb.group(StudentsComponent.createStudentForm());
        this.triggerModalOrOverlay('close', 'createStudent');
        createdStudentResponse['status'] = '1';
        this.allStudents.unshift(createdStudentResponse);
        // console.log('Newly created student ', createdStudentResponse);
      },
      (error) => {
        this.load.message.create = 'Create';
        this.load.requesting.create = false;
        // this.triggerModal('close','createSchool');
        console.log('Error ', error);
        this.Alert.error(`Could not create ${this.createStudentForm.value.first_name}`, error);
      }
    );
  }

  public updateStudent() {
    this.load.requesting.create = true;
    this.load.message.update = 'Updating...';
    this.schoolService.updateStudent(this.updateStudentForm.value, this.updatedStudent.id).subscribe(
      (updatedStudentResponse) => {
        this.load.message.update = 'Update';
        this.updateStudentForm = this.fb.group(StudentsComponent.updateStudentForm());
        this.Alert.success(`${this.updateStudentForm.value.first_name} Updated successfully\n`);
        // this.allStudents[this.editedIndex] = updatedStudentResponse;
        this.allStudents.splice(this.editedIndex, 1, updatedStudentResponse);
        this.load.requesting.create = false;
        this.triggerModalOrOverlay('close', 'updateStudent');
      },
      (error) => {
        this.load.message.update = 'Update';
        this.load.requesting.create = false;
        this.Alert.error(`Could not update ${this.updateStudentForm.value.first_name}`, error);
      }
    );
  }

  public async uploadStudentsInBulk(fileId) {
    this.load.requesting.create = true;
    this.load.message.create = 'Creating...';
    console.log('Create PUTME object ', this.studentBulkUploadForm.value);
    const bulkFileId = `#${fileId}`;
    const formFile = this.el.nativeElement.querySelector(bulkFileId);
    // console.log('Form File ', formFile);
    const $linkId = $(bulkFileId);
    const $newLinkId = $linkId.val();
    const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
    // const file_type = '';
    $linkId.removeClass('alert alert-danger animated rubberBand');
    if (file_type && (file_type !== '.csv') && (file_type !== '.docx') && (file_type !== '.xlsx')) {
      $linkId.addClass('alert alert-danger animated rubberBand');
      this.load.requesting.create = false;
      return this.Alert.error('You can only upload csv, docx or xls files');
    }
    await this.schoolService.uploadStudentsInBulk(formFile).subscribe(
      (fileResponse) => {
        // this.allPutmes = this.formatPutmeList(fileResponse['records']); uncomment this after Dammy fixes import response format
        this.Alert.success(`Admission list has been uploaded successfully\n`);
        this.studentBulkUploadForm = this.fb.group(StudentsComponent.studentBulkUploadForm());
        // this.allStudents = [];
        this.renderStudents();
        this.load.requesting.create = false;
        this.triggerModalOrOverlay('close', 'studentBulkTemplate');
        // console.log('File upload Response', fileResponse);
      },
      (error) => {
        this.load.requesting.create = false;
        this.Alert.error('An error occurred uploading file', error);
      }
    );
  }

  public downloadTemplate(ind, buttonId: string) {
    this.schoolService.getStudentBulkTemplate().subscribe((templateUrl) => {
        // this.feedBack.loader = false;
        this.templateUrl = templateUrl;
        const downloadButton = $(`#${buttonId}`);
        downloadButton.attr('href', this.templateUrl);
        downloadButton[0].click();
        console.log('Download button', downloadButton);
      },
      (error) => {
        // this.load.download.loading = false;
        this.Alert.error('An error occured, could not download template', error);
      });
  }

  public renderStudents(): void {
    if (this.tableWasOnceRendered) {
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          this.tableWasOnceRendered = false;
          // Call the dtTrigger to rerender again

        },
        (rejection) => {
        });
    }

    this.getAllStudents();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
      this.tableWasOnceRendered = false;
      // this.getAllStudents();
    });
  }


  public getDenomination(value) {
    (value === 'Christianity') ? this.allDenominations = this.allChristianDenominations : this.createStudentForm.removeControl('denomination');
  }


  public getAllFaculties() {
    this.schoolService.getAllFaculties().subscribe(
      (facultiesResponse) => {
        console.log('All faculties ', facultiesResponse);
        this.allFaculties = facultiesResponse;
      },
      (error) => {
        this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
      }
    );
  }

  /**
   * This method returns a single faculty which contains a list of departments
   */
  public getDepartmentByFacultyId(facultyId) {
    this.load.departments.list = true;
    this.load.departments.loading = true;
    this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
      (departmentResponse) => {
        this.load.departments.loading = false;
        this.allDepartments = departmentResponse.departments;
      },
      (error) => {
        this.load.departments.list = false;
        this.load.departments.loading = false;
        this.Alert.error(`Sorry could not load departments`, error);
      }
    );
  }

  public getAllCoursesOfStudy() {
    this.schoolService.getAllCourses().subscribe(
      (res) => {
        this.allCoursesofStudy = res;
      },
      (error) => {
        this.Alert.error('Network error', error);
      });
  }

  public getAllDepartments() {
    this.staffConfigService.getDepartmentList().subscribe(res => {
      this.allDepartments = res;
    }, err => {
      this.Alert.error('Network error', err);
    });
  }

  public getAllSessionList() {
    this.staffConfigService.getSessionList().subscribe(res => {
      this.allSession = res;
    }, err => {
      this.Alert.error('Network error', err);
    });
  }

  public getAllLevelList() {
    this.staffConfigService.getLevelList().subscribe(res => {
      this.allLevels = res;
    }, err => {
      this.Alert.error('Network error', err);
    });
  }

  public getSingleCourseOfStudy($event) {
    this.load.level.loading = true;
    this.load.session.loading = true;
    this.staffConfigService.getSingleCourseOfStudy($event)
      .subscribe((response) => {
          this.load.level.loading = false;
          this.load.session.loading = false;
          this.allLevels = response['degree']['programme']['levels'];
          this.allSession = response['degree']['programme']['sessions'];
        },
        (error) => {
          this.load.level.loading = false;
          this.load.session.loading = false;
          console.log('Level error', error);
        });
  }

  public getCourseByDepartmentId(departmentId) {
    this.load.courses.list = true;
    this.load.courses.loading = true;
    this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
      (coursesResponse) => {
        this.load.courses.loading = false;
        this.allCoursesofStudy = coursesResponse.course_of_studies;
        console.log('returned courses', coursesResponse);
      },
      (error) => {
        this.load.courses.list = false;
        this.load.courses.loading = false;
        this.Alert.error(`Sorry could not load courses`, error);
      }
    );
  }

  public getAllCountries() {
    this.schoolService.getAllCountries().subscribe(
      (countriesResponse) => {
        this.allCountries = countriesResponse;
      },
      (error) => {
        this.Alert.error(`Sorry could not load countries`, error);
      }
    );
  }

  public getStateByCountryId(countryId) {
    this.load.loading_states = true;
    this.schoolService.getStateByCountryId(countryId).subscribe(
      (statesResponse) => {
        this.load.loading_states = false;
        this.allStates = statesResponse.states;
      },
      (error) => {
        this.load.loading_states = false;
        this.Alert.error(`Sorry could not load States`, error);
      }
    );
  }

  public logStaffIn() {
    this.schoolService.logStaffIn().subscribe(
      (loginResponse) => {
        this.userService.setAuthUser(loginResponse.token);

      }
    );
  }

  public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
    if (ind >= 0) {
      this.allStudents[ind].loading = true;
      this.editedIndex = ind;
      this.updatedStudent = this.allStudents[ind];
      this.updateStudentForm.patchValue(this.updatedStudent);
    }
    (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }

  public editStudent(student, index) {
    this.updatedStudent = student;
    this.editedIndex = index;
    this.updateStudentForm.patchValue(student);
    this.triggerModalOrOverlay('open', 'updateStudent');
  }

  public getLgaByStateId(stateId) {
    this.load.lgas.loading = true;
    let dataItem = this.selectWhereId(this.allStates, 'id', stateId);
    this.admissionsService.getLgaByStateId(stateId).subscribe(
      (lgasResponse) => {
        this.load.lgas.loading = false;
        this.allLgas = lgasResponse.lgas;
      },
      (error) => {
        this.load.lgas.loading = false;
        this.Alert.error(`Sorry could not load Lgas for`, error);
        // ${this.selectWhereId(this.allStates, stateId).name}
      });
  }

  public selectWhereId(data: any[], key, id) {
    const dataItem: any[] = [];
    data.forEach(item => {
      const itemKey = parseInt(item[key], 10);
      const itemId = parseInt(id, 10);
      if (itemKey === itemId) {
        dataItem.push(item);
      }
    });
    return dataItem[0];
  }

  public exportAsXLSX() {
    this.load.excelling = true;
    // this.schoolService.getAllFilteredStudents(this.studentFilterForm.value).subscribe(res => {
    const formatted_data = [];
    if (this.allStudents && this.allStudents.length) {
      this.allStudents.forEach(student => {
        formatted_data.push({
          'Matric Number': student.matric_no,
          'Name': student.full_name,
          // 'First Name': student.first_name,
          // 'Middle Name': student.middle_name,
          'Faculty': student.course_of_study.department.faculty.name,
          'Department': student.course_of_study.department.name,
          'Course of Study': student.course_of_study.name,
          'Gender': student.gender ? student.gender.toUpperCase() : '',
          'State': (student.state) ? (student.state.name ? student.state.name : '') : '',
          'LGA': (student.lga) ? (student.lga.name ? student.lga.name : '') : '',
          'Religion': student.religion,
          'Level': student.level.name,
        });
      });
    }
    // console.log(formatted_data);
    this.excelService.exportAsExcelFile(formatted_data, 'Students Report');
    this.load.excelling = false;
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  public downloadMatricUploadTemplate(buttonId: string) {
    this.load.download = true;
    // this.selectedCurriculumCourseId = this.feedBack.allResponse['curriculum_courses'][ind].id;
    this.staffConfigService.downloadMatricUploadTemplate()
      .subscribe((url) => {
          this.load.download = false;
          // const templateUrl = url;
          const downloadButton = $(`#${buttonId}`);
          downloadButton.attr('href', url);
          downloadButton[0].click();
          // console.log('Download button', downloadButton);
        },
        (error) => {
          this.load.download = false;
          this.Alert.error('An error occured, could not download template', error);
        });
  }

  public uploadMatricTemplate(fileId) {
    if (!window.confirm('Are you sure you want submit')) {
      return;
    }
    this.load.uploading = true;
    const bulkFileId = `#${fileId}`;
    const formFile = this.el.nativeElement.querySelector(bulkFileId);
    // console.log('Form File ', formFile);
    const $linkId = $(bulkFileId);
    const $newLinkId = $linkId.val();
    const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
    $linkId.removeClass('alert alert-danger animated rubberBand');
    if (file_type && (file_type !== '.pdf') && (file_type !== '.docx') && (file_type !== '.doc') && (file_type !== '.xlsx')) {
      $linkId.addClass('alert alert-danger animated rubberBand');
      this.load.uploading = false;
      return this.Alert.error('You can only upload pdf, docx or doc files');
    }
    this.staffConfigService.uploadMatricTempate(formFile)
      .subscribe((res) => {
          this.load.uploading = false;
          this.Alert.success(res.message || 'Success.');
        },
        (error) => {
          this.load.uploading = false;
          this.Alert.error('An error occurred uploading file', error);
        }
      );

  }

}
