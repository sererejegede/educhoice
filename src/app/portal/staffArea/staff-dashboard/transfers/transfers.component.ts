import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';
import {Cache} from "../../../../../utils/cache";
import {Validators, FormGroup, FormBuilder} from '@angular/forms';
import * as utils from '../../../../../utils/util';
import {Faculty} from '../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../interfaces/department.interface';
import {Course} from '../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../services/school.service';
import {UserService} from '../../../../../services/user.service';
import {NotificationService} from '../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../services/api-service/staff-config.service';
import {AdmissionsService} from "../../../../../services/api-service/admissions.service";


declare const $: any;

@Component({
    selector: 'app-transfers',
    templateUrl: './transfers.component.html',
    styleUrls: ['./transfers.component.css']
})
export class TransfersComponent implements OnInit {

    el: ElementRef;
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject();
    dtElement: DataTableDirective = new DataTableDirective(this.el);

    public allLevels: any[] = [];
    public allChristianDenominations: any[] = [
        {
            name: "Anglican",
            value: "Anglican"
        },
        {
            name: "Baptist",
            value: "Baptist"
        },
        {
            name: "Catholic",
            value: "Catholic"
        },
        {
            name: "Methodist",
            value: "Methodist"
        },
        {
            name: "Presbyterian",
            value: "Presbyterian"
        }
    ];
    public allDenominations: any[] = [];
    public allLgas: any[] = [];
    public overlay = utils.overlay;
    public allTransfers: any[] = [];
    public updatedTransfer: any;
    public allFaculties: Faculty[] = [];
    public allDepartments: Department[] = [];
    public allCourses: Course[] = [];
    public allStates: any[] = [];
    public allCountries: any[] = [];
    public authenticatedUser: any;
    public myTransfers: any[] = [];
    public myEditedTransfer: any = {};
    public createTransferForm: FormGroup;
    public updateTransferForm: FormGroup;
    public editedIndex: number;
    public load = {
        requesting: {
            list: true,
            create: false
        },
        message: {
            create: 'Create',
            update: 'Update'
        },
        departments: {
            list: false,
            loading: false
        },
        courses: {
            list: false,
            loading: false
        },
        Transfers: {
            list: false,
            loading: false
        },
        lgas: {
            list: false,
            loading: false
        }

    };


    static createTransferForm = function () {
        return {
            email: ['', Validators.compose([Validators.required, Validators.email])],
            first_name: ['', Validators.compose([Validators.required])],
            other_names: '',
            last_name: ['', Validators.compose([Validators.required])],
            faculty_id: ['', Validators.compose([Validators.required])],
            department_id: ['', Validators.compose([Validators.required])],
            course_of_study_id: ['', Validators.compose([Validators.required])],
            country_id: ['', Validators.compose([Validators.required])],
            state_id: '',
            level_id: ['', Validators.compose([Validators.required])],
            matric_no: ['', Validators.compose([Validators.required])],
            lga_id: ['', Validators.compose([Validators.required])],
            birth_of_date: ['', Validators.compose([Validators.required])],
            gender: ['', Validators.compose([Validators.required])],
            religion: ['', Validators.compose([Validators.required])],
            denomination: ['', Validators.compose([Validators.required])],
            home_town: ['', Validators.compose([Validators.required])],
            marital_status: ['', Validators.compose([Validators.required])],
            middle_name: ''

        };
        // email,first_name,last_name,course_of_study_id
    };


    static updateTransferForm = function () {

        return {
            email: ['', Validators.compose([Validators.required, Validators.email])],
            first_name: ['', Validators.compose([Validators.required])],
            other_names: '',
            last_name: ['', Validators.compose([Validators.required])],
            faculty_id: ['', Validators.compose([Validators.required])],
            department_id: ['', Validators.compose([Validators.required])],
            course_of_study_id: ['', Validators.compose([Validators.required])],
            country_id: ['', Validators.compose([Validators.required])],
            state_id: '',
            level_id: ['', Validators.compose([Validators.required])],
            matric_no: ['', Validators.compose([Validators.required])],
            lga_id: ['', Validators.compose([Validators.required])],
            birth_of_date: ['', Validators.compose([Validators.required])],
            gender: ['', Validators.compose([Validators.required])],
            religion: ['', Validators.compose([Validators.required])],
            denomination: '',
            home_town: ['', Validators.compose([Validators.required])],
            marital_status: ['', Validators.compose([Validators.required])],
            middle_name: ''
        };
    };

    constructor(private _script: ScriptLoaderService,
                private userService: UserService,
                private schoolService: SchoolService,
                private fb: FormBuilder,
                private staffConfigService: StaffConfigService,
                private admissionsService: AdmissionsService,
                private Alert: NotificationService) {
        // this.authenticatedUser = this.userService.getAuthUser().login.user;
        this.createTransferForm = this.fb.group(TransfersComponent.createTransferForm());
        this.updateTransferForm = this.fb.group(TransfersComponent.updateTransferForm());
        this.dtOptions = {
            pagingType: 'full_numbers',
        };
        console.log('Authenticated user ', this.authenticatedUser);
    }

    ngOnInit() {
        this.getAllTransfers();
        this.getAllCountries();
        this.getAllFaculties();
        this.getAllLevels();
        // this.triggerModalOrOverlay('open','createTransfer')
    }

    // ngAfterViewInit() {
    //     this._script.loadScripts('app-Transfers',
    //         ['assets/Transfer_assets/demo/demo4/base/scripts.bundle.js', 'assets/Transfer_assets/app/js/dashboard.js']);

    // }

    public getAllTransfers() {
        this.load.requesting.list = true;
        this.schoolService.getAllTransfers().subscribe(
            (allTransfersResponse) => {
                this.load.requesting.list = false;
                this.allTransfers = allTransfersResponse.data;
                console.log('All transfers response ', allTransfersResponse)
                if (this.allTransfers.length > 0) {
                    this.dtTrigger.next();
                }
                console.log('All Transfers ', this.allTransfers);
            },
            (error) => {
                console.log('Could not load list of Transfers')
                this.load.requesting.list = false;

            }
        );
    }

    public async createTransfer() {

        this.load.requesting.create = true;
        this.load.message.create = 'Creating...';

        console.log('Create Transfer object ', this.createTransferForm.value);
        await this.schoolService.creatStudentTransfer(this.createTransferForm.value).subscribe(
            (createdTransferResponse) => {
                this.load.message.create = 'Create';
                this.Alert.success(`${this.createTransferForm.value.first_name} created successfully\n`);
                this.createTransferForm = this.fb.group(TransfersComponent.createTransferForm());
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'createTransfer');
                createdTransferResponse['status'] = '1';
                this.allTransfers.unshift(createdTransferResponse);
                console.log('Newly created Transfer ', createdTransferResponse);
            },
            (error) => {
                this.load.message.create = 'Create';
                // this.triggerModal('close','createSchool');
                console.log('Eroorroroor ', error);
                this.load.requesting.create = false;
                this.Alert.error(`Could not create ${this.createTransferForm.value.first_name}`, error);
            }
        );
    }

    public updateTransfer() {
        this.load.requesting.create = true;
        this.load.message.update = 'Updating...';

        this.schoolService.updateStudentTransfer(this.updateTransferForm.value, this.updatedTransfer.id).subscribe(
            (updatedTransferResponse) => {
                this.load.message.update = 'Update';
                this.updateTransferForm = this.fb.group(TransfersComponent.updateTransferForm());
                this.Alert.success(`${this.updateTransferForm.value.first_name} updated successfully\n`);
                this.allTransfers[this.editedIndex] = updatedTransferResponse;
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'updateTransfer');

                console.log('Updated school ', this.updateTransferForm);
            },
            (error) => {
                this.load.message.update = 'Update';
                this.load.requesting.create = false;
                this.Alert.error(`Could not update ${this.updateTransferForm.value.first_name}`, error);
            }
        );
    }

    public getDenomination(value) {
        this.allDenominations = (value === 'Christianity') ? this.allChristianDenominations : [];
    }


    public getAllFaculties() {
        this.schoolService.getAllFaculties().subscribe(
            (facultiesResponse) => {
                console.log('All faculties ', facultiesResponse);
                this.allFaculties = facultiesResponse;
            },
            (error) => {
                this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
            }
        );
    }

    /**
     * This method returns a single faculty which contains a list of departments
     */
    public getDepartmentByByFacultyId(facultyId) {
        this.load.departments.list = true;
        this.load.departments.loading = true;

        this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
            (departmentResponse) => {
                this.load.departments.loading = false;

                this.allDepartments = departmentResponse.departments;
                console.log('returned departments', departmentResponse);
            },
            (error) => {
                this.load.departments.list = false;
                this.load.departments.loading = false;

                this.Alert.error(`Sorry could not load departments`, error);
            }
        );
    }

    public async getAllCourses() {
        await this.schoolService.getAllCourses().subscribe(
            (allCoursesResponse) => {
                this.allCourses = allCoursesResponse.data;
                this.updatedTransfer.loading = false;
            },
            (error) => {
                this.updatedTransfer.loading = false;
                this.Alert.error('Sorry, could not load school courses', error);
            });
    }

    public getCourseByDepartmentId(departmentId) {
        this.load.courses.list = true;
        this.load.courses.loading = true;
        this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
            (coursesResponse) => {
                this.load.courses.loading = false;

                this.allCourses = coursesResponse.course_of_studies;
                console.log('returned courses', coursesResponse);
            },
            (error) => {
                this.load.courses.list = false;
                this.load.courses.loading = false;
                this.Alert.error(`Sorry could not load courses`, error);
            }
        );
    }

    public getAllCountries() {
        this.schoolService.getAllCountries().subscribe(
            (countriesResponse) => {
                this.allCountries = countriesResponse;
                console.log('returned countries', countriesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load countries`, error);
            }
        );
    }

    public getStateByCountryId(countryId) {
        this.schoolService.getStateByCountryId(countryId).subscribe(
            (statesResponse) => {
                this.allStates = statesResponse.states;
                console.log('returned States', statesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load States`, error);
            }
        );
    }

    public getAllLevels() {
        this.staffConfigService.getAllLevel().subscribe(
            (levelsResponse) => {
                this.allLevels = levelsResponse.data;
                console.log(levelsResponse);
            },
            (error) => {
                console.log('Could not load levels');
            }
        );
    }


    public logStaffIn() {
        this.schoolService.logStaffIn().subscribe(
            (loginResponse) => {
                this.userService.setAuthUser(loginResponse.token);

            }
        );
    }

    public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        if (ind >= 0) {
            this.allTransfers[ind].loading = true;
            this.editedIndex = ind;
            this.getAllCourses();
            this.updatedTransfer = this.allTransfers[ind];
            const TransferManagementObject = {
                first_name: this.updatedTransfer.first_name,
                last_name: this.updatedTransfer.last_name,
                middle_name: this.updatedTransfer.middle_name,
                matric_no: this.updatedTransfer.matric_no,
                email: this.updatedTransfer.email,
                course_of_study_id: this.updatedTransfer.course_of_study.id,
                department_id: this.updatedTransfer.course_of_study.department.id,
                faculty_id: this.updatedTransfer.course_of_study.department.faculty.id,
                country_id: 1,
                state_id: 2
            };
            this.updateTransferForm.patchValue(this.updatedTransfer);
            const patchUpdateObject = {
                faculty_id: ''
            }
            console.log('Transfer management object ', this.updatedTransfer);

            // this.updatedTransfer.loading=false

        }
        (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
        // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
        // });

    }

    public getLgaByStateId(stateId) {
        this.load.lgas.loading = true;
        let dataItem = this.selectWhereId(this.allStates, 'id', stateId)
        console.log("Select state where id is ", dataItem);
        this.admissionsService.getLgaByStateId(stateId).subscribe(
            (lgasResponse) => {
                this.load.lgas.loading = false;

                this.allLgas = lgasResponse.lgas;
                console.log("returned lgas", lgasResponse);
            },
            (error) => {
                this.load.lgas.loading = false;
                this.Alert.error(`Sorry could not load Lgas for`, error);
                // ${this.selectWhereId(this.allStates, stateId).name}
            }
        )
    }

    public selectWhereId(data: any[], search_key: string, id) {
        let dataItem: any[] = [];
        data.forEach(item => {
            let itemKey = parseInt(item[search_key]);
            let itemId = parseInt(id);
            if (itemKey === itemId) {
                dataItem.push(item);
            }
        });
        return dataItem[0];
    }


}
