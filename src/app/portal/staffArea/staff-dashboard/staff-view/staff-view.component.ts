import {Component, OnInit, AfterViewInit, ElementRef, ViewChild} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';

import {Validators, FormGroup, FormBuilder, FormControl} from '@angular/forms';
import * as utils from '../../../../../utils/util';
import {Faculty} from '../../../../../interfaces/faculty.inteface';
import {Department} from '../../../../../interfaces/department.interface';
import {Course} from '../../../../../interfaces/course.interface';
import {ScriptLoaderService} from '../../../../../services/script-loader.service';
import {SchoolService} from '../../../../../services/school.service';
import {UserService} from '../../../../../services/user.service';
import {NotificationService} from '../../../../../services/notification.service';
import {StaffConfigService} from '../../../../../services/api-service/staff-config.service';
import {AdmissionsService} from "../../../../../services/api-service/admissions.service";
import {ActivatedRoute} from "@angular/router";
// import {StaffServiceService} from "../../../../../services/api-service/staff-service.service";


declare const $: any;

@Component({
    selector: 'app-staff-view',
    templateUrl: './staff-view.component.html',
    styleUrls: ['./staff-view.component.css']
})

export class StaffViewComponent implements OnInit {


    el: ElementRef;
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<any> = new Subject();
    @ViewChild(DataTableDirective)
    dtElement: DataTableDirective;

    public allLevels: any[] = [];
    public allSemesters: any[] = [];
    public allChristianDenominations: any[] = [
        {
            name: "Anglican",
            value: "Anglican"
        },
        {
            name: "Baptist",
            value: "Baptist"
        },
        {
            name: "Catholic",
            value: "Catholic"
        },
        {
            name: "Methodist",
            value: "Methodist"
        },
        {
            name: "Presbyterian",
            value: "Presbyterian"
        }
    ];
    public noDataMessage: string = '';
    public searchedSessionAndSemesterMessage: string = '';
    public setSemester: string = '';
    public setResultSemester: string = '';
    public allDenominations: any[] = [];
    public allLgas: any[] = [];
    public allFees: any[] = [];
    public allFeeItems: any[] = [];
    public allSessions: any[] = [];
    public allProgrammes: any[] = [];
    public loadingResult: boolean = false;
    public loadSessions: boolean = false;
    public loader: boolean = false;
    public showResults: boolean = false;
    public overlay = utils.overlay;
    public allStaffs: any[] = [];
    public searchTerm = new FormControl();
    public searchDiv: boolean = false;
    public searching: boolean = false;
    public tableWasOnceRendered: boolean = false;
    public searchResult: any[] = [];
    public noName: string = '';
    public viewedStaffsProgrammeId;
    public allViewedResults: any[] = [];
    public updatedStaff: any;
    public allFaculties: Faculty[] = [];
    public allDepartments: Department[] = [];
    public allCourses: Course[] = [];
    public allStates: any[] = [];
    public allStaffTransfers: any[] = [];
    public allCountries: any[] = [];
    public authenticatedUser: any;
    public viewedStaff: any;
    public staffResults: any[] = [];
    public staffId: number = 0;
    public selectedSessionId: number = 0;
    public myStaffs: any[] = [];
    public myEditedStaff: any = {};
    public createStaffForm: FormGroup;
    public updateStaffForm: FormGroup;
    public transferStaffForm: FormGroup;
    public getStaffResultForm: FormGroup;
    public createStaffExceptionForm: FormGroup;
    public assignFeeToStaffForm: FormGroup;

    public editedIndex: number;
    public load = {
        requesting: {
            list: true,
            create: false
        },
        message: {
            create: 'Create',
            update: 'Update'
        },
        departments: {
            list: false,
            loading: false
        },
        courses: {
            list: false,
            loading: false
        },
        staffs: {
            list: false,
            loading: false
        },
        lgas: {
            list: false,
            loading: false
        }

    };

    static transferStaffForm = function () {
        return {
            to_course_id: ['', Validators.compose([Validators.required, Validators.email])],
            to_level_id: ['', Validators.compose([Validators.required])],
        }
    }

    static getStaffResultForm = function () {
        return {
            session: ['', Validators.compose([Validators.required])],
            semester: ['', Validators.compose([Validators.required])]
        }
    }

    static createStaffExceptionForm = function () {
        return {
            session_year: ['', Validators.compose([Validators.required])],
            status: ['', Validators.compose([Validators.required])],
            type: ['', Validators.compose([Validators.required])],


        }
        // email,first_name,last_name,course_of_study_id
    }

    static assignFeeToStaffForm = function () {
        return {
            amount: ['', Validators.compose([Validators.required])],
            fee_item_id: ['', Validators.compose([Validators.required])],
            session_year: ['', Validators.compose([Validators.required])],
        }
        // email,first_name,last_name,course_of_study_id
    }

    static createStaffForm = function () {
        return {
            email: ['', Validators.compose([Validators.required, Validators.email])],
            first_name: ['', Validators.compose([Validators.required])],
            other_names: '',
            last_name: ['', Validators.compose([Validators.required])],
            faculty_id: ['', Validators.compose([Validators.required])],
            department_id: ['', Validators.compose([Validators.required])],
            course_of_study_id: ['', Validators.compose([Validators.required])],
            country_id: ['', Validators.compose([Validators.required])],
            state_id: '',
            level_id: ['', Validators.compose([Validators.required])],
            matric_no: ['', Validators.compose([Validators.required])],
            lga_id: ['', Validators.compose([Validators.required])],
            birth_of_date: ['', Validators.compose([Validators.required])],
            gender: ['', Validators.compose([Validators.required])],
            religion: ['', Validators.compose([Validators.required])],
            denomination: ['', Validators.compose([Validators.required])],
            home_town: ['', Validators.compose([Validators.required])],
            marital_status: ['', Validators.compose([Validators.required])]

        };
        // email,first_name,last_name,course_of_study_id
    };


    static updateStaffForm = function () {

        return {
            email: ['', Validators.compose([Validators.required, Validators.email])],
            first_name: ['', Validators.compose([Validators.required])],
            other_names: '',
            last_name: ['', Validators.compose([Validators.required])],
            faculty_id: ['', Validators.compose([Validators.required])],
            department_id: ['', Validators.compose([Validators.required])],
            course_of_study_id: ['', Validators.compose([Validators.required])],
            country_id: ['', Validators.compose([Validators.required])],
            state_id: '',
            level_id: ['', Validators.compose([Validators.required])],
            matric_no: ['', Validators.compose([Validators.required])],
            lga_id: ['', Validators.compose([Validators.required])],
            birth_of_date: ['', Validators.compose([Validators.required])],
            gender: ['', Validators.compose([Validators.required])],
            religion: ['', Validators.compose([Validators.required])],
            denomination: '',
            home_town: ['', Validators.compose([Validators.required])],
            marital_status: ['', Validators.compose([Validators.required])]

        };

    };

    constructor(private _script: ScriptLoaderService,
                private userService: UserService,
                private schoolService: SchoolService,
                private fb: FormBuilder,
                private staffConfigService: StaffConfigService,
                private admissionsService: AdmissionsService,
                private activeRoute: ActivatedRoute,
                private Alert: NotificationService) {
        this.searchTerm.valueChanges
            .debounceTime(400)
            .subscribe(data => {
                this.searchDiv = true;
                this.searching = true;
                this.schoolService.searchStaffByName(data).subscribe(response => {
                        this.searching = false;
                        console.log('School Name Respone ', response);
                        this.searchResult = response['data'];
                        if (this.searchResult.length === 0) {
                            this.noName = `No staff was found with the name '${this.searchTerm.value}'`;

                        }
                    },
                    (error) => {
                        this.Alert.error(`Sorry your search for ${this.searchTerm.value} failed`, error);
                    })
            })
        this.createStaffForm = this.fb.group(StaffViewComponent.createStaffForm());
        this.updateStaffForm = this.fb.group(StaffViewComponent.updateStaffForm());
        this.transferStaffForm = this.fb.group(StaffViewComponent.transferStaffForm());
        this.getStaffResultForm = this.fb.group(StaffViewComponent.getStaffResultForm());
        this.createStaffExceptionForm = this.fb.group(StaffViewComponent.createStaffExceptionForm());
        this.assignFeeToStaffForm = this.fb.group(StaffViewComponent.assignFeeToStaffForm());

        this.dtOptions = {
            pagingType: 'full_numbers',
        };
        console.log('Authenticated user ', this.authenticatedUser);
    }

    ngOnInit() {
        this.activeRoute.params.subscribe(params => {
            this.staffId = +params['id']; // (+) converts string 'id' to a number
            this.getStaffById(this.staffId);
            // In a real app: dispatch action to load the details here.
        });
        // this.getAllStaffs();
        this.getAllCourses();
        this.getAllCountries();
        this.getAllFaculties();
        this.getAllLevels();
        this.getAllProgrammes();
        this.getAllFees();

    }

    // ngAfterViewInit() {
    //     this._script.loadScripts('app-staffs',
    //         ['assets/staff_assets/demo/demo4/base/scripts.bundle.js', 'assets/staff_assets/app/js/dashboard.js']);

    // }

    public getAllStaffs() {
        this.load.requesting.list = true;
        this.schoolService.getAllStaffs().subscribe(
            (allStaffsResponse) => {
                this.load.requesting.list = false;
                if (allStaffsResponse.data) {
                    this.allStaffs = allStaffsResponse.data;

                }
                else {
                    this.allStaffs = allStaffsResponse;

                }
                if (this.allStaffs.length > 0) {
                }
                console.log('All Staffs ', this.allStaffs);
            },
            (error) => {
                console.log('Could not load list of staffs')
                this.load.requesting.list = false;

            }
        );
    }

    public async createStaff() {

        this.load.requesting.create = true;
        this.load.message.create = 'Creating...';

        console.log('Create staff object ', this.createStaffForm.value);
        await this.schoolService.createOrUpdateStaff(this.createStaffForm.value).subscribe(
            (createdStaffResponse) => {
                this.load.message.create = 'Create';
                this.Alert.success(`${this.createStaffForm.value.first_name} created successfully\n`);
                this.createStaffForm = this.fb.group(StaffViewComponent.createStaffForm());
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'createStaff');
                createdStaffResponse['status'] = '1';
                this.allStaffs.unshift(createdStaffResponse);
                console.log('Newly created staff ', createdStaffResponse);
            },
            (error) => {
                this.load.message.create = 'Create';
                // this.triggerModal('close','createSchool');
                console.log('Eroorroroor ', error);
                this.load.requesting.create = false;
                this.Alert.error(`Could not create ${this.createStaffForm.value.first_name}`, error);
            }
        );
    }

    public setSelectedSemesters(value) {
        this.selectedSessionId = value;
        this.allSemesters = this.selectWhereId(this.allSessions, 'id', value)['session_semesters'];

        console.log("All semesters ", this.allSemesters);

    }

    public async assignFeeToCurrentStaff() {

        this.load.requesting.create = true;
        this.load.message.create = "Creating...";

        console.log("Create fee object ", this.assignFeeToStaffForm.value);
        let data = this.assignFeeToStaffForm.value;
        data['staff_id'] = this.viewedStaff.id;
        await this.schoolService.createOrUpdateFee(data).subscribe(
            (createdFeeResponse) => {
                this.load.message.create = "Create";
                this.Alert.success(`A fee of ${this.assignFeeToStaffForm.value.amount} has been Sent for Authorization`);
                this.assignFeeToStaffForm = this.fb.group(StaffViewComponent.assignFeeToStaffForm());
                this.load.requesting.create = false;

            },
            (error) => {
                this.load.message.create = "Create";
                // this.triggerModal('close','createSchool');
                console.log("Eroorroroor ", error);
                this.load.requesting.create = false;
                this.Alert.error(`Could not create ${this.assignFeeToStaffForm.value.name}`, error)
            }
        )
    }

    public updateStaff() {
        this.load.requesting.create = true;
        this.load.message.update = 'Updating...';

        this.schoolService.createOrUpdateStaff(this.updateStaffForm.value, this.updatedStaff.id).subscribe(
            (updatedStaffResponse) => {
                this.load.message.update = 'Update';
                this.updateStaffForm = this.fb.group(StaffViewComponent.updateStaffForm());
                this.Alert.success(`${this.updateStaffForm.value.first_name} updated successfully\n`);
                this.allStaffs[this.editedIndex] = updatedStaffResponse;
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'updateStaff');

                console.log('Updated school ', this.updateStaffForm);
            },
            (error) => {
                this.load.message.update = 'Update';
                this.load.requesting.create = false;
                this.Alert.error(`Could not update ${this.updateStaffForm.value.first_name}`, error);
            }
        );
    }

    public getDenomination(value) {
        this.allDenominations = (value === 'Christianity') ? this.allChristianDenominations : [];
    }

    public hideSearchDiv() {
        this.searchDiv = false;
    }


    public getAllFaculties() {
        this.schoolService.getAllFaculties().subscribe(
            (facultiesResponse) => {
                console.log('All faculties ', facultiesResponse);
                this.allFaculties = facultiesResponse;
            },
            (error) => {
                this.Alert.error('Sorry, could not load faculties. Please reload page.', error);
            }
        );
    }

    /**
     * This method returns a single faculty which contains a list of departments
     */
    public getDepartmentByByFacultyId(facultyId) {
        this.load.departments.list = true;
        this.load.departments.loading = true;

        this.schoolService.getDepartmentByFacultyId(facultyId).subscribe(
            (departmentResponse) => {
                this.load.departments.loading = false;

                this.allDepartments = departmentResponse.departments;
                console.log('returned departments', departmentResponse);
            },
            (error) => {
                this.load.departments.list = false;
                this.load.departments.loading = false;

                this.Alert.error(`Sorry could not load departments`, error);
            }
        );
    }

    public getAllCourses() {

        this.schoolService.getAllCourses().subscribe(
            (allCoursesResponse) => {
                this.allCourses = allCoursesResponse.data;
                console.log('All Courses from hereejdhhd ', allCoursesResponse
                );
                // this.updatedStaff.loading = false;
            },
            (error) => {
                // this.updatedStaff.loading = false;
                this.Alert.error('Sorry, could not load school courses', error);
            });
    }

    public getCourseByDepartmentId(departmentId) {
        this.load.courses.list = true;
        this.load.courses.loading = true;
        this.schoolService.getCourseByDepartmentId(departmentId).subscribe(
            (coursesResponse) => {
                this.load.courses.loading = false;

                this.allCourses = coursesResponse.course_of_studies;
                console.log('returned courses', coursesResponse);
            },
            (error) => {
                this.load.courses.list = false;
                this.load.courses.loading = false;
                this.Alert.error(`Sorry could not load courses`, error);
            }
        );
    }

    public getAllCountries() {
        this.schoolService.getAllCountries().subscribe(
            (countriesResponse) => {
                this.allCountries = countriesResponse;
                console.log('returned countries', countriesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load countries`, error);
            }
        );
    }

    public getStateByCountryId(countryId) {
        this.schoolService.getStateByCountryId(countryId).subscribe(
            (statesResponse) => {
                this.allStates = statesResponse.states;
                console.log('returned States', statesResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load States`, error);
            }
        );
    }

    public getAllLevels() {
        this.staffConfigService.getAllLevel().subscribe(
            (levelsResponse) => {
                this.allLevels = levelsResponse.data;
                console.log('All levels fro just now', levelsResponse);
            },
            (error) => {
                console.log('Could not load levels');
            }
        );
    }

    public getAllFees() {
        this.load.requesting.list = true;
        this.schoolService.getAllFees().subscribe(
            (allFeesResponse) => {
                this.load.requesting.list = false;
                this.allFees = allFeesResponse.data;
                this.getAllFeeItems();

                console.log("All fees response", allFeesResponse)
            },
            (error) => {

                this.load.requesting.list = false;

            }
        );

    }

    public getAllFeeItems() {
        this.schoolService.getAllFeeItems().subscribe(
            (allFeeItemsResponse) => {
                this.load.requesting.list = false;
                this.allFeeItems = allFeeItemsResponse.data;
                this.allFees.forEach((fee) => {
                    this.allFeeItems.forEach((feeItem) => {
                        const feeItemId = parseInt(feeItem.id);
                        const feeFeeItemId = parseInt(fee.fee_item_id);
                        if (feeItemId === feeFeeItemId) {
                            fee.fee_item_name = feeItem.name
                        }
                    })
                })
                console.log("All fee items response", allFeeItemsResponse)
            },
            (error) => {
                this.load.requesting.list = false;

            }
        )
    }


    public logStaffIn() {
        this.schoolService.logStaffIn().subscribe(
            (loginResponse) => {
                this.userService.setAuthUser(loginResponse.token);

            }
        );
    }

    public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        if (ind >= 0) {
            this.allStaffs[ind].loading = true;
            this.editedIndex = ind;
            this.getAllCourses();
            this.updatedStaff = this.allStaffs[ind];
            const staffManagementObject = {
                first_name: this.updatedStaff.first_name,
                last_name: this.updatedStaff.last_name,
                middle_name: this.updatedStaff.middle_name,
                matric_no: this.updatedStaff.matric_no,
                email: this.updatedStaff.email,
                course_of_study_id: this.updatedStaff.course_of_study.id,
                department_id: this.updatedStaff.course_of_study.department.id,
                faculty_id: this.updatedStaff.course_of_study.department.faculty.id,
                country_id: 1,
                state_id: 2
            };
            this.updateStaffForm.patchValue(this.viewedStaff);
            const patchUpdateObject = {
                faculty_id: ''
            }
            console.log('Staff management object ', this.updatedStaff);

            // this.updatedStaff.loading=false

        }
        (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
        // (action === "open") ? this.overlay.open(modalId, 'slideInLeft') : this.overlay.close(modalId, () => {
        // });

    }

    public getStaffById(staffId) {
        this.loader = true;
        this.schoolService.getStaffById(staffId).subscribe(
            (staffResponse) => {
                this.loader = false;

                this.viewedStaff = staffResponse;
            },
            (error) => {
                this.loader = false;
                this.Alert.error(`Could not load this staff's record`, error);
            }
        )
    }



    public setSelectedSemester(semesterId) {
        const semester = this.selectWhereId(this.allSemesters, 'id', semesterId);
        this.setSemester = semester['name'];
        this.setResultSemester = semester['semester'];
    }



    public getSessionsByProgrammeId(programmeId) {
        this.loadSessions = true;
        this.schoolService.getSessionsByProgrammeId(programmeId).subscribe(
            (sessionsResponse) => {
                this.loadSessions = false;

                this.allStaffTransfers.forEach((staffTransfer) => {
                    staffTransfer['session_year'] = this.selectWhereId(this.allSessions, 'id', staffTransfer['session_id']).year;
                })
                console.log('All sessions by programme Id ', sessionsResponse);
            },
            (error) => {
                this.loadSessions = false;

                console.log('An eror occured getting session ', error)
            }
        )
    }




    public getAllProgrammes() {
        this.staffConfigService.getAllProgramme().subscribe(
            (programmeResponse) => {
                this.allProgrammes = programmeResponse.data;
                console.log('returned programmes', programmeResponse);
            },
            (error) => {
                this.Alert.error(`Sorry could not load Programmes`, error);
            }
        );
    }


    public getLgaByStateId(stateId) {
        this.load.lgas.loading = true;
        let dataItem = this.selectWhereId(this.allStates, 'id', stateId)
        console.log("Select state where id is ", dataItem);
        this.admissionsService.getLgaByStateId(stateId).subscribe(
            (lgasResponse) => {
                this.load.lgas.loading = false;

                this.allLgas = lgasResponse.lgas;
                console.log("returned lgas", lgasResponse);
            },
            (error) => {
                this.load.lgas.loading = false;
                this.Alert.error(`Sorry could not load Lgas for`, error);
                // ${this.selectWhereId(this.allStates, stateId).name}
            }
        )
    }

    public selectWhereId(data: any[], search_key: string, id) {
        let dataItem: any[] = [];
        data.forEach(item => {
            let itemKey = parseInt(item[search_key]);
            let itemId = parseInt(id);
            if (itemKey === itemId) {
                dataItem.push(item);
            }
        });
        return dataItem[0];

    }




    public setGradeColor(score) {
        return (score < 40) ? {'color': 'red'} : {}
    }






}

