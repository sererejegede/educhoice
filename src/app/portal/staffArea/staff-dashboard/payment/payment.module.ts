import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PaymentComponent} from './payment/payment.component';
import {RouterModule, Routes} from "@angular/router";
import {PaymentResponseComponent} from './payment-response/payment-response.component';
import {Angular4PaystackModule} from "angular4-paystack";
import {DataTablesModule} from 'angular-datatables';

const PAYMENT_ROUTE: Routes = [
  {
    path: 'pay-transact/:ref_id',
    component: PaymentResponseComponent,
    pathMatch: 'full'
  },

  {
    path: 'pay-transact',
    component: PaymentResponseComponent,
    pathMatch: 'full'
  }

];


@NgModule({
  imports: [
    CommonModule,
    DataTablesModule,
    RouterModule.forChild(PAYMENT_ROUTE)
  ],
  exports: [RouterModule, PaymentComponent, PaymentResponseComponent],
  declarations: [PaymentComponent, PaymentResponseComponent]
})
export class PaymentModule {
}
