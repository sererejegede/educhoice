import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {StaffConfigService} from '../../../../../../services/api-service/staff-config.service';
import {NotificationService} from '../../../../../../services/notification.service';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Subject';
import {Cache} from '../../../../../../utils/cache';
import {AuthenticationService} from '../../../../../../services/authentication.service';

declare const $;

@Component({
  selector: 'app-manual-payment',
  templateUrl: './manual-payment.component.html',
  styleUrls: ['./manual-payment.component.css']
})
export class ManualPaymentComponent implements OnInit, AfterViewInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject;
  public manualPayments: any[] = [];
  public manualPayment: any;
  public settings: any;
  public master_object = {
    loading: false,
    accepting: false,
    rejecting: false,
    viewing: false,
  };
  public permissions = {
    id: [
      'school.payment.manual-payment.create',
    ],
    class: [
      'school.payment.manual-payment.change_status',
      'school.payment.manual-payment.delete',
      'school.payment.manual-payment.view'
    ]
  };

  constructor(private staffConfigService: StaffConfigService,
              private notification: NotificationService,
              private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.settings = (Cache.get('ping')) ? Cache.get('ping') : Cache.get('app_settings');
    this.getManualPayments();
  }

  private getManualPayments() {
    this.master_object.loading = true;
    this.staffConfigService.getAllManualPayments().subscribe(res => {
      this.master_object.loading = false;
      this.manualPayments = res.reverse();
      if (this.manualPayments.length > 10) {
        this.dtTrigger.next();
      }
    }, err => {
      this.master_object.loading = false;
      this.notification.error('Could not complete request. Please retry', err);
      console.log(err);
    });
  }

  public updateManualPaymentStatus(status, manual_payment) {
    if (confirm('Are you sure you want to ' + status.toUpperCase() + ' this payment?')) {
      const index = this.manualPayments.indexOf(manual_payment);
      manual_payment[status] = true;
      this.staffConfigService.updateManualPaymentStatus(status, manual_payment.id).subscribe(res => {
        manual_payment[status] = false;
        if (res['data']) {
          this.manualPayments.splice(index, 1, res['data']);
        }
        this.notification.success(`${(status === 'accept') ? 'Approved successfully!' : 'Declined successfully!'}`);
      }, err => {
        manual_payment[status] = false;
        this.notification.error('Could not complete request. Please retry', err);
      });
    }
  }

  public viewImage(manual_pay) {
    if (manual_pay && manual_pay.upload) {
      const img = manual_pay.upload.split(':');
      const img_link = (img[0] !== 'data') ? `data:${manual_pay.upload_content_type};base64,${manual_pay.upload}` : manual_pay.upload;
      window.open().document.body.innerHTML = `<img src=${img_link}>`;
    }
  }

  public viewManualPayment(manual) {
    manual['viewing'] = true;
    this.staffConfigService.getManualPayment(manual.id).subscribe(res => {
      manual['viewing'] = false;
      if (res) {
        this.manualPayment = {
          ...res,
          settings: this.settings.settings,
          student: res.central_transaction.owner.student_registration.student,
          session: res.central_transaction.owner.student_registration.session,
          level: res.central_transaction.owner.student_registration.level,
          course_of_study: res.central_transaction.owner.student_registration.course_of_study,
          student_transaction_items: res.central_transaction.owner.student_transaction_items
        };
        this.modalAction('open', 'viewModal');
      }
    }, err => {
      manual['viewing'] = false;
      this.notification.error('Error viewing payment. This might be a network error', err);
    });
  }

  private toRemoveByPermission () {
    if (this.permissions.id.length) {
      this.permissions.id.forEach(permission => {
        if (permission && !this.iAmPermitted(permission)) {
          const temp = document.getElementById(permission);
          temp.parentNode.removeChild(temp);
        }
      });
    }
    if (this.permissions.class.length) {
      this.permissions.class.forEach(permission => {
        if (permission && !this.iAmPermitted(permission)) {
          const temp_elements = document.getElementsByClassName(permission);
          for (let i = 0; i < temp_elements.length; i++) {
            temp_elements[i].parentNode.removeChild(temp_elements[i]);
          }
        }
      });
    }
  }

  ngAfterViewInit(): void {
    this.toRemoveByPermission();
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  // noinspection JSMethodCanBeStatic
  public modalAction(action: string, modal_id: string) {
    (action === 'open') ? $(`#${modal_id}`).modal('show') : $(`#${modal_id}`).modal('hide');
  }

}
