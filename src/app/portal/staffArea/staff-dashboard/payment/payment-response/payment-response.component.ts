import {Component, OnInit} from '@angular/core';
import {SchoolService} from '../../../../../../services/school.service';
import {ActivatedRoute, ActivationEnd, Router} from '@angular/router';
import {NotificationService} from '../../../../../../services/notification.service';
import {SECRET_PAYMENT_KEY} from '../../../../../../utils/magic-methods';
import * as CryptoJS from 'crypto-js';
import {Cache} from '../../../../../../utils/cache';
import {UserService} from "../../../../../../services/user.service";

@Component({
    selector: 'app-payment-response',
    templateUrl: './payment-response.component.html',
    styleUrls: ['./payment-response.component.css']
})


export class PaymentResponseComponent implements OnInit {

    public paymentFeedBack = {
        message: '',
        process: 'Payment',
        loader: true
    };
    private school_id: string;
    public paymentDetailsFromCache;
    public appSettings: any = {};
    public transactionReference = null;
    public masterTransactionReference = null;
    public etransactReference = null;
    public etransactDisplayObject = null;
    public xpresspayDisplayObject = null;
    public paychoiceObject = null;
    public paychoiceTransaction = {
        tcost: 0
    };
    public transactionFailed = false;
    public payChoiceDiv = false;
    public etransactDiv = false;
    public xpresspayDiv = false;
    public paymentDetails: any;
    public schoolName = '';
    public applicantUser:any;

    public messageStyle = {
        success: false,
        warning: false,
        error: false
    };

    public formOrMatric: string = '';

    constructor(private schoolService: SchoolService,
                private activeRoute: ActivatedRoute,
                private router: Router,
                private Alert: NotificationService,
                private userService:UserService) {

        this.getAppSettings();
        this.school_id = Cache.get('app_settings')['id'] || Cache.get('ping')['id'];
        this.initializePaymentFeedBack();

        this.formOrMatric = (Cache.get('user_is_applicant')) ? 'Form' : 'Matric';

    }


    ngOnInit() {
        this.paymentDetailsFromCache = Cache.get('paymentDetails');
        this.applicantUser = this.userService.getAuthUser()['login'];
        console.error('tjhajhsjhsjshshshhsja',this.userService.getAuthUser());

        // alert('djfjk')
        // console.log('Payment details from cache ',Cache.get('paymentDetails'));
        if(this.paymentDetailsFromCache && this.paymentDetailsFromCache.feeItems){
            this.paymentDetailsFromCache['total_amount'] = this.paymentDetailsFromCache.feeItems.reduce((a, b) => a + b.amount, 0);
            this.paymentDetailsFromCache['tooltip'] = 'Click to copy';
        }
        Cache.set('paymentDetails', null);

        // console.log(this.paymentDetailsFromCache);
    }

    private getAppSettings() {
        this.schoolService.getAppSettings().subscribe((appSettingsResponse) => {
                // console.log('Application setings ', appSettingsResponse)
                Cache.set('app_settings', appSettingsResponse);
                this.appSettings = Cache.get('app_settings')['settings'];
                // console.log(Cache.get('app_settings'));
                this.school_id = appSettingsResponse['id'];
                this.schoolName = Cache.get('app_settings')['name'];
                // console.log('Application settings from admin hedr ', this.appSettings)
            },
            (error) => {
                // console.log('Could not load application settings ', error);
            });
    }

    public initializePaymentFeedBack() {
        this.router.events.subscribe(val => {
            if (val instanceof ActivationEnd) {
                this.transactionReference = val.snapshot.queryParams['reference'] || val.snapshot.params['reference'];
                this.masterTransactionReference = val.snapshot.queryParams || val.snapshot.params;
                // console.log('Etransact reference ', this.masterTransactionReference);
                if (this.transactionReference) {
                    this.verifyPayStackPayment(this.transactionReference);
                    return;
                }
                if (this.masterTransactionReference['etr']) {
                    this.displayEtransactDetails(this.masterTransactionReference);
                    return;
                }
                if (this.masterTransactionReference['hash']) {
                    console.log('Xpress pay', this.masterTransactionReference);
                    this.verifyXpressPayPayment(this.masterTransactionReference);
                    return;
                }
            }
        });
        this.paychoiceObject = this.activeRoute.snapshot.paramMap.get('paychoice');
        if (this.paychoiceObject) {
            const decryptedTransaction = CryptoJS['AES'].decrypt(this.paychoiceObject, SECRET_PAYMENT_KEY);
            this.paychoiceTransaction = JSON.parse(decryptedTransaction.toString(CryptoJS['enc'].Utf8));

            this.paymentFeedBack.loader = false;
            this.payChoiceDiv = true;
        }
    }

    public verifyPayStackPayment(ref_id) {
        if (!ref_id) {
            this.paymentFeedBack.loader = false;
            this.router.navigateByUrl('/');
            return this.Alert.error('UNKNOWN REQUEST');
        }
        this.schoolService.verifyPayStackPayment(ref_id).subscribe(
            (paymentResponse) => {
                this.paymentFeedBack.loader = false;
                if (paymentResponse) {
                    this.paymentDetails = paymentResponse;
                } else {
                    this.transactionFailed = true;
                }
                // console.log('Payment response ', paymentResponse)
            },
            (error) => {
                this.paymentFeedBack.loader = false;
                this.transactionFailed = true;
                // console.log('Payment error response ', error)
            });
    }

    private displayEtransactDetails(data) {
        this.paymentFeedBack.loader = false;
        this.etransactDiv = true;
        this.etransactDisplayObject = data;
    }

    /*private verifyXpressPayPayment(data) {
       this.paymentFeedBack.loader = false;
       this.xpresspayDiv = true;
       this.xpresspayDisplayObject = data;
     }*/

    private verifyXpressPayPayment(data) {
        this.paymentFeedBack.loader = true;
        const path = window.location.href.split('?')[1];
        this.schoolService.verifyXpressPayPayment(data, this.school_id, path).subscribe((res) => {
            this.paymentFeedBack.loader = false;
            this.xpresspayDisplayObject = res;
            /*const r = {
              "responseCode": "XXX",
              "responseMsg": "Transaction Failed",
              "transactionID": "3061154896550331",
              "merchantID": "00001",
              "amount": "9149900",
              "hash": "DAFDDC6443B05B2B4549877C915DD6A585CF5865A29DBDA0E1CCA12C10C2AD74",
              "hash_type": null,
              "amountInNaira": "91499.00",
              "hashType": "SHA256",
              "transactionDate": "2019-01-31 21:12:46.96",
              "maskedPan": null,
              "type": null,
              "brand": null,
              "paymentReference": "XPS/011931/2113402710000004719581",
              "transactionProccessDate": null
            };*/
            this.xpresspayDiv = true;
        }, (err) => {
            this.paymentFeedBack.loader = false;
            this.Alert.error('An error occurred', err);
        });
    }

    public setStatusColor(transactionStatus: string, payment_method?: string): any {
        if (payment_method && payment_method === 'xpresspay') {
            switch (transactionStatus) {
                case '000':
                case '00':
                    return {color: 'blue'};

                case '08':
                    return {color: 'red'};

                default :
                    return {color: 'red'};

            }
        }
        switch (transactionStatus.split(' ').join('_').toLowerCase()) {
            case 'transaction_successful':
                return {color: 'blue'};

            case 'cancelled_by_user':
                return {color: 'red'};

            default :
                return {color: 'red'};

        }
    }

    public copyToClipboard = str => {
        const el = document.createElement('textarea');
        el.value = str;
        // el.style.display = 'none';
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        this.paymentDetailsFromCache['tooltip'] = 'Text copied!';
        document.body.removeChild(el);
    }

    public goBackToOrigin() {
        window.location.replace(Cache.get('RETURN_URL'));
    }

    public setHeaderColor() {
        return {
            'background-color': this.appSettings['primary_color'],
            'color': 'white',
        };
    }
}
