import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {StaffLoginComponent} from './staff-authentication/staff-login.component';
import {StaffDashboardComponent} from './staff-dashboard/dashboard-component.component';
import {StudentsComponent} from './staff-dashboard/students/students.component';
import {ResultsComponent} from './staff-dashboard/results/results.component';
import {ReportsComponent} from './staff-dashboard/reports/reports.component';
import {HostelsComponent} from './staff-dashboard/hostels/hostels.component';
import {FeesComponent} from './staff-dashboard/fees/fees.component';
import {DepartmentsComponent} from './staff-dashboard/departments/departments.component';
import {CoursesComponent} from './staff-dashboard/courses/courses.component';
import {StaffComponent} from './staff-dashboard/staff/staff.component';
import {StaffConfigurationsComponent} from './staff-dashboard/configurations/configurations.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {ConfigurationsModule} from './staff-dashboard/configurations/configurations.module';
import {AdministrationModule} from './staff-dashboard/administration/administration.module';
import {AdmissionsModule} from './staff-dashboard/admissions/admissions.module';
import {FeeItemsComponent} from './staff-dashboard/fee-items/fee-items.component';
import {StudentExceptionsComponent} from './staff-dashboard/student-exceptions/student-exceptions.component';
import {HomeComponent} from '../../home/home.component';
import {StaffDashboardModule} from './staff-dashboard/dashboard.module';
import {DashboardHomeComponent} from './staff-dashboard/dashboard-home/dashboard-home.component';
import {DataTablesModule} from 'angular-datatables';
import {PipeModule} from '../../../shared/modules/pipe.module';
import {LayoutModule} from '../../layouts/layout.module';
import {ReservationComponent} from './staff-dashboard/reservation/reservation/reservation.component';
import {PaymentComponent} from './staff-dashboard/payment/payment/payment.component';
import {PaymentModule} from './staff-dashboard/payment/payment.module';
import {ReportsModule} from './staff-dashboard/reports/reports.module';
import {ReportServiceService} from '../../../services/api-service/report-service.service';
import {ResultTemplatesComponent} from './staff-dashboard/results/result-templates/result-templates.component';
import {FacultyViewComponent} from './staff-dashboard/configurations/faculties/faculty-view/faculty-view-component';
import {ProgrammeViewComponent} from './staff-dashboard/configurations/programme/programme-view/programme-view.component';
import {RoleGuardService} from '../../../services/role-guard.service';
import {ExeatPermissionComponent} from './staff-dashboard/exeat-permission/exeat-permission.component';
import {InterviewModule} from './staff-dashboard/interview/interview.module';
import {Select2Module} from 'ng2-select2';
import {StudentViewComponent} from './staff-dashboard/student-view/student-view.component';
import {MatAutocompleteModule, MatInputModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TransfersComponent} from './staff-dashboard/transfers/transfers.component';
import {AcceptanceFeeComponent} from './staff-dashboard/acceptance-fee/acceptance-fee.component';
import {TransferApplicationsComponent} from './staff-dashboard/transfer-applications/transfer-applications.component';
import {TranscriptsComponent} from './staff-dashboard/results/transcripts/transcripts.component';
import {ScriptLoaderService} from '../../_services/script-loader.service';
import {DepartmentViewComponent} from './staff-dashboard/configurations/departments/department-view/department-view.component';
import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {MessagesModule} from './staff-dashboard/messages/messages.module';
import {NgSelectModule} from '@ng-select/ng-select';
import {StaffViewComponent} from './staff-dashboard/staff-view/staff-view.component';
import {CSVExportService} from '../../../services/c-sv-export.service';
import {ManualPaymentComponent} from "./staff-dashboard/payment/manual-payment/manual-payment.component";
import {BulkResultComponent} from "./staff-dashboard/results/bulk-result/bulk-result.component";
import {DebtorListComponent} from "./staff-dashboard/debtor-list/debtor-list.component";


const STAFF_ROUTES: Routes = [
  {path: '', component: StaffLoginComponent},
  {
    canActivate: [RoleGuardService],
    path: 'dashboard',
    component: StaffDashboardComponent,
    children: [
      {path: '', component: DashboardHomeComponent},
      {path: 'students', component: StudentsComponent},
      {path: 'student/:id', component: StudentViewComponent},
      {path: 'debtors-list', component: DebtorListComponent},
      {path: 'results', component: ResultsComponent},
      {path: 'result-template', component: ResultTemplatesComponent},
      {path: 'bulk-result', component: BulkResultComponent},
      {path: 'reports', component: ReportsComponent},
      {path: 'staff', component: StaffComponent},
      {path: 'staff/:id', component: StaffViewComponent},
      {path: 'hostels', component: HostelsComponent},
      {path: 'fees', component: FeesComponent},
      {path: 'departments', component: DepartmentsComponent},
      {path: 'courses', component: CoursesComponent},
      {path: 'fee-items', component: FeeItemsComponent},
      {path: 'acceptance-fee', component: AcceptanceFeeComponent},
      {path: 'student-exceptions', component: StudentExceptionsComponent},
      {path: 'exeat-permission', component: ExeatPermissionComponent},
      {path: 'transfers', component: TransfersComponent},
      {path: 'transfer-applications', component: TransferApplicationsComponent},
      {path: 'transcripts', component: TranscriptsComponent},
      {path: 'manual-payment', component: ManualPaymentComponent},
      {path: 'messaging', loadChildren: './staff-dashboard/messages/messages.module#MessagesModule'},


    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    ReportsModule,
    InterviewModule,
    PaymentModule,
    ReactiveFormsModule,
    ConfigurationsModule,
    MatAutocompleteModule,
    MatInputModule,
    AdministrationModule,
    StaffDashboardModule,
    AdmissionsModule,
    DataTablesModule,
    PipeModule,
    Select2Module,
    MultiselectDropdownModule,
    NgSelectModule,
    RouterModule.forChild(STAFF_ROUTES)
  ],
  exports: [
    RouterModule,
    /*StaffLoginComponent,
    DashboardHomeComponent*/
  ],
  declarations: [
    HomeComponent,
    StaffLoginComponent,
    FacultyViewComponent,
    DepartmentViewComponent,
    ExeatPermissionComponent,
    ProgrammeViewComponent,
    CoursesComponent,
    DepartmentsComponent,
    FeesComponent,
    AcceptanceFeeComponent,
    FeeItemsComponent,
    HostelsComponent,
    ResultsComponent,
    ResultTemplatesComponent,
    BulkResultComponent,
    StudentsComponent,
    DebtorListComponent,
    StudentViewComponent,
    StaffDashboardComponent,
    StaffComponent,
    StaffViewComponent,
    StaffConfigurationsComponent,
    StudentExceptionsComponent,
    TransfersComponent,
    TransferApplicationsComponent,
    TranscriptsComponent,
    DashboardHomeComponent,
    ManualPaymentComponent
  ],
  providers: [ReportServiceService, RoleGuardService, ScriptLoaderService, CSVExportService]
})


export class StaffRoutingModule {
}
