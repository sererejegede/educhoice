import {Component, OnInit} from "@angular/core";
import {NotificationService} from "../../../../services/notification.service";
import {UserService} from "../../../../services/user.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthenticationService} from "../../../../services/authentication.service";
import {Cache} from "../../../../utils/cache";
import {SchoolService} from "../../../../services/school.service";

declare const $;

@Component({
  selector: 'app-staff-login',
  templateUrl: './staff-login.component.html',
  styleUrls: ['./staff-login.component.css']
})
export class StaffLoginComponent implements OnInit {

  model: any = {};
  loading = false;
  returnUrl: string;
  public showPassword = false;
  public userRoutes;
  public userModules;
  public userLoginForm: FormGroup;
  public registrationForm: FormGroup;
  public appSettings: any = {};
  public ping;
  schoolDetails: any = {};

  static userLoginForm = function () {
    return {
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    };
  };

  static registrationForm = function () {
    return {
      first_name: ['', Validators.compose([Validators.required])],
      last_name: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      password_confirmation: ['', Validators.compose([Validators.required])]
    };
  };

  constructor(private router: Router,
              private userService: UserService,
              private _route: ActivatedRoute,
              private authService: AuthenticationService,
              private fb: FormBuilder,
              private Alert: NotificationService,
              private  schoolService: SchoolService) {
    if (Cache.get('app_settings')) {
      this.appSettings = Cache.get('app_settings')['settings'];
      this.schoolDetails = Cache.get('app_settings');
      this.ping = Cache.get('app_settings');
      // this.getAllSettings();

    } else {
      this.getAllSettings();
    }
    this.userLoginForm = this.fb.group(StaffLoginComponent.userLoginForm());
    this.registrationForm = this.fb.group(StaffLoginComponent.registrationForm());

  }

  ngOnInit() {
    if (this.userService.getAuthUser()) {
      if (this.userService.getAuthUser()['login']['user_type'] === 'App\\Models\\School\\Auth\\Staff') {
        this.router.navigateByUrl('/staff/dashboard');
      } else {
        this.router.navigateByUrl('/staff');
      }
    }
    this.setBgImage();
  }

  private setBgImage() {
    // console.log(this.appSettings.bg_image);
    if (this.appSettings['bg_image']) {
      const staffStyle = $(`.overlay-staff`);
      staffStyle.css({background: `linear-gradient(rgba(0, 0, 0, 0.55), rgba(67, 34, 167, 0.7)), rgba(0, 0, 0, 0.55) url(${this.appSettings['bg_image']}) no-repeat center`, backgroundSize: 'cover'});
    }
  }

  public signin() {
    this.loading = true;
      this.getAllSettings();
      if (this.userService.getAuthUser()) {
        this.authService.logout('stay').subscribe(
            (logResponse) => {
                this.logStaffIn();
            },
            (error) => {
                this.logStaffIn();
                console.log('Could not log user out', error);
            }
        );


    } else {
        this.logStaffIn();
    }

  }

  /*getPingStudent() {
   this.loginService.pingDomain()
   .subscribe((response) => {
   Cache.set('student-ping', response);
   },
   error => {
   console.log('error from profile ::', error);
   });
   }*/

  private logStaffIn() {
      const staffLoginObject = this.userLoginForm.value;
      staffLoginObject['user_type'] = 'staff';
    this.authService.login(staffLoginObject).subscribe(
        (response) => {
          this.loading = false;
          if (response && response.group && response.group.id) {
            Cache.set('user_groups', response.group.id);
          }
          this.userService.setAuthUser(response.token);
          const userType: boolean = this.userService.getAuthUser()['login']['user_type'].split(`\\`).includes('Staff');
          this.setUserGroups(response.group);
          (userType) ? this.router.navigateByUrl('/staff/dashboard') : this.Alert.error('Invalid Login Credentials');
          Cache.set('LOGOUT_URL', '/staff');
          if (staffLoginObject['password'].toLowerCase() === 'password') {
              Cache.set('USER_PASSWORD', true);
          }
        },
        (error) => {
          this.loading = false;
          console.log('There was an error :: ', error);
          this.Alert.error(`Unable to to login`, error);
        });
  }

  public signup() {
    this.loading = true;
    this.authService.registerUser(this.registrationForm.value).subscribe(
        (registrationResponse) => {
          this.loading = false;
          this.Alert.success(`${registrationResponse.first_name}'s application was successful!`);
          console.log(`Registered user `, registrationResponse.first_name);
          this.registrationForm = this.fb.group(StaffLoginComponent.registrationForm());
        },
        (error) => {
          this.Alert.error(error);
          this.loading = false;
        });
  }

  public getAllSettings() {
    this.getAppSettings();
  }

  private getAppSettings() {
    this.schoolService.getAppSettings().subscribe((appSettingsResponse) => {
          // console.log('Application setings ', appSettingsResponse);
          Cache.set('app_settings', appSettingsResponse);
          this.ping = Cache.get('app_settings');
          this.appSettings = Cache.get('app_settings')['settings'];
          // console.log('Application settings from admin hedr ', this.appSettings);
        },
        (error) => {
          console.log('Could not load application settings ', error);
        });
  }

  private setUserGroups(data) {
    const userGroups: any[] = data['tasks'];
    const userRoutes: any[] = [];
    const userModules: any[] = [];

    userGroups.forEach(
        (userGroup) => {
          userRoutes.push(userGroup['route']);
          userModules.push(userGroup['module']['name']);
        }
    );



    const userPermissions = {
      userRoutes: userRoutes,
      userModules: userModules
    }
    Cache.set('user_permissions', userPermissions);
    // console.log('All user groups ', userGroups);
    // console.log('All user routes ', userRoutes);
  }

    public setRouteParam() {
        this.router.navigate(['/forgot-password', {txhref: 'zsta4m'} ]);
    }

}
