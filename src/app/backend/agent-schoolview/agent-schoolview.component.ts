import {Component, OnInit,AfterViewInit} from '@angular/core';
import {NotificationService} from "../../../services/notification.service";
import {ActivatedRoute} from "@angular/router";
import {AgentService} from "../../../services/agent.service";
import {AuthenticationService} from "../../../services/authentication.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../services/user.service";
import {ScriptLoaderService} from "../../../services/script-loader.service";

declare const $: any;

@Component({
    selector: 'app-agent-schoolview',
    templateUrl: './agent-schoolview.component.html',
    styleUrls: ['./agent-schoolview.component.css']
})


export class AgentSchoolviewComponent implements OnInit,AfterViewInit {

    public viewedSchool: any = {};
    public viewedSchoolDashboard: any = {};
    public activeUser:any;
    public createSchoolForm:FormGroup;
    public updateSchoolForm:FormGroup;
    public loadMessage:string = "";
    public viewedSchoolId: number = 0;
    public loading = {
        list: false,
        toggle:false,
        create:false,
        semester:false
    };

    static createSchoolForm = function () {
        return {
            name: ['', Validators.compose([Validators.required])],
            sub_domain: ['', Validators.compose([Validators.required])],
            user_id: '',
            agent_id: ''
        }
    }
    static updateSchoolForm = function () {
        return {
            name: ['', Validators.compose([Validators.required])],
            sub_domain: ['', Validators.compose([Validators.required])],
            domain: ['', Validators.compose([Validators.required])],
            user_id: '',
            agent_id: ''

        }
    }


    constructor(private Alert: NotificationService,
                private activeRoute: ActivatedRoute,
                private _script: ScriptLoaderService,
                private agentService: AgentService,
                private userService: UserService,
                private fb: FormBuilder,
                private authService: AuthenticationService) {
        this.createSchoolForm = this.fb.group(AgentSchoolviewComponent.createSchoolForm());
        this.updateSchoolForm = this.fb.group(AgentSchoolviewComponent.updateSchoolForm());
    }

    ngOnInit() {
        this.activeRoute.params.subscribe(params => {
            this.viewedSchoolId = +params['id']; // (+) converts string 'id' to a number
            (this.isUser())? this.getSchoolForUserById(this.viewedSchoolId):
            this.getSchoolById(this.viewedSchoolId);
            // In a real app: dispatch action to load the details here.
        });
    }

    ngAfterViewInit(){
       this._script.loadScript('app-agent-schoolview','assets/js/dashboard.js')
    }


    public getSchoolById(SchoolId) {
        this.loading.list = true;
        this.agentService.getSchoolById(SchoolId).subscribe(
            (sessionResponse) => {
                this.loading.list = false;
                this.viewedSchool = sessionResponse['school'];
                this.viewedSchoolDashboard =sessionResponse['dashboard']['original'];
                console.log('Viewed school response', this.viewedSchool);
            },
            (error) => {
                this.loading.list = false;
                this.Alert.error('An error occurred, could not load current school ', error)
            })
    }

    public getSchoolForUserById(SchoolId) {
        this.loading.list = true;
        this.agentService.getSchoolByIdForUser(SchoolId).subscribe(
            (sessionResponse) => {
                this.loading.list = false;
                this.viewedSchool = sessionResponse['school'];
                this.viewedSchoolDashboard =sessionResponse['dashboard']['original'];
                console.log('Viewed school response', this.viewedSchool);
                console.log('Viewed school dashboard ', this.viewedSchoolDashboard);
            },
            (error) => {
                this.loading.list = false;
                this.Alert.error('An error occurred, could not load current school ', error)
            })
    }

    public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    }

    public iAmPermitted(routeName): boolean {
        return this.authService.iAmPermitted(routeName);
    }

    public toggleSchool(action) {
        this.loading.toggle = true;
        this.agentService.toggleSchool(action,this.viewedSchoolId).subscribe(
            (schoolResponse) => {
                this.loading.toggle = false;
                this.viewedSchool.configuration.status = schoolResponse.status.toString();
                this.Alert.success(`School ${action}d`);
console.log("School activation response ",schoolResponse)
            },
            (error)=>{
                this.loading.toggle = false;
                this.Alert.error(`Could not ${action} school`);
            }

            )
    }

    public startNewSemester(){

    }

    public isUser() {
        this.activeUser = this.userService.getAuthUser().login.user;
        return this.userService.getAuthUser().login.user_type === 'App\\User';
    }
}
