import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';
import {ScriptLoaderService} from "../../_services/script-loader.service";
import {UserService} from "../../../services/user.service";
import {Router} from "@angular/router";
import {AgentService} from "../../../services/agent.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NotificationService} from "../../../services/notification.service";

declare const $:any;

@Component({
  selector: 'app-agent-users',
  templateUrl: './agent-users.component.html',
  styleUrls: ['./agent-users.component.css']
})
export class AgentUsersComponent implements OnInit {
    el: ElementRef;
    dtOptions: DataTables.Settings = {};
    //noinspection TypeScriptValidateTypes
    dtTrigger: Subject<any> = new Subject();
    dtElement: DataTableDirective = new DataTableDirective(this.el);

    public authenticatedUser: any;
    public myUsers: any[] = [];
    public allAgents: any[] = [];
    public myEditedUser: any = {};
    public createUserForm: FormGroup;
    public updateUserForm: FormGroup;
    public editedIndex: number;
    public load = {
        requesting: {
            list: false,
            create: false
        },
        message: {
            create: "Create",
            update: "Update"
        }
    }

    constructor(
        private userService: UserService,
        private router: Router,
        private agentService: AgentService,
        private fb: FormBuilder,
        private Alert: NotificationService) {
        this.authenticatedUser = this.userService.getAuthUser().login.user;
        if (this.userService.getAuthUser() == null) {
            this.router.navigateByUrl('/home');
        }
        this.createUserForm = this.fb.group(AgentUsersComponent.createUserForm());
        this.updateUserForm = this.fb.group(AgentUsersComponent.updateUserForm());
        console.log("Authenticated user ", this.authenticatedUser);
    }

    static createUserForm = function () {
        return {
            first_name: ['', Validators.compose([Validators.required])],
            last_name: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],
            agent_id: ''
        }
    }
    static updateUserForm = function () {
        return {
            first_name: ['', Validators.compose([Validators.required])],
            last_name: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],
            agent_id: ''

        }
    }
    ngOnInit() {
        this.getMyUsers();
        if(this.visitorIsUpperlink()){
            this.getMyAgents();
        }

    }
    ngAfterViewInit() {

    }

    public triggerModal(action: string, modalId: string) {
        (action === "open") ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    }

    /**
     * This method gets a user's agents
     */
    public getMyUsers() {

        this.load.requesting.list = true;
        this.agentService.getAllAgentUsers().subscribe(
            (agentsResponse) => {
                this.load.requesting.list = false;
                this.myUsers = agentsResponse.data;
                if(this.myUsers.length>0){
                    this.dtTrigger.next();
                }
                console.log("All My Users == ", this.myUsers)
            },
            (error) => {
                this.load.requesting.list = false;
                this.Alert.error('Could not load your users', error);
            },
            () => {

            }
        )
    }

    /**
     * This method creates a new agent
     */
    public async createUser() {
        this.load.requesting.create = true;
        this.load.message.create = "Creating...";
     let userObject =this.createUserForm.value;
     if(this.visitorIsUpperlink()){
         userObject['agent_id'] = this.authenticatedUser['agent_id'];
     }
        await this.agentService.createOrUpdateAgentUser(userObject).subscribe(
            (createdUserResponse) => {
                this.load.message.create = "Create";
                this.Alert.success(`${this.createUserForm.value.name} created successfully\n`);
                this.createUserForm = this.fb.group(AgentUsersComponent.createUserForm());
                this.load.requesting.create = false;
                this.triggerModal('close', 'createUser');
                this.myUsers.unshift(createdUserResponse);
                console.log("Newly created agent ", createdUserResponse)
            },
            (error) => {
                this.load.message.create = "Create";
                // this.triggerModal('close','createUser');
                console.log("Eroorroroor ", error);
                this.load.requesting.create = false;
                this.Alert.error(`Could not create ${this.createUserForm.value.name}`, error)
            }
        );
    }

    public updateUser() {
        this.load.requesting.create = true;
        this.load.message.update = "Updating...";
        let userObject =this.updateUserForm.value;
        if(this.visitorIsUpperlink()){
            userObject['agent_id'] = this.authenticatedUser['agent_id'];
        }
        this.agentService.createOrUpdateAgentUser(userObject, this.myEditedUser.id).subscribe(
            (updatedUserResponse) => {
                this.load.message.update = "Update";
                this.updateUserForm = this.fb.group(AgentUsersComponent.updateUserForm());
                this.Alert.success(`${this.updateUserForm.value.name} updated successfully\n`);
                this.myUsers[this.editedIndex] = updatedUserResponse;
                this.load.requesting.create = false;
                this.triggerModal('close', 'updateUser')

                console.log("Updated agent ", updatedUserResponse)
            },
            (error) => {
                this.load.message.update = "Update";
                this.load.requesting.create = false;
                this.Alert.error(`Could not update ${this.updateUserForm.value.name}`, error)
            }
        )
    }

    public editUser(ind: number) {
        this.editedIndex = ind;
        this.myEditedUser = this.myUsers[this.editedIndex];
        this.updateUserForm.patchValue(this.myEditedUser);
        this.triggerModal('open', 'updateUser');
    }

    public getAllUsers() {
        this.agentService.getAllAgentUsers().subscribe(
            (allUsersResponse) => {
                console.log("All agents response ", allUsersResponse);
            }
        )
    }

    public getMyAgents() {

        this.load.requesting.list = true;
        this.agentService.getMyAgents().subscribe(
            (AgentsResponse) => {
                this.load.requesting.list = false;
                this.allAgents = AgentsResponse.data;
                // this.myAgents.forEach((Agent) => {
                //     Agent.url = `http://${Agent.configuration.domain}`;
                // });
                console.log("All My Agents == ", AgentsResponse)
            },
            (error) => {
                this.load.requesting.list = false;
                this.Alert.error('Could not load your Agents', error);
            },
            () => {

            }
        )
    }

    public visitorIsUpperlink(){
        return this.agentService.visitorIsUpperlink();
    }

}
