import { Component, OnInit } from '@angular/core';
import {NotificationService} from "../../../services/notification.service";
import {ActivatedRoute} from "@angular/router";
import {AgentService} from "../../../services/agent.service";
import {AuthenticationService} from "../../../services/authentication.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {environment} from "../../../environments/environment";
import {SchoolService} from "../../../services/school.service";

declare const $:any;

@Component({
  selector: 'app-agent-user-view',
  templateUrl: './agent-user-view.component.html',
  styleUrls: ['./agent-user-view.component.css']
})
export class AgentUserViewComponent implements OnInit {

    public viewedUser: any = {};
    public viewedUserId: number = 0;
    public loading = {
        list: false
    }
    public authenticatedUser: any;
    public domainName: string = `-portal.`+environment.DEFAULT_DOMAIN;
    public mySchools: any[] = [];
    public myEditedSchool: any = {};
    public createSchoolForm: FormGroup;
    public updateSchoolForm: FormGroup;
  public customDomain = false;
  public editedIndex: number;
    public load = {
        requesting: {
            list: false,
            create: false
        },
        message: {
            create: "Create",
            update: "Update"
        }
    }


    static createSchoolForm = function () {
        return {
            name: ['', Validators.compose([Validators.required])],
            sub_domain: ['', Validators.compose([Validators.required])],
            domain: ['']
        }
    }
    static updateSchoolForm = function () {
        return {
            name: ['', Validators.compose([Validators.required])],
            sub_domain: ['', Validators.compose([Validators.required])],
            domain: ['', Validators.compose([Validators.required])]

        }
    }

    constructor(private Alert: NotificationService,
                private activeRoute: ActivatedRoute,
                private agentService: AgentService,
                private schoolService: SchoolService,
                private authService: AuthenticationService,
                private fb: FormBuilder) {
        this.createSchoolForm = this.fb.group(AgentUserViewComponent.createSchoolForm());
        this.updateSchoolForm = this.fb.group(AgentUserViewComponent.updateSchoolForm());
    }

    ngOnInit() {
        this.activeRoute.params.subscribe(params => {
            this.viewedUserId = +params['id']; // (+) converts string 'id' to a number
            this.getUserById(this.viewedUserId);
            // In a real app: dispatch action to load the details here.
        });
    }


    public getUserById(UserId) {
        this.loading.list = true;
        this.agentService.getAgentUserById(UserId).subscribe(
            (sessionResponse) => {
                this.viewedUser = sessionResponse;
                this.loading.list = false;
                this.mySchools = this.viewedUser.schools;
                this.mySchools.forEach((school)=>{
                    school.url =(school.configuration)? `${school.configuration.domain}/staff`:school.url;
                    console.log('School url ',school.url);
                })
                console.log('Viewed school response', this.viewedUser);
            },
            (error) => {
                this.loading.list = false;
                this.Alert.error('An error occurred, could not load current school ', error)
            })
    }

    public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    }

    public iAmPermitted(routeName): boolean {
        return this.authService.iAmPermitted(routeName);
    }

    public editSchool(ind: number) {
        this.editedIndex = ind;
        this.myEditedSchool = this.mySchools[this.editedIndex];
        this.updateSchoolForm.patchValue({
            name: this.myEditedSchool.name,
            sub_domain: this.myEditedSchool.configuration.subdomain,
            domain: this.myEditedSchool.configuration.domain
        });
        this.triggerModalOrOverlay('open', 'updateSchool');
    }

    public async createSchool() {
        this.load.requesting.create = true;
        this.load.message.create = "Creating...";
        const schoolData = this.createSchoolForm.value;
        schoolData['user_id'] = this.viewedUserId;
        await this.schoolService.createOrUpdateSchool(schoolData).subscribe(
            (createdSchoolResponse) => {
                this.load.message.create = "Create";
                this.Alert.success(`${this.createSchoolForm.value.name} created successfully\n`);
                this.createSchoolForm = this.fb.group(AgentUserViewComponent.createSchoolForm());
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'createSchool');
                this.mySchools.push(createdSchoolResponse);
                console.log("Newly created school ", createdSchoolResponse)
            },
            (error) => {
                this.load.message.create = "Create";
                // this.triggerModal('close','createSchool');
                console.log("Eroorroroor ", error);
                this.load.requesting.create = false;
                this.Alert.error(`Could not create ${this.createSchoolForm.value.name}`, error)
            }
        );
    }

    public updateSchool() {
        this.load.requesting.create = true;
        this.load.message.update = "Updating...";

        this.schoolService.createOrUpdateSchool(this.updateSchoolForm.value, this.myEditedSchool.id).subscribe(
            (updatedSchoolResponse) => {
                this.load.message.update = "Update";
                this.updateSchoolForm = this.fb.group(AgentUserViewComponent.updateSchoolForm());
                this.Alert.success(`${this.updateSchoolForm.value.name} updated successfully\n`);
                this.mySchools[this.editedIndex] = updatedSchoolResponse;
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'updateSchool')

                console.log("Updated school ", updatedSchoolResponse)
            },
            (error) => {
                this.load.message.update = "Update";
                this.load.requesting.create = false;
                this.Alert.error(`Could not update ${this.updateSchoolForm.value.name}`, error)
            }
        )
    }

}


