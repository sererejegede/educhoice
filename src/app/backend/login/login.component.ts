import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormGroup, Validators, FormBuilder} from '@angular/forms';
import {AuthenticationService} from '../../../services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../services/user.service';
import {NotificationService} from '../../../services/notification.service';
import {AgentService} from "../../../services/agent.service";
import {Cache} from "../../../utils/cache";


declare const $: any;

@Component({
  selector: 'app-backend-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class BackendLoginComponent implements OnInit {


  public loading = false;
  public forgotPass: any;
  public feedback = {
    loadingStatus: false
  };
  public userLoginForm: FormGroup;
  public registrationForm: FormGroup;
  public isUpperlink: any;
  public isAgent: any;
  /*public captcha = {
    siteKey:environment.CAPTCHA_KEY,
    size:'normal',
    lang:'en',
    theme:'theme',
    type:'type'
  }*/

  static userLoginForm = function () {
    return {
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      recaptcha: ['', Validators.required]

    };
  };
  static registrationForm = function () {
    return {
      first_name: ['', Validators.compose([Validators.required])],
      last_name: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      password_confirmation: ['', Validators.compose([Validators.required])]
    }
  };

  constructor(private fb: FormBuilder,
              private Alert: NotificationService,
              private userService: UserService,
              private agentService: AgentService,
              private router: Router,
              private route: ActivatedRoute,
              private authService: AuthenticationService,
              private el: ElementRef) {

    if (!this.visitorIsUpperlink()) {
      this.getAgentSettings();
    }
    else {
      this.isUpperlink = true;
    }
    this.userLoginForm = this.fb.group(BackendLoginComponent.userLoginForm());
    this.registrationForm = this.fb.group(BackendLoginComponent.registrationForm());

  }


  ngOnInit() {
    this.disableAutoComplete();
  }


  public signin() {
    this.feedback.loadingStatus = true;

    this.authService.login(this.userLoginForm.value).subscribe(
      (response) => {
        console.log('login reponse :: ', response);
        this.userService.setAuthUser(response.token);
        this.feedback.loadingStatus = false;
        Cache.set('LOGOUT_URL', '/home');
        this.router.navigateByUrl('home/dashboard');


      },
      (error) => {
        console.log('There was an error :: ', error);
        this.Alert.error(`Unable to to login, please check your credentials and retry`, error);
        this.feedback.loadingStatus = false;
        // return this.router.navigateByUrl('backend/admin');

      });
  }

  public signup() {
    this.loading = true;
    this.authService.registerUser(this.registrationForm.value).subscribe(
      (registrationResponse) => {
        this.loading = false;
        this.Alert.success(`${registrationResponse.first_name} has been successfully registered!`);
        console.log(`Registered user `, registrationResponse.first_name);
        this.registrationForm = this.fb.group(BackendLoginComponent.registrationForm());
        this.flipRegistration('signin');
      },
      (error) => {
        this.loading = false;
        this.Alert.error('Could not register', error);
      });
  }

  public onScriptLoad() {
    console.log('Google captcha');
  }

  public flipRegistration(id: string) {
    if (id == 'signup') {
      let login = $('#m_login');
      login.removeClass('m-login--forget-password');
      login.removeClass('m-login--signin');

      login.addClass('m-login--signup');
      (<any>login.find('.m-login__signup')).animateClass('flipInX animated');
      return this.scrollIntoView('#sign_f');
    }
    else {
      let login = $('#m_login');
      login.removeClass('m-login--forget-password');
      login.removeClass('m-login--signup');
      login.addClass('m-login--signin');
      (<any>login.find('.m-login__signin')).animateClass('flipInX animated');
    }

  }

  public setRouteParam() {
    this.router.navigate(['/home/forgot-password', {txhref: 'xyzag5n'}]);
  }

  public getAgentSettings() {
    this.agentService.getAgentSettings().subscribe(
      (agentSettingsResponse) => {
        this.isAgent = agentSettingsResponse;
        console.log('Agent settings response ', agentSettingsResponse);
      },
      (error) => {
        console.log('An error occurred. Could not load your settings.')
      }
    )
  }

  public visitorIsUpperlink() {
    return this.agentService.visitorIsUpperlink();
  }

  private scrollIntoView(location) {
    setTimeout(() => {
      this.el.nativeElement.querySelector(location).scrollIntoView({behavior: 'smooth'});
    }, 300);
  }

  public disableAutoComplete() {
    $('form').attr('autocomplete', 'vcvbcv');
  }

  public handleSuccess($event){}
  public handleExpire(){}
  public handleLoad(){}
}
