import {Component, OnInit} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {Router} from "@angular/router";
import {SchoolService} from "../../../services/school.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NotificationService} from "../../../services/notification.service";
import * as Dashboard from "../../../assets/js/runScript";
import {ScriptLoaderService} from "../../../services/script-loader.service";
import {environment} from "../../../environments/environment";
import {AgentService} from "../../../services/agent.service";

declare const $: any;

@Component({
  selector: 'app-agent-dashboard',
  templateUrl: './agent-dashboard.component.html',
  styleUrls: ['./agent-dashboard.component.css']
})
export class AgentDashboardComponent implements OnInit {

  public authenticatedUser: any;
  public domaiName: string = `-portal.` + environment.DEFAULT_DOMAIN;
  public mySchools: any[] = [];
  public myEditedSchool: any = {};
  public agentDashboardData: any = {};
  public createSchoolForm: FormGroup;
  public updateSchoolForm: FormGroup;
  public editedIndex: number;
  public customDomain = false;
  public load = {
    requesting: {
      list: false,
      create: false
    },
    message: {
      create: "Create",
      update: "Update"
    }
  };

  constructor(private userService: UserService,
              private router: Router,
              private schoolService: SchoolService,
              private fb: FormBuilder,
              private Alert: NotificationService,
              private agentService: AgentService,
              private _script: ScriptLoaderService) {
    this.authenticatedUser = this.userService.getAuthUser().login.user;
    if (this.userService.getAuthUser() == null) {
      this.router.navigateByUrl('/agent');
    }
    this.createSchoolForm = this.fb.group(AgentDashboardComponent.createSchoolForm());
    this.updateSchoolForm = this.fb.group(AgentDashboardComponent.updateSchoolForm());
    console.log("Authenticated user ", this.authenticatedUser);
  }

  static createSchoolForm = function () {
    return {
      name: ['', Validators.compose([Validators.required])],
      sub_domain: ['', Validators.compose([Validators.required])],
      domain: ['']
    }
  }
  static updateSchoolForm = function () {
    return {
      name: ['', Validators.compose([Validators.required])],
      sub_domain: ['', Validators.compose([Validators.required])],
      domain: ['', Validators.compose([Validators.required])]

    }
  }

  ngOnInit() {
    if (this.isUser()) {
      this.getMySchools();
      this.getSchoolDashboard();
    }
    else {
      this.getAgentDashboardData();
    }


  }

  ngAfterViewInit() {
    this._script.loadScript('app-agent-dashboard', '../../../assets/js/dashboard.js');
  }

  public triggerModal(action: string, modalId: string) {
    (action === "open") ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
  }

  /**
   * This method gets a user's schools
   */
  public getMySchools() {

    this.load.requesting.list = true;
    this.schoolService.getMySchools().subscribe(
      (schoolsResponse) => {
        this.load.requesting.list = false;
        this.mySchools = schoolsResponse.data;
        // this.mySchools.forEach((school) => {
        //     school.url = `http://${school.configuration.domain}`;
        // });
        console.log("All My Schools == ", this.mySchools)
      },
      (error) => {
        this.load.requesting.list = false;
        this.Alert.error('Could not load your schools', error);
      },
      () => {

      }
    )
  }

  /**
   * This method creates a new school
   */
  public async createSchool() {
    this.load.requesting.create = true;
    this.load.message.create = "Creating...";
    if (this.isUser()) {
      this.userCreateSchool();
      return;
    }
    await this.schoolService.createOrUpdateSchool(this.createSchoolForm.value).subscribe(
      (createdSchoolResponse) => {
        this.load.message.create = "Create";
        this.Alert.success(`${this.createSchoolForm.value.name} created successfully\n`);
        this.createSchoolForm = this.fb.group(AgentDashboardComponent.createSchoolForm());
        this.load.requesting.create = false;
        this.triggerModal('close', 'createSchool');
        this.mySchools.push(createdSchoolResponse);
        console.log("Newly created school ", createdSchoolResponse)
      },
      (error) => {
        this.load.message.create = "Create";
        // this.triggerModal('close','createSchool');
        console.log("Eroorroroor ", error);
        this.load.requesting.create = false;
        this.Alert.error(`Could not create ${this.createSchoolForm.value.name}`, error)
      }
    );
  }

  public updateSchool() {
    this.load.requesting.create = true;
    this.load.message.update = "Updating...";
    if (this.isUser()) {
      this.userUpdateSchool();
      return;
    }

    this.schoolService.createOrUpdateSchool(this.updateSchoolForm.value, this.myEditedSchool.id).subscribe(
      (updatedSchoolResponse) => {
        this.load.message.update = "Update";
        this.updateSchoolForm = this.fb.group(AgentDashboardComponent.updateSchoolForm());
        this.Alert.success(`${this.updateSchoolForm.value.name} updated successfully\n`);
        this.mySchools[this.editedIndex] = updatedSchoolResponse;
        this.load.requesting.create = false;
        this.triggerModal('close', 'updateSchool')

        console.log("Updated school ", updatedSchoolResponse)
      },
      (error) => {
        this.load.message.update = "Update";
        this.load.requesting.create = false;
        this.Alert.error(`Could not update ${this.updateSchoolForm.value.name}`, error)
      }
    )
  }

  public async userCreateSchool() {
    this.load.requesting.create = true;
    this.load.message.create = "Creating...";
    await this.schoolService.userCreateOrUpdateSchool(this.createSchoolForm.value).subscribe(
      (createdSchoolResponse) => {
        this.load.message.create = "Create";
        this.Alert.success(`${this.createSchoolForm.value.name} created successfully\n`);
        this.createSchoolForm = this.fb.group(AgentDashboardComponent.createSchoolForm());
        this.load.requesting.create = false;
        this.triggerModal('close', 'createSchool');
        this.mySchools.push(createdSchoolResponse);
        console.log("Newly created school ", createdSchoolResponse)
      },
      (error) => {
        this.load.message.create = "Create";
        // this.triggerModal('close','createSchool');
        console.log("Eroorroroor ", error);
        this.load.requesting.create = false;
        this.Alert.error(`Could not create ${this.createSchoolForm.value.name}`, error)
      }
    );
  }

  public userUpdateSchool() {
    this.load.requesting.create = true;
    this.load.message.update = "Updating...";

    this.schoolService.userCreateOrUpdateSchool(this.updateSchoolForm.value, this.myEditedSchool.id).subscribe(
      (updatedSchoolResponse) => {
        this.load.message.update = "Update";
        this.updateSchoolForm = this.fb.group(AgentDashboardComponent.updateSchoolForm());
        this.Alert.success(`${this.updateSchoolForm.value.name} updated successfully\n`);
        this.mySchools[this.editedIndex] = updatedSchoolResponse;
        this.load.requesting.create = false;
        this.triggerModal('close', 'updateSchool')

        console.log("Updated school ", updatedSchoolResponse)
      },
      (error) => {
        this.load.message.update = "Update";
        this.load.requesting.create = false;
        this.Alert.error(`Could not update ${this.updateSchoolForm.value.name}`, error)
      }
    )
  }

  public editSchool(ind: number) {
    this.editedIndex = ind;
    this.myEditedSchool = this.mySchools[this.editedIndex];
    this.updateSchoolForm.patchValue({
      name: this.myEditedSchool.name,
      sub_domain: this.myEditedSchool.configuration.subdomain,
      domain: this.myEditedSchool.configuration.domain
    });
    this.triggerModal('open', 'updateSchool');
  }

  public getAllSchools() {
    this.schoolService.getAllSchools().subscribe(
      (allSchoolsResponse) => {
        console.log("All schools response ", allSchoolsResponse);
      }
    )
  }

  public isUser() {
    return this.userService.getAuthUser().login.user_type === 'App\\User';
  }

  public getSchoolDashboard() {
    this.schoolService.getSchoolDashboard().subscribe(
      (dashboardResponse) => {
        console.log('Dashboard response ', dashboardResponse);
      },
      (error) => {
        // this.Alert.error('Could not load your schools dashboard');
      }
    )
  }

  public getAgentDashboardData() {
    this.agentService.getAgentDashboardData().subscribe(
      (dashboardResponse) => {
        this.agentDashboardData = this.formatDashboardData(dashboardResponse);
        // active_user
        //     :
        //     22
        // in_active_school
        //     :
        //     3
        // inactive_users
        //     :
        //     0
        // schools
        //     :
        //     11
        console.log('Agent dashboard response ', dashboardResponse)
      },
      (error) => {
        console.log('An error occurred ', error);
        this.Alert.error('Could not load your dashboard data ', error);
      }
    )
  }

  private formatDashboardData(data) {
    data['all_users'] = data['active_user'] + data['inactive_users'];
    data['active_schools'] = data['schools'] - data['in_active_school'];

    return data;
  }


}
