import { Component, OnInit } from '@angular/core';
import {NotificationService} from "../../../services/notification.service";
import {ActivatedRoute} from "@angular/router";
import {AgentService} from "../../../services/agent.service";
import {AuthenticationService} from "../../../services/authentication.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

declare const $:any;

@Component({
    selector: 'app-agent-view',
    templateUrl: './agent-view.component.html',
    styleUrls: ['./agent-view.component.css']
})
export class AgentViewComponent implements OnInit {

    public createUserForm: FormGroup;
    public updateUserForm: FormGroup;
    public viewedAgent:any = {};
    public agentUsers:any[] = [];
    public editedAgentUser:any = {};
    public viewedAgentId:number = 0;
    public editedIndex: number;
    public loading = {
        list:false,
        semester:false
    };
    public load = {
        requesting: {
            list: false,
            create: false
        },
        message: {
            create: "Create",
            update: "Update"
        }
    }
    static createUserForm = function () {
        return {
            first_name: ['', Validators.compose([Validators.required])],
            last_name: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],
        }
    }
    static updateUserForm = function () {
        return {
            first_name: ['', Validators.compose([Validators.required])],
            last_name: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],


        }
    }


    constructor(
        private Alert:NotificationService,
        private activeRoute:ActivatedRoute,
        private agentService:AgentService,
        private fb:FormBuilder,
        private authService:AuthenticationService
    ) {
        this.createUserForm = this.fb.group(AgentViewComponent.createUserForm());
        this.updateUserForm = this.fb.group(AgentViewComponent.updateUserForm());
    }

    ngOnInit() {
        this.activeRoute.params.subscribe(params => {
            this.viewedAgentId = +params['id']; // (+) converts string 'id' to a number
            this.getAgentById(this.viewedAgentId);
            // In a real app: dispatch action to load the details here.
        });
    }

    public async createUser() {
        this.load.requesting.create = true;
        this.load.message.create = "Creating...";
        let userObject =this.createUserForm.value;

            userObject['agent_id'] = this.viewedAgentId;

        await this.agentService.createOrUpdateAgentUser(userObject).subscribe(
            (createdUserResponse) => {
                this.load.message.create = "Create";
                this.Alert.success(`${this.createUserForm.value.name} created successfully\n`);
                this.createUserForm = this.fb.group(AgentViewComponent.createUserForm());
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'createUser');
                this.viewedAgent.users.unshift(createdUserResponse);
                console.log("Newly created agent ", createdUserResponse)
            },
            (error) => {
                this.load.message.create = "Create";
                // this.triggerModal('close','createUser');
                console.log("Eroorroroor ", error);
                this.load.requesting.create = false;
                this.Alert.error(`Could not create ${this.createUserForm.value.name}`, error)
            }
        );
    }

    public updateUser() {
        this.load.requesting.create = true;
        this.load.message.update = "Updating...";
        let userObject =this.updateUserForm.value;

            userObject['agent_id'] = this.viewedAgentId;

        this.agentService.createOrUpdateAgentUser(userObject, this.editedAgentUser.id).subscribe(
            (updatedUserResponse) => {
                this.load.message.update = "Update";
                this.updateUserForm = this.fb.group(AgentViewComponent.updateUserForm());
                this.Alert.success(`${this.updateUserForm.value.name} updated successfully\n`);
                this.agentUsers[this.editedIndex] = updatedUserResponse;
                this.load.requesting.create = false;
                this.triggerModalOrOverlay('close', 'updateUser')

                console.log("Updated agent ", updatedUserResponse)
            },
            (error) => {
                this.load.message.update = "Update";
                this.load.requesting.create = false;
                this.Alert.error(`Could not update ${this.updateUserForm.value.name}`, error)
            }
        )
    }


    public getAgentById(AgentId) {
        this.loading.list = true;
        this.agentService.getAgentById(AgentId).subscribe(
            (AgentResponse) => {
                this.loading.list = false;
                this.viewedAgent = AgentResponse;
                this.agentUsers = AgentResponse.users;
                console.log('Viewed Agent response', this.viewedAgent);
            },
            (error) => {
                this.loading.list = false;
                this.Alert.error('An error occurred, could not load current Agent ', error)
            })
    }



    public triggerModalOrOverlay(action: string, modalId: string, ind?: number) {
        (action === 'open') ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    }

    public iAmPermitted(routeName): boolean {
        return this.authService.iAmPermitted(routeName);
    }

    public editUser(ind: number) {
        this.editedIndex = ind;
        this.editedAgentUser = this.agentUsers[this.editedIndex];
        this.updateUserForm.patchValue(this.editedAgentUser);
        this.triggerModalOrOverlay('open', 'updateUser');
    }


}
