import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminComponent} from './admin/admin.component';
import {RouterModule, Routes} from '@angular/router';
import {BackendLoginComponent} from './login/login.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {LayoutModule} from "../layouts/layout.module";
import {AgentDashboardComponent} from './agent-dashboard/agent-dashboard.component';
import {AgentUsersComponent} from './agent-users/agent-users.component';
import {AgentUserViewComponent} from './agent-user-view/agent-user-view.component';
import {AgentSchoolsComponent} from './agent-schools/agent-schools.component';
import {AgentSchoolviewComponent} from './agent-schoolview/agent-schoolview.component';
import {AgentsComponent} from './agents/agents.component';
import {AgentViewComponent} from './agent-view/agent-view.component';
import {DataTablesModule} from "angular-datatables";
import { RecaptchaModule } from 'angular5-google-recaptcha';

import {environment} from "../../environments/environment";
// import {NgxCaptchaModule} from "ngx-captcha";


const BACKEND_ROUTES: Routes = [
  {path: '', component: BackendLoginComponent},
  {
    path: 'dashboard', component: AdminComponent,
    children: [
      {path: '', component: AgentDashboardComponent},
      {path: 'agents', component: AgentsComponent},
      {path: 'agents/:id', component: AgentViewComponent},
      {path: 'users', component: AgentUsersComponent},
      {path: 'users/:id', component: AgentUserViewComponent},
      {path: 'schools', component: AgentSchoolsComponent},
      {path: 'schools/:id', component: AgentSchoolviewComponent},
    ]

  }
]

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,
    DataTablesModule,
    RecaptchaModule.forRoot({
      siteKey: environment.CAPTCHA_KEY,
    }),
    // NgxCaptchaModule,
    // ReCaptchaModule,
    RouterModule.forChild(BACKEND_ROUTES)
  ],
  declarations: [
    AdminComponent,
    BackendLoginComponent,
    AgentDashboardComponent,
    AgentUsersComponent,
    AgentUserViewComponent,
    AgentSchoolsComponent,
    AgentSchoolviewComponent,
    AgentsComponent,
    AgentViewComponent
  ],
  exports: [
    // ReCaptchaModule,
  ]

})
export class BackendModule {
}
