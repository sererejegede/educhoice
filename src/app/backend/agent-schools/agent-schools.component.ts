import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';
import {ScriptLoaderService} from "../../_services/script-loader.service";
import {UserService} from "../../../services/user.service";
import {Router} from "@angular/router";
import {SchoolService} from "../../../services/school.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NotificationService} from "../../../services/notification.service";
import {AgentService} from "../../../services/agent.service";
import {environment} from "../../../environments/environment";

declare const $: any;

@Component({
    selector: 'app-agent-schools',
    templateUrl: './agent-schools.component.html',
    styleUrls: ['./agent-schools.component.css']
})
export class AgentSchoolsComponent implements OnInit {
    el: ElementRef;
    dtOptions: DataTables.Settings = {};
    //noinspection TypeScriptValidateTypes
    dtTrigger: Subject<any> = new Subject();
    dtElement: DataTableDirective = new DataTableDirective(this.el);

  public authenticatedUser: any;
    public domainName:string = `-portal.`+environment.DEFAULT_DOMAIN;
    public mySchools: any[] = [];
    public myUsers: any[] = [];
    public myAgents: any[] = [];
    public selectedAgent: any;
    public myEditedSchool: any = {};
    public createSchoolForm: FormGroup;
    public updateSchoolForm: FormGroup;
  public customDomain = false;
  public editedIndex: number;
    public load = {
        requesting: {
            list: false,
            create: false
        },
        message: {
            create: "Create",
            update: "Update"
        }
    }

    constructor(private userService: UserService,
                private router: Router,
                private schoolService: SchoolService,
                private agentService: AgentService,
                private fb: FormBuilder,
                private Alert: NotificationService) {
        this.authenticatedUser = this.userService.getAuthUser().login.user;
        if (this.userService.getAuthUser() == null) {
            this.router.navigateByUrl('/agent');
        }

        this.createSchoolForm = this.fb.group(AgentSchoolsComponent.createSchoolForm());
        this.updateSchoolForm = this.fb.group(AgentSchoolsComponent.updateSchoolForm());
        console.log("Authenticated user ", this.authenticatedUser);
    }

    static createSchoolForm = function () {
        return {
            name: ['', Validators.compose([Validators.required])],
            sub_domain: ['', Validators.compose([Validators.required,Validators.maxLength(15)])],
            domain: [''],
            user_id: '',
            agent_id: ''
        }
    }
    static updateSchoolForm = function () {
        return {
            name: ['', Validators.compose([Validators.required])],
            sub_domain: ['', Validators.compose([Validators.required])],
            user_id: '',
            agent_id: ''

        }
    }

    ngOnInit() {

        if (!this.isUser()) {
            (!this.visitorIsUpperlink()) ? this.getMyUsersAndSchools() : this.getAdminSchoolsAgentsAndUsers();
        }
        else {
            this.getMySchools();
        }


    }

    ngAfterViewInit() {

    }

    public triggerModal(action: string, modalId: string) {
        (action === "open") ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    }

    /**
     * This method gets a user's schools
     */

    public getAdminSchoolsAgentsAndUsers() {
        this.getMyAgents();
        this.getAllSchools();
    }

    public getMySchools() {

        this.load.requesting.list = true;
        this.schoolService.getMySchools().subscribe(
            (schoolsResponse) => {
                this.mySchools = schoolsResponse.data;
                this.load.requesting.list = false;
                this.mySchools.forEach((school) => {
                    school.url = `${school.configuration.domain}/staff`;
                });
                if (this.mySchools.length > 0) {
                    this.dtTrigger.next();
                }
                console.log("All My Schools == ", this.mySchools)
            },
            (error) => {
                this.load.requesting.list = false;
                this.Alert.error('Could not load your schools', error);
            },
            () => {

            }
        )
    }

    /**
     * This method creates a new school
     */
    public async createSchool() {
        this.load.requesting.create = true;
        this.load.message.create = "Creating...";
        const schoolData = this.createSchoolForm.value;
        await this.schoolService.createOrUpdateSchool(this.createSchoolForm.value).subscribe(
            (createdSchoolResponse) => {
                this.load.message.create = "Create";
                this.Alert.success(`${this.createSchoolForm.value.name} created successfully\n`);
                this.createSchoolForm = this.fb.group(AgentSchoolsComponent.createSchoolForm());
                this.load.requesting.create = false;
                this.triggerModal('close', 'createSchool');
                this.mySchools.unshift(createdSchoolResponse);
                console.log("Newly created school ", createdSchoolResponse)
            },
            (error) => {
                this.load.message.create = "Create";
                // this.triggerModal('close','createSchool');
                console.log("Eroorroroor ", error);
                this.load.requesting.create = false;
                this.Alert.error(`Could not create ${this.createSchoolForm.value.name}`, error)
            }
        );
    }

    public updateSchool() {
        this.load.requesting.create = true;
        this.load.message.update = "Updating...";

        this.schoolService.createOrUpdateSchool(this.updateSchoolForm.value, this.myEditedSchool.id).subscribe(
            (updatedSchoolResponse) => {
                this.load.message.update = "Update";
                this.updateSchoolForm = this.fb.group(AgentSchoolsComponent.updateSchoolForm());
                this.Alert.success(`${this.updateSchoolForm.value.name} updated successfully\n`);
                this.mySchools[this.editedIndex] = updatedSchoolResponse;
                this.load.requesting.create = false;
                this.triggerModal('close', 'updateSchool')

                console.log("Updated school ", updatedSchoolResponse)
            },
            (error) => {
                this.load.message.update = "Update";
                this.load.requesting.create = false;
                this.Alert.error(`Could not update ${this.updateSchoolForm.value.name}`, error)
            }
        )
    }

    public editSchool(ind: number) {
        this.editedIndex = ind;
        this.myEditedSchool = this.mySchools[this.editedIndex];
        this.updateSchoolForm.patchValue({
            name: this.myEditedSchool.name,
            sub_domain: this.myEditedSchool.configuration.subdomain,
            domain: this.myEditedSchool.configuration.domain
        });
        this.triggerModal('open', 'updateSchool');
    }

    public getAllSchools() {
        this.schoolService.getAllSchools().subscribe(
            (allSchoolsResponse) => {
                this.mySchools = allSchoolsResponse.data;
                console.log("All schools response ", allSchoolsResponse);
            }
        )
    }

    public isUser() {
        return this.userService.getAuthUser().login.user_type === 'App\\User';
    }

    public visitorIsUpperlink() {
        return this.agentService.visitorIsUpperlink();
    }

    public getMyUsersAndSchools() {
        this.getAllSchools();
        this.load.requesting.list = true;
        this.agentService.getAllAgentUsers().subscribe(
            (agentsResponse) => {
                this.load.requesting.list = false;
                this.myUsers = agentsResponse.data;

                console.log("All My Users == ", this.myUsers)
            },
            (error) => {
                this.load.requesting.list = false;
                this.Alert.error('Could not load your users', error);
            },
            () => {

            }
        )
    }

    public getMyAgents() {

        this.load.requesting.list = true;
        this.agentService.getMyAgents().subscribe(
            (AgentsResponse) => {
                this.load.requesting.list = false;
                this.myAgents = AgentsResponse.data;
                // this.myAgents.forEach((Agent) => {
                //     Agent.url = `http://${Agent.configuration.domain}`;
                // });
                console.log("All My Agents == ", AgentsResponse)
            },
            (error) => {
                this.load.requesting.list = false;
                console.log('Could not load your Agents', error);
            },
            () => {

            }
        )
    }

    public getUserByAgentId(id) {
        this.selectedAgent = this.selectWhereId(this.myAgents, 'id', id);
        this.myUsers = this.selectedAgent['users'];
    }


    public selectWhereId(data: any[], search_key: string, id) {
        let dataItem: any[] = [];
        data.forEach(item => {
            let itemKey = parseInt(item[search_key]);
            let itemId = parseInt(id);
            if (itemKey === itemId) {
                dataItem.push(item);
            }
        });
        return dataItem[0];

    }

    public setHeadColor(color) {
        return {
            'backgroud-color': '#d21502'
        }
    }

}
