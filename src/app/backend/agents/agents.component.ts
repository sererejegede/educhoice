import {Component, OnInit, AfterViewInit, ElementRef} from '@angular/core';
import {DataTableDirective} from 'angular-datatables';
import {Subject} from 'rxjs/Rx';
import {ScriptLoaderService} from "../../_services/script-loader.service";
import {UserService} from "../../../services/user.service";
import {Router} from "@angular/router";
import {AgentService} from "../../../services/agent.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NotificationService} from "../../../services/notification.service";

declare const $: any;

@Component({
    selector: 'app-agents',
    templateUrl: './agents.component.html',
    styleUrls: ['./agents.component.css']
})


export class AgentsComponent implements OnInit {

    dtOptions: DataTables.Settings = {};
    //noinspection TypeScriptValidateTypes
    dtTrigger: Subject<any> = new Subject();
    dtElement: DataTableDirective = new DataTableDirective(this.el);

    public authenticatedUser: any;
    public myAgents: any[] = [];
    public myEditedAgent: any = {};
    public createAgentForm: FormGroup;
    public updateAgentForm: FormGroup;
    public editedIndex: number;
    public load = {
        requesting: {
            list: false,
            create: false
        },
        message: {
            create: "Create",
            update: "Update"
        }
    }

    constructor(private userService: UserService,
                private router: Router,
                private Agentservice: AgentService,
                private fb: FormBuilder,
                private Alert: NotificationService,
                private el: ElementRef) {
        this.authenticatedUser = this.userService.getAuthUser().login.user;
        if (this.userService.getAuthUser() == null) {
            this.router.navigateByUrl('/agent');
        }
        this.createAgentForm = this.fb.group(AgentsComponent.createAgentForm());
        this.updateAgentForm = this.fb.group(AgentsComponent.updateAgentForm());
        console.log("Authenticated user ", this.authenticatedUser);
    }

    static createAgentForm = function () {
        return {
            name: ['', Validators.compose([Validators.required])],
            fullname: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],
            primary_color: ['', Validators.compose([Validators.required])],
            secondary_color: ['', Validators.compose([Validators.required])],
            staff_email: ['', Validators.compose([Validators.required])],
            employee_id: ['', Validators.compose([Validators.required])],
            subdomain: ['', Validators.compose([Validators.required])],
        }
    }
    static updateAgentForm = function () {
        return {
            name: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],
            primary_color: ['', Validators.compose([Validators.required])],
            secondary_color: ['', Validators.compose([Validators.required])],
            subdomain: ['', Validators.compose([Validators.required])],
        }
    }

    ngOnInit() {
        this.getMyAgents();


    }

    ngAfterViewInit() {

    }

    public triggerModal(action: string, modalId: string) {
        (action === "open") ? $(`#${modalId}`).modal('show') : $(`#${modalId}`).modal('hide');
    }

    /**
     * This method gets a user's Agents
     */
    public getMyAgents() {

        this.load.requesting.list = true;
        this.Agentservice.getMyAgents().subscribe(
            (AgentsResponse) => {
                this.load.requesting.list = false;
                this.myAgents = AgentsResponse.data;
                if (this.myAgents.length > 0) {
                    this.dtTrigger.next();
                }
                // this.myAgents.forEach((Agent) => {
                //     Agent.url = `http://${Agent.configuration.domain}`;
                // });
                console.log("All My Agents == ", AgentsResponse)
            },
            (error) => {
                this.load.requesting.list = false;
                this.Alert.error('Could not load your Agents', error);
            },
            () => {

            }
        )
    }

    /**
     * This method creates a new Agent
     */
    public async createAgent() {
        this.load.requesting.create = true;
        this.load.message.create = "Creating...";
        let agentData = this.createAgentForm.value;
        const agentStaffObject = {
            staff: {
                fullname: agentData['fullname'],
                email: agentData['staff_email'],
                employee_id: agentData['employee_id']
            },
            color: {
                primary: agentData['primary_color'],
                secondary: agentData['secondary_color']
            }
        };
        agentData = Object.assign(agentData, agentStaffObject);
        const bulkFileId = `#agentLogo`;
        const formFile = this.el.nativeElement.querySelector(bulkFileId);
        console.log('Form File ', formFile);
        const $linkId = $(bulkFileId);
        const $newLinkId = $linkId.val();
        const file_type = $newLinkId.substr($newLinkId.lastIndexOf('.')).toLowerCase();
        // const file_type = '';
        $linkId.removeClass('alert alert-danger animated rubberBand');
        if (file_type && (file_type !== '.png') && (file_type !== '.jpg') && (file_type !== '.jpeg')) {
            $linkId.addClass('alert alert-danger animated rubberBand');
            this.load.requesting.create = false;
            return this.Alert.error('You can only upload png, jpg or jpeg files');
        }

        await this.Agentservice.createOrUpdateAgent(agentData, formFile).subscribe(
            (createdAgentResponse) => {
                this.load.message.create = "Create";
                this.Alert.success(`${this.createAgentForm.value.name} created successfully\n`);
                this.createAgentForm = this.fb.group(AgentsComponent.createAgentForm());
                this.load.requesting.create = false;
                this.triggerModal('close', 'createAgent');
                this.myAgents.push(createdAgentResponse);
                console.log("Newly created Agent ", createdAgentResponse)
            },
            (error) => {
                this.load.message.create = "Create";
                // this.triggerModal('close','createAgent');
                console.log("Eroorroroor ", error);
                this.load.requesting.create = false;
                this.Alert.error(`Could not create ${this.createAgentForm.value.name}`, error)
            }
        );
    }

    public updateAgent() {
        this.load.requesting.create = true;
        this.load.message.update = "Updating...";

        let agentData = this.updateAgentForm.value;
        agentData['color'] = {
            primary: agentData['primary_color'],
            secondary: agentData['secondary_color']
        };

        const formFile = this.el.nativeElement.querySelector(`#agentupdateLogo`);
        console.log('Form File ', formFile);
        const $linkId = $(`#agentupdateLogo`);
        const file_type = $linkId.val().substr($linkId.val().lastIndexOf('.')).toLowerCase();

        $linkId.removeClass('alert alert-danger animated rubberBand');
        if (file_type && (file_type !== '.png') && (file_type !== '.jpg') && (file_type !== '.jpeg')) {
            $linkId.addClass('alert alert-danger animated rubberBand');
            this.load.requesting.create = false;
            return this.Alert.error('You can only upload png, jpg or jpeg files');
        }

        this.Agentservice.createOrUpdateAgent(agentData, formFile, this.myEditedAgent.id).subscribe(
            (updatedAgentResponse) => {
                this.load.message.update = "Update";
                this.updateAgentForm = this.fb.group(AgentsComponent.updateAgentForm());
                this.Alert.success(`${this.updateAgentForm.value.name} updated successfully\n`);
                this.myAgents[this.editedIndex] = updatedAgentResponse;
                this.load.requesting.create = false;
                this.triggerModal('close', 'updateAgent')

                console.log("Updated Agent ", updatedAgentResponse)
            },
            (error) => {
                this.load.message.update = "Update";
                this.load.requesting.create = false;
                this.Alert.error(`Could not update ${this.updateAgentForm.value.name}`, error)
            }
        )
    }

    public editAgent(ind: number) {
        this.editedIndex = ind;
        this.myEditedAgent = this.myAgents[this.editedIndex];
        console.log('Logged agent data ', this.myEditedAgent);
        const staffObject = this.myEditedAgent['staff'][0];
        this.myEditedAgent['staff_email'] = staffObject['email'];
        this.myEditedAgent['fullname'] = staffObject['fullname'];
        this.myEditedAgent['employee_id'] = staffObject['employee_id'];
        this.myEditedAgent['primary_color'] = (this.myEditedAgent['color'])?this.myEditedAgent['color']['primary']:null;
        this.myEditedAgent['secondary_color'] = (this.myEditedAgent['color'])?
        this.myEditedAgent['color']['secondary']:null;
        this.updateAgentForm.patchValue(this.myEditedAgent);
        this.triggerModal('open', 'updateAgent');
    }

    public getAllAgents() {
        this.Agentservice.getAllAgents().subscribe(
            (allAgentsResponse) => {
                console.log("All Agents response ", allAgentsResponse);
            }
        )
    }



    public setHeaderColor(element?:string):any {

        let navHeaderColor = '#0a1529';
        let theadColor =  '#d21502';

        return (element==='nav')? {'background-color': navHeaderColor}:{'background-color':theadColor,'color':'white'}
    }
}



