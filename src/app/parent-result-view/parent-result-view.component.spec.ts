import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentResultViewComponent } from './parent-result-view.component';

describe('ParentResultViewComponent', () => {
  let component: ParentResultViewComponent;
  let fixture: ComponentFixture<ParentResultViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentResultViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentResultViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
