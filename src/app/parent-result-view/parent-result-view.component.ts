import {Component, OnInit} from '@angular/core';
import {Validators, FormGroup, FormBuilder} from "@angular/forms";
import {ResultsService} from "../../services/api-service/results.service";
import {NotificationService} from "../../services/notification.service";
import {Cache} from "../../utils/cache";

@Component({
  selector: 'app-parent-result-view',
  templateUrl: './parent-result-view.component.html',
  styleUrls: ['./parent-result-view.component.css']
})
export class ParentResultViewComponent implements OnInit {
  load: any = {
    printing: false
  };
  showResults: boolean;
  allViewedResults: any[] = [];
  loadingResult: boolean;
  appSettings: any;
  viewedStudent: any;
  studentInfo: any;


  public studentDetailsForm: FormGroup;

  static studentDetailsForm = () => {
    return {
      dob: ['', Validators.required],
      matric_no: ['', Validators.required],
      surname: ['', Validators.required]
    }
  }

  constructor(
    private resultService: ResultsService,
    private Alert: NotificationService,
    private fb: FormBuilder
  ) {
    this.studentDetailsForm = this.fb.group(ParentResultViewComponent.studentDetailsForm());
    this.appSettings = Cache.get('app_settings') || Cache.get('ping');
  }

  ngOnInit() {

  }


  public getResultForParentView() {
    this.loadingResult = true;
    this.resultService.getResultForParentView(this.studentDetailsForm.value).subscribe((resultResponse) => {
        this.loadingResult = false;
        this.showResults = true;
        this.allViewedResults = resultResponse;
        this.studentInfo = resultResponse[0];
        this.viewedStudent = this.studentInfo['student'];
        console.log("Viewed Student", this.studentInfo.course_of_study);
        this.processComputation();
      },
      (error) => {
        this.loadingResult = false;
        this.showResults = false;
        this.Alert.error(`Could not load results`, error);
      });
  }


  public almightyPrint(id, title) {
    this.load.printing = true;
    const that = this;
    const prev_frame = $('#print_frame');
    if (prev_frame) {
      prev_frame.remove();
    }
    const contents = $('#' + id).html();
    const frame1 = $('<iframe />');
    frame1[0]['id'] = 'print_frame';
    frame1[0]['name'] = 'frame1';
    frame1.css({'position': 'absolute', 'top': '-1000000px', 'pointer-events': 'none'});
    $('body').append(frame1);
    const frameDoc = frame1[0]['contentWindow'] ? frame1[0]['contentWindow'] : frame1[0]['contentDocument'].document ? frame1[0]['contentDocument'].document : frame1[0]['contentDocument'];
    frameDoc.document.open();
    frameDoc.document.write('<html><head><title>Print ' + title + '</title>');
    frameDoc.document.write('</head><body>');
    frameDoc.document.write('<link href="/assets/css/style.bundle.css" rel="stylesheet">');
    frameDoc.document.write('<style>.logo-img {max-width: 100%;max-height: 200px;display: block;margin: auto;}</style>');
    frameDoc.document.write(contents);
    frameDoc.document.write('</body>');
    frameDoc.document.write('</html>');
    frameDoc.document.close();
    frameDoc.onload = function () {
      window.frames['frame1'].focus();
      window.frames['frame1'].print();
      // frame1.remove();
      that.load.printing = false;
    }
  }

  private processComputation() {
    let cg = {
      tc: 0,
      twgp: 0
    };
    for (let i = 0; i < this.allViewedResults.length; i++) {
      let session_result = this.allViewedResults[i];
      session_result.semestersData = session_result.semestersData.sort((x, y) => x.semester_info.semester - y.semester_info.semester);
      for (let j = 0; j < session_result.semestersData.length; j++) {
        let semester_result = session_result.semestersData[j];
        if (j > 0) {
          semester_result['current_tc'] = session_result.semestersData[j - 1].current_tc + this.computeStuff(semester_result.result, 'cu');
          semester_result['current_twgp'] = session_result.semestersData[j - 1].current_twgp + this.computeStuff(semester_result.result, 'cp');
        } else {
          semester_result['current_tc'] = this.computeStuff(semester_result.result, 'cu') + cg.tc;
          semester_result['current_twgp'] = this.computeStuff(semester_result.result, 'cp') + cg.twgp;
        }
        if (j === session_result.semestersData.length - 1) {
          cg.tc = semester_result['current_tc'];
          cg.twgp = semester_result['current_twgp'];
        }
      }
    }
    // console.info(this.allViewedResults);
  }

  public computeStuff(results: any[], stuff): number {
    return results.reduce((a, b) => a + parseInt(b[stuff], 10), 0);
  }

  public setGradeColor(score) {
    return (score < 40) ? {'color': 'red'} : {};
  }


}
