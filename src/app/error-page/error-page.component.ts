import { Component, OnInit } from '@angular/core';
import {ActivationEnd, Router} from "@angular/router";

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit {

  public showBackButton:boolean = true;

  constructor( private router:Router) {
      this.router.events.subscribe(val => {
          if (val instanceof ActivationEnd) {
              const paramKey = val.snapshot.params['txhref'];
              if(paramKey && paramKey.toLowerCase() === 'void'){
                this.disableBackButton();
              }
          }
      });
  }

  /**
   * This is used to navigate back to previous url while 404 error is encountered.
   */
  goBackUrl() {
    window.history.back();
  }

  private disableBackButton(){
      this.showBackButton = false;
     window.location.hash='404';//disable browser back button
  }

  ngOnInit() {
  }

}
