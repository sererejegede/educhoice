import {Component, Inject} from '@angular/core';
import {DOCUMENT} from "@angular/common";
import {environment} from '../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';
import {SchoolService} from '../services/school.service';
import {Cache} from '../utils/cache';
import {AgentService} from "../services/agent.service";
import {NotificationService} from "../services/notification.service";
import {Title} from "@angular/platform-browser";
import * as APPCOOKIES from "../utils/cookies";


declare const Tawk_API: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor(private _router: Router,
              private _activeRoute: ActivatedRoute,
              private schoolService: SchoolService,
              private agentService: AgentService,
              private Alert: NotificationService,
              @Inject(DOCUMENT) private _document: HTMLDocument,
              private titleService: Title) {

    this.setVisitor();
    if (!Cache.get('educhoice_language')) {
      Cache.set('educhoice_language', navigator.language || navigator['userLanguage']);
    }
    // this.setUpAppChat('5b06d1e910b99c7b36d4453d');
    // m-table--head-bg-brand
  }


  private getAppSettings() {
    this.schoolService.getAppSettings().subscribe((appSettingsResponse) => {
        Cache.set('app_settings', appSettingsResponse);
        APPCOOKIES.set('app_settings', appSettingsResponse, 1);
        // console.log('App Settings ', appSettingsResponse)
        if (appSettingsResponse['settings'] && appSettingsResponse['settings']['tawkto_chat_app_code']) {
          this.setUpAppChat(appSettingsResponse['settings']['tawkto_chat_app_code'])
        }
        // this.setTableSecondaryColor(appSettingsResponse);
        this.setTitleAndFavIcon(appSettingsResponse);
      },
      (error) => {
        if (JSON.parse(error['_body'])['message'].toString().toLowerCase() === 'school not found') {
          this._router.navigate(['/not-found', {txhref: 'void'}]);
        }
      });
  }

  public getAgentSettings() {
    this.agentService.getAgentSettings().subscribe(
      (agentSettingsResponse) => {
        Cache.set('is_agent', agentSettingsResponse);
        APPCOOKIES.set('is_agent', agentSettingsResponse, 1);
        this.setTitleAndFavIcon(agentSettingsResponse);
        console.log('Agent settings response ', agentSettingsResponse);
      },
      (error) => {
        console.log('An error occurred. Could not load your settings.')
      }
    )
  }

  private setVisitor() {
    const HOST = window.location;
    const APP_DEFAULT_DOMAIN = environment.DEFAULT_DOMAIN;
    const HOST_NAME = HOST.hostname;
    const SPLIT_HOST = HOST_NAME.split(APP_DEFAULT_DOMAIN);
    const isAgent = (SPLIT_HOST.length === 2 && SPLIT_HOST[0] !== "");
    const urlHasPortal = SPLIT_HOST[0].includes('portal');
    const appIsHost = (HOST_NAME === APP_DEFAULT_DOMAIN);
    const appIncludesHost = (HOST_NAME.includes(APP_DEFAULT_DOMAIN));
    const prepareReroute = '/home';
    setTimeout(() => {
      if (appIsHost) {
        this.setTitleAndFavIcon('Educhoice');
        if (HOST.pathname === prepareReroute || HOST.pathname === '/') {
          return this._router.navigateByUrl(prepareReroute)
        }

        if (HOST.pathname === 'reset-password' || HOST.pathname === '/home/reset-password') {
          return this._router.navigateByUrl(HOST.pathname);
        }
        return this._router.navigateByUrl(HOST.pathname);

      }
      if (!appIsHost && appIncludesHost && isAgent && !urlHasPortal) {
        this.getAgentSettings();

        if (!HOST.href.includes(prepareReroute)) {
          return this._router.navigateByUrl(prepareReroute)
        }
        return this._router.navigateByUrl(HOST.pathname);
      }
      if (isAgent || urlHasPortal || !appIncludesHost) {
        if (HOST.href.includes(prepareReroute)) {
          return this._router.navigateByUrl('/');
        }
        if (HOST.pathname.includes('staff')) {
          this._router.navigateByUrl(HOST.pathname);
        }
        else if (HOST.pathname.includes('student') || HOST.pathname.includes('appl')) {
          this._router.navigateByUrl(HOST.pathname);
        }
      }
    }, 2000);
    this.getAppSettings();
    // this._router.navigateByUrl('/agent');
  }

  public setTitleAndFavIcon(SettingsResponse) {
    (SettingsResponse.name) ? this.titleService.setTitle(SettingsResponse.name) : this.titleService.setTitle(SettingsResponse);
    // $('#appIcon').remove();
  }

  private setTableSecondaryColor(settingsResponse) {
    $(`table.m-table--head-bg-brand thead`).css('background-color', `${settingsResponse['settings']['secondary_color']}`);
  }


  private setUpAppChat(appChatKey) {
    let Tawk_LoadStart = new Date();
    let s1 = document.createElement("script")
    let s0 = document.getElementsByTagName("script")[0];
    s1.async = true;
    s1.src = `https://embed.tawk.to/${appChatKey}/default`;
    s1.charset = 'UTF-8';
    s1.setAttribute('crossorigin', '*');
    s0.parentNode.insertBefore(s1, s0);
  };


}
