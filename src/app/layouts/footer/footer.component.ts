import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {Cache} from "../../../utils/cache";

// import { Helpers } from '../../../helpers';


@Component({
  selector: 'app-footer-general',
  templateUrl: './footer.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class FooterComponent implements OnInit {
  public portaldetail = {name:'Bowen'};

  constructor(private userService: UserService) {

  }

  ngOnInit() {
    if (this.userService.getAuthUser()) {
      this.portaldetail = Cache.get('ping');
    }
  }

}
