import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UserService} from "../../../services/user.service";
import {AuthenticationService} from "../../../services/authentication.service";
import {Cache} from "../../../utils/cache";
import {environment} from "../../../environments/environment";
import {AgentService} from "../../../services/agent.service";

@Component({
    selector: 'app-agent-header-nav',
    templateUrl: './agent-header-nav.component.html',
    styleUrls: ['./agent-header-nav.component.css']
})
export class AgentHeaderNavComponent implements OnInit, AfterViewInit {

    public authenticatedUser: any;
    public isUpperlink: any;
    public isAgent: any = {};
    public navHeaderColor: string = '';
    public theadColor: string = '';

    constructor(private userService: UserService,
                private authService: AuthenticationService,
                private agentService: AgentService) {
        this.authenticatedUser = this.userService.getAuthUser().login.user;
        this.isUpperlink = this.visitorIsUpperlink();
        if (!this.visitorIsUpperlink()) {
            this.getAgentSettings();
        }
        console.log("This is ahhjha jhajh agent headerrsd ", this.isAgent)
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        // this.setTableSecondaryColor();
    }

    public myModuleIsPermitted(moduleName): boolean {
        return this.authService.myModuleIsPermitted(moduleName);
    }

    public iAmPermitted(routeName): boolean {
        return this.authService.iAmPermitted(routeName);
    }

    //
    // public setHeaderColor(element?:string):any {
    //
    //     this.navHeaderColor = (this.isAgent['color']) ? this.isAgent['color']['primary'] : '#0a1529';
    //     this.theadColor = (this.isAgent['color']) ? this.isAgent['color']['secondary'] : '#d21502';
    //
    //     return (element==='nav')? {'background-color': this.navHeaderColor}:{'background-color': this.theadColor}
    // }

    public logUserOut() {
        this.authService.logout('home').subscribe(
            (response) => {
                console.log('Log out done!');
            },
            (error) => {
                console.log('Could not log user out');
                // this.userService.logout('home');

            }
        )

    }

    public isUser() {
        return this.userService.getAuthUser().login.user_type === 'App\\User';
    }

    public visitorIsUpperlink() {
        return this.agentService.visitorIsUpperlink();

    }

    public getAgentSettings() {
        this.agentService.getAgentSettings().subscribe(
            (agentSettingsResponse) => {
                this.isAgent = agentSettingsResponse;


                console.log('Agent settings response ', agentSettingsResponse);
            },
            (error) => {
                console.log('An error occurred. Could not load your settings.')
            }
        )
    }

    public setHeaderColor() {

        this.navHeaderColor = (this.isAgent['color']) ? this.isAgent['color']['primary'] : '#0a1529';
        this.theadColor = (this.isAgent['color']) ? this.isAgent['color']['secondary'] : '#d21502';
        this.setActiveSpanColor();
        // console.log('Primary color that is supposed to be nav color', this.appSettings['settings']['primary_color']);



        return {
            'background-color': `${this.navHeaderColor}` //this.appSettings['primary_color']
        };
    }

    private setActiveSpanColor() {
        $(`span.m-menu__item-here`).css('color', `${this.navHeaderColor}`);
        $('thead tr th').css('background-color', `${this.theadColor}`);

    }

    // private setTableSecondaryColor() {
    //
    // }

}
