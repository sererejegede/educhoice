import {AfterViewInit, Component, OnInit} from "@angular/core";
import {UserService} from "../../../services/user.service";
import {AuthenticationService} from "../../../services/authentication.service";
import {Cache} from "../../../utils/cache";
import {SchoolService} from "../../../services/school.service";
import {NavigationEnd, Router} from "@angular/router";
import {ScriptLoaderService} from "../../../services/script-loader.service";
import {EmitSettingsService} from "../../../services/emitSettings.service";
import * as APPCOOKIES from "../../../utils/cookies";

declare const mLayout: any;

// declare const $: any;

@Component({
  selector: 'app-admin-header-nav',
  templateUrl: './admin-header-nav.component.html',
  styleUrls: ['./admin-header-nav.component.css']
})
export class AdminHeaderNavComponent implements OnInit, AfterViewInit {

  public authenticatedUser: any;
  public routesHaveBeenChecked: boolean = false;
  public userName: string = '';
  public appLogo: string;
  public language: string;
  public schoolName: string;
  public appSettings: any = {};
  public portaldetail = [];
  public userModules = [];
  public headerModules = [
    '_Administration',
    '_Configuration',
    '_Admission'

  ];
  public userPermissions = {
    userRoutes: [],
    userModules: []
  };
  public enableAdmissionOnAcceptance = false;

  constructor(private userService: UserService,
              private authService: AuthenticationService,
              private schoolService: SchoolService,
              private _script: ScriptLoaderService,
              private router: Router,
              private emitSettingsService: EmitSettingsService) {
    this.authenticatedUser = this.userService.getAuthUser();
    // console.log('auth user', this.authenticatedUser);
    this.userName = this.authenticatedUser.login.username;
    if (Cache.get('app_settings')) {
      this.appSettings = Cache.get('app_settings') || Cache.get('ping');
      this.enableAdmissionOnAcceptance = (this.appSettings['settings']['enable_admit_acceptance'].toString() == '1');
      this.portaldetail = Cache.get('app_settings');
      this.schoolName = (this.appSettings.name) ? this.appSettings.name : this.schoolName;
      // console.log('school name :: ', this.portaldetail);
    } else {
      this.appSettings = APPCOOKIES.get('app_settings');
      this.getAllSettings();
    }

    emitSettingsService.changeEmitted$.subscribe(
      (text) => {

        this.appSettings = Cache.get('app_settings');
        this.setAppLogo(text);
      });
    this.language = Cache.get('educhoice_language');
  }

  ngOnInit() {
    if (this.userService.getAuthUser()) {
      this.portaldetail = Cache.get('ping');
      if (this.userService.getAuthUser()['login']['user_type'] !== 'App\\Models\\School\\Auth\\Staff') {
        this.router.navigateByUrl('/staff');
      }
    }
  }

  ngAfterViewInit() {
    if (!this.routesHaveBeenChecked) {
      this.checkRoutes();
    }

    // this._script.loadScript('app-admin-header-nav','assets/js/dashboard.js');
    // this.setTableSecondaryColor();
    mLayout.initHeader();

// this.setTableSecondaryColor();
  }

  public logUserOut() {
    this.authService.logout('stay').subscribe(
      (response) => {
        // console.log('Log out done!');
      },
      (error) => {
        console.log('Could not log user out');
        this.userService.logout('stay');

      });

  }

  public setLanguage(language) {
    Cache.set('educhoice_language', language);
    window.location.reload();
  }

  public getAllSettings() {
    this.getAppSettings();
  }

  private getAppSettings() {
    this.schoolService.getAppSettings().subscribe((appSettingsResponse) => {
        // console.log('Application setings ', appSettingsResponse);
        Cache.set('app_settings', appSettingsResponse);
        APPCOOKIES.set('app_settings', appSettingsResponse, 1);
        this.appSettings = Cache.get('app_settings')['settings'];
        this.schoolName = this.appSettings.name;
        // console.log('Application settings from admin hedr ', this.appSettings)
      },
      (error) => {
        console.log('Could not load application settings ', error);
      });
  }

  public setHeaderColor() {
    this.setActiveSpanColor();
    this.setTableSecondaryColor();
    // console.log('Primary color that is supposed to be nav color', this.appSettings['settings']['primary_color']);
    return {
      'background-color': `${this.appSettings['settings']['primary_color']}` //this.appSettings['primary_color']
    };
  }

  private setActiveSpanColor() {
    $(`span.m-menu__item-here`).css('color', `${this.appSettings['settings']['primary_color']}`);
  }

  private setAppLogo(logoSrc) {
    // $('#app_logo').attr('src',null);
    $('#app_logo').attr('src', logoSrc);
    // this.appLogo = logoSrc;
  }

  public renderDynamicHeaderLinks(userPermissions) {
    const userModules: string[] = userPermissions.userModules;
    // console.log('User modules from dynamic header ', userModules)
    const userRoutes: string[] = userPermissions.userRoutes;
    $('#Report').remove();

    this.headerModules.forEach((headerModule) => {
      const headerLinkId = `#${headerModule}`;
      // console.log('Header link eyedee ', headerLinkId)
      if (!userModules.includes(headerModule)) {
      }
    })


  }

  public myModuleIsPermitted(moduleName): boolean {
    return this.authService.myModuleIsPermitted(moduleName);
  }

  public iAmPermitted(routeName): boolean {
    return this.authService.iAmPermitted(routeName);
  }

  private checkRoutes() {
    const show_debtors = this.appSettings && (this.appSettings.name.toLowerCase().includes('bowen') || this.appSettings.name.toLowerCase().includes('crysto'));
    for (let obj in document.getElementsByTagName('li')) {
      const elementId = document.getElementsByTagName('li')[obj].id;
      const element = document.getElementById(elementId);
      if (elementId && !this.iAmPermitted(elementId)) {
        element.parentNode.removeChild(element)
      }
    }
    this.routesHaveBeenChecked = true;
  }

  private setTableSecondaryColor() {

    const appSecondaryColor = (this.appSettings['settings']) ? this.appSettings['settings']['secondary_color'] : this.appSettings['secondary_color'];
    $('thead tr th').css('background-color', `${appSecondaryColor}`);

  }


}
