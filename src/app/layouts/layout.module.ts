import {NgModule} from '@angular/core';
import {AsideNavComponent} from './student-aside-nav/aside-nav.component';
import {HeaderNavComponent} from './student-header-nav/header-nav.component';
import {FooterComponent} from './footer/footer.component';
import {QuickSidebarComponent} from './quick-sidebar/quick-sidebar.component';
import {ScrollTopComponent} from './scroll-top/scroll-top.component';
import {TooltipsComponent} from './tooltips/tooltips.component';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {UnwrapTagDirective} from '../_directives/unwrap-tag.directive';
import {AdminHeaderNavComponent} from './admin-header-nav/admin-header-nav.component';
import {PipeModule} from "../../shared/modules/pipe.module";
import { AgentHeaderNavComponent } from './agent-header-nav/agent-header-nav.component';

@NgModule({
    declarations: [
        AsideNavComponent,
        HeaderNavComponent,
        // AsideLeftDisplayDisabledComponent,
        FooterComponent,
        QuickSidebarComponent,
        ScrollTopComponent,
        TooltipsComponent,
        UnwrapTagDirective,
        AdminHeaderNavComponent,
        AgentHeaderNavComponent,
    ],
    exports: [
        AsideNavComponent,
        HeaderNavComponent,
        AdminHeaderNavComponent,
        AgentHeaderNavComponent,

        // AsideLeftDisplayDisabledComponent,
        FooterComponent,
        QuickSidebarComponent,
        ScrollTopComponent,
        TooltipsComponent,
    ],
    imports: [
        CommonModule,
        RouterModule,
        PipeModule
    ]
})
export class LayoutModule {
}
