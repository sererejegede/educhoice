import {Component, OnInit, ViewEncapsulation, AfterViewInit} from '@angular/core';
import {Helpers} from '../../portal/studentArea/student-dashboard/helpers';
import {Cache} from "../../../utils/cache";
import {UserService} from "../../../services/user.service";
import {AuthenticationService} from "../../../services/authentication.service";
import {Router} from "@angular/router";
import {LogoutServiceService} from "../../../services/api-service/login-service.service";
import {StudentServiceService} from "../../../services/api-service/student-service.service";
import {NotificationService} from "../../../services/notification.service";

declare let mLayout: any;

@Component({
  selector: 'app-header-student-nav',
  templateUrl: './header-nav.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./header-nav.component.css']
})
export class HeaderNavComponent implements OnInit, AfterViewInit {
  public userDetail = [];
  public edited: number;
  public portaldetail;
  public logo = '';
  public appSettings;
  public loadForms = {
    oath: false

  };
  public formUrl: any;


  constructor(private userService: UserService,
              private loginService: LogoutServiceService,
              private authService: AuthenticationService,
              private studentService: StudentServiceService,
              private Alert: NotificationService,
              private router: Router) {
    this.appSettings = Cache.get('app_settings');

    console.log('APP SETTINGS', this.appSettings);
    const student = Cache.get('cbsp_student_profile');
    this.edited = student.edit_status;
    if (this.edited === 0) {
      this.router.navigateByUrl('/student/profile')
    }

  }

  ngOnInit() {

    if (this.userService.getAuthUser()) {
      this.userDetail = this.userService.getAuthUser()['login']['user'];
      this.portaldetail = Cache.get('ping') || Cache.get('app_settings');
    }
    if (Cache.get('ping')) {
      this.logo = Cache.get('ping')['settings']['logo'];
    } else {
      this.getPingStudent();
    }
    if (this.userService.getAuthUser()) {
      if (this.userService.getAuthUser()['login']['user_type'] !== 'App\\Models\\School\\Student') {
        this.router.navigateByUrl('/');
      }
    }
  }

  getPingStudent() {
    this.loginService.pingDomain()
      .subscribe((response) => {
          Cache.set('ping', response);
          if (Cache.get('ping')) {
            this.logo = Cache.get('ping')['settings']['logo'];
          }
        },
        error => {
          console.log('error from profile ::', error);
        });
  }

  logout() {
    this.authService.logout()
      .subscribe(response => {
        console.log('logout');
      });
  }

  public setHeaderColor() {
    this.setActiveSpanColor();
    // console.log('Primary color that is supposed to be nav color', this.appSettings['settings']['primary_color']);

    return (this.appSettings) ? {
      'background-color': `${this.appSettings['settings']['primary_color']}` //this.appSettings['primary_color']
    } : {'background-color': ''};
  }

  private setActiveSpanColor() {
    const primaryColor = (this.appSettings && this.appSettings['settings']) ? this.appSettings['settings']['primary_color'] : '';
    $(`span.m-menu__item-here`).css('color', `${primaryColor}`);
  }

  public checkAndDownloadOathForm() {
    this.loadForms['oath'] = true;
    this.studentService.checkAndDownloadOathForm().subscribe(
      (response) => {
        console.log('Oath form response ', response);

        this.loadForms['oath'] = false;

        this.formUrl = response['download_link'];
        const downloadButton = $(`#oath_form`);
        downloadButton.attr('href', this.formUrl);
        downloadButton[0].click();


      },
      error => {
        this.loadForms['oath'] = false;
        this.Alert.error('Could not download oath form', error);
      });
  }

  ngAfterViewInit() {

    mLayout.initHeader();

  }

}
