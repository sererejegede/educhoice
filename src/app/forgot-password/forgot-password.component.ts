import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NotificationService} from "../../services/notification.service";
import {SchoolService} from "../../services/school.service";
import {LogoutServiceService} from "../../services/api-service/login-service.service";
import {StudentServiceService} from "../../services/api-service/student-service.service";
import {ActivatedRoute, ActivationEnd, Router} from "@angular/router";
import {AuthenticationService} from "../../services/authentication.service";
import {AdmissionsService} from "../../services/api-service/admissions.service";
import {EmitPaymentService} from "../../services/emit-payment-service";
import {environment} from "../../environments/environment";
import {Cache} from "../../utils/cache";
import {UserService} from "../../services/user.service";


@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
    public feedback = {
        loadingStatus: false
    };
    public logo = '';
    public ping;
    public resetRequest: boolean = false;
    public resetResponseMessage: string = '';
    public userType: any;
    public appSettings: any = {};
    public loginForm: FormGroup;
    public applicantLoginForm: FormGroup;
    public IsBonafideApplicant: boolean;
    public authenticatedUser: any = {};
    public applicant = {
        hasPaid: false
    };

    static userLoginForm = function () {
        return {
            email: ['', Validators.compose([Validators.required, Validators.email])],
            recaptcha: ['', Validators.required]

        };
    };
    static applicantLoginForm = function () {
        return {
            email: ['', Validators.compose([Validators.required])],
            password: ['', Validators.compose([Validators.required])],
        };
    };

    public userRouteParams = {
        'xap2k': 'applicant',
        'yst3l': 'student',
        'zsta4m': 'staff',
        'xyzag5n': 'agent'
    };

    public captcha = {
        siteKey:environment.CAPTCHA_KEY,
        size:'normal',
        lang:'en',
        theme:'theme',
        type:'type'
    }



    constructor(private fb: FormBuilder,
                private Alert: NotificationService,
                private userService: UserService,
                private schoolService: SchoolService,
                private loginService: LogoutServiceService,
                private studentServiceService: StudentServiceService,
                private router: Router,
                private route: ActivatedRoute,
                private authService: AuthenticationService,
                private admissionService: AdmissionsService,
                private emitPaymentService: EmitPaymentService) {

        this.router.events.subscribe(val => {
            if (val instanceof ActivationEnd) {
                const paramKey = val.snapshot.params['txhref'];
                this.userType = this.userRouteParams[paramKey];
            }
        });

        this.loginForm = this.fb.group(ForgotPasswordComponent.userLoginForm());
        this.applicantLoginForm = this.fb.group(ForgotPasswordComponent.applicantLoginForm());
    }


    ngOnInit() {
        this.getPingStudent();

    }


    public requestPasswordReset() {
        this.feedback.loadingStatus = true;
        const return_url =(this.userType!=='agent')?window.location.origin + '/reset-password'
        :window.location.origin + '/home/reset-password';

        const passwordResetObject = this.loginForm.value;
        passwordResetObject['user_type'] = this.userType;
        passwordResetObject['return_url'] = return_url;
        this.portalRequest(passwordResetObject);

    }

    private portalRequest(passwordResetObject){
        this.authService.requestPasswordReset(passwordResetObject).subscribe(
            (passwordResetResponse) => {
                this.resetRequest = true;
                this.Alert.success(`${passwordResetResponse.message}`);
                this.resetResponseMessage = `A password reset link has been sent to ${this.loginForm.value['email']}`;
                this.feedback.loadingStatus = false;
                setTimeout(() => {
                    this.resetRequest = false;
                }, 3000)

                console.log('Password reset response ', passwordResetResponse);
            },
            (error) => {
                this.feedback.loadingStatus = false;
                this.Alert.error(`Invalid credentials`, error);
                console.log('Password reset errorororoor ', error);

            }
        )
    }


    private agentRequest(passwordResetObject){
        this.authService.requestPasswordReset(passwordResetObject).subscribe(
            (passwordResetResponse) => {
                this.resetRequest = true;
                this.Alert.success(`${passwordResetResponse.message}`);
                this.resetResponseMessage = `A password reset link has been sent to ${this.loginForm.value['email']}`;
                this.feedback.loadingStatus = false;
                setTimeout(() => {
                    this.resetRequest = false;
                }, 3000)

                console.log('Password reset response ', passwordResetResponse);
            },
            (error) => {
                this.feedback.loadingStatus = false;
                this.Alert.error(`Invalid credentials`, error);
                console.log('Password reset errorororoor ', error);

            }
        )
    }


    // private setBgImage() {
    //   console.log(this.ping['settings']['bg_image']);
    //   if (this.ping['settings']['bg_image']) {
    //     const staffStyle = $(`.overlay`);
    //     staffStyle.css({background: `linear-gradient(rgba(0, 0, 0, 0.55), rgba(67, 34, 167, 0.7)), rgba(0, 0, 0, 0.55) url(${this.ping['settings']['bg_image']}) no-repeat center`, backgroundSize: 'cover'});
    //   }
    // }

    public logInApplicant() {
        this.feedback.loadingStatus = true;
        if (localStorage.getItem('biodata')) {
            // localStorage.setItem('biodata', null);
        }
        this.applicantLoginForm.value['user_type'] = 'applicant';
        this.admissionService.applicantLogin(this.applicantLoginForm.value).subscribe(
            (loginResponse) => {
                this.feedback.loadingStatus = false;
                this.userService.setAuthUser(loginResponse.token);
                const applicantId = this.userService.getAuthUser()['login']['user']['id'];
                Cache.set('LOGOUT_URL', '/');

                this.getApplicantProfile(applicantId);
            },
            (error) => {
                this.feedback.loadingStatus = false;
                this.Alert.error('Login Failed. Please check your credentials and retry!', error);
            }
        );
    }


    public flipRegistrationDiv(id: string) {
        if (id === 'signup') {
            const login = $('#m_login');
            login.removeClass('m-login--forget-password');
            login.removeClass('m-login--signin');

            login.addClass('m-login--signup');
            (<any>login.find('.m-login__signup')).animateClass('flipInX animated');
            return;
        } else {
            const login = $('#m_login');
            login.removeClass('m-login--forget-password');
            login.removeClass('m-login--signup');
            login.addClass('m-login--signin');
            (<any>login.find('.m-login__signin')).animateClass('flipInX animated');
        }

    }


    getPingStudent() {
        this.loginService.pingDomain()
            .subscribe((response) => {
                    Cache.set('ping', response);
                    if (Cache.get('ping')) {
                        this.ping = Cache.get('ping');
                        this.logo = Cache.get('ping')['settings']['logo'];
                    }
                },
                error => {
                    console.log('error from profile ::', error);
                });
    }

    public goBack(){
        window.history.back()
    }


    public flipRegistration(id: string) {
        if (id === 'signup') {
            const login = $('#m_login');
            login.removeClass('m-login--forget-password');
            login.removeClass('m-login--signin');

            login.addClass('m-login--signup');
            (<any>login.find('.m-login__signup')).animateClass('flipInX animated');
            return;
        } else {
            const login = $('#m_login');
            login.removeClass('m-login--forget-password');
            login.removeClass('m-login--signup');
            login.addClass('m-login--signin');
            (<any>login.find('.m-login__signin')).animateClass('flipInX animated');
        }
    }


    private getAppSettings() {
        this.schoolService.getAppSettings().subscribe((appSettingsResponse) => {
                Cache.set('app_settings', appSettingsResponse);
                this.appSettings = Cache.get('app_settings')['settings'];
            },
            (error) => {
                console.log('Could not load application settings ', error);
            });
    }


    public getApplicantProfile(id) {

        this.admissionService.getApplicantProfile(id).subscribe(
            (applicantProfileResponse) => {
                console.log('Applicant profile response from applicant dasstudent login component boaerd ', applicantProfileResponse);
                this.authenticatedUser = applicantProfileResponse['applicant'];
                this.authenticatedUser['programme_object'] = applicantProfileResponse['programme'];
                this.applicant.hasPaid = (this.authenticatedUser['payment_status'] === '1');

                this.router.navigateByUrl('/applicant');

            },
            (error) => {
                console.log('Could not load applicant profile response', error);
            }
        );
    }

    public handleSuccess($event){}
    public handleExpire(){}
    public handleLoad(){}

}
