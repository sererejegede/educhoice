import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ActivationEnd, Router} from "@angular/router";
import {NotificationService} from "../../services/notification.service";
import {SchoolService} from "../../services/school.service";
import {Cache} from "../../utils/cache";
import {AuthenticationService} from "../../services/authentication.service";
import {Validators, FormGroup, FormBuilder} from '@angular/forms';

@Component({
    selector: 'app-email-auth',
    templateUrl: './email-auth.component.html',
    styleUrls: ['./email-auth.component.css']
})
export class EmailAuthComponent implements OnInit {

    public passwordFeedBack = {
        message: "",
        process: "password",
        loader: true
    };

    public appSettings: any = {};
    public schoolName: string = '';
    public userName: string = '';
    public userType: string = '';
    public errorMessage: string = '';
    private resetCode: any;
    public resetCodeReference = null;
    public paychoiceObject = null;
    public paychoiceTransaction = null;
    public transactionFailed: boolean = false;
    public payChoiceDiv: boolean = false;
    public confirmationError: boolean = false;
    public loading: boolean = false;
    public codeError: boolean = false;
    public changePasswordForm: FormGroup;

    public messageStyle = {
        success: false,
        warning: false,
        error: false
    };

    static changePasswordForm = function () {

        return {
            password: ['', Validators.compose([Validators.required])],
            password_confirmation: ['', Validators.compose([Validators.required])]

        };

    };

    constructor(private schoolService: SchoolService,
                private activeRoute: ActivatedRoute,
                private authService: AuthenticationService,
                private router: Router,
                private fb: FormBuilder,
                private Alert: NotificationService) {
        this.changePasswordForm = this.fb.group(EmailAuthComponent.changePasswordForm());

        if (Cache.get('app_settings')) {
            this.appSettings = Cache.get('app_settings')['settings'];
            this.schoolName = Cache.get('app_settings')['name'];
        }
        else {
            this.getAppSettings();
        }
        this.initializePasswordFeedBack();

    }


    ngOnInit() {
    }


    public verifyReferenceCode(reset_code) {
        if (!reset_code) {
            this.passwordFeedBack.loader = false;
            this.router.navigateByUrl('/')
            return this.Alert.error('UNKNOWN REQUEST');
        }
        this.resetCode = reset_code;
        const verificationObject = {
            reset_code: this.resetCode
        }
        this.authService.verifyPasswordReset(verificationObject).subscribe(
            (passwordResponse) => {
                this.passwordFeedBack.loader = false;
                if (passwordResponse) {
                    this.setUserType(passwordResponse);
                }
            },
            (error) => {
                this.passwordFeedBack.loader = false;
                this.codeError = true;
                this.Alert.error('Could not authenticate ', error);
                const errors = JSON.parse(error['_body'])['errors'];
                for (let key in errors) {
                    this.errorMessage += errors[key] + '\n';
                }
            }
        )
    }

    private setUserType(passwordResponseData) {
        switch (passwordResponseData['user_type']) {
            case 'App\\Models\\School\\Applicant':
                this.userType = 'applicant';
                this.userName = passwordResponseData['user']['name'];
                Cache.set('bonafide_applicant', {val: 'yes'});
                break;
            case 'App\\Models\\School\\Student':
                this.userType = 'student';
                this.userName = passwordResponseData['username'];
                Cache.set('bonafide_applicant', null);
                break;
            case 'App\\Models\\School\\Auth\\Staff':
                this.userType = 'staff';
                this.userName = passwordResponseData['user']['fullname'];
                Cache.set('bonafide_applicant', null);
                break;
            case 'App\\User':
                this.userType = 'user';
                this.userName = `${passwordResponseData['user']['first_name']} ${passwordResponseData['user']['last_name']}`;
                Cache.set('bonafide_applicant', null);
                break;
            default:
                return this.Alert.error('Unknown User!!');
        }
    }

    private getAppSettings() {
        this.schoolService.getAppSettings().subscribe((appSettingsResponse) => {
                console.log('Application setings ', appSettingsResponse)
                Cache.set('app_settings', appSettingsResponse);
                this.appSettings = Cache.get('app_settings')['settings'];
                this.schoolName = Cache.get('app_settings')['name'];
                console.log('Application settings from admin hedr ', this.appSettings)
            },
            (error) => {
                console.log('Could not load application settings ', error);
            })
    }

    public initializePasswordFeedBack() {
        this.router.events.subscribe(val => {
            if (val instanceof ActivationEnd) {
                this.resetCodeReference = val.snapshot.queryParams['reset_code'];
                if (this.resetCodeReference) {
                    this.verifyReferenceCode(this.resetCodeReference);
                }
            }
        });

        this.paychoiceObject = this.activeRoute.snapshot.paramMap.get('paychoice');
    }

    public resetPassword() {
        this.loading = true;
        this.confirmationError = false;
        this.changePasswordForm.value['user_type'] = this.userType;
        this.changePasswordForm.value['reset_code'] = this.resetCode;
        if (this.changePasswordForm.value['password'] !== this.changePasswordForm.value['password_confirmation']) {
            this.loading = false;
            this.Alert.error('Mismatched Passwords!!');
            this.confirmationError = true;
            return;
        }
        this.authService.resetPassword(this.changePasswordForm.value).subscribe(
            (passwordChangeResponse) => {
                this.loading = false;
                this.Alert.success(`${passwordChangeResponse.message}\n Redirecting...`);
                (this.userType !== 'staff') ? this.router.navigateByUrl('/') : this.router.navigateByUrl('/staff')
                console.log('Password change response ', passwordChangeResponse);
            },
            (error) => {
                this.loading = false;
                this.Alert.error('Password change failed! Please retry.', error);
            }
        )
    }

    public goBackToOrigin() {
        // console.log(Cache.get('RETURN_URL'));
        // window.location.replace(Cache.get('RETURN_URL'));
        window.history.back();
    }

    protected setRouteParam(userType = 'student') {
        if (userType === 'staff') {
            this.router.navigate(['/forgot-password', {txhref: 'zsta4m'}]);
            return;
        }
        const userRouteParam = (userType === 'student') ? 'yst3l' : 'xap2k';
        this.router.navigate(['/forgot-password', {txhref: userRouteParam}]);
    }

    public setHeaderColor() {
        return {
            'background-color': this.appSettings['primary_color'],
            'color': 'white',
        }
    }

}
