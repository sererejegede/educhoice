/**
 * Created by Serere on 4/6/2018.
 */
export interface Result {
  student_course_registration_id: number,
  curriculum_course_id: number,
  student_registration_id: number,
  ca: number,
  exam: number,
  comment: String
}