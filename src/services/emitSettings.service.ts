import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class EmitSettingsService {
    private emitChangeSource = new Subject<any>();     // Observable string sources
    changeEmitted$ = this.emitChangeSource.asObservable(); // Observable string streams
    emitChange(change: any) {
        this.emitChangeSource.next(change);  // Service message commands
    }
}