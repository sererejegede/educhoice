import {Injectable} from "@angular/core";
import "rxjs/add/operator/map";
import {ApiHandlerService} from "./api-handler.service";
import {UserService} from "./user.service";
import {Http} from "@angular/http";
import {Cache} from "../utils/cache";


@Injectable()
export class AuthenticationService {

    public authenticationUrls = {
        login: "auth/login",
        register: "auth/register",
        logout: "auth/logout"
    }

    constructor(private http: Http,
                private apiHandler: ApiHandlerService,
                private userService: UserService) {
    }


    public login(data) {
        const path = this.authenticationUrls.login;
        return this.apiHandler.post(path, data);
        // console.log("User details ", data);
        //  this.http.post('/api/authenticate', JSON.stringify({ email: email, password: password }))
        //     .map((response: Response) => {
        //         // login successful if there's a jwt token in the response
        //         let user = response.json();
        //         if (user && user.token) {
        //             // store user details and jwt token in local storage to keep user logged in between page refreshes
        //             localStorage.setItem('currentUser', JSON.stringify(user));
        //         }
        //     });
    }

    public registerUser(data) {
        const path = this.authenticationUrls.register;
        return this.apiHandler.post(path, data);
    }

    public logout(action?: string) {
        const path = this.authenticationUrls.logout;
        if (action) {
            this.userService.logout(action);
            return this.apiHandler.post(path);
        }
        this.userService.logout();
        return this.apiHandler.post(path);
    }

    public changePassword(data) {
        const path = 'auth/password/change';
        return this.apiHandler.post(path, data);
    }

    public resetPassword(data) {
        const path = 'auth/password/reset';
        return this.apiHandler.post(path, data);
    }

    public verifyPasswordReset(data) {
        const path = 'auth/password/reset-confirm';
        return this.apiHandler.post(path, data);
    }

    public requestPasswordReset(data) {
        const path = 'auth/password/reset-request';
        return this.apiHandler.post(path, data);
    }

    /*Agent password reset*/

    public changeAgentPassword(data) {
        const path = 'auth/password/change';
        return this.apiHandler.post(path, data);
    }

    public resetAgentPassword(data) {
        const path = 'auth/password/reset';
        return this.apiHandler.post(path, data);
    }

    public verifyAgentPasswordReset(data) {
        const path = 'auth/password/reset-confirm';
        return this.apiHandler.post(path, data);
    }

    public requestAgentPasswordReset(data) {
        const path = 'auth/password/reset-request';
        return this.apiHandler.post(path, data);
    }


    public myModuleIsPermitted(moduleName): boolean {
        const userModules = (Cache.get('user_permissions')) ? Cache.get('user_permissions')['userModules'] : [];
        if (this.userService.getAuthUser()['login']['username'].toString() === 'system') {
            return true;
        }
        return (userModules.includes(moduleName));
    }

    public iAmPermitted(routeName): boolean {
        const userRoutes = (Cache.get('user_permissions')) ? Cache.get('user_permissions')['userRoutes'] : [];
        if (this.userService.getAuthUser()['login']['username'].toString() === 'system') {
            return true;
        }
        return (userRoutes.includes(routeName));

    }


}
