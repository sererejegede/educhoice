import {Injectable} from '@angular/core';
import {ApiHandlerService} from "./api-handler.service";
import {environment} from "../environments/environment";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AgentService {

    constructor(private apihandlerservice: ApiHandlerService) {
    }


    public visitorIsUpperlink() {
        const SPLIT_NAME = window.location.hostname.split(environment.DEFAULT_DOMAIN);
        return (SPLIT_NAME.length === 2 && SPLIT_NAME[0] === "");

    }


    public setHeaderColor(section) {
        let theme = {
            'background-color': ''
        }
        switch (section) {
            case 'nav':
                theme['background-color'] = '#0a1529';
                return {
                    'background-color': theme['background-color']
                };
            case 'thead':
                theme['background-color'] = '#d21502';
                return {
                    'background-color': theme['background-color']
                };
            default:
                theme['background-color'] = '';
                return {
                    'background-color': theme['background-color']
                }

        }

        // this.navHeaderColor = (this.isAgent['color'])?this.isAgent['color']['primary']:'#0a1529';
        // this.theadColor = (this.isAgent['color'])?this.isAgent['color']['secondary']:'#d21502';
        //
        // return {
        //     'background-color':this.navHeaderColor
        // }
    }

    public getMyAgents() {
        const url = 'admin/agent';
        return this.apihandlerservice.get(url);
    }

    /**
     * This method creates or updates an agent
     */
    public createOrUpdateAgent(data, agentLogo, id?: number):Observable<any> {
        let url = 'admin/agent';
        if (id) {
            url += `/${id}`;
            data['_method']='PUT';
        }
        return this.apihandlerservice.postFile(data, agentLogo, url, 'logo');

    }


    public getAllAgents() {
        return this.apihandlerservice.get('admin/agent');
    }

    public getAgentById(agentId) {
        return this.apihandlerservice.get(`admin/agent/${agentId}`);
    }

    public getSchoolById(schoolId) {
        return this.apihandlerservice.get(`admin/school/${schoolId}`);
    }

    public getSchoolByIdForUser(schoolId) {
        return this.apihandlerservice.get(`my_school/${schoolId}`);

    }

    public getAllAgentUsers() {
        return this.apihandlerservice.get('admin/user');
    }

    public getAgentUserById(agentUserId) {
        return this.apihandlerservice.get(`admin/user/${agentUserId}`);
    }

    public createOrUpdateAgentUser(data, id?: number) {
        let url = 'admin/user';
        if (id) {
            url += `/${id}`;
            return this.apihandlerservice.put(url, data);
        }
        return this.apihandlerservice.post(url, data);
    }

    public getAgentSettings() {
        return this.apihandlerservice.post(`auth/agent`);
    }

    public toggleSchool(action, schoolId) {
        const path = `my_school/${action}/${schoolId}`;
        return this.apihandlerservice.post(path);

    }

    public getAgentDashboardData(){
        return this.apihandlerservice.get('admin/dashboard/data');
    }


}
