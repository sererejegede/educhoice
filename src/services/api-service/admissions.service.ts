import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {ApiHandlerService} from '../api-handler.service';

@Injectable()


export class AdmissionsService {
  private re_query_url = environment.API_URL.reQueryURL.general;


  constructor(private apihandle: ApiHandlerService) {

    }


    public getAllJambRecords(data) {
        return this.apihandle.post('jamb_records', data);
    }


    public UpdateJambRecord(data, id) {
        const url = `jamb_records/update_record/${id}`;
        return this.apihandle.put(url, data);

    }

    public importJambRecord() {
        return this.apihandle.get('jamb_records/import');
    }


    public getAllOtherApplicants() {
        return this.apihandle.get('other_applicants');
    }


    public createOtherApplicant(data) {
        const url = `other_applicants/store`;
        return this.apihandle.post(url, data);

    }

    public createOtherProcessApplicant(data, formFile) {
        const url = `applicant/other_application_process/store`;
        // return this.apihandle.post(url, data);
      return this.apihandle.postFile(data, formFile, url, 'image');
    }




    public updateOtherApplicant(data, id) {
        const url = `other_applicants/update/${id}`;
        return this.apihandle.put(url, data);

    }


    public getAllPUTME() {
        return this.apihandle.get('putme_process');
    }


    public UpdatePUTME(data, id) {
        const url = `putme_process/update/${id}`;
        return this.apihandle.put(url, data);

    }

    public importPUTME() {

        return this.apihandle.get('putme_process/import');
    }

    public getPutmeCandidateById(id) {
        return this.getApplicantProfile(id);
        // return this.apihandle.get(`putme_process/show/${id}`);

    }

    public getPutmeTemplate() {
        const path = `download_template`;
        return this.apihandle.getFile(path);
    }

    public getAllAdmissions() {
        return this.apihandle.get('admission_list');
    }


    public UpdateAdmission(data, id) {
        const url = `admission_list/update/${id}`;
        return this.apihandle.put(url, data);

    }

    public importAdmission(formFile, data) {
        const path = `admission_list/import`;
        return this.apihandle.postFile(data, formFile, path, 'admission_list');
    }

    public getAdmissionListTemplate() {
        const path = `admission_list/download_template`;
        return this.apihandle.getFile(path);
    }



    public getAllAdmissionLetters() {
        return this.apihandle.getLetter(`admission_letter`);
    }


    public UpdateAdmissionLetter(data, id) {
        const url = `admission_constter/update/${id}`;
        return this.apihandle.put(url, data);

    }

    public createAdmissionLetter(data,admissionLetterId) {
        const path = (admissionLetterId===0)?`admission_letter/store`:`admission_letter/update/${admissionLetterId}`;
        return this.apihandle.post(path, data);
    }

    public getLgaByStateId(stateId) {
        return this.apihandle.get(`profile/state/${stateId}`);

    }


    public getAllAdmissionApplications() {
        return this.apihandle.get('application?paginate=no');
    }


    public UpdateAdmissionApplication(data, id) {
        const url = `application/update/${id}`;
        return this.apihandle.post(url, data);

    }

    public createAdmissionApplication(data) {
        return this.apihandle.post(`application/create`, data);
    }

    public applicantLogin(data) {
        return this.apihandle.post('auth/login', data);
    }

    public getApplicantUtmeInfo(data) {
        return this.apihandle.post(`applicant/utme_application_process`, data);
    }

    public getApplicantsForMigration() {
        return this.apihandle.get(`applicant_student_migration/admitted_applicants`);
    }

    public migrateApplicantToStudent(course_id, data) {
        return this.apihandle.post(`applicant_student_migration/move_applicants/${course_id}`, data);

    }

    public getAllActiveApplications() {
        return this.apihandle.get('active_applications');
    }

    public getAllActiveApplicantApplications() {
        return this.apihandle.get('applicant/active_applications');
    }


    public downloadJambRecordTemplate(idsObject) {
        const path = `jamb_records/import`;
        return this.apihandle.getFile(path);
    }

    public uploadJambRecord(formFile, data) {
        const path = `jamb_records/import`;
        return this.apihandle.postFile(data, formFile, path, 'csv');
    }

    public updateApplicantInfo(reg_no, data) {
        const path = `applicant/utme_application_process/update/${reg_no}`;
        return this.apihandle.post(path, data);
    }

    public createApplicantBio(formFile, data) {
        const path = 'applicant/utme_application_process/bio_data';
        return this.apihandle.postFile(data, formFile, path, 'image');
    }

    public getApplicantProfile(applicant_id) {
        const path = `applicant/applicant_profile/${applicant_id}`;
        return this.apihandle.post(path);
    }

    public forceApplicantPayment(applicant_id) {
        const path = `applicant/application_payment/forcePayment/${applicant_id}`;
        return this.apihandle.post(path);
    }

    public transactApplicantPayment(data, applicant_id) {
        const path = `applicant/application_payment/${applicant_id}`;
        return this.apihandle.post(path, data);
    }

    public getStudentPhoto(id) {
        const path = ``;
        return this.apihandle.getFile(path).map(
            (imgSrc) => {
                return this.getStudentImage(imgSrc);
            }
        );
    }

    private getStudentImage(imgSrc) {
        return this.apihandle.getImage(imgSrc);
    }

    public uploadPutmeRecord(formFile, data) {
        const path = `putme_process/import`;
        return this.apihandle.postFile(data, formFile, path, 'putme_csv');
    }

    public prepareApplicantForPayment(email) {
        const path = `applicant/utme_application_process/start_payment/${email}`;
        return this.apihandle.get(path);
    }

    public createAcceptanceFee(applicant_id, data) {
        const path = `applicant/application_payment/acceptance_fee/${applicant_id}`;
        return this.apihandle.post(path, data);
    }

    public createOrUpdateAcceptanceFee(data, id?) {
        let path = `acceptance_fee/create`;
        if (id) {
            path = `acceptance_fee/update/${id}`;
        }
        return this.apihandle.post(path, data);
    }

    public getAcceptanceFee() {
        const path = `acceptance_fee`;
        return this.apihandle.get(path);
    }


    public generateTransactionId(data) {
        const path = `applicant/utme_application_process/generate_transaction_id`;
        return this.apihandle.post(path, data);
    }

    public createApplicantSSCEResult(data, applicant_id) {
        const path = `applicant/applicant_profile/ssce_results/${applicant_id}`;
        return this.apihandle.post(path);
    }

    public showApplicantSSCEResult(applicant_id) {
        const path = `applicant/applicant_profile/list_applicant_ssce/${applicant_id}`;
        return this.apihandle.get(path);
    }

    public saveApplicationForm(data, applicant_id, applicationMode: boolean) {
        const suffix = (applicationMode) ? `?submit=1` : `?submit=0`;
        const path = `applicant/applicant_profile/submit_application_form/${applicant_id}${suffix}`;
        return this.apihandle.post(path, data);
    }

  public saveApplicationForm2(applicant_id, formFile) {
    const path = `applicant/applicant_profile/upload_image/${applicant_id}`;
    return this.apihandle.postFile({}, formFile, path, 'profile_image');
  }

    public getAllApllicantFaculties() {
        const path = `applicant/faculties`;
        return this.apihandle.get(path);
    }

    public getApplicationById(application_id) {
        const path = `applicant/get_open_application/${application_id}`;
        return this.apihandle.get(path);
    }

    public toggleApplicationStatus(status: boolean, id) {
        let path = (status) ? `application/open_application/${id}` : `application/close_application/${id}`;
        return this.apihandle.post(path);
    }

    public getAllProgrammesForApplicant() {
        const path = `applicant/get_programmes`;
        return this.apihandle.get(path);
    }

    public getAllAdmissionDeferments() {
        return this.apihandle.get('manage_deferment')
    }

    public toggleDefermentstatus(student_id, action = 'approve') {
        if (action !== 'approve') {
            return this.declineStudentDeferment(student_id);
        }
        return this.approveStudentDeferment(student_id);
    }

    public approveStudentDeferment(student_id) {
        return this.apihandle.post(`manage_deferment/approve/${student_id}`)
    }

    public declineStudentDeferment(student_id) {
        return this.apihandle.post(`manage_deferment/decline/${student_id}`)
    }

    public createAdmissionDeferments(data) {
        return this.apihandle.post('student/deferment/create', data);
    }

    public updateAdmissionDeferment(data, id) {
        return this.apihandle.put(`student/deferment/create/${id}`, data);

    }


    public applyForTransfer() {
        return this.apihandle.post('student/application/transfer');
    }

    public getAllStudentTransfers() {
        return this.apihandle.get(`student/application/transfer`);
    }

    public getAllApplicantScreeningCenters() {
        return this.apihandle.get(`applicant/utme_application_process/screening_center`);
    }

    public getAllOlevelSubjects() {
        return this.apihandle.get(`applicant/utme_application_process/olevel_subjects`);

    }

  public makeReQuery (transaction_id, confirmation_no?) {
    // const school_id = (Cache.get('ping')) ? Cache.get('ping')['id'] : Cache.get('app_settings')['id'];
    let path = `${this.re_query_url}/${transaction_id}`; // ?confirmation_no=${confirmation_no}
    if (confirmation_no) {
      path += `?confirmation_no=${confirmation_no}`;
    }
    return this.apihandle.get(path);
  }

  public unSubmitApplication(applicant_id) {
    return this.apihandle.get(`applicants/unsubmit-application-form/${applicant_id}`);
  }

}
