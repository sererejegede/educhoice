import {Injectable} from '@angular/core';
import {ApiHandlerService} from '../api-handler.service';
import {environment} from '../../environments/environment';
import {Subject} from "rxjs/Subject";
import {Cache} from "../../utils/cache";

@Injectable()
export class ReportServiceService {
  public  selectReport = new Subject();
  private report = environment.API_URL.report;
  private re_query_url = environment.API_URL.reQueryURL.general;
  private hostel_management = `hostel_management`;

  constructor(private apiService: ApiHandlerService) {
  }

  public departmentFinance(id) {
    return this.apiService.get(`${this.report}/transaction/byDepartment/${id}`);
  }

  public courseOfStudyFinance(id) {
    return this.apiService.get(`${this.report}/transaction/byCourseOfStudy/${id}`);
  }

  public courseRegistration (paginator, filter) {
    // http://ws.educhoice.ng/api/external_transaction/course_registration_report/bowen
    const sort_path = `report/course_registration`; // ?pages=${paginator.per_page}
    // for (const param in filter) {
    //  sort_path = `${sort_path}&${param}=${filter[param]}`;
    // }
    return this.apiService.post(`${sort_path}`, filter);
  }

  public courseRegistrationStatistics(data) {
    return this.apiService.post(`${this.report}/course_registration_statistics`, data);
  }

  // noinspection SpellCheckingInspection
  public onGetSession(filter, paginator) {
    let sort_path = `${this.report}/transaction/bySession/${filter.session_year}?pages=${paginator.per_page}`;
    for (const param in filter) {
      if (filter[param] && param !== 'session_year') {
        sort_path = `${sort_path}&${param}=${filter[param]}`;
      }
    }
    const path = (paginator.page_number) ? `${sort_path}&page=${paginator.page_number}` : `${sort_path}`;
    const search_path = (paginator.search) ? `${path}&search=${paginator.search}` : path;
    return this.apiService.get(search_path);
  }

  public sessionReportStats (filter) {
    let sort_path = `${this.report}/transaction/bySession/${filter.session_year}?paginate=no`;
    for (const param in filter) {
      if (filter[param] && param !== 'session_year') {
        sort_path = `${sort_path}&${param}=${filter[param]}`;
      }
    }
    return this.apiService.get(sort_path);
  }

  // noinspection SpellCheckingInspection
  public getHostel(config, paginator) {
    const sort_path = `${this.report}/hostel/${config.type}?pages=${paginator.per_page}`;
    const hostel_filter = (config.hostel_id) ? `${sort_path}&hostel_id=${config.hostel_id}` : sort_path;
    const gender_filter = (config.gender) ? `${hostel_filter}&gender=${config.gender}` : hostel_filter;
    const path = (paginator.page_number) ? `${gender_filter}&page=${paginator.page_number}` : gender_filter;
    const search_path = (paginator.search) ? `${path}&search=${paginator.search}` : path;
    return this.apiService.get(search_path);
  }

  public hostelReportStats (filter) {
    let sort_path = `${this.report}/hostel/get_allocations`;
    sort_path += (filter.hostel_id) ? `?hostel_id=${filter.hostel_id}` : '';
    if (filter.gender) {
      sort_path += (filter.hostel_id) ? `&gender=${filter.hostel_id}` : `?gender=${filter.hostel_id}`;
    }
    sort_path += (filter.gender) ? `?gender=${filter.hostel_id}` : '';
    // for (const param in filter) {
    //   if (filter[param] && param !== 'type' && param !== 'hostel_id') {
    //     sort_path += `&${param}=${filter[param]}`;
    //   }
    // }
    return this.apiService.get(sort_path);
  }

  public onGetFromDate(filter, paginator) {
    const sort_path = `${this.report}/transaction/byDateRange/${filter.from}/${filter.to}?level_id=${filter.level_id}&pages=${paginator.per_page}`;
    const path = (paginator.page_number) ? `${sort_path}&page=${paginator.page_number}` : sort_path;
    const search_path = (paginator.search) ? `${path}&search=${paginator.search}` : path;
    return this.apiService.get(search_path);
  }

  public onGetFromDateNoPaginate(filter) {
    const sort_path = `${this.report}/transaction/byDateRange/${filter.from}/${filter.to}?level_id=${filter.level_id}&paginate=no`;
    // const path = (paginator.page_number) ? `${sort_path}&page=${paginator.page_number}` : sort_path;
    // const search_path = (paginator.search) ? `${path}&search=${paginator.search}` : path;
    return this.apiService.get(sort_path);
  }

  public onGetPaymentMethod(filter, paginator) {
    const sort_path = `${this.report}/transaction/byMethod/${filter.payment_method}?session=${filter.session}&pages=${paginator.per_page}`;
    const path = (paginator.page_number) ? `${sort_path}&page=${paginator.page_number}` : sort_path;
    const search_path = (paginator.search) ? `${path}&search=${paginator.search}` : path;
    return this.apiService.get(search_path);
  }

  public onGetStudent(student_id) {
    return this.apiService.get(`${this.report}/transaction/byStudent/${student_id}`);
  }

  public makeReQuery (transaction_id) {
    // const school_id = (Cache.get('ping')) ? Cache.get('ping')['id'] : Cache.get('app_settings')['id'];
    const path = `${this.re_query_url}/${transaction_id}`; // ?transact_no=${transaction_id}
    return this.apiService.get(path);
  }

  public deleteAllocation (allocation_id) {
    return this.apiService.post(`${this.hostel_management}/delete_allocation/${allocation_id}`);
  }

  public createSpecialReport (data) {
    return this.apiService.post(`${this.report}/special_report`, data);
  }

  public listSpecialReports () {
    return this.apiService.get(`${this.report}/special_report`);
  }

  public getSpecialReport (report_id) {
    return this.apiService.get(`${this.report}/special_report/${report_id}`);
  }

  public downloadSpecialReport (report_id) {
    return this.apiService.getFile(`${this.report}/special_report/${report_id}/download`);
  }

  public byFeeItem(data) {
    return this.apiService.post(`result/payment/ByFeeItem`, data);
  }

  public studentStatistics(data) {
    return this.apiService.post(`student-statistics`, data);
  }
}
