import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {ApiHandlerService} from '../api-handler.service';
import {Observable} from "rxjs/Observable";

@Injectable()
export class AdministationServiceService {

    private permission = environment.API_URL.permission;

    constructor(private apiService: ApiHandlerService) {
    }

    /**
     * Load User's dashboard
     */

    public loadMyDashboard() {
        return this.apiService.get('administration/dashboard');
    }

    /**
     * getting all permission
     */
    getAllPermission() {
        return this.apiService.get(this.permission);
    }

    /**
     * creating permission
     */
    createPermission(data) {
        return this.apiService.post(this.permission, data);
    }

    updatePermission(id, value: any) {
        return this.apiService.put(`${this.permission}/${id}`, value);
    }


    public getAllTasks() {
        return this.apiService.get('administration/tasks?paginate=no');
    }

    public generateTasks() {
        return this.apiService.get('administration/tasks/generate?paginate=no');
    }

    public createOrUpdateTask(data, id?: string) {
        let url = 'administration/tasks';
        if (id) {
            url += `/${id}`;
            return this.apiService.put(url, data);
        }
        return this.apiService.post(url, data);
    }

    public getAllModules() {
        return this.apiService.get('administration/modules');
    }

    public createOrUpdateModule(data, id?: string) {
        let url = 'administration/modules';
        if (id) {
            url += `/${id}`;
            return this.apiService.put(url, data);
        }
        return this.apiService.post(url, data);
    }

    public getAllGroups() {
        return this.apiService.get('administration/groups');

    }

    public getGroupById(groupId: number): Observable<any> {
        return this.apiService.get(`administration/groups/${groupId}`);
    }

    public createOrUpdateGroup(data, id?: string) {
        let url = 'administration/groups';
        if (id) {
            url += `/${id}`;
            return this.apiService.put(url, data);
        }
        return this.apiService.post(url, data);
    }

    public assignTasksToGroup(group_id, data) {
        const path = `administration/permissions/${group_id}`;
        return this.apiService.put(path, data);
    }

    public toggleGroup(id) {
        return this.apiService.put(`admin/groups/${id}/toggle`);
    }

    public getAllPermissions() {
        return this.apiService.get('administration/permissions');
    }

    public createOrUpdatePermission(data, id?: string) {
        let url = 'administration/permissions';
        if (id) {
            url += `/${id}`;
            return this.apiService.put(url, data);
        }
        return this.apiService.post(url, data);
    }


    public getAllAuthorizations() {
        return this.apiService.get('administration/authorize');
    }


    public toggleAuthorizationStatus(authorization_id, action = 'approve', comment?: any) {
        if ((action !== 'approve') && comment) {
            return this.rejectAuthorization(authorization_id, comment);
        }
        return this.approveAuthorization(authorization_id);
    }


    public checkAuthorization(type) {
        return this.apiService.get(`administration/authorize/checked/${type}`);
    }

    public forwardAuthorization(id) {
        return this.apiService.put(`administration/authorize/${id}/forward`);
    }

    public approveAuthorization(id) {
        return this.apiService.put(`administration/authorize/${id}/approve`);
    }

    public rejectAuthorization(id, comment) {
        return this.apiService.put(`administration/authorize/${id}/reject`, comment);
    }

    public forwardManyAuthorizations(id) {
        return this.apiService.put(`administration/authorize/forward`);
    }


    public createOrUpdateAdminAuthorization(data, group_id?: string) {
        let url = `administration/authorizers`;
        if (group_id) {
            url += `/${group_id}`;
            return this.apiService.put(url, data);
        }
        return this.apiService.post(url, data);
    }

    public getAllAdminAuthorizations() {
        return this.apiService.get('administration/authorizers');
    }

    public getAllExeatRequests() {
        return this.apiService.get(`manage_exeat`);
    }

    public declineExeatRequest(exeat_id) {
        return this.apiService.post(`manage_exeat/decline/${exeat_id}`);
    }

    public createOrUpdateAuthorization(data, id?: string) {
        let url = 'administration/authorize';
        if (id) {
            url += `/${id}`;
            return this.apiService.put(url, data);
        }
        return this.apiService.post(url, data);
    }

    public approveExeatRequest(exeat_id) {
        return this.apiService.post(`manage_exeat/approve/${exeat_id}`);
    }

    public getAllAndEveryAuthorization() {
        return this.apiService.get(`admin/authorize/all`);
    }

    public setDean(data) {
        return this.apiService.post(`profile/manage_dean/create`, data);
    }

    public setHOD(data) {
        return this.apiService.post(`staff_management/assign_as_hod/${data.department_id}/${data.staff_id}/${data.status}`);
    }

    public setCourseAdviser(data) {
        return this.apiService.post(`profile/course/adviser`, data);
    }

}
