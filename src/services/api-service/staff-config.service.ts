import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {ApiHandlerService} from '../api-handler.service';

@Injectable()
export class StaffConfigService {
  private programme = environment.API_URL.programme;
  private schoolType = environment.API_URL.schoolType;
  private programmeType = environment.API_URL.programmeType;
  private degree = environment.API_URL.degree;
  private faculty = environment.API_URL.faculty;
  private department = environment.API_URL.department;
  private level = environment.API_URL.level;
  private course = environment.API_URL.course_of_study;
  private session = environment.API_URL.session;
  private active_hour = environment.API_URL.active_hour;
  private hostel = environment.API_URL.hostel;
  private room = environment.API_URL.room;
  private bedspace = environment.API_URL.bedspace;
  private hostel_session = environment.API_URL.hostel_session;
  private hostel_allocation = environment.API_URL.hostel_allocation;
  private applicant_report = environment.API_URL.applicant_report;
  private course_report = environment.API_URL.course_report;
  private settings = environment.API_URL.settings;
  private applicant_interview = environment.API_URL.applicant_interview;
  private zone = environment.API_URL.zone;
  private profile = environment.API_URL.profile;
  private matric_no = environment.API_URL.matric_no;
  private screening_center = environment.API_URL.screening_center;

  constructor(private apiService: ApiHandlerService) {
  }

  getCountry() {
    return this.apiService.get(`${this.profile}/country?paginate=no `);
  }

  getCountryById(id) {
    return this.apiService.get(`${this.profile}/country/${id}`);
  }

  getAllStaff() {
    return this.apiService.get(`${this.profile}/staff?paginate=no`);
  }

  addStaffToDepartment(department_id, data) {
    return this.apiService.post(`staff_management/department_staff/${department_id}/update`, data);
  }

  getStaffByDepartment(department_id) {
    return this.apiService.get(`staff_management/department_staff/${department_id}`);
    // return this.apiService.get(`${this.profile}/staff?paginate=no&department_id=${department_id}`);
  }

  assignLecturerToCourse(course_id, data) {
    return this.apiService.post(`course-manager/curriculum-course/${course_id}/lecturers/add`, data);
  }

  getLecturersByCurriculumCourse(course_id) {
    return this.apiService.get(`course-manager/curriculum-course/${course_id}/lecturers`);
  }

  public deleteLecturerCourse(lecturer_course_id) {
    return this.apiService.delete(`course-manager/curriculum-course/${lecturer_course_id}/lecturers`);
  }

  getDepartmentHODs(department_id) {
    return this.apiService.get(`staff_management/hod/${department_id}`);
  }

  getFacultyDeans(faculty_id) {
    return this.apiService.get(`profile/manage_dean?paginate=no&faculty_id=${faculty_id}`);
  }

  getStaffByFaculty(faculty_id) {
    return this.apiService.get(`${this.profile}/staff?paginate=no&faculty_id=${faculty_id}`);
  }

  getCourseAdvisers(course_of_study_id) {
    return this.apiService.get(`${this.profile}/course/adviser?paginate=no&course_of_study_id=${course_of_study_id}`);
  }

  postZone(data) {
    return this.apiService.post(`${this.zone}`, data);
  }

  postZoneStates(id, data) {
    return this.apiService.post(`${this.zone}/${id}/state`, data);
  }

  getZone() {
    return this.apiService.get(`${this.zone}`);
  }

  updateZone(id, data) {
    return this.apiService.put(`${this.zone}/${id}`, data);
  }

  after_interview(applicant_id, admission_status, course_of_study, data) {
    return this.apiService.post(`${this.applicant_interview}/after_interview/${applicant_id}/${admission_status}/${course_of_study}`, data);
  }

  get_all_scheduled_applicants() {
    return this.apiService.get(`${this.applicant_interview}/get_all_scheduled_applicants`);
  }

  get_all_interviewed_applicants() {
    return this.apiService.get(`${this.applicant_interview}/get_all_interviewed_applicants`);
  }

  getApplicantInterview() {
    return this.apiService.get(`${this.applicant_interview}/submitted_applications`);
  }

  postVerify(email) {
    return this.apiService.post(`${this.applicant_interview}/verify_interview/${email}`);
  }

  postInvite(date, time, venue, type, data) {
    return this.apiService.post(`${this.applicant_interview}/schedule_interviews/${date}/${time}/${venue}/${type}`, data);
  }

  getApplicantReports() {
    return this.apiService.get(`${this.applicant_report}/all_applicants`);
  }

  getAdmitteApplicants() {
    return this.apiService.get(`${this.applicant_report}/admitted_applicants`);
  }

  getPaidApplicants() {
    return this.apiService.get(`${this.applicant_report}/paid_applicants`);
  }

  getReportByProgrammeId(id) {
    return this.apiService.get(`${this.applicant_report}/${id}`);
  }

  getReportByDepartmentId(id) {
    return this.apiService.get(`${this.course_report}/${id}`);
  }

  getReportByCOSDepartmentId(id) {
    return this.apiService.get(`${this.course_report}/course_of_study/${id}`);
  }

  getAllocation() {
    return this.apiService.get(this.hostel_allocation);
  }

  createHostelAllocation(id, data) {
    return this.apiService.post(`${this.hostel_allocation}/create`, data);
  }

  updateHostelAllocation(id, data) {
    return this.apiService.post(`${this.hostel_allocation}/update/${id}`, data);
  }

  getAllHostelSession() {
    return this.apiService.get(this.hostel_session);
  }

  createAllHostelSession(data) {
    return this.apiService.post(`${this.hostel_session}/create`, data);
  }

  updateAllHostelSession(id, value: any) {
    return this.apiService.post(`${this.hostel_session}/update/${id}`, value);
  }

  deleteHostelSession(id) {
    return this.apiService.delete(`${this.hostel_session}/delete/${id}`);
  }

  getReservations() {
    return this.apiService.get(`${this.hostel}/all_reservations`);
  }

  getPaymentsHistory() {
    return this.apiService.get(`${this.hostel}/payment_history`);
  }

  /**
   * getting all hostel quick patch
   */

  getAllHostel1() {
    const path = `${this.hostel}`;
    return this.apiService.get(path);
  }

  /**
   * getting all hostel
   */
  getAllHostel() {
    const path = `${this.hostel}/get_available_hostel`;
    return this.apiService.get(path);
  }

  /**
   * getting hostel by id
   */
  getHostel(id) {
    return this.apiService.get(`${this.hostel}/show/${id}`);
  }

  /**
   * getting hostel by id
   */
  public newGetHostel(id) {
    return this.apiService.get(`${this.hostel}/get_available_hostel?hostel_id=${id}`);
  }

  /**
   * creating hostel
   */
  createAllHostel(data) {
    return this.apiService.post(`${this.hostel}/create`, data);
  }

  updateAllHostel(id, value: any) {
    return this.apiService.post(`${this.hostel}/update/${id}`, value);
  }

  deleteHostel(id) {
    return this.apiService.delete(`${this.hostel}/delete/${id}`);
  }

  getRoomById(id) {
    return this.apiService.get(`${this.room}/show/${id}`);
  }

  createAllRoom(data) {
    return this.apiService.post(`${this.room}/create`, data);
  }

  updateAllRoom(id, value: any) {
    return this.apiService.post(`${this.room}/update/${id}`, value);
  }

  deleteRoom(id) {
    return this.apiService.delete(`${this.room}/delete/${id}`);
  }

  getAllBedspace() {
    return this.apiService.get(this.bedspace);
  }

  getBedspace(id) {
    return this.apiService.get(`${this.bedspace}/${id}`);
  }

  createAllBedspace(data) {
    return this.apiService.post(`${this.bedspace}/create`, data);
  }

  updateAllBedspace(id, value: any) {
    return this.apiService.post(`${this.bedspace}/update/${id}`, value);
  }

  deleteBedspace(id) {
    return this.apiService.delete(`${this.bedspace}/delete/${id}`);
  }

  public makeReservation(data) {
    return this.apiService.post(`${this.hostel}/make_reservation`, data);
  }

  /**
   * getting all active_hour
   */
  getAllActiveHour() {
    return this.apiService.get(this.active_hour);
  }

  getAllCgpaConfigurations() {
    return this.apiService.get('cgpa');
  }

  getAllProbationLimitConfigurations() {
    return this.apiService.get('probation-limit');
  }

  createCgpaConfiguration(data) {
    return this.apiService.post('cgpa', data);
  }

  createProbationLimit(data) {
    return this.apiService.post('probation-limit', data);
  }

  updateProbationLimit(id, value: any) {
    return this.apiService.put(`probation-limit/${id}`, value);
  }

  /**
   * creating ActiveHour
   */
  createActiveHour(data) {
    return this.apiService.post(this.active_hour, data);
  }

  updateActiveHour(id, value: any) {
    return this.apiService.put(`${this.active_hour}/${id}`, value);
  }

  /**
   * getting all degree
   */
  getAllDegree() {
    return this.apiService.get(this.degree);
  }

  /**
   * creating degree
   */
  createDegree(data) {
    return this.apiService.post(this.degree, data);
  }

  updateDegree(id, value: any) {
    return this.apiService.put(`${this.degree}/${id}`, value);
  }

  createProgramme(value: any) {
    return this.apiService.post(this.programme, value);
  }

  updateProgramme(id, value: any) {
    return this.apiService.put(`${this.programme}/${id}`, value);
  }

  getAllProgrammeById(id) {
    return this.apiService.get(`${this.programme}/${id}`);
  }

  getAllProgramme() {
    return this.apiService.get(this.programme);
  }

  getCurrentSemester(programme) {
    return this.apiService.get(`${this.programme}/${programme}/semester/current`);
  }

  getCurrentSession(programme) {
    return this.apiService.get(`${this.programme}/${programme}/session/current`);
  }

  getSession(programme) {
    return this.apiService.get(`${this.programme}/${programme}/session`);
  }


  /**
   * getting all degree
   */
  getAllSchoolType() {
    return this.apiService.get(this.schoolType);
  }

  createSchoolType(value: any) {
    return this.apiService.post(this.schoolType, value);
  }

  updateSchoolType(id, value: any) {
    return this.apiService.put(`${this.schoolType}/${id}`, value);
  }


  getAllProgrammeType() {
    return this.apiService.get(this.programmeType);
  }

  createProgrammeType(value: any) {
    return this.apiService.post(this.programmeType, value);
  }

  updateProgrammeType(id, value: any) {
    return this.apiService.put(`${this.programmeType}/${id}`, value);
  }


  /**
   * getting all Faculty
   */
  getAllFaculty() {
    return this.apiService.get(this.faculty);
  }

  /**
   * getting a particular faculty
   */
  getFaculty(id) {
    return this.apiService.get(`${this.faculty}/${id}`);
  }

  /**
   * getting a particular department
   */
  getDepartment(id) {
    return this.apiService.get(`department/${id}`);
  }

  // Get Course By Department, Level and Semester
  getCourseByDepartment(department_id, level_id, semester) {
    return this.apiService.get(`course-manager/course/${department_id}/${level_id}/${semester}`);
  }

  /**
   * getting a particular programme
   */
  getProgramme(id) {
    return this.apiService.get(`${this.programme}/${id}`);
  }

  createFaculty(value: any) {
    return this.apiService.post(this.faculty, value);
  }

  updateFaculty(id, value: any) {
    return this.apiService.put(`${this.faculty}/${id}`, value);
  }


  /**
   * getting the list of schooltype to dropdown option
   */
  getSchoolTypeList() {
    return this.apiService.get(`${this.schoolType}?paginate = no`);
  }

  /**
   * getting the list of session to dropdown option
   */
  getSessionList() {
    return this.apiService.get(`${this.session}?paginate=no`);
  }

  /**
   * getting the list of faculty to dropdown option
   */
  getFacultyList() {
    return this.apiService.get(`${this.faculty}?paginate = no`);
  }

  /**
   * getting the list of Programme to dropdown option
   */
  getProgrammeList() {
    return this.apiService.get(`${this.programme}?paginate = no`);
  }

  /**
   * getting the list of department to dropdown option
   */
  getDepartmentList() {
    return this.apiService.get(`${this.department}?paginate=no`);
  }

  /**
   * getting the list of course to dropdown option
   */
  getCourseList() {
    return this.apiService.get(`${this.course}?paginate=no`);
  }

  /**
   * getting the list of degree to dropdown option
   */
  getDegreeList() {
    return this.apiService.get(`${this.degree}?paginate = no`);
  }

  /**
   * getting the list of level to dropdown option
   */
  getLevelList() {
    return this.apiService.get(`${this.level}?paginate=no`);
  }

  /**
   * getting all Faculty
   */
  getAllDepartment() {
    return this.apiService.get(this.department);
  }

  createDepartment(value: any) {
    return this.apiService.post(this.department, value);
  }

  updateDepartment(id, value: any) {
    return this.apiService.put(`${this.department}/${id}`, value);
  }


  /**
   * getting all level
   */
  getAllLevel() {
    return this.apiService.get(this.level);
  }

  createLevel(value: any) {
    return this.apiService.post(this.level, value);
  }

  updateLevel(id, value: any) {
    return this.apiService.put(`${this.level}/${id}`, value);
  }

  /**
   * getting all Course
   */
  getAllCourse() {
    return this.apiService.get(this.course);
  }

  /**
   * getting all Course
   */
  getCourseOfStudyById(department_id) {
    return this.apiService.get(`${this.department}/${department_id}`);
  }

  getSingleSession(session_id) {
    return this.apiService.get(`session/${session_id}`);
  }

  getSingleCourseOfStudy(id) {
    return this.apiService.get(`course-of-study/${id}`);
  }

  createCourse(value: any) {
    return this.apiService.post(this.course, value);
  }

  updateCourse(id, value: any) {
    return this.apiService.put(`${this.course}/${id}`, value);
  }

  getCoursesByDepartment(department_id) {
    return this.apiService.get(`course_report/${department_id}`);
  }

  /**
   * getting all currentSession
   */
  getAllCurrentSession() {
    return this.apiService.get(`${this.session}/current`);
  }

  /**
   * getting all Session
   */
  getAllSession() {
    return this.apiService.get(this.session);
  }

  public getSessionById(sessionId) {
    return this.apiService.get(`session/${sessionId}`);
  }


  public startSessionSemester(sessionId, data) {
    return this.apiService.post(`session/${sessionId}/semester/start`, data);
  }

  public updateSessionSemester(sessionId, data) {
    return this.apiService.put(`session/${sessionId}/semester/start`, data);
  }

  /**
   * starting a Session
   */
  startSession(sessionId) {
    return this.apiService.get(`${this.session}/${sessionId}/semester/start`);
  }

  createSession(value: any) {
    return this.apiService.post(this.session, value);
  }

  updateSession(id, value: any) {
    return this.apiService.put(`${this.session}/${id}`, value);
  }

  public getGraduatingList(data) {
    return this.apiService.post(`profile/student/graduating/${data.session_id}/${data.semester}`, data);
  }

  getCurriculumCourses(course_of_study_id, level_id, session_id, semester) {
    return this.apiService.get(`course-manager/curriculum/${course_of_study_id}/${level_id}/${session_id}/${semester}`);
  }

  downloadTemplate(curriculum_course_id) {
    return this.apiService.get2(`course-manager/result/template/${curriculum_course_id}`, {"Accept": "application/vnd.ms-excel"});
  }

  /**
   * Create a settings
   */
  /**
   * Create a settings
   */

  createSettings(data) {
    return this.apiService.post(`${this.settings}`, data);
  }

  /**
   * Posting multiple files
   */
  createSettingsFile(settingsObject) {
    return this.apiService.postFile(settingsObject.data, settingsObject.formFile, `${this.settings}`, settingsObject.name);
  }

  getAllSettings() {
    return this.apiService.get(this.settings);
  }

  /**
   * Matric Number Generation
   * */
  createMatricNumber(data) {
    return this.apiService.post(`${this.matric_no}`, data)
  }

  listMatricNumberFormats() {
    return this.apiService.get(this.matric_no);
  }

  /**
   * Screening centre
   * */
  createScreeningCentre(data) {
    return this.apiService.post(`${this.screening_center}/create`, data);
  }

  updateScreeningCentre(data, id) {
    return this.apiService.post(`${this.screening_center}/update/${id}`, data);
  }

  getAllScreeningCenters() {
    return this.apiService.get(`${this.screening_center}`);
  }

  public getAllManualPayments() {
    return this.apiService.get(`payment/manual-payment?paginate=no`);
  }

  public updateManualPaymentStatus(status, id) {
    return this.apiService.put(`payment/manual-payment/${id}/status/${status}`);
  }

  public getManualPayment(payment_id) {
    return this.apiService.get(`payment/manual-payment/${payment_id}`);
  }

  public downloadMatricUploadTemplate() {
    // const path = `${environment.API_URL.course_manager}/result/template/${idsObject['cc_id']}/${idsObject['level_id']}/${idsObject['session_id']}/${idsObject['semester']}`;
    return this.apiService.getFile(`matric_number_import_template`);
  }

  public uploadMatricTempate(formFile) {
    return this.apiService.postFile({}, formFile, `import_student_matric_number`, 'students_list');
  }

  public downloadDebtorsListTemplate() {
    return this.apiService.getFile(`download-debtor-list-template`);
  }

  public uploadDebtorsList(formFile) {
    return this.apiService.postFile({}, formFile, `upload-debtors-list`, 'debtors');
  }

  public getAllDebtors() {
    return this.apiService.get(`all-debtors`);
  }

  public removeDebtor(debtor_id) {
    return this.apiService.delete(`remove-debtor-list/${debtor_id}`);
  }
}
