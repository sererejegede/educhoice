import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {ApiHandlerService} from '../api-handler.service';

@Injectable()
export class CourseManagementServiceService {

  private course = `${environment.API_URL.course}`;
  private curriculum = environment.API_URL.curriculum;
  private curriculum_course = environment.API_URL.curriculum_course;
  public id: number;

  constructor(private apiService: ApiHandlerService) {
  }

  /**
   * getting all course
   */
  getAllCourse(paginator) {
    const path = (paginator.page_number) ? `${this.course}?pages=${paginator.per_page}&page=${paginator.page_number}` : `${this.course}?pages=${paginator.per_page}`;
    const search_path = (paginator.search) ? `${path}&search=${paginator.search}` : path;
    return this.apiService.get(search_path);
    // return this.apiService.get(this.course);
  }

  getSingleCourse(course_id) {
    return this.apiService.get(`course-manager/curriculum-course/${course_id}`)
  }

  /**
   * getting the list of course to dropdown option
   */
  getCourseList() {
    return this.apiService.get(`${this.course}?paginate = no`);
  }

  /**
   * getting the curriculum by id
   */
  getCurriculumById(id) {
    return this.apiService.get(`${this.curriculum}/${id}`);
  }

  /**
   * getting the list of curriculum to dropdown option
   */
  getCurriculumList() {
    return this.apiService.get(`${this.curriculum}?paginate = no`);
  }

  /**
   * creating course
   */
  createCourse(data) {
    return this.apiService.post(this.course, data);
  }

  /**
   * updating course
   * @param id
   * @param value
   * @returns {Observable<any>}
   */
  updateCourse(id, value: any) {
    return this.apiService.put(`${this.course}/${id}`, value);
  }

  public getCourseSubstitutions() {
    return this.apiService.get('course-manager/course_substitution');
  }

  public createCourseSubstitution(data) {
    return this.apiService.post('course-manager/course_substitution/create', data);
  }

  public updateCourseSubstitution(data, sub_id) {
    return this.apiService.post(`course-manager/course_substitution/update/${sub_id}`, data);
  }

  public deleteCourseSubstitution(sub_id) {
    return this.apiService.delete(`course-manager/course_substitution/delete/${sub_id}`);
  }


  /**
   * getting all course
   */
  getAllCurriculum(paginator) {
    const sort_path = `${this.curriculum}?pages=${paginator.per_page}`;
    const path = (paginator.page_number) ? `${sort_path}&page=${paginator.page_number}` : sort_path;
    const search_path = (paginator.search) ? `${path}&search=${paginator.search}` : path;
    return this.apiService.get(search_path);
  }

  /**
   * creating course
   */
  createCurriculum(data) {
    return this.apiService.post(this.curriculum, data);
  }

  /**
   * updating course
   * @param id
   * @param value
   * @returns {Observable<any>}
   */
  updateCurriculum(id, value: any) {
    return this.apiService.put(`${this.curriculum}/${id}`, value);
  }

  /**
   * getting all curriculum_course
   */
  getAllCurriculumcourse() {
    return this.apiService.get(this.curriculum_course);
  }

  /**
   * creating Curriculum_course
   */
  createCurriculumcourse(data) {
    return this.apiService.post(this.curriculum_course, data);
  }

  /**
   * updating course
   * @param id
   * @param value
   * @returns {Observable<any>}
   */
  updateCurriculumcourse(id, value: any) {
    return this.apiService.put(`${this.curriculum_course}/${id}`, value);
  }

  public deleteCurriculumCourse (course_id) {
    return this.apiService.delete(`${this.curriculum_course}/${course_id}`);
  }

  /**
   * get course curriculum by id
   * @param {number} id
   */
  getAllCurriculumcoursebyId(id: number) {
    return this.apiService.get(`${this.curriculum}/${id}`);
  }
}
