import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {ApiHandlerService} from "../api-handler.service";
import {Observable} from "rxjs/Observable";
import {Cache} from "../../utils/cache";

@Injectable()
export class StudentServiceService {
  private student = environment.API_URL.student;
  private student_course_reg = `${this.student}/course_registration`;
  private re_query_url = environment.API_URL.reQueryURL.general;

  constructor(private apiService: ApiHandlerService) {}

  /*************  student profile *****************/


  getStudentProfile(): Observable<any> {
    return this.apiService.get(this.student);
  }

  updateStudentProfile(value: any) {
    return this.apiService.post(`${this.student}/update`, value);
  }

  public setProfileImage (formFile) {
    return this.apiService.postFile({}, formFile, `${this.student}/update_passport`, 'passport');
  }

  public getStudentDashboard() {
    return this.apiService.get('student/dashboard');
  }

  public checkForAdmissionLetter() {
    return this.apiService.get('student/admission-letter');
  }


  /*************  student *****************/
  getPaidFee(student_id) {
    return this.apiService.get(`${this.student}/fee/paid_fee/${student_id}`);
  }

  getStudentFeeByLevelSession(registration) {
    let path = `${this.student}/fee/student_fee`;
    path += (registration.level_id && registration.session_id) ? `/${registration.level_id}/${registration.session_id}` : ``;
    return this.apiService.get(path);
  }

  getStudentFee() {
    return this.apiService.get(`${this.student}/fee/student_fee`);
  }

  public checkPartPayment (data) {
    return this.apiService.get(`${this.student}/fee/check_part_payment/${data.session_id}/${data.semester_id}/${data.type}`);
  }

  getStudentCourseList() {
    return this.apiService.get(`${this.student}/course_registration`);
  }

  getAvailableHostels() {
    return this.apiService.get(`${this.student}/hostel_booking/available_hostels`);
  }

  onbookHostel(id, data) {
    return this.apiService.post(`${this.student}/transactions/pay_hostel`, data);
  }

  getForceReg(session, level) {
    return this.apiService.post(`${this.student_course_reg}/force_student_registration/${session}/${level}`);
  }

  makePayment(data) {
    return this.apiService.post(`${this.student}/transactions/pay_school`, data);
  }

  getStudentCourses(student_reg_id, semester) {
    return this.apiService.get(`${this.student_course_reg}/list_courses/${student_reg_id}/${semester}`);
    // return this.apiService.get(`profile/student/course_reg/${student_reg_id}/${semester}`);
  }

  public verifyRegistration (obj) {
    const path = `${this.student_course_reg}/check_student_course_registration`;
    return this.apiService.get(`${path}/${obj.reg_id}/${obj.session}/${obj.semester}/${obj.level}`);
  }

  public checkException (data) {
    return this.apiService.post(`${this.student}/check/exception`, data);
  }
  // check_student_course_registration/{studentRegistration}/{session_year}/{semester}/{level}

  viewStudentCourses(student_reg_id, level_id, semester) {
    return this.apiService.get(`${this.student_course_reg}/view_courses/${student_reg_id}/${level_id}/${semester}`);
  }

  postRegisterCourses(data) {
    const path = `${this.student_course_reg}/register_courses/${data.id}/${data.session}/${data.semester}/${data.level}`;
    return this.apiService.post(path, {courses: data.courses});
  }

  applyExtraUnit(student_reg_id, unit, semester) {
    return this.apiService.post(`${this.student_course_reg}/request_extra_course_unit/${student_reg_id}/${unit} / ${semester}`);
  }

  getReservations() {
    return this.apiService.get(`${this.student}/hostel_booking/my-reservations`
    );
  }

  getSessionAndSemesterResult(session = null, semester = null) {
    session = (session) ? `/${session}` : '';
    semester = (semester) ? `/${semester}` : '';
    return this.apiService.get(`${this.student}/result${session}${semester}`);
  }

  public getStudentResults(student_id) {
    return this.apiService.get(`course-manager/result/student/${student_id}`);
  }

  /**
   * Student Exeats
   * */

  exeatRequest(obj) {
    return this.apiService.post(`${this.student}/exeat/create`, obj);
  }

  getAllExeatRequests(student_id) {
    return this.apiService.get(`${this.student}/exeat/list_exeats/${student_id}`);
  }

  updateExeatRequest(exeat_id, update) {
    return this.apiService.post(`${this.student}/exeat/update/${exeat_id}`, update);
  }

  /**
   * Transfer
   * */

  transferRequest(data) {
    return this.apiService.post(`${this.student}/application/transfer`, data);
  }

  payTransferFee(data) {
    return this.apiService.post(`${this.student}/transactions/pay_transfer`, data);
  }

  getDepartmentTransferFee(transaction_id) {
    return this.apiService.get(`${this.student}/fee/transfer/${transaction_id}`);
  }

  getAllTransferRequests() {
    return this.apiService.get(`${this.student}/application/transfer`);
  }

  /***
   * Transcript
   */
  transcriptRequest(data) {
    return this.apiService.post(`${this.student}/transcript/apply`, data);
  }

  payTranscriptFee(data) {
    return this.apiService.post(`${this.student}/transactions/pay_transcript/${data.student_transcript_application_id}`, data);
  }

  getDepartmentTranscriptFee(transaction_id) {
    return this.apiService.get(`${this.student}/fee/transcript`);
  }

  getAllTranscriptApplications(student_id) {
    return this.apiService.get(`${this.student}/transcript/${student_id}`);
  }

  /**
   * Student Exeats
   * */

  defermentRequest(data) {
    return this.apiService.post(`${this.student}/deferment/create`, data)
  }

  updateDefermentRequest(deferment_id, update) {
    return this.apiService.post(`${this.student}/deferment/update/${deferment_id}`, update);
  }

  getAllDefermentRequests(student_id) {
    return this.apiService.get(`${this.student}/deferment/student_deferments`);
  }

  /**
   * getting all Faculty
   * api/v1/student/fee/payment_history
   */
  getAllFaculty() {
    return this.apiService.get(`${this.student}/faculties`);
  }

  getCourseOfStudyByDepartmentId(department_id) {
    return this.apiService.get(`${this.student}/course_of_studies/${department_id}`)
  }

  getSingleCourseOfStudyBy(id) {
    return this.apiService.get(`course-of-studies/${id}`)
  }

  /**
   * Payment History
   * api/v1/student/fee/payment_history
   */
  getPaymentHistory(status) {
    let path = `${this.student}/fee/payment_history`;
    path = status ? path + `?status=${status}` : path;
    return this.apiService.get(path);
  }

  public makeReQuery (transaction_id, confirmation_no?) {
    // const school_id = (Cache.get('ping')) ? Cache.get('ping')['id'] : Cache.get('app_settings')['id'];
    let path = `${this.re_query_url}/${transaction_id}`; // ?confirmation_no=${confirmation_no}
    if (confirmation_no) {
      path += `?confirmation_no=${confirmation_no}`;
    }
    return this.apiService.get(path);
  }

  public generateOneTimePass(userDetails): Observable<any> {
    const path = `administration/passcode/generate`;
    return this.apiService.post(path, userDetails);
  }

  public checkCourseForm (data) {
    return this.apiService.get(`${this.student_course_reg}/check_clearance_card/${data.reg_id}/${data.semester}`);
  }

  public getSingleSession(session_id) {
    return this.apiService.get(`session/${session_id}`);
  }

  public resetPassword (matric_no) {
    return this.apiService.post(`profile/${this.student}/reset_password`, {matric_no: matric_no});
  }

  public staffListStudentCourses (student_reg_id, semester) {
    return this.apiService.get(`profile/student/course_reg/${student_reg_id}/${semester}`);
  }

  public staffRegisterCourses (student_reg_id, semester, courses) {
    return this.apiService.post(`profile/student/course_reg/${student_reg_id}/${semester}`, {courses: courses});
  }

  public checkAndDownloadOathForm(){
      return this.apiService.get(`student/download-oath-form`);

  }
}
