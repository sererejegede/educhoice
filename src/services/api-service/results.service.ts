import {Injectable} from '@angular/core';
import {ApiHandlerService} from '../api-handler.service';
import {environment} from "../../environments/environment";

@Injectable()

export class ResultsService {

    constructor(private apiHandlerService: ApiHandlerService) {

    }


    public getAllStudentResults() {
        return this.apiHandlerService.get(`${environment.API_URL.course_manager}/result`);
    }

    getStudentResultByCourseOfStudy(course_of_study, level_id, session_id, semester) {
        return this.apiHandlerService.get(`${environment.API_URL.course_manager}/result/${course_of_study}/${level_id}/${session_id}/${semester}`);
    }

    public createOrUpdateStudentResult(data, id?) {
        let url = `${environment.API_URL.course_manager}/result`;
        if (id) {
            url += `/${id}`;
            return this.apiHandlerService.put(url, data);
        }
        return this.apiHandlerService.post(url, data);
    }

    public allstudentRegistrations() {
        return this.apiHandlerService.get('student/course_registration');

    }

    public updateStudentResult(id, data) {
        return this.apiHandlerService.put(`${environment.API_URL.course_manager}/result/${id}`, data);
    }

    public downloadResultTemplate(curriculum_course_id) {
        // const path = `${environment.API_URL.course_manager}/result/template/${idsObject['cc_id']}/${idsObject['level_id']}/${idsObject['session_id']}/${idsObject['semester']}`;
        const path = `${environment.API_URL.course_manager}/result/template/${curriculum_course_id}`;
        return this.apiHandlerService.getFile(path);
    }

    public uploadResultTempate(formFile, curriculum_course_id) {
        // const path = `${environment.API_URL.course_manager}/result/template/${idsObject['cc_id']}`;
        const path = `${environment.API_URL.course_manager}/result/template/${curriculum_course_id}`;
        return this.apiHandlerService.postFile({}, formFile, path, 'result');
    }

    public getAllCurriculumCourses() {
        const path = `${environment.API_URL.course_manager}/curriculum-course`;
        return this.apiHandlerService.get(path);
    }

    getResultByCurriculumCourse(id) {
        return this.apiHandlerService.get(`${environment.API_URL.course_manager}/result/curriculum-course/${id}`);
    }

    inputMultipleResult(curriculum_course_id, data) {
        return this.apiHandlerService.post(`${environment.API_URL.course_manager}/result/multiple/${curriculum_course_id}`, data);
    }

    /**
     *Result maker and checker
     **/
    // Singular
    hodApproval(result_id, status) {
        return this.apiHandlerService.put(`${environment.API_URL.course_manager}/result/${result_id}/hod_approval/${status}`);
    }

    deanApproval(result_id, status) {
        return this.apiHandlerService.put(`${environment.API_URL.course_manager}/result/${result_id}/dean_approval/${status}`);
    }

    senateApproval(result_id, status) {
        return this.apiHandlerService.put(`${environment.API_URL.course_manager}/result/${result_id}/seneate_approval/${status}`);
    }

    publishResult(result_id, status) {
        return this.apiHandlerService.put(`${environment.API_URL.course_manager}/result/${result_id}/publish_result/${status}`);
    }

    // Multiple
    hodMultipleApproval(curriculum_course_id, status, data) {
        return this.apiHandlerService.post(`${environment.API_URL.course_manager}/result/${curriculum_course_id}/hod_multiple_approval/${status}`, data);
    }

    // ${environment.API_URL.course_manager}/result/{curriculum_course}/hod_multiple_approval/{status?}
    deanMultipleApproval(curriculum_course_id, status, data) {
        return this.apiHandlerService.post(`${environment.API_URL.course_manager}/result/${curriculum_course_id}/dean_multiple_approval/${status}`, data);
    }

    senateMultipleApproval(curriculum_course_id, status, data) {
        return this.apiHandlerService.post(`${environment.API_URL.course_manager}/result/${curriculum_course_id}/seneate_multiple_approval/${status}`, data);
    }

    publishMultipleResult(curriculum_course_id, data, publish = true) {
        if (!publish) {
            data['action'] = 'unpublish';
        }

        return this.apiHandlerService.post(`${environment.API_URL.course_manager}/result/${curriculum_course_id}/publish_multiple_result`, data);
    }

    public getResultForParentView(data) {
        const path = `parent/check-result/bowen`;
        return this.apiHandlerService.result_check(path, data);
    }

    public createTranscriptFee(data) {
        return this.apiHandlerService.post('payment/transcript-fee', data);
    }

    /**
     * Since it is bad UX to filter down to Curriculum Courses, we'll just
     * with this API, get the list of courses for the staff
     * @returns {Observable<any>}
     */
    public getStaffCourses(session_id, semester) {
        return this.apiHandlerService.get(`profile/staff/my_courses/${session_id}/${semester}`);
    }

    /**
     * Senate List of Result
     * @param data, course_of_study_id, level_id, session_id, semester
     * @returns {Observable<any>}
     */
    public getFilteredResult(data) {
        if (data.course_of_study_id) {
            return this.apiHandlerService.get(`${environment.API_URL.course_manager}/result/course-of-study/${data.course_of_study_id}/${data.level_id}/${data.session_id}/${data.semester}`);
        }
        // return this.apiHandlerService.get(`${environment.API_URL.course_manager}/result/course-of-study/34/2/18/1`);
        // return this.apiService.get(`${environment.API_URL.course_manager}/result/course-of-study/15/2/17/1`);
    }

    public getMattersArising(data) {
        if (data) {
            return this.apiHandlerService.get(`${environment.API_URL.course_manager}/result/other_matters/${data.course_of_study_id}/${data.session_id}/${data.semester}`);
        }
        return this.apiHandlerService.get(`${environment.API_URL.course_manager}/result/other_matters/34/18/1`)
    }

    public senateApprove(data) {
        return this.apiHandlerService.post(`${environment.API_URL.course_manager}/result/senate_approve_filtered_results`, data);
    }

    public publishAll(data, unpublish: boolean) {
        if (unpublish) {
            data['action'] = 'unpublish';
        }
        return this.apiHandlerService.post(`${environment.API_URL.course_manager}/result/publish_filtered_results`, data);
    }

}
