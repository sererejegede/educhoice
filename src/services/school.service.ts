import {Injectable} from "@angular/core";
import {ApiHandlerService} from "./api-handler.service";
import {environment} from "../environments/environment";
import {Observable} from "rxjs/Observable";

@Injectable()

export class SchoolService {
  private profile = environment.API_URL.profile;
  private filteredCourseOfStudy = environment.API_URL.filteredCourseOfStudy;
  private schoolUrls = {default: 'admin/school'};


  constructor(private apihandlerservice: ApiHandlerService) {
  }


  /**
   * This method returns the schools a user created
   * @param
   */

  public getAppSettings() {
    return this.apihandlerservice.get('ping');
  }

  public getMySchools() {
    const url = 'my_school';
    return this.apihandlerservice.get(url);
  }

  /**
   * This method creates a new school
   */
  public createOrUpdateSchool(data, id?: number) {
    let url = this.schoolUrls.default;
    if (id) {
      url += `/${id}`;
      return this.apihandlerservice.put(url, data);
    }
    return this.apihandlerservice.post(url, data);
  }

  public userCreateOrUpdateSchool(data, id?: number) {
    let url = `my_school`;
    if (id) {
      url += `/${id}`;
      return this.apihandlerservice.put(url, data);
    }
    return this.apihandlerservice.post(url, data);
  }


  public getAllSchools() {
    return this.apihandlerservice.get('admin/school');
  }

  public getSchoolDashboard() {
    return this.apihandlerservice.get(`dashboard/my_school`);
  }

  public logStaffIn() {
    return this.apihandlerservice.post('auth/login', {email: 'system', password: 'password'});
  }

  /*************************************STUDENT API STARTS HERE */
  public getAllStudents() {
    return this.apihandlerservice.get('profile/student?paginate=no');
  }

  public getAllFilteredStudents(data) {
    let api_string = 'profile/student?';
    for (const key in data) {
      if (data[key] !== '') {
        api_string += `${key}=${data[key]}&`;
      }
    }
    api_string += `paginate=no`;
    return this.apihandlerservice.get(api_string);
  }

  public getAllFilteredStaff(data) {
    let api_string = 'profile/staff?';
    for (const key in data) {
      if (data[key] !== '') {
        api_string += `${key}=${data[key]}&`;
      }
    }
    api_string += `paginate=no`;
    return this.apihandlerservice.get(api_string);
  }

  public studentStatistics(data) {
    return this.apihandlerservice.post(`student-statistics`, data);
  }

  public createStudent(data) {
    let url = 'profile/student';
    return this.apihandlerservice.post(url, data);
  }

  public updateStudent (data, id) {
    const url = `profile/student/${id}`;
    return this.apihandlerservice.put(url, data);
  }

  public changeStudentStatus (student_id, status) {
    const url = `profile/student/change_status/${student_id}`;
    return this.apihandlerservice.post(url, {status: status});
  }

  public getStudentBulkTemplate() {
    const path = `student_upload_template`;
    return this.apihandlerservice.getFile(path);
  }

  public uploadStudentsInBulk(formFile) {
    const path = `admission_list/upload_students`;
    return this.apihandlerservice.postFile({}, formFile, path, 'students_list');
  }


  /*************************************APPLICANT API STARTS HERE */

  public getAllApplicants() {
    return this.apihandlerservice.get('admission/applicant');
  }

  public createOrUpdateApplicant(data, id?: string) {
    let url = 'other_applicants/store';
    if (id) {
      url = `other_applicants/update/${id}`;
    }
    return this.apihandlerservice.post(url, data);
  }

  public getAllFaculties() {
    return this.apihandlerservice.get(`faculty?paginate=no`);
  }

  public getDepartmentByFacultyId(facultyId) {
    return this.apihandlerservice.get(`faculty/${facultyId}`);
  }

  public getCourseByDepartmentId(departmentId) {
    return this.apihandlerservice.get(`department/${departmentId}`);
  }

  public getStateByCountryId(countryId) {
    return this.apihandlerservice.get(`profile/country/${countryId}`);
  }

  public getSingleState(state_id) {
    return this.apihandlerservice.get(`profile/state/${state_id}`);
  }

  public getAllCountries() {
    return this.apihandlerservice.get(`profile/country?paginate=no`);
  }

  public getAllCourses() {
    return this.apihandlerservice.get('course-of-study?paginate=no');
  }


  public getAllStaffs() {
    return this.apihandlerservice.get('profile/staff');

  }


  public createOrUpdateStaff(data, id?: string) {
    let url = 'profile/staff';
    if (id) {
      url += `/${id}`;
      return this.apihandlerservice.put(url, data);
    }
    return this.apihandlerservice.post(url, data);
  }

  public getAllActiveHours() {
    return this.apihandlerservice.get('active-hour');
  }

  public getStaffById(id) {
    return this.apihandlerservice.get(`${this.profile}/staff/${id}`);
  }

  public searchStaffByName(staffName) {
    const path = `profile/staff?search=${staffName}`;
    console.log('Search for path ', path);
    return this.apihandlerservice.get(path);

  }

  public getStudentById(id) {
    return this.apihandlerservice.get(`profile/student/${id}`);
  }

  public searchStudentByName(studentName) {
    const path = `profile/student?search=${studentName}`;
    console.log('Search for path ', path);
    return this.apihandlerservice.get(path);

  }

  public createOrUpdateActiveHour(data, id?: string) {
    let url = 'active-hour';
    if (id) {
      url += `/${id}`;
      return this.apihandlerservice.put(url, data);
    }
    return this.apihandlerservice.post(url, data);
  }


  public getAllGroups() {
    return this.apihandlerservice.get('administration/groups');

  }


  public createOrUpdateFeeItems(data, id?: string) {
    let url = 'payment/fee-item';
    if (id) {
      url += `/${id}`;
      return this.apihandlerservice.put(url, data);
    }
    return this.apihandlerservice.post(url, data);
  }

  public createPartPaymentFee(data) {
    return this.apihandlerservice.post(`payment/part-payment-fee`, data);
  }

  public updatePartPaymentFee(id: number, data: any) {
    return this.apihandlerservice.put(`payment/part-payment-fee/${id}`, data);
  }

  public getAllPartPaymentFee() {
    return this.apihandlerservice.get(`payment/part-payment-fee`);
  }

  public getFilteredPartPayments(data) {
    let api_string = 'payment/part-payment-fee?';
    for (const key in data) {
      if (data[key]) {
        api_string += `${key}=${data[key]}&`;
      }
    }
    api_string += `paginate=no`;
    return this.apihandlerservice.get(api_string);
  }

  public createTranscriptFee(data) {
    return this.apihandlerservice.post(`payment/transcript-fee`, data);
  }

  public updateTranscriptFee(data, transcript_id) {
    return this.apihandlerservice.put(`payment/transcript-fee/${transcript_id}`, data);
  }

  public getTranscriptFee() {
    return this.apihandlerservice.get(`payment/transcript-fee?paginate=no`);
  }

  public createTransferFee(data) {
    return this.apihandlerservice.post(`payment/transfer-fee`, data)
  }

  public updateTransferFee(data, transfer_id) {
    return this.apihandlerservice.put(`payment/transfer-fee/${transfer_id}`, data)
  }

  public getTransferFee() {
    return this.apihandlerservice.get(`payment/transfer-fee?paginate=no`);
  }

  public getAllFeeItems() {
    return this.apihandlerservice.get('payment/fee-item');

  }

  /**
   * assigning department
   */
  assignDepartment(data) {
    return this.apihandlerservice.post(`${this.profile}/lecturer-department`, data);
  }

  /**
   * deleting assigned department
   */
  deleteDepartment(id) {
    return this.apihandlerservice.delete(`${this.profile}/lecturer-department/${id}`);
  }

  /**
   * assigning course
   */
  assignCourse(data) {
    return this.apihandlerservice.post(`${this.profile}/lecturer-course`, data);
  }

  /**
   * delete assigned course
   */
  deleteCourse(id) {
    return this.apihandlerservice.delete(`${this.profile}/lecturer-course/${id}`);
  }

  /**
   * fetching filter course of study
   */
  getFilteredCourseOfStudy(course_of_study_id, level, session, semester) {
    return this.apihandlerservice.get(`${this.filteredCourseOfStudy}/${course_of_study_id}/curricula/${level}/${session}/${semester}`);
  }


  public getAllFees() {
    return this.apihandlerservice.get('payment/fee?paginate=no');
  }

  public createOrUpdateFee(data, id?) {
    let url = 'payment/fee';
    if (id) {
      if (typeof id === 'boolean') {
        url += `/test`;
        return this.apihandlerservice.post(url, data);
      }
      url += `/${id}`;
      return this.apihandlerservice.put(url, data);
    }
    return this.apihandlerservice.post(url, data);
  }

  public getAllStudentExceptions() {
    return this.apihandlerservice.get('profile/student-exception');

  }

  public createOrUpdateStudentException(data, id?) {
    let url = 'profile/student/exception';
    if (id) {
      url += `/${id}`;
      return this.apihandlerservice.put(url, data);
    }
    return this.apihandlerservice.post(url, data);
  }


  public getAllTransfers() {
    return this.apihandlerservice.get('profile/student/transfer');
  }

  public creatStudentTransfer(data) {
    const path = `profile/student/transfer`;
    return this.apihandlerservice.post(path, data);
  }

  public updateStudentTransfer(data, id) {
    return this.apihandlerservice.put(`profile/student/transfer/${id}`);
  }

  public getAllStudentTransferApplications() {
    return this.apihandlerservice.get('application/transfer');
  }

  public toggleApplicationStatus(status: boolean, id) {
    let path = (status) ? `application/transfer/toggleApprove/${id}` : `application/transfer/toggleDisapprove/${id}`;
    return this.apihandlerservice.put(path);
  }

  public completeTransferApplication(id) {
    return this.apihandlerservice.put(`application/transfer/complete/${id}`);
  }

  public getTransferStatuses() {
    return this.apihandlerservice.get(`application/transfer/statuses`);

  }


  public getAllStudentTranscriptApplications() {
    return this.apihandlerservice.get('application/transcript');
  }

  public toggleApplicationTranscriptStatus(status: boolean, id) {
    let path = (status) ? `application/transfer/toggleApprove/${id}` : `application/transfer/toggleDisapprove/${id}`;
    return this.apihandlerservice.put(path);
  }

  public completeTranscriptApplication(id) {
    return this.apihandlerservice.put(`application/transcript/process/${id}`);
  }

  public getTranscriptStatuses() {
    return this.apihandlerservice.get(`application/transcript/statuses`);

  }


  public getSessionsByProgrammeId(programmeId) {
    const path = `programme/${programmeId}/session`;
    return this.apihandlerservice.get(path);
  }

  public getStudentRegistrationsForSessions(studentId) {
    const path = `profile/student/registrations/${studentId}`;
    return this.apihandlerservice.get(path);
  }


  public verifyPayStackPayment(ref_id) {
    const path = `applicant/application_payment/validatePayStack/${ref_id}`;
    return this.apihandlerservice.post(path);
    // return Observable.from('hhjdhj');
  }

  public verifyXpressPayPayment(data, school_id, url?) {
    const path = `transactions/validate_express_pay/${school_id}`;
    // if (url) {
    //   return this.apihandlerservice.get(`${path}/${data['transaction-id']}`);
    // }
    return this.apihandlerservice.post(path, data);
  }

  public sendMessage(data) {
    return this.apihandlerservice.post(`${this.profile}/message`, data);
  }

}

